<?php

    require_once 'include/config.common.php';
    require_once 'commonFunctions.php';
    include_once 'main.inc.php';
    global $__db;
    if(!empty($_POST)){
        
        $face = 'neutral';
        $message = 'Thanks for providing your valuable feedback.';
        require_once 'response/response.phtml';
        $ticketId = $_POST['ticket_id'];
        $comment = $_POST['merchant_comment'];
        $sql = 'UPDATE ost_merchant_response SET response = ?, is_survey_submitted = ? WHERE ticket_id = ?';
        $data = array($comment,1,$ticketId);
        $res = mysqli_prepared_query($__db, $sql, 'sii', $data);
    }else{
        
        $params = $_GET['key'];
        $params = explode('|', clues_decrypt_data($params));
        if(empty($params[0])){
            $face = 'neutral';
            $message = 'Invalid URL';
            require_once 'response/response.phtml';
        }
        $characterLimit = RESPONSETEXTLENGTH;
        $sql = 'SELECT id FROM ost_merchant_response WHERE ticket_id=?';
        $res = mysqli_prepared_query($__db, $sql, 'i', array($params[0]));
        if(!empty($res)) {
            $face = 'neutral';
            $message = 'You have already responded to this survey.';
            require_once 'response/response.phtml';
        }else {
            if($params[3] < time()){
                $face = 'neutral';
                $message = 'It looks like you already responded to this survey or the survey has expired.';
                require_once 'response/response.phtml';
            }else if($params[2] == '1'){
                $face = 'happy';
                $message = 'Thanks for providing your valuable feedback.';
                require_once 'response/response.phtml';
                $sql = 'INSERT INTO ost_merchant_response (`ticket_id`,`response_flag`,`response_time`,`mail_sent_time`,`is_survey_submitted`) '
                     . 'values (?,?,?,?,?)';
                $data = array($params[0],1,date('Y-m-d H:i:s'),date('Y-m-d H:i:s',$params[1]),1);
                $res = mysqli_prepared_query($__db, $sql, 'iissi', $data);
            }else if($params[2] == '0'){
                $ticketId = $params[0];
                require_once 'response/merchantFeedback.phtml';
                $sql = 'INSERT INTO ost_merchant_response (`ticket_id`,`response_flag`,`response_time`,`mail_sent_time`,`is_survey_submitted`) '
                     . 'values (?,?,?,?,?)';
                $data = array($params[0],0,date('Y-m-d H:i:s'),date('Y-m-d H:i:s',$params[1]),0);
                $res = mysqli_prepared_query($__db, $sql, 'iissi', $data);
            }
        }
    }