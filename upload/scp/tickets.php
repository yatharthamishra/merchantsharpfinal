<?php

/* * ***********************************************************************
  tickets.php

  Handles all tickets related actions.

  Peter Rotich <peter@osticket.com>
  Copyright (c)  2006-2013 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */


require('staff.inc.php');
require_once(INCLUDE_DIR . 'class.ticket.php');
require_once(INCLUDE_DIR . 'class.dept.php');
require_once(INCLUDE_DIR . 'class.filter.php');
require_once(INCLUDE_DIR . 'class.canned.php');
require_once(INCLUDE_DIR . 'class.json.php');
require_once(INCLUDE_DIR . 'class.dynamic_forms.php');
require_once(INCLUDE_DIR . 'class.export.php');       // For paper sizes
require_once(INCLUDE_DIR . 'ajax.tickets.php');
require_once(INCLUDE_DIR . 'class.task.php');
require_once(INCLUDE_DIR . 'class.triage.php');
require_once(INCLUDE_DIR . 'class.topic.php');


$sc_css = true;
$page = '';
$ticket = $user = $triage = null; //clean start.
//LOCKDOWN...See if the id provided is actually valid and if the user has access.
//added by navneet

$obj = new Dept($thisstaff->getDeptId());
$manager = $obj->getManagerId();
$my_count = $obj->ticketCount($thisstaff, $thisstaff->getId(), false);

$unassign_count = $obj->ticketCount($thisstaff, 0);
$unassign_email_count = Ticket::email_count();
$reassign_count = $obj->ticketCount($thisstaff, 0, true, $thisstaff->getDeptId());



if (0 && !empty($_REQUEST['id'])) {

    $id = $_REQUEST['id'];

    $mysqli = db::getDbInstance();
    if ($mysqli) {
        $ticket = new Ticket($id);
        $os_ticket_number = $ticket->getNumber();
        $query = "select carrier_name from clues_merchant_response_tracking where ticket_number=" . $os_ticket_number;
        $response = mysqli_query($mysqli, $query);
        $result = mysqli_fetch_row($response);
        $carrier_name = $result['0'];
        $mysqli->close;
    }
}


if ($_POST['bulk_assign']) {

    if ($_POST['b_tickets'] == "") {
        $_SESSION['bulk_status'] = 0;
        header("Location:" . $_POST['location']);
        exit;
    }
    $arr = explode(",", $_POST['b_tickets']);
    $count = 0;
    foreach ($arr as $key => $val) {

        $ticket = new Ticket($val);
        $dept_id = $ticket->getDeptId();
        if ($dept_id == $thisstaff->getDeptId() || Dept::isParent($dept_id, $thisstaff->getDeptId())) {
            $count++;
            $ticket->assign($_POST['Staff_Id'], 'Bulk task assignment', false, 'U');
            $department = new Dept($dept_id);
            $dept_sla = $department->getTaskSLA();
            $ticket->updateTaskSLA($dept_sla);
        }
    }

    $_SESSION['bulk_status'] = ($count == 0) ? 0 : 1;
    header("Location:" . $_POST['location']);
    exit;
}
if ($_POST['bulk_assign2']) {
    if ($_POST['b_tickets2'] == "") {
        $_SESSION['bulk_status2'] = 0;
        header("Location:" . $_POST['location']);
        exit;
    }
    Task::bulkTaskUpdate($_POST['b_tickets2'], $_POST['Staff_Id']);
    $_SESSION['bulk_status2'] = 1;
    header("Location:" . $_POST['location']);
    exit;
}
//get canned response sla timing
if ($_POST['canned_id'] != '') {
    echo Canned::getCannedSLA($_POST['canned_id']);
    exit();
}

if ($_REQUEST['a'] == 'search') {
    if ($_REQUEST['new_search']) {
        $searchType = $_REQUEST['SearchBy'];
        $searchParam = $_REQUEST['query'];
        $tickets = Ticket::search($searchType,$searchParam);
        if(count($tickets) == 1){
            header('location:' . $_SERVER['SCRIPT_NAME'].'?id='.$tickets[0]);
        }
    }
    else $tickets = TicketsAjaxAPI::_search($_REQUEST);
    
    $uid = md5($_SERVER['QUERY_STRING']);
    foreach ($_SESSION as $key => $value) {
        if ("adv_" == substr($key, 0, 4)) {
            unset($_SESSION[$key]);
        }
        if ("search_" == substr($key, 0, 7)) {
            unset($_SESSION[$key]);
        }
    }
    $_SESSION["adv_$uid"] = json_encode($tickets);
    if ($_REQUEST['deptId']) {
        $deptId = $_REQUEST['deptId'];
        $reassign_url = isset($_REQUEST['reassign']) ? '&reassign=' . $_REQUEST['reassign'] : '';
        $email_url = isset($_REQUEST['ticket_source']) ? '&ticket_source=' . $_REQUEST['ticket_source'] : '';
        $div_id = $_REQUEST['div_id'];
        header("Location: tickets.php?advsid=$uid&DeptId=$deptId" . $reassign_url . $email_url . "&div_id=$div_id");
        exit;
    }
    if ($_REQUEST['assignee']) {
        $staffId = ltrim($_REQUEST['assignee'], "s");
        $reassign_url = isset($_REQUEST['reassign']) ? '&reassign=' . $_REQUEST['reassign'] : '';
        $div_id = $_REQUEST['div_id'];
        header("Location: tickets.php?advsid=$uid&staffId=$staffId" . $reassign_url . "&div_id=$div_id");
        exit;
    }
    if (isset($_REQUEST['ticket_timer'])) {
        header("Location: tickets.php?advsid=$uid&ticket_timer=" . $_REQUEST['ticket_timer'] . "&div_id=" . $_REQUEST['ticket_timer']);
        exit;
    }
    if (isset($_REQUEST['task_timer'])) {
        header("Location: tickets.php?advsid=$uid&task_timer=" . $_REQUEST['task_timer'] . "&div_id=" . $_REQUEST['task_timer']);
        exit;
    }
    if (isset($_REQUEST['label'])) {
        header("Location: tickets.php?advsid=$uid&label=" . $_REQUEST['label'] . "&div_id=" . $_REQUEST['label']);
        exit;
    }
    if (isset($_REQUEST['taskAssignee'])) {
        header("Location: tickets.php?advsid=$uid&taskAssignee=" . $_REQUEST['taskAssignee'] . "&div_id=UT");
        exit;
    }
}
if (isset($_POST['dept_id'])) {
    $dept_id = $_POST['dept_id'];
    $staff_id = $_POST['staff_id'];
    echo json_encode(Dept::getDepartmentMembers($dept_id, $staff_id));
    exit();
}
//end here
if ($_REQUEST['id']) {
    if (!($ticket = Ticket::lookup($_REQUEST['id'])))
        $errors['err'] = sprintf(__('%s: Unknown or invalid ID.'), __('ticket'));
    elseif (!$ticket->checkStaffAccess($thisstaff)) {
        $errors['err'] = __('Access denied. Contact admin if you believe this is in error');
        $ticket = null; //Clear ticket obj.
    }
    //added by navneet
    $department_level_privileges = $ticket->checkDepartmentAccess($thisstaff);
}

//if (isset($_POST['triage_form'])) {
//    if (isset($_POST['close']) && $_POST['close']) {
//        $ticket->autoclose();
//        $ticket->logActivity(_S('Ticket Marked Closed'), _S('Ticket flagged as Closed by the system.'));
//    } else {
//        $triage = $ticket->getTriage();
//        if ($triage) {
//            $call_customer = $_POST['call'];
//            $communication = $_POST['communication'];
//            $priority = $_POST['priority'];
//            $comment_read = $_POST['comment'];
//            $triage->movetriage($call_customer, $communication, $priority, $comment_read);
//        }
//    }
//}
//Lookup user if id is available.
if ($_REQUEST['uid'])
    $user = User::lookup($_REQUEST['uid']);

// Configure form for file uploads
$response_form = new Form(array(
    'attachments' => new FileUploadField(array('id' => 'attach',
        'name' => 'attach:response',
        'configuration' => array('extensions' => '')))
        ));
$note_form = new Form(array(
    'attachments' => new FileUploadField(array('id' => 'attach',
        'name' => 'attach:note',
        'configuration' => array('extensions' => '')))
        ));

//At this stage we know the access status. we can process the post.
if ($_POST && !$errors):

    if ($ticket && $ticket->getId()) {
        //More coffee please.
        $errors = array();
        $lock = $ticket->getLock(); //Ticket lock if any
        switch (strtolower($_POST['a'])):
            case 'reply':
                if (!$thisstaff->canPostReply())
                    $errors['err'] = __('Action denied. Contact admin for access');
                else {

                    if (!$_POST['response'])
                        $errors['response'] = __('Response required');
                    //Use locks to avoid double replies
                    if ($lock && $lock->getStaffId() != $thisstaff->getId())
                        $errors['err'] = __('Action Denied. Ticket is locked by someone else!');

                    //Make sure the email is not banned
                    if (!$errors['err'] && TicketFilter::isBanned($ticket->getEmail()))
                        $errors['err'] = __('Email is in banlist. Must be removed to reply.');
                    if (!$_POST['sla'])
                        $errors['err'] = _('SLA vale is incorrect.');
                }

                //If no error...do the do.
                $vars = $_POST;

                $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();
                //changes by meghraj
                $last_id = $thisstaff->getDeptId();
                // $last_parent_id = $ticket->getParentDeptId();

                if (!$errors && ($response = $ticket->postReply($vars, $errors, $_POST['emailreply']))) {
                    $msg = sprintf(__('%s: Reply posted successfully'), sprintf(__('Ticket #%s'), sprintf('<a href="tickets.php?id=%d"><b>%s</b></a>', $ticket->getId(), $ticket->getNumber()))
                    );
                    //changes by meghraj
                    $ticket->new_update($last_id);
                    if ($msg) {
                        $_SESSION['new_msg'] = $msg;
                        header("Location:" . "tickets.php?id=" . $ticket->getId());
                        exit;
                    }

                    // Clear attachment list
                    $response_form->setSource(array());
                    $response_form->getField('attachments')->reset();

                    // Remove staff's locks
                    TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());

                    // Cleanup response draft for this user
                    Draft::deleteForNamespace(
                            'ticket.response.' . $ticket->getId(), $thisstaff->getId());

                    // Go back to the ticket listing page on reply
                    $ticket = null;
                } elseif (!$errors['err']) {
                    $errors['err'] = __('Unable to post the reply. Correct the errors below and try again!');
                }
                break;
            case 'transfer': /** Transfer ticket * */
                //Check permission
                $cond = 0;
                if (!$thisstaff->canTransferTickets())
                    $errors['err'] = $errors['transfer'] = __('Action Denied. You are not allowed to transfer tickets.');
                else {
                    //Check target dept.
                    if (!$_POST['deptId']) {

                        $errors['deptId'] = __('Select department');
                    } elseif ($_POST['deptId'] == $ticket->getDeptId()) {
                        if ($_POST['deptId'] == 56) {
                            $department = new Dept($_POST['deptId']);
                            $slaObj = new SLA($department->ht['sla_id']);
                            $grace_period = 48;
                            if ($slaObj->id != 0)
                                $grace_period = $slaObj->ht['grace_period'];
                            if ($_POST['assignId'])
                                $ticket->saveTaskHistory(1, $_POST['deptId'], $grace_period, 'Agent', 1);
                        }
                        //commented below code to allow ticket assignment on same department
                        //$errors['deptId'] = __('Ticket already in the department');
                        $cond = 1;
                    } elseif (!($dept = Dept::lookup($_POST['deptId'])))
                        $errors['deptId'] = __('Unknown or invalid department');

                    //Transfer message - required.
                    //if (!$_POST['transfer_comments'])
                    //    $errors['transfer_comments'] = __('Transfer comments required');
                    //elseif (strlen($_POST['transfer_comments']) < 0)
                    //    $errors['transfer_comments'] = __('Transfer comments too short!');
                    //If no errors - them attempt the transfer.
//                    if (!$errors && $ticket->transfer($_POST['deptId'], $_POST['transfer_comments'],TRUE,1)) {
                    if ($cond == 0) {
                        if (!$errors && $ticket->transfer($_POST['deptId'], $_POST['transfer_comments'], TRUE, 1, $_POST['a'], $_POST['assignId'])) {
                            $msg = sprintf(__('Task transferred successfully to %s'), $ticket->getDeptName());
                            //check if this is follow up of another ticket
                            /* ---------check if this is follow up of another ticket ------------------ */
                            if ($ticket->ht['followup_number']) {
                                $ft_Sql = "update " . TICKET_TABLE . " set dept_id=" . $_POST['deptId'] . " where number=" . db_input($ticket->ht['followup_number']);
                                db_query($ft_Sql);
                            }
                            /* --------------------end check------------------- */
                            //Check to make sure the staff still has access to the ticket
                            if (!$ticket->checkStaffAccess($thisstaff))
                                $ticket = null;
                        } elseif (!$errors['transfer']) {
                            $errors['err'] = __('Unable to complete the task transfer');
                            $errors['transfer'] = __('Correct the error(s) below and try again!');
                        }
                    }
                }
                //assign code
                if (!$thisstaff->canAssignTickets())
                    $errors['err'] = $errors['assign'] = __('Action Denied. You are not allowed to assign/reassign tickets.');
                else {

                    $id = preg_replace("/[^0-9]/", "", $_POST['assignId']);

                    $claim = (is_numeric($_POST['assignId']) && $_POST['assignId'] == $thisstaff->getId());
                    $dept = $ticket->getDept();

                    if (!$_POST['assignId']) {
                        // $errors['assignId'] = __('Select assignee');
                    } elseif ($_POST['assignId'][0] != 's' && $_POST['assignId'][0] != 't' && !$claim)
                        $errors['assignId'] = sprintf('%s - %s', __('Invalid assignee'), __('get technical support'));
                    elseif ($_POST['assignId'][0] != 's' && $dept->assignMembersOnly() && !$dept->isMember($id)) {
                        $errors['assignId'] = sprintf('%s. %s', __('Invalid assignee'), __('Must be department member'));
                    } elseif ($ticket->isAssigned()) {
                        if ($_POST['assignId'][0] == 's' && $id == $ticket->getStaffId()) {
                            // $errors['assignId'] = __('Ticket already assigned to the agent.');
                        } elseif ($_POST['assignId'][0] == 't' && $id == $ticket->getTeamId())
                            $errors['assignId'] = __('Task already assigned to the team.');
                    }elseif ($id == 0) {
                        // if ($_POST['assignId'][0] == 's' && $id == $ticket->getStaffId())
                        //   $errors['assignId'] = __('Ticket already unassigned');
                    }

                    //Comments are not required on self-assignment (claim)
                    if ($claim && !$_POST['transfer_comments'])
                        $_POST['transfer_comments'] = sprintf(__('Task claimed by %s'), $thisstaff->getName());
                    //elseif (!$_POST['transfer_comments'])
                    //    $errors['transfer_comments'] = __('Transfer comments required');
                    //elseif (strlen($_POST['transfer_comments']) < 5)
                    //    $errors['transfer_comments'] = __('Transfer comments too short');
                    if ($_POST['a'] == 'transfer') {
                        $_POST['a'] = 'U';
                    }

//                    if (!$errors && $ticket->assign($_POST['assignId'], $_POST['transfer_comments'], !$claim,$_POST['a'])) {
                    if ($ticket->assign($_POST['assignId'], $_POST['transfer_comments'], !$claim, $_POST['a'])) {

                        if ($cond == 1) {

                            $department = $ticket->getDept();
                            $t_sla = $department->getTaskSLA();
                            $ticket->updateTaskSLA($t_sla, 2);
                            $ticket->new_update($thisstaff->getDeptId());
                        }
                        /* ---------check if this is follow up of another ticket ------------------ */
                        $at_type = $_POST['assignId'][0];
                        $at_id = preg_replace("/[^0-9]/", "", $_POST['assignId']);
                        if ($ticket->ht['followup_number']) {
                            if ($at_type == 's') {
                                $at_Sql = "update " . TICKET_TABLE . " set staff_id=" . $at_id . " where number=" . db_input($ticket->ht['followup_number']);
                                db_query($at_Sql);
                            } else if ($at_type == 't') {

                                $at_Sql = "update " . TICKET_TABLE . " set team_id=" . $at_id . " where number=" . db_input($ticket->ht['followup_number']);
                                db_query($at_Sql);
                            }
                        }
                        /* --------------------end check------------------- */
                        if ($claim) {
                            $msg = __('Task is NOW assigned to you!');
                        } else {
                            if ($_POST['assignId'][1] == 0) {

                                $msg = 'Task unassigned successfully';
                            } else {
                                $msg = sprintf(__('Task assigned successfully to %s'), $ticket->getAssigned());
                            }
                            TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());
                            //$ticket = null;
                        }
                    } elseif (!$errors['assign']) {
                        $errors['err'] = __('Unable to complete the task assignment');
                        $errors['assign'] = __('Correct the error(s) below and try again!');
                    }
                }
                //end here
                break;

            case 'assign':

                //  if ($_POST['deptId'] != $ticket->getDeptId()) {
                if (!$thisstaff->canTransferTickets())
                    $errors['err'] = $errors['os_transfer'] = __('Action Denied. You are not allowed to transfer tickets.');
                else {
                    //Check target dept.
                    if (!$_POST['deptId'])
                        $errors['deptId'] = __('Select department');
                    /* elseif ($_POST['deptId'] == $ticket->getDeptId()) {
                      //commented below code to allow ticket assignment on same department
                      $errors['os_deptId'] = __('Ticket already in the department');
                      } */ elseif (!($dept = Dept::lookup($_POST['deptId'])))
                        $errors['os_deptId'] = __('Unknown or invalid department');

                    //Transfer message - required.
//                    if (!$_POST['assign_comments'])
//                        $errors['os_transfer_comments'] = __('Transfer comments required');
//                    elseif (strlen($_POST['assign_comments']) < 5)
//                        $errors['os_transfer_comments'] = __('Transfer comments too shortss!');
                    //If no errors - them attempt the transfer.
                    if (!$errors && $ticket->transfer($_POST['deptId'], $_POST['assign_comments'], true, 0, $_POST['a'])) {
                        $msg = sprintf(__('Task transferred successfully to %s'), $ticket->getDeptName());
                        //check if this is follow up of another ticket
                        /* ---------check if this is follow up of another ticket ------------------ */
                        if ($ticket->ht['followup_number']) {
                            $ft_Sql = "update " . TICKET_TABLE . " set dept_id=" . $_POST['deptId'] . " where number=" . db_input($ticket->ht['followup_number']);
                            db_query($ft_Sql);
                        }
                        /* --------------------end check------------------- */
                        //Check to make sure the staff still has access to the ticket
                        if (!$ticket->checkStaffAccess($thisstaff))
                            $ticket = null;
                    } elseif (!$errors['os_transfer']) {
                        $errors['err'] = __('Unable to complete the task transfer');
                        $errors['os_transfer'] = __('Correct the error(s) below and try again!');
                    }
                }
                // }
                //assign code
                if (!$thisstaff->canAssignTickets())
                    $errors['err'] = $errors['os_assign'] = __('Action Denied. You are not allowed to assign/reassign tickets.');
                else {

                    $id = preg_replace("/[^0-9]/", "", $_POST['assignId']);

                    $claim = (is_numeric($_POST['assignId']) && $_POST['assignId'] == $thisstaff->getId());
                    $dept = $ticket->getDept();

                    if (!$_POST['assignId']) {
                        // $errors['os_assignId'] = __('Select assignee');
                    } elseif ($_POST['assignId'][0] != 's' && $_POST['assignId'][0] != 't' && !$claim)
                        $errors['os_assignId'] = sprintf('%s - %s', __('Invalid assignee'), __('get technical support'));
                    elseif ($_POST['assignId'][0] != 's' && $dept->assignMembersOnly() && !$dept->isMember($id)) {
                        $errors['os_assignId'] = sprintf('%s. %s', __('Invalid assignee'), __('Must be department member'));
                    } elseif ($ticket->isAssigned()) {
                        if ($_POST['assignId'][0] == 's' && $id == $ticket->getStaffId())
                            $errors['os_assignId'] = __('Task already assigned to the agent.');
                        elseif ($_POST['assignId'][0] == 't' && $id == $ticket->getTeamId())
                            $errors['os_assignId'] = __('Task already assigned to the team.');
                    }elseif ($id == 0) {
                        // if ($_POST['assignId'][0] == 's' && $id == $ticket->getStaffId())
                        //   $errors['os_assignId'] = __('Ticket already unassigned');
                    }

                    //Comments are not required on self-assignment (claim)
                    if ($claim && !$_POST['assign_comments'])
                        $_POST['assign_comments'] = sprintf(__('Task claimed by %s'), $thisstaff->getName());
                    //elseif (!$_POST['assign_comments'])
                    //    $errors['os_assign_comments'] = __('Assignment comments required');
                    //elseif (strlen($_POST['assign_comments']) < 5)
                    //    $errors['os_assign_comments'] = __('Comment too short');
                    if (!$errors && $ticket->assign($_POST['assignId'], $_POST['assign_comments'], !$claim, $_POST['a'])) {

                        /* ---------check if this is follow up of another ticket ------------------ */
                        $at_type = $_POST['assignId'][0];
                        $at_id = preg_replace("/[^0-9]/", "", $_POST['assignId']);
                        if ($ticket->ht['followup_number']) {
                            if ($at_type == 's') {
                                $at_Sql = "update " . TICKET_TABLE . " set staff_id=" . $at_id . " where number=" . db_input($ticket->ht['followup_number']);
                                db_query($at_Sql);
                            } else if ($at_type == 't') {
                                $at_Sql = "update " . TICKET_TABLE . " set team_id=" . $at_id . " where number=" . db_input($ticket->ht['followup_number']);
                                db_query($at_Sql);
                            }
                        }
                        /* --------------------end check------------------- */
                        if ($claim) {
                            $msg = __('Task is NOW assigned to you!');
                        } else {
                            if ($_POST['assignId'] == 0) {
                                $msg = 'Task unassigned successfully';
                            } else {
                                $msg = sprintf(__('Task assigned successfully to %s'), $ticket->getAssigned());
                            }
                            TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());
                            //$ticket = null;
                        }
                    } elseif (!$errors['os_assign']) {
                        $errors['err'] = __('Unable to complete the task assignment');
                        $errors['os_assign'] = __('Correct the error(s) below and try again!');
                    }
                }
                break;
            case 'postnote': /* Post Internal Note */
                $vars = $_POST;
                $attachments = $note_form->getField('attachments')->getClean();
                $vars['cannedattachments'] = array_merge(
                        $vars['cannedattachments'] ? : array(), $attachments);

                $wasOpen = ($ticket->isOpen());
                //changes by navneet
                //end here
                if ($vars['internal_note'] != 0) {
                    $canned_id = $vars['internal_note'];
                    $canned_obj = new Canned($canned_id);
                    $vars['title'] = $canned_obj->getTitle();
                }
                //variable created by meghraj

                $last_id = $ticket->getDeptId();

                $last_parent_id = $ticket->getParentDeptId();
                $last_id = ($last_parent_id) ? $last_parent_id : $last_id;
                if ($thisstaff->getDeptId() == MRG_ID) {
                    $last_id = $thisstaff->getDeptId();
                }
                $vars['last_id'] = $last_id;

                if (!$errors['err'] && ($note = $ticket->postNote($vars, $errors, $thisstaff, true, true))) {

                    $msg = __('Internal note posted successfully');
                    // Clear attachment list
                    $note_form->setSource(array());
                    $note_form->getField('attachments')->reset();

                    if ($wasOpen && $ticket->isClosed()) {
                        //$ticket = null; //Going back to main listing.
                    } else
                    // Ticket is still open -- clear draft for the note
                        Draft::deleteForNamespace('ticket.note.' . $ticket->getId(), $thisstaff->getId());
                } else {

                    if (!$errors['err'])
                        $errors['err'] = __('Unable to post internal note - missing or invalid data.');

                    $errors['postnote'] = __('Unable to post the note. Correct the error(s) below and try again!');
                }


                break;
            case 'triage':
                // if (isset($_POST['triage_form'])) {
                if (!isset($_POST['triage_form'])) {
                    $errors['err'] = __('Something went wrong.');
                } else if ($_POST['call'] == '' || $_POST['communication'] == '' || $_POST['priority'] == '' || $_POST['comment'] == '') {
                    $errors['err'] = __('Wrong value entered.');
                } else {
                    if (isset($_POST['close']) && $_POST['close']) {
                        $ticket->autoclose();
                        $ticket->logActivity(_S('Ticket Marked Closed'), _S('Task flagged as Closed by the system.'), $thisstaff->getUserName());
                    } else {
                        $triage = $ticket->getTriage();
                        if ($triage) {
                            $call_customer = $_POST['call'];
                            $communication = $_POST['communication'];
                            $priority = $_POST['priority'];
                            $comment_read = $_POST['comment'];
                            if ($triage->movetriage($call_customer, $communication, $priority, $comment_read)) {
                                $msg = __('Triage submitted successfully');
                            }
                        }
                    }
                }
                break;
            case 'edit':
            case 'update':
                if (!$ticket || !$thisstaff->canEditTickets())
                    $errors['err'] = __('Permission Denied. You are not allowed to edit tickets');
                elseif ($ticket->update($_POST, $errors)) {
                    $msg = __('Task updated successfully');
                    $_REQUEST['a'] = null; //Clear edit action - going back to view.
                    //Check to make sure the staff STILL has access post-update (e.g dept change).
                    if (!$ticket->checkStaffAccess($thisstaff))
                        $ticket = null;
                } elseif (!$errors['err']) {
                    $errors['err'] = __('Unable to update the task. Correct the errors below and try again!');
                }
                break;
            case 'process':

                switch (strtolower($_POST['do'])):
                    case 'release':
                        if (!$ticket->isAssigned() || !($assigned = $ticket->getAssigned())) {
                            $errors['err'] = __('Task is not assigned!');
                        } elseif ($ticket->release()) {
                            $msg = sprintf(__(
                                            /* 1$ is the current assignee, 2$ is the agent removing the assignment */
                                            'Task released (unassigned) from %1$s by %2$s'), $assigned, $thisstaff->getName());
                            $ticket->logActivity(__('Task unassigned'), $msg);
                        } else {
                            $errors['err'] = __('Problems releasing the task. Try again');
                        }
                        break;
                    case 'claim':
                        if (!$thisstaff->canAssignTickets()) {
                            $errors['err'] = __('Permission Denied. You are not allowed to assign/claim tasks.');
                        } elseif (!$ticket->isOpen()) {
                            $errors['err'] = __('Only open tickets can be assigned');
                        } elseif ($ticket->isAssigned()) {
                            $errors['err'] = sprintf(__('Task is already assigned to %s'), $ticket->getAssigned());
                        } elseif ($ticket->claim()) {
                            $msg = __('Task is now assigned to you!');
                        } else {
                            $errors['err'] = __('Problems assigning the task. Try again');
                        }
                        break;
                    case 'overdue':
                        $dept = $ticket->getDept();
                        if (!$dept || !$dept->isManager($thisstaff)) {
                            $errors['err'] = __('Permission Denied. You are not allowed to flag tickets OSLA');
                        } elseif ($ticket->markOverdue()) {
                            $msg = sprintf(__('Task flagged as OSLA by %s'), $thisstaff->getName());
                            $ticket->logActivity(__('Task Marked OSLA'), $msg);
                        } else {
                            $errors['err'] = __('Problems marking the the task OSLA. Try again');
                        }
                        break;
                    case 'answered':
                        $dept = $ticket->getDept();

                        if (!$dept || !$dept->isManager($thisstaff)) {
                            $errors['err'] = __('Permission Denied. You are not allowed to flag tickets');
                        } elseif ($ticket->markAnswered()) {

                            $msg = sprintf(__('Task flagged as answered by %s'), $thisstaff->getName());
                            $ticket->logActivity(__('Ticket Marked Answered'), $msg);
                        } else {
                            $errors['err'] = __('Problems marking the the task answered. Try again');
                        }
                        break;
                    case 'unanswered':
                        $dept = $ticket->getDept();
                        if (!$dept || !$dept->isManager($thisstaff)) {
                            $errors['err'] = __('Permission Denied. You are not allowed to flag tasks');
                        } elseif ($ticket->markUnAnswered()) {
                            $msg = sprintf(__('Task flagged as unanswered by %s'), $thisstaff->getName());
                            $ticket->logActivity(__('Task Marked Unanswered'), $msg);
                        } else {
                            $errors['err'] = __('Problems marking the task unanswered. Try again');
                        }
                        break;
                    case 'banemail':
                        if (!$thisstaff->canBanEmails()) {
                            $errors['err'] = __('Permission Denied. You are not allowed to ban emails');
                        } elseif (BanList::includes($ticket->getEmail())) {
                            $errors['err'] = __('Email already in banlist');
                        } elseif (Banlist::add($ticket->getEmail(), $thisstaff->getName())) {
                            $msg = sprintf(__('Email %s added to banlist'), $ticket->getEmail());
                        } else {
                            $errors['err'] = __('Unable to add the email to banlist');
                        }
                        break;
                    case 'unbanemail':
                        if (!$thisstaff->canBanEmails()) {
                            $errors['err'] = __('Permission Denied. You are not allowed to remove emails from banlist.');
                        } elseif (Banlist::remove($ticket->getEmail())) {
                            $msg = __('Email removed from banlist');
                        } elseif (!BanList::includes($ticket->getEmail())) {
                            $warn = __('Email is not in the banlist');
                        } else {
                            $errors['err'] = __('Unable to remove the email from banlist. Try again.');
                        }
                        break;
                    case 'changeuser':
                        if (!$thisstaff->canEditTickets()) {
                            $errors['err'] = __('Permission Denied. You are not allowed to edit tasks');
                        } elseif (!$_POST['user_id'] || !($user = User::lookup($_POST['user_id']))) {
                            $errors['err'] = __('Unknown user selected');
                        } elseif ($ticket->changeOwner($user)) {
                            $msg = sprintf(__('Ticket ownership changed to %s'), Format::htmlchars($user->getName()));
                        } else {
                            $errors['err'] = __('Unable to change ticket ownership. Try again');
                        }
                        break;
                    default:
                        $errors['err'] = __('You must select action to perform');
                endswitch;
                break;
            case 'task_assign':

                $task = $ticket->getTask();
                if (!$task->assign($_POST['assignId'], 'assign')) {
                    $errors['assign_task'] = __('Unable to assign ticket.');
                } else {
                    $msg = __('Ticket assigned successfully');
                }

                break;
            default:
                $errors['err'] = __('Unknown action');
        endswitch;
        if ($ticket && is_object($ticket))
            $ticket->reload(); //Reload ticket info following post processing
    }elseif ($_POST['a']) {

        switch ($_POST['a']) {
            case 'open':
                $ticket = null;
                if (!$thisstaff || !$thisstaff->canCreateTickets()) {
                    $errors['err'] = sprintf('%s %s', sprintf(__('You do not have permission %s.'), __('to create tickets')), __('Contact admin for such access'));
                } else {
                    $vars = $_POST;
                    $vars['uid'] = $user ? $user->getId() : 0;

                    $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();

                    if (($ticket = Ticket::open($vars, $errors))) {
                        $msg = __('Ticket created successfully');
                        $_REQUEST['a'] = null;
                        if (!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket = null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();
                        unset($_SESSION[':form-data']);
                    } elseif (!$errors['err']) {
                        $errors['err'] = __('Unable to create the ticket. Correct the error(s) and try again');
                    }
                }
                break;
        }
    }
    if (!$errors)
        $thisstaff->resetStats(); //We'll need to reflect any changes just made!
endif;

/* ... Quick stats ... */
//added by navneet
if (!isset($_REQUEST['task_timer']) && !isset($_REQUEST['ticket_timer']) && !isset($_REQUEST['label']) && !($_REQUEST['div_id'] == 'UT') && !($_REQUEST['div_id'] == 'UTI')) {
    if ($_GET['DeptId'] || $_GET['deptId']) {
        $d_id = isset($_GET['DeptId']) ? $_GET['DeptId'] : $_GET['deptId'];
        if (isset($_GET['reassign'])) {
            $stats = $thisstaff->getDeptTicketsStats($d_id, true, $_GET['reassign']);
            $dept_link = '&deptId=' . $d_id . '&reassign=' . $_GET['reassign'];
        } else {
            $stats = $thisstaff->getDeptTicketsStats($d_id);
            $dept_link = '&deptId=' . $d_id;
        }
    } else if (isset($_GET['staffId'])) {

        /*  if (isset($_GET['reassign'])) {
          $stats = $thisstaff->getDeptTicketsStats($_GET['staffId'], true, $_GET['reassign']);
          $dept_link = '&staffId=' . $_GET['staffId'] . '&reassign=' . $_GET['reassign'];
          } else { */
        $stats = $thisstaff->getDeptTicketsStats($_GET['staffId'], true);
        $dept_link = '&staffId=' . $_GET['staffId'];
        // }
    } else {

        $stats = $thisstaff->getTicketsStats();
        $dept_link = '';
    }
    if ($_REQUEST['ticket_source'] == 'Email') {
        $dept_link .= '&ticket_source=Email';
    }
    /* MC-39 Dinesh */
    if ($_GET['div_id'] <> '') {
        $dept_link.='&div_id=' . $_GET['div_id'];
    }
//Navigation
    $nav->setTabActive('tickets');
    $open_name = _P('queue-name',
            /* This is the name of the open ticket queue */ 'Open');
    if ($cfg->showAnsweredTickets()) {
        $nav->addSubMenu(array('desc' => $open_name . ' (' . number_format($stats['open']) . ')',
            'title' => __('Open Tickets'),
            'href' => 'tickets.php?' . ltrim($dept_link, "&"),
            'iconclass' => 'Ticket'), (!$_REQUEST['status'] || $_REQUEST['status'] == 'open'));
    } else {

        if ($stats) {

            $nav->addSubMenu(array('desc' => $open_name . ' (' . number_format($stats['open']) . ')',
                'title' => __('Open Tickets'),
                'href' => 'tickets.php' . $dept_link,
                'iconclass' => 'Ticket'), (!$_REQUEST['status'] || $_REQUEST['status'] == 'open'));
        }

        //if($stats['answered']) {
        $nav->addSubMenu(array('desc' => __('Answered') . ' (' . number_format($stats['answered']) . ')',
            'title' => __('Answered Tickets'),
            'href' => 'tickets.php?status=answered' . $dept_link,
            'iconclass' => 'answeredTickets'), ($_REQUEST['status'] == 'answered'));
        // }
    }
    $nav->addSubMenu(array('desc' => __('Reopen') . ' (' . number_format($stats['reopen']) . ')',
        'title' => __('Reopen Tickets'),
        'href' => 'tickets.php?status=reopen' . $dept_link,
        'iconclass' => 'reopenTickets'), ($_REQUEST['status'] == 'reopen'));

    $nav->addSubMenu(array('desc' => __('Inprogress') . ' (' . number_format($stats['inprogress']) . ')',
        'title' => __('Inprogress Tickets'),
        'href' => 'tickets.php?status=inprogress' . $dept_link,
        'iconclass' => 'inprogressTickets'), ($_REQUEST['status'] == 'inprogress'));

//if($stats['assigned']) {
    /*
      $nav->addSubMenu(array('desc' => __('My Tickets') . ' (' . number_format($stats['assigned']) . ')',
      'title' => __('Assigned Tickets'),
      'href' => 'tickets.php?status=assigned' . $dept_link,
      'iconclass' => 'assignedTickets'), ($_REQUEST['status'] == 'assigned'));
      //}
      //if($stats['reaching_osla']) {
      $nav->addSubMenu(array('desc' => __('Reaching OSLA') . ' (' . number_format($stats['reaching_osla']) . ')',
      'title' => __('Reaching Osla Tickets'),
      'href' => 'tickets.php?status=reachingOSLA' . $dept_link,
      'iconclass' => 'roslaTickets'), ($_REQUEST['status'] == 'reachingOSLA'));
     */

//}
//if($stats['overdue']) {
    $nav->addSubMenu(array('desc' => __('OSLA') . ' (' . number_format($stats['overdue']) . ')',
        'title' => __('Stale Tickets'),
        'href' => 'tickets.php?status=overdue' . $dept_link,
        'iconclass' => 'overdueTickets'), ($_REQUEST['status'] == 'overdue'));

    if (!$sysnotice && $stats['overdue'] > 10)
        $sysnotice = sprintf(__('%d OSLA tickets!'), $stats['overdue']);
//}
//if($stats['solved']) {
    $nav->addSubMenu(array('desc' => __('solved') . ' (' . number_format($stats['solved']) . ')',
        'title' => __('Solved Tickets'),
        'href' => 'tickets.php?status=solved' . $dept_link,
        'iconclass' => 'solvedTickets'), ($_REQUEST['status'] == 'solved'));


//}

    if ($thisstaff->showAssignedOnly() && $stats['closed']) {
        $nav->addSubMenu(array('desc' => __('My Closed Tickets') . ' (' . number_format($stats['closed']) . ')',
            'title' => __('My Closed Tickets'),
            'href' => 'tickets.php?status=closed' . $dept_link,
            'iconclass' => 'closedTickets'), ($_REQUEST['status'] == 'closed'));
    } else {

        $nav->addSubMenu(array('desc' => __('Closed') . ' (' . number_format($stats['closed']) . ')',
            'title' => __('Closed Tickets'),
            'href' => 'tickets.php?status=closed' . $dept_link,
            'iconclass' => 'closedTickets'), ($_REQUEST['status'] == 'closed'));
    }

    if ($thisstaff->canCreateTickets()) {
        $nav->addSubMenu(array('desc' => __('New Ticket'),
            'title' => __('Open a New Ticket'),
            'href' => 'tickets.php?a=open&' . $dept_link,
            'iconclass' => 'newTicket',
            'id' => 'new-ticket'), ($_REQUEST['a'] == 'open'));
    }
}
$ost->addExtraHeader('<script type="text/javascript" src="js/ticket.js"></script>');
$ost->addExtraHeader('<meta name="tip-namespace" content="tickets.queue" />', "$('#content').data('tipNamespace', 'tickets.queue');");

$inc = 'tickets.inc.php';
if ($ticket) {
    if ($ticket->getStatusId() == RRSTATUSID) {
        $ticket->setStatus(OSTATUSID);
        header('location:' . $_SERVER['REQUEST_URI']);
    }
    $ost->setPageTitle(sprintf(__('Ticket #%s'), $ticket->getNumber()));
    $nav->setActiveSubMenu(-1);
    $inc = 'ticket-view.inc.php';
    if ($_REQUEST['a'] == 'edit' && $thisstaff->canEditTickets()) {
        $inc = 'ticket-edit.inc.php';
        if (!$forms)
            $forms = DynamicFormEntry::forTicket($ticket->getId());
        // Auto add new fields to the entries
        foreach ($forms as $f)
            $f->addMissingFields();
    } elseif ($_REQUEST['a'] == 'print' && !$ticket->pdfExport($_REQUEST['psize'], $_REQUEST['notes']))
        $errors['err'] = __('Internal error: Unable to export the ticket to PDF for print.');
} else {
    $inc = 'tickets.inc.php';
    if ($_REQUEST['a'] == 'open' && $thisstaff->canCreateTickets())
        $inc = 'ticket-open.inc.php';
    elseif ($_REQUEST['a'] == 'export') {
        $ts = strftime('%Y%m%d');
        /* if (!($token = $_REQUEST['h']))
          $errors['err'] = __('Query token required');
          elseif (!($query = $_SESSION['search_' . $token]))
          $errors['err'] = __('Query token not found');
          elseif (isset($_REQUEST['ticket_id']) && $_REQUEST['ticket_id'] <> '') {

          if (!Export::saveSelectedTickets($query, "tickets-$ts.csv", 'csv', $_REQUEST['ticket_id']))
          $errors['err'] = __('Internal error: Unable to dump query results');
          }
          elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv'))
          $errors['err'] = __('Internal error: Unable to dump query results'); */
        if (!($token = $_REQUEST['h']))
            $errors['err'] = __('Query token required');
        elseif (isset($_SESSION['search_' . $token]) && $_SESSION['search_' . $token] <> '') {
            $query = $_SESSION['search_' . $token];
            if (isset($_REQUEST['ticket_id']) && $_REQUEST['ticket_id'] <> '') {

                if (!Export::saveSelectedTickets($query, "tickets-$ts.csv", 'csv', $_REQUEST['ticket_id'], false))
                    $errors['err'] = __('Internal error: Unable to dump query results');
            }
            elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv', false))
                $errors['err'] = __('Internal error: Unable to dump query results');
        }
        elseif (isset($_SESSION['temp_export_query']) && $_SESSION['temp_export_query'] <> '') {
            $query = $_SESSION['temp_export_query'];
            if (isset($_REQUEST['ticket_id']) && $_REQUEST['ticket_id'] <> '') {

                if (!Export::saveSelectedTickets($query, "tickets-$ts.csv", 'csv', $_REQUEST['ticket_id'], true))
                    $errors['err'] = __('Internal error: Unable to dump query results');
            }
            elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv', true))
                $errors['err'] = __('Internal error: Unable to dump query results');
        }else {

            $errors['err'] = __('Query token not found');
        }
    }

    //Clear active submenu on search with no status
    if ($_REQUEST['a'] == 'search' && !$_REQUEST['status'])
        $nav->setActiveSubMenu(-1);

    //set refresh rate if the user has it configured
    if (!$_POST && !$_REQUEST['a'] && ($min = $thisstaff->getRefreshRate())) {
        $js = "clearTimeout(window.ticket_refresh);
               window.ticket_refresh = setTimeout($.refreshTicketView,"
                . ($min * 60000) . ");";
        $ost->addExtraHeader('<script type="text/javascript">' . $js . '</script>', $js);
    }
}

require_once(STAFFINC_DIR . 'header.inc.php');
require_once(STAFFINC_DIR . $inc);
print $response_form->getMedia();
require_once(STAFFINC_DIR . 'footer.inc.php');
