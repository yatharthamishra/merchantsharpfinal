<?php

require('admin.inc.php');
include(INCLUDE_DIR.'ajax.tickets.php');

$page='bulkReply.inc.php';
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
if($_POST){
        
    $ticket_nos=trim($_POST['ticket'],",");
    $ticket_nos = preg_replace('/\s+/', '', $ticket_nos);
    $ticket_array=  explode(",", $ticket_nos);
    $canned_id=$_POST['canned_id'];
    $status=$_POST['status'];

    if(!$_POST['ticket']){
        echo 'Please enter Ticket No';exit;
    }
    if(!$_POST['canned_id']){
        echo 'Please enter Canned Id';exit;
    }
    if(!$_POST['status']){
        echo 'Please enter Status';exit;
    }
    $obj=Ticket::lookupByNumber($ticket_array[0]);
    $response=TicketsAjaxAPI::cannedResponse($obj->getId(),$canned_id,'json');
    $response=json_decode($response,true);
    $vars['cannedattachments']=array();

    $files=$response['files'];

    foreach($files as $k=>$v){
      array_push($vars['cannedattachments'],$v['id']);
     }
    
    
    $vars['sla']=48;
    $vars['reply_status_id']=$status;
    $vars['emailreply']=1;
    
    $tickets=array();
    foreach ($ticket_array as $k=>$v){
        
        $ticket=  Ticket::lookupByNumber($v);
        $response=TicketsAjaxAPI::cannedResponse($ticket->getId(),$canned_id,'json');
        $response=json_decode($response,true);
        $vars['response']=$response['response'];
        
        if(!$ticket){
            $err= 'Wrong Ticket No entered: '.$v;break;
        }
        if($ticket->postReply($vars, $errors, true)){
            array_push($tickets, $ticket->getId());
        }
       
    }
    $t_string=implode(",", $tickets);
    $sql = 'update ' . TICKET_TABLE . ' set last_id=' . MRG_ID . ' where ticket_id in (' .$t_string.')';
    db_query($sql);
    if($err){
        echo $err;exit;
    }
    
    echo 'Bulk post reply successfuly executed';exit;
}
include(STAFFINC_DIR.'footer.inc.php');
?>

