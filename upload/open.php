<?php

/* * *******************************************************************
  open.php

  New tickets handle.

  Peter Rotich <peter@osticket.com>
  Copyright (c)  2006-2013 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */
require('client.inc.php');
define('SOURCE', 'Web'); //Ticket source.
$ticket = null;
$errors = array();
$_SESSION['selected_topic_id'] = $_REQUEST[topic_id];


if ($_POST['mode']) {
    $result=Topic::ChildhelpTopics(true,$_POST['ht_id']);
        
     if(count($result) > 0 ){   
        $next = $_POST['type'];
        if($next<4){
            echo '<select id="htdd_'.$next.'" name="htdd_'.$next.'" onchange="fn_custom_dropdown_new('.$next.');">';
        }else{
            echo '<select id="topicId" name="topicId">';
        }
           echo '<option value="">- select - </option>';
        foreach($result  as $k =>$v){
            echo '<option value="'.$k.'">'.$v.'</option>';
        }
        echo '</select>';
    }


    die;
}

if ($_REQUEST['follow_id']) {
    $_SESSION['follow_id'] = $_REQUEST['follow_id'];
}
if ($_POST) {
    //zendesk code starts here
//    $topic_id = $_POST['olddropdown'];
//    $sql = "select topic_id,topic,notes from " . TOPIC_TABLE . " where topic_id=$topic_id";
//
//    if (($res = db_query($sql)) && db_num_rows($res)) {
//        while ($rec = db_fetch_array($res)) {
//            $topic_id = $rec['topic_id'];
//            $topic = $rec['topic'];
//            $note = $rec['notes'];
//        }
//        $main_note = $note;
//        $main_issue = $topic;
//        $main_id = $topic_id;
//    }
//    $arr = explode(" ", $topic);
//    $string = "";
//    foreach ($arr as $val) {
//        if ($val == '&') {
//            $string.='_';
//            continue;
//        }
//        $string.='_' . $val;
//    }
//    $string = ltrim($string, '_');
//
//    if ($_POST[$string]) {
//
//        $topic_id = $_POST[$string];
//        $sql = "select topic_id,topic,notes from " . TOPIC_TABLE . " where topic_id=$topic_id";
//
//        if (($res = db_query($sql)) && db_num_rows($res)) {
//            while ($rec = db_fetch_array($res)) {
//                $topic_id = $rec['topic_id'];
//                $topic = $rec['topic'];
//                $note = $rec['notes'];
//            }
//
//            $sub_note = $note;
//            $sub_issues = $topic;
//            $sub_id = $topic_id;
//        }
//    }
//    $arr = explode(" ", $topic);
//    $string = "";
//    foreach ($arr as $val) {
//        if ($val == '&') {
//            $string.='_';
//            continue;
//        }
//        $string.='_' . $val;
//    }
//    $string = ltrim($string, '_');
//    if ($_POST[$string]) {
//
//        $topic_id = $_POST[$string];
//        $sql = "select topic_id,topic,notes from " . TOPIC_TABLE . " where topic_id=$topic_id";
//
//        if (($res = db_query($sql)) && db_num_rows($res)) {
//            while ($rec = db_fetch_array($res)) {
//                $topic_id = $rec['topic_id'];
//                $topic = $rec['topic'];
//                $note = $rec['notes'];
//            }
//
//            $sub_note_new = $note;
//            $sub_issues_new = $topic;
//            $sub_id_new = $topic_id;
//        }
//    }
    if ($_POST['topicId']) {
        $topic_id = $_POST['topicId'];
        $p_issue=array();
        $sql = "select topic_id,topic_pid,topic,notes from " . TOPIC_TABLE . " where topic_id=$topic_id";
        $res1 = db_query($sql);
        $res1 = db_fetch_array($res1);
        $p_id = $res1['topic_pid'];
        $p_topic = $res1['topic'];
        $i = 0;
        while(!empty($p_id)){
            $sql1 = "select topic_pid,topic from " . TOPIC_TABLE . " where topic_id=$p_id";
            $res1 = db_query($sql1);
            $res1 = db_fetch_array($res1);
            $p_topic = $res1['topic'];
            $p_issue[$i++]=array($p_id,$p_topic);
            $p_id = $res1['topic_pid'];
        }
        if (($res = db_query($sql)) && db_num_rows($res)) {
            while ($rec = db_fetch_array($res)) {
                $topic_id = $rec['topic_id'];
                $topic = $rec['topic'];
                $note = $rec['notes'];
            }
            $sub_sub_note = $note;
            $sub_sub_issues = $topic;
            $sub_sub_id = $topic_id;
        }
        $main_issue = $p_issue[1][1];
        $sub_issues = $p_issue[0][1];
    }
//    if ($_POST['olddropdown'] && $_REQUEST['email'] && $_REQUEST['merchantPhone'] && ZENDESK_TICKET) {
//
//        require('ticket_create_merchant.php');
//    }

    //zendesk code end here
    //////////////////////////////
    $vars = $_POST;
    if ($vars['792f47270240ac4f']) {
        $summary = $main_issue ? $main_issue . ' / ' : '';
        $summary.=$sub_issues ? $sub_issues . ' / ' : '';
        $summary.=$sub_issues_new ? $sub_issues_new . ' / ' : '';
        $summary.=$sub_sub_issues ? $sub_sub_issues . ' / ' : '';
        $vars['792f47270240ac4f'] = rtrim($summary, ' / ');
    }
    $vars['deptId'] = $vars['emailId'] = 0; //Just Making sure we don't accept crap...only topicId is expected.
    if ($thisclient) {
        $vars['uid'] = $thisclient->getId();
    } elseif ($cfg->isCaptchaEnabled()) {
        if (!$_POST['captcha'])
            $errors['captcha'] = __('Enter text shown on the image');
        elseif (strcmp($_SESSION['captcha'], md5(strtoupper($_POST['captcha']))))
            $errors['captcha'] = __('Invalid - try again!');
    }
   $cause_id = json_decode(CAUSE_ID,true);
   
   if(in_array($_POST['topicId'],array_keys($cause_id)) && (!filter_var($_POST['7c3d46b1fa80a6f7'],FILTER_VALIDATE_INT)===TRUE)){
       $errors['orderId']=__('Enter single Order Id');
   }

    $tform = TicketForm::objects()->one()->getForm($vars);
    $messageField = $tform->getField('message');
    $attachments = $messageField->getWidget()->getAttachments();
    if (!$errors && $messageField->isAttachmentsEnabled())
        $vars['cannedattachments'] = $attachments->getClean();

    // Drop the draft.. If there are validation errors, the content
    // submitted will be displayed back to the user
    Draft::deleteForNamespace('ticket.client.' . substr(session_id(), -12));

    //follow ticket integrate 20-08-2015
    if ($_SESSION['follow_id']) {
        $original_message = $vars['message'];
        $changed_message = "Follow up of this Ticket #" . $_SESSION['follow_id'] . "<br>" . $original_message;
        $vars['message'] = $changed_message;
    }
    //Ticket::create...checks for errors..
    if (($ticket = Ticket::create($vars, $errors, SOURCE))) {
        $msg = __('Support ticket request created');
        $vars['number'] = $ticket->number;
        Ticket::TicketCreationSource($vars);
        // Drop session-backed form data
        unset($_SESSION[':form-data']);
        //Logged in...simply view the newly created ticket.
        if ($thisclient && $thisclient->isValid()) {
            session_write_close();
            session_regenerate_id();
            $logUpload = json_encode(array("field1" => 'Successful Ticket Creation', "field2" => 'ticket_id : '.$ticket->getId(), 
                "field3" => 'draft_id : '.$vars['draft_id'], "data" => array("variables"=>array('email'=>$vars['email'], 'phone'=>$vars['merchantPhone'],
                'topic_id'=>$vars['topicId']),"file"=>"open.php")));
            Analog::log('merchant_helpdesk', 'create_ticket_success',$logUpload ,$config_logger['DOMAIN_MODULE_THRESHOLD_LOG_LEVEL']['merchant_helpdesk']['create_ticket_success']);            
            @header('Location: tickets.php?id=' . $ticket->getId());
        }
    } else {   
        $logUpload = json_encode(array("field1" => 'Ticket Creation Failure', "error_name" => "ticket_creation_failure",
            "file"=>"open.php", "data" => array('email'=>$vars['email'], 'phone'=> $vars['merchantPhone'],'topic_name'=>$vars['792f47270240ac4f'],
                'order_id'=>$vars['7c3d46b1fa80a6f7'],'topic_id'=>$vars['topicId'], 'sub_topic_1'=>$vars['htdd_1'], 'sub_topic_2'=>$vars['htdd_2'], 'sub_topic_3'=>$vars['htdd_3'])));
        Analog::log('merchant_helpdesk', 'create_ticket_failure',$logUpload ,$config_logger['DOMAIN_MODULE_THRESHOLD_LOG_LEVEL']['merchant_helpdesk']['create_ticket_failure']);
        $errors['err'] = $errors['err'] ? $errors['err'] : __('Unable to create a ticket. Please correct errors below and try again!'."/n".$errors['orderId']);
    }
}

//page
$nav->setActiveNav('new');
if ($cfg->isClientLoginRequired()) {
    if (!$thisclient) {
        require_once 'secure.inc.php';
    } elseif ($thisclient->isGuest()) {
        require_once 'login.php';
        exit();
    }
}

require(CLIENTINC_DIR . 'header.inc.php');

if ($ticket && (
        (($topic = $ticket->getTopic()) && ($page = $topic->getPage())) || ($page = $cfg->getThankYouPage())
        )) {
    // Thank the user and promise speedy resolution!
    echo Format::viewableImages($ticket->replaceVars($page->getBody()));
} else {
    require(CLIENTINC_DIR . 'open.inc.php');
}
//require(CLIENTINC_DIR.'footer.inc.php');
?>
