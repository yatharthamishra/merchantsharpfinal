<?php

function clues_encrypt_data($data) {
    
    $urlEncrypt = json_decode(SIGNONDECRYPTION, true);
    $encryption_method = $urlEncrypt["encryption_method"];
    $secret_hash = $urlEncrypt["secret_hash"];
    $iv_size = $urlEncrypt["iv_size"]/2;
    $iv = bin2hex(openssl_random_pseudo_bytes($iv_size));
    $iv_size = strlen($iv);
    
    $encrypted_message = bin2hex(rawurlencode(openssl_encrypt($data, $encryption_method, $secret_hash, 0, $iv)));
    $encrypted_message = $iv . $encrypted_message;

    return $encrypted_message;
}

function clues_decrypt_data($data) {
    
    $urlDecrypt = json_decode(SIGNONDECRYPTION, true);
    $encryption_method = $urlDecrypt["encryption_method"];
    $secret_hash = $urlDecrypt["secret_hash"];
    $iv_size = $urlDecrypt["iv_size"];
    $iv = substr($data, 0, $iv_size);
    
    $decrypted_message = openssl_decrypt(rawurldecode(pack("H*", substr($data, $iv_size))), $encryption_method, $secret_hash, 0, $iv);
    return $decrypted_message;
}