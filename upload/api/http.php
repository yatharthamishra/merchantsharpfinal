<?php
/*********************************************************************
    http.php

    HTTP controller for the osTicket API

    Jared Hancock
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
// Use sessions — it's important for SSO authentication, which uses
// /api/auth/ext
define('DISABLE_SESSION', false);

require 'api.inc.php';

# Include the main api urls
require_once INCLUDE_DIR."class.dispatcher.php";

$dispatcher = patterns('',
        url_post("^/tickets\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','create')),
        url('^/tasks/', patterns('',
                url_post("^cron$", array('api.cron.php:CronApiController', 'execute'))
         )),
         url_post("^/ticket\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getTickets')),
        url_post("^/thread\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getThreads')),
        url_get("^/closedTicketForMerchant\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getClosedTicketsInWhichMerchantHasNotReplied')),
        url_post("^/internalnote\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','postNote')),
        url_post("^/message\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','postMessage')),
        url_post("^/helptopic\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getTopics')),
        url_post("^/subject\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getSubject')),
        url_post("^/mandatory\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getMandatoryInfo')),
        url_get("^/ticket/(?P<ticket_id>\d+)$", array('api.tickets.php:TicketApiController','getTicket'))
        
       
        );

Signal::send('api', $dispatcher);

# Call the respective function
print $dispatcher->resolve($ost->get_path_info());
?>
