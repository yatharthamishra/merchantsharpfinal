<?php

include_once INCLUDE_DIR . 'class.api.php';
include_once INCLUDE_DIR . 'class.ticket.php';
include_once INCLUDE_DIR . 'class.topic.php';

class TicketApiController extends ApiController {
    # Supported arguments -- anything else is an error. These items will be
    # inspected _after_ the fixup() method of the ApiXxxDataParser classes
    # so that all supported input formats should be supported

    function getRequestStructure($format, $data = null) {
        $supported = array(
            "alert", "autorespond", "source", "topicId",
            "attachments" => array("*" =>
                array("name", "type", "data", "encoding", "size")
            ),
            "message", "ip", "priorityId"
        );
        # Fetch dynamic form field names for the given help topic and add
        # the names to the supported request structure
        if (isset($data['topicId']) && ($topic = Topic::lookup($data['topicId'])) && ($form = $topic->getForm())) {
            foreach ($form->getDynamicFields() as $field)
                $supported[] = $field->get('name');
        }

        # Ticket form fields
        # TODO: Support userId for existing user
        if (($form = TicketForm::getInstance()))
            foreach ($form->getFields() as $field)
                $supported[] = $field->get('name');

        # User form fields
        if (($form = UserForm::getInstance()))
            foreach ($form->getFields() as $field)
                $supported[] = $field->get('name');

        if (!strcasecmp($format, 'email')) {
            $supported = array_merge($supported, array('header', 'mid',
                'emailId', 'to-email-id', 'ticketId', 'reply-to', 'reply-to-name',
                'in-reply-to', 'references', 'thread-type',
                'flags' => array('bounce', 'auto-reply', 'spam', 'viral'),
                'recipients' => array('*' => array('name', 'email', 'source'))
            ));

            $supported['attachments']['*'][] = 'cid';
        }

        return $supported;
    }

    /*
      Validate data - overwrites parent's validator for additional validations.
     */

    function validate(&$data, $format, $strict = true) {
        global $ost;

        //Call parent to Validate the structure
        if (!parent::validate($data, $format, $strict) && $strict)
            $this->exerr(400, __('Unexpected or invalid data received'));

        // Use the settings on the thread entry on the ticket details
        // form to validate the attachments in the email
        $tform = TicketForm::objects()->one()->getForm();
        $messageField = $tform->getField('message');
        $fileField = $messageField->getWidget()->getAttachments();

        // Nuke attachments IF API files are not allowed.
        if (!$messageField->isAttachmentsEnabled())
            $data['attachments'] = array();

        //Validate attachments: Do error checking... soft fail - set the error and pass on the request.
        if ($data['attachments'] && is_array($data['attachments'])) {
            foreach ($data['attachments'] as &$file) {
                if ($file['encoding'] && !strcasecmp($file['encoding'], 'base64')) {
                    if (!($file['data'] = base64_decode($file['data'], true)))
                        $file['error'] = sprintf(__('%s: Poorly encoded base64 data'), Format::htmlchars($file['name']));
                }
                // Validate and save immediately
                try {
                    $file['id'] = $fileField->uploadAttachment($file);
                } catch (FileUploadError $ex) {
                    $file['error'] = $file['name'] . ': ' . $ex->getMessage();
                }
            }
            unset($file);
        }

        return true;
    }

    function create($format) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $ticket = null;
        if (!strcasecmp($format, 'email')) {
            # Handle remote piped emails - could be a reply...etc.
            $ticket = $this->processEmail();
        } else {
            # Parse request body
            $ticket = $this->createTicket($this->getRequest($format));
        }

        if (!$ticket)
            return $this->exerr(500, __("Unable to create new ticket: unknown error"));

        $this->response(201, $ticket->getNumber());
    }

    /* function to get ticket detail */
    /* author:Navneet */

    function getTicket($number) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $ticket_id = Ticket::getIdByNumber($number);
        $ticket = new Ticket($ticket_id);
        $ticket->ht['order_id'] = $ticket->getOrderId();
        $ticket->ht['subject'] = $ticket->getSubject();
        $message_array = $ticket->getMessages();
        $arr = end($message_array);
        $array = $arr['body'];
        $id = $arr['id'];
        $tentry = $ticket->getThreadEntry($id);
        $ticket->ht['attachment_link'] = $tentry->getAttachmentsLinks();
        $ticket->ht['ticket_detail'] = $array->body;
        $ticket->ht['staff'] = $ticket->getAssignee()->name;
        $ticket->ht['status'] = $ticket->getStatus()->ht['name'];
        $ticket->ht['issue_type'] = $ticket->getDeptName();
        $ticket->ht['sla'] = $ticket->getSLA()->ht['grace_period'];
        $helptopic = explode('/', $ticket->getHelpTopic());
        $ticket->ht['helptopic']=$helptopic;
        $ticket->ht['issue'] = $helptopic[0];
        $ticket->ht['sub_issue'] = end($helptopic);
        $ticket->ht['count'] = 1;
        $output[] = $ticket->ht;
        $this->response(201, json_encode($output));
    }

    /* function to get ticket detail list(multiple tickets) */
    /* author:Navneet */

    function getTickets($format) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $parms = $this->getRequest($format);

        $result = Ticket::getTicketList($parms);
        $count = $result['count'];
        $total_count = $result['total_count'];
        $result = Ticket::getTicketdetail($result['ticket_list']);

        foreach ($result as $v) {
            /*
             * future code
              $message_array = Thread::getAPIEntries('M', 'ASC', $v['ticket_id']);
              $array = $message_array[0]['body'];
              $v['ticket_detail'] = $array->body;
              $id = $message_array[0]['id'];
              $tentry = ThreadEntry::lookup($id, $v->ticket_id);
              $v['attachment_link'] = $tentry->getAttachmentsLinks();
             */
            $v['issue_type'] = $v['dept_name'];
            $v['count'] = $count;
            $v['total_count']=$total_count;
            
            $topic = Topic::lookup($v['topic_id']);
            $helptopic = array();
            if ($topic) {
                $helptopic = explode('/', $topic->getFullName());
            }
            $v['helptopic']=$helptopic;
            $v['issue'] = $helptopic[0];
            $v['sub_issue'] = end($helptopic);
            $output[] = $v;
        }
        /* ----------------------------- 
          old api code

          foreach ($result['ticket_list'] as $v) {

          $ticket = new Ticket($v);
          $ticket->ht['order_id'] = $ticket->getOrderId();

          $message_array = $ticket->getMessages();
          //$this->response(201, json_encode($message_array));
          $array = $message_array[0]['body'];
          $ticket->ht['ticket_detail'] = $array->body;

          $id = $message_array[0]['id'];
          $tentry = $ticket->getThreadEntry($id);
          $ticket->ht['attachment_link'] = $tentry->getAttachmentsLinks();

          $ticket->ht['staff'] = $ticket->getAssignee()->name;
          $ticket->ht['status'] = $ticket->getStatus()->ht['name'];
          $ticket->ht['issue_type'] = $ticket->getDeptName();
          $ticket->ht['sla'] = $ticket->getSLA()->ht['grace_period'];
          $helptopic = explode('/', $ticket->getHelpTopic());
          $ticket->ht['issue'] = $helptopic[0];
          $ticket->ht['sub_issue'] = end($helptopic);
          $ticket->ht['count'] = $result['count'];
          $output[] = $ticket->ht;
          unset($ticket);
          }
          ---------------------------- */

        $this->response(201, json_encode($output));
    }

    function getThreads($format) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $parms = $this->getRequest($format);
        $ticket_id = Ticket::getIdByNumber($parms['ticket_number']);
        $ticket = new Ticket($ticket_id);
        $types = array('R', 'M', 'I', 'N', 'T', 'U');
        $thread = $ticket->getThreadEntries($types);
        $output = array();
        foreach ($thread as $th) {

            if ($th['thread_type'] && $th['attachments'] && ($tentry = $ticket->getThreadEntry($th['id'])) && ($urls = $tentry->getAttachmentUrls()) && ($links = $tentry->getAttachmentsLinks())) {
                $link = $tentry->getAttachmentsLinks();
            } else {
                $link = '';
            }
            $th['link'] = $link;
            $output[] = $th;
        }

        $this->response(201, json_encode($output));
    }
    function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    function getClosedTicketsInWhichMerchantHasNotReplied($format) {
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $params=$_GET;
        global $cfg;
        $mid=(int)$params['mid'];
        $startDate=$this->validateDate($params['startDate'])?$params['startDate']:"1970-01-01";
        $endDate=$this->validateDate($params['endDate'])?$params['endDate']:date('Y-m-d');
        $sql="select user_id from ost_user_account where merchant_id=?";
        global $__db;
        $user_ids=mysqli_prepared_query($__db,$sql,"i",array($mid));
        $user_id_list=[];
        foreach ($user_ids as $user_id){
            $user_id_list[]=$user_id['user_id'];
        }
        if(count($user_id_list)==0){
            $this->response(200,
                    json_encode(array('status'=>'success','data'=>array())));
        }
        $topic_ids= json_decode($cfg->config['merchant_topics_allowed']['value'], true);
        $inParamForTopic = implode(',', array_fill(0, count($topic_ids), '?'));
        $inParamForUser=implode(',', array_fill(0, count($user_id_list), '?'));
        $paramsList=array($topic_ids,$user_id_list,$startDate,$endDate);
        $finalParamList=[];
        foreach ($topic_ids as $topic_id){
            $finalParamList[]=$topic_id;
        }
        foreach ($user_id_list as $user_id){
            $finalParamList[]=$user_id;
        }
        $finalParamList[]=$startDate;
        $finalParamList[]=$endDate;
        $inParamType = implode('', array_fill(0, count($topic_ids)+count($user_id_list), 'i'));
        $inParamType.="ss";
        $sql="SELECT 
                   `topic`,
                count(1) as topicWiseCount
            FROM
                ost_ticket_new otn 
                    join ost_help_topic_new ohtn on 
                otn.topic_id=ohtn.topic_id 
                    LEFT JOIN
                ost_ticket_thread ott ON otn.ticket_id = ott.ticket_id
                    AND (ott.thread_type = 'M' or (ott.thread_type='I' 
                    and ott.poster='MCR panel'))  
                    and ott.source <> 'API' 
            WHERE
                otn.topic_id IN (" . $inParamForTopic . ")
                    AND otn.user_id IN (" . $inParamForUser . ")
                    AND otn.status_id = 14
                    AND DATE(otn.created) BETWEEN ? AND ?
                    AND ott.id IS NULL group by otn.topic_id ;";
        $data=[];
        $finalResult=
                mysqli_prepared_query($__db, $sql, $inParamType, $finalParamList);
        foreach ($finalResult as $row){
            $data['total']+=$row['topicWiseCount'];
            $data['topicwisecount'][$row['topic']]=$row['topicWiseCount'];
        }
       $this->response(200,json_encode(array('status'=>'success','data'=>$data)));
    }
    
      function postNote($format) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $errors = array();
        $parms = $this->getRequest($format);

        $ticket_id = Ticket::getIdByNumber($parms['ticket_number']);
        $vars['id'] = $ticket_id;
        $vars['internal_note'] = 0;
        $vars['ticket_sla'] = '';
        $vars['task_sla'] = '';
        $vars['t_dept'] = 0;
        $vars['title'] = $parms['title'];
        $vars['note'] = $parms['note'];
        if (trim($vars['title']) == '') {

            return $this->exerr(500, json_encode(__("Unable to post internal note:Please Enter Title")));
        }
        $ticket = new Ticket($vars['id']);

        if ($ticket && is_object($ticket)) {
            //everything right
        } else {
            return $this->exerr(500, json_encode(__("Unable to post internal note: unknown error")));
        }
        if ($ticket->postNote($vars, $errors, 'MCR panel', true, true)) {
            $this->response(201, $ticket->getId());
        } else {
            return $this->exerr(500, __("Unable to post internal note: unknown error"));
        }
    }
      function postMessage($format) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $errors = array();
        $parms = $this->getRequest($format);
        $ticket_id = Ticket::getIdByNumber($parms['ticket_number']);
        if (trim($parms['message']) == '') {

            return $this->exerr(500, json_encode(__("Unable to post message:Please Enter Message")));
            
        }
        $ticket = new Ticket($ticket_id);
        $vars['userId']=$ticket->getUserId();
        $vars['message']=$parms['message'];
        $userObj=  User::lookup($vars['userId']);
        $vars['poster']=$userObj->getName();
        $vars['file']=$parms['file'];
        
        if($message=$ticket->postMessage($vars, 'API')){
        //attachment 
            
        if ($cdn_upload_count=count($vars['file']['name'])) {
            for ($i = 0; $i < $cdn_upload_count; $i++) {
                $file_id = CustomAttachments::apiFileEntry($vars['file']['name'][$i], $vars['file']['type'][$i], $vars['file']['size'][$i], $vars['file']['key'][$i]);
                $thread_id = $message->getId();
                if ($file_id) {
                    CustomAttachments::AttachmentEntry($file_id, $ticket_id, $thread_id, 0);
                }
            }
        }
        
        $this->response(201, __("Message posted successfuly"));
        } else {
            return $this->exerr(500, __("Unable to post message: unknown error"));
        }
    }

    function getTopics($format) {
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $errors = array();
        $parms = $this->getRequest($format);
        $topics = Topic::ChildhelpTopics(true, $parms['parent_id'],'C');
        $this->response(201, json_encode($topics));
    }

    function getSubject($format) {
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $errors = array();
        $parms = $this->getRequest($format);
        $topic = Topic::lookup($parms['issue_id']);
        $subject = $topic->getFullName();
        $type= $topic->getType();
        $this->response(201, json_encode(array($subject,$type)));
    }
    
    function getMandatoryInfo($format){
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $parms = $this->getRequest($format);
        $topic = Topic::lookup($parms['topicId']);
        $info = $topic->getMandatoryInfo();
        $this->response(201, json_encode($info));
    }

    /* private helper functions */

    function createTicket($data) {

        # Pull off some meta-data
        $alert = (bool) (isset($data['alert']) ? $data['alert'] : true);
        $autorespond = (bool) (isset($data['autorespond']) ? $data['autorespond'] : true);

        # Assign default value to source if not defined, or defined as NULL
        $data['source'] = isset($data['source']) ? $data['source'] : 'API';

        # Create the ticket with the data (attempt to anyway)
        $errors = array();

        $ticket = Ticket::create($data, $errors, $data['source'], $autorespond, $alert);
        # Return errors (?)
        if (count($errors)) {
            if (isset($errors['errno']) && $errors['errno'] == 403)
                return $this->exerr(403, __('Ticket denied'));
            else
                return $this->exerr(
                                400, __("Unable to create new ticket: validation errors") . ":\n"
                                . Format::array_implode(": ", "\n", $errors)
                );
        } elseif (!$ticket) {
            return $this->exerr(500, __("Unable to create new ticket: unknown error"));
        }

        return $ticket;
    }

    function processEmail($data = false) {

        if (!$data)
            $data = $this->getEmailRequest();

        if (($thread = ThreadEntry::lookupByEmailHeaders($data)) && ($t = $thread->getTicket()) && ($data['staffId'] || !$t->isClosed() || $t->isReopenable()) && $thread->postEmail($data)) {
            return $thread->getTicket();
        }
        return $this->createTicket($data);
    }

}

//Local email piping controller - no API key required!
class PipeApiController extends TicketApiController {

    //Overwrite grandparent's (ApiController) response method.
    function response($code, $resp) {

        //Use postfix exit codes - instead of HTTP
        switch ($code) {
            case 201: //Success
                $exitcode = 0;
                break;
            case 400:
                $exitcode = 66;
                break;
            case 401: /* permission denied */
            case 403:
                $exitcode = 77;
                break;
            case 415:
            case 416:
            case 417:
            case 501:
                $exitcode = 65;
                break;
            case 503:
                $exitcode = 69;
                break;
            case 500: //Server error.
            default: //Temp (unknown) failure - retry
                $exitcode = 75;
        }

        //echo "$code ($exitcode):$resp";
        //We're simply exiting - MTA will take care of the rest based on exit code!
        exit($exitcode);
    }

    function process() {
        $pipe = new PipeApiController();
        if (($ticket = $pipe->processEmail()))
            return $pipe->response(201, $ticket->getNumber());

        return $pipe->exerr(416, __('Request failed - retry again!'));
    }

}

?>
