<?php
if (!defined('OSTSCPINC') || !$thisstaff || !@$thisstaff->isStaff())
    die('Access Denied');

$qs = array(); //Query string collector
if ($_REQUEST['status']) { //Query string status has nothing to do with the real status used below; gets overloaded.
    $qs += array('status' => $_REQUEST['status']);
}

//See if this is a search
$search = ($_REQUEST['a'] == 'search');
$searchTerm = '';
//make sure the search query is 3 chars min...defaults to no query with warning message
if ($search) {
    $searchTerm = $_REQUEST['query'];
    if (($_REQUEST['query'] && strlen($_REQUEST['query']) < 1) || (!$_REQUEST['query'] && isset($_REQUEST['basic_search']))) { //Why do I care about this crap...
        $search = false; //Instead of an error page...default back to regular query..with no search.
        $errors['err'] = __('Search term must be more than 1 chars');
        $searchTerm = '';
    }
}
$showoverdue = $showanswered = false;
$staffId = 0; //Nothing for now...TODO: Allow admin and manager to limit tickets to single staff level.
$showassigned = true; //show Assigned To column - defaults to true
//Get status we are actually going to use on the query...making sure it is clean!
$status = null;
switch (strtolower($_REQUEST['status'])) { //Status is overloaded
    case 'open':
        $status = 'open';
        $results_type = __('Open Tickets');
        break;
    case 'closed':
        $status = 'closed';
        $results_type = __('Closed Tickets');
        $showassigned = true; //closed by.
        break;
    case 'overdue':
        $status = 'open';
        $showoverdue = true;
        $results_type = __('Overdue Tickets');
        break;
    case 'assigned':
        $status = 'open';
        $staffId = $thisstaff->getId();
        $results_type = __('My Tickets');
        break;
    case 'answered':
        $status = 'open';
        $showanswered = true;
        $results_type = __('Answered Tickets');
        break;
    case 'reachingosla':
        $status = 'open';
        $showROSLA = true;
        $results_type = __('Reaching Osla Tickets');
        break;
    case 'solved':
        $status = 'open';
        $showsolved = true;
        $results_type = __('Solved Tickets');
        break;
    case 'inprogress':
        $status = 'open';
        $showInprogress = true;
        $results_type = __('Inprogress Tickets');
        break;
    case 'reopen':
        $status = 'open';
        $showReopen = true;
        $results_type = __('Reopen Tickets');
        break;
    default:
        if (!$search && !isset($_REQUEST['advsid'])) {
            $_REQUEST['status'] = $status = 'open';
            $results_type = __('Open Tickets');
        }
}

$qwhere = '';
/*
  STRICT DEPARTMENTS BASED PERMISSION!
  User can also see tickets assigned to them regardless of the ticket's dept.
 */
//changes by navneet

$depts = $thisstaff->getDepts();
$qwhere.=' WHERE ( ';
if (!$_REQUEST['deptId']) {
    // $reassign_part = isset($_REQUEST['reassign']) ? ' AND ticket.isreassign=' . $_REQUEST['reassign'] : '';
    $staff_id_part = isset($_REQUEST['staffId']) && $_REQUEST['staffId'] == 0 ? $_REQUEST['staffId'] : db_input($thisstaff->getId());
    $qwhere.='  ( ticket.staff_id=' . $staff_id_part
            . ' ) ';
} else {
    $qwhere.='1=2 ';
}
//if (!$thisstaff->showAssignedOnly()) {
if ($_REQUEST['deptId']) {
    $arr = explode(',', $_REQUEST['deptId']);

    $qwhere.='OR ticket.dept_id IN (' . implode(',', db_input($arr)) . ')';
    if (isset($_REQUEST['reassign']) && $_REQUEST['reassign'] == 0) {
        $qwhere.= ' AND ticket.isreassign=' . $_REQUEST['reassign'] . " AND ticket.staff_id=0 AND ticket.unassigned=1";
    }
    if (isset($_REQUEST['reassign']) && $_REQUEST['reassign'] == 1) {
        $qwhere.= ' AND ticket.isreassign=' . $_REQUEST['reassign'] . " AND ticket.staff_id=0 ";
    }
    if ($_REQUEST['deptId'] == MTS_ID && !isset($_REQUEST['reassign'])) {
        $qwhere.=' AND ticket.unassigned=0 ';
    }
     if ($_REQUEST['ticket_source']=='Email'){
         $qwhere.=" AND ticket.source='Email' AND ticket.is_email=0 ";
     }

    if ($thisstaff->getDeptId() != MRG_ID) {
        $qwhere.= ' AND ticket.last_response=0 ';
    }

} else if (isset($_REQUEST['staffId'])) {
    $qwhere.='';

    if ($thisstaff->getDeptId() != MRG_ID) {
        $qwhere.= ' AND ticket.last_response=0 ';
    }
} else {
    $qwhere.='OR ticket.dept_id IN (' . ($depts ? implode(',', db_input($depts)) : 0) . ')';
    
}

//}
if (!$_REQUEST['deptId'] && !isset($_REQUEST['staffId'])) {
    // if (($teams = $thisstaff->getTeams()) && count(array_filter($teams)))
    //  $qwhere.=' OR (ticket.team_id IN (' . implode(',', db_input(array_filter($teams)))
    //        . ') AND status.state="open") ';
}
$qwhere .= ' )';

//STATUS to states
$states = array(
    'open' => array('open'),
    'closed' => array('closed'));

if ($status && isset($states[$status])) {
    $qwhere.=' AND status.state IN (
                ' . implode(',', db_input($states[$status])) . ' ) ';
}

if (isset($_REQUEST['uid']) && $_REQUEST['uid']) {
    $qwhere .= ' AND (ticket.user_id=' . db_input($_REQUEST['uid'])
            . ' OR collab.user_id=' . db_input($_REQUEST['uid']) . ') ';
    $qs += array('uid' => $_REQUEST['uid']);
}

//Queues: Overloaded sub-statuses  - you've got to just have faith!
if ($staffId && ($staffId == $thisstaff->getId())) { //My tickets
    $results_type = __('Assigned Tickets');
    $qwhere.=' AND ticket.staff_id=' . db_input($staffId);
    $showassigned = false; //My tickets...already assigned to the staff.
} elseif ($showoverdue) { //overdue
    $qwhere.=' AND ticket.isoverdue=1 ';
} elseif ($showanswered) { ////Answered
    $qwhere.="AND ticket.isanswered=1  or status.name='In Progress' or status.name='Solved'";
} elseif ($showROSLA) { //reaching osla
    $qwhere.=' AND ticket.is_rosla=1 ';
} elseif ($showsolved) {
    $qwhere.=" AND status.name='Solved'"; //solved
} elseif ($showInprogress) {
    $qwhere.=" AND status.name='In Progress'";
} elseif ($showReopen) {
    $qwhere.=" AND ticket.is_reopen=1";
} elseif (!strcasecmp($status, 'open') && !$search) { //Open queue (on search OPEN means all open tickets - regardless of state).
    //Showing answered tickets on open queue??
    if (!$cfg->showAnsweredTickets())
        $qwhere.=' AND ticket.isanswered=0 ';

    /* Showing assigned tickets on open queue?
      Don't confuse it with show assigned To column -> F'it it's confusing - just trust me!
     */
    if (!($cfg->showAssignedTickets() || $thisstaff->showAssignedTickets())) {
        $qwhere.=" AND status.name='Open'"; //XXX: NOT factoring in team assignments - only staff assignments.
        $showassigned = false; //Not showing Assigned To column since assigned tickets are not part of open queue
    }    
}
//Search?? Somebody...get me some coffee
$deep_search = false;
$order_by = $order = null;
if ($search):
    $qs += array('a' => $_REQUEST['a'], 't' => $_REQUEST['t']);
    if ($searchTerm) {
        $qs += array('query' => $searchTerm);
        $queryterm = db_real_escape($searchTerm, false);
        if (is_numeric($searchTerm) && $_REQUEST['SearchBy'] == 'ticket') {
            $queryterm = (int) $queryterm;
            $qwhere.=" AND ticket.`number` = '$queryterm'";
        } elseif ($_REQUEST['SearchBy'] == 'email' && strpos($searchTerm, '@') && Validator::is_email($searchTerm)) {
            $qwhere.=" AND email.address='$queryterm'";
        } else {
            require_once(INCLUDE_DIR . 'ajax.tickets.php');

            if ($_REQUEST['new_search']) {
                $searchType = $_REQUEST['SearchBy'];
                $searchParam = $_REQUEST['query'];
                $tickets = Ticket::search($searchType, $searchParam);
                $qs += array('SearchBy' => $searchType);
                $qs += array('new_search' => 'new');
            } else
                $tickets = TicketsAjaxApi::_search(array('query' => $queryterm));
            if (count($tickets)) {
                $ticket_ids = implode(',', db_input($tickets));
                $qwhere .= ' AND ticket.ticket_id IN (' . $ticket_ids . ')';
                $order_by = 'FIELD(ticket.ticket_id, ' . $ticket_ids . ')';
                $order = ' ';
            } else
            // No hits -- there should be an empty list of results
                $qwhere .= ' AND false';
        }
    }

endif;

if ($_REQUEST['advsid'] && !empty($_SESSION['adv_' . $_REQUEST['advsid']])) {
    $result = json_decode($_SESSION['adv_' . $_REQUEST['advsid']]);

    if (json_last_error() === 0) {

        $ticket_ids = implode(',', db_input($result));
    } else {
        $ticket_ids = implode(',', db_input($_SESSION['adv_' . $_REQUEST['advsid']]));
    }

    $qs += array('advsid' => $_REQUEST['advsid']);
    if (count($_SESSION['adv_' . $_REQUEST['advsid']])) {
        $qwhere .= ' AND ticket.ticket_id IN (' . $ticket_ids . ')';
        // Thanks, http://stackoverflow.com/a/1631794
        // $order_by = 'FIELD(ticket.ticket_id, ' . $ticket_ids . ')';
        //$order = ' ';
    }
} elseif ($_REQUEST['advsid']) {
    $qwhere .= ' AND ticket.ticket_id IN (0)';
}
if ($_REQUEST['last_dept_id']) {
    $qwhere.=' AND ticket.last_id = ' . $_REQUEST['last_dept_id'] . '';
}

$sortOptions = array('date' => 'effective_date', 'ID' => 'ticket.`number`*1',
    'pri' => 'pri.priority_urgency', 'name' => 'user.name', 'subj' => 'cdata.subject',
    'status' => 'status.name', 'assignee' => 'assigned', 'staff' => 'staff',
    'dept' => 'dept.dept_name');

$orderWays = array('DESC' => 'DESC', 'ASC' => 'ASC');

//Sorting options...
$queue = isset($_REQUEST['status']) ? strtolower($_REQUEST['status']) : $status;
if ($_REQUEST['sort'] && $sortOptions[$_REQUEST['sort']])
    $order_by = $sortOptions[$_REQUEST['sort']];
elseif ($sortOptions[$_SESSION[$queue . '_tickets']['sort']]) {
    $_REQUEST['sort'] = $_SESSION[$queue . '_tickets']['sort'];
    $_REQUEST['order'] = $_SESSION[$queue . '_tickets']['order'];

    $order_by = $sortOptions[$_SESSION[$queue . '_tickets']['sort']];
    $order = $_SESSION[$queue . '_tickets']['order'];
}

if ($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])])
    $order = $orderWays[strtoupper($_REQUEST['order'])];

//Save sort order for sticky sorting.
if ($_REQUEST['sort'] && $queue) {
    $_SESSION[$queue . '_tickets']['sort'] = $_REQUEST['sort'];
    $_SESSION[$queue . '_tickets']['order'] = $_REQUEST['order'];
}

//Set default sort by columns.
if (!$order_by) {
    if ($showanswered)
        $order_by = 'ticket.lastresponse, ticket.created'; //No priority sorting for answered tickets.
    elseif (!strcasecmp($status, 'closed'))
        $order_by = 'ticket.closed, ticket.created'; //No priority sorting for closed tickets.
    elseif ($showoverdue) //priority> duedate > age in ASC order.
        $order_by = 'pri.priority_urgency ASC, ISNULL(ticket.duedate) ASC, ticket.duedate ASC, effective_date ASC, ticket.created';
    else //XXX: Add due date here?? No -
        $order_by = 'pri.priority_urgency ASC, effective_date DESC, ticket.created';
}

$order = $order ? $order : 'DESC';
if ($order_by && strpos($order_by, ',') && $order)
    $order_by = preg_replace('/(?<!ASC|DESC),/', " $order,", $order_by);

$sort = $_REQUEST['sort'] ? strtolower($_REQUEST['sort']) : 'pri.priority_urgency'; //Urgency is not on display table.
$x = $sort . '_sort';
$$x = ' class="' . strtolower($order) . '" ';

if ($_GET['limit'])
    $qs += array('limit' => $_GET['limit']);

$qselect = 'SELECT distinct(ticket.ticket_id),ticket.topic_id,tlock.lock_id,ticket.`number`,ticket.dept_id,ticket.staff_id,ticket.team_id '

        . ' ,user.name'    
        . ' ,merchant_id as mid '
        . ' ,last_response as lastresponse '
        . ' ,max(tevent.timestamp) as transfertime, tevent.state as tstate '
        . ' ,email.address as email, dept.dept_name, status.state '
        . ' ,status.name as status,ticket.source,ticket.isoverdue,ticket.isanswered,ticket.created,dept_n.dept_name as last_dept,ticket.last_id ';

$qfrom = ' FROM ' . TICKET_TABLE . ' ticket ' .
        ' LEFT JOIN ' . TICKET_STATUS_TABLE . ' status
            ON (status.id = ticket.status_id) ' .
        ' LEFT JOIN ' . USER_TABLE . ' user ON user.id = ticket.user_id' .
        ' LEFT JOIN ' . TICKET_EVENT_TABLE . ' tevent 
                ON (tevent.ticket_id = ticket.ticket_id AND tevent.state="Transferred")' . 
        ' LEFT JOIN ' . USER_ACCOUNT_TABLE . ' acc ON user.id = acc.user_id' .        
        ' LEFT JOIN ' . USER_EMAIL_TABLE . ' email ON user.id = email.user_id' .
        ' LEFT JOIN ' . DEPT_TABLE . ' dept ON ticket.dept_id=dept.dept_id ' .
        ' LEFT JOIN ' . DEPT_TABLE . ' dept_n ON ticket.last_id=dept_n.dept_id ';

if ($_REQUEST['uid'])
    $qfrom.=' LEFT JOIN ' . TICKET_COLLABORATOR_TABLE . ' collab
        ON (ticket.ticket_id = collab.ticket_id )';


$sjoin = '';
if ($search && $deep_search) {
    $sjoin.=' LEFT JOIN ' . TICKET_THREAD_TABLE . ' thread ON (ticket.ticket_id=thread.ticket_id )';
}
if ($_REQUEST['a'] == 'search') {

    //$sjoin.= ' LEFT JOIN ' . USER_ACCOUNT_TABLE . ' acc ON (acc.user_id = ticket.user_id) ';
}
//get ticket count based on the query so far..
$total = db_count("SELECT count(DISTINCT ticket.ticket_id) $qfrom $sjoin $qwhere");


//pagenate
$pagelimit = ($_GET['limit'] && is_numeric($_GET['limit'])) ? $_GET['limit'] : PAGE_LIMIT;
$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$pageNav = new Pagenate($total, $page, $pagelimit);
//print_r($pageNav);
$qstr = '&amp;' . http::build_query($qs);
$qs += array('sort' => $_REQUEST['sort'], 'order' => $_REQUEST['order']);
$pageNav->setURL('tickets.php', $qs);
//navneet
$join_part = '';
if ($_REQUEST['a'] == 'search') {
    // $join_part = ' LEFT JOIN ' . USER_ACCOUNT_TABLE . ' acc ON (acc.user_id = ticket.user_id) ';
}
//end here
//ADD attachment,priorities, lock and other crap
$qselect.=' ,IF(ticket.duedate IS NULL,IF(sla.id IS NULL, NULL, DATE_ADD(ticket.created, INTERVAL sla.grace_period HOUR)), ticket.duedate) as duedate '
        . ' ,CAST(GREATEST(IFNULL(ticket.lastmessage, 0), IFNULL(ticket.closed, 0), IFNULL(ticket.reopened, 0), ticket.created) as datetime) as effective_date '
        . ' ,ticket.created as ticket_created, CONCAT_WS(" ", staff.firstname, staff.lastname) as staff, team.name as team '
        . ' ,IF(staff.staff_id IS NULL,team.name,CONCAT_WS(" ", staff.lastname, staff.firstname)) as assigned '

        
        . ' ,IF(ptopic.topic_pid IS NULL, topic.topic, CONCAT_WS(" / ", ptopic.topic, topic.topic)) as helptopic '
        . ' ,cdata.priority as priority_id, cdata.subject,cdata.carrier_name, pri.priority_desc, pri.priority_color';

$qfrom.=' LEFT JOIN ' . TICKET_LOCK_TABLE . ' tlock ON (ticket.ticket_id=tlock.ticket_id AND tlock.expire>NOW()
               AND tlock.staff_id!=' . db_input($thisstaff->getId()) . ') '
        . ' LEFT JOIN ' . STAFF_TABLE . ' staff ON (ticket.staff_id=staff.staff_id) '
        . ' LEFT JOIN ' . TEAM_TABLE . ' team ON (ticket.team_id=team.team_id) '
        . ' LEFT JOIN ' . SLA_TABLE . ' sla ON (ticket.sla_id=sla.id AND sla.isactive=1) '
        . ' LEFT JOIN ' . TOPIC_TABLE . ' topic ON (ticket.topic_id=topic.topic_id) '
        
        . ' LEFT JOIN ' . TOPIC_TABLE . ' ptopic ON (ptopic.topic_id=topic.topic_pid) '
        . ' LEFT JOIN ' . TABLE_PREFIX . 'ticket__cdata cdata ON (cdata.ticket_id = ticket.ticket_id) '
        . ' LEFT JOIN ' . PRIORITY_TABLE . ' pri ON (pri.priority_id = cdata.priority)' . $join_part;

TicketForm::ensureDynamicDataView();


 $query = "$qselect $qfrom $qwhere GROUP BY ticket.ticket_id ORDER BY $order_by $order LIMIT " . $pageNav->getStart() . "," . $pageNav->getLimit();
$_SESSION['temp_export_query'] = preg_replace('|AND ticket\.ticket_id IN (.*) GROUP BY |',' CHANGEMEHERE GROUP BY ',$query);

$hash = md5($query);
$_SESSION['search_' . $hash] = $query;
$res = db_query($query);
$showing = db_num_rows($res) ? ' &mdash; ' . $pageNav->showing() : "";
if (!$results_type)
    $results_type = sprintf(__('%s Tickets' /* %s will be a status such as 'open' */), mb_convert_case($status, MB_CASE_TITLE));

if ($search)
    $results_type.= ' (' . __('Search Results') . ')';


$negorder = $order == 'DESC' ? 'ASC' : 'DESC'; //Negate the sorting..
// Fetch the results
$results = array();
while ($row = db_fetch_array($res)) {
    $results[$row['ticket_id']] = $row;
}
if(count($results) == 1){
    redirect($_SERVER['SCRIPT_NAME'].'?id='.key($results));
}
function redirect($url)
{
    $string = '<script type="text/javascript">';
    $string .= 'window.location = "' . $url . '"';
    $string .= '</script>';
    echo $string;
}
// Fetch attachment and thread entry counts
if ($results) {
    $counts_sql = 'SELECT ticket.ticket_id,
        count(DISTINCT attach.attach_id) as attachments,
        count(DISTINCT thread.id) as thread_count,
        count(DISTINCT collab.id) as collaborators
        FROM ' . TICKET_TABLE . ' ticket
        LEFT JOIN ' . TICKET_ATTACHMENT_TABLE . ' attach ON (ticket.ticket_id=attach.ticket_id) '
            . ' LEFT JOIN ' . TICKET_THREAD_TABLE . ' thread ON ( ticket.ticket_id=thread.ticket_id) '
            . ' LEFT JOIN ' . TICKET_COLLABORATOR_TABLE . ' collab
            ON ( ticket.ticket_id=collab.ticket_id) '
            . ' WHERE ticket.ticket_id IN (' . implode(',', db_input(array_keys($results))) . ')
        GROUP BY ticket.ticket_id';
    $ids_res = db_query($counts_sql);
    while ($row = db_fetch_array($ids_res)) {
        $results[$row['ticket_id']] += $row;
    }
}

//YOU BREAK IT YOU FIX IT.
?>
<?php
if (isset($_SESSION['bulk_status'])) {
    $msg = ($_SESSION['bulk_status'] == 1) ? "Bulk assignment completed successfully" : "Bulk assignment failed";
    echo $msg;
    unset($_SESSION['bulk_status']);
}
if (isset($_SESSION['bulk_status2'])) {
    $msg = ($_SESSION['bulk_status2'] == 1) ? "Bulk assignment completed successfully" : "Bulk assignment failed";
    echo $msg;
    unset($_SESSION['bulk_status2']);
}
?>
<!-- search_bar here_move -->
<?php
//$sid = $thisstaff->getId();
//$query = "select D.dept_id,D.dept_name from " . STAFF_TABLE . " S inner join " . DEPT_TABLE . " D on S.dept_id=D.dept_id where S.staff_id=$sid";
//$res = db_query($query);
//$dept_array = db_fetch_array($res);
//$dept_id = $dept_array['dept_id'];
$dept_id = $thisstaff->getDeptId();
?>
<div class="clear"></div>
<div>
    <div class="heading_action_section grid-row padding-container">
        <div class="pull-left flush-left float_left">
            <h2><a href="<?php echo Format::htmlchars($_SERVER['REQUEST_URI']); ?>"
                   title="<?php echo __('Refresh'); ?>"><i class="refresh"></i> <?php
                       echo
                       $results_type . $showing;
                       ?></a></h2>
        </div>
        <?php if (in_array($dept_id, json_decode(DEPT_PERMISSION))) { ?>
            <!--      <div class="pull-right flush-right actions_right float_right">
        
            <?php if ($thisstaff->canDeleteTickets()) { ?>
                                                                                            <a id="tickets-delete" class=" pull-right tickets-action delete_btn"
                                                                                               href="#tickets/status/delete"> <i
                                                                                                    class="icon-trash"</i>  <?php // echo __('Delete');                ?></a>
            <?php }
            ?>
            <?php
            if ($thisstaff->canManageTickets()) {
                // echo TicketStatus::status_options();
            }
            ?>
                    </div> -->
        <?php } ?>
    </div>


    <div class="table_with_links container_section grid-row margin-container">
        <div class="left_tickets_link grid2 float_left">
            <?php
            $dept_name = $dept_array['dept_name'];
            $count_array = $obj->ticketCount($thisstaff);
            $triage_count = Triage::getTriageCount();
            $non_triage = $re_triage = $triage = 0;
            if ($triage_count) {
                foreach ($triage_count as $tr_count) {
                    if ($tr_count['status'] == 'N') {
                        $non_triage += $tr_count['count'];
                    } else if ($tr_count['status'] == 'R') {
                        $re_triage += $tr_count['count'];
                    } else if ($tr_count['status'] == 'T') {
                        $triage += $tr_count['count'];
                    }
                }
            }
            ?>

            <ul>
                <li id="M"><a href="tickets.php?a=search&query=&statusId=<?php echo OPEN_ID; ?>&deptId=&flag=&assignee=s<?php echo $thisstaff->getId(); ?>&topicId=&staffId=0&startDate=&endDate=&div_id=M">My Tasks <span><?php echo $my_count; ?></span></a></li>
                <?php if (CLOSED_LOOPING && $dept_id == MRG_ID) { ?><li id="UT"><a href="tickets.php?a=search&query=&deptId=&flag=&taskAssignee=s0&topicId=&staffId=&startDate=&endDate=&div_id=UT">Unassigned Tickets <span><?php echo Task::unassignedCount(0) ?></span></a></li><?php } ?>
                <?php
                $key = $dept_id;
                $childDept = $obj->getChildDepartments(array("parent" => $key));
                $key_array = array_keys($childDept);
                $temp = implode(", ", array_keys($childDept));
                $key.= (!empty($temp)) ? ',' . $temp : '';
                ?>

                <li id="UTI"><a href="tickets.php?a=search&query=&deptId=<?php echo $key; ?>&flag&assignee=s0=&topicId=&staffId=0&startDate=&endDate=&div_id=UTI">Unassigned Tasks <span><?php echo Ticket::unassignedCount($dept_id) ?></span></a></li>
                <?php if (in_array($dept_id, json_decode(DEPT_PERMISSION))) { ?>                
                    <li id="U"><a href="tickets.php?a=search&ticket_source=Email&query=&statusId=<?php echo OPEN_ID; ?>&deptId=<?php echo MRG_ID; ?>&flag=&assignee=s0&topicId=&staffId=0&startDate=&endDate=&reassign=0&div_id=U">Email Tickets <span> <?php echo $unassign_email_count; ?></span></a></li>
                <?php } ?>                

<!--                <li id="R"><a href="tickets.php?a=search&query=&statusId=<?php echo OPEN_ID; ?>&deptId=<?php echo $dept_id ?>&flag=&assignee=s0&topicId=&staffId=0&startDate=&endDate=&reassign=1&div_id=R">Reassinged Tasks <span> <?php echo $reassign_count; ?></span></a></li>-->

                <?php if (CLOSED_LOOPING) { ?>
                    <?php
                    if ($dept_id == MRG_ID) {
                        $timer = json_decode(TASK_TIMER);
                        $timer = (array) $timer;
                        $timer_count = Task::task_timer_count($thisstaff->getId());
                        ?>
                        <li class="dropdown_nav"><h6>Ticket Respond</h6>
                            <i class="arrow_right"></i>
                            <ul>
                                <?php
                                foreach ($timer as $key => $val) {
                                    $count = Task::task_priority_count($key);
                                    ?>
                                    <li id="<?php echo 't_' . $key; ?>" ><a href="tickets.php?a=search&query=&task_timer=<?php echo $key; ?>"><?php echo $key; ?> </a><span><?php echo $timer_count[$key] ?></span>
                                        <ul class="left_sub_menu">
                                            <?php
                                            $data = Priority::getPriorities();
                                            foreach ($data as $k => $v) {
                                                ?>
                                                <li><a href="tickets.php?a=search&query=&task_timer=<?php echo $key; ?>&task_priority=<?php echo $k; ?>"><?php echo $v; ?><span><?php echo $count[$v]; ?></span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>

                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>



                    <?php
                    $timer = json_decode(TICKET_TIMER);
                    $timer = (array) $timer;
                    $timer_count = Task::ticket_timer_count($thisstaff->getId());
                    ?>

                    <li class="dropdown_nav"><h6>Task Respond</h6> 
                        <i class="arrow_right"></i>
                        <ul>
                            <?php
                            foreach ($timer as $key => $val) {
                                $count = Task::ticket_priority_count($thisstaff->getID(), $key);
                                ?>
                                <li id="<?php echo $key; ?>"><a href="tickets.php?a=search&query=&ticket_timer=<?php echo $key; ?>"><?php echo $key; ?> </a><span><?php echo $timer_count[$key] ?></span>
                                    <ul class="left_sub_menu">
                                        <?php
                                        $data = Priority::getPriorities();
                                        foreach ($data as $k => $v) {
                                            ?>
                                            <li><a href="tickets.php?a=search&query=&ticket_timer=<?php echo $key; ?>&priority=<?php echo $k; ?>"><?php echo $v; ?><span><?php echo $count[$v]; ?></span></a></li>
                                        <?php } ?>
                                    </ul>

                                </li>

                            <?php } ?>
                        </ul>
                    </li>


                    <?php //} else {  ?>
                                    <!--  <li id=<?php echo $k; ?>><a href="tickets.php?a=search&query=&statusId=<?php echo OPEN_ID; ?>&deptId=<?php echo $dept_id; ?>&flag=&assignee=&topicId=&staffId=0&startDate=&endDate=&div_id=<?php echo $k; ?>"> <?php echo $dept_name . '<span>' . ( $count_array[$dept_id] ? $count_array[$dept_id] : 0) . '<span>'; ?></a></li>-->
                    <?php //}    ?>
                    <?php if ($dept_id == MRG_ID) { ?>
                        <li class="dropdown_nav"><h6>Triage status</h6>
                            <i class="arrow_right"></i>
                            <ul>
                                <li id="N">

                                    <a href="tickets.php?a=search&query=&triageId=1&label=N">
                                        Not Triage
                                        <span><?php echo $non_triage; ?></span>
                                    </a>
                                </li>
                                <li id="T">

                                    <a href="tickets.php?a=search&query=&triageId=1&label=T">
                                        Triage
                                        <span><?php echo $triage; ?></span>
                                    </a>
                                </li>
                                <li id="R">

                                    <a href="tickets.php?a=search&query=&triageId=1&label=R">
                                        Triage Again
                                        <span><?php echo $re_triage; ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li class="dropdown_nav"><h6>Departments</h6>
                    <i class="arrow_right"></i>
                    <ul>
                        <li>
                            <?php //if (in_array($dept_id, json_decode(DEPT_PERMISSION))) { ?>
                            <?php
                            $depts = $obj->getDepartments(array("parent" => true));

                            foreach ($depts as $key => $value) {

                                if ($key != $dept_id && !(in_array($dept_id, json_decode(DEPT_PERMISSION)))) {
                                    continue;
                                }
                                $childDept = $obj->getChildDepartments(array("parent" => $key));
                                $count = $count_array[$key];
                                $key_array = array_keys($childDept);
                                foreach ($key_array as $c) {
                                    $count+=$count_array[$c];
                                }
                                $temp = implode(", ", array_keys($childDept));
                                $k = $key;
                                $key.= (!empty($temp)) ? ',' . $temp : '';
                                ?>
                            <li id=<?php echo $k; ?> >
                                <?php if (!empty($childDept)) { ?>
                                    <i class="arrow_right"></i>
                                <?php } ?>
                                <a href="tickets.php?a=search&query=&statusId=<?php echo OPEN_ID; ?>&deptId=<?php echo $key; ?>&flag=&assignee=&topicId=&staffId=0&startDate=&endDate=&div_id=<?php echo $k; ?>"> <?php echo $value . '<span>' . (($count) ? $count : 0) . '</span>'; ?></a>
                                <?php if (!empty($childDept)) { ?>
                                    <ul class="left_sub_menu">
                                        <?php foreach ($childDept as $childkey => $childvalue) { ?>
                                            <li id=<?php echo $childkey; ?> class = "child" ><a href="tickets.php?a=search&query=&statusId=<?php echo OPEN_ID; ?>&deptId=<?php echo $childkey; ?>&flag=&assignee=&topicId=&staffId=0&startDate=&endDate=&div_id=<?php echo $childkey; ?>"><?php echo $childvalue . '<span>' . (($count_array[$childkey]) ? $count_array[$childkey] : 0) . '</span>'; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                </li>
            </ul>
            </li>
            </ul>
        </div>


        <div class="pull-right grid10 table_section float_right">

            <form action="tickets.php" method="POST" name='tickets' id="tickets">
                <?php csrf_token(); ?>
                <input type="hidden" name="a" value="mass_process" >
                <input type="hidden" name="do" id="action" value="" >
                <input type="hidden" name="status" value="<?php
                echo
                Format::htmlchars($_REQUEST['status'], true);
                ?>" >
                <table class="list" border="0" cellspacing="1" cellpadding="2" width="100%">
                    <thead>
                        <tr>
                            <?php if ($thisstaff->canManageTickets()) { ?>
                                <th width="1%">
                                    <input id="all_check" class="ckb" type="checkbox">
                                    <label for="all_check"></label>
                                </th>
                            <?php } ?>
                            <th class="testAlign" width="15%">
                                <a <?php echo $id_sort; ?> href="tickets.php?sort=ID&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                           title="<?php echo sprintf(__('Sort by %s %s'), __('Ticket ID'), __($negorder)); ?>"><?php echo __('Ticket'); ?></a></th>
                            <th class="testAlign" width="10%">
                                <a  <?php echo $date_sort; ?> href="tickets.php?sort=date&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                              title="<?php echo sprintf(__('Sort by %s %s'), __('Date'), __($negorder)); ?>"><?php echo __('Date'); ?></a></th>
                            <th class="testAlign" width="15%">
                                <a <?php echo $subj_sort; ?> href="tickets.php?sort=subj&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                             title="<?php echo sprintf(__('Sort by %s %s'), __('Subject'), __($negorder)); ?>"><?php echo __('Subject'); ?></a></th>
                            <th class="testAlign" width="15%">
                                <a <?php echo $name_sort; ?> href="tickets.php?sort=name&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                             title="<?php echo sprintf(__('Sort by %s %s'), __('Name'), __($negorder)); ?>"><?php echo __('From'); ?></a></th>
                            <th class="testAlign" width="10%">
                                <a <?php echo $status_sort; ?> href="tickets.php?sort=status&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                               title="<?php echo sprintf(__('Sort by %s %s'), __('Status'), __($negorder)); ?>"><?php echo __('Status'); ?></a></th>
                            <th class="testAlign" width="10%">
                                <a <?php echo $status_sort; ?> href="tickets.php?sort=status&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                               title="<?php echo sprintf(__('Sort by %s %s'), __('Status'), __($negorder)); ?>"><?php echo __('Priority'); ?></a></th>
                            <?php
                            if ($showassigned) {
                                //Closed by
                                if (!strcasecmp($status, 'closed')) {
                                    ?>
                                    <th class="testAlign" width="10%">
                                        <a <?php echo $dept_sort; ?> href="tickets.php?sort=dept&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                                     title="<?php echo sprintf(__('Sort by %s %s'), __('Department'), __($negorder)); ?>"><?php echo __('Department'); ?></a></th>
                                    <th class="testAlign" width="10%">
                                        <a <?php echo $staff_sort; ?> href="tickets.php?sort=staff&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                                      title="<?php echo sprintf(__('Sort by %s %s'), __("Closing Agent's Name"), __($negorder)); ?>"><?php echo __('Closed By'); ?></a></th>
                                    <?php } else { //assigned to 
                                        ?>
                                    <th class="testAlign" width="10%">
                                        <a <?php echo $dept_sort; ?> href="tickets.php?sort=dept&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                                     title="<?php echo sprintf(__('Sort by %s %s'), __('Department'), __($negorder)); ?>"><?php echo __('Department'); ?></a></th>
                                    <th class="testAlign" width="10%">
                                        <a <?php echo $assignee_sort; ?> href="tickets.php?sort=assignee&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                                         title="<?php echo sprintf(__('Sort by %s %s'), __('Assignee'), __($negorder)); ?>"><?php echo __('Task Assigned To'); ?></a></th>
                                        <?php
                                    }
                                } else {
                                    ?>
                                <th class="testAlign" width="15%">
                                    <a <?php echo $dept_sort; ?> href="tickets.php?sort=dept&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                                 title="<?php echo sprintf(__('Sort by %s %s'), __('Department'), __($negorder)); ?>"><?php echo __('Department'); ?></a></th>
                                <?php }
                                ?>

                            <th class="testAlign" width="10%">
                                <a <?php echo $aging_sort; ?> href="tickets.php?sort=date&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                              title="<?php echo sprintf(__('Sort by %s %s'), __('Aging'), __($order)); ?>"><?php echo __('Aging'); ?></a></th>
                                <?php
                                // if ($thisstaff->getDeptId() == MRG_ID) {
                                //if (strcasecmp($status, 'closed')) {
                                ?>
                            <th class="testAlign" width="10%">
<!--                                    <a <?php echo $dept_sort; ?> href="tickets.php?sort=dept&order=<?php echo $negorder; ?><?php echo $qstr; ?>"
                                                         title="<?php echo sprintf(__('Sort by %s %s'), __('Department'), __($negorder)); ?>"><?php echo __('Last Updated By'); ?></a>-->
                                <!--                                     <form action="" method="GET">-->
                                <select id="deptId" style="width: 125px;"onchange="location = this.value;">
                                    <?php
                                    $_SERVER['REQUEST_URI'] = str_replace('index.php', 'tickets.php?', $_SERVER['REQUEST_URI']);
                                    $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                                    $last_len = strlen($url) - strrpos($url, 'last_dept_id') - 13;
                                    $last_url = substr($url, 0, -($last_len + 14));
                                    ?>

                                    <option value=<?php echo $last_url; ?> <?php echo (!$_REQUEST['last_dept_id']) ? 'selected=selected' : ''; ?>><?php echo __('Last Updated By'); ?> </option>
                                    <?php
                                    $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                                    $check_url = substr(substr($url, -4), 0, 3);
                                    if ($check_url == 'scp')
                                        $url.='tickets.php?';

                                    if ($_REQUEST['last_dept_id']) {
                                        $last_len = strlen($url) - strrpos($url, 'last_dept_id') - 13;
                                        $last_url = substr($url, 0, -$last_len);
                                        echo sprintf('<option value="%s" %s>%s</option>', $last_url . '10000', ($_REQUEST['last_dept_id'] == '10000') ? 'selected=selected' : '', 'Merchant');
                                    } else {
                                        $last_url = $url . '&last_dept_id=';
                                        echo sprintf('<option value="%s" %s>%s</option>', $last_url . '10000', ($_REQUEST['last_dept_id'] == '10000') ? 'selected=selected' : '', 'Merchant');
                                    }
                                    if ($_REQUEST['last_dept_id']) {
                                        $last_len = strlen($url) - strrpos($url, 'last_dept_id') - 13;
                                        $last_url = substr($url, 0, -$last_len);
                                        echo sprintf('<option value="%s" %s>%s</option>', $last_url . '20000', ($_REQUEST['last_dept_id'] == '20000') ? 'selected=selected' : '', 'MCR');
                                    } else {
                                        $last_url = $url . '&last_dept_id=';
                                        echo sprintf('<option value="%s" %s>%s</option>', $last_url . '20000', ($_REQUEST['last_dept_id'] == '20000') ? 'selected=selected' : '', 'MCR');
                                    }
                                    ?>

                                    <?php
                                    if ($depts = Dept::getDepartments(array('parent' => true))) {
                                        foreach ($depts as $id => $name) {
                                            $_SERVER['REQUEST_URI'] = str_replace('index.php', 'tickets.php?', $_SERVER['REQUEST_URI']);

                                            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                                            $check_url = substr(substr($url, -4), 0, 3);
                                            if ($check_url == 'scp')
                                                $url.='tickets.php?';

                                            if ($_REQUEST['last_dept_id']) {
                                                $last_len = strlen($url) - strrpos($url, 'last_dept_id') - 13;
                                                $last_url = substr($url, 0, -$last_len);
                                                echo sprintf('<option value="%s" %s>%s</option>', $last_url . $id, ($_REQUEST['last_dept_id'] == $id) ? 'selected=selected' : '', $name);
                                            } else {
                                                $last_url = $url . '&last_dept_id=';
                                                echo sprintf('<option value="%s" %s>%s</option>', $last_url . $id, ($_REQUEST['last_dept_id'] == $id) ? 'selected=selected' : '', $name);
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                                <!--                                    </form>-->
                            </th>
                            <?php
// }
//}
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
// Setup Subject field for display
                        $subject_field = TicketForm::objects()->one()->getField('subject');

                        $class = "row1";
                        $total = 0;

                        if ($res && ($num = count($results))):
                            $ids = ($errors && $_POST['tids'] && is_array($_POST['tids'])) ? $_POST['tids'] : null;
                            foreach ($results as $row) {
                                $tag = $row['staff_id'] ? 'assigned' : 'openticket';
                                $flag = null;
                                if ($row['lock_id'])
                                    $flag = 'locked';
                                elseif ($row['isoverdue'])
                                    $flag = 'overdue';

                                $lc = '';
                                if ($showassigned) {
                                    if ($row['staff_id'])
                                        $lc = sprintf('<span class="Icon staffAssigned">%s</span>', Format::truncate($row['staff'], 40));
                                    elseif ($row['team_id'])
                                        $lc = sprintf('<span class="Icon teamAssigned">%s</span>', Format::truncate($row['team'], 40));
                                    else
                                        $lc = ' ';
                                }else {
                                    $lc = Format::truncate($row['dept_name'], 40);
                                }
                                $lc1 = '';
                                $lc1 = Format::truncate($row['last_dept'], 40);
                                $l_id = $row['last_id'];
                                if (empty($lc1) && $l_id == 10000) {
                                    $lc1 = 'Merchant';
                                }
                                if ($l_id == 20000) {
                                    $lc1 = 'MCR panel';
                                }
                                $tid = $row['number'];

                                $subject = Format::truncate($subject_field->display(
                                                        $subject_field->to_php($row['subject']) ? : $row['subject']
                                                ), 40);
                                $threadcount = $row['thread_count'];
                                if (!strcasecmp($row['state'], 'open') && !$row['isanswered'] && !$row['lock_id']) {
                                    $tid = sprintf('<b>%s</b>', $tid);
                                }
                                $now = time();
                                $date = strtotime($row['ticket_created']);
                                $datediff = abs($now - $date);
                                $age = floor($datediff / (60 * 60 * 24));
                
                                if ($row['transfertime']==NULL){
                                    $tdate = strtotime($row['ticket_created']);
                                }    
                                else{
                                    $tdate=strtotime($row['transfertime']);
                                }
                                $tdiff=abs($now-$tdate);
                                $taging= floor($tdiff/(60*60*24));
                                ?>
                                <tr id="<?php echo $row['ticket_id']; ?>">
                                    <?php
                                    if ($thisstaff->canManageTickets()) {

                                        $sel = false;
                                        if ($ids && in_array($row['ticket_id'], $ids))
                                            $sel = true;
                                        ?>
                                        <td class="testAlign"  class="nohover">
                                            <input id="<?php echo $row['ticket_id'] . '_chk'; ?>" class="ckb" type="checkbox" name="tids[]"
                                                   value="<?php echo $row['ticket_id']; ?>" <?php echo $sel ? 'checked="checked"' : ''; ?>>
                                            <label for="<?php echo $row['ticket_id'] . '_chk'; ?>"></label>
                                        </td>
                                    <?php } ?>
                                    <td class="testAlign" title="<?php echo $row['email']; ?>" nowrap>
                                        <a class="Icon <?php echo strtolower($row['source']); ?>Ticket ticketPreview"
                                           title="<?php echo __('Preview Ticket'); ?>"
                                           href="tickets.php?id=<?php echo $row['ticket_id']; ?>"><?php echo $tid; ?></a></td>
                                    <td class="testAlign"  nowrap><?php echo Format::db_datetime($row['effective_date']); ?></td>
                                    <td class="testAlign" ><a <?php if ($flag) { ?> class="Icon <?php echo $flag; ?>Ticket" title="<?php echo ucfirst($flag); ?> Ticket" <?php } ?>
                                                                 href="tickets.php?id=<?php echo $row['ticket_id']; ?>"><?php
                                                                     $topic_object = Topic::lookup($row['topic_id']);
                                                                     if (is_object($topic_object)) {
                                                                         echo $topic_object->getFullName();
                                                                     } else {
                                                                         echo $subject;
                                                                     }
                                                                     ?></a>
                                        <?php
                                        if ($threadcount > 1)
                                            echo "<small>($threadcount)</small>&nbsp;" . '<i
                                class="icon-fixed-width icon-comments-alt"></i>&nbsp;';
                                        if ($row['collaborators'])
                                            echo '<i class="icon-fixed-width icon-group faded"></i>&nbsp;';
                                        if ($row['attachments'])
                                            echo '<i class="icon-fixed-width icon-paperclip"></i>&nbsp;';
                                        ?>
                                    </td>
                                    <td class="testAlign"><?php
                                        echo Format::htmlchars(Format::truncate($row['name'], 22, strpos($row['name'], '@')));
                                        ?></td>
                                    <?php
                                    $displaystatus = ucfirst($row['status']);
                                    if (!strcasecmp($row['state'], 'open'))
                                        $displaystatus = "<b>$displaystatus</b>";
                                    echo "<td>$displaystatus</td>";
                                    ?>
                                    <td class="testAlign" style="background-color:<?php echo $row['priority_color'];?>"><?php echo $row['priority_desc']; ?></td>
                                    <?php
                                    if ($showassigned) {
                                        $lc2 = Format::truncate($row['dept_name'], 40);
                                        ?>
                                        <td class="testAlign"><?php echo $lc2; ?></td>
                                    <?php } ?>

                                    <td class="testAlign"><?php echo $lc; ?></td>
                                    <?php if (CLOSED_LOOPING && $dept_id == MRG_ID ) {
                                     ?>
                                    <td class="testAlign"><?php echo $age; ?></td>
                                    <?php } 
                                    
                                    else { ?>
                                    <td class="testAlign"><?php echo $taging; ?></td>
                                    <?php } ?>
                                    <?php
                                    // if ($thisstaff->getDeptId() == MRG_ID) {
                                    //if (strcasecmp($status, 'closed')) {
                                    ?>
                                    <td class="testAlign"><?php echo $lc1; ?></td>
                                    <?php
                                    //}
                                    // }
                                    ?>

                                </tr>
                                <?php
                            } //end of while.
                        else: //not tickets found!! set fetch error.
                            $ferror = __('There are no tickets matching your criteria.');
                        endif;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="testAlign" colspan="8">
                                <?php if ($res && $num && $thisstaff->canManageTickets()) { ?>
                                    <?php echo __('Select'); ?>:
                                    <a id="selectAll" href="#ckb"><?php echo __('All'); ?></a>
                                    <a id="selectNone" href="#ckb"><?php echo __('None'); ?></a>

                                    <a id="selectToggle" href="#ckb"><?php echo __('Toggle'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php if ($num > 0) { ?> 
                                        <a href="" class="export-csv no-pjax export_bttm" id="bulk_assign_link">Bulk Task Assign</a>
                                        <?php if ($thisstaff->getDeptId() == MRG_ID) { ?>
                                            <a href="" class="export-csv no-pjax export_bttm" id="bulk_assign_link2">Bulk Ticket Assign</a>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php
                                } else {
                                    echo '<i>';
                                    echo $ferror ? Format::htmlchars($ferror) : __('Query returned 0 results.');
                                    echo '</i>';
                                }
                                ?>
                                <?php
                                if ($num > 0) {
                                    echo sprintf('<a style="cursor:pointer" class="export-csv no-pjax export_bttm" id="selected_export" >%s</a>', __('Export Selected'));
                                    // $h_href = "?a=export&h=".$hash."&status=".$_REQUEST['status'];
                                    if (strpos($_SERVER['QUERY_STRING'], 'a=search') !== false) {
                                        $h_href = str_replace("a=search", "a=export", $_SERVER['QUERY_STRING']);
                                        $h_href.="&h=" . $hash . "&status=" . $_REQUEST['status'];
                                    } elseif (strpos($_SERVER['QUERY_STRING'], 'advsid=') !== false) {

                                        //$h_href = str_replace("a=search","a=export",$_SERVER['QUERY_STRING']);
                                        $h_href = "&a=export&h=" . $hash . "&status=" . $_REQUEST['status'];
                                    } elseif (strpos($_SERVER['QUERY_STRING'], 'sort=&order=&p=') !== false) {
                                        $h_href = "&a=export&h=" . $hash . "&status=" . $_REQUEST['status'];
                                    } else {
                                        /* mc-39 dinesh */
                                        if (trim(substr($_SERVER['REQUEST_URI'], -1) == '?')) {
                                            $h_href = "a=export&h=" . $hash . "&status=" . $_REQUEST['status'];
                                        } elseif ($_SERVER['QUERY_STRING'] == 'status=reopen' ||
                                                $_SERVER['QUERY_STRING'] == 'status=inprogress' ||
                                                $_SERVER['QUERY_STRING'] == 'status=overdue' ||
                                                $_SERVER['QUERY_STRING'] == 'status=solved' ||
                                                $_SERVER['QUERY_STRING'] == 'status=closed') {
                                            $h_href = "&a=export&h=" . $hash . "&status=" . $_REQUEST['status'];
                                        } else {
                                            $h_href = "?a=export&h=" . $hash . "&status=" . $_REQUEST['status'];
                                        }
                                    }
                                }
                                ?>

                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php
                if ($num > 0) { //if we actually had any tickets returned.
                    echo '<div class="paging"><span>' . __('Page') . ':</span><span class="pagination" >' . $pageNav->getPageLinks() . '</span>&nbsp;';
                    echo sprintf('<a class="export-csv no-pjax export_bttm" href="?%s">%s</a>', Http::build_query(array(
                                'a' => 'export', 'h' => $hash,
                                'status' => $_REQUEST['status'])), __('Export'));
                    echo '&nbsp;<i class="help-tip icon-question-sign  help_icon" href="#export"></i></div>';
                }
                ?>

                <input type="hidden" id="h_export_selected_href" value="<?php echo $h_href; ?>">
            </form>
        </div>
    </div>
</div>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm'); ?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="mark_overdue-confirm">
        <?php echo __('Are you sure want to flag the selected tickets as <font color="red"><b>OSLA</b></font>?'); ?>
    </p>
    <div><?php echo __('Please confirm to continue.'); ?></div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="pull-left float_left">
            <input type="button" value="<?php echo __('No, Cancel'); ?>" class="close buttons">
        </span>
        <span class="pull-right float_right">
            <input type="button" value="<?php echo __('Yes, Do it!'); ?>" class="confirm buttons">
        </span>
    </p>
    <div class="clear"></div>
</div>
<div class="dialog advance_search_popup grid-row" style="display:none;" id="bulk_assign_popup">
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <h3><?php echo __('Bulk Assignment'); ?></h3>
    <form action="tickets.php" method="POST" name='tickets' id="tickets">
        <?php csrf_token(); ?>
        <input type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" name="location"/>
        <input id="b_tickets" type="hidden" value="" name="b_tickets"/>
        <fieldset class="span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="statusId"><?php echo __('Agents'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select  name="Staff_Id">
                    <?php
                    $users = Dept::getDepartmentMembers($dept_id);
                    if ($users) {

                        foreach ($users as $id => $name) {

                            if (!is_object($name))
                                $name = new PersonsName($name);

                            $k = "s$id";
                            echo sprintf('<option value="%s">%s</option>', $k, $name);
                        }
                    }
                    ?>
                </select>
            </div>
        </fieldset>

        <input type="submit" value="Bulk Assign" name="bulk_assign"/>
    </form>
</div>
<div class="dialog advance_search_popup grid-row" style="display:none;" id="bulk_assign_popup2">
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <h3><?php echo __('Bulk Assignment'); ?></h3>
    <form action="tickets.php" method="POST" name='tickets2' id="tickets2">
        <?php csrf_token(); ?>
        <input type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" name="location"/>
        <input id="b_tickets2" type="hidden" value="" name="b_tickets2"/>
        <fieldset class="span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="statusId"><?php echo __('Agents'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select  name="Staff_Id">
                    <?php
                    $users = Dept::getDepartmentMembers(MRG_ID);
                    if ($users) {

                        foreach ($users as $id => $name) {

                            if (!is_object($name))
                                $name = new PersonsName($name);

                            $k = "s$id";
                            echo sprintf('<option value="%s">%s</option>', $k, $name);
                        }
                    }
                    ?>
                </select>
            </div>
        </fieldset>

        <input type="submit" value="Bulk Assign" name="bulk_assign2"/>
    </form>
</div>


<div class="dialog advance_search_popup grid-row" style="display:none;" id="advanced-search">
    <h3><?php echo __('Advanced Ticket Search'); ?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <form action="tickets.php" method="post" id="search" name="search">
        <input type="hidden" name="a" value="search">
        <fieldset class="query grid-row">
            <div class="grid12">
                <input type="input" id="query" name="query" size="20" placeholder="<?php echo __('Keywords') . ' &mdash; ' . __('Optional'); ?>"></div>
        </fieldset>
        <fieldset class="span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="statusId"><?php echo __('Statuses'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select id="statusId" name="statusId">
                    <option value="">&mdash; <?php echo __('Any Status'); ?> &mdash;</option>
                    <?php
                    foreach (TicketStatusList::getStatuses(
                            array('states' => array('open', 'closed'))) as $s) {
                        echo sprintf('<option data-state="%s" value="%d">%s</option>', $s->getState(), $s->getId(), __($s->getName()));
                    }
                    ?>
                </select>
            </div>
        </fieldset>
        <fieldset class="span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="deptId"><?php echo __('Departments'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select id="deptId" name="deptId[]" multiple="multiple" size="5 " style="height:100%; display:inline;">
                    <option value="">&mdash; <?php echo __('Hold ctrl key for Multiple Selection'); ?> &mdash;</option>
                    <option value="">&mdash; <?php echo __('All Departments'); ?> &mdash;</option>
                    <?php
                    if (($mydepts = $thisstaff->getDepts()) && ($depts = Dept::getDepartments())) {
                        foreach ($depts as $id => $name) {
                            if (!in_array($id, $mydepts))
                                continue;
                            echo sprintf('<option value="%d">%s</option>', $id, $name);
                        }
                    }
                    ?>
                </select>
            </div>
        </fieldset>
        <fieldset class="span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="flag"><?php echo __('Flags'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select id="flag" name="flag">
                    <option value="">&mdash; <?php echo __('Any Flags'); ?> &mdash;</option>
                    <?php if (!$cfg->showAnsweredTickets()) { ?>
                        <option data-state="open" value="answered"><?php echo __('Answered'); ?></option>
                    <?php }
                    ?>
                    <option data-state="open" value="overdue"><?php echo __('OSLA'); ?></option>
                </select>
            </div>
        </fieldset>
        <fieldset class="owner span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="assignee"><?php echo __('Task Assigned To'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select id="assignee" name="assignee[]" multiple="multiple" size="5 " style="height:100%; display:inline;">
                    <option value="">&mdash; <?php echo __('Hold ctrl Key for Multiple selection'); ?> &mdash;</option>
                    <option value="">&mdash; <?php echo __('Anyone'); ?> &mdash;</option>
                    <option value="s0">&mdash; <?php echo __('Unassigned'); ?> &mdash;</option>
                    <option value="s<?php echo $thisstaff->getId(); ?>"><?php echo __('Me'); ?></option>
                    <?php
                    if (($users = Staff::getStaffMembers())) {
                        echo '<OPTGROUP label="' . sprintf(__('Agents (%d)'), count($users)) . '">';
                        foreach ($users as $id => $name) {
                            $k = "s$id";
                            echo sprintf('<option value="%s">%s</option>', $k, $name);
                        }
                        echo '</OPTGROUP>';
                    }

                    if (($teams = Team::getTeams())) {
                        echo '<OPTGROUP label="' . __('Teams') . ' (' . count($teams) . ')">';
                        foreach ($teams as $id => $name) {
                            $k = "t$id";
                            echo sprintf('<option value="%s">%s</option>', $k, $name);
                        }
                        echo '</OPTGROUP>';
                    }
                    ?>
                </select>
            </div>
        </fieldset>
        <fieldset class="span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="topicId"><?php echo __('Help Topics'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select id="topicId" name="topicId">
                    <option value="" selected >&mdash; <?php echo __('All Help Topics'); ?> &mdash;</option>
                    <?php
                    if ($topics = Topic::getHelpTopics()) {
                        foreach ($topics as $id => $name)
                            echo sprintf('<option value="%d" >%s</option>', $id, $name);
                    }
                    ?>
                </select>
            </div>
        </fieldset>
        <fieldset class="owner span6 form-row grid-row">
            <div class="grid4 float_left">
                <label for="staffId"><?php echo __('Closed By'); ?>:</label>
            </div>
            <div class="grid8 float_left">
                <select id="staffId" name="staffId">
                    <option value="0">&mdash; <?php echo __('Anyone'); ?> &mdash;</option>
                    <option value="<?php echo $thisstaff->getId(); ?>"><?php echo __('Me'); ?></option>
                    <?php
                    if (($users = Staff::getStaffMembers())) {
                        foreach ($users as $id => $name)
                            echo sprintf('<option value="%d">%s</option>', $id, $name);
                    }
                    ?>
                </select>
            </div>
        </fieldset>
        <fieldset class="date_range form-row grid-row">
            <div class="grid4 float_left">
                <label><?php echo __('Date Range') . ' &mdash; ' . __('Create Date'); ?>:</label>
            </div>
            <div class="grid8 float_left date_input">
                <input class="dp" type="input" size="20" name="startDate">
                <span class="between"><?php echo __('TO'); ?></span>
                <input class="dp" type="input" size="20" name="endDate">
            </div>
        </fieldset>
        <?php
        $tform = TicketForm::objects()->one();
        echo $tform->getForm()->getMedia();
        foreach ($tform->getInstance()->getFields() as $f) {
            if (!$f->hasData())
                continue;
            elseif (!$f->getImpl()->hasSpecialSearch())
                continue;
            ?>
            <fieldset class="span6 form-row grid-row">
                <div class="grid4 float_left">
                    <label><?php echo $f->getLabel(); ?>:</label>
                </div>
                <div class="grid8 float_left">
                    <div class="select_change"><?php $f->render('search'); ?></div>
                </div>
            </fieldset>
        <?php } ?>
        <?php if (CLOSED_LOOPING) { ?>
            <fieldset class="span6 form-row grid-row">
                <div class="grid4 float_left">
                    <label for="7c3d46b1fa80a6f7"><?php echo __('Order Id'); ?>:</label>
                </div>
                <div class="grid8 float_left">
                    <input type="text" value="" name="7c3d46b1fa80a6f7" id="order_id"/>
                </div>
            </fieldset>
        <?php } ?>
        <hr/>
        <div id="result-count" class="clear"></div>
        <p>
            <span class="float_right">
                <input type="submit" value="<?php echo __('Search'); ?>">
            </span>
            <span class="float_left">
                <input type="reset" value="<?php echo __('Reset'); ?>">
                <input type="button" value="<?php echo __('Cancel'); ?>" class="close">
            </span>
            <span class="spinner">
                <img src="./images/ajax-loader.gif" width="16" height="16">
            </span>
        </p>
    </form>
</div>
<script type="text/javascript">
    
    $('.testAlign').css("text-align","center");
    $(function () {
        $(document).off('.tickets');
        $(document).on('click.tickets', 'a.tickets-action', function (e) {
            e.preventDefault();
            var count = checkbox_checker($('form#tickets'), 1);
            if (count) {
                var url = 'ajax.php/'
                        + $(this).attr('href').substr(1)
                        + '?count=' + count
                        + '&_uid=' + new Date().getTime();
                $.dialog(url, [201], function (xhr) {
                    window.location.href = window.location.href;
                });
            }
            return false;
        });
    });
</script>