<?php

class db {

    protected static $instance = null;
    private static $dbname = SHOPCLUES_DB;
    private static $dbhost = SHOPCLUES_HOST;
    private static $dbusername = SHOPCLUES_USER;
    private static $dbpassword = SHOPCLUES_PASSWORD;
    
    protected static $instance_order = null;
    private static $dbname_order = SHOPCLUES_DB_OT;
    private static $dbhost_order = SHOPCLUES_HOST_OT;
    private static $dbusername_order = SHOPCLUES_USER_OT;
    private static $dbpassword_order = SHOPCLUES_PASSWORD_OT;

    public static function getDbInstance($dbServer = "ff") {
        switch ($dbServer) {
            case "ff":
                if (!isset(static::$instance)) {
                    static::$instance = new mysqli(static::$dbhost, static::$dbusername, static::$dbpassword, static::$dbname);
                    if (static::$instance->connect_errno) {
                        echo "Failed to connect to MySQL: (" . static::$instance->connect_errno . ")" . static::$instance->connect_errno;
                        exit();
                    }
                } 
                return static::$instance;
                break;
            case "om":
                if (!isset(static::$instance_order)) {
                    static::$instance_order = new mysqli(static::$dbhost_order, static::$dbusername_order, static::$dbpassword_order, static::$dbname_order);
                    if (static::$instance_order->connect_errno) {
                        echo "Failed to connect to MySQL: (" . static::$instance_order->connect_errno . ")" . static::$instance_order->connect_errno;
                        exit();
                    }
                } 
                return static::$instance_order;
                break;
        }

        
    }
    
public static function updateTicket($status_id,$ticket_id){
    $map=array(11=>'Open',12=>'In Progress',13=>'Solved',14=>'Closed');
    $status=$map[$status_id];
    $ticket = Ticket::lookup($ticket_id);
    $number=$ticket->getNumber();
    $sql="UPDATE clues_merchant_tickets set ticket_status='$status' where ticket_number=$number";
    $mysqli = db::getDbInstance();
    mysqli_query($mysqli, $sql);
    $mysqli->close;
}
    
}
