<?php

/* 
 * Taskflow admin setting
 * @author - Akash Kumar
 */

require('admin.inc.php');

$page='ticketflow.inc.php';
$nav->setTabActive('manage');
if($_POST)
{$remove=0;
    foreach($_POST as $k=>$check){
        if(stristr($k,'remove')){
            $id = explode("-",$k)[1];
            $remove=1;
            TicketFlow::objects()->filter(array("ticketflow_id"=>$id))->delete();
            
            $msg = "Ticket Flow removed Successfully";
        }
    }
    if(!$remove){
        $dept = $_POST["topic"];
        $type = $_POST["type"];
        $status = $_POST[$type."Status"];

        $numRows = TicketFlow::objects()->filter(array("help_topic"=>$opic,"status_type"=>$type,"status"=>$status))->count();
        if($numRows<1){
            $taskFlowObj = TicketFlow::create();
            $taskFlowObj->help_topic = $dept;
            $taskFlowObj->status_type = $type;
            $taskFlowObj->status = $status;
            $taskFlowObj->ChangeStatus = 2; // Closed Statisfactorily by default
            $taskFlowObj->save();
            $msg = "Successfully Updated";
        }else{
         $msg = "Ticket Flow combination already exists";   
        }
    }
}
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
