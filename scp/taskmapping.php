<?php

/* 
 * Taskflow admin setting
 * @author - 
 */

require('admin.inc.php');
include_once(INCLUDE_DIR.'class.template.php');
require_once('../include/class.taskmapping.php');
$issueSubIssueDictionary=getTopicDetailDictionary();
$page='taskmapping.inc.php';
$nav->setTabActive('manage');
if($_POST)
{   global $thisstaff;
    $remove=0;
    foreach($_POST as $k=>$check){
        if(stristr($k,'remove')){
            $id = explode("-",$k)[1];
            $remove=1;
            $taskDisable=TaskMapping::objects()->filter(array("id"=>$id,"task_status_id"=>1,"entity_type"=>"order"))->first();
            UtilityFunctions::flowLogs('mst_first_task',$id,0,$thisstaff->ht['staff_id'],1,'task removed',null);   
            $taskDisable->task_status_id=0;
            $taskDisable->save();
            $msg = "Task removed Successfully";
        }
    }
    if(!$remove){
        $topicId = $_POST["topicId"];
        $taskType = $_POST["taskType"];
        $orderStatus = $_POST["orderStatus"];
        $dept = $_POST["taskDept"];
        $return_status=$_POST['return_status'];
        $billing_status=$_POST['billing_status'];
	$autoResponse = $_POST['response'];
        if(in_array(-1, $orderStatus)){
            $orderStatus = array(-1);
        }
        $res = TaskMapping::objects()->filter(array("topic_id"=>$topicId,"order_status_id__in"=>$orderStatus,
            "dept_id"=>$dept,
            "entity_type"=>"order",
            "billing_status"=>$billing_status,
            "return_status"=>$return_status,
            "task_type_id"=>$taskType,
            "task_status_id"=>1,
	    "autoresponse"=>$autoResponse,
            "entity_type"=>"order"))->values('order_status_id')->all();
        foreach ($res as $value) {
            $orderStatusAlreadyPresent[] = $value['order_status_id'];
        }
        $present = 0;
        $notPresent = 0;
        foreach ($orderStatus as $val) {
            if(!in_array($val, $orderStatusAlreadyPresent)) {
            $FlowObj = TaskMapping::create();
            $FlowObj->topic_id = $topicId;
            $FlowObj->order_status_id = $val;
            $FlowObj->dept_id = $dept;
            $FlowObj->entity_type = "order";
            $FlowObj->task_type_id = $taskType;
            $FlowObj->return_status = $return_status;
            $FlowObj->billing_status=$billing_status;
            $FlowObj->task_status_id = 1;
	    $FlowObj->autoresponse = $autoResponse;
            $FlowObj->save();
            $flow_id=$FlowObj->ht['id'];
            UtilityFunctions::flowLogs('mst_first_task',$flow_id,json_encode($FlowObj->ht),$thisstaff->ht['staff_id'],null,' new task added',null);
            $notPresent++;
            }else{
                $present++;  
            }
        }
	if(!empty($notPresent)){
            $msg = $notPresent." Successfully Added ";
        }
        if(!empty($present)){
            $msg .= $present." Task attribute combination already exists.";
        }
    }
}
if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
   require(STAFFINC_DIR.$page);
}
else{
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
}
?>
