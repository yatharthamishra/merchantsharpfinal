<?php
/* 
 * Data reprocessing script
 * @author - Divyanshu
 */
ini_set('memory_limit','2048M');
set_time_limit(0);
require('admin.inc.php');
include_once(INCLUDE_DIR.'class.template.php');
require(INCLUDE_DIR.'/class.dataReprocessingController.php');
if($_POST)
	$data=$_POST;
$controller = new DataReprocessingController();
$controller->invoke($data);


?>
