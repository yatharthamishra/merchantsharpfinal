<?php

/* 
 * Taskflow admin setting
 * @author - Akash Kumar
 */

require('admin.inc.php');

$page='taskflow.inc.php';
$nav->setTabActive('manage');
if($_POST)
{$remove=0;
global $thisstaff;
    foreach($_POST as $k=>$check){
        if(stristr($k,'remove')){
            $id = explode("-",$k)[1];
            $remove=1;
            $taskFlowDisable=TaskFlow::objects()->filter(array("task_id"=>$id,"flowstatus"=>1))->first();
            UtilityFunctions::flowLogs('mst_task_flow',$taskFlowDisable->task_id,0,$thisstaff->ht['staff_id'],1,'removed',null);
          $taskFlowDisable->flowstatus=0;
           $taskFlowDisable->save();
            $msg = "Task Flow removed Successfully";
        }
    }
    if(!$remove){
        $dept = $_POST["department"];
        $type = $_POST["type"];
        $response = $_POST["response"];
        $status = $_POST["status"];
        $ticket = $_POST["ticket_status"];
        $next = $_POST["next_task"];

        $numRows = TaskFlow::objects()->filter(array("type"=>$type,"dept_id"=>$dept,"task_status"=>$status,"task_response"=>$response,"flowstatus"=>1))->count();
        if($numRows<1){
            $taskFlowObj = TaskFlow::create();
            $taskFlowObj->dept_id = $dept;
            $taskFlowObj->type = $type;
            $taskFlowObj->task_status = $status;
            $taskFlowObj->task_response = $response;
            $taskFlowObj->ticket_status = $ticket;
            $taskFlowObj->next_task_id = $next;
            $taskFlowObj->save();
            $task_id=$taskFlowObj->ht['task_id'];
          UtilityFunctions::flowLogs('mst_task_flow',$task_id,json_encode($taskFlowObj->ht),$thisstaff->ht['staff_id'],null,'added',null);
            $msg = "Successfully Added";
        }else{
         $msg = "Task attribute combination already exists";   
        }
    }
}
if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
   require(STAFFINC_DIR.$page);
}
else{
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
}
?>
