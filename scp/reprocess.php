<?php 
require('admin.inc.php');
require_once(INCLUDE_DIR.'class.orderStatusCron.php');

$orderidErr = false;
$statusErr = false;
$displayMsg = "";

if($_POST['submit']){
    $orderId =  $_POST['orderid'];
    $status = $_POST['status'];
    $statusType =  $_POST['statusType'];

    if(empty($orderId)){
        $orderidErr = true;
    } elseif($status == 'none'){
        $statusErr = true;
    } 
    
    if(!$orderidErr && !$statusErr) {
        $orderIdArray[] = explode(",", $orderId);
        $errorOrderIds = array();
        $existingOrdderIds = array();
        $res = false;

        $statusCronArr = OrderStatusCron::objects()
                ->filter(array("status_type" =>$statusType,"to_status" => $status, "order_id__in" => $orderIdArray[0]))
                ->values("order_id","id")
                ->all();
        foreach ($statusCronArr as $records) {
            if(in_array($records['order_id'],$orderIdArray[0])){
                $existingOrdderIds[] = $records['order_id'];
                $order_id = $records['order_id'];
                $id = $records['id'];
                $toUpdatestatus = OrderStatusCron::objects()->filter(array("status_type" =>$statusType,"order_id" => $order_id, "id__gte" => $id));
                foreach($toUpdatestatus as $toUpdate){
                    $toUpdate->processed = '0';
                    $res = $toUpdate->save();
                }
            }
        }
        $errorOrderIds=array_diff($orderIdArray[0],$existingOrdderIds);
 
        if($res == true){
            $displayMsg = "Order statuses succesfully updated";
            $errorCss = "color:#009815;";
        } else{
            $displayMsg = "Please check the order ids, either wrong id OR order id & status doesnt match";
            $errorCss = "color:#a70000;";
        }
        if(!empty($errorOrderIds) && $res==true){
            $displayMsg = "Order statuses updated successfully except for following, Either ID does not exist OR status doesn't match: ";
            $displayMsg.= "(". implode(',', $errorOrderIds).")";
            $errorCss = "color:#a70000;";
        }
    }
}

$staticData = getCachedTicketFields();
$res["Order"] = $staticData["orderStatus"]['Order'];
$res["Return"] = $staticData["orderStatus"]['Return'];
$res["Refund"] = $staticData["orderStatus"]['Refund'];
$nav->setTabActive('manage');
require(STAFFINC_DIR.'header.inc.php');
?>

<h3><b style="font-size: 19px;color: rgba(0, 0, 0, 0.97);">
        &nbsp;&nbsp;Enter comma separated order id's to Reprocess Orders</b></h3>

<div class="reprocessFrom">
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
    <label><b style="font-size: 16px; margin: 34px;">
            Order Id's :</b>
    </label> 
    <br><input class="rpText" type="text" name="orderid" placeholder="Enter order ids" value="<?php echo $orderId;?>">
  <?php if($orderidErr){ echo "<span style='color:#FF0000';>*Please enter at least one order id</span>";}?>
  <br><br>
  <label><b style="font-size: 16px; margin: 34px;">
            Status Type :</b>
    </label>
  <br><br><b style="font-size: 13px; margin: 34px;"><input type="radio" name="statusType" value="Order" checked>&nbsp; Order
       &nbsp;&nbsp;<input type="radio" name="statusType" value="Return">&nbsp; Return
       &nbsp;&nbsp;<input type="radio" name="statusType" value="Refund">&nbsp; Refund<br></b>
  <br><br>
    <label><b style="font-size: 16px; margin: 34px;">
            Status :</b>
    </label>  <br><select class="rpText"name="status">
                <option value="none">Select</option>
               <?php foreach ($res as $key => $value) {
                    foreach ($value as $k => $v) {
                        $statusDesc = $key.'   - '.$k.'  : '.$v;?>
                    <option value="<?php echo $k;?>"><?php echo $statusDesc;?></option>
                <?php }
                }
               ?>
            </select>
  <?php if($statusErr){ echo "<span style='color:#FF0000';>*Please select status to process above order Id's</span>";}?>
  <br><br>
  
  <input id="rpSubmit" type="submit" name="submit" value="Submit">  
  <?php if(!empty($displayMsg)){
      echo "<br><br><span style='font-size: 19px;margin-left: 15%; $errorCss';>$displayMsg</span>";
  }?>
</form></div>
<?php include(STAFFINC_DIR.'footer.inc.php');?>



<style>
.rpText {
    width: 60%;
    padding: 14px 20px;
    margin: 12px 35px;
    display: inline-block;
    border: 1px solid rgba(0, 0, 0, 0.25);
    border-radius: 4px;
    box-sizing: border-box;
}
#rpSubmit {
    width: 59%;
    background-color: #4CAF50;
    color: white;
    font-size: 15px;
    padding: 11px 6px;
    margin: 41px 35px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
/*
rpSubmit:hover {
    background-color: #45a049;
}*/

div.reprocessFrom {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
</style>