<?php
/* *******************************************************************
  login.php

  Handles staff authentication/logins
  Peter Rotich <peter@osticket.com>
  Copyright (c)  2006-2013 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */
require_once('../main.inc.php');
if (!defined('INCLUDE_DIR'))
    die('Fatal Error. Kwaheri!');
global $cfg;
if (!$cfg->config['localLoginEnabled']['value']) {
    if (!empty($_REQUEST["token"])) {
        $tokenDecrypt = clues_decrypt_data($_REQUEST["token"]);
        if (!$tokenDecrypt["sessionServer"]) {
            global $ost;
            $ost->logError("Token Mismatch login attempt", "Message - " . $_REQUEST["token"]);
            echo "Invalid token";die;
        } else {
            define("ADMIN_MEMCACHE_SERVERS", $tokenDecrypt["sessionServer"]);
            
            $totalServers = count(explode(",",$tokenDecrypt["sessionServer"]));
            $server = 0;
            while(!$adminData && $server<$totalServers){
                $adminMemecacheObj = new MemcacheStorage(30000, 1,$server);
                $adminData = $adminMemecacheObj->getValue("userdata" . $tokenDecrypt["userId"]);
                $server++;
            }
            
            $MemecacheObj = new MemcacheStorage();
            setcookie("adminToken", $_REQUEST["token"], time() + (3600000), "/");
            $admin = $adminData["user_data"];
            $privilege = $adminData["user_privilege"];
            if(!in_array("osticket_access",$privilege)){
                //echo "You need osticket access to view the page";die;
            }
            $adminAgentConfig = json_decode($cfg->config['adminAgentConfigSingleSignon']['value'], true);
            $defaultPassDec = $adminAgentConfig['password']['decoded']; //'welcome';
            $defaultPassEnc = $adminAgentConfig['password']['encoded']; //; // Setting Password as welcome
            if ($admin["email"]) {
                $staff = Staff::objects()->filter(array("email" => $admin["email"]))->values('username', 'dept_id', 'staff_id', 'passwd')->all();
                if (count($staff) > 0) {
                    $staff = $staff[0];
                    $staffObj = Staff::lookup(array("email" => $admin["email"]));
                    if ($staff["passwd"] != $defaultPassEnc) {
                        $staffObj->passwd = $defaultPassEnc;
                    }
                    
                    if($staffObj->role_id==0){
                        $staffObj->dept_id = $adminAgentConfig['default_dept_id'];
                        $staffObj->role_id = $adminAgentConfig['default_role_id'];
                    }
                    $staffObj->permissions = json_encode($priv);
                    $staffObj->save();
                    $_POST['userid'] = $staff['username'];
                    $_POST['passwd'] = $defaultPassDec;
                    $userDest = $_GET["url"];
                } else {

                    $newStaff = Staff::create();
                    $newStaff->username = substr(explode("@",$admin["email"])[0],0,20);
                    $newStaff->firstname = $admin["firstname"];
                    $newStaff->lastname = $admin["lastname"];
                    $newStaff->isactive = $adminAgentConfig['default_active'];
                    $newStaff->created = SqlFunction::NOW();
                    $newStaff->permissions = json_encode($priv);
                    $newStaff->isadmin = $adminAgentConfig['default_admin'];
                    $newStaff->dept_id = $adminAgentConfig['default_dept_id'];
                    $newStaff->role_id = $adminAgentConfig['default_role_id'];
                    $newStaff->passwd = $defaultPassEnc;
                    $newStaff->max_page_size = 50;
                    $newStaff->email = $admin["email"];
                    $newStaff->save();

                    $_POST['userid'] = substr(explode("@",$admin["email"])[0],0,20);
                    $_POST['passwd'] = $defaultPassDec;
                    $userDest = $_GET["url"];
                }
            } else {
                redirectForLogout();
            }
        }
    } else {
        redirectForLogin();
    }
}
function redirectForLogout(){
    if ($_SERVER['HTTP_REFERER']) {
        $refrer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
    }
    global $cfg;
    $singleSignonAuthUrl = json_decode($cfg->config['singleSignonAuthUrl']['value'], true);
    $authDomain = $refrer ? (stristr($refrer,"shopclues.com")?$refrer:$singleSignonAuthUrl['defaultDomain']) : $singleSignonAuthUrl['defaultDomain']; //"http://localhost";
    $authUrl = $authDomain . explode("?",$singleSignonAuthUrl['path'])[0]."?dispatch=auth.logout&"; //"/branches/b5/UniTechCity.php?dispatch=ticket.auth&destination=";
    
    if(!stristr($authUrl,"ttp")){
        $authUrl = "http://".  $authUrl;
    }
    $loginDomainReplace=json_decode($cfg->config['login_domain_replace']['value'],1);
    $authUrl = str_replace($loginDomainReplace['old'],$loginDomainReplace['new'],$authUrl);
    $link = $_SERVER['REQUEST_URI'];
    $link_array = explode('/',$link);
    $landing = $_GET["url"] ? $_GET["url"] : (end($link_array));
    header('Location: ' . $authUrl . $landing);
    die;
}
function redirectForLogin() {
    if ($_SERVER['HTTP_REFERER']) {
        $refrer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
    }
    global $cfg;
    $singleSignonAuthUrl = json_decode($cfg->config['singleSignonAuthUrl']['value'], true);

    $authDomain = $refrer ? $refrer : $singleSignonAuthUrl['defaultDomain']; //"http://localhost";
    $authUrl = $authDomain . $singleSignonAuthUrl['path']; //"/branches/b5/UniTechCity.php?dispatch=ticket.auth&destination=";
    
    if(!stristr($authUrl,"ttp")){
        $authUrl = "http://".  $authUrl;
    }
    $loginDomainReplace=json_decode($cfg->config['login_domain_replace']['value'],1);
    $authUrl = str_replace($loginDomainReplace['old'],$loginDomainReplace['new'],$authUrl);
    $link = $_SERVER['REQUEST_URI'];
    $link_array = explode('/',$link);
    $landing = $_GET["url"] ? $_GET["url"] : (end($link_array));
    header('Location: ' . $authUrl . $landing);
    die;
}

// Bootstrap gettext translations. Since no one is yet logged in, use the
// system or browser default
TextDomain::configureForUser();

require_once(INCLUDE_DIR . 'class.staff.php');
require_once(INCLUDE_DIR . 'class.csrf.php');

$content = Page::lookupByType('banner-staff');

if (!$userDest) {
    $dest = $_SESSION['_staff']['auth']['dest'];
    $msg = $_SESSION['_staff']['auth']['msg'];
    $msg = $msg ? : ($content ? $content->getLocalName() : __('Authentication Required'));
    $dest = ($dest && (!strstr($dest, 'login.php') && !strstr($dest, 'ajax.php'))) ? $dest : 'index.php';
} else {
    $dest = $userDest;
}
$show_reset = false;
if ($_POST) {
    // Check the CSRF token, and ensure that future requests will have to
    // use a different CSRF token. This will help ward off both parallel and
    // serial brute force attacks, because new tokens will have to be
    // requested for each attempt.
    // if (!$ost->checkCSRFToken())
    //   Http::response(400, __('Valid CSRF Token Required'));
    // Rotate the CSRF token (original cannot be reused)
    //$ost->getCSRF()->rotate();
    // Lookup support backends for this staff
    $username = trim($_POST['userid']);
    if ($user = StaffAuthenticationBackend::process($username, $_POST['passwd'], $errors)) {
        session_write_close();
        Http::redirect($dest);
        require_once('index.php'); //Just incase header is messed up.
        exit;
    }

    $msg = $errors['err'] ? $errors['err'] : __('Invalid login');
    $show_reset = true;
} elseif ($_GET['do']) {
    switch ($_GET['do']) {
        case 'ext':
            // Lookup external backend
            if ($bk = StaffAuthenticationBackend::getBackend($_GET['bk']))
                $bk->triggerAuth();
    }
    Http::redirect('login.php');
}
// Consider single sign-on authentication backends
elseif (!$thisstaff || !($thisstaff->getId() || $thisstaff->isValid())) {
    if (($user = StaffAuthenticationBackend::processSignOn($errors, false)) && ($user instanceof StaffSession))
        @header("Location: $dest");
}

define("OSTSCPINC", TRUE); //Make includes happy!
include_once(INCLUDE_DIR . 'staff/login.tpl.php');
?>
