<?php

/* 
 * Taskflow admin setting
 * @author - Akash Kumar
 */

require('admin.inc.php');
include_once(INCLUDE_DIR.'class.template.php');

$page='tasksla.inc.php';
$nav->setTabActive('manage');

if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
   require(STAFFINC_DIR.$page);
}
else{
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
}
?>
