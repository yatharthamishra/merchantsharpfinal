<?php

/*
 * Taskflow admin setting
 * @author - Akash Kumar
 */

require('admin.inc.php');
include_once(INCLUDE_DIR . 'class.template.php');

$page = 'returnflow.inc.php';
$nav->setTabActive('manage');
if ($_POST) {
    global $thisstaff;
    $remove = 0;
    foreach ($_POST as $k => $check) {
        if (stristr($k, 'remove')) {
            $id = explode("-", $k)[1];
            $remove = 1;
         $returnDisable=ReturnFlow::objects()->filter(array("flow_id" => $id,"flowstatus"=>1))->first(); 
    // print_r($returnDisable);die;
         UtilityFunctions::flowLogs('mst_return_flow',$id,0,$thisstaff->ht['staff_id'],1,"removed",null);
            $returnDisable->flowstatus=0;
        // print_r($returnDisable);die;
            $returnDisable->save();
            $msg = "Return Flow removed Successfully";
        }
    }
    if (!$remove) {
        $type = $_POST["type"];
        $statusArr = $_POST["returnStatus"];
        if (empty($statusArr))
            $msg = "Return Status Selection is mandatory";
        else {
            $statusArr = !in_array("0",$statusArr)?$statusArr:array("0");
            $return_actionArr = !in_array("0",$_POST["returnAction"]) ? $_POST["returnAction"] : array("0");
            $valueArr = !in_array("0",$_POST["valueRange"]) ? $_POST["valueRange"] : array("0");
            $tag_nameArr = !in_array("0",$_POST["tagName"]) ? $_POST["tagName"] : array("0");
            $sub_reasonArr = !in_array("0",$_POST["subReason"]) ? $_POST["subReason"] : array("0");
            $taskTypeArr = !in_array("0",$_POST["taskType"]) ? $_POST["taskType"] : array("0");
            $deptArr = !in_array("0",$_POST["taskDept"]) ? $_POST["taskDept"] : array("0");
            $response = $_POST["response"];
            $communication = $_POST["communication"];
            foreach ($statusArr as $status) {
                foreach ($return_actionArr as $return_action) {
                    foreach ($valueArr as $value) {
                        foreach ($tag_nameArr as $tag_name) {
                            foreach ($sub_reasonArr as $sub_reason) {
                                foreach ($taskTypeArr as $taskType) {
                                    foreach ($deptArr as $dept) {

                                        $value_min = explode("-", $value)[0];
                                        $value_max = explode("-", $value)[1];

                                        $numRows = ReturnFlow::objects()->filter(array("status_type" => $type, "status" => $status,"flowstatus"=>1, "return_action" => $return_action, "value_min" => $value_min, "value_max" => $value_max, "tag_name" => $tag_name, "sub_reason" => $sub_reason, "dept" => $dept, "task_type" => $taskType))->count();
                                        if ($numRows < 1) {
                                            $FlowObj = ReturnFlow::create();
                                            $FlowObj->status_type = $type;
                                            $FlowObj->status = $status;
                                            $FlowObj->return_action = $return_action;
                                            $FlowObj->value_min = $value_min;
                                            $FlowObj->value_max = $value_max;
                                            $FlowObj->tag_name = $tag_name;
                                            $FlowObj->sub_reason = $subReason;
                                            $FlowObj->dept = $dept;
                                            $FlowObj->task_type = $taskType;
                                            $FlowObj->task_response = $response;
                                            $FlowObj->communication = $communication;
                                            $FlowObj->save();
                               $flowId=$FlowObj->ht['flow_id'];             
      UtilityFunctions::flowLogs('mst_return_flow',$flowId,json_encode($FlowObj->ht),$thisstaff->ht['staff_id'],NULL,'returnflow added',NULL);   
                                            $msg .= " Successfully Added<br>";
                                        } else {
                                            $msg .= " Return attribute combination already exists<br>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
   require(STAFFINC_DIR.$page);
}
else{
require(STAFFINC_DIR . 'header.inc.php');
require(STAFFINC_DIR . $page);
include(STAFFINC_DIR . 'footer.inc.php');
}
?>
