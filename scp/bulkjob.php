<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$s = dirname(__FILE__) . "/../";
chdir($s);
ini_set("memory_limit","2048M");
set_time_limit(0);
global $disableDefaultShutdownFunction;
$disableDefaultShutdownFunction=1;
global $disableDefaultExceptionHandler;
$disableDefaultExceptionHandler=1;
require_once('main.inc.php');
$dir = getcwd()."/scp";
chdir($dir);
global $cfg;
$login = json_decode($cfg->config['ost_super_user_login']['value'],true);
$thisstaff = StaffAuthenticationBackend::process($login["username"],$login["password"], $errors);
require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.dept.php');
require_once(INCLUDE_DIR.'class.filter.php');
require_once(INCLUDE_DIR.'class.canned.php');
require_once(INCLUDE_DIR.'class.json.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');
require_once(INCLUDE_DIR.'class.export.php');       // For paper sizes
include_once(INCLUDE_DIR.'class.bulkjob.php');
$bulkObj = new bulkjob();
$jobList = $bulkObj->performJobOperation();
print_r($jobList);die;

$thisstaff->logOut();
