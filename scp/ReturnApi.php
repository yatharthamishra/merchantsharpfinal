<?php

function getReturnDetails($returnId,$orderid,$email='') {
    global $cfg;
    //for zero order id
    if(!$returnId){
        $returnId='';
    }
    if(!$orderid){
        $orderid='';
    }
        
    $basePath = $cfg->config['orderDetailApiBasePath']["value"];
    $authKey = $cfg->config['authKeyOrderDetailApi']["value"];
    $token = $cfg->config['tokenOrderDetailApi']["value"];
    $action = 'getReturnDataForOsTicket';
    ini_restore('include_path');
    require_once 'HTTP/Request2.php';
//      $basePath='http://localhost/API_new/api/v11/';
      
    $request = new HTTP_Request2($basePath . $action."?key=".$authKey."&ostoken=".$token."&return_id=" . $returnId."&order_id=" . $orderid."&email_id=".$email."&time=".time(), HTTP_Request2::METHOD_GET);
    $request->setConfig(array(
        'connect_timeout' => 30,
        'timeout' => 30,
        'follow_redirects' => TRUE,
        'ssl_verify_peer' => FALSE,
        'ssl_verify_host' => FALSE,
    ));       
    try 
    {          
        $response = $request->send();  
        if (200 == $response->getStatus()) 
        {
            $returnData = json_decode($response->getBody()); 
           return $returnData;
        }
        else 
            error_log("\nBAZOOKA START Improper status from SOA API. API response is: " . json_encode($response) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");            
    }
    catch (HTTP_Request2_Exception $e)
    {
        error_log("\nBAZOOKA START Exception while calling SOA API. Exception is: " . json_encode($e) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");
    }     
}

