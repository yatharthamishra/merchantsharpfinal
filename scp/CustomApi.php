<?php

function getOrderDetails($orderId) {
    global $cfg;
    $basePath = $cfg->config['orderDetailApiBasePath']["value"];
    $authKey = $cfg->config['authKeyOrderDetailApi']["value"];
    $token = $cfg->config['tokenOrderDetailApi']["value"];
    $action = 'getMultipleOrderDataForOsTicket';
     ini_restore('include_path');
    require_once 'HTTP/Request2.php';
    
    
    $request = new HTTP_Request2($basePath . $action."?key=".$authKey."&ostoken=".$token."&order_id=" . $orderId."&time=".time(), HTTP_Request2::METHOD_GET);
    $request->setConfig(array(
        'connect_timeout' => 30,
        'timeout' => 30,
        'follow_redirects' => TRUE,
        'ssl_verify_peer' => FALSE,
        'ssl_verify_host' => FALSE,
    ));       
    try 
    {          
        $response = $request->send();
        if (200 == $response->getStatus()) 
        {
            $orderData = json_decode($response->getBody());
           return $orderData;
            
        }
        else 
            error_log("\nBAZOOKA START Improper status from SOA API. API response is: " . json_encode($response) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");            
    }
    catch (HTTP_Request2_Exception $e)
    {
        error_log("\nBAZOOKA START Exception while calling SOA API. Exception is: " . json_encode($e) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");
    }     
}

function getSellerDetails($email='') {
    global $cfg;
    $basePath = $cfg->config['orderDetailApiBasePath']["value"];
    $authKey = $cfg->config['authKeyOrderDetailApi']["value"];
    $token = $cfg->config['tokenOrderDetailApi']["value"];
    $action = 'getSellerDetailsWithLastThreeRecords';
     ini_restore('include_path');
    require_once 'HTTP/Request2.php';
    
    
    $request = new HTTP_Request2($basePath . $action."?key=".$authKey."&ostoken=".$token."&email_id=" . $email."&time=".time(), HTTP_Request2::METHOD_GET);
    $request->setConfig(array(
        'connect_timeout' => 30,
        'timeout' => 30,
        'follow_redirects' => TRUE,
        'ssl_verify_peer' => FALSE,
        'ssl_verify_host' => FALSE,
    ));       
    try 
    {          
        $response = $request->send();
        if (200 == $response->getStatus()) 
        {
            $sellerData = json_decode($response->getBody());
           return $sellerData;
            
        }
        else 
            error_log("\nBAZOOKA START Improper status from SOA API. API response is: " . json_encode($response) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");            
    }
    catch (HTTP_Request2_Exception $e)
    {
        error_log("\nBAZOOKA START Exception while calling SOA API. Exception is: " . json_encode($e) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");
    }     
}
