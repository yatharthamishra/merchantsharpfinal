<?php

$reportType = isset($_GET['type'])? $_GET['type']: 'triaging';
$reportSubtype = isset($_GET['subtype'])? $_GET['subtype']: 'sla adherence';
$reportSubSubtype = isset($_GET['subsubtype'])? $_GET['subsubtype']: 0;
$fileName=  html_entity_decode($reportType.$reportSubtype.$reportSubSubtype).  date("Y-m-d");
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=$fileName.csv");
// Disable caching
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); 
require_once('staff.inc.php');
include_once(INCLUDE_DIR.'class.template.php');
include_once (STAFFINC_DIR.'report/reportConfig.php');
$search = SavedSearch::create();
$tickets = Ticket::objects();
$staticFields = getCachedTicketFields();
global $reportFilterConfig;
global $reportFields;
global $reportFieldNotToBeDisplayed;
global $reportFilterDictionary;
global $reportRelatedConfig;
$reportConfigObject=new CustomConfig(array( 'report'));
$config=$reportConfigObject->getConfigInfo();
$reportRelatedConfig=$config['report'];
define('TRIAGE_DATA_BUCKET_SIZE',$reportRelatedConfig['triagedatabucketsize']);//triagedatabucketsize
define('OPS_DATA_BUCKET_SIZE',$reportRelatedConfig['opsdatabucketsize']);//opsdatabucketsize 
//define('CRG_CCG_DUMP_BUCKET_SIZE',1000);
define("SLA_REVISED_BUCKET_SIZE",$reportRelatedConfig['slarevisedbucketsize']); //slarevisedbucketsize
define("OVERALL_TICKET_BUCKET_SIZE",$reportRelatedConfig['oveticketbucketsize']);//oveticketbucketsize
define("CCG_DEPARTMENT_ID",$reportRelatedConfig['ccgdeptId']);//ccgdeptId
define("OVERALL_TICKET_DUMP_TIMEOUT", $reportRelatedConfig['oveticketdumptimeout']);//oveticketdumptimeout
        //define("CRG_CCG_DUMP_TIMEOUT", 3600);
define("RAW_OPS_DUMP_TIMEOUT", $reportRelatedConfig['rawopsdumptimeout']);//rawopsdumptimeout
define("SLA_REV_DUMP_TIMEOUT", $reportRelatedConfig['slarevdumptimeout']);//slarevdumptimeout
define("OVERALL_REPORT_DEFAULT_TIME_PERIOD", $reportRelatedConfig['overeportdefaulttimeperiod']);//overeportdefaulttimeperiod
define("REPORT_CSV_SAVE_DIR",$reportRelatedConfig['reportsavecsvdir']);//reportsavecsvdir
      
$filterConfig=$reportFilterConfig;
$fields=$reportFields;
$fieldNotToBeDisplayed=$reportFieldNotToBeDisplayed;
$filterDictionary=$reportFilterDictionary;
ob_get_clean();
include REPORT_LIBRARY_PATH."ReportLibrary.php";
include REPORT_FILTERS_PATH . 'FilterData.php';
include REPORT_FILTERS_PATH . 'FilterRequest.php';

updateFilterConfigVariable($fields,$filterConfig);
//These parameters determine which report to display
$reportType = $_GET['type']? : 'triaging';
$reportSubtype = $_GET['subtype']? : 'sla adherence';
$reportSubSubtype = $_GET['subsubtype']? : 0;
if (!$_GET['subtype'])
    $reportSubSubtype = 'agent level';
include STAFFINC_DIR."report/cs_reports/query_for_reports.php";
include STAFFINC_DIR."report/cs_reports/file_export.php";
function updateFilterConfigVariable($fields,&$filterConfig) {

        /**
         * These lines update the filter config with the data according to the filter report type 
         * Updates $filterConfig['filterDictionary'] varible according to the request .
         */
        $filterDataObject = new FilterData();
        $filterDataObject->generateFilterData($fields,$filterConfig); // updates filter config data with data of filter required to render the same
        $filterRequestObject = new FilterRequest();
        $filterRequestObject->generateActiveAndUpdateSelectedFilters($filterConfig); // updates filter config with currently selected value of 
// the filter
       // var_dump($filterConfig);exit;
    }
?>
