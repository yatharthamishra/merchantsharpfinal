<?php
/*************************************************************************
    tickets.php

    Handles all tickets related actions.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.taskflow.php');
require_once(INCLUDE_DIR.'class.dept.php');
require_once(INCLUDE_DIR.'class.filter.php');
require_once(INCLUDE_DIR.'class.canned.php');
require_once(INCLUDE_DIR.'class.json.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');
require_once(INCLUDE_DIR.'class.export.php');       // For paper sizes
require_once(INCLUDE_DIR.'class.orderStatusCron.php');
require_once(INCLUDE_DIR.'class.tracking.php');
//require_once(INCLUDE_DIR.'class.mailfetch.php');
//MailFetcher::run();die;
//$_SESSION["advsearch"]="";
/*
setting time when user reaches this page. This time is used in user tracking
*/

$timeForTracking = time();
/*
end of time setting code
*/

$ticket = $user = null; //clean start.
//LOCKDOWN...See if the id provided is actually valid and if the user has access.
if ($_REQUEST['checkorderid']) {
    $order = OrderStatusCron::objects()->filter(array("order_id" => $_REQUEST['checkorderid']))->values('order_id','status_type','from_status','to_status','transition_time','processed','update_timestamp')->order_by('-update_timestamp')->all();
    
    foreach($order as $o){
        echo "Order - ".$o['order_id']." -> ".$o["status_type"]." Status- ".$o["from_status"]." to ".$o["to_status"]." is ".($o["processed"]?"processed":"Not Processed")." on ".$o["update_timestamp"]." ENTRY came on ".$o["transition_time"]." - "."<br>";
    }
    echo "-----";
$sql = "select * from clues_order_master where order_id=".$_REQUEST['checkorderid'];
$res = db_query($sql) or die('Query failed: ' . db_error()) ;
$row = db_fetch_array($res);
echo "<br><br><br>Order status in Clues_order_reporting_master - ".$row["order_status"];


    die;
}

    function flow($arr,$flow){
        global $newFlow;
        if($arr[$flow]["next"]){
            $newFlow[$flow] = $arr[$flow]["next"];
            if(count($newFlow)>3)
                return;

            flow($arr,$arr[$flow]["next"]);
        }
        $newFlow[$flow] = $arr[$flow]["next"];
    }
if ($_REQUEST['tree']) {
    $taskStatic = getCachedTaskFields();
     $orderArr = TaskFlow::objects()->filter(array("flowstatus"=>1))->values("task_id","task_response","next_task_id","dept_id","type")->all();
    foreach($orderArr as $flow){
        $flowArr[$flow['task_id']] = array("type"=>$taskStatic["Task Types"][$flow["type"]],"Dept"=>$flow["dept_id"],"next"=>$flow['next_task_id'],"response"=>$taskStatic["Task Response"][$flow['task_response']]);
    }
    $count = 0;
    $page = $_GET["page"]?:1;
    $flowArr  = array_slice($flowArr,($page-1)*40,$page*40);

    foreach($flowArr as $id=>$flow){
        if($flowArr[$flow['next']]){
            global $newFlow;
            $newFlow = array();
            $flowNext = flow($flowArr,$flow['next']);
        }else{
            $newFlow = 0;
        }
        $flowNewArr[$id] = array("next"=>$newFlow);
    } ?>
     <table border='1'>
    <?php foreach($flowNewArr as $id=>$new){ ?>
         <tr>
             <td><?php echo $flowArr[$id]["type"]."<br><span style='font-size:9px;'>".$flowArr[$id]["response"]."</span>"; ?></td>
             <td><table>
                 <?php 
                 if(!$new["next"])echo "<span style='color:red;'>Complete</span>";
                 foreach($new["next"] as $from=>$next){ ?>
                     <tr>
                         <td>
                             <?php echo $flowArr[$from]["type"]." ---<span style='font-size:9px;'>".$flowArr[$from]["response"]."</span>--- ";echo $next?$flowArr[$next]["type"]:"<span style='color:red;'>Complete</span>"; ?>
                         </td>
                         
                     </tr>
                 <?php } ?>
                 </table>
             </td>
         </tr>
    <?php } ?>
     </table>
    <?php    
    die;
}
// if($_REQUEST["closetest"]){
//     $Arr = array(7515,9477,9489,11412,12608,12611,13175,13644,14662,14688,16038,17235,18582,18964,19795,19870,19960,20099,20326,20718,22097,22098,22115,22121,23811);
//     foreach($Arr as $id){
//         $ticketObj = Ticket::lookup($id);
//         $ticketObj->setStatus(2,'By Cron on All task Closure');
//         echo $id." Done - ";
//     }
//     die;
// }

if($_REQUEST['id'] || $_REQUEST['number']) {
    global $allowedToEditTicket;
    global $allowedToCreateTaskAndTriage;
    global $thisstaff;
    global $cfg;
    $allowedToEditTicket=true;
    $allowedToCreateTaskAndTriage=false;

if($_REQUEST['number']){
    $ticket = Ticket::lookupByNumber($_REQUEST['number']);
    $textToBeShownInError=" Ticket Number ";
}else {
    $ticket=Ticket::lookup($_REQUEST['id']);
    $textToBeShownInError=" Ticket Id ";
}
$edDepartmentId=$cfg->getEDDepartmentId();

$ticketDepartmentIdAssigned=$ticket->ht['dept_id'];
$departmentIdExcludedForTaskCreation=$cfg->getDepartmentsToRestrictCreationOfTaskOnCCG();
$departmentIdsAccessExcludedTaskTriageArray=explode(',', $departmentIdExcludedForTaskCreation);

if(!($thisstaff->checkIfLoginUserOfEDDepartment() || $thisstaff->checkIfLoginUserOfRRDepartment()) && $edDepartmentId==$ticketDepartmentIdAssigned){
    $allowedToEditTicket=false;
}
if(($thisstaff->checkIfCCGLikeAccessForTask()) && !in_array($ticketDepartmentIdAssigned, $departmentIdsAccessExcludedTaskTriageArray)){
    $allowedToCreateTaskAndTriage=true;
}else if(($thisstaff->checkIfLoginUserOfEDDepartment())){
    $allowedToCreateTaskAndTriage=true;
;}else if(in_array($ticketDepartmentIdAssigned, $thisstaff->getDepts())){
    $allowedToCreateTaskAndTriage=true;
}
    if(!($ticket))
         $errors['err']=sprintf(__('%s: Unknown or invalid .'.$textToBeShownInError), __('ticket'));
} 
//print_r($_REQUEST);exit;
if ($_REQUEST['comment']) {
    $sql='INSERT IGNORE INTO '.THREAD_ENTRY_TABLE.' SET body="'.$_REQUEST['comment'].
    '" ,thread_id='.$_REQUEST['thread_id'].' ,created=now(), type="R" ,staff_id='.$_REQUEST['staffid'].'  ,poster="'.$_REQUEST['poster'].'"';
    db_query($sql) or die('Error inserting');
    //$user = User::lookup($_REQUEST['uid']);
}
//Lookup user if id is available.
if ($_REQUEST['uid']) {
    $user = User::lookup($_REQUEST['uid']);
}
elseif (@!isset($_REQUEST['advanced']) && @$_REQUEST['a'] != 'search' && !isset($_GET['status']) && isset($_SESSION['::Q'])) {
    //$_GET['status'] = $_REQUEST['status'] = $_SESSION['::Q'];
}
// Configure form for file uploads
$response_form = new SimpleForm(array(
    'attachments' => new FileUploadField(array('id'=>'attach',
        'name'=>'attach:response',
        'configuration' => array('extensions'=>'')))
));
$note_form = new SimpleForm(array(
    'attachments' => new FileUploadField(array('id'=>'attach',
        'name'=>'attach:note',
        'configuration' => array('extensions'=>'')))
));

//At this stage we know the access status. we can process the post.
if($_POST && !$errors):

    if($ticket && $ticket->getId()) {
        //More coffee please.
        $errors=array();
        $lock = $ticket->getLock(); //Ticket lock if any
        $role = $thisstaff->getRole($ticket->getDeptId());
        switch(strtolower($_POST['a'])):
        case 'reply':
            if (!$role || !$role->hasPerm(TicketModel::PERM_REPLY)) {
                $errors['err'] = __('Action denied. Contact admin for access');
            }
            else {

                if(!$_POST['response'])
                    $errors['response']=__('Response required');

                if ($cfg->getLockTime()) {
                    if (!$lock) {
                        $errors['err'] = __('This action requires a lock. Please try again');
                    }
                    // Use locks to avoid double replies
                    elseif ($lock->getStaffId()!=$thisstaff->getId()) {
                        $errors['err'] = __('Action Denied. Ticket is locked by someone else!');
                    }
                    // Attempt to renew the lock if possible
                    elseif (($lock->isExpired() && !$lock->renew())
                        ||($lock->getCode() != $_POST['lockCode'])
                    ) {
                        $errors['err'] = __('Your lock has expired. Please try again');
                    }
                }

                //Make sure the email is not banned
                if(!$errors['err'] && Banlist::isBanned($ticket->getEmail()))
                    $errors['err']=__('Email is in banlist. Must be removed to reply.');
            }

            //If no error...do the do.
            $vars = $_POST;
            $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();
            if(!$errors && ($response=$ticket->postReply($vars, $errors, $_POST['emailreply']))) {
                /*
                Tracking user movements
                */
                // $trackingData = array('staff_id' => $thisstaff->ht['staff_id'],
                //                         'timestamp' => date("Y-m-d H:i:s",time()),
                //                         'event' => 'Email reply',
                //                         'ticket_id' => $ticket->getId(),
                //                         'task_id' => 0
                //                          );
                // $trackingObj = new Tracking;
                // $trackingObj->tracking($trackingData);
                /*
                user tracking code ends
                */
                $msg = sprintf(__('%s: Reply posted successfully'),
                        sprintf(__('Ticket #%s'),
                            sprintf('<a href="tickets.php?id=%d"><b>%s</b></a>',
                                $ticket->getId(), $ticket->getNumber()))
                        );
            if($_POST["nextCustomerUpdate"]){
                $ticket->setDueDate(date('Y-m-d H:i:s',strtotime($_POST["nextCustomerUpdate"])));
            }    
                // Clear attachment list
                $response_form->setSource(array());
                $response_form->getField('attachments')->reset();

                // Remove staff's locks
                $ticket->releaseLock($thisstaff->getId());

                // Cleanup response draft for this user
                Draft::deleteForNamespace(
                    'ticket.response.' . $ticket->getId(),
                    $thisstaff->getId());

                // Go back to the ticket listing page on reply
                //$ticket = null;

            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to post the reply. Correct the errors below and try again!');
            }
            break;
        case 'transfer': /** Transfer ticket **/
            //Check permission
            if(!$role->hasPerm(TicketModel::PERM_TRANSFER))
                $errors['err']=$errors['transfer'] = __('Action Denied. You are not allowed to transfer tickets.');
            else {

                //Check target dept.
                if(!$_POST['deptId'])
                    $errors['deptId'] = __('Select department');
                elseif($_POST['deptId']==$ticket->getDeptId())
                    $errors['deptId'] = __('Ticket already in the department');
                elseif(!($dept=Dept::lookup($_POST['deptId'])))
                    $errors['deptId'] = __('Unknown or invalid department');

                //Transfer message - required.
                if(!$_POST['transfer_comments'])
                    $errors['transfer_comments'] = __('Transfer comments required');
                elseif(strlen($_POST['transfer_comments'])<5)
                    $errors['transfer_comments'] = __('Transfer comments too short!');

                //If no errors - them attempt the transfer.
                if(!$errors && $ticket->transfer($_POST['deptId'], $_POST['transfer_comments'], true, $_POST['ticketPriority'])) {
                    $msg = sprintf(__('Ticket transferred successfully to %s'),
                            $ticket->getDept()->getFullName());
                    //Check to make sure the staff still has access to the ticket
                    if(!$ticket->checkStaffPerm($thisstaff))
                        $ticket=null;

                } elseif(!$errors['transfer']) {
                    $errors['err'] = __('Unable to complete the ticket transfer');
                    $errors['transfer']=__('Correct the error(s) below and try again!');
                }
            }
            break;
        case 'assign':

             if(!$role->hasPerm(TicketModel::PERM_ASSIGN))
                 $errors['err']=$errors['assign'] = __('Action Denied. You are not allowed to assign/reassign tickets.');
             else {

                 $id = preg_replace("/[^0-9]/", "",$_POST['assignId']);
                 $claim = (is_numeric($_POST['assignId']) && $_POST['assignId']==$thisstaff->getId());
                 $dept = $ticket->getDept();

                 if (!$_POST['assignId'] || !$id)
                     $errors['assignId'] = __('Select assignee');
                 elseif ($_POST['assignId'][0]!='s' && $_POST['assignId'][0]!='t' && !$claim)
                     $errors['assignId']= sprintf('%s - %s',
                             __('Invalid assignee'),
                             __('get technical support'));
                 elseif ($_POST['assignId'][0]=='s'
                         && $dept->assignMembersOnly()
                         && !$dept->isMember($id)) {
                     $errors['assignId'] = sprintf('%s. %s',
                             __('Invalid assignee'),
                             __('Must be department member'));
                 } elseif($ticket->isAssigned()) {
                     if($_POST['assignId'][0]=='s' && $id==$ticket->getStaffId())
                         $errors['assignId']=__('Ticket already assigned to the agent.');
                     elseif($_POST['assignId'][0]=='t' && $id==$ticket->getTeamId())
                         $errors['assignId']=__('Ticket already assigned to the team.');
                 }

                 if(!$errors && $ticket->assign($_POST['assignId'], $_POST['assign_comments'], !$claim)) {
                     if($claim) {
                         $msg = __('Ticket is NOW assigned to you!');
                     } else {
                         $msg=sprintf(__('Ticket assigned successfully to %s'), $ticket->getAssigned());
                         //$ticket->releaseLock($thisstaff->getId());
                         //$ticket=null;
                     }
                 } elseif(!$errors['assign']) {
                     $errors['err'] = __('Unable to complete the ticket assignment');
                     $errors['assign'] = __('Correct the error(s) below and try again!');
                 }
             }
            break;
        case 'postnote': /* Post Internal Note */
            $vars = $_POST;
            $attachments = $note_form->getField('attachments')->getClean();
            $vars['cannedattachments'] = array_merge(
                $vars['cannedattachments'] ?: array(), $attachments);

            if ($cfg->getLockTime()) {
                if (!$lock) {
                    $errors['err'] = __('This action requires a lock. Please try again');
                }
                // Use locks to avoid double replies
                elseif ($lock->getStaffId()!=$thisstaff->getId()) {
                    $errors['err'] = __('Action Denied. Ticket is locked by someone else!');
                }
                elseif ($lock->getCode() != $_POST['lockCode']) {
                    $errors['err'] = __('Your lock has expired. Please try again');
                }
            }

            $wasOpen = ($ticket->isOpen());
            if(($note=$ticket->postNote($vars, $errors, $thisstaff))) {
                $msg=__('Internal note posted successfully');
                // Clear attachment list
                $note_form->setSource(array());
                $note_form->getField('attachments')->reset();

                if($wasOpen && $ticket->isClosed()){
                 //   $ticket = null; //Going back to main listing.
                }
                else
                    // Ticket is still open -- clear draft for the note
                    Draft::deleteForNamespace('ticket.note.'.$ticket->getId(),
                        $thisstaff->getId());

            } else {

                if(!$errors['err'])
                    $errors['err'] = __('Unable to post internal note - missing or invalid data.');

                $errors['postnote'] = __('Unable to post the note. Correct the error(s) below and try again!');
            }
            break;
        case 'customernote': /* Post Internal Note */
            $vars = $_POST;
            $attachments = $note_form->getField('attachments')->getClean();
            $vars['cannedattachments'] = array_merge(
                $vars['cannedattachments'] ?: array(), $attachments);

            if ($cfg->getLockTime()) {
                if (!$lock) {
                    $errors['err'] = __('This action requires a lock. Please try again');
                }
                // Use locks to avoid double replies
                elseif ($lock->getStaffId()!=$thisstaff->getId()) {
                    $errors['err'] = __('Action Denied. Ticket is locked by someone else!');
                }
                elseif ($lock->getCode() != $_POST['lockCode']) {
                    $errors['err'] = __('Your lock has expired. Please try again');
                }
            }
            $vars["message"]=$vars["note"];
            $vars["userId"]=$ticket->user_id;
            $wasOpen = ($ticket->isOpen());
            if(($note=$ticket->postMessage($vars))) {
                $ticket->logEvent('behalf', null, $thisstaff);
                $msg=__('Note on behalf of Customer posted successfully');
                // Clear attachment list
                $note_form->setSource(array());
                $note_form->getField('attachments')->reset();

                if($wasOpen && $ticket->isClosed())
                    $ticket = null; //Going back to main listing.
                else
                    // Ticket is still open -- clear draft for the note
                    Draft::deleteForNamespace('ticket.note.'.$ticket->getId(),
                        $thisstaff->getId());

            } else {

                if(!$errors['err'])
                    $errors['err'] = __('Unable to post customer note - missing or invalid data.');

                $errors['customernote'] = __('Unable to post customer note. Correct the error(s) below and try again!');
            }
            break;
        case 'edit':
        case 'update':
            if(!$ticket || !$role->hasPerm(TicketModel::PERM_EDIT))
                $errors['err']=__('Permission Denied. You are not allowed to edit tickets');
            elseif($ticket->update($_POST,$errors)) {
                $msg=__('Ticket updated successfully');
                $urlOfTicket = str_replace("&a=edit", "", $_SERVER['REQUEST_URI']);
                header('Location: '.$urlOfTicket);
                $_REQUEST['a'] = null; //Clear edit action - going back to view.
                //Check to make sure the staff STILL has access post-update (e.g dept change).
                if(!$ticket->checkStaffPerm($thisstaff))
                    $ticket=null;
            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to update the ticket. Correct the errors below and try again!');
            }
            break;
        case 'process':
            switch(strtolower($_POST['do'])):
                case 'release':
                    if(!$ticket->isAssigned() || !($assigned=$ticket->getAssigned())) {
                        $errors['err'] = __('Ticket is not assigned!');
                    } elseif($ticket->release()) {
                        $msg=sprintf(__(
                            /* 1$ is the current assignee, 2$ is the agent removing the assignment */
                            'Ticket released (unassigned) from %1$s by %2$s'),
                            $assigned, $thisstaff->getName());
                        $ticket->logActivity(__('Ticket unassigned'),$msg);
                    } else {
                        $errors['err'] = __('Problems releasing the ticket. Try again');
                    }
                    break;
                case 'claim':
                    if(!$role->hasPerm(TicketModel::PERM_EDIT)) {
                        $errors['err'] = __('Permission Denied. You are not allowed to assign/claim tickets.');
                    } elseif(!$ticket->isOpen()) {
                        $errors['err'] = __('Only open tickets can be assigned');
                    } elseif($ticket->isAssigned()) {
                        $errors['err'] = sprintf(__('Ticket is already assigned to %s'),$ticket->getAssigned());
                    } elseif ($ticket->claim()) {
                        $msg = __('Ticket is now assigned to you!');
                    } else {
                        $errors['err'] = __('Problems assigning the ticket. Try again');
                    }
                    break;
                case 'overdue':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets overdue');
                    } elseif($ticket->markOverdue()) {
                        $msg=sprintf(__('Ticket flagged as overdue by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Overdue'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the the ticket overdue. Try again');
                    }
                    break;
                case 'answered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets');
                    } elseif($ticket->markAnswered()) {
                        $msg=sprintf(__('Ticket flagged as answered by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Answered'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the the ticket answered. Try again');
                    }
                    break;
                case 'unanswered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets');
                    } elseif($ticket->markUnAnswered()) {
                        $msg=sprintf(__('Ticket flagged as unanswered by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Unanswered'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the ticket unanswered. Try again');
                    }
                    break;
                case 'banemail':
                    if (!$thisstaff->hasPerm(Email::PERM_BANLIST)) {
                        $errors['err']=__('Permission Denied. You are not allowed to ban emails');
                    } elseif(BanList::includes($ticket->getEmail())) {
                        $errors['err']=__('Email already in banlist');
                    } elseif(Banlist::add($ticket->getEmail(),$thisstaff->getName())) {
                        $msg=sprintf(__('Email %s added to banlist'),$ticket->getEmail());
                    } else {
                        $errors['err']=__('Unable to add the email to banlist');
                    }
                    break;
                case 'unbanemail':
                    if (!$thisstaff->hasPerm(Email::PERM_BANLIST)) {
                        $errors['err'] = __('Permission Denied. You are not allowed to remove emails from banlist.');
                    } elseif(Banlist::remove($ticket->getEmail())) {
                        $msg = __('Email removed from banlist');
                    } elseif(!BanList::includes($ticket->getEmail())) {
                        $warn = __('Email is not in the banlist');
                    } else {
                        $errors['err']=__('Unable to remove the email from banlist. Try again.');
                    }
                    break;
                case 'changeuser':
                    if (!$role->hasPerm(TicketModel::PERM_EDIT)) {
                        $errors['err']=__('Permission Denied. You are not allowed to edit tickets');
                    } elseif (!$_POST['user_id'] || !($user=User::lookup($_POST['user_id']))) {
                        $errors['err'] = __('Unknown user selected');
                    } elseif ($ticket->changeOwner($user)) {
                        $msg = sprintf(__('Ticket ownership changed to %s'),
                            Format::htmlchars($user->getName()));
                    } else {
                        $errors['err'] = __('Unable to change ticket ownership. Try again');
                    }
                    break;
                default:
                    $errors['err']=__('You must select action to perform');
            endswitch;
            break;
        default:
            $errors['err']=__('Unknown action');
        endswitch;
    }elseif($_POST['a']) {

        switch($_POST['a']) {
            case 'open':
                $ticket=null;
                if (!$thisstaff ||
                        !$thisstaff->hasPerm(TicketModel::PERM_CREATE, false)) {
                     $errors['err'] = sprintf('%s %s',
                             sprintf(__('You do not have permission %s.'),
                                 __('to create tickets')),
                             __('Contact admin for such access'));
                } else {
                    $vars = $_POST;
                    $vars['uid'] = $user? $user->getId() : 0;

                    $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();

                    if(($ticket=Ticket::open($vars, $errors))) {
                        $msg=__('Ticket created successfully');
                        $_REQUEST['a']=null;
                        if (!$ticket->checkStaffPerm($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();
                        unset($_SESSION[':form-data']);
                    } elseif(!$errors['err']) {
                        $errors['err']=__('Unable to create the ticket. Correct the error(s) and try again');
                    }
                }
                break;
        }
    }
    if(!$errors)
        $thisstaff ->resetStats(); //We'll need to reflect any changes just made!
endif;

/*... Quick stats ...*/
$stats= $thisstaff->getTicketsStats();


// Clear advanced search upon request
if (isset($_GET['clear_filter'])){
    unset($_SESSION['advsearch']);
    unset($_SESSION["searched"]);
    unset($_SESSION['bulk']);
    unset($_SESSION['searchORM']);
}

//Navigation
$nav->setTabActive('tickets');
$open_name = _P('queue-name',
    /* This is the name of the open ticket queue */
    'Open');
if(0 && $cfg->showAnsweredTickets()) {
    $nav->addSubMenu(array('desc'=>$open_name.' ('.number_format($stats['open']+$stats['answered']).')',
                            'title'=>__('Open Tickets'),
                            'href'=>'tickets.php?status=open',
                            'iconclass'=>'Ticket'),
                        ((!$_REQUEST['status'] && !isset($_SESSION['advsearch'])) || $_REQUEST['status']=='open'));
} elseif(0) {

    if ($stats) {

        $nav->addSubMenu(array('desc'=>$open_name.' ('.number_format($stats['open']).')',
                               'title'=>__('Open Tickets'),
                               'href'=>'tickets.php?status=open',
                               'iconclass'=>'Ticket'),
                            ((!$_REQUEST['status'] && !isset($_SESSION['advsearch'])) || $_REQUEST['status']=='open'));
    }

    if($stats['answered']) {
        $nav->addSubMenu(array('desc'=>__('Answered').' ('.number_format($stats['answered']).')',
                               'title'=>__('Answered Tickets'),
                               'href'=>'tickets.php?status=answered',
                               'iconclass'=>'answeredTickets'),
                            ($_REQUEST['status']=='answered'));
    }
}

if($stats['assigned']) {
    
}else{
    $stats['assigned'] = 0;
}

    $nav->addSubMenu(array('desc'=>__('My Tickets').(!isset($_SESSION['advsearch'])?(' ('.number_format($stats['assigned']).')'):''),
                           'title'=>__('Assigned Tickets'),
                           'href'=>'tickets.php?status=assigned',
                           'iconclass'=>'assignedTickets'),
                        ($_REQUEST['status']=='assigned'));


if(0 && $stats['overdue']) {
    $nav->addSubMenu(array('desc'=>__('Overdue').' ('.number_format($stats['overdue']).')',
                           'title'=>__('Stale Tickets'),
                           'href'=>'tickets.php?status=overdue',
                           'iconclass'=>'overdueTickets'),
                        ($_REQUEST['status']=='overdue'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
}

if(0 && $thisstaff->showAssignedOnly() && $stats['closed']) {
    $nav->addSubMenu(array('desc'=>__('My Closed Tickets').' ('.number_format($stats['closed']).')',
                           'title'=>__('My Closed Tickets'),
                           'href'=>'tickets.php?status=closed',
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
} else if(0) {

    $nav->addSubMenu(array('desc' => __('Closed').' ('.number_format($stats['closed']).')',
                           'title'=>__('Closed Tickets'),
                           'href'=>'tickets.php?status=closed',
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
}

if (isset($_SESSION['advsearch'])) {
    // XXX: De-duplicate and simplify this code
    global $thisstaff;
    $edDepartmentId=$cfg->getEDDepartmentID();
    $search = SavedSearch::create();
    $form = $search->getFormFromSession('advsearch');
    
    
    $tickets = TicketModel::objects();
    $tickets = $search->mangleQuerySet($tickets, $form);
    if($thisstaff->checkIfLoginUserOfEDDepartment()){
        $tickets->filter(array('dept_id__in'=>array($edDepartmentId)));
    }else{
        $tickets->filter(array('dept_id__notin'=>array($edDepartmentId)));
    }
    if (!isset($form->state['status_id+search'])) {
        $tickets->filter(array('status_id__in' => array(1, 6)));
    } else {
        global $ignoreStatusCondition;
        $ignoreStatusCondition = 1;
    }
    $count = $tickets->count();
    $nav->addSubMenu(array('desc' => __('Search').' ('.number_format($count).')',
                           'title'=>__('Advanced Ticket Search'),
                           'href'=>'tickets.php?status=search',
                           'iconclass'=>'Ticket'),
                        (!$_REQUEST['status'] || $_REQUEST['status']=='search'));
}

if (0 && $thisstaff->hasPerm(TicketModel::PERM_CREATE, false)) {
    $nav->addSubMenu(array('desc'=>__('New Ticket'),
                           'title'=> __('Open a New Ticket'),
                           'href'=>'tickets.php?a=open',
                           'iconclass'=>'newTicket',
                           'id' => 'new-ticket'),
                        ($_REQUEST['a']=='open'));
}


$ost->addExtraHeader('<script type="text/javascript" src="js/ticket.js?33f18c5"></script>');
$ost->addExtraHeader('<script type="text/javascript" src="js/thread.js?33f18c5"></script>');
$ost->addExtraHeader('<meta name="tip-namespace" content="tickets.queue" />',
    "$('#content').data('tipNamespace', 'tickets.queue');");

if($ticket) {
    $ost->setPageTitle(sprintf(__('Ticket #%s'),$ticket->getNumber()));
    $nav->setActiveSubMenu(-1);
    $inc = 'ticket-view.inc.php';
    if ($_REQUEST['a']=='edit'
            && $ticket->checkStaffPerm($thisstaff, TicketModel::PERM_EDIT)) {
        $inc = 'ticket-edit.inc.php';
        if (!$forms) $forms=DynamicFormEntry::forTicket($ticket->getId());
        // Auto add new fields to the entries
        foreach ($forms as $f) $f->addMissingFields();
    } elseif($_REQUEST['a'] == 'print' && !$ticket->pdfExport($_REQUEST['psize'], $_REQUEST['notes']))
        $errors['err'] = __('Internal error: Unable to export the ticket to PDF for print.');
} else {
        
        if(isset($_GET['triageId'])){
            $inc = 'tickets-triage-list.inc.php';
        }elseif(isset($_GET['task'])){
            $inc = 'tickets-task-list.inc.php';
        }else{
            $inc = 'tickets.inc.php';
        }    
    if ($_REQUEST['a']=='open' &&
            $thisstaff->hasPerm(TicketModel::PERM_CREATE, false))
        $inc = 'ticket-open.inc.php';
        
    //Clear active submenu on search with no status
    if($_REQUEST['a']=='search' && !$_REQUEST['status'])
        $nav->setActiveSubMenu(-1);

    //set refresh rate if the user has it configured
    if(!$_POST && !$_REQUEST['a'] && ($min=(int)$thisstaff->getRefreshRate())) {
        $js = "+function(){ var qq = setInterval(function() { if ($.refreshTicketView === undefined) return; clearInterval(qq); $.refreshTicketView({$min}*60000); }, 200); }();";
        $ost->addExtraHeader('<script type="text/javascript">'.$js.'</script>',
            $js);
    }
}
if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
    require_once(STAFFINC_DIR.$inc);
}
else{
    require_once(STAFFINC_DIR.'header.inc.php');
    require_once(STAFFINC_DIR.$inc);
    print $response_form->getMedia();
    require_once(STAFFINC_DIR.'footer.inc.php');
}
