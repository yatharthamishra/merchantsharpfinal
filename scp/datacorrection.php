<?php

require('staff.inc.php');
require_once(INCLUDE_DIR . 'class.ticket.php');
require_once(INCLUDE_DIR . 'class.taskflow.php');
require_once(INCLUDE_DIR . 'class.dept.php');
require_once(INCLUDE_DIR . 'class.filter.php');
require_once(INCLUDE_DIR . 'class.canned.php');
require_once(INCLUDE_DIR . 'class.json.php');
require_once(INCLUDE_DIR . 'class.dynamic_forms.php');
require_once(INCLUDE_DIR . 'class.export.php');       // For paper sizes
require_once(INCLUDE_DIR . 'class.orderStatusCron.php');
require_once(INCLUDE_DIR . 'class.tracking.php');

if ($_GET["order_id"]) {
    $ticketOData = TicketOData::objects()->filter(array("order_id" => 0));
    foreach ($ticketOData as $odata) {
        $ticketId = $odata->ticket_id;
        $order_id = DynamicFormEntryAnswer::objects()->filter(array('entry__object_id' => $ticketId, 'entry__object_type' => 'T', 'field__name' => 'order_id'))->values("value")->all()[0]["value"];
        if ($odata->order_id == 0) {
            $odata->order_id = $order_id;
            $odata->save();
            echo $ticketId . "\n";
        }
    }
}

if ($_GET["odata"]) {
    $ticketObj = Ticket::objects()->filter(array("odata__order_id__isnull" => 1));
    foreach ($ticketObj as $tdata) {
        $ticketodata = TicketOData::objects()->filter(array("order_id__isnull" => 1, "ticket_id" => $tdata->ticket_id));
        if ($ticketodata->count() < 1) {
            $odata = TicketOData::create();
        } else {
            $odata = $ticketodata[0];
        }
        echo $ticketObj->count()."\n";
        $ticketId = $tdata->ticket_id;
        $order_id = DynamicFormEntryAnswer::objects()->filter(array('entry__object_id' => $ticketId, 'entry__object_type' => 'T', 'field__name' => 'order_id'))->values("value")->all()[0]["value"];
        $subject = DynamicFormEntryAnswer::objects()->filter(array('entry__object_id' => $ticketId, 'entry__object_type' => 'T', 'field__name' => 'subject'))->values("value")->all()[0]["value"];
        if ($odata->order_id == 0) {
            $odata->ticket_id = $ticketId;
            $odata->order_id = $order_id?:0;
            $odata->subject = $subject;
            $odata->priority = 2;
            $odata->save();
            echo "(".$ticketId.','.$odata->order_id.",'".$subject."',".$odata->priority."),";
            if($limit>$_GET["limit"]){
                die("LIMIT");
            }
        }
    }
}