<?php

/* 
 * Taskflow admin setting
 * @author - Akash Kumar
 */

require('admin.inc.php');
include_once(INCLUDE_DIR.'class.template.php');

$page='orderflow.inc.php';
$nav->setTabActive('manage');
if($_POST)
{   global $thisstaff;
    $remove=0;
    foreach($_POST as $k=>$check){
        if(stristr($k,'remove')){
            $id = explode("-",$k)[1];
            $remove=1;
            $orderFlowDisable=OrderFlow::objects()->filter(array("orderflow_id"=>$id,"flowstatus"=>1))->first();
         UtilityFunctions::flowLogs('mst_order_flow',$id,0,$thisstaff->ht['staff_id'],1,'orderflow removed',null);   
             $orderFlowDisable->flowstatus=0;
            $orderFlowDisable->save();
            $msg = "Order Flow removed Successfully";
        }
    }
    if(!$remove){
        $type = $_POST["type"];
        $status = $_POST[$type."Status"];
        $taskType = $_POST["taskType"];
        $dept = $_POST["taskDept"];
        $response = $_POST["response"];
        $communication = $_POST["communication"];
        $refundStatus = $_POST["combinedRefundStatus"];
        
        $numRows = OrderFlow::objects()->filter(array("status_type"=>$type,"status"=>$status,"dept"=>$dept,"task_type"=>$taskType,"task_response"=>$response,"communication"=>$communication,"flowstatus"=>1))->count();
        if($numRows<1){
            $FlowObj = OrderFlow::create();
            $FlowObj->status_type = $type;
            $FlowObj->status = $status;
            $FlowObj->refund_status = $refundStatus;
            $FlowObj->dept = $dept;
            $FlowObj->task_type = $taskType;
            $FlowObj->task_response = $response;
            $FlowObj->communication = $communication;
            $FlowObj->save();
            $flow_id=$FlowObj->ht['orderflow_id'];
        UtilityFunctions::flowLogs('mst_order_flow',$flow_id,json_encode($FlowObj->ht),$thisstaff->ht['staff_id'],null,'orderflow added',null);
            $msg = "Successfully Added";
        }else{
         $msg = "Order attribute combination already exists";   
        }
    }
}
if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
   require(STAFFINC_DIR.$page);
}
else{
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
}
?>
