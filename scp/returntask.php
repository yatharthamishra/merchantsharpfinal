<?php

/*
 * Taskflow admin setting
 * @author - Akash Kumar
 */
require('admin.inc.php');
include_once(INCLUDE_DIR . 'class.template.php');

$page = 'returntask.inc.php';
$nav->setTabActive('manage');
if ($_POST) {
    $remove = 0;
    foreach ($_POST as $k => $check) {
        if (stristr($k, 'remove')) {
            $id = array_filter(explode(",",explode("-", $k)[1]));
            $remove = 1;
            ReturnTaskAssign::objects()->filter(array("flow_id__in" => $id))->delete();

            $msg = "Return Task Assignment removed Successfully";
        }
    }
    if (!$remove) {
        $return_status = $_POST["return_status"];
        $issue_id = $_POST["issue_id"];
        $sub_issue_id = $_POST["sub_issue_id"];
        if (empty($issue_id))
            $msg = "Return Status Selection is mandatory";
        else {
            $any = array(0);
            $sub_sub_issue_id = $_POST["sub_sub_issue_id"]?:0;
            $merchant_typeArr = in_array('0', $_POST["merchant_type"]) ? $any : $_POST["merchant_type"];
            $valueArr = in_array('0', $_POST["valueRange"]) ? $any : $_POST["valueRange"];
            $categoryArr = in_array('0', $_POST["category"]) ? $any : $_POST["category"];
            $return_statusArr = in_array('0', $_POST["return_status"]) ? $any : $_POST["return_status"];
            $sub_reasonArr = in_array('0', $_POST["sub_reason"]) ? $any : $_POST["sub_reason"];
            $return_actionArr = in_array('0', $_POST["return_action"]) ? $any : $_POST["return_action"];

            $dept_id = $_POST["taskDept"];
            $task_type = $_POST["taskType"];
            
            foreach ($return_statusArr as $return_status) {
                foreach ($merchant_typeArr as $merchant_type) {
                    foreach ($valueArr as $value) {
                        foreach ($categoryArr as $category) {
                            foreach ($sub_reasonArr as $sub_reason) {
                                foreach ($return_actionArr as $return_action) {

                                    $value_min = explode("-", $value)[0];
                                    $value_max = explode("-", $value)[1];

                                    $numRows = ReturnTaskAssign::objects()->filter(array(
                                                "issue_id" => $issue_id,
                                                "sub_issue_id" => $sub_issue_id,
                                                "sub_sub_issue_id" => $sub_sub_issue_id,
                                                "merchant_type" => $merchant_type,
                                                "category" => $category,
                                                "return_status" => $return_status,
                                                "value_min" => $value_min,
                                                "value_max" => $value_max,
                                                "sub_reason" => $sub_reason,
                                                "return_action" => $return_action,
                                                "dept" => $dept_id,
                                                "task_type" => $task_type))->count();
                                    if ($numRows < 1) {
                                        $FlowObj = ReturnTaskAssign::create();
                                        $FlowObj->issue_id = $issue_id;
                                        $FlowObj->sub_issue_id = $sub_issue_id;
                                        $FlowObj->sub_sub_issue_id = $sub_sub_issue_id;
                                        $FlowObj->merchant_type = $merchant_type;
                                        $FlowObj->category = $category;
                                        $FlowObj->return_status = $return_status?:'0';
                                        $FlowObj->value_min = $value_min;
                                        $FlowObj->value_max = $value_max;
                                        $FlowObj->sub_reason = $sub_reason?:'0';
                                        $FlowObj->return_action = $return_action;
                                        $FlowObj->dept = $dept_id;
                                        $FlowObj->task_type = $task_type;
                                        $FlowObj->save();
                                        $msg = " Successfully Updated<br>";
                                    } else {
                                        $msg = " Return attribute combination already exists<br>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
require(STAFFINC_DIR . 'header.inc.php');
require(STAFFINC_DIR . $page);
include(STAFFINC_DIR . 'footer.inc.php');
?>
