var check=1;    //this decides whether we should open the dialog box when logged out

/*
function to create the redirect url and return to the same url after login 

*/
function find_redirect_url(current_url,redirect_url)
{
    current_url = current_url.split("/").pop();
    current_url=encodeURIComponent(current_url);
    redirect_url+=current_url;
    return redirect_url;
}

/*
function to create dialog boxes when the user gets logged out from another window

*/
function UIchangeswhenloggedout(){
    if(check==1)
    {
        $("#overlay").css("display","block");
        if(!$("#login-msg").length)
        {
            var html="<div id='login-msg'>You need to login again.</div>";
            $("body").append(html);
        }
        $("#login-msg").dialog({
            title:"Logged out",
            open: function(event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
            modal:true,
            dialogClass: "dialog",
            buttons: [
                {
                    text: "OK",
                    click: function() {
                        
                        var current_url=window.location.href;
                        var final_url=find_redirect_url(current_url,redirect_url);
                        
                        var win=window.open(final_url,'_blank');
                        if(win)
                        {
                            check=0;
                            win.focus();
                        }
                        else
                        {
                            alert('Please allow Popups for this website');
                                
                        }
                    }
                }
            ]
        });
    }
}


/*
timer to call ajax call to check login status

*/
$(document).ready(function(){
    var temp=time;
    var timer=setInterval(function () {  
    	$.ajax({
    	type:"POST",
    	url:"ajax.php/login/validate-agent",


    	success: function(data){
            if(locallogin==0 && data.code==0)
            {
                temp=time;
                UIchangeswhenloggedout();
            }
            else
            {
                if($("#login-msg").length && $("#login-msg").dialog("isOpen")){
                    $("#login-msg").dialog('close');
                    $("#overlay").css("display","none");
                }
                check=1;
                temp=5*time;
            }
          
    	},
        beforeSend: function(){
        	
        },

        error: function(){
            //UIchangeswhenloggedout();
        }

      	});

    }, temp);
});
