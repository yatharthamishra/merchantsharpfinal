<?php

/* 
 * Taskflow admin setting
 * @author - Meghraj
 */

require('admin.inc.php');
include_once(INCLUDE_DIR.'class.template.php');
require_once('../include/class.taskmapping.php');
$issueSubIssueDictionary=getTopicDetailDictionary();
$page='productMapping.inc.php';
$nav->setTabActive('manage');
if($_POST)
{   global $thisstaff;
    $remove=0;
    foreach($_POST as $k=>$check){
        if(stristr($k,'remove')){
            $id = explode("-",$k)[1];
            $remove=1;
            $taskDisable=TaskMapping::objects()->filter(array("id"=>$id,"task_status_id"=>1,"entity_type"=>"product"))->first();
            UtilityFunctions::flowLogs('mst_first_task',$id,0,$thisstaff->ht['staff_id'],1,'task removed',null);   
            $taskDisable->task_status_id=0;
            $taskDisable->save();
            $msg = "Task removed Successfully";
        }
    }
    if(!$remove){
        $topicId = $_POST["topicId"];
        $taskType = $_POST["taskType"];
        $productStatus = $_POST["productStatus"];
        $dept = $_POST["taskDept"];
        $batchUploadStatus=$_POST['batchUploadStatus'];
        //$batchUpdateStatus=$_POST['batchUpdateStatus'];
	if(in_array(-1, $productStatus)){
            $productStatus = array(-1);
        }
        $res = TaskMapping::objects()->filter(array("topic_id"=>$topicId,"product_status__in"=>$productStatus,
            "dept_id"=>$dept,
            "entity_type"=>"product",
            "batch_upload_status"=>$batchUploadStatus,
            //"batch_update_status"=>$batchUpdateStatus,
            "task_type_id"=>$taskType,
            "task_status_id"=>1,
            "entity_type"=>"product"))->values('product_status')->all();
        foreach ($res as $value) {
            $productStatusAlreadyPresent[] = $value['product_status'];
        }
        $present = 0;
        $notPresent = 0;
        foreach ($productStatus as $val) {
            if(!in_array($val, $productStatusAlreadyPresent)) {
            $FlowObj = TaskMapping::create();
            $FlowObj->topic_id = $topicId;
            $FlowObj->product_status = $val;
            $FlowObj->dept_id = $dept;
            $FlowObj->entity_type = "product";
            $FlowObj->batch_upload_status = $batchUploadStatus;
            //$FlowObj->batch_update_status = $batchUpdateStatus;
            $FlowObj->task_type_id = $taskType;
            $FlowObj->task_status_id = 1;
            $FlowObj->save();
            $flow_id=$FlowObj->ht['id'];
            UtilityFunctions::flowLogs('mst_first_task',$flow_id,json_encode($FlowObj->ht),$thisstaff->ht['staff_id'],null,' new task added',null);
            $notPresent++;
            }else{
                $present++;  
            }
        }
	if(!empty($notPresent)){
            $msg = $notPresent." Successfully Added ";
        }
        if(!empty($present)){
            $msg .= $present." Task attribute combination already exists.";
        }
    }
}
if($_GET['EXPORT'] && $_GET['EXPORT']=='exportTrue'){
    header("Content-Type: text/csv");
    $fileName = 'TicketCSVData';
    header("Content-Disposition: attachment; filename=$fileName.csv");
    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); 
   require(STAFFINC_DIR.$page);
}
else{
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
}
?>
