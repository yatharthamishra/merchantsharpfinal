<?php

function soa_get_access_token() 
{
    $login_api_url = "https://auth.shopclues.com/loginToken.php";
    $soa_token_user = "devtest@shopclues.com";
    $soa_token_password = "5f8b62a2dced0cd28946a9c891ff3e5e";
    $client_id = "10945358";
    $client_secret = "KM1LCWRT8E6P";
    $authorization = '';        
    if (isset($_SESSION['soa_cst_access_token']) && isset($_SESSION['soa_cst_access_token_type']) && isset($_SESSION['soa_cst_access_token_expiry']) && time() < $_SESSION['soa_cst_access_token_expiry'])
        $authorization = $_SESSION['soa_cst_access_token_type'] . ' ' . $_SESSION['soa_cst_access_token'];
    else 
    {   
        //set_include_path();
        ini_restore('include_path');
        require_once 'HTTP/Request2.php';
        $request = new HTTP_Request2($login_api_url, HTTP_Request2::METHOD_POST);        
        $request->setConfig(array(
            'connect_timeout' => 30,
            'timeout' => 30,
            'follow_redirects' => TRUE,
            'ssl_verify_peer' => FALSE,
            'ssl_verify_host' => FALSE,
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
        ));
        $request->addPostParameter(array(
            'username' => $soa_token_user,
            'password' => $soa_token_password,
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'password',
        ));        
        try 
        {
            $response = $request->send(); 
            //var_dump($response);
            if (200 == $response->getStatus()) 
            {
                $access_token_object = json_decode($response->getBody());                                
                if (is_object($access_token_object) && isset($access_token_object->access_token)) 
                {
                    $_SESSION['soa_cst_access_token'] = $access_token_object->access_token;
                    $_SESSION['soa_cst_access_token_expiry'] = time() + $access_token_object->expires_in;
                    $_SESSION['soa_cst_access_token_type'] = $access_token_object->token_type;
                    $authorization = $_SESSION['soa_cst_access_token_type'] . ' ' . $_SESSION['soa_cst_access_token'];
                }
            }
            else 
                error_log("\nBAZOOKA START Improper status from token API. API response is: " . json_encode($response) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");            
        }
        catch (HTTP_Request2_Exception $e) 
        {
            error_log("\nBAZOOKA START Exception while calling token API. Exception is: " . json_encode($e) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");
        }
    }
    return $authorization;
}


function getOrderInfo($orderId,$token, $text = "") {
    $soa_api_url = "http://developer.shopclues.com/api/";
     ini_restore('include_path');
    require_once 'HTTP/Request2.php';
    if($text=="")
    {
        $url_end = "order/" . $orderId;
    }
    else
    {
        $url_end = $text;
    }
    $request = new HTTP_Request2($soa_api_url . $url_end, HTTP_Request2::METHOD_GET);
    $request->setConfig(array(
        'connect_timeout' => 30,
        'timeout' => 30,
        'follow_redirects' => TRUE,
        'ssl_verify_peer' => FALSE,
        'ssl_verify_host' => FALSE,
    ));      
    $request->setHeader(array(
        'Content-Type' => 'application/json',
        'Authorization' => $token
    ));       
    try 
    {          
        $response = $request->send();  
        if (200 == $response->getStatus()) 
        {
            $orderData = json_decode($response->getBody());  
            return $orderData;
            //foreach ($orderData['data'] as $key => $value) {
                
            //}
        }
        else 
            error_log("\nBAZOOKA START Improper status from SOA API. API response is: " . json_encode($response) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");            
    }
    catch (HTTP_Request2_Exception $e)
    {
        error_log("\nBAZOOKA START Exception while calling SOA API. Exception is: " . json_encode($e) . "ms\nBAZOOKA END\n", 3, "/tmp/bazooka.log");
    }     
}
?>