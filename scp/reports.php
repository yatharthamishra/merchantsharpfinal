<?php

/* 
 * Taskflow admin setting
 * @author - Akash Kumar
 */
ini_set('memory_limit','256M');
require_once('staff.inc.php');
require_once(INCLUDE_DIR.'class.cron.php');
//Cron::sendAutoMailForManifestPickUp();die('hey');
include_once(INCLUDE_DIR.'class.template.php');
include_once (STAFFINC_DIR.'report/reportConfig.php');

$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.reports" />',
    "$('#content').data('tipNamespace', 'dashboard.reports');");
if($_GET["report"])
{   
    $page = $_GET["report"].".inc.php";
    $displayPage = "display.inc.php";
    
}else{
    $reportList=array("Ticket Inflow","Total Closed Tickets","Total Answered Tickets","Total Triaged Tickets","Repeat SLA updated Tickets","CS Report");
    $page='reports.inc.php';
}
global $thisstaff;
require_once(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.'report/'.$page);
if($displayPage && $thisstaff->hasPerm(SearchBackend::PERM_VIEW_REPORT))
    require(STAFFINC_DIR.'report/'.$displayPage);
require_once(STAFFINC_DIR.'footer.inc.php');

?>
