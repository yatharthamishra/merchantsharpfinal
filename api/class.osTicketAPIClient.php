<?php
#
# Class to make API osticket call 

Class osTicketAPIClient {

    var $api_url = '';
    var $api_key = '';
    var $api_function = '';
//    public $data = array(
//        'name' => 'John Doe', // from name aka User/Client Name
//        'email' => 'john@gmail.com', // from email aka User/Client Email
//        'phone' => '1234567890', // phone number aka User/Client Phone Number
//        'subject' => 'Test API message from class ', // test subject, aka Issue Summary
//        'message' => 'This is a test of the osTicket API', // test ticket body, aka Issue Details.
//        'ip' => '', // $_SERVER['REMOTE_ADDR'], // Should be IP address of the machine thats trying to open the ticket.
//        'topicId' => '1', // the help Topic that you want to use for the ticket 
//        //'Agency'  =>      '58', //this is an example of a custom list entry. This should be the number of the entry.
//        //'Site'    =>      'Bermuda'; // this is an example of a custom text field.  You can push anything into here you want. 
//        'attachments' => array()
//    );
    

    public $data = array(
        'name' => 'NavneetStore', // from name aka User/Client Name
        'email' => 'navneet.singh@shopclues.com', // from email aka User/Client Email
        'phone' => '1234567890', // phone number aka User/Client Phone Number
        'subject' => 'Ticket creation test', // test subject, aka Issue Summary
        'message' => 'Ticket cretaion test body', // test ticket body, aka Issue Details.
        'ip' => '', // $_SERVER['REMOTE_ADDR'], // Should be IP address of the machine thats trying to open the ticket.
        'topicId' => '20', // the help Topic that you want to use for the ticket 
        'order_ids'=>'89292700,892909973331113038111122112288661',
        'merchant_id'=>'111',
        'courierName'=>'couriername',
        'trackingNo'=>1111,
        'attachments' => '{"order":{"89292700":{"path":"\/images\/MerchantTicket\/G\/G_Zx5FgM_99VR3cEYoF-MOo7CUGBV03U.png","type":"image\/jpg","size":"123324"},"111":{"path":"\/images\/MerchantTicket\/G\/G_Zx5FgM_99VR3cEYoF-MOo7CUGBV03U.png","type":"image\/jpg","size":"123324"}}}'
    );
    
    // If 1, display things to debug.
    private $debug = "1";

    public function __construct($osticket_api_key, $osticket_api_url,$perform='') {
        $this->api_url = $osticket_api_url;
        $ticketData=$_POST['ticketData'];
        $this->api_key = $osticket_api_key;
        $this->api_function = $perform;
        $this->data=  json_decode($ticketData, true);
    }

    public function osTicketAPI($ticket_data) {
        if ($this->IsNullOrEmptyString($this->api_key) || ($this->api_key === 'PUTyourAPIkeyHERE')) {
            echo "<p style=\"color:red;\"><b>Error: No API Key</b><br>You have not configured this script with an API Key!</p>";
            echo "<p>Please log into osticket as an admin and navigate to: Admin panel -> Manage -> Api Keys then add a new API Key.<br>";
            echo "Once you have your key edit this file " . __FILE__ . " and add the key at line 19.</p>";
            die();
        }
        function_exists('curl_version') or die('CURL support required');
        function_exists('json_encode') or die('JSON support required');
        $url = $this->api_url.$this->api_function;
        #set timeout
        set_time_limit(30);
        #curl post
        $ch = curl_init();
        if(empty($ticket_data)){
            $ticket_data=  $this->data;
        
}
curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ticket_data));
        curl_setopt($ch, CURLOPT_USERAGENT, 'osTicket API Client v1.8');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:', 'X-API-Key: ' . $this->api_key));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

if(curl_error($ch))
        {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if($code == 201){
            $ticket_id = (int) $result;
            return $ticket_id;
        }else{
            return $result;
        }
    }

# Continue onward here if necessary. $ticket_id has the ID number of the
# newly-created ticket

    function IsNullOrEmptyString($question) {
        return (!isset($question) || trim($question) === '');
    }

}
//$obj=new osTicketAPIClient('556F6997F02BBACDFA3F0E794181B110','http://newost.shopclues.com/api/http.php/tickets.json');
//$result=$obj->osTicketAPI($data);
//echo $result;
