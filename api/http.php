<?php
/*********************************************************************
    http.php

    HTTP controller for the osTicket API

    Jared Hancock
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
// Use sessions — it's important for SSO authentication, which uses
// /api/auth/ext
define('DISABLE_SESSION', false);
ini_set('display_errors', '0');
require 'api.inc.php';

# Include the main api urls
require_once INCLUDE_DIR."class.dispatcher.php";
$logConfig = logConfig();
$domain = $logConfig["domain"]["D1"];
$module = $logConfig["module"][$domain]["D1_M1"];
$error = $logConfig["error"]["E16"];
$logObj = new Analog_logger();
$logObj->report($domain, $module, $logConfig["level"]["INFO"], $error, "title:API CALL", "data:".json_encode($_REQUEST), "", "", "");
$dispatcher = patterns('',
        url_post("^/tickets\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','create')),
        url_post("^/ticketsupdate\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','update')),
        url_post("^/ticketCommunication\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','communication')),
        url_post("^/addThreadOnTicket\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','addThread')),
        url_post("^/checkTicketStatus\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','isTktStatusClosed')),
        url_post("^/createTaskForTicket\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','createTaskForTicket')),
        url_post("^/addMessage\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','postMessage')),
        //Task API Akash
	url_get("^/ticketComments\.(?P<format>xml|json|email)$", array('api.tickets_comments.php:TicketCommentApiController','fetch')),
        url_get("^/taskMoreInfo\.(?P<format>xml|json|email)$", array('api.tasks.php:TaskApiController','getMoreInfo')),
        url_post("^/taskMoreInfo\.(?P<format>xml|json|email)$", array('api.tasks.php:TaskApiController','postMoreInfo')),
        url_post("^/taskupdate\.(?P<format>xml|json)$", array('api.tasks.php:TaskApiController','update')),
        url_post("^/internalnote\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','postNote')),
        url_get("^/ticketList\.(?P<format>xml|json)$", array('api.tickets.php:TicketApiController','tickets')),
        url_get("^/ticket/(?P<number>\d+)$", array('api.tickets.php:TicketApiController','getTicket')), 
        url_get("^/helptopic\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getTopics')),
        url_get("^/thread\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getThreads')),
        url_get("^/mandatory\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','getMandatoryInfo')),
        url('^/tasks/', patterns('',
                url_post("^cron$", array('api.cron.php:CronApiController', 'execute'))
         ))
        );

Signal::send('api', $dispatcher);

# Call the respective function
print $dispatcher->resolve($ost->get_path_info());
?>
