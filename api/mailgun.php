<?php
@chdir(dirname(__FILE__) . '/'); //Change dir.
require('api.inc.php');
global $cfg;
// print ;exit;
date_default_timezone_set('Asia/Kolkata');
$mail['mid'] = db_input($_POST['Message-Id'], false);
$MainAlreadyExist = db_result(db_squery("SELECT mid from `" . TABLE_PREFIX . "thread_entry_email` WHERE `mid`='" . $mail["mid"] . "'"));
if (!empty($MainAlreadyExist)) {
    exit();
}
$to_array = explode('@', $_POST['To']);
$to_email_first_part = current($to_array);
$ticket_number = str_replace('Shopclues Helpdesk <helpdesk+id', '', $to_email_first_part);
if(empty($ticket_number) || stristr($_POST['To'],'pdesk@support.shopclues.com')){
    $ticket_number = explode(']',explode('[#',$_POST['subject'])[1])[0];
}
if($ticket_number<1000){
    $ticket_number = explode(']',explode('[#',$_POST['body-plain'])[1])[0];
}
if($ticket_number<1000){
    $ticket_number = explode(']',explode('[#',$_POST['stripped-text'])[1])[0];
}
$_POST['stripped-text'] = str_replace('From: Shopclues Helpdesk','\n\n\nFrom: Shopclues Helpdesk',$_POST['stripped-text']);

global $cfg;
$sendAllEmailsToAll = $cfg->config['testEnvironmentEmailId']['value'];
$sendAllEmailsTo = array_filter(explode(",",$sendAllEmailsToAll));
if (in_array($_POST['sender'], $sendAllEmailsTo)) {
    $customer_uid = db_result(db_query('SELECT user_id FROM `' . TABLE_PREFIX . 'ticket` WHERE number = "' . db_input($ticket_number, false) . '"'));
} elseif(is_numeric ($ticket_number) && $ticket_number>0) {
    if (empty($_POST['from']) || empty($ticket_number) || empty($_POST['subject']) ||
            empty($_POST['body-plain']) || empty($mail['mid'])) {
        exit();
    }

    $customer_uid = db_result(db_squery('SELECT user_id FROM `' . TABLE_PREFIX . 'user_email` WHERE address = "' . db_input($_POST['sender'], false) . '"'));

    
}else{
    include_once 'class.osTicketAPIClient.php';
    $escalationDeskEmailKey = json_decode($cfg->config['escalationDeskEmailKey']['value'],true);
    $emailIdToStringArray=  getEmailIdFromString($_POST['To']);
    $emailIdToString="";
    foreach ($emailIdToStringArray as $emailToString){
        if(isset($escalationDeskEmailKey[$emailToString]['create'])){
            $emailIdToString=$emailToString;
            $emailIdToString=preg_replace('/\s+/', '', $emailIdToString);
            break;
        }
    }
    $emailIdFromString=getEmailIdFromString($_POST['from']);
    $emailIdFromString=$emailIdFromString[0];
    if($escalationDeskEmailKey[$emailIdToString]['create']){
        $data = array(
            'name'      =>      preg_replace('/@.*?$/', '', $_POST['sender']),  // from name aka User/Client Name
            'email'     =>      $emailIdFromString,  // from email aka User/Client Email
            'subject'   =>      $escalationDeskEmailKey[$emailIdToString]["Subject"],
            'message'   =>      "Subject - ".$_POST['subject']."\n".$_POST['stripped-text'],
            'deptId'    =>      $escalationDeskEmailKey[$emailIdToString]["deptId"],
            'issue_id'   =>      $escalationDeskEmailKey[$emailIdToString]["issue"], 
            'order_id'  => '150000',
            'escalation' => $escalationDeskEmailKey[$emailIdToString]['autotriage']
        );
     $osticket_api_url = $cfg->config['helpdesk_url']['value'].'/api/http.php/tickets.json';
     $osticket_api_key= $escalationDeskEmailKey[$emailIdToString]["key"];
     $objTicket = new osTicketAPIClient($osticket_api_key,$osticket_api_url);
     $ticket_id = $objTicket->osTicketAPI($data);
     die;
    }
    //Create Ticket
}

$customer_name = db_result(db_query('SELECT name FROM `' . TABLE_PREFIX . 'user` WHERE id = ' . $customer_uid));

$ticket_id = db_result(db_query('SELECT ticket_id FROM `' . TABLE_PREFIX . 'ticket` WHERE number = "' . db_input($ticket_number, false) . '" AND user_id = ' . $customer_uid));

if (empty($ticket_id)) {

    $userCount = CscartUsers::objects(array("email"=>db_input($_POST['sender'], false)))->count();
    if($userCount>0){
        $orderCount = CscartOrders::objects(array("email"=>db_input($_POST['sender'], false)))->count();
            if($orderCount>0){
            $recipient = db_result(db_squery('SELECT u.name FROM `' . TABLE_PREFIX . 'user` u LEFT JOIN `' . TABLE_PREFIX . 'user_email` e ON (u.id = e.user_id) WHERE e.address = "' . db_input($_POST['sender'], false) . '"'));
            if(empty($recipient))
                $recipient="User";
            $templateclass=new EmailTemplateGroup(1);
            $template=$templateclass->getInvalidEmailAlertMsgTemplate();
            if(is_object($template))
            {
                $replacer = new VariableReplacer();
                $replacer->assign(array('recipient'=> $recipient));
                //$replacer->assign(array('ticket'=>$ticket_number));
                $msg = $replacer->replaceVars($template->asArray());
                $email =new Email;
                $email->send($_POST['sender'], $msg['subj'], $msg['body'], null, null);
            }
        }
    }
    exit();
}
/**
 * Code for changing status from solved to reopen status .Creates logs in mst_ticket_reopen_logs for
 * every reopen status .
 */
$ticketObject=Ticket::lookup($ticket_id);
$ticketStatus=$ticketObject->getState();
$cachedFields=getCachedTicketFields();
$statusIdMap=$cachedFields['statusIdMap'];
switch ($ticketStatus){
    case 'solved':
        $newTicket = $ticketObject->setStatus($statusIdMap['reopened']);
        //solved case
        break;
    case 'closed':
        //closed case 
        $newTicket = Ticket::cloneTicket($ticket_id,$_POST['stripped-text']);
	if($newTicket){
		$ticket_id=$newTicket;
	}
        break;
}


$thread_id = db_result(db_query('SELECT id FROM `' . TABLE_PREFIX . 'thread` WHERE object_id = ' . $ticket_id));

db_query('INSERT INTO `' . TABLE_PREFIX . 'thread_entry` (`pid`, `thread_id`, `staff_id`, `user_id`, 
         `type`, `flags`, `poster`, `editor`, `editor_type`, `source`, `title`, `body`, `format`, 
         `ip_address`, `created`, `updated`) VALUES (0, ' . $thread_id . ', 0, ' . $customer_uid . ', 
         "M", 0, "' . $customer_name . '", "", "", "Email", "' . db_input($_POST['subject'], false) . '", "' . db_input(nl2br($_POST['stripped-text']), false) . '", "html", "", "' . date('Y-m-d H:i:s') . '", "' . date('Y-m-d H:i:s') . '")');

$thread_entry_id = db_insert_id();

db_query('INSERT INTO `' . TABLE_PREFIX . 'thread_entry_email` (`thread_entry_id`, `mid`, `headers`) VALUES (' . $thread_entry_id . ', "' . $mail['mid'] . '",  "' . db_input($_POST['message-headers'], false) . '")');
$sql = "Update " . TRIAGE_TABLE . " SET `triage`=3,`triage_again`=NOW() WHERE `ticket_id`=" . $ticket_id . " AND `triage`!=1";
$res = db_query($sql);

db_query('UPDATE  `' . TICKET_TABLE . '` set `user_thread_count`=`user_thread_count`+1, `last_response_customer`=NOW() WHERE ticket_id = ' . $ticket_id);

$ccgTasks = Task::objects()->filter(array("flags"=>1,"object_id"=>$ticket_id,"dept_id"=>$cfg->config['deptToAlertWithTriageAgain']['value']?:0));
foreach($ccgTasks as $tasks){
    $updationData = array("taskId" => $tasks->getId(), "taskDue" => date("Y-m-d H:i:s",time()+$cfg->config['TimeToTriageAlert']['value']), "taskAgent" => "On Customer Reply", "email" => "system.auto@shopclues.com");
    $tasks->updateTask($updationData);
}

/**
 * UPLOADING ATTACHMENTS
 * @author - Akash Kumar
 */
global $cfg;
$emailAttachmentConfig = $cfg->config['EmailAttachment']['value'];
//$emailAttachmentConfig= db_result(db_query('SELECT value FROM `'.TABLE_PREFIX.'config` WHERE `key` = "EmailAttachment"'));
$emailAttachmentConfig = json_decode($emailAttachmentConfig, true);

$moveUploadFileTries=$cfg->config['move_upload_file_tries']['value'];
$rsyncNumberOfTries=$cfg->config['rsync_number_of_tries']['value'];
$enableAttachmentLogging=$cfg->config['enable_email_attachment_logging']['value'];
$enableAttachmentContentLogging=$cfg->config['enable_email_attachment_content_logging']['value'];
$count = 1;
$AttachmentRsyncConfig = json_decode($cfg->config['AttachmentRsync']['value'], true);
$parameter = $AttachmentRsyncConfig["parameter"];
$local = $AttachmentRsyncConfig["local"];
$remote = $AttachmentRsyncConfig["remote"];

foreach ($_FILES as $Att) {
    $fileSize=$Att['size'];
    $fileType=$Att['type'];
    $fileName=$Att['name'];
    $fileTmpPath=$Att['tmp_name'];
    if($enableAttachmentLogging){
    $sql="insert into mst_mail_attachment_logs (ticket_id,user_id,file_tmp_path,email_id,file_name,file_size,file_type,status_id) 
            values ('$ticket_id','$customer_uid','$fileTmpPath','$emailIdFromString','$fileName', '$fileSize', '$fileType','0')";
    db_query($sql);
    $mailAttachmentInsertId=db_insert_id();
    }
    
    //To check for maximum Upload
    if ($count > $emailAttachmentConfig["maxNumber"]){
        if($enableAttachmentLogging){
        $sql="update mst_mail_attachment_logs set declined_reason='attachment count exceeded' where id='$mailAttachmentInsertId'";
        db_query($sql);
        }
        break;
    }
    if ($Att["size"] < $emailAttachmentConfig["maxSize"] && !in_array($Att["type"], $emailAttachmentConfig["type"]) && !in_array(mime_content_type($Att["tmp_name"]),$emailAttachmentConfig["mimeType"])) {
        
//Email Attachment Fetching Fix
       $up=$Att;
       if($cfg->config['EmailTesting']['value']){
        $exten= explode('.', $Att["name"]);
       $countExtension=count($exten);
       if(strlen($exten[$countExtension-1])>2)
            $FileExt=$exten[$countExtension-1];
       else {
           $exten=  explode('/', $up["type"]);
           $FileExt=$exten[1];
       }   
               }
       else
       { 
         $FileExt= str_replace("image/", "", $up["type"]);
       }
            
        $count++;
        $key = 'EMail' . time() . rand(1, 99999);
         $fileName = $key . rand(1, 5000) . '.' . $FileExt;
        $signature = md5('signature');
        //file_put_contents('./test.log', print_r($Att, true)."\n\n\n\n\n", FILE_APPEND);
        $year = date("Y");
        $month = date("m");
        $day = date("d");

        
        $filePath = $year . '/' . $month . '/' . $day . '/' . $fileName;
        $thumbWidth = 100;
        $thumbHeight = 100;
        /**
         * To prepare Images and its thumbnail
         * 
         */
        $uploadDir = '/uploads/image/';
        $output_file = INCLUDE_DIR . '..' . $uploadDir . $filePath;
        if (!file_exists(INCLUDE_DIR . '..' . $uploadDir . '' . $year))
            mkdir(INCLUDE_DIR . '..' . $uploadDir . '' . $year, 0777, true);

        if (!file_exists(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month))
            mkdir(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month, 0777, true);

        if (!file_exists(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month . '/' . $day))
            mkdir(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month . '/' . $day, 0777, true);
        //file_put_contents('./test.log', print_r($output_file, true)."\n\n\n\n\n", FILE_APPEND);
        if($enableAttachmentLogging){
        $sql="update mst_mail_attachment_logs set file_upload_path='$output_file' where id='$mailAttachmentInsertId'";    
        db_query($sql);
        }
        $moveUploadFileStatus=move_uploaded_file($up['tmp_name'], $output_file);
        while($moveUploadFileTries>0 && !$moveUploadFileStatus){
            // try moving again 
            $moveUploadFileStatus=move_uploaded_file($up['tmp_name'], $output_file);
            $moveUploadFileTries--;
        };
        if(!$moveUploadFileStatus){
            // move content to db 
            $fp      = fopen($fileTmpPath, 'r');
            $content = fread($fp, filesize($fileTmpPath));
            $content = addslashes($content);
            fclose($fp);
            if($enableAttachmentLogging && $enableAttachmentContentLogging){
            $sql="update mst_mail_attachment_logs set declined_reason='attachment move upload error',content='$content' where id='$mailAttachmentInsertId'";    
            db_query($sql);
            }
        }
        
        if (!stristr($Att["type"], "pdf") && $emailAttachmentConfig["thumbnail"]) {
            /**
             * Making Thumbnail
             */
            $im = imagecreatefromstring(file_get_contents($output_file));

            // Get width and height of original image resource
            $origWidth = imagesx($im);
            $origHeight = imagesy($im);

            // Create new destination image resource for new 24 x 24 image
            $imNew = imagecreatetruecolor($thumbWidth, $thumbHeight);

            // Re-sample image to smaller size and display
            imagecopyresampled($imNew, $im, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);
            ob_start();

            imagejpeg($imNew);
            $thumbnailData = ob_get_contents();
            ob_end_clean();

            $uploadDir = '/uploads/thumbnail/';
            $thumbnail_file = INCLUDE_DIR . '..' . $uploadDir . $filePath;
            if (!file_exists(INCLUDE_DIR . '..' . $uploadDir . '' . $year))
                mkdir(INCLUDE_DIR . '..' . $uploadDir . '' . $year, 0777, true);

            if (!file_exists(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month))
                mkdir(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month, 0777, true);

            if (!file_exists(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month . '/' . $day))
                mkdir(INCLUDE_DIR . '..' . $uploadDir . '' . $year . '/' . $month . '/' . $day, 0777, true);
            //file_put_contents('./test.log', print_r($thumbnail_file, true)."\n\n\n\n\n", FILE_APPEND);
            $thf = fopen($thumbnail_file, "wb");
            fwrite($thf, $thumbnailData);
            fclose($thf);
        }
        // EN
        //File
        db_query('INSERT INTO `' . TABLE_PREFIX . 'file` (`ft`, `bk`, `type`, `size`, `key`, `signature`,`name`,`attrs`,`created`,`path`) VALUES ("T", "F",  "' . $Att["type"] . '","' . $Att["size"] . '","' . $key . '","' . $signature . '","' . $Att["name"] . '",NULL,NOW(),"' . $filePath . '")');
        $file_entry_id = db_insert_id();
        //Attachment
        db_query('INSERT INTO `' . TABLE_PREFIX . 'attachment` (`object_id`, `file_id`, `type`, `name`, `inline`, `lang`) VALUES (' . $thread_entry_id . ', "' . $file_entry_id . '",  "H",NULL,"0",NULL)');

        //RSync
        exec("rsync $parameter ../uploads/ $remote", $outputRsync);
        file_put_contents('./test.log', $outputRsync, FILE_APPEND);

        while($rsyncNumberOfTries>0 && !$outputRsync) {
            // try rsync again
            exec("rsync $parameter ../uploads/ $remote", $outputRsync);
            file_put_contents('./test.log', $outputRsync, FILE_APPEND);
            $rsyncNumberOfTries--;
        }
        if(!$outputRsync){
            if($enableAttachmentLogging){
            $sql = "update mst_mail_attachment_logs set declined_reason='attachment rsync error' where id=$mailAttachmentInsertId";
            db_query($sql); 
            }
        }else{
            unlink($output_file);
            unlink($thumbnail_file);
            if($enableAttachmentLogging){
            $sql="update mst_mail_attachment_logs set status_id='1',update_date_time=now() where id='$mailAttachmentInsertId'";    
            db_query($sql);
            }
        } 
    }else if ($enableAttachmentLogging){
        if($Att["size"] < $emailAttachmentConfig["maxSize"]){
           $sql="update mst_mail_attachment_logs set declined_reason='attachment size exceeded' where id=$mailAttachmentInsertId";    
           db_query($sql);
        }else{
           $sql="update mst_mail_attachment_logs set declined_reason='attachment type not supported' where id=$mailAttachmentInsertId";    
           db_query($sql); 
        }
    }
}
function getEmailIdFromString($inputEmailString){
$inputEmailString = preg_replace('/\s+/', '', $inputEmailString);//removing white space
$pattern	=	"/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
preg_match_all($pattern, $inputEmailString, $matches);
$matches[0][0]=preg_replace('/\s+/', '', $matches[0][0]);//removing white space
return $matches[0];
}
