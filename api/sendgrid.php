<?php

@chdir(dirname(__FILE__).'/'); //Change dir.
require('api.inc.php');
global $cfg;



// print ;exit;

date_default_timezone_set('Asia/Kolkata');

$parsed  = array();
$blocks  = preg_split('/\n\n/', $_REQUEST['headers']);
$lines   = array();
$matches = array();
foreach ($blocks as $i => $block) {
    $parsed[$i] = array();
    $lines = preg_split('/\n(([\w.-]+)\: *((.*\n\s+.+)+|(.*(?:\n))|(.*))?)/',
                        $block, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($lines as $line) {
        if(preg_match('/^\n?([\w.-]+)\: *((.*\n\s+.+)+|(.*(?:\n))|(.*))?$/',
                      $line, $matches)) {
            $parsed[$i][$matches[1]] = preg_replace('/\n +/', ' ',
                                                    trim($matches[2]));
        }
    }
}

$parsed = current($parsed);
// print_r($parsed);

//file_put_contents('./test.log', print_r($_REQUEST, true)."\n\n\n\n\n", FILE_APPEND);
//exit;
//file_put_contents('./test.log', print_r($_FILES, true)."\n\n\n\n\n", FILE_APPEND);
  
$mail['from'] = preg_replace('/.*<(.*)>.*/', "$1", $parsed['From']);
$mail['mid'] = $parsed['Message-ID'];
$mail['to'] = preg_replace('/.*<(.*)>.*/', "$1", $parsed['To']);

$mail['subject'] = $_REQUEST['subject'];

if (isset($_REQUEST['text']) && $_REQUEST['text'] != '') {
  $mail['body'] = $_REQUEST['text'];
  
  $fragment_without_date_author = preg_replace(
      '/\n\nOn(.*?)wrote:(.*?)$/si',
      '',
      trim($mail['body'])
  );

  $mail['body'] = $fragment_without_date_author;

  $mail['body'] = current(explode('-----Original Message-----', trim(current(explode($cfg->getReplySeparator(), $mail['body'])))));
}
else {
  $mail['body'] = strip_tags($_REQUEST['html'], '<br><br/>');
  $mail['body'] = trim(current(explode($cfg->getReplySeparator(), str_replace("\r", "", str_replace("\n", "", $mail['body'])))));
}


$MainAlreadyExist = db_result(db_squery("SELECT mid from `".TABLE_PREFIX."thread_entry_email` WHERE `mid`='".$mail["mid"]."'"));
if (!empty($MainAlreadyExist)) {
  exit();
}


// $mail['body'] = preg_match('/^On [^"]*"[^"]*" <([^>]*)> wrote:$/', $mail['body'], $re) ? $re[1] : '';
// $message = preg_replace('~On(.*?)wrote:(.*?)$~si', '', $message); 
// $mail['body'] = $message;

// file_put_contents('./test.log', print_r($_REQUEST, true)."\n\n\n\n\n", FILE_APPEND);
// file_put_contents('./test.log', print_r($mail, true)."\n\n\n\n\n", FILE_APPEND);

// print_r($mail);exit;

$to_array = explode('@', $mail['to']);
$to_email_first_part = current($to_array);
$ticket_number = str_replace('helpdesk+id', '', $to_email_first_part);

// print_r($ticket_number);exit;

if (empty($mail['from']) || empty($ticket_number) || empty($mail['subject']) || 
    empty($mail['body']) || empty($mail['mid'])) {
  exit();
}



$customer_uid = db_result(db_squery('SELECT user_id FROM `'.TABLE_PREFIX.'user_email` WHERE address = "'.db_input($mail['from'], false).'"'));

if (empty($customer_uid)) {
  exit();
}

$customer_name = db_result(db_query('SELECT name FROM `'.TABLE_PREFIX.'user` WHERE id = '.$customer_uid));
// print $customer_email;exit; 

$ticket_id = db_result(db_query('SELECT ticket_id FROM `'.TABLE_PREFIX.'ticket` WHERE number = "'.db_input($ticket_number, false).'" AND user_id = '.$customer_uid));

if (empty($ticket_id)) {
  exit();
}

// print $ticket_id;exit;

$thread_id = db_result(db_query('SELECT id FROM `'.TABLE_PREFIX.'thread` WHERE object_id = '.$ticket_id));
// print $thread_id;exit;


db_query('INSERT INTO `'.TABLE_PREFIX.'thread_entry` (`pid`, `thread_id`, `staff_id`, `user_id`, 
         `type`, `flags`, `poster`, `editor`, `editor_type`, `source`, `title`, `body`, `format`, 
         `ip_address`, `created`, `updated`) VALUES (0, '.$thread_id.', 0, '.$customer_uid.', 
         "M", 0, "'.$customer_name.'", "", "", "Email", "'.db_input($mail['subject'], false).'", "'.db_input(nl2br($mail['body']), false).'", "html", "", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'")');

$thread_entry_id = db_insert_id();

db_query('INSERT INTO `'.TABLE_PREFIX.'thread_entry_email` (`thread_entry_id`, `mid`, `headers`) VALUES ('.$thread_entry_id.', "'.$mail['mid'].'",  "'.db_input($_REQUEST['headers'], false).'")');

/**
 * UPLOADING ATTACHMENTS
 * @author - Akash Kumar
 */
global $cfg;
$emailAttachmentConfig = $cfg->config['EmailAttachment']['value'];
//$emailAttachmentConfig= db_result(db_query('SELECT value FROM `'.TABLE_PREFIX.'config` WHERE `key` = "EmailAttachment"'));
$emailAttachmentConfig = json_decode($emailAttachmentConfig,true);
$count = 1;

$AttachmentRsyncConfig = json_decode($cfg->config['AttachmentRsync']['value'],true);
$parameter=$AttachmentRsyncConfig["parameter"];
$local=$AttachmentRsyncConfig["local"];
$remote=$AttachmentRsyncConfig["remote"];
foreach($_FILES as $Att){
    
    //To check for maximum Upload
    if($count>$emailAttachmentConfig["maxNumber"])
        break;
    if($Att["size"]<$emailAttachmentConfig["maxSize"] && !in_array($Att["type"],$emailAttachmentConfig["type"]) && !in_array(mime_content_type($Att["tmp_name"]),$emailAttachmentConfig["mimeType"])){

        $count++;
        $key='EMail'.time().rand(1,99999);
        $signature=md5('signature');
        //file_put_contents('./test.log', print_r($Att, true)."\n\n\n\n\n", FILE_APPEND);
        $up = $Att;
        $year = date("Y");
        $month = date("m");
        $day = date("d");

        $fileName =$key.rand(1,5000).'.'.str_replace("image/","",$up["type"]);
        $filePath = $year.'/'.$month.'/'.$day.'/'.$fileName;
        $thumbWidth=100;
        $thumbHeight=100;
        /**
         * To prepare Images and its thumbnail
         * 
         */
        $uploadDir = '/uploads/image/';
        $output_file = INCLUDE_DIR.'..'.$uploadDir.$filePath;
        if(!file_exists(INCLUDE_DIR.'..'.$uploadDir.''.$year))
            mkdir(INCLUDE_DIR.'..'.$uploadDir.''.$year, 0777, true);

        if(!file_exists(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month))
            mkdir(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month, 0777, true);

        if(!file_exists(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month.'/'.$day))
            mkdir(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month.'/'.$day, 0777, true);
        //file_put_contents('./test.log', print_r($output_file, true)."\n\n\n\n\n", FILE_APPEND);
        move_uploaded_file($up['tmp_name'], $output_file);


        /**
         * Making Thumbnail
         */
        $im = imagecreatefromstring(file_get_contents( $output_file ));

        // Get width and height of original image resource
        $origWidth = imagesx($im);
        $origHeight = imagesy($im);

        // Create new destination image resource for new 24 x 24 image
        $imNew = imagecreatetruecolor($thumbWidth, $thumbHeight);

        // Re-sample image to smaller size and display
        imagecopyresampled($imNew, $im, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);
        ob_start (); 

          imagejpeg ($imNew);
          $thumbnailData = ob_get_contents (); 
          ob_end_clean (); 

        $uploadDir = '/uploads/thumbnail/';
        $thumbnail_file = INCLUDE_DIR.'..'.$uploadDir.$filePath;
        if(!file_exists(INCLUDE_DIR.'..'.$uploadDir.''.$year))
            mkdir(INCLUDE_DIR.'..'.$uploadDir.''.$year, 0777, true);

        if(!file_exists(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month))
            mkdir(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month, 0777, true);

        if(!file_exists(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month.'/'.$day))
            mkdir(INCLUDE_DIR.'..'.$uploadDir.''.$year.'/'.$month.'/'.$day, 0777, true);
        //file_put_contents('./test.log', print_r($thumbnail_file, true)."\n\n\n\n\n", FILE_APPEND);
        $thf = fopen($thumbnail_file, "wb"); 
        fwrite($thf, $thumbnailData); 
        fclose($thf);
        // END
        //File
        db_query('INSERT INTO `'.TABLE_PREFIX.'file` (`ft`, `bk`, `type`, `size`, `key`, `signature`,`name`,`attrs`,`created`,`path`) VALUES ("T", "F",  "'.$Att["type"].'","'.$Att["size"].'","'.$key.'","'.$signature.'","'.$Att["name"].'",NULL,NOW(),"'.$filePath.'")');
        $file_entry_id = db_insert_id();
        //Attachment
        db_query('INSERT INTO `'.TABLE_PREFIX.'attachment` (`object_id`, `file_id`, `type`, `name`, `inline`, `lang`) VALUES ('.$thread_entry_id.', "'.$file_entry_id.'",  "H",NULL,"0",NULL)');
        
        //RSync
        exec("rsync $parameter $local $remote", $outputThumb);
        if($outputThumb){
            unlink($thumbnail_file);
            unlink($output_file);
        }
    }
}
// file_put_contents('./test.log', print_r(array('success' => 1), true)."\n\n\n\n\n", FILE_APPEND);
