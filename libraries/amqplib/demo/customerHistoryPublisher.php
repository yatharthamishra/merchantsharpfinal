<?php

//error_reporting(1);

define('AREA', 'A');
define('AREA_NAME', 'admin');

require_once dirname(__FILE__) . '/../../prepare.php';
require_once dirname(__FILE__) . '/../../init.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'customer_transaction_history_exchange';
$routingKey = 'customer_transaction_history';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
$ch->exchange_declare($exchange, 'direct', true, true, false);

$type= isset($_GET['type'])?$_GET['type']:'';

if ($type== "full") {
    $start = 0;
    $batchSize = 5000;
    $totalUsers = getUserCount();
    while ($start < $totalUsers) {
        $users = selectAllUserId($start, $batchSize);
        addUsersToQueue($users);
        echo "fetching from userid: $start \n";
        $start = $start + $batchSize;
    }
} else {
    $seconds= isset($_GET['seconds'])?$_GET['seconds']:3600;
    $endTime = time();
    $startTime = time() - $seconds;
    $users = getUsersInAnInterval($startTime, $endTime);
    addUsersToQueue($users);
    die("last hour users added");
}

$ch->close();
$conn->close();

function addUsersToQueue($users) {
    global $exchange,$routingKey,$ch;
    foreach ($users as $userId) {
        $arr = array();
        $arr['user_id'] = $userId;
        $msg_body = json_encode($arr);
        $msg = new AMQPMessage($msg_body);
        $ch->basic_publish($msg, $exchange, $routingKey);
    }
}
