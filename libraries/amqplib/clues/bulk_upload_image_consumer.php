<?php

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'move_bulk_upload_image_to_consumer_exchange';
$consumer_tag = 'move_bulk_upload_image_to_consumer_queue';
$queue = 'move_bulk_upload_image_to_consumer_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg) {
    $body = json_decode($msg->body);
    $body = http_build_query($body);
    $url = CONSUMER_HOST . "/tools/bulk_upload_image_cron.php";
    $result = curlpost($url, $body);
//    echo $result;die;
    if ($result == "1") {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

function curlpost($url, $data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
//    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}
