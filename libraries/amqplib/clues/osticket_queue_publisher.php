<?php
include __DIR__ . '/config.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
use PhpAmqpLib\Message\AMQPMessage;

function AddToQueue($data_arr, $exchange, $key_to_exchange, $priority=false,$enable_logging=true,$maxNumberOfPriorities=2)
{  
        $conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
        $channel   = $conn->channel();
        
        if($priority===false){
            $channel->exchange_declare($exchange, 'direct', true, false, false,false,false,array('x-max-priority'=>array('I',$maxNumberOfPriorities)));
        }else{
            $channel->exchange_declare($exchange, 'direct', true, false, false);
        }
        
        $data = serialize($data_arr);
        $msg = new AMQPMessage($data,array('delivery_mode' => 2,'priority'=>$priority));
        $channel->basic_publish($msg, $exchange, $key_to_exchange);
        $channel->close();
        $conn->close();
        
        return true;
}

function addMailToMailingQueue($entity_type,$relatedData,$priority=false,$maxNumberOfPriorities=2){
        $conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
        $channel   = $conn->channel();
        global $cfg;
        $mailingQueue=  json_decode($cfg->config['mailingQueue']['value'],true);
        $exchange=$mailingQueue['exchange'];
        $key_to_exchange=$mailingQueue['key'];
        if($priority===false){
            $channel->exchange_declare($exchange, 'direct', true, false, false,false,false,array('x-max-priority'=>array('I',$maxNumberOfPriorities)));
        }else{
            $channel->exchange_declare($exchange, 'direct', true, false, false);
        }
        $queueData=  array('entity'=>$entity_type,'data'=>$relatedData);
        $data = serialize($queueData);
        $msg = new AMQPMessage($data,array('delivery_mode' => 2,'priority'=>$priority));
        $channel->basic_publish($msg, $exchange, $key_to_exchange);
        $channel->close();
        $conn->close();
        return true;
    
}
