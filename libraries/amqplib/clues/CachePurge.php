<?php
require_once 'RabbitmqDelayedQueue.php';
//require_once  dirname(__FILE__) . '/../../controllers/admin/clues_deals';
/**
 * Product cache purge 
 */
class CachePurge implements ICahePurge {
    

    /**
     *
     * @var type 
     */

    private static $_valid_platforms = array(
        self::PLATFORM_DESKTOP,
        self::PLATFORM_MOBILE,
        self::PLATFORM_API
        
    );
    
    /**
     *
     * @var array all supported caching purge services
     */
    private static $_cache_services = array(
                                        self::MEMCACHE,
                                        self::REDIS,
                                        self::CDN_AKAMAI_CACHE
                                    );
    

    /**
     *
     * @var purge componets for purge request 
     */
    private $_caches      = array() ;
    
    /**
     *
     * @var refrence to rabbitmq delayed queue class object 
     */
    private $_queue ;

    /**
     *
     * @var array platform for cache purge 
     */
    private $_platforms ;

    
    
    /**
     * 
     * @param array $purge_componetns 
     * @param array $options
     * @return boolean
     */
    public function __construct( $caches = array() , $platforms = array()) {
      //print_r('expression'); die;
        try {
            if( !empty($caches) && !empty($platforms)) {
                $this->_setCacheServices( $caches );
                $this->_setPlatforms( $platforms );
            }
            $delay = Registry::get('config.cache_purge_request_delay');
            $this->_queue = new RabbitmqDelayedQueue( 
                    CachePurge::RABBITMQ_DELAYED_EXCHANGE, 
                    CachePurge::RABBITMQ_DELAYED_QUEUE, 
                    CachePurge::RABBITMQ_PURGE_EXCHANGE, 
                    CachePurge::RABBITMQ_PURGE_QUEUE, 
                    $delay
                );
           // print_r($this->_queue); die;
            
        } catch (Exception $exc) {
            Analog::log ('purge_cache_service',$exc->getMessage(), 
                    "Cache Purge Service", "ADMIN",
                    Registry::get('config.LOG_LEVELS.ERROR'));
        }

        return true ;
    }
    
    public function __destruct() {
        $this->closeConnection();
    }
    
    /**
     * 
     * @param string $cache_name validate cache service 
     * @return type
     */
    private static function _isValidCacheService( $cache_name ){
        return in_array($cache_name, self::$_cache_services );
    }
    
    /**
     * set function for setting purge components
     * @param array $components 
     * @return boolean
     */
    private function _setCacheServices( $caches ){
        
        if( empty($caches) ){
            return false ;
        }
        $valid_caches = array();
        foreach( $caches as $cache ){
            if( !self::_isValidCacheService( $cache )){
                throw new Exception("Must provide atleast one cache service for purge", 470); ;
            }
            $valid_caches[] = $cache;
            
        }
        $this->_caches = $valid_caches;
        return true ;
    }
    private static function _isValidPlatform( $platform ){
        return in_array($platform, self::$_valid_platforms );
    }
    private function _setPlatforms( $platforms ){
        
        if( empty($platforms) ){
            return false ;
        }
        $valid_platforms = array();
        foreach( $platforms as $platform ){
            if( !self::_isValidPlatform( $platform )){
                throw Exception("Must provide atleast one platform  for purge", 472); ;
            }
            $valid_platforms[] = $platform;
        }
        
        $this->_platforms = $valid_platforms;
        
        return true ;
    }
    
    
    
    /**
     * used purge api for purge the cache
     * @param array $product
     */
    public static function _purgeCache( $data ){


        $client_id = Registry::get('config.admin_internal_client_id');
        $client_secret = Registry::get('config.admin_internal_client_secret');
        $soa_token_generate_url = Registry::get('config.soa_token_generate_url');
        $soa_api_end_point_url = Registry::get('config.admin_soa_api_end_point_url');

        $credentials = array(
            "grant_type" => 'client_credentials',
            "client_id" => $client_id,
            "client_secret" => $client_secret
        );

        $purge_request_end_point = $soa_api_end_point_url . PURGE_REQUEST_API_END_POINT;
        $token = self::genToken($soa_token_generate_url, $credentials);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $purge_request_end_point);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data );
        echo "Request::".$data;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer $token",
            "Content-Type: application/json"
                )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        echo "server_output:".$server_output;
        $logging = Registry::get('config.cache_purge_logging') ;
        $logging = isset($logging)?$logging:false;
        if( $logging === true ){
            Analog::log ('purge_cache_service',"Purge Request :$purge_request_end_point:Bearer $token Params: $data Responce:$server_output", 
                    "Cache Purge Service", "ADMIN",
                    Registry::get('config.LOG_LEVELS.ERROR'));
        }
        curl_close($ch);        
        return true;        
    }
    
    /**
     * 
     * @param object $msg
     */
    
    public function callBackPurgeCache( $msg ){
        
        
        /*
         * call purge call on product
         * Successfull ack queue about consumption of message 
         * don't ack msg if not able to process
         */
        if( $this->_sendPurgeRequestCache($msg) === true ){
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);
        }else{
            
        }
        
        // Send a message with the string "quit" to cancel the consumer.
        if ($msg->body === 'quit') {
            $msg->delivery_info['channel']->
                    basic_cancel($msg->delivery_info['consumer_tag']);
        }
        
        return true ;        
    }
    /**
     * 
     * @param msg object of rabbitmq lib $msg
     * @return boolean
     */
    public function _sendPurgeRequestCache( $msg){
        
        if (empty($msg->body) ) {
            return false;
        }

        
        $data_string = base64_encode($msg->body);
        $purge_request_end_point = CONSUMER_HOST."/tools/cache_purge_endpoint.php";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $purge_request_end_point);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('data'=>$data_string)) );         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responce  = curl_exec($ch);
        echo $responce;
        $logging = Registry::get('config.cache_purge_logging') ;
        
        $logging = isset($logging)?$logging:false;
        
        if( $logging == true ){
            
            Analog::log ('purge_cache_service',"Purge Request send to end point payload:$data and responce $responce ", 
                    "Cache Purge Service", "ADMIN",
                    Registry::get('config.LOG_LEVELS.DEBUG'));
            
        }
        curl_close($ch); 
        
        
        /**
         * to do
         * need to handle $responce 
         */
        return true ;
    }
    /**
     * Call purge call for every products in queue
     * This function take time to purge all products in queue
     * purge product cache
     */
    public function purgeCache(){
        
        try {
    
            $this->_queue->rabbitmq_channel->basic_consume(
                        self::RABBITMQ_PURGE_QUEUE, 
                        __CLASS__, 
                        RabbitmqDelayedQueue::NO_LOCAL_FALSE,
                        RabbitmqDelayedQueue::NO_ACK_FALSE, 
                        RabbitmqDelayedQueue::EXCLUSIVE_FALSE, 
                        RabbitmqDelayedQueue::NO_WAIT_FALSE, 
                        array($this,'callBackPurgeCache')
            );
        
            while (count($this->_queue->rabbitmq_channel->callbacks)) {
                    $this->_queue->rabbitmq_channel->wait();
            }            
        } catch (Exception $exc) {
            
            Analog::log ('purge_cache_service',$exc->getMessage(), 
                    "Cache Purge Service", "ADMIN",
                    Registry::get('config.LOG_LEVELS.ERROR'));
        }
    }
    /**
     *  close all open channel and connection 
     */
    public function closeConnection(){
        $this->_queue->close();
    }
    /**
     * 
     * @param array $products
     * @return boolean
     */
    public function addPurgeRequest( $purge_data ) {
      //print_r($purge_data); die;
        $purge_msg = new stdClass();
        
        if( in_array( self::MEMCACHE, $this->_caches) ) { 
            $purge_msg->cache->{self::MEMCACHE} = $this->_formatMsgMemcache(self::MEMCACHE, $purge_data);
         }
        if( in_array( self::REDIS, $this->_caches) ) {
            $purge_msg->cache->{self::REDIS} = $this->_formatMsgMemcache(self::REDIS, $purge_data);
              }
        if( in_array( self::CDN_AKAMAI_CACHE, $this->_caches) ){
               $purge_msg->cache->{self::CDN_AKAMAI_CACHE} = $this->_formatMsgCDN(
                                                                self::CDN_AKAMAI_CACHE, 
                                                                $purge_data
                                                            );
        }
        $this->_queue->publish(json_encode($purge_msg));
        return true ;
        
    }
    /**
     * 
     * @param string $cache_type
     * @param array $data
     * @return \stdClass
     */
    private function _formatMsgMemcache($cache_type ,$data ){
          $object = new stdClass();
        
        foreach( $data as $purge_component => $values ){

                if(!is_array($values) && isset($values)){
                    $values = array($values);
                }
                if($purge_component != self::COMPONENT_SITEURLS){ 
                foreach( $values as $value ){
                    $object->{$purge_component}[] = $value;
                }
                
                }
                
        }
        
        return $object;
    }
    /**
     * 
     * @param string $cache_type
     * @param array $data
     * @return stdClass
     */
    private function _formatMsgCDN($cache_type ,$data ){
        
        $object = new stdClass();
        
        foreach( $data as $purge_component => $values ){
                if(!is_array($values) && isset($values)){
                    $values = array($values);
                }
                if(( $purge_component == self::COMPONENT_PRODUCT )
                        || ( $purge_component == self::COMPONENT_CATEGORY )){
                    foreach( $values as $value ){
                        
                        if(in_array( self::PLATFORM_DESKTOP, $this->_platforms)){
                            $type = ( $purge_component == self::COMPONENT_PRODUCT )
                                ? 'P':'C';
                            $arl = $this->_getSEOName($value,$type ,self::PLATFORM_DESKTOP );
                            
                            if( !empty($arl) ){
                                $object->arls[] = $arl;
                            }
                        }
                        if(in_array( self::PLATFORM_MOBILE, $this->_platforms)){
                            $type = ( $purge_component == self::COMPONENT_PRODUCT )
                                ? 'P':'C';
                            $arl = $this->_getSEOName($value,$type,self::PLATFORM_MOBILE);
                            if( $arl !== false ){
                                $object->arls[] = $arl;
                            }
                        }
                        if(in_array( self::PLATFORM_API, $this->_platforms)){
                            
                            $api_arls = $this->_getAPIURL($value,$purge_component);
                            if( !empty($api_arls) ){
                                foreach( $api_arls as $api_arl)
                                    $object->arls[] = $api_arl;
                            }
                        }
                        
                        
                        
                        
                    }
                } else if($purge_component == self::COMPONENT_SITEURLS){
                    foreach( $values as $value ){
                        $object->arls[] = $value;
                    }
                } else if($purge_component == self::COMPONENT_CAMPAIGN){
                     foreach( $values as $value ){
                      $api_arls = $this->_getCampaignAPIURL($value,$purge_component);
                        if( !empty($api_arls) ){
                                foreach( $api_arls as $api_arl)
                                    $object->arls[] = $api_arl;
                        }
                }
                  foreach( $values as $value){
                     $api_arls = $this->_gethome_campaignsAPIURL($value,$purge_component);
                         if( !empty($api_arls)){ 
                            foreach($api_arls as $api_arl)
                              $object->arls[] = $api_arl;
              }
             }
             foreach( $values as $value ){
                    $api_arls = $this->_getMobile_dealsURL($value,$purge_component);
                        if(!empty($api_arls)){
                               foreach($api_arls as $api_arl)
                                     $object->arls[] = $api_arl;
            }

                }
                foreach( $values as $value ){
                    $api_arls = $this->_getDesktop_homeURL($value,$purge_component);
                        if(!empty($api_arls)){
                               foreach($api_arls as $api_arl)
                                     $object->arls[] = $api_arl;
            }

                }
                 foreach($values as $value ){
                       $api_arls = $this->_getNew_DealsAPIURL($value,$purge_component);
                        if(!empty($api_arls)){
                               foreach($api_arls as $api_arl)
                                     $object->arls[] = $api_arl;
            }
 }

               }

        return $object;
    }
}

    /**
     * 
     * @param string $oauth_url
     * @param array $credentials
     * @return string
     */
    public static function genToken($oauth_url, $credentials) {
        
        $memcache = $GLOBALS['memcache'];
        
        if(
              isset($memcache)  
              && Registry::get('config.memcache') 
              && $GLOBALS['memcache_status'] 
              && ( $token  = $memcache->get('CachePurgeApiToken')) !== false
        ) {
              return $token;
        }
        
        $curl = curl_init($oauth_url);
        
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_HTTPHEADER      => array('Content-Type: application/json'),
            CURLOPT_POST            => 1,
            CURLOPT_POSTFIELDS      => json_encode($credentials)
            )
        );
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        $response   = json_decode($response, true);
        $token      = $response['access_token'] ;
        
        if(
              isset($memcache)  
              && Registry::get('config.memcache') 
              && $GLOBALS['memcache_status'] 
        ) {
            $memcache->set(
                        'CachePurgeApiToken',$token, 
                        MEMCACHE_COMPRESSED,
                        self::API_TOKEN_MEMCACHE_TIME 
                    );
        }
        
        return $token;
    }
    /**
     * 
     * @param int $id
     * @param string $type 'P' ,'C'
     * @return string|boolean
     */
    private function _getSEOName( $id ,$type ,$platform ){
        
        if(empty($id) && in_array($type, array('P','C'))){
            return false ;
        }
        
        $sql = "SELECT 
                    name 
                FROM 
                    cscart_seo_names 
                WHERE 
                    object_id= ?i AND type=?s";
        
        $seo_name = db_get_field( $sql, $id , $type );
        
        if( !empty($seo_name)){
            if( $platform == self::PLATFORM_DESKTOP ){
                $url = Registry::get('config.domain_url_desktop');
            }else if( $platform == self::PLATFORM_MOBILE ){
                $url = Registry::get('config.domain_url_mobile');
            }
            
            $url = $url.'/'.$seo_name.'.html' ;
            return $url;
        }else{
            return false ;
        }
    }
    private function  _getAPIURL( $id ,$type ){
        $urls_with_version = array();
        $all_urls = array();
        $api_keys = Registry::get('config.rest_api_key');
        $versions = Registry::get('config.new_rest_api_supported_versions');
        $url = Registry::get('config.rest_api_url');
        foreach($versions as $version ){
            if( $type == self::COMPONENT_PRODUCT ){
                $param = 'products?product_id='.$id ;
            }else if(  $type == self::COMPONENT_CATEGORY ) {
                $param = 'categories?category_id='.$id;
            }
           $urls_with_version[] =  $url.$version.'/'.$param;
        }
        
        foreach( $api_keys as $key ){
            foreach( $urls_with_version as $url_with_version ){
                $all_urls[] = $url_with_version.'&key='.$key;
            } 
        }
        
        if( !empty($all_urls)){
            return $all_urls;
        }else{
            return false;
        }
       
    }
    private function _getCampaignAPIURL($id, $type){
         $urls_with_version = array();
         $urls_with_zone = array();
         $urls_with_key[] = array();
         $all_urls = array();
         $url = Registry::get('config.frontend_api_path');
         $api_keys = Registry::get('config.rest_api_key');
         $versions = Registry::get('config.new_rest_api_supported_versions');
         $zones = Registry::get('config.product_user_zone_mapping');
         $camp_urls = array();
         foreach($versions as $version ){
         if( $type == self::COMPONENT_CAMPAIGN ){
                 $param = 'campaigns?gid='.$id;
               }
              $urls_with_version[] =  $url.$version.'/'.$param; 
                  // $param = '/api/v9/campaigns?gid='.$id.'&key='.$api_keys['M'] ;
         }
         foreach($api_keys as $key  ){
                   foreach( $urls_with_version as $url_with_version ){
                $all_urls[] = $url_with_version.'&key='.$key;
            }
        }
       foreach( $zones as $key => $value ){
            foreach( $all_urls as $all_url ){
                      $camp_urls[] = $all_url.'&zone='.$key;
            } 
        }
         if( !empty($camp_urls)){
            return $camp_urls;}
          else{     
            return false;
        }
    }
    //home_campaign cache purge-generates url for home_campaign
    private function _gethome_campaignsAPIURL($id, $type){
         $all_urls = array();
         $url = Registry::get('config.frontend_api_path');
         $api_keys = Registry::get('config.rest_api_key');
         $versions = Registry::get('config.new_rest_api_supported_versions');
         $zones = Registry::get('config.product_user_zone_mapping');  
         if( $type == self::COMPONENT_CAMPAIGN ){
                 $param = 'home_campaigns.php?callback=home'.$id.'?gid='.$id;
               }
             foreach( $zones as $key => $value ){
                     $all_urls[] = $url.$param.'&zone='.$key;
            }
        if( !empty($all_urls)){
            return $all_urls;
        }else{
            return false;
        }
    }
    //new_deals cache purge-generates url for new_deals
    private function _getNew_DealsAPIURL($id, $type){
         $urls_with_version = array();
         $urls_with_zone = array();
         $urls_with_key[] = array();
         $all_urls = array();
         $url = Registry::get('config.frontend_api_path');
         $api_keys = Registry::get('config.rest_api_key');
         $versions = Registry::get('config.new_rest_api_supported_versions');
         $zones = Registry::get('config.product_user_zone_mapping');
         $newdeals_urls = array();

         foreach($versions as $version ){
          if( $type == self::COMPONENT_CAMPAIGN ){
                 $param = 'new_deals?gid='.$id;
               }
              $urls_with_version[] =  $url.$version.'/'.$param; 
                  // $param = '/api/v9/campaigns?gid='.$id.'&key='.$api_keys['M'] ;
         }
         foreach($api_keys as $key  ){
                foreach( $urls_with_version as $url_with_version ){
                $all_urls[] = $url_with_version.'&key='.$key;
            }
        }
    
             
     foreach( $zones as $key => $value ){
            foreach( $all_urls as $all_url ){
        
                          $newdeals_urls[] = $all_url.'&zone='.$key;
            } 
        }
        if( !empty($newdeals_urls)){
            return $newdeals_urls;
        }else{
            return false;
        }
    }
    private function _getMobile_dealsURL($id,$type)
    {
        $all_urls = array();
        $url = Registry::get('config.frontend_api_path');
        $api_keys = Registry::get('config.rest_api_key');
        $versions = Registry::get('config.new_rest_api_supported_versions');
          foreach($versions as $version ){
          if( $type == self::COMPONENT_CAMPAIGN ){
                 $param = 'mobile_page?type=H';
               }
              $urls_with_version[] =  $url.$version.'/'.$param; 
           }   
        foreach($api_keys as $key  ){
                foreach( $urls_with_version as $url_with_version ){
                $all_urls[] = $url_with_version.'&key='.$key;
            }
        }  
         if( !empty($all_urls)){
            return $all_urls;
        }else{
            return false;
        }
    }
    private function _getDesktop_homeURL($id,$type)
    {
        $all_urls = array();
        $url = Registry::get('config.frontend_api_path');
        $api_keys = Registry::get('config.rest_api_key');
        $versions = Registry::get('config.new_rest_api_supported_versions');
         foreach($versions as $version ){
          if( $type == self::COMPONENT_CAMPAIGN ){
                 $param = 'desktop_structure?';
               }
              $urls_with_version[] =  $url.$version.'/'.$param; 
           }   
        foreach($api_keys as $key  ){
                foreach( $urls_with_version as $url_with_version ){
                $all_urls[] = $url_with_version.'&key='.$key.'&type=H';
            }
        } 
         if( !empty($all_urls)){
            return $all_urls;
        }else{
            return false;
        }
    }
    private function _getCampaignURL($id){
        $query =  "SELECT c.page_id from clues_deal_groups a
                    INNER join clues_campaign c ON a.campaign_id=c.id  where a.id =?i limit 1";
        $page_id = db_get_field( $query, $id ); 
        $arl = $this->_getSEOName($page_id,'a'); 
        if( $arl !== false ){
            return $arl;
        }else{
            false;
        }
    }
    
}


interface ICahePurge {
    
    const MEMCACHE                  = 'memcache' ;
    
    const REDIS                     = 'redis' ;
    
    const CDN_AKAMAI_CACHE          = 'akamai' ;
    
    const OPTION_REFRESH_MEMCACHE   = 'memcache_prefetch';
    
    const OPTION_REFRESH_REDIS      = 'radis_prefetch';
    
    const RABBITMQ_DELAYED_EXCHANGE = 'cache_purge_delayed_exchange';
    
    const RABBITMQ_DELAYED_QUEUE    = 'cache_purge_delayed_queue';
    
    const RABBITMQ_PURGE_EXCHANGE   = 'cache_purge_exchange';
    
    const RABBITMQ_PURGE_QUEUE      = 'cache_purge_queue';
    
    const PLATFORM_DESKTOP          = 'desktop';
    
    const PLATFORM_MOBILE           = 'mshopclues';

    const PLATFORM_API              = 'api';    
    
    const COMPONENT_PRODUCT         = 'products';
    
    const COMPONENT_CATEGORY        = 'categories';
    
    const COMPONENT_SITEURLS        = 'siteurl';
    
    const COMPONENT_CAMPAIGN        = 'campaign';

    const COMPONENT_NEW_DEALS       = 'new_deals';
    
    const COMPONENT_HOME_CAMPAIGN   = 'home_campaign';  

    const API_TOKEN_MEMCACHE_TIME   = 1800 ;
    
    public function addPurgeRequest( $purge_data );
}
