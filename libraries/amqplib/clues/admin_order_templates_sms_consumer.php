<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$queue = 'admin_order_templates_sms_queue';
$consumer_tag = 'consumer_admin_order_templates_sms';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){

	$data = json_decode($msg->body,true);

	$order_id = $data['order_id'];
	$template_id = $data['template_id'];

    $user_id = '';

    if (isset($data['user_id'])) {
	   $user_id = $data['user_id'];
    }

	$dynamic_variables = '';

	if (isset($data['dynamic_variables'])) {
        $dynamic_variables = urlencode(serialize(json_decode($data['dynamic_variables'], true)));
    }

    $refund_id = '';

    if (isset($data['refund_id'])) {
    	$refund_id = $data['refund_id'];
    }

    $variable = '';

    if (isset($data['variable'])) {
        $variable = urlencode(serialize($data['variable']));
    }
    $item_id = '';
    if (isset($data['item_id']) && !empty($data['item_id'])) {
        $item_id = $data['item_id'];
    }

	$result = file_get_contents(CONSUMER_HOST . "/tools/admin_order_templates_sms.php?order_id=$order_id&template_id=$template_id&user_id=$user_id&dynamic_variables=$dynamic_variables&refund_id=$refund_id&variable=$variable&item_id=$item_id");

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>