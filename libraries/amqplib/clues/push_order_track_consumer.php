<?php

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'push_order_track_exchange';
$queue = 'push_order_track_queue';
$consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg) {
    $order_data = json_decode($msg->body, true);

    if ($order_data['mode'] == "real_time_tracking") {
        $order_id = $order_data['tracking_details']['order_id'];
        $mode = $order_data['mode'];
        $from_status = $order_data['tracking_details']['from_status'];
        $to_status = $order_data['tracking_details']['to_status'];
        $carrier_name = str_replace(" ", "_", $order_data['tracking_details']['carrier_name']);
        $shipment_type = $order_data['tracking_details']['shipment_type'];
        $shipment_data = $order_data['tracking_details']['shipment_data'];
        $awbno = $order_data['tracking_details']['awbno'];
        $rules_from_status = $order_data['tracking_details']['rules_from_status'];
        $location = str_replace(" ", "_", $order_data['tracking_details']['location']);
        $carrier_id = $order_data['carrier_id'];
        $shipment_id = $order_data['tracking_details']['shipment_id'];
        $update_date_time = str_replace(" ", "_", $order_data['tracking_details']['update_date_time']);
        $reciever_name = str_replace(" ", "_", $order_data['tracking_details']['reciever_name']);
        $user_id = $order_data['tracking_details']['user_id'];
        
        $url = CONSUMER_HOST . "/tools/push_order_track_consumer_process.php?mode=" . $mode . "&order_id=" . $order_id . "&from_status=" . $from_status . "&to_status=" . $to_status . "&carrier_name=" . $carrier_name . "&shipment_type=" . $shipment_type . "&shipment_data=" . $shipment_data . "&awbno=" . $awbno . "&rules_from_status=" . $rules_from_status . "&carrier_id=" . $carrier_id . "&shipment_id=" . $shipment_id . "&update_date_time=" . $update_date_time . "&location=" . $location . "&reciever_name=" . $reciever_name . "&user_id=" . $user_id;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $data = curl_exec($ch);
        curl_close($ch);

        if ($data == "done") {
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }
    }
}
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}
