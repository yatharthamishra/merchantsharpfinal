<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'return_status_sms_exchange';
$queue = 'return_status_sms_day_queue';
$consumer_tag = 'consumer_return_sms_day';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
	$return_data = json_decode($msg->body,true);

	$return_id = $return_data['return_id'];
	$to_status = $return_data['to_status'];
	$notify = $return_data['notify'];
	
	$result = file_get_contents(CONSUMER_HOST . "/tools/return_status_sms.php?return_id=$return_id&to_status=$to_status&notify=$notify");
	// print $result."\n\n\n";die;
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>