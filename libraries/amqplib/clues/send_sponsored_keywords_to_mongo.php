<?php
define('AREA','A');
include(__DIR__ . '/config.php');
include(__DIR__.'/../../config.local.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange_name = "search_keywords_report_exchange";
$queue = "search_keywords_report_queue";
$consumer_tag = "search_keywords_report_tag";

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
         $data = json_decode($msg->body);
        //$order_data = json_decode($msg->body,true);

        //$order_id = $order_data['order_id'];
        //$status_to = $order_data['status_to'];
         
        $result = file_get_contents(CONSUMER_HOST."/tools/send_sponsored_keywords_to_mongo.php?report_id={$data->reportID}&type={$data->type}");
        if ($result) {
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }


}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>

