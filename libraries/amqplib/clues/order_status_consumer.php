<?php

define('AREA', 'A');
define('AREA_NAME', 'admin');
/*
require  dirname(__FILE__) . '/../../prepare.php';
require  dirname(__FILE__) . '/../../init.php';

fn_load_addon("hp_changes");
fn_load_addon("my_changes");
 
 */
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;



$exchange = 'order_status_exchange';
$queue = 'order_status_queue';
$consumer_tag = 'consumer_status_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();


/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg)
{
        $order_data = json_decode($msg->body,true);
        file_put_contents("/tmp/bulk_order_status_consumer.log","consumer-".date('Y-m-d H:i:s')."\n".print_r($order_data,true)."\n",FILE_APPEND);
        
        
	$order_id = $order_data['order_id'];
        $status_fr_to = $order_data['status_fr_to'];
        $fr=$status_fr_to['status_from'];
        $to=$status_fr_to['status_to'];
	$force_notification = serialize($order_data['force_notification']);
        /*
	$order_info1 = fn_get_order_info($order_id, true);
        $edp_data = fn_generate_ekeys_for_edp(array('status_from' => $status_fr_to['status_from'], 'status_to' => $status_fr_to['status_to']), $order_info1);	
        
	fn_order_notification($order_info1, $edp_data, $force_notification);
        */
        $url=CONSUMER_HOST."/tools/order_status_consumer_process.php?order_id=$order_id&fr=$fr&to=$to&force_notification=$force_notification";
        file_put_contents("/tmp/bulk_order_status_consumer.log",$url."\n",FILE_APPEND);
        file_get_contents($url);
        
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        

	
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $exchange, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}




?>

