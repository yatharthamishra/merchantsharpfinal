<?php
/*
$config['billing_request_settle_exchange']="billing_request_settle_exchange";
$config['billing_request_settle_key']="billing_request_settle_key";
$config['billing_request_settle_queue']="billing_request_settle_queue";
?>
*/

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'billing_request_settle_exchange';
$queue = 'billing_request_settle_queue';
$consumer_tag = 'billing_request_settle_consumer';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
	$data = json_decode($msg->body,true);
	$url=CONSUMER_HOST."/tools/billing_request_settle_consumer_cron.php";
	$d['request_id']=$data['fn_hp_payment_request_change_status']['request_id'];
	$d['statusto']=$data['fn_hp_payment_request_change_status']['statusto'];
	$d['statusfrom']=$data['fn_hp_payment_request_change_status']['statusfrom'];
	$d['transac_id']=$data['fn_hp_payment_request_change_status']['transac_id'];
	$d['user_type']=$data['fn_hp_payment_request_change_status']['user_type'];
	$d['dont_validate']=$data['fn_hp_payment_request_change_status']['dont_validate'];
	$d['manifest_id']=$data['fn_hp_company_payment_create']['manifest_id'];
	$d['mode']=$data['fn_hp_company_payment_create']['mode'];
	$d['rabbit_mq']=1;

	$response=curlpost($url,$d);
	
    if ($response) {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}


function curlpost($url,$data) {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,true);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch); 
    curl_close($ch);
    return $response;
}
?>