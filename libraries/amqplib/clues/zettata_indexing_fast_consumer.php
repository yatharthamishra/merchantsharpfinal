<?php
define('AREA','A');
include(__DIR__ . '/config.php');
include(__DIR__.'/../../config.local.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange     = $config['zettata_fast_queue']['exchange'];
$queue        = $config['zettata_fast_queue']['queue'];
$consumer_tag = 'zettata_importer_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){

        $result = file_get_contents(CONSUMER_HOST."/tools/zettata_consumer.php?data=".urlencode($msg->body));
        //if ($result) {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        //}


}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>

