<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'order_sms_exchange';
$queue = 'order_sms_queue';
$consumer_tag = 'consumer_order_sms';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
	$order_data = json_decode($msg->body,true);

	$order_id = $order_data['order_id'];
	$status_to = $order_data['status_to'];

	$user_id = '';

	if (isset($order_data['user_id'])) {
		$user_id = $order_data['user_id'];
	}
	
	$result = file_get_contents(CONSUMER_HOST . "/tools/order_sms.php?order_id=$order_id&status_to=$status_to&user_id=$user_id");
	
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>