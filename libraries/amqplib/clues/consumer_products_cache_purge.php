<?php
/**
 * This consumer takes the data from update third price queue 
 * and calculates and updates the third price of the products
 *
 *
 */
use PhpAmqpLib\Connection\AMQPConnection;

define('AREA', 'A');
define('AREA_NAME', 'admin');
define('PURGE_REQUEST_API_END_POINT','cache');
ini_set('max_execution_time', 0);

require_once dirname(__FILE__). '/config.php';
require_once dirname(__FILE__) . '/CachePurge.php';
require_once dirname(__FILE__) . '/../../core/class.registry.php';
require_once dirname(__FILE__) . '/../../config.local.php';
require_once dirname(__FILE__) . '/../../../vendor/Analog/vendor/analog/analog/lib/Analog.php';

Registry::set('config', $config);


Analog::handler (Analog\Handler\File::init (Registry::get('config.web_log_file_path')));
Analog::$default_level = Registry::get('config.THRESHOLD_LOG_LEVEL');
Analog::$default_levels_domain_module = Registry::get('config.DOMAIN_MODULE_THRESHOLD_LOG_LEVEL');


$purgeCache = new CachePurge();
$purgeCache->purgeCache();
unset($purgeCache);

