<?php

require(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'incomplete_open_ivr_exchange';
$queue = 'incomplete_open_ivr_queue';
$consumer_tag = 'ivr_key';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/**
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/**
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg)
{
	usleep(1000); // Waiting for .1 sec
	$product_data = json_decode($msg->body,true);

    $working = '';
	if(isset($product_data['order_id'])){
		$working=file_get_contents(CONSUMER_HOST."/tools/order_ivr_call_consumer.php?product_name=".urlencode($product_data['product_name'])."&product_id=".$product_data['product_id']."&retry=".$product_data['retry']."&status=".$product_data['status']."&order_id=".$product_data['order_id']."&timestamp=".$product_data['timestamp']."&phone=".urlencode($product_data['phone'])."&category_id=".$product_data['category_id']."&email=".urlencode($product_data['email'])."&s_city=".urlencode($product_data['s_city'])."&s_state=".urlencode($product_data['s_state'])."&s_zipcode=".$product_data['s_zipcode']);

    }


    if (trim($working) == '' || trim($working) == 'true') {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}

/**
  * queue: Queue from where to get the messages
  * consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while(count($ch->callbacks)) {
    $ch->wait();
}
