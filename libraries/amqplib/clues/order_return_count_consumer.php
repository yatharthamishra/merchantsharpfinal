<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'return_product_count_exchange';
$queue = 'return_product_count';
$consumer_tag = 'consumer_notification_queue';

// $exchange = 'order_notification_exchange';
// $queue = 'order_notification_queue';
// $consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg)
{
	$product_data = json_decode($msg->body,true);
    $product_id = $product_data['id'];

        if($product_data['retry'] < 3 && !empty($product_id) )
        {
        // $result  = exec("/usr/bin/lynx http://localhost/AP-88-rmq-publisher-order-notification/tools/order_notification.php?notify=$notify&order_id=$order_id&status=$status > /dev/null 2>&1 &");
        // echo $result = exec("/usr/bin/php /var/www/AP-88-rmq-publisher-order-notification/tools/order_notification.php?notify=$notify&order_id=$order_id&status=$status);
            $result = file_get_contents(CONSUMER_HOST."/tools/order_return_count_rabbitmq.php?&product_id=$product_id");

            if($result == 'false')
            {
                $product_id_array=array();
                $product_id_array[0]['id']=$product_data['id'];
                $product_id_array[0]['retry']=$product_data['retry']+1;
                send_data_to_queue($product_id_array,$exchange,$key);
            } 
            else 
            {
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            }
        }
        else
        {
            error_log("No product_id for return count or product_data[retry] >3 in amqplib/clues/order_return_count_consumer.php", 1, "vinay.gupta@shopclues.com");
            
        }

        
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

