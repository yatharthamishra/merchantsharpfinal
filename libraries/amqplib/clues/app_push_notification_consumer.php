<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'app_push_notification_exchange';
$queue = 'app_push_notification_queue';
$consumer_tag = 'app_push_notification_tag';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
	$order_data = json_decode($msg->body,true);

	$order_id = $order_data['order_id'];
	$to_status = $order_data['to_status'];
	
	$result = file_get_contents(CONSUMER_HOST . "/tools/app_push_notification.php?order_id=$order_id&to_status=$to_status");
	// print_r($result);die;
    
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>