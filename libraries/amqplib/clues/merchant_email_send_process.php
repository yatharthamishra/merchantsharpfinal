<?php
require_once(__DIR__.'/../../../main.inc.php');
include INCLUDE_DIR . 'api.tickets.php';
require_once(INCLUDE_DIR . 'class.task.php');
require_once(INCLUDE_DIR . 'class.returnStatusCron.php');
use PhpAmqpLib\Connection\AMQPConnection;
global $cfg;
global $disableDefaultShutdownFunction;
$disableDefaultShutdownFunction=1;
register_shutdown_function('shutdown1');
$inputProcessMessage="";
while($data = fgets(STDIN)){
$inputProcessMessage.=$data;
}
$msg = unserialize(($inputProcessMessage));
global $inputQueueMessage;
$inputQueueMessage = $msg;
global $entityId;
$entityId = 0;
try {
    $logConfig = logConfig();
    $domain = $logConfig["domain"]["D1"];
    $module = $logConfig["module"][$domain]["D1_M1"];
    $logObj = new Analog_logger();
    $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TICKET CREATION EMAIL QUEUE COMPLETE", "ticker_id:" . $ticketId, "", "", "VARS:" . substr(json_encode($message), 0, 2000));
    $messageBody = unserialize($msg->body);
    $mailEntityType = $messageBody['entity'];
    $mailMessageVersion = $messageBody["message_version"];
    $entityId = getTicketID($messageBody['data']);
    $mailMessageVersion = 1;
    switch ($mailMessageVersion) {
        case 1 :
            switch ($mailEntityType) {
                case 'ticketCreation':
                    mailOnTicketCreation($messageBody['data']);
                    break;
                case 'postReply':
                    mailOnCannedReply($messageBody['data']);
                    break;
                case 'taskClosureAutoReply':
                    mailOnTaskClosure($messageBody['data']);
                    break;
                case 'InfoRequredTask':
                    mailOnInfoRequiredTask($messageBody['data']);
                    break;
            }
    }
} catch (Exception $e) {
    logException('Exception:' . $e->getMessage(), $msg, $queue);
}

// no thread entry involved

function mailOnCannedReply($messageBody){
    
    $objectPassed=  unserialize(($messageBody['objects']));
    $ticketObject=$objectPassed['ticket'];
    $emailObject=$objectPassed['email'];
    $msg=$messageBody['msg'];
    $attachments=$messageBody['attachments'];
    $options=$messageBody['options'];
    $emailObject->send($ticketObject->getOwner(), $msg['subj'], $msg['body'], $attachments, $options);
}

// thread entry involved
function mailOnTicketCreation($messageBody) {
    $objectPassed = unserialize($messageBody['objects']);
    $ticketObject = $objectPassed['ticket'];
    $message = $objectPassed['message'];
    $vars = $messageBody['vars'];
    $autorespond = $messageBody['autorespond'];
    $alertstaff = $messageBody['alertstaff'];
    $ticketObject->onNewTicket($message, $autorespond, $alertstaff, $vars);
}

/**
 * 
 * @param type $messageBody
 * required object in mail task , ticket
 * thread entry involved
 */
function mailOnInfoRequiredTask($messageBody) {
    $objectPassed = unserialize($messageBody['objects']);
}

/**
 * 
 * @param type $messageBody
 * required object in mail task object , ticket object
 * thread entry involved 
 */
function mailOnTaskClosure($messageBody) {
    $objectPassed = unserialize($messageBody['objects']);
    $ticket = $objectPassed['ticket'];
    $task = $objectPassed['task'];
    $taskData = $objectPassed['taskData'];
    $dept = $objectPassed['dept'];
    $email = $objectPassed['email'];
    $message = $messageBody['message'];
    $autoSentMessage = $messageBody['autoSentMessage'];
    $msg=$objectPassed['msg'];  
    $msg = $ticket->replaceVars(
            $msg->asArray(), array('message' => $message,
        'recipient' => $ticket->getOwner(),
        'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : ''
            )
    );
    $options = array();
    $email->sendAutoReply($ticket->getOwner(), $msg['subj'], $msg['body'], null, $options);
    $autoSentMessage["note"] = "<strong>Auto Mail -</strong> <br>" . $msg['subj'] . "<br>" . $msg['body'];
    $ticket->postNote($autoSentMessage);
    $taskData->autocommunication = 1;
    $taskData->save();
}

function getTicketID($messageBody) {
    $objectPassed = unserialize($messageBody['objects']);
    $ticket = $objectPassed['ticket'];
    if ($ticket instanceof Ticket) {
        $ticket_id = $ticket->getId();
        return $ticket_id;
    }
    return 0;
}
function shutdown1() {

    $lastError = error_get_last();
    if ($lastError['type'] === E_ERROR) {
        logException('shutdown: ' . json_encode($lastError));
    }
}
function logException($errorMessage) {
    global $inputQueueMessage;
    $serverDetails = db_input(HOST . "|" . PORT . "|" . USER . "|" . PASS . "|" . VHOST)?:"''";
    $messageBody = json_decode($inputQueueMessage->body,true);
    $messageJsonForm=db_input($inputQueueMessage->body);
    $exchangeUsed = db_input($inputQueueMessage->delivery_info['exchange'])?:"''";
    $routingKeyUsed = db_input($inputQueueMessage->delivery_info['routing_key'])?:"''";
    $queueIdentifier = db_input("$exchangeUsed|$routingKeyUsed")?:"''";
    $ticketId = (int) $messageBody['ticketId']?:"''";
    $entityName = db_input($messageBody['entity'])?:"''";
    $debugBacktrace=json_encode(debug_backtrace());
    $errorMessage=$errorMessage."---".$debugBacktrace;
    $errorMessage = db_input($errorMessage);
    global $entityId;
    $entityId = (int) $entityId;
    $sql = "insert into mst_consumer_error_log (entityId,entityType,queueIdentifier,data,serverDetails,exceptionOccured) values (
             '$entityId',$entityName,$queueIdentifier,$messageJsonForm,$serverDetails,$errorMessage)";
    db_query($sql);
}

?>

