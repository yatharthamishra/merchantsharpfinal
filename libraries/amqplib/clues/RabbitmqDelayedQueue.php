<?php

require_once 'config.php';


use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
/**
 * Delayed queue connection class 
 */
class RabbitmqDelayedQueue {
    

    const PASSIVE_TRUE              = false ;
    
    const PASSIVE_FALSE             = false ;
    
    const DURABLE_TRUE              = true ;
    
    const DURABLE_FALSE             = false ;
    
    const EXCLUSIVE_TRUE            = true ;
    
    const EXCLUSIVE_FALSE           = false ;
    
    const AUTO_DELETE_FALSE         = false ;
    
    const AUTO_DELETE_TRUE          = true ;
    
    const NO_WAIT_TRUE              = true ;
    
    const NO_WAIT_FALSE             = false ;
    
    const NO_LOCAL_FALSE            = false ;
    
    const NO_LOCAL_TRUE             = true ;

    const NO_ACK_FALSE              = false;
    
    const NO_ACK_TRUE               = true ;
    
    const DELIVERY_MODE_PERSISTENT  = 2 ;
    
    /**
     *
     * @var string name of delayed queue 
     */
    private $_delayed_exchange ;
    /**
     *
     * @var string name of delayed queue 
     */
    private $_delayed_queue ;
    /**
     *
     * @var string exchange name 
     */
    private $_exchange ;
    
    /**
     *
     * @var string main queue name 
     */
    private $_queue ;
    /**
     *
     * @var int delayed time  
     */
    private $_delayed_time ;
    /**
     *
     * @var resource rabbitmq channel 
     */
    public $rabbitmq_channel      = null ;
    /**
     *
     * @var resource rabbitmq connection 
     */
    private $_rabbitmq_connection   = null ;
    
    /**
     * 
     * @param string $delayed_exchange_name name of delayed exchange
     * @param string $delayed_queue_name 
     * @param string $exchange_name
     * @param string $queue_name
     * @param int $delayed_time delayed time in seconds
     * @return boolean
     * @throws Exception
     */
    public function __construct( 
            $delayed_exchange_name , 
            $delayed_queue_name , 
            $exchange_name ,
            $queue_name ,
            $delayed_time ) {
        
        if( !(defined('HOST') && defined('PORT') && defined('USER') 
                && defined('PASS') && defined('VHOST'))){
            throw new Exception('Please include rabbitmq config for connection params 
                HOST,PORT,USER,PASS');
        }
        if( empty($delayed_exchange_name)
            ||empty($delayed_queue_name)
            ||empty($queue_name)
            ||empty($exchange_name)    
            ||empty($delayed_time)){
            throw new Exception("Please provide queue , 
                delayed queue name and delayed time",701);
        }
        $this->_delayed_exchange    = $delayed_exchange_name;
        $this->_delayed_queue       = $delayed_queue_name;
        $this->_exchange            = $exchange_name;
        $this->_queue               = $queue_name;
        $this->_delayed_time        = $delayed_time;
        
        return $this->_initRabbitmq() ;
    }
    /**
     * initilaize rabbitmq 
     * initilaize delayed and main exchange-queue
     */
    private function _initRabbitmq(){
        
        try {
            // create connection
            $this->_rabbitmq_connection = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);


            // create a channel
            $channel = $this->_rabbitmq_connection->channel();

            // create the right.now.queue, 
            // the exchange for that queue and bind them together
            $channel->queue_declare( $this->_queue,self::PASSIVE_FALSE,                          
                    self::DURABLE_TRUE, self::EXCLUSIVE_FALSE,self::AUTO_DELETE_FALSE                      
                    );

            $channel->exchange_declare( $this->_exchange, 'direct' );

            $channel->queue_bind( $this->_queue, $this->_exchange, $this->_queue );

            // now create the delayed queue and the exchange
            $channel->queue_declare( $this->_delayed_queue, self::PASSIVE_FALSE,
                    self::DURABLE_FALSE,self::EXCLUSIVE_FALSE, self::AUTO_DELETE_TRUE,
                    self::NO_WAIT_TRUE,
                    array(                          //arguments
                        'x-message-ttl'             => array('I', $this->_delayed_time * 1000), // delay in seconds to milliseconds
                        //"x-expires"                 => array("I", $this->_delayed_time * 1000 + 2000),
                        'x-dead-letter-exchange'    => array('S', $this->_exchange), // after message expiration in delay queue, move message to the right.now.queue
                        'x-dead-letter-routing-key' => array('S', $this->_queue)
                    )
            );

            $channel->exchange_declare( $this->_delayed_exchange, 'direct');

            $channel->queue_bind( $this->_delayed_queue, $this->_delayed_exchange ,  
                        $this->_delayed_queue
                    );

            $channel->set_nack_handler(
                        function(AMQPMessage $message) {
                            Analog::log ('purge_cache_service','Unable to send msg', 
                                "Cache Purge Service", "ADMIN",
                                Registry::get('config.LOG_LEVELS.ERROR'));
                        }
                );

            $channel->set_ack_handler(
                        function(AMQPMessage $message) {
                            Analog::log ('purge_cache_service','Successfully msg sended', 
                                "Cache Purge Service", "ADMIN",
                                Registry::get('config.LOG_LEVELS.DEBUG'));
                        }
                );

            $channel->confirm_select();

            $this->rabbitmq_channel    = $channel;

        } catch (Exception $exc) {
            Analog::log ('purge_cache_service',$exc->getMessage(), 
                    "Cache Purge Service", "ADMIN",
                    Registry::get('config.LOG_LEVELS.ERROR'));
        }
    }
    
    /**
     * 
     * @param string $msg json encoded msg publish 
     * @return boolean
     */
    public function publish( $msg ){
        try {
            
            $message = new AMQPMessage(
                        $msg, array(
                                    'content_type'  => 'text/plain',
                                    'delivery_mode' => self::DELIVERY_MODE_PERSISTENT
                                    )
                        );
        
            

            $message->setBody($msg);
            
            $this->rabbitmq_channel->basic_publish($message, 
                                        $this->_delayed_exchange, 
                                        $this->_delayed_queue 
                                    );
            $this->rabbitmq_channel->wait_for_pending_acks();
            
            return true;
        } catch (Exception $exc) {
            Analog::log ('purge_cache_service',$exc->getMessage(), 
                    "Cache Purge Service", "ADMIN",
                    Registry::get('config.LOG_LEVELS.ERROR'));
        }

    }
    /**
     * close connection and channel
     */
    public function close(){
        //$this->_rabbitmq_connection->close();
        //$this->rabbitmq_channel->close();
    }
}