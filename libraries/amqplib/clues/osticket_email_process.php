<?php
define('AREA','A');
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;


$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
$ch->exchange_declare($exchange, 'direct', true, false, false);
$ch->queue_bind($queue, $exchange,$key);

function process_message($msg){
	$data = json_decode(json_decode($msg->body,true),true);
        if(is_array($data)){
            $dataForQueue = $data;//array("ticket_id"=>$ticket->getId(),"message"=>$message,"autorespond"=>$autorespond,"alertstaff"=>$alertstaff,"vars"=>$vars);
            $ticketId = $dataForQueue['ticket_id'];
            $message = $dataForQueue["message"];
            $autorespond = $dataForQueue["autorespond"];
            $alertstaff = $dataForQueue["alertstaff"];
            $vars = $dataForQueue["vars"];

            $ticketObj = Ticket::lookup($ticketId);
            if($ticketObj)
                $ticketObj->onNewTicket($message, $autorespond, $alertstaff,$vars);
        }
        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D1"];
        $module = $logConfig["module"][$domain]["D1_M1"];        
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TICKET CREATION EMAIL QUEUE COMPLETE", "ticker_id:".$ticketId,"", "", "VARS:".substr(json_encode($message),0,2000));
	
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_qos(null, 1, null);
$ch->basic_consume($queue, false, false, false, false, false, 'process_message');


function shutdown1($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown1', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>

