<?php

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'bulk_order_status_change_batch_exchange';
$queue = 'bulk_order_status_change_batch_queue';
$consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
  The following code is the same both in the consumer and the producer.
  In this way we are sure we always have a queue to consume from and an
  exchange where to publish messages.
 */

/*
  name: $queue
  passive: false
  durable: true // the queue will survive server restarts
  exclusive: false // the queue can be accessed in other channels
  auto_delete: false //the queue won't be deleted once the channel is closed.
 */
$ch->queue_declare($queue, false, true, false, false);

/*
  name: $exchange
  type: direct
  passive: false
  durable: true // the exchange will survive server restarts
  auto_delete: false //the exchange won't be deleted once the channel is closed.
 */

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg) {
    $order_data = json_decode($msg->body, true);

    //print_r($order_data);
    if ($order_data['mode'] == "bulk_change_status") {


        $batch_id = $order_data['batch_id'];
        $parent_id = $order_data['parent_id'];
        $order_id = trim($order_data['order_id']);
        $date = $order_data['date'];
        $pos = $order_data['pos'];
        $p_pos = $order_data['p_pos'];


        $url = CONSUMER_HOST . "/tools/bulk_order_status_change_from_batch_consumer_process.php?batch_id=" . $batch_id . "&parent_id=" . $parent_id . "&order_id=".$order_id."&pos=" . $pos . "&p_pos=" . $p_pos."&date=".urlencode($date);
        //echo "\n".$url."\n";  //exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $data = curl_exec($ch);
        //echo $data;
        curl_close($ch);
        if($data=="done")
        {
          $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
          //echo $order_id."\n";
        }
    }
}

/*
  queue: Queue from where to get the messages
  consumer_tag: Consumer identifier
  no_local: Don't receive messages published by this consumer.
  no_ack: Tells the server if the consumer will acknowledge the messages.
  exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
  nowait:
  callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

