<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$queue = 'store_status_email_queue';
$consumer_tag = 'consumer_store_status_email';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
	$data = json_decode($msg->body,true);
	$url_data = urlencode(serialize($data));	
	
	$result = file_get_contents(CONSUMER_HOST . "/tools/store_status_email.php?data=$url_data");
	
    if ($result) {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>