<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Jira : RPT-195, Dev:Anoop, Date:01-Feb-2016
 */

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
$exchange='orderRevenueDataMasterExchange';
$queue='orderRevenueDataMasterQueue';
$consumer_tag = 'orderRevenueDataMasterTag';
$key='orderRevenueDataMasterKey';
$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
$ch->queue_declare($queue, false, true, false, false);
function updateOrderRevenueDataMaster($msg) {
    $order = json_decode($msg->body,true);
    $result = 'false';
    
    require_once dirname(__FILE__) . '/../../tools/productSummaryConsumer.php';
    if(revenueDataMasterOrderRefresh($order['order_ids'],$order['billing'])) {
        $result = 'true';
    }
    if($result == 'true'){
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        Logging::log_data(array('DATA'=>$result,'QUEUE'=>'orderRevenueDataMasterQueue'),'MESSAGE','SUCCESS','../../images/logs/orderRevenueDataMasterConsumer.log');
    }
//    else {
//        Logging::log_data(array('DATA'=>$result,'QUEUE'=>'orderRevenueDataMasterQueue'),'MESSAGE','PUBLISHING','../../images/logs/orderRevenueDataMasterConsumer.log');    
//        //Connection for Delay Queue
//        $AMQPConnection = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
//        $channel = $AMQPConnection->channel();    
//        //Publishing to Delay Queue
//        $channel->basic_publish($msg,'orderRevenueDataMasterExchange','orderRevenueDataMasterKey');
//        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
//        $channel->close();
//        $AMQPConnection->close();
//    }
}
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'updateOrderRevenueDataMaster');
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);
while (count($ch->callbacks)) {
    $ch->wait();
}