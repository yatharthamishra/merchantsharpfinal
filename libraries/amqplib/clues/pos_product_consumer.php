<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'create_pos_product_exchange';
$queue = 'create_pos_product_queue';
$consumer_tag = 'create_pos_product_exchange_tag';


$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);


function process_message($msg) {
    $product_id = json_decode($msg->body,true);
    $result = file_get_contents(CONSUMER_HOST . "/tools/pos_product_tools.php?product_id=$product_id");
    
    if($result){
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>