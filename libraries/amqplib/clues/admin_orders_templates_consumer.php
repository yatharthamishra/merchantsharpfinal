<?php
/**
 *  Consumer for sending emails from order details page: (e.g reutrns,shipment-delay)
 *  Vishal Sachdeva: 8 APRIL 15
 */

include (__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'admin_order_templates_exchange';
$queue = 'admin_order_templates_queue';
$consumer_tag = 'consumer_admin_order_templates';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg) {
    $alert_data = json_decode($msg->body, true); 
    $notify     = $alert_data['notify'];
    $templateid = $alert_data['template_id'];
    $orderid   =  $alert_data['order_id'];
    $senderid  =  $alert_data['sender_id'];
    $result = file_get_contents(CONSUMER_HOST . "/tools/admin_order_templates_mail.php?notify=$notify&template_id=$templateid&orderid=$orderid&sender_id=$senderid");
    
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

?>

