<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Logging\Logging;
/**
 *  Test Publisher
 *
 *  Usage : limit-> number of messages to be sent
 *
 *        : msg-> message body of each message
 *
 **/


try
{
  $conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
  $ch = $conn->channel();
}
catch (Exception $e)
{
  Logging::log_data(array('EXCEPTION'=>$e->getMessage()),'EXCEPTION','FAILURE','/tmp/rabbit_publisher.log');
  return false;
}  

$ch->set_nack_handler(
  function(AMQPMessage $message)
  {
    //echo "NACKNOWLEDGING " . $message->body .PHP_EOL; 
    Logging::log_data($message->body,'MESSAGE_NACK','FAILURE','/tmp/test_publisher.log');
  }
);


$ch->set_ack_handler(
  function(AMQPMessage $message)
  {
    //echo "ACKNOWLEDGING " . $message->body .PHP_EOL;
    Logging::log_data($message->body,'MESSAGE_ACK','SUCCESS','/tmp/test_publisher.log');
  }
);

$ch->confirm_select();

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
 */

$exchange = 'bench_exchange';
try
{
  $ch->exchange_declare($exchange, 'direct', true, true, false);
}
catch (Exception $e)
{
  Logging::log_data(array('EXCEPTION'=>$e,'EXCHANGE'=>$exchange),'EXCEPTION','FAILURE','/tmp/test_publisher.log');
  return false;
}


$limit    = ($_REQUEST['limit']?$_REQUEST['limit']:'1');
$msg_body = ($_REQUEST['msg']?$_REQUEST['msg']:"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
$msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));

$i=0;

for($i; $i<$limit ;$i++)
{
  $msg_body_new = $msg_body.$i;

  $msg->setBody($msg_body_new);

  $ch->basic_publish($msg, $exchange);

  Logging::log_data(array('BODY'=>$msg_body_new,'EXCHANGE'=>$exchange),'MESSAGE','PUBLISHING','/tmp/test_publisher.log');

}
try
{
  $ch->wait_for_pending_acks();
}
catch (Exception $e)
{
  Logging::log_data(array('EXCEPTION'=>$e->getMessage(),'EXCHANGE'=>$exchange),'EXCEPTION','FAILURE','/tmp/rabbit_publisher.log');
  return false;
}  
try
{
  $ch->close();
  $conn->close();
}
catch (Exception $e)
{
  Logging::log_data(array('EXCEPTION'=>$e->getMessage(),'EXCHANGE'=>$exchange),'EXCEPTION','FAILURE','/tmp/rabbit_publisher.log');
  return false;
}  
?>

