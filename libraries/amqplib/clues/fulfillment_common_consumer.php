<?php
include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'fulfillment_common_exchange';
$queue = 'fulfillment_common_queue';
$consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
  The following code is the same both in the consumer and the producer.
  In this way we are sure we always have a queue to consume from and an
  exchange where to publish messages.
 */

/*
  name: $queue
  passive: false
  durable: true // the queue will survive server restarts
  exclusive: false // the queue can be accessed in other channels
  auto_delete: false //the queue won't be deleted once the channel is closed.
 */
$ch->queue_declare($queue, false, true, false, false);

/*
  name: $exchange
  type: direct
  passive: false
  durable: true // the exchange will survive server restarts
  auto_delete: false //the exchange won't be deleted once the channel is closed.
 */

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg) {
    $order_data = json_decode($msg->body, true);

    //print_r($order_data);exit;
    if ($order_data['mode'] == "fulfillment_batch_builder") {
        $id = $order_data['id'];
        $url = CONSUMER_HOST . "/tools/fulfillment_common_consumer_process.php?id=". $id."&mode=fulfillment_batch_builder";
        //echo "\n".$url."\n";  exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $data = curl_exec($ch);
        //echo $data;
        curl_close($ch);
        if($data=="done")
        {
          $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
          //echo $order_id."\n";
        }
    }
    
    
    else if ($order_data['mode'] == "bluedart_realtime_awb") {


        $order_id = $order_data['order_id'];
        $url = CONSUMER_HOST . "/tools/fulfillment_common_consumer_process.php?order_id=".$order_id."&mode=bluedart_realtime_awb";
        
        
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $data = curl_exec($ch);
//        echo $data;
        curl_close($ch);
        
      if($data=="done")
        {
          $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
          //echo $order_id."\n";
        }
    }
    else if ($order_data['mode'] == "cancel_shipment") {

        $order_id = $order_data['order_id'];
        $mode = $order_data['mode'];
        $url = CONSUMER_HOST . "/tools/fulfillment_common_consumer_process.php?order_id=" . $order_id . "&mode=" . $mode;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $data = curl_exec($ch);
        curl_close($ch);

        if ($data == "done") {
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }
    }
    else if ($order_data['mode'] == "dss_cost_beyond_threshold") {				
						
        $order_id = $order_data['order_id'];				
        $mode = $order_data['mode'];				
        $url = CONSUMER_HOST . "/tools/fulfillment_common_consumer_process.php?order_id=" . $order_id . "&mode=" . $mode;				

        $ch = curl_init();				
        curl_setopt($ch, CURLOPT_URL, $url);				
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);				
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);				
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);				
        $data = curl_exec($ch);				
        curl_close($ch);				

        if ($data == "done") {				
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);				
        }				
    }
    else if($order_data['mode'] == "process_tracking_response"){
        
        $order_id = $order_data['tracking_details']['order_id'];				
        $mode = $order_data['mode'];
        $from_status = $order_data['tracking_details']['from_status'];
        $to_status = $order_data['tracking_details']['to_status'];
        $carrier_name = str_replace(" ", "_", $order_data['tracking_details']['carrier_name']);
        $shipment_type = $order_data['tracking_details']['shipment_type'];
        $shipment_data = $order_data['tracking_details']['shipment_data'];
        $awbno = $order_data['tracking_details']['awbno'];
        $rules_from_status = $order_data['tracking_details']['rules_from_status'];
        $location = str_replace(" ", "_", $$order_data['tracking_details']['location']);
        $carrier_id = $order_data['carrier_id'];
        $shipment_id = $order_data['tracking_details']['shipment_id'];
        $update_date_time = str_replace(" ", "_", $order_data['tracking_details']['update_date_time']);
    
        $url = CONSUMER_HOST . "/tools/fulfillment_common_consumer_process.php?mode=" .$mode. "&order_id=" .$order_id. "&from_status=" .$from_status. "&to_status=" .$to_status. "&carrier_name=" .$carrier_name. "&shipment_type=" .$shipment_type. "&shipment_data=" .$shipment_data. "&awbno=" .$awbno. "&rules_from_status=" .$rules_from_status. "&carrier_id=" .$carrier_id. "&shipment_id=" .$shipment_id. "&update_date_time=" .$update_date_time."&location=" .$location;
      
        $ch = curl_init();				
        curl_setopt($ch, CURLOPT_URL, $url);				
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);				
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);				
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);				
        $data = curl_exec($ch);				
        curl_close($ch);
        
             if ($data == "done") {				
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);				
        }
    }
}


/*
  queue: Queue from where to get the messages
  consumer_tag: Consumer identifier
  no_local: Don't receive messages published by this consumer.
  no_ack: Tells the server if the consumer will acknowledge the messages.
  exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
  nowait:
  callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

