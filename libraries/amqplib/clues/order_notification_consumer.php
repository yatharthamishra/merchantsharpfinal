<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$queue = 'order_notification_queue';
$consumer_tag = 'consumer_order_notification';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg) {

    $order_data = json_decode($msg->body, true);

    $consumer_script_type = '';

    if (isset($order_data['consumer_script_type'])) {
        $consumer_script_type = $order_data['consumer_script_type'];
    }

    $user_id = '';

    if (isset($order_data['user_id'])) {
        $user_id = $order_data['user_id'];
    }

    if ($consumer_script_type == "m") {

        $notify = $order_data['notify'];
        $order_id = $order_data['order_id'];
        $status = $order_data['status'];

        $email_source = '';

        if (isset($order_data['email_source'])) {
            $email_source = $order_data['email_source'];
        }

        $result = file_get_contents(CONSUMER_HOST . "/tools/order_notification.php?notify=$notify&order_id=$order_id&status=$status&email_source=$email_source&user_id=$user_id");

    } else if ($consumer_script_type == "p") {
        
        file_put_contents("/tmp/bulk_order_status_consumer.log", "consumer-" . date('Y-m-d H:i:s') . "\n" . print_r($order_data, true) . "\n", FILE_APPEND);

        $order_id = $order_data['order_id'];
        $status_fr_to = $order_data['status_fr_to'];
        $fr = $status_fr_to['status_from'];
        $to = $status_fr_to['status_to'];
        $force_notification = serialize($order_data['force_notification']);
        $url = CONSUMER_HOST . "/tools/order_status_consumer_process.php";

        $fields = array(
            'order_id' => $order_id,
            'fr' => $fr,
            'to' => $to,
            'force_notification' => urlencode($force_notification));

        $fields_string = "";

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');
        file_put_contents("/tmp/bulk_order_status_consumer.log", $url . "\n", FILE_APPEND);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (curl_exec($ch) === false) {
            file_put_contents("/tmp/bulk_order_status_consumer.log", curl_error($ch) . "\n", FILE_APPEND);
        }

        curl_close($ch);
        
    } else {

        $notify = $order_data['notify'];
        $order_id = $order_data['order_id'];
        $status = $order_data['status'];

        $result = file_get_contents(CONSUMER_HOST . "/tools/order_notification.php?notify=$notify&order_id=$order_id&status=$status&user_id=$user_id");
    }

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>