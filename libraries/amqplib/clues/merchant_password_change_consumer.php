<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;



$exchange = 'merchant_password_exchange';
$queue = 'merchant_password_queue';
$consumer_tag = 'consumer_merchant_password';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);


function process_message($msg)
{
    
      $data = json_decode($msg->body,true);
      $temp = explode("---", $data);
      $email_id = $temp[0];
      $password = $temp[1];
     
     $result = file_get_contents(CONSUMER_HOST . "/tools/change_password.php?email_id=$email_id&password=$password");
	
    if ($result) {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
} 
 

$ch->basic_consume($queue, $exchange, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

?>

