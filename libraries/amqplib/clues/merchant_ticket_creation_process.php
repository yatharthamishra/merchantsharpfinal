<?php
require_once(__DIR__.'/../../../main.inc.php');
include INCLUDE_DIR . 'api.tickets.php';
require_once(INCLUDE_DIR.'class.task.php');
require_once(INCLUDE_DIR.'class.returnStatusCron.php');
use PhpAmqpLib\Connection\AMQPConnection;
global $cfg;
global $disableDefaultShutdownFunction;
$disableDefaultShutdownFunction=1;
$inputProcessMessage="";
while($data = fgets(STDIN)){
$inputProcessMessage.=$data;
}
$msg = unserialize(($inputProcessMessage));
global $inputQueueMessage;
global $mapIds;
$mapIds=array();
$inputQueueMessage = $msg;
register_shutdown_function('shutdown1');
try {
    $logConfig = logConfig();
    $domain = $logConfig["domain"]["D1"];
    $module = $logConfig["module"][$domain]["D1_M1"];
    $logObj = new Analog_logger();
    $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TICKET CREATION EMAIL QUEUE COMPLETE", "ticker_id:" . $ticketId, "", "", "VARS:" . substr(json_encode($message), 0, 2000));
    $messageBody = unserialize($msg->body);
    $ticket = unserialize($messageBody['ticket']);
    $data = ($messageBody['data']);
    $callingObject = unserialize($messageBody['callingobject']);
    $ticketId = $messageBody['ticketId'];
    $messageVersion = $messageBody['messageVersion'];
    $merchantMessage=unserialize($messageBody['merchantMessage']);
    //$data["fulfillment_id"] = $data["merchant_type"];
    switch ($messageVersion) {
        case MERCHANT_CONSUMER_VERSION:
            $orderRelatedData = Task::getOrderInfoMap($data);
            $tasksToBeMade = Task::firstTasksToBeMade($data, $orderRelatedData, $ticket);
            if (empty($tasksToBeMade)) {
                $ticket->sendMail($ticket,$data,$merchantMessage);
                $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TASK MAPPING NOT FOUND", "ticker_id:".$ticketId,"Calling Side:Consumer", "", "VARS:".substr(json_encode($data),0,2000));
            }else{
            $dataTask["task"] = $tasksToBeMade;
            $dataTask["ticketId"] = $ticket->getId();
            $dataTask["duedate"] = $ticket->est_duedate;
            $dataTask["source"] = 2; // On ticket Create
            $dataTask["orderReportingMasterData"] = $orderInfoMap;

            $task = $callingObject->createTask($dataTask);
            $sql = 'select max(ceil((TIMESTAMPDIFF(SECOND,created,duedate))/(24*60*60))) as max_sla from mst_task where object_id='.$dataTask["ticketId"];
            $sla = db_result(db_query($sql));
            $ticket->sendMail($ticket,$data,$merchantMessage,$sla);
            }
            if (!empty($data['attachment'])) {

               $userId = $ticket->getUserId();

                foreach ($task as $t) {
                    $t->taskAttachments($data, NULL, $userId);
                }

                $ticket->attachments($data['attachment']);
            }

            if (empty($dataTask)) {
                return;
            }

    }
} catch (Exception $e) {
    logException('Exception:' . $e->getMessage(), $msg);
}

function shutdown1() {
    $lastError = error_get_last();
    if ($lastError['type'] === E_ERROR) {
        logException('shutdown: ' . json_encode($lastError));
    }
}

function logException($errorMessage) {
    global $inputQueueMessage;
    $serverDetails = db_input(HOST . "|" . PORT . "|" . USER . "|" . PASS . "|" . VHOST)?:"''";
    $messageBody = json_decode($inputQueueMessage->body, true)?:"''";
    $exchangeUsed = $inputQueueMessage->delivery_info['exchange']?:"''";
    $routingKeyUsed = $inputQueueMessage->delivery_info['routing_key']?:"''";
    $queueIdentifier = db_input("$exchangeUsed|$routingKeyUsed")?:"''";
    $ticketId = $messageBody['ticketId']?:"''";
    $ticketId=(int)$ticketId;
    $errorMessage=  db_input($errorMessage)?:"''";
    $body=  db_input($inputQueueMessage->body)?:"''";
    $sql = "insert into mst_consumer_error_log (entityId,entityType,queueIdentifier,data,serverDetails,exceptionOccured) values (
             '$ticketId','ticket',$queueIdentifier,$body,$serverDetails,$errorMessage)";
    db_query($sql);
}


?>
