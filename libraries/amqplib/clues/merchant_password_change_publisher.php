<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Logging\Logging;

function send_data_to_queue($data_arr,$exchange,$key_to_exchange)
{
   
   try
   {
        $conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
        $ch = $conn->channel();
   }
   catch (Exception $e)
   {
       Logging::log_data(array('EXCEPTION'=>$e->getMessage(),'DATA'=>$data_arr,'EXCHANGE'=>$exchange,'KEY'=>$key_to_exchange),'EXCEPTION','FAILURE','/tmp/rabbit_publisher.log');
       return false;
   }  
   
   $ch->set_nack_handler(
    function(AMQPMessage $message)
    {
      //echo "NACKNOWLEDGING " . $message->body .PHP_EOL; 
      Logging::log_data($message->body,'MESSAGE_NACK','FAILURE','/tmp/rabbit_publisher.log');
    }
  );


  $ch->set_ack_handler(
    function(AMQPMessage $message)
    {
      //echo "ACKNOWLEDGING " . $message->body .PHP_EOL;
      Logging::log_data($message->body,'MESSAGE_ACK','SUCCESS','/tmp/rabbit_publisher.log');
    }
  );

  $ch->confirm_select();

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
 */
  
 try
 {
     $ch->exchange_declare($exchange, 'direct', true, true, false);
 }
 catch (Exception $e)
 {
     Logging::log_data(array('EXCEPTION'=>$e,'DATA'=>$data_arr,'EXCHANGE'=>$exchange,'KEY'=>$key_to_exchange),'EXCEPTION','FAILURE','/tmp/rabbit_publisher.log');
     return false;
 }
 
 
  $msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));

  foreach($data_arr as $data)
  {
      $msg_body = json_encode($data);
  
      $msg->setBody($msg_body);
  
      $ch->basic_publish($msg, $exchange,$key_to_exchange);

      Logging::log_data(array('DATA'=>$data,'EXCHANGE'=>$exchange,'KEY'=>$key_to_exchange),'MESSAGE','PUBLISHING','/tmp/rabbit_publisher.log');
  
  }    
  $ch->wait_for_pending_acks();
  $ch->close();
  $conn->close();
  return true;
}

?>


