<?php

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;


$exchange='solr_update_fields_exchange';
$queue='solr_update_fields_queue';
$consumer_tag = 'consumer_solr_update_fields';
$key='update_fields_solr';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */

function update_solr($msg)
{
    
    $_data = json_decode($msg->body,true);
    $data = $_data['data'];
    $module=$_data['module'];
    $product_data=implode("&",$data);  

       //echo CONSUMER_HOST."/tools/solr_update_fields_rmq.php?".$product_data."&module=$module";die;
       $result = file_get_contents(CONSUMER_HOST."/tools/solr_update_fields_rmq.php?".$product_data."&module=$module");
       // echo $result;die;
        
        if($result == 'true'){
            
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            Logging::log_data(array('DATA'=>$result,'QUEUE'=>'solr_update_fields_queue'),'MESSAGE','SUCCESS','/tmp/solr_rmq.log');

        }else{
            
            Logging::log_data(array('DATA'=>$result,'QUEUE'=>'solr_delay_queue'),'MESSAGE','PUBLISHING','/tmp/solr_rmq.log');
            
            //Connection for Delay Queue
            $AMQPConnection = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);

            $channel = $AMQPConnection->channel();
            
            //Publishing to Delay Queue
            $channel->basic_publish($msg,'solr_delay_exchange','solr_delay_key');

            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            $channel->close();
            $AMQPConnection->close();
        } 
    
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'update_solr');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

