<?php
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
define('AREA', 'A');
define('AREA_NAME', 'admin');
require  dirname(__FILE__) . '/../../prepare.php';
require  dirname(__FILE__) . '/../../init.php';

fn_load_addon("hp_changes");
fn_load_addon("my_changes");
/**
 *  Test Consumer
 *
 *  Consumer messages from bench_queue and logs into /tmp/consumer.log
 **/

// Specify queue name
$queue = 'bench_queue';
$consumer_tag = 'consumer_bench_queue';

try
{
  $conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
  $ch = $conn->channel();
}
catch (Exception $e)
{
  Logging::log_data($e->getMessage(),'EXCEPTION','FAILURE','/tmp/test_consumer.log');
  return false;
}  

/**
 * Checks for queue 
 * passive flag is set, so we don't actually create queue
 * 
 **/
try
{
  $ch->queue_declare($queue, true, true, false, false);
}
catch(Exception $e)
{
  Logging::log_data($e,'QUEUE_ERROR','EXCEPTION','/tmp/test_consumer.log');
  return false;
}

/**
  ===============================================
  message body is json encoded array of order_id,
  
  from_status and to status 
  ===============================================
**/

function process_message($msg)
{
    $order_id    = ($_REQUEST['order_id']?$_REQUEST['order_id']:'10474267');
    $order_info = fn_get_order_info($order_id,true);
    Logging::log_data(array('Body'=>$msg->body,'ORDER_DATA'=>$order_info,'ORDER_ID'=>$order_id),'MESSAGE','SUCCESS','/tmp/test_consumer.log');
    /**
     * Send Ack
     */ 
    $msg->delivery_info['channel']->
       basic_ack($msg->delivery_info['delivery_tag']);
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
  try
  {
    $ch->close();
    $conn->close();
  }
  catch (Exception $e)
  {
    Logging::log_data($e->getMessage(),'EXCEPTION','FAILURE','/tmp/test_consumer.log');
    return false;
  }  
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks) or 1) {
    $ch->wait();
}
