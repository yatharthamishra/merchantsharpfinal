<?php
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

// Queue Details 
$exchange = 'stockout_products_msp_exchange';
$queue = 'stockout_products_msp_queue';
$consumer_tag = 'stockout_products_msp_tag';
$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg) {
    $items = json_decode($msg->body, true);
    if(isset($items['product_ids']))
    {
       $items['ids']=$items['product_ids'];
       $items['type']='P';
    }
    $result= file_get_contents(CONSUMER_HOST . "/tools/convert_into_json_format_for_cache_purging.php?ids=".implode(',', $items['ids'])."&type=".$items['type']);
    if ($result == "true" ) {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}
