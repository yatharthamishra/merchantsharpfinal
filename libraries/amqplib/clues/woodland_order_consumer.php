<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'woodland_orders_exchange';
$queue = 'woodland_orders_queue';
$consumer_tag = 'consumer_notification_queue';


$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();


$ch->queue_declare($queue, false, true, false, false);


function process_message($msg)
{
    $order_id = json_decode($msg->body,true);
        
        if(!empty($order_id))
        {
          try{
          $result = file_get_contents(CONSUMER_HOST."/tools/woodland_orders_api.php?&order_id=$order_id");

          if($result == 'successfull'){
              
              $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
          
          }
          }catch(Exception $e)
                {
                  Logging::log_data($e,'WOODLAND_ORDER_ERROR','EXCEPTION','/tmp/woodland_oreders.log');
                }
        }
         
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

