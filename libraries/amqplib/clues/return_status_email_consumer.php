<?php
include (__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'return_status_email_exchange';
$queue = 'return_status_email_queue';
$consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);



/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg) {
    $return_data = json_decode($msg->body, true);
    
    $notify = $return_data['notify'];
    $to_status = $return_data['to_status'];
    $changed_by = $return_data['changed_by'];
    $return_id = $return_data['return_id'];
            
    $result = file_get_contents(CONSUMER_HOST . "/tools/return_status_email.php?notify=$notify&return_id=$return_id&to_status=$to_status&changed_by=$changed_by");
    // print_r(error_get_last());die;
     // print($result);die;

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

