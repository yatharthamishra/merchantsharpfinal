<?php

/**
 * @Author: PankajGoyal7
 * @Date:   2016-11-23 10:00:18
 * @Last Modified by:   PankajGoyal7
 * @Last Modified time: 2016-11-23 10:01:32
 */

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange      = 'catalog_sm_batch_complete_queue_exchange';
$queue         = 'catalog_sm_batch_complete_queue';
$consumer_tag  = 'catalog_sm_batch_complete';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();


/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
 */

function process_message($msg)
{ 
    $consumerData= $msg->body; 
    $batch_id = json_decode($consumerData);
    $url = CONSUMER_HOST . "/tools/consumer_sm_catalog_batch_complete.php?batch_id=". $batch_id;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 200);
    $response  = curl_exec($ch);
    curl_close($ch);
    if($response == "DONE"){
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
    
}
/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}