<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Jira : RPT-216, Dev:Anoop, Date:10-April-2016
 */

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
$exchange='reports_product_summary_exchange';
$queue='reports_product_summary_queue';
$consumer_tag = 'reports_product_summary_tag';
$key='reports_product_summary_key';
$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_msg($msg) {
    $order = json_decode($msg->body,true);
    $url=CONSUMER_HOST."/tools/product_summary_consumer_tools.php";
    //curlpost($url,$order);
    $urls[] = $url;
    $urls[] = CONSUMER_HOST."/tools/productDailyProjectionRevReport.php";
//    $urls[] = CONSUMER_HOST."/tools/productDailyProjectionReport.php";
    curlMultiPost($urls,$order);
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);        
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_msg');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

function curlpost($url,$data) {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,true);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch); 
    curl_close($ch);
    return $response;
}
/**
 * This method is used for multicurl request
 * @param type $urls
 * @param type $data
 * @return type
 */
function curlMultiPost($urls,$data){
    $mh = curl_multi_init();
    $curls=array();
    foreach($urls as $i=>$url){
        $ch = $curls[$i] = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_multi_add_handle($mh, $ch);
        unset($ch);
    }
    $running = NULL; 
    do { 
        curl_multi_exec($mh,$running); 
    } while($running > 0); 
    $res = array(); 
    foreach($curls as $curl) 
    { 
        $res[] = curl_multi_getcontent($curl);
        curl_multi_remove_handle($mh, $curl); 
    }
    curl_multi_close($mh); 
    return $res;    
}