<?php

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;

/**** FOR calling this file pass parameters in below format ****/
//php /home/shopclue/live/amqplib/clues/common_consumer.php QUEUE EXCHANGE KEY CONSUMER_TAG CRON_FILE Failure_Queue_Exchange Failure_Queue_Key

$queue_info['queue']=$argv[1];                    //  Queue;
$queue_info['exchange']=$argv[2];                 //  Exchange;
$queue_info['key']=$argv[3];                      //  Key;
$queue_info['consumer_tag'] = $argv[4];           //  Consumer Tag;
$queue_info['file']=$argv[5];                     //  Cron File
$queue_info['failure_exchange']=$argv[6];         //  Failure Queue Exchange
$queue_info['failure_key']=$argv[7];              //  Failure Queue Key

if(empty($queue_info['queue']) || empty($queue_info['exchange']) || empty($queue_info['key']) || empty($queue_info['consumer_tag']) 
    || empty($queue_info['file']) || empty($queue_info['failure_exchange']) || empty($queue_info['failure_key'])){
    
    Logging::log_data(array('DATA'=>$queue_info),'MESSAGE','Mandatory Fields Required','/tmp/'.$queue_info['queue'].'.log');

}
//print_r($queue_info);//die;

define($queue_info['consumer_tag'],json_encode($queue_info));
//echo "<pre>";print_r(json_decode(consumer_solr_update_fields));die;


$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue_info['queue']
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue_info['queue'], false, true, false, false);

/*
    name: $queue_info['exchange']
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/


/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */

function process_message($msg)
{       
       $queue = $msg->delivery_info;
       $const_var= $queue['consumer_tag'];
       
       $details=json_decode(constant($const_var),true);// For Getting queue_info array in function
       //echo "<pre>";print_r($details);die;

       $_data = json_decode($msg->body,true);
       //echo "<pre>";print_r($queue_info);die;
       $data = implode("&",$_data['data']); 

       //echo CONSUMER_HOST."/tools/".$details['file'].".php?".$data;die;
       $result = file_get_contents(CONSUMER_HOST."/tools/".$details['file'].".php?".$data);
       //echo "<br>".$result;//die;
        
        if($result == 'true'){
            
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            Logging::log_data(array('DATA'=>$result,'QUEUE'=>$details['queue']),'MESSAGE','SUCCESS','/tmp/'.$details['queue'].'.log');

        }else{
            
            Logging::log_data(array('DATA'=>$result,'EXCHANGE'=>$details['failure_exchange']),'MESSAGE','FAILURE QUEUE','/tmp/'.$details['queue'].'.log');
            
            //Connection for Failure Queue
            $AMQPConnection = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);

            $channel = $AMQPConnection->channel();
            
            //Publishing to Failure Queue
            $channel->basic_publish($msg,$details['failure_exchange'],$details['failure_key']);

            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            $channel->close();
            $AMQPConnection->close();
        } 
    
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue_info['queue'], $queue_info['consumer_tag'], false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}
