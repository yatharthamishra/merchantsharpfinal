<?php

/**
 * This consumer takes the data from make mass coupons queue 
 * and makes mass coupons
 *
 *
 */
include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

// Consumer tag for this consumer that can be seen from the rabbitmq interface
// to identify which consumers are currently running and from where
$consumer_tag = 'MAKE_MASS_COUPONS_CONSUMER_ADMIN';

// The queue name for the update third price consumer
$queue = 'mass_coupons_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();



/*
  The following code is the same both in the consumer and the producer.
  In this way we are sure we always have a queue to consume from and an
  exchange where to publish messages.
 */

function process_message($msg) {
    $consumerData = base64_encode($msg->body);

    $url = CONSUMER_HOST . "/tools/consumer_mass_coupons.php";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('data' => $consumerData)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
//    print_r($response);die("Casd");

    $msg->delivery_info['channel']->
            basic_ack($msg->delivery_info['delivery_tag']);

    // Send a message with the string "quit" to cancel the consumer.
    if ($msg->body === 'quit') {
        $msg->delivery_info['channel']->
                basic_cancel($msg->delivery_info['consumer_tag']);
    }
}

/*
  queue: Queue from where to get the messages
  consumer_tag: Consumer identifier
  no_local: Don't receive messages published by this consumer.
  no_ack: Tells the server if the consumer will acknowledge the messages.
  exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
  nowait:
  callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}