<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'similar_search_exchange';
$queue = 'similar_search_queue';
$consumer_tag = 'similiar_search_tag';


$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg) {
   $products = json_decode($msg->body,true);
                 $query=make_query($products);
   if($query!=''){
     $result = file_get_contents(CONSUMER_HOST . "/tools/similar_search.php?status=".$products[0]['product_status'].$query);
   }


    if($result){
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
   //Logging::log_data(array('DATA'=>$result,'QUEUE'=>'order_reporting_master_status_queue'),'MESSAGE','SUCCESS','../../images/logs/order_reports_master_daily.log');    
}



$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

function make_query($products)
{
    $query='';
    foreach ($products as $value)
    {
        $query.='&product_ids[]='.$value['product_id'];
    }
    return $query;
}
?>

