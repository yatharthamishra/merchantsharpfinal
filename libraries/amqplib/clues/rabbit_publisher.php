<?php

include __DIR__ . '/config.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
use PhpAmqpLib\Message\AMQPMessage;

function send_data_to_queue($data_arr, $exchange, $key_to_exchange, $enable_logging=true)
{

    try
    { 
        //$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
        $conn = new AMQPConnection('localhost', PORT, 'guest', 'guest', VHOST);
        $ch   = $conn->channel();
    } catch (Exception $e) {
        if ($enable_logging) {
            Logging::log_data(array('EXCEPTION' => $e->getMessage(), 'DATA' => $data_arr, 'EXCHANGE' => $exchange, 'KEY' => $key_to_exchange), 'EXCEPTION', 'FAILURE', '/tmp/rabbit_publisher.log');
        }
        return false;
    }

    if ($enable_logging) {
        $ch->set_nack_handler(
            function (AMQPMessage $message) {
                Logging::log_data($message->body, 'MESSAGE_NACK', 'FAILURE', '/tmp/rabbit_publisher.log');
            }
        );
    }

    if ($enable_logging) {
        $ch->set_ack_handler(
            function (AMQPMessage $message) {
                Logging::log_data($message->body, 'MESSAGE_ACK', 'SUCCESS', '/tmp/rabbit_publisher.log');
            }
        );
    }

    $ch->confirm_select();

    try
    {
        $ch->exchange_declare($exchange, 'direct', true, true, false);
    } catch (Exception $e) {
        if ($enable_logging) {
            Logging::log_data(array('EXCEPTION' => $e, 'DATA' => $data_arr, 'EXCHANGE' => $exchange, 'KEY' => $key_to_exchange), 'EXCEPTION', 'FAILURE', '/tmp/rabbit_publisher.log');
        }
        return false;
    }
    $msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));

    foreach ($data_arr as $data) {
        $msg_body = json_encode($data);

        $msg->setBody($msg_body);

        $ch->basic_publish($msg, $exchange, $key_to_exchange);

        if ($enable_logging) {
            Logging::log_data(array('DATA' => $data, 'EXCHANGE' => $exchange, 'KEY' => $key_to_exchange), 'MESSAGE', 'PUBLISHING', '/tmp/rabbit_publisher.log');
        }

    }

    if ($enable_logging) {
        $ch->wait_for_pending_acks();
    }

    $ch->close();
    $conn->close();
    return true;
}
