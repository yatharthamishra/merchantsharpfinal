<?php

include (__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$queue = 'merchant_communication_queue';
$consumer_tag = 'consumer_merchant_communication';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg) {

    $data = json_decode($msg->body, true);

    if (isset($data['sms_id'])) {

    	$sms_id = $data['sms_id'];

    	$company_id = $data['company_id'];

    	$result = file_get_contents(CONSUMER_HOST . "/tools/merchant_sms_communication.php?sms_id=$sms_id&company_id=$company_id");

    } else {
    
	    $url_email_data = urlencode(serialize($data));
	        
	    $result = file_get_contents(CONSUMER_HOST . "/tools/merchant_communication.php?data=$url_email_data");
	}

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>