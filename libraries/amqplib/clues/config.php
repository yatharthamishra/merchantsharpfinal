<?php

require_once __DIR__.'/../vendor/autoload.php';

define('HOST', '10.10.254.219');
define('PORT', 5672);
//define('USER', 'guest');
//define('PASS', 'guest');
define('USER', 'shopclueadmin');
define('PASS', 'rabbitmqadmin');
define('VHOST', '/');
define('CONSUMER_HOST', 'http://localhost/');

//If this is enabled you can see AMQP output on the CLI
define('AMQP_DEBUG', false);

define('TICKET_CREATE_EXCHANGE','mst_ticket_exchange');
define('TICKET_CREATE_QUEUE','mst_ticket_create_queue');
define('TICKET_CREATE_KEY','ticket_create');
define('TICKET_MAIL_EXCHANGE','mst_ticket_exchange');
define('TICKET_MAIL_QUEUE','mst_mailing_queue');
define('TICKET_MAIL_KEY','ticket_mail');
define("MAX_CONSUMERS_TICKET_CREATE",5);
define("MAX_CONSUMERS_TICKET_MAIL",5);
