<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$queue = 'ldap_user_management_queue';
$consumer_tag = 'consumer_ldap_user_management';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){

	$data = json_decode($msg->body,true);

    $email = '';

    if (isset($data['email'])) {
	   $email = $data['email'];
    }

    $emp_id = '';

    if (isset($data['emp_id'])) {
       $emp_id = $data['emp_id'];
    }

    $status = '';

    if (isset($data['status'])) {
       $status = $data['status'];
    }

	$result = file_get_contents(CONSUMER_HOST . "/tools/ldap_user_management.php?email=$email&emp_id=$emp_id&status=$status");

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>