<?php
include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = TICKET_CREATE_EXCHANGE;
$key = TICKET_CREATE_KEY;
$queue = TICKET_CREATE_QUEUE;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
list(,,$currentConsumerCount) =$ch->queue_declare($queue, false, true, false, false);
$maxNumberOfConsumersEmail=MAX_CONSUMERS_TICKET_CREATE;
if ($currentConsumerCount > $maxNumberOfConsumersEmail) {
    exit();
}
/*
  name: $queue
  passive: false
  durable: true // the queue will survive server restarts
  exclusive: false // the queue can be accessed in other channels
  auto_delete: false //the queue won't be deleted once the channel is closed.
 */
global $cfg;

function process_message($msg) {
  
    $descriptorspec = array(
        0 => array("pipe", "r"), // stdin is a pipe that the child will read from
        1 => array("pipe", "w"), // stdout is a pipe that the child will write to
        2 => array("file", "/tmp/error-output.txt", "a") // stderr is a file to write to
    );

    $cwd = '/tmp';
    $env = array('some_option' => 'aeiou');
    $process = proc_open('/usr/bin/php71 '.__DIR__.'/merchant_ticket_creation_process.php', $descriptorspec, $pipes, $cwd, $env);
	if (is_resource($process)) {
        // $pipes now looks like this:
        // 0 => writeable handle connected to child stdin
        // 1 => readable handle connected to child stdout
        // Any error output will be appended to /tmp/error-output.txt

        fwrite($pipes[0], serialize($msg));
        fclose($pipes[0]);

     //echo stream_get_contents($pipes[1]);die;
        fclose($pipes[1]);

        // It is important that you close any pipes before calling
        // proc_close in order to avoid a deadlock
        $return_value = proc_close($process);
        if (!$return_value) {
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }
    }
}
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');
while (count($ch->callbacks)) {
    $ch->wait();
}
?>  
