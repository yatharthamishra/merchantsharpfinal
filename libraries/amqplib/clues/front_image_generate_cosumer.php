<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

// Consumer tag for this consumer that can be seen from the rabbitmq interface
// to identify which consumers are currently running and from where
//$exchange = 'front_image_generate_exchange_catalog';
//$queue = 'front_image_generate_queue_catalog';
//$consumer_tag = 'front_image_generate_consumer_catalog';

$exchange = 'front_image_generate_exchange_catalog';
$queue = 'front_image_generate_queue_catalog';
$consumer_tag = 'front_image_generate_consumer_catalog';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();


/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
 */

function process_message($msg)
{ 
    $consumerData=  base64_encode($msg->body);
    $url = CONSUMER_HOST . "/tools/consumer_front_image_generate.php";    
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('data'=>$consumerData)) );         
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responce  = curl_exec($ch);
    $msg->delivery_info['channel']->
        basic_ack($msg->delivery_info['delivery_tag']);
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}
