<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Jira : RPT-149, Dev:Anoop, Date:23-Feb-2015
 */

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
$exchange='order_reporting_master_exchange';
$queue='order_reporting_master_status_queue';
$consumer_tag = 'order_reporting_consumer_tag';
$key='order_reporting_master_status';
$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);
/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */


function update_order_reports_master($msg) {
    $order = json_decode($msg->body,true);
    $callCurl = true;
    $result = 'false';
    if (isset($order['data']) && isset($order['data']['mode']) && $order['data']['mode'] == 'address') {
        $order = $order['data'];
    }
    if($order['mode']=='get_dss_cost' || $order['mode']== 'address' || $order['mode']== 'revenue_data') {
        $data= $order;
    } elseif($order['mode'] == 'product_summary'){
        $callCurl = false;
        require_once dirname(__FILE__) . '/../../tools/productSummaryConsumer.php';
        if(populateOrderReportingMasterFieldForProductSummary($order['product_id'],$order['order_date']) && populateOrderRevenueDataMasterFieldForProductSummary($order['product_id'],$order['order_date'])) {
            $result = 'true';
//	logActivityDateStructure($order,"product_summary","product_summary.log");
        }
    } elseif($order['data']['mode'] == 'status'){
        $callCurl = false;
        require_once dirname(__FILE__) . '/../../tools/productSummaryConsumer.php';
        if(updateStatusOrderReportingMaster($order['data']['order_ids'],$order['data']['to_status'],$order['data']['from_status'])) {
            $result = 'true';
        }
    }
//    else {
//        $data['order_ids']      = $order['data']['order_ids'];
//        $data['mode']           = $order['data']['mode'];
//        $data['billing']        = $order['data']['billing'];
//        $data['billing_type']   = $order['data']['billing_type'];
//    }
    if($callCurl){
        $url=CONSUMER_HOST."/tools/order_reporting_master_status_consumer.php";
        $result = curlpost($url,$data);
    }
    if(trim($result) == 'true'){
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        Logging::log_data(array('DATA'=>$result,'QUEUE'=>'order_reporting_master_status_queue'),'MESSAGE','SUCCESS','../../images/logs/order_reports_master_daily.log');
    } 
//    else {
//        if($order['mode']=='get_dss_cost') {
//            $mode = $order['mode'];  
//            $data = $order;
//        } elseif($order['mode'] == 'product_summary') {
//            $mode = $order['mode'];
//            $data = $order;
//        } elseif($order['data']['mode'] == 'status') {
//            $mode = $order['data']['mode'];
//            $data = $order['data'];
//        }
//        $logData = array("field1"=>$mode,"field2"=>"","field3"=>"","field4"=>"","error_name"=>"Message could not be processed","data"=>$data);
//        Analog::log('reporting','order_reporting_master_status_consumer',json_encode($logData), Registry::get('config.LOG_LEVELS.ERROR'));
//    }
}
/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'update_order_reports_master');
/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);
// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}


function curlpost($url,$data) {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, urldecode(http_build_query($data)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch); 
    curl_close($ch);
    return $response;
}
