<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;


//$exchange = 'mongo_update_fields_exchange';
$queue = 'mongo_update_fields';
$key = 'update_fields_mongo';
$exchange = 'solr_update_fields_exchange';
$consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg)
{
	$product_data = json_decode($msg->body,true);
    $product_id = $product_data['data']['product_id'];
    $last_update = $product_data['data']['up_time'];
     $module = $product_data['module'];
        if($product_data['retry'] < 3 && !empty($product_id))
        {
            $result = file_get_contents(CONSUMER_HOST."/tools/mongo_fields_affiliate.php?&$product_id&$last_update");
            if($result == 'TRUE'){
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                Logging::log_data(array('DATA'=>$result,'QUEUE'=>'mongo_delay_queue'),'MESSAGE','SUCCESS','/tmp/mongo_rmq.log');
                
        }
            else 
            {
             Logging::log_data(array('DATA'=>$result,'QUEUE'=>'mongo_delay_queue'),'MESSAGE','PUBLISHING','/tmp/mongo_rmq.log');
            //Connection for Delay Queue
            $AMQPConnection = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
            $channel = $AMQPConnection->channel();
            //Publishing to Delay Queue
            $channel->basic_publish($msg,'mongo_delay_exchange','mongo_delay_key');
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            $channel->close();
            $AMQPConnection->close();
            }
        }
        else
        {
            error_log("No product_id for return count or product_data[retry] >3 in order_return_count_consumer.php", 1, "vinay.gupta@shopclues.com");
            
        }
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

