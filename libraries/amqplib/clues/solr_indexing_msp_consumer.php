<?php
define('AREA','A');
include(__DIR__ . '/config.php');
include(__DIR__.'/../../config.local.php');
use PhpAmqpLib\Connection\AMQPConnection;

$exchange = $config['importer_msp_queue']['exchange'];
$queue = $config['importer_msp_queue']['queue'];
$consumer_tag = 'solr_msp_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){
	//$order_data = json_decode($msg->body,true);

	//$order_id = $order_data['order_id'];
	//$status_to = $order_data['status_to'];
	
	$result = file_get_contents(CONSUMER_HOST."/importer/?indexing=msp&module=consume&data=".urlencode($msg->body));
	if ($result) {
		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
	}
	
    
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>
