<?php

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPConnection;

$exchange = 'carrier_invoice_upload_exchange';
$queue = 'carrier_invoice_upload_queue';
$consumer_tag = 'consumer_notification_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

/*
  The following code is the same both in the consumer and the producer.
  In this way we are sure we always have a queue to consume from and an
  exchange where to publish messages.
 */

/*
  name: $queue
  passive: false
  durable: true // the queue will survive server restarts
  exclusive: false // the queue can be accessed in other channels
  auto_delete: false //the queue won't be deleted once the channel is closed.
 */
$ch->queue_declare($queue, false, true, false, false);

/*
  name: $exchange
  type: direct
  passive: false
  durable: true // the exchange will survive server restarts
  auto_delete: false //the exchange won't be deleted once the channel is closed.
 */

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */
function process_message($msg) {
    $upload_data = json_decode($msg->body, true);
    if ($upload_data['mode'] == "carrier_invoice_upload") {

        $file_row = serialize($upload_data['file_row']);
        $form_post_data = serialize($upload_data['form_post_data']);
        $parent_last_queue = $upload_data['parent_last_queue'];
        $child_last_queue = $upload_data['child_last_queue'];
        $url = CONSUMER_HOST . "/tools/carrier_invoice_upload.php";

        // POST REQUEST
        $fields = array(
            'file_row' => $file_row,
            'form_post_data' => $form_post_data,
            'parent_last_queue' => $parent_last_queue,
            'child_last_queue' => $child_last_queue
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $json_response = curl_exec($ch);

        curl_close($ch);

        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}

/*
  queue: Queue from where to get the messages
  consumer_tag: Consumer identifier
  no_local: Don't receive messages published by this consumer.
  no_ack: Tells the server if the consumer will acknowledge the messages.
  exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
  nowait:
  callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}

