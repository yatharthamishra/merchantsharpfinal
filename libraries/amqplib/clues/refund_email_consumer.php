<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$queue = 'refund_email_queue';
$consumer_tag = 'consumer_refund_email';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

function process_message($msg){

	$data = json_decode($msg->body,true);

	$order_id = $data['order_id'];
    $refund_id = $data['refund_id'];
    $user_id = $data['user_id'];

    $status_to = '';

    if (isset($data['status_to'])) {
	   $status_to = $data['status_to'];
    }

    $template_id = '';

    if (isset($data['template_id'])) {
    	$template_id = $data['template_id'];
    }

    $col = '';

    if (isset($data['col'])) {
        $col = urlencode(htmlspecialchars(serialize($data['col'])));
    }

	$result = file_get_contents(CONSUMER_HOST . "/tools/refund_email.php?order_id=$order_id&refund_id=$refund_id&user_id=$user_id&status_to=$status_to&template_id=$template_id&col=$col");

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn){
    $ch->close();
    $conn->close();
}

register_shutdown_function('shutdown', $ch, $conn);

while (count($ch->callbacks)) {
    $ch->wait();
}

?>