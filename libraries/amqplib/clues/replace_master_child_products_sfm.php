<?php
/**
 * This consumer takes the data from edit_master_product_orders_queue 
 * and replaces the master products in order with their corresponding
 * child products
 *
 */
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

// The exchange for the update third price consumer
$exchange = 'edit_master_product_orders_exchange';


// Consumer tag for this consumer that can be seen from the rabbitmq interface
// to identify which consumers are currently running and from where
$consumer_tag = 'REPLACE_MASTER_CHILD_PRODUCT_CONSUMER_ADMIN';

// The queue name for the update third price consumer
$queue = 'edit_master_product_orders_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();


/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
 */

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
 */
$ch->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
 */

$ch->exchange_declare($exchange, 'fanout', false, true, false);

//$ch->queue_bind($queue, $exchange);
$ch->queue_bind($queue, $exchange, 'KEY');


function process_message($msg)
{   
	$order_id = json_decode($msg->body, true);
	$order_id = $order_id["order_id"];

	file_get_contents(CONSUMER_HOST."/tools/edit_master_product_orders_consumer.php?order_id=".$order_id);

    $msg->delivery_info['channel']->
        basic_ack($msg->delivery_info['delivery_tag']);

    // Send a message with the string "quit" to cancel the consumer.
    if ($msg->body === 'quit') {
        $msg->delivery_info['channel']->
            basic_cancel($msg->delivery_info['consumer_tag']);
    }
}


/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}
