<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Jira : RPT-166, Dev:Dheeraj, Date:17-July-2015
 */

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
$exchange='rabbitmq_status_summary_daily_exchange';
$queue='rabbitmq_status_summary_daily_queue';
$consumer_tag = 'status_summary_daily_tag';
$key='rabbitmq_status_summary_daily_key';
$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();
/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$ch->queue_declare($queue, false, true, false, false);
/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

//$ch->exchange_declare($exchange, 'direct', false, true, false);
//$ch->queue_bind($queue, $exchange);

/**
 * @param \PhpAmqpLib\Message\AMQPMessage $msg
 */


function update_daily_reports_master($msg) {
    $order = json_decode($msg->body,true);
    //$url=CONSUMER_HOST."/tools/status_summary_daily_report_consumer.php";
    $result = file_get_contents(CONSUMER_HOST."/tools/status_summary_daily_report_consumer.php?order_date=".$order['order_date']."&order_status=".$order['order_status']);
    //$result = curlpost($url,$order);
    if($result == 'true'){        
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        Logging::log_data(array('DATA'=>$result,'QUEUE'=>'rabbitmq_status_summary_daily_queue'),'MESSAGE','SUCCESS','../../images/logs/order_reports_master_daily.log');
    } else { 
        Logging::log_data(array('DATA'=>$result,'QUEUE'=>'rabbitmq_status_summary_daily_queue'),'MESSAGE','PUBLISHING','../../images/logs/order_reports_master_daily.log');    
        //Connection for Delay Queue
        $AMQPConnection = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
        $channel = $AMQPConnection->channel();    
        //Publishing to Delay Queue
        $channel->basic_publish($msg,'rabbitmq_status_summary_daily_exchange','status_summary_daily_status');
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        $channel->close();
        $AMQPConnection->close();
    }
}
/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'update_daily_reports_master');
/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $ch
 * @param \PhpAmqpLib\Connection\AbstractConnection $conn
 */
function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);
// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}


function curlpost($url,$data) {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,true);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch); 
    curl_close($ch);
    return $response;
}