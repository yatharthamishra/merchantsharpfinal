<?php
/**
 * This consumer takes the data from update third price queue 
 * and calculates and updates the third price of the products
 *
 *
 */
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;


// Consumer tag for this consumer that can be seen from the rabbitmq interface
// to identify which consumers are currently running and from where
$consumer_tag = 'omniture_server_consumer_desktop';

// The queue name for the update third price consumer
$queue = 'omniture_queue_desktop';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();


/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
 */


function process_message($msg)
{ 
    $consumerData=  base64_encode($msg->body);
    $url = CONSUMER_HOST . "/tools/hit_omniture_server_desktop.php";    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('data'=>$consumerData)) );         
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_exec($ch);
    $response=curl_getinfo($ch);
    curl_close($ch);
    
    if($response['http_code']==200)
    {
        $msg->delivery_info['channel']->
        basic_ack($msg->delivery_info['delivery_tag']);
    }
    
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
 */

$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}
