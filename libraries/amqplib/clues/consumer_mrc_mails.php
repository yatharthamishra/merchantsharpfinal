<?php
include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Logging\Logging;
/**
  =============================================
  Load cscart framework
  =============================================
**/

define('AREA', 'A');
define('AREA_NAME', 'admin');
require  dirname(__FILE__) . '/../../prepare.php';
require  dirname(__FILE__) . '/../../init.php';

fn_load_addon("hp_changes");
fn_load_addon("my_changes");

// Specify queue name
$queue = 'status_mail_communication';
$consumer_tag = 'consumer_mail_communication';

try
{
  $conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
  $ch = $conn->channel();
}
catch (Exception $e)
{
  Logging::log_data($e->getMessage(),'EXCEPTION','FAILURE','/tmp/consumer_mrc.log');
  return false;
}  

/**
 * Checks for queue 
 * passive flag is set, so we don't actually create queue
 * 
 **/
try
{
  $ch->queue_declare($queue, true, true, false, false);
}
catch(Exception $e)
{
  Logging::log_data($e,'QUEUE_ERROR','EXCEPTION','/tmp/consumer_mrc.log');
  return false;
}

/**
  ===============================================
  message body is json encoded array of order_id,
  
  from_status and to status 
  ===============================================
**/

function process_message($msg)
{
  $order_data = json_decode($msg->body,true);

    /**
     * Log message data
     **/
    Logging::log_data($order_data,'ORDER_DATA','SUCCESS','/tmp/consumer_mrc.log');
    $order_id = $order_data['order_id'];
    $order_info = fn_get_order_info($order_id,true);
    $order_info['to_status']= $order_data ['to_status'];
    $force_notification = array("C"=>true,"A"=>false,"S"=>false);
   // $force_notification = array();

		$edp_data = fn_generate_ekeys_for_edp(array(), $order_info);
    /**
     * This fn_order_notification can throw exception 
     * Catch and log 
     *
     **/
    try
    {
      	$sent_mail = fn_order_notification($order_info, $edp_data, $force_notification);
    }
    catch(Exception $e)
    {
      Logging::log_data($e, 'ORDER_NOTIFICATION','EXCEPTION', '/tmp/consumer_mrc.log');
    }
    /**
     * Send Ack
     */ 
    $msg->delivery_info['channel']->
        basic_ack($msg->delivery_info['delivery_tag']);
}


$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks) or 1) {
    $ch->wait();
}

