<?php 

namespace PhpAmqpLib\Logging;

/**
 * 
 *Class Logging 
 *  
 **/
Class Logging 
{
  /**
   * public static function log_data
   * 
   * used for logging data
   *
   * @data data to be logged, will be logged after json_encode
   *
   * @message message with data logged, default ORDER_DATA
   *
   * @message_type type of message, default SUCCESS
   * 
   * @path path to log file, default /tmp/rabbit_data.log
   *
   **/

    public static function log_data($data, $message = 'ORDER_DATA',$message_type='SUCCESS',$path = '/tmp/rabbit_data.log')
    {
        $date_array = explode(" ",microtime());
        $timestamp = date('Y-m-d h:i:s',strtotime('now')).' '.($date_array[0]*1000).'ms';
        $extra = array("timestamp" => $timestamp, "message_type" => $message_type, "message" => $message);

        if (!array_key_exists("DATA", $data)){

          $data = array("DATA" => $data);

        }

        $log_data = array_merge($data, $extra);
        $log_json = json_encode($log_data);
        $log = $log_json . "\n\n";
        error_log($log, 3, $path);
    }

}

