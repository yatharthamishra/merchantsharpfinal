<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataCorrectionOTS
 *
 * @author shopclues
 */
require('../main.inc.php');
require ('model/DataCorrectionModel.php');
define("TASK_DATA_CORRECTION_DATA_CHUNK", 1000);
define("TEST_SCRIPT", 1);

class DataCorrectionOTS {

    private $model;
    private $scriptToRun;

    //put your code here
    public function __construct() {
        $this->model = new DataCorrectionModel();
        $this->scriptToRun = $_GET['correctionScript'];
    }

    public function main() {
        switch ($this->scriptToRun) {
            case 'topic':
                $this->correctIssueTopicData();
                break;
            case 'orderstatus':
                $this->correctBlankOrderStatusData();
                break;
            case 'topicandorderstatus':
                $this->correctIssueAndBlankOrderStatusData();
                break;
            default :
                return;
        }
    }

    public function correctIssueAndBlankOrderStatusData() {
        $this->correctIssueTopicData();
        $this->correctBlankOrderStatusData();
    }

    public function correctIssueTopicData() {
        $filePointer = fopen("/tmp/ticketTopicCorrection", 'w');
        set_time_limit(1000);
        $ostTicketIds = $this->model->getCorruptedTopicData();
        $totalNumberOfRecords = count($ostTicketIds);
        $issueSubIssueData = $this->model->getIssueIdSubIssueIdSubSubIssueIdFromClues($ostTicketIds);
        $recordNotFoundInCluesTable = 0;
        $ostAdminTopicMapping = $this->model->getIssueToTopicMapping();
        foreach ($ostTicketIds as $ticketId) {
            if (!isset($issueSubIssueData[$ticketId])) {
                echo "Ticket Not Found in Clues Table - $ticketId \n";
                fwrite($filePointer, "Ticket Not Found in Clues Table - $ticketId \n");
                $recordNotFoundInCluesTable++;
            }
        }
        $allZeroRecordsInAdminTable = 0;
        $recordsNotFoundInMappingTable = 0;
        $recordsUpdated = 0;
        $recordsNotUpdated = 0;
        $ticketsUpdated = [];
        foreach ($issueSubIssueData as $ticketId => $issueRelatedData) {
            $issueId = $issueRelatedData['issueId'];
            $subIssueId = $issueRelatedData['subIssueId'];
            $subsubIssueId = $issueRelatedData['subsubIssueId'];
            $valueToSearch = 0;
            if ($subsubIssueId > 0) {
                $valueToSearch = $subsubIssueId;
            } elseif ($subIssueId > 0) {
                $valueToSearch = $subIssueId;
            } elseif ($issueId > 0) {
                $valueToSearch = $issueId;
            }
            if ($valueToSearch == 0) {
                $outputString = "$ticketId -- all zero entry on admin side \n";
                $allZeroRecordsInAdminTable++;
                echo $outputString;
                fwrite($filePointer, $outputString);
                continue;
            }
            $topicId = $ostAdminTopicMapping[$valueToSearch];
            if ($topicId == 0 || is_null($topicId)) {
                $recordsNotFoundInMappingTable++;
                $outputString = "$ticketId -- $issueId -- $subIssueId -- $subsubIssueId -- $valueToSearch -- 0 -- 0  not found in mapping \n";
                echo $outputString;
                fwrite($filePointer, $outputString);
                continue;
            }
            if (TEST_SCRIPT) {
                echo "topic ID => $topicId ticket ID => $ticketId \n";
            } else {
                $updateStatus = $this->model->updateEntriesWhereTopicIdZero($topicId, $ticketId);
            }
            if ($updateStatus == 1) {
                $ticketsUpdated[] = $ticketId;
                $recordsUpdated++;
            } else {
                $recordsNotUpdated++;
            }
            $outputString = "$ticketId -- $issueId -- $subIssueId -- $subsubIssueId -- $valueToSearch -- $topicId -- $updateStatus\n";
            echo $outputString;
            fwrite($filePointer, $outputString);
        }
        fwrite($filePointer, "All Zero Records - $allZeroRecordsInAdminTable \n");
        echo "All Zero Records - $allZeroRecordsInAdminTable \n";
        fwrite($filePointer, "Not Found In Mapping - $recordsNotFoundInMappingTable \n");
        echo "Not Found In Mapping - $recordsNotFoundInMappingTable \n";
        fwrite($filePointer, "Total Number Of Records - $totalNumberOfRecords \n");
        echo "Total Number Of Records - $totalNumberOfRecords \n";
        fwrite($filePointer, "Records Updated - $recordsUpdated \n");
        echo "Records Updated - $recordsUpdated \n";
        fwrite($filePointer, "Records Not Updated - $recordsNotUpdated \n");
        echo "Records Not Updated - $recordsNotUpdated \n";
        fwrite($filePointer, "Records Not Found In Clues Table - $recordNotFoundInCluesTable \n");
        echo "Records Not Found In Clues Table - $recordNotFoundInCluesTable \n";
        echo "Tickets Updated \n";
        $issuePointer = fopen("/tmp/topicUpdatedIds", "w");
        fwrite($issuePointer, "Tickets Updated \n");
        foreach ($ticketsUpdated as $ticketIds) {
            echo "$ticketIds, \n";
            fwrite($issuePointer, "$ticketIds \n");
        }
    }

    public function correctBlankOrderStatusData() {
        $filePointer = fopen("/tmp/orderStatusTaskCorrection", 'w');
        $bucketNumber = 0;
        set_time_limit(3600);
        $ticketNotFoundInCluesTable = 0;
        $recordsUpdated = 0;
        $recordsNotUpdated = 0;
        $totalNumberOfRecords = 0;
        $taskIdsUpdated = [];
        while (1) {
            $dataToBeCorrected = $this->model->getTaskToBeCorrectedForCorruptedTaskStatus($bucketNumber);
            $ticketIds = [];
            foreach ($dataToBeCorrected as $taskId => $ticketId) {
                $ticketIds[] = $ticketId;
            }

            $orderStatusTicketIdMap = $this->model->getOrderStatusForTicketIdsToBeUpdatedOnTasks($ticketIds);
            foreach ($dataToBeCorrected as $taskId => $ticketId) {
                $totalNumberOfRecords++;
                $orderStatus = $orderStatusTicketIdMap[$ticketId];
                if (!isset($orderStatusTicketIdMap[$ticketId])) {
                    $outputString = "$ticketId -- $taskId not found in clues table \n";
                    $ticketNotFoundInCluesTable++;
                    echo $outputString;
                    fwrite($filePointer, $outputString);
                    continue;
                }
                if (TEST_SCRIPT) {
                    echo "taskId :- $taskId ,ticketNumber :- $ticketId , orderStatus :- $orderStatus \n";
                } else {
                    $updateStatus = $this->model->correctOrderStatusEntriesForTickets($taskId, $orderStatus);
                }

                if ($updateStatus == 1) {
                    $recordsUpdated++;
                    $taskIdsUpdated[] = $taskId;
                } else {
                    $recordsNotUpdated++;
                }
                $outputString = "$ticketId -- $taskId -- $orderStatus -- $updateStatus \n";
                echo $outputString;
                fwrite($filePointer, $outputString);
            }
            if (count($dataToBeCorrected) < TASK_DATA_CORRECTION_DATA_CHUNK) {
                break;
            }
            $bucketNumber++;
        }
        fwrite($filePointer, "Total Number Of Records- $totalNumberOfRecords \n");
        echo "Total Number Of Records- $totalNumberOfRecords \n";
        fwrite($filePointer, "Tickets Not Found Clues Table - $ticketNotFoundInCluesTable \n");
        echo "Tickets Not Found Clues Table - $ticketNotFoundInCluesTable \n";
        fwrite($filePointer, "Tasks Updated - $recordsUpdated \n");
        echo "Tasks Updated - $recordsUpdated \n";
        fwrite($filePointer, "Tasks Not Updated - $recordsNotUpdated \n");
        echo "Tasks Not Updated - $recordsNotUpdated \n";

        $tasksFinalLog = fopen("/tmp/tasksFinalLog", 'w');
        fwrite($tasksFinalLog, "Tasks Updated");
        echo "Tasks Updated";
        foreach ($taskIdsUpdated as $taskId) {
            echo "$taskId, \n";
            fwrite($tasksFinalLog, "$taskId,");
        }
    }

}

$dtOts = new DataCorrectionOTS();
$dtOts->main();
?>
