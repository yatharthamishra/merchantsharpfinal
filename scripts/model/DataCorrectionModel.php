<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataCorrectionModel
 *
 * @author shopclues
 */
class DataCorrectionModel {

    public function getTaskToBeCorrectedForCorruptedTaskStatus($bucketNumber) {
        $offset = $bucketNumber * TASK_DATA_CORRECTION_DATA_CHUNK;
        $sql = 'SELECT 
                ot.id AS taskId,
                ost.`number` AS ticketId 
              FROM
                `mst_task__data` t 
                JOIN mst_task ot 
                  ON t.`task_id` = ot.`id` 
                join mst_ticket ost on 
                ot.object_id=ost.ticket_id 
              WHERE 
                t.`taskSource` = "57,On Ticket Create" limit ' . $offset . ',' . TASK_DATA_CORRECTION_DATA_CHUNK;
        $res = db_query($sql);
        $set = array();
        while ($result = db_fetch_array($res)) {
            $set[$result['taskId']] = $result['ticketId'];
        }
        return $set;
    }

    public function getOrderStatusForTicketIdsToBeUpdatedOnTasks($ostTicketIds) {
        $ostTicketIds = array_map('intval', $ostTicketIds);
        if (count($ostTicketIds) == 0) {
            return array();
        }
        $ids = join("','", $ostTicketIds);
        $sql = "SELECT 
                `status` as orderStatus,
                `mst_ticket_id` as number 
              FROM
                clues_customer_queries 
              WHERE mst_ticket_id IN ('$ids') ";
        $res = db_query($sql);
        $set = array();
        while ($result = db_fetch_array($res)) {
            $set[$result['number']] = $result['orderStatus'];
        }
        return $set;
    }

    public function correctOrderStatusEntriesForTickets($taskId, $orderStatus) {
        global $__db;
        $sql = "Update mst_task__data set createStatus='$orderStatus' where task_id=$taskId limit 1";
        echo $sql;exit;
        db_query($sql);
        return $__db->affected_rows;
    }

    public function getCorruptedTopicData() {
        $sql = "SELECT 
            ot.`number` as ticketId
          FROM
            `mst_ticket` ot 
          WHERE ot.`topic_id` = 0 ";
        $res = db_query($sql);
        $set = array();
        while ($result = db_fetch_array($res)) {
            $set[] = $result['ticketId'];
        }
        return $set;
    }

    public function getIssueIdSubIssueIdSubSubIssueIdFromClues($ostTicketIds) {
        $ostTicketIds = array_map('intval', $ostTicketIds);
        if (count($ostTicketIds) == 0) {
            return array();
        }
        $ids = join("','", $ostTicketIds);
        $sql = "SELECT 
            sub_issue_id as subIssueId,
            sub_sub_issue_id as subsubIssueId,
            issue_id as issueId,
            mst_ticket_id as number
          FROM
            clues_customer_queries 
          WHERE mst_ticket_id IN ('$ids') ";
        $res = db_query($sql);
        $set = array();
        while ($result = db_fetch_array($res)) {
            $set[$result['number']] = $result;
        }
        return $set;
    }

    public function updateEntriesWhereTopicIdZero($topicId, $ticketId) {
        global $__db;
        $sql = "UPDATE `mst_ticket` SET `topic_id`=$topicId WHERE `number`=$ticketId limit 1";
        echo $sql;exit;
        db_query($sql);
        return $__db->affected_rows;
    }

    public function getIssueToTopicMapping() {
        $sql = "select `key`,`value` from clues_osticket_map_manager
            where submodule='helpTopicMapping'";
        $res = db_query($sql);
        $set = array();
        while ($result = db_fetch_array($res)) {
            $set[$result['key']] = $result['value'];
        }
        return $set;
    }

}

?>
