<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TaskMappingModel extends VerySimpleModel {

    static $meta = array(
        'table' => TASK_MAPPING_TABLE,
        'ordering' => array('id'),
        'pk' => array('id')
    );

}

class TaskMapping extends TaskMappingModel {
    
    static function lookup($id) {
        $taskmap = self::objects()->filter(array("task_status_id"=>1,'id'=>$id))->first();
        return $taskmap;
    }
    
}