<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class CluesIssues extends VerySimpleModel{
    static $meta = array(
        'table' => CLUES_ISSUES,
        'ordering' => array('issue_id'),
        'pk' => array('issue_id')
    );
}
class CscartCategory extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_CATEGORY,
        'pk' => array('category_id')
    );
}
class CluesExceptionOrderCause extends VerySimpleModel{

    static $meta = array(
        'table' => CLUES_EXCEPTION_CAUSES_ORDER_REL,
        'pk' => array('order_id')
    );
}
class CscartOrdersDetails extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_ORDER_DETAILS,
        'pk' => array('item_id')
    );
}
class CscartOrders extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_ORDERS,
        'pk' => array('order_id'),
        'joins' => array(
            'company' => array(
                'constraint' => array('company_id' => 'CscartCompany.company_id'),
            )
        ),
    );
}
class CscartUsers extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_USERS,
        'pk' => array('user_id')
    );
}
class CscartCompany extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_COMPANY,
        'pk' => array('company_id')
    );
}
class CscartProducts extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_PRODUCTS,
        'pk' => array('product_id')
    );
}
Class BulkBatchUpload extends VerySimpleModel{
    static $meta = array(
        'table' => CSCART_BULK_PRODUCT,
        'pk' => array('batch_id')
    );
}
class CluesCustomerQueries extends VerySimpleModel{

    static $meta = array(
        'table' => CLUES_CUSTOMER_QUERIES,
        'pk' => array('id')
    );
}
class CscartCategoryDescription extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_CATEGORY_DESCRIPTION,
        'pk' => array('category_id'),
        'joins' => array(
            'cat' => array(
                'constraint' => array('category_id' => 'CscartCategory.category_id'),
            )
        ),
    );
}
class RMAPropertyDescription extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_RMA_PROPERTY_DESCRIPTIONS,
        'pk' => array('category_id')
    );
}
class CscartRmaReturns extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_RMA_RETURNS,
        'pk' => array('return_id')
    );
}
class CscartRMAReturnsProducts extends VerySimpleModel{

    static $meta = array(
        'table' => CSCART_RMA_RETURN_PRODUCTS,
        'pk' => array('return_id'),
        'joins' => array(
            'product' => array(
                'constraint' => array('product_id' => 'CscartOrdersDetails.product_id'),
            ),
            'issue' => array(
                'constraint' => array('reason' => 'CscartRMAReturnsMapping.returns_reason_id'),
            )
        ),
    );
}
class CscartRMAReturnsMapping extends VerySimpleModel{

    static $meta = array(
        'table' => CLUES_RMA_RETURNS_MAPPING,
        'pk' => array('returns_reason_id')
    );
}
class CluesRefunds extends VerySimpleModel{

    static $meta = array(
        'table' => CLUES_REFUNDS,
        'pk' => array('id')
    );
}
class CluesRmaHistory extends VerySimpleModel{

    static $meta = array(
        'table' => CLUES_RMA_HISTORY,
        'pk' => array('id')
    );
}