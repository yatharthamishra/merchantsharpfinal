<?php

/* * *******************************************************************
  class.attachment.php

  Attachment Handler - mainly used for lookup...doesn't save!

  Peter Rotich <peter@osticket.com>
  Copyright (c)  2006-2013 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */
require_once(INCLUDE_DIR . 'class.ticket.php');
require_once(INCLUDE_DIR . 'class.file.php');

class Attachment extends VerySimpleModel {

    static $meta = array(
        'table' => ATTACHMENT_TABLE,
        'pk' => array('id'),
        'select_related' => array('file'),
        'joins' => array(
            'draft' => array(
                'constraint' => array(
                    'type' => "'D'",
                    'object_id' => 'Draft.id',
                ),
            ),
            'file' => array(
                'constraint' => array(
                    'file_id' => 'AttachmentFile.id',
                ),
            ),
            'thread_entry' => array(
                'constraint' => array(
                    'type' => "'H'",
                    'object_id' => 'ThreadEntry.id',
                ),
            ),
            'task' => array(
                'constraint' => array(
                    'object_id' => 'Task.id',
                ),
            ),
        ),
    );
    var $object;

    function getId() {
        return $this->id;
    }

    function getFileId() {
        return $this->file_id;
    }

    function getFile() {
        return $this->file;
    }

    function getFilename() {
        return $this->name ? : $this->file->name;
    }

    function getHashtable() {
        return $this->ht;
    }

    function getInfo() {
        return $this->getHashtable();
    }

    function getObject() {

        if (!isset($this->object))
            $this->object = ObjectModel::lookup(
                            $this->ht['object_id'], $this->ht['type']);

        return $this->object;
    }

    static function lookupByFileHash($hash, $objectId = 0) {
        $file = static::objects()
                ->filter(array('file__key' => $hash));

        if ($objectId)
            $file->filter(array('object_id' => $objectId));

        return $file->first();
    }

    static function lookup($var, $objectId = 0) {
        return (is_string($var)) ? static::lookupByFileHash($var, $objectId) : parent::lookup($var);
    }

}

class GenericAttachments extends InstrumentedList {

    var $lang;

    function getId() {
        return $this->key['object_id'];
    }

    function getType() {
        return $this->key['type'];
    }

    /**
     * Drop attachments whose file_id values are not in the included list,
     * additionally, add new files whose IDs are in the list provided.
     */
    function keepOnlyFileIds($ids, $inline = false, $lang = false) {
        if (!$ids)
            $ids = array();
        $new = array_fill_keys($ids, 1);
        foreach ($this as $A) {
            if (!isset($new[$A->file_id]) && $A->lang == $lang && $A->inline == $inline)
            // Not in the $ids list, delete
                $this->remove($A);
            unset($new[$A->file_id]);
        }
        // Everything remaining in $new is truly new
        $this->upload(array_keys($new), $inline, $lang);
    }

    function upload($files, $inline = false, $lang = false) {
        $i = array();
        if (!is_array($files))
            $files = array($files);
        foreach ($files as $file) {
            if (is_numeric($file))
                $fileId = $file;
            elseif (is_array($file) && isset($file['id']))
                $fileId = $file['id'];
            elseif (isset($file['tmp_name']) && ($F = AttachmentFile::upload($file)))
                $fileId = $F->getId();
            elseif ($F = AttachmentFile::create($file))
                $fileId = $F->getId();
            else
                continue;

            $_inline = isset($file['inline']) ? $file['inline'] : $inline;

            $att = $this->add(Attachment::create(array(
                        'file_id' => $fileId,
                        'inline' => $_inline ? 1 : 0,
                        'created'=> date("Y-m-d H:i:s",time()),
            )));

            // Record varying file names in the attachment record
            if (is_array($file) && isset($file['name'])) {
                $filename = $file['name'];
            }
            if ($filename) {
                // This should be a noop since the ORM caches on PK
                $file = $F ? : AttachmentFile::lookup($fileId);
                // XXX: This is not Unicode safe
                if ($file && 0 !== strcasecmp($file->name, $filename))
                    $att->name = $filename;
            }
            if ($lang)
                $att->lang = $lang;

            // File may already be associated with the draft (in the
            // event it was deleted and re-added)
            $att->save();
            $i[] = $fileId;
        }
        return $i;
    }

    function save($file, $inline = true) {
        $ids = $this->upload($file, $inline);
        return $ids[0];
    }

    function getInlines($lang = false) {
        return $this->_getList(false, true, $lang);
    }

    function getSeparates($lang = false) {
        return $this->_getList(true, false, $lang);
    }

    function getAll($lang = false) {
        return $this->_getList(true, true, $lang);
    }

    function count($lang = false) {
        return count($this->getSeparates($lang));
    }

    function _getList($separates = false, $inlines = false, $lang = false) {
        $base = $this;

        if ($separates && !$inlines)
            $base = $base->filter(array('inline' => 0));
        elseif (!$separates && $inlines)
            $base = $base->filter(array('inline' => 1));

        if ($lang)
            $base = $base->filter(array('lang' => $lang));

        return $base;
    }

    function delete($file_id) {
        return $this->objects()->filter(array('file_id' => $file_id))->delete();
    }

    function deleteAll($inline_only = false) {
        if ($inline_only)
            return $this->objects()->filter(array('inline' => 1))->delete();

        return parent::expunge();
    }

    function deleteInlines() {
        return $this->deleteAll(true);
    }

    static function forIdAndType($id, $type) {
        return new static(array(
            'Attachment',
            array('object_id' => $id, 'type' => $type)
        ));
    }

}

class CustomAttachments {

    private function CustomAttachments() {
        
    }

    function emailUpload($file, $inline = FALSE) {

        global $cfg;
        $emailAttachmentConfig = $cfg->config['EmailAttachment']['value'];
        $emailAttachmentConfig = json_decode($emailAttachmentConfig, true);
        $moveUploadFileTries = $cfg->config['move_upload_file_tries']['value'];
        $rsyncNumberOfTries = $cfg->config['rsync_number_of_tries']['value'];
        $enableAttachmentLogging = $cfg->config['enable_email_attachment_logging']['value'];
        $enableAttachmentContentLogging = $cfg->config['enable_email_attachment_content_logging']['value'];
        $AttachmentRsyncConfig = json_decode($cfg->config['AttachmentRsync']['value'], true);
        $parameter = $AttachmentRsyncConfig["parameter"];
        $local = $AttachmentRsyncConfig["local"];
        $remote = $AttachmentRsyncConfig["remote"];
        $cdn_flag = $AttachmentRsyncConfig["cdn"];

        if (empty($file)) {
            return NULL;
        }

        $tform = TicketForm::objects()->one()->getForm();
        $messageField = $tform->getField('message');
        $fileField = $messageField->getWidget()->getAttachments();
        
        $count = 0;
        $ids = array();
        $fileArray=array();
        $pathArray=array();
        $keyArray=array();
        foreach ($file as $f) {

            if ($count > $emailAttachmentConfig["maxNumber"]) {
                break;
            }

            if (!$fileField->isValidFileType($f['name'], $f['type'],$f['tmp_name']))
                throw new FileUploadError(__('File type is not allowed'));

            if (is_callable($f['data']))
                $f['data'] = $f['data']();
            if (!isset($f['size'])) {
                // bootstrap.php include a compat version of mb_strlen
                if (extension_loaded('mbstring'))
                    $f['size'] = mb_strlen($f['data'], '8bit');
                else
                    $f['size'] = strlen($f['data']);
            }

            $config = $fileField->getConfiguration();
            if ($f['size'] > $config['size'])
                throw new FileUploadError(__('File size is too large'));

            // $key = md5(uniqid(rand(), true));
            $key = 'EMail' . time() . rand(1, 99999);
            $FilePath = CustomAttachments::getFilePath($f['name'], $key, $local);
            $status = CustomAttachments::write($f['data'], $FilePath[0]);
            if ($status) {
                exec("chmod -R 777 " . $FilePath[0]);
                array_push($fileArray,$FilePath[0]);
                array_push($pathArray,$FilePath[1]);
                $arr = explode(".", $f['name']);
                $key = $key . "." . end($arr);
                array_push($keyArray,$key);
            $count++;
        }
        }
        
        $cdn = 0;
        if ($cdn_flag) {
            $cdn = CustomAttachments::cdn_write($fileArray, $local, $remote, $rsyncNumberOfTries);
        }
        for($i=0;$i<$count;$i++) {
            $key=$keyArray[$i];
            $Path=$pathArray[$i];
            $id = CustomAttachments::FileEntry($f, $cdn, $key, $Path);
            array_push($ids, $id);
            $i++;
        }
        return $ids;
    }

    function AttachmentEntry($file_id, $ticket_id, $object_id, $inline, $type, $user_id, $staff_id) {
        $sql = 'INSERT IGNORE INTO ' . ATTACHMENT_TABLE . ' SET created=NOW() '
                . ' ,file_id=' . db_input($file_id)
                . ' ,ticket_id=' . db_input($ticket_id)
                . ' ,inline=' . db_input($inline ? 1 : 0)
                . ' ,object_id=' . db_input($object_id)
                . ' ,`type`=' . db_input($type)
                . ' ,user_id=' . db_input($user_id)
                . ' ,staff_id=' . db_input($staff_id);

        db_query($sql);
    }

    function FileEntry($file, $cdn = 0, $key, $path) {
        if (!$file['type'])
            $file['type'] = 'application/octet-stream';

        $sql = 'INSERT INTO ' . FILE_TABLE . ' SET created=NOW() '
                . ',type=' . db_input(strtolower($file['type']))
                . ',name=' . db_input($file['name'])
                . ',ft=' . db_input('T')
                . ',bk=' . db_input('C')
                . ',`key`=' . db_input($key)
                . ',cdn=' . db_input($cdn)
                . ',path=' . db_input($path);

        if (isset($file['size']))
            $sql .= ',size=' . db_input($file['size']);

        if (!(db_query($sql) && ($id = db_insert_id())))
            return false;

        return $id;
    }

    function write($data, $filepath) {

        $fp = @fopen($filepath, 'wb');
        if (!$fp)
            throw new IOException($filename . ':Unable to open for reading');
        if (($status = @fwrite($fp, $data)) === false)
            throw new IOException($filename . ': Unable to write to file');
        return $status;
    }

    function cdn_write($path, $local, $remote, $rsyncNumberOfTries) {
        while ($rsyncNumberOfTries > 0 && $return_var) {
            exec("rsync -" . PARAM . " " . $local . " " . $remote . " 2>&1", $output, $return_var);
            $rsyncNumberOfTries--;
        }
        if ($return_var == 0) {
            foreach($path as $val){
            unlink($val);
            }
            return 1;
        }
        return 0;
    }

    function getFilePath($filename, $key, $local_path) {
        $local_path = $local_path . "image/";
        $local_path = INCLUDE_DIR . '../' . $local_path;

        $currentDate = date("Y-m-d", time());
        $path = explode("-", $currentDate);
        $year = $path[0];
        $month = $path[1];
        $date = $path[2];
        $date_path = $local_path . "/" . $year . "/" . $month . "/" . $date;
        $path = $year . "/" . $month . "/" . $date;
        if (!file_exists($date_path)) {
            mkdir($date_path, 0755, true);
        }
        exec("chmod -R 777 " . $date_path);
        $arr = explode(".", $filename);
        $date_path = $date_path . "/" . $key . "." . end($arr);
        $path = $path . "/" . $key . "." . end($arr);
        return array($date_path, $path);
    }

}

?>
