<?php
include (INCLUDE_DIR.'class.dataReprocessingModel.php');
/**
*   Controller class for data reprocessing script
*/
class DataReprocessingController
{
	private static $modelObject=null;
	private $model;	
	function __construct()
	{
		if(!self::$modelObject){
            self::$modelObject=new DataReprocessingModel();
        }
        $this->model=self::$modelObject;
	}
	function invoke($input = array())
	{
		global $ost,$nav,$thisstaff;

		if($input){
			$check = ModelAuth::verifyPassword($input['password']);
			//$check['code']=1;
			$input['password']="hidden";
			if($check['code']){
				$bool = $this->conditions($input);
				//$bool = $this->model->main($data);
			}
			else{
				$error = $check['message'];
			}
		}
		
		$page='dataReprocessing.inc.php';
		//$nav->setTabActive('manage');
		if(!$bool){
			require(STAFFINC_DIR.'header.inc.php');
			require(STAFFINC_DIR.$page);
			include(STAFFINC_DIR.'footer.inc.php');
		}
		else{
			require(STAFFINC_DIR.$page);
		}
	}

	function conditions($input){
		if(empty($input))
			return 0;
		$paramData = explode(',', $input['paramValue']);
		$paramData = array_map('trim', $paramData);
		//$paramData = implode(',', $paramData);
		$ticketObj = new ModelTicket;
		$ticketIds = $ticketObj->getTicketIds($paramData,$input['searchParam']);
		$data = array('ticketIds' => $ticketIds, 
						'fromDate' => $input['fromDate'],
						'toDate' => $input['toDate'],
						'operation' => $input['operation']
						);
		

		echo "INPUT:<pre>";
		echo $input['operation'];
		echo "<br>".$input['searchParam']."<br>";
		$logObj = new LogModelClass;
		//$logObj->_logEntityWiseData('--INPUT--', $data);
		echo "START<br>";
		$operations = $data['operation'];
		$taskObj = new ModelTask;
		$orderObj = new ModelOrder;
		if(in_array('all', $operations)){
			$ticketObj->openTickets($data);
			$taskObj->setNegativeTicketIdForTasks($data,$output);
			$tasjObj->openLastTasks($data);
			$orderObj->reprocessTickets($data,$output);

		}
		if(in_array('openTickets', $operations)){
			$ticketObj->openTickets($data);
		}
		if(in_array('setNegativeTicketIdForTasks', $operations)){
			$taskObj->setNegativeTicketIdForTasks($data,$output);
		}
		if(in_array('openLastTasks', $operations)){
			$taskObj->openLastTasks($data);
		}
		if(in_array('reprocessTickets', $operations)){
			$orderObj->reprocessTickets($data,$output);
		}
		if(in_array('closeTickets', $operations)){
			$ticketObj->closeTickets($data);
		}
		if(in_array('changeToTriageAgain', $operations)){
			$ticketObj->changeToTriageAgain($data);
		}
		if(in_array('openLastTasksOverall', $operations)){
			$taskObj->openLastTasks($data,$outputData,"overall");
		}

		$msg = "FINISHED";
		$logObj->_logEntityWiseData('--'.$msg.'--', '.............');
		echo $msg;
		$logObj->queriesToRevertBack($data['ticketIds'],$outputData['taskIds']);
		return 1;



	}
}