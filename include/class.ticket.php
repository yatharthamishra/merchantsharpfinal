<?php

/* * *******************************************************************
  class.ticket.php

  The most important class! Don't play with fire please.
  Peter Rotich <peter@osticket.com>
  Copyright (c)  2006-2013 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */
include_once(INCLUDE_DIR . 'class.thread.php');
include_once(INCLUDE_DIR . 'class.task_thread.php');
include_once(INCLUDE_DIR . 'class.staff.php');
include_once(INCLUDE_DIR . 'class.client.php');
include_once(INCLUDE_DIR . 'class.team.php');
include_once(INCLUDE_DIR . 'class.email.php');
include_once(INCLUDE_DIR . 'class.dept.php');
include_once(INCLUDE_DIR . 'class.topic.php');
include_once(INCLUDE_DIR . 'class.lock.php');
include_once(INCLUDE_DIR . 'class.file.php');
include_once(INCLUDE_DIR . 'class.attachment.php');
include_once(INCLUDE_DIR . 'class.banlist.php');
include_once(INCLUDE_DIR . 'class.template.php');
include_once(INCLUDE_DIR . 'class.variable.php');
include_once(INCLUDE_DIR . 'class.priority.php');
include_once(INCLUDE_DIR . 'class.sla.php');
include_once(INCLUDE_DIR . 'class.canned.php');
require_once(INCLUDE_DIR . 'class.dynamic_forms.php');
require_once(INCLUDE_DIR . 'class.user.php');
require_once(INCLUDE_DIR . 'class.collaborator.php');
require_once(INCLUDE_DIR . 'class.task.php');
require_once(INCLUDE_DIR . 'class.faq.php');

class TicketModel extends VerySimpleModel {

    static $meta = array(
        'table' => TICKET_TABLE,
        'pk' => array('ticket_id'),
        'joins' => array(
            'user' => array(
                'constraint' => array('user_id' => 'User.id')
            ),
            'useremail' => array(
                'constraint' => array('user_id' => 'UserEmail.user_id')
            ),
            'status' => array(
                'constraint' => array('status_id' => 'TicketStatus.id'),
		'null' => true,
            ),
            'lock' => array(
                'constraint' => array('lock_id' => 'Lock.lock_id'),
                'null' => true,
            ),
            'dept' => array(
                'constraint' => array('dept_id' => 'Dept.id'),
		'null' => true,
            ),
            'sla' => array(
                'constraint' => array('sla_id' => 'Sla.id'),
                'null' => true,
            ),
            'staff' => array(
                'constraint' => array('staff_id' => 'Staff.staff_id'),
                'null' => true,
            ),
            'tasks' => array(
                'reverse' => 'Task.ticket',
            ),
            'team' => array(
                'constraint' => array('team_id' => 'Team.team_id'),
                'null' => true,
            ),
            'topic' => array(
                'constraint' => array('topic_id' => 'Topic.topic_id'),
                'null' => true,
            ),
            'taskdata'=>array(
                'constraint' => array('ticket_id' => 'TaskData.ticket_id')
            ),
            'thread' => array(
                'reverse' => 'TicketThread.ticket',
                'list' => false,
                'null' => true,
            ),
            'cdata' => array(
                'reverse' => 'TicketCData.ticket',
                'list' => false,
            ),
            'odata' => array(
                'reverse' => 'TicketOData.ticket',
                'list' => false,
            ),
            'triage' => array(
                'reverse' => 'TicketTData.ticket',
                'list' => false,
            ),
            'entries' => array(
                'constraint' => array(
                    "'T'" => 'DynamicFormEntry.object_type',
                    'ticket_id' => 'DynamicFormEntry.object_id',
                ),
            ),
        )
    );

    const PERM_CREATE = 'ticket.create';
    const PERM_EDIT = 'ticket.edit';
    const PERM_ASSIGN = 'ticket.assign';
    const PERM_TRANSFER = 'ticket.transfer';
    const PERM_REPLY = 'ticket.reply';
    const PERM_CLOSE = 'ticket.close';
    const PERM_DELETE = 'ticket.delete';
    const PERM_CUSTOMER_BEHALF = 'ticket.customerbehalf';
    const PERM_TOPIC_CHANGE = 'ticket.topicchange';
    const FULL_DEPT = 'ticket.full';
    const PERM_TICKET_ORDER_MAP = 'ticket.ordermap';

    static protected $perms = array(
        self::PERM_CREATE => array(
            'title' =>
            /* @trans */ 'Create',
            'desc' =>
            /* @trans */ 'Ability to open tickets on behalf of users'),
        self::PERM_EDIT => array(
            'title' =>
            /* @trans */ 'Edit',
            'desc' =>
            /* @trans */ 'Ability to edit tickets'),
        self::PERM_ASSIGN => array(
            'title' =>
            /* @trans */ 'Assign',
            'desc' =>
            /* @trans */ 'Ability to assign tickets to agents or teams'),
        self::PERM_TRANSFER => array(
            'title' =>
            /* @trans */ 'Transfer',
            'desc' =>
            /* @trans */ 'Ability to transfer tickets between departments'),
        self::PERM_REPLY => array(
            'title' =>
            /* @trans */ 'Post Reply',
            'desc' =>
            /* @trans */ 'Ability to post a ticket reply'),
        self::PERM_CLOSE => array(
            'title' =>
            /* @trans */ 'Close',
            'desc' =>
            /* @trans */ 'Ability to close tickets'),
        self::PERM_DELETE => array(
            'title' =>
            /* @trans */ 'Delete',
            'desc' =>
            /* @trans */ 'Ability to delete tickets'),
        self::PERM_CUSTOMER_BEHALF => array(
            'title' =>
            /* @trans */ 'Post on customer Behalf',
            'desc' =>
            /* @trans */ 'Ability to post on customer behalf'),
        self::FULL_DEPT => array(
            'title' =>
            /* @trans */ 'View all Dept tickets',
            'desc' =>
            /* @trans */ 'Ability to view full department tickets'),
        self::PERM_TOPIC_CHANGE => array(
            'title' =>
            /* @trans */ 'Change Issue Type of ticket',
            'desc' =>
            /* @trans */ 'Ability to Change Issue Type of tickets'),
        self::PERM_TICKET_ORDER_MAP => array(
            'title' =>
            /* @trans */ 'Change Order linked with Ticket',
            'desc' =>
            /* @trans */ 'Ability to Link order with tickets'),
    );

    function getId() {
        return $this->ticket_id;
    }

    function getTraigeInfo() {
        global $cfg;

        $triageObj = Triage::lookup($this->getId());
        if ($triageObj->triage) {

            $staticTriageData = getCachedTriageFields();
            foreach ($staticTriageData["Triage Status"] as $key => $value) {
                if ($key == $triageObj->triage) {
                    $triage = $value;
                }
            }
        }
        $triage = $triage ? $triage : "Not Triage";
        $timeDiff = time() - strtotime($this->created);
        if ($triage == "Not Triage" && $timeDiff > $cfg->config['TimeToTriage']['value']) {
            $triage = "Triage Delayed";
        }
        return $triage;
    }

    function getEffectiveDate() {
        return Format::datetime(max(
                                strtotime($this->thread->lastmessage), strtotime($this->closed), strtotime($this->reopened), strtotime($this->created)
        ));
    }

    static function registerCustomData(DynamicForm $form) {
        if (!isset(static::$meta['joins']['cdata+' . $form->id])) {
            $cdata_class = <<<EOF
class DynamicForm{$form->id} extends DynamicForm {
    static function getInstance() {
        static \$instance;
        if (!isset(\$instance))
            \$instance = static::lookup({$form->id});
        return \$instance;
    }
}
class TicketCdataForm{$form->id}
extends VerySimpleModel {
    static \$meta = array(
        'view' => true,
        'pk' => array('ticket_id'),
        'joins' => array(
            'ticket' => array(
                'constraint' => array('ticket_id' => 'TicketModel.ticket_id'),
            ),
        )
    );
    static function getQuery(\$compiler) {
        return '('.DynamicForm{$form->id}::getCrossTabQuery('T', 'ticket_id').')';
    }
}
EOF;
            eval($cdata_class);
            $join = array(
                'constraint' => array('ticket_id' => 'TicketCdataForm' . $form->id . '.ticket_id'),
                'list' => true,
            );
            // This may be necessary if the model has already been inspected
            if (static::$meta instanceof ModelMeta)
                static::$meta->addJoin('cdata+' . $form->id, $join);
            else {
                static::$meta['joins']['cdata+' . $form->id] = array(
                    'constraint' => array('ticket_id' => 'TicketCdataForm' . $form->id . '.ticket_id'),
                    'list' => true,
                );
            }
        }
    }

    static function getPermissions() {
        return self::$perms;
    }

            function attachments($data) {
        global $mandateInfoCreation;
        global $mapIds;
        $mandateInfoCreation=TicketUtilityFunctions::getMandatoryInfoInWhichTicketHasBeenMade($this->getId());
if($mandateInfoCreation == 'batch_upload' || $mandateInfoCreation =='batch_update'){
$mandateInfoCreation='batch';
}
$attachData = $data[$mandateInfoCreation];
foreach ($attachData as $key => $val) {
            if (in_array($key, $mapIds)) {
                continue;
            }
            foreach ($val as $files) {
                $key = 'Ticket' . time() . rand(1, 99999);
                $signature = md5('signature');
                if (empty($files["name"])) {
                    $files["name"] = pathinfo($files["path"], PATHINFO_FILENAME);
                }
                $fileParam = array("ft" => "T", "bk" => "F", "type" => $files["type"], "size" => $files["size"], "key" => $key, "signature" => $signature, "name" => $files["name"], "path" => $files["path"], "cdn" => 1);
                $fileParamObj = AttachmentModel::create($fileParam);
                $fileParamObj->created = SqlFunction::NOW();
                $fileParamObj->save();
                $attachParam = array("object_id" => $this->getId(), "file_id" => $fileParamObj->id, "type" => 'T', "inline" => 0, "staff_id" => 0, "user_id" => $this->getUserId(), "ticket_id" => $this->getId());

                $attachParamObj = Attachment::create($attachParam);
                $attachParamObj->created = SqlFunction::NOW();
$attachParamObj->save();
            }
        }
//for other attachment

$attachData = $data['other'];
foreach ($attachData as $files) {
                $key = 'Ticket' . time() . rand(1, 99999);
                $signature = md5('signature');
                if (empty($files["name"])) {
                    $files["name"] = pathinfo($files["path"], PATHINFO_FILENAME);
                }
                $fileParam = array("ft" => "T", "bk" => "F", "type" => $files["type"], "size" => $files["size"], "key" => $key, "signature" => $signature, "name" => $files["name"], "path" => $files["path"], "cdn" => 1);
                $fileParamObj = AttachmentModel::create($fileParam);
                $fileParamObj->created = SqlFunction::NOW();
                $fileParamObj->save();
                $attachParam = array("object_id" => $this->getId(), "file_id" => $fileParamObj->id, "type" => 'T', "inline" => 0, "staff_id" => 0, "user_id" => $this->getUserId(), "ticket_id" => $this->getId());

                $attachParamObj = Attachment::create($attachParam);
                $attachParamObj->created = SqlFunction::NOW();
$attachParamObj->save();
            }
//end
    }
}

RolePermission::register(/* @trans */ 'Tickets', TicketModel::getPermissions(), true);

class TicketCData extends VerySimpleModel {

    static $meta = array(
        'pk' => array('ticket_id'),
        'joins' => array(
            'ticket' => array(
                'constraint' => array('ticket_id' => 'TicketModel.ticket_id'),
            ),
            ':priority' => array(
                'constraint' => array('priority' => 'Priority.priority_id'),
                'null' => true,
            ),
        ),
    );

}

TicketCData::$meta['table'] = TABLE_PREFIX . 'ticket__data'; // Cdata Made oDATA

class TicketTData extends VerySimpleModel {

    static $meta = array(
        'pk' => array('ticket_id'),
        'joins' => array(
            'ticket' => array(
                'constraint' => array('ticket_id' => 'TicketModel.ticket_id'),
            ),
        ),
    );

}

TicketTData::$meta['table'] = TABLE_PREFIX . 'triage_data'; // Cdata Made oDATA

Class CluesMerchantTicket extends VerySimpleModel {

    static $meta = array(
        'pk' => array('id'),
        'table' => 'clues_merchant_tickets'
    );

}

Class CluesBizGuru extends VerySimpleModel {
    static $meta = array(
        'pk' => array('id'),
        'table' => 'clues_bizguru'
    );
}

class TicketReopenLogs extends VerySimpleModel {

    static $meta = array(
        'pk' => array('id')
    );

}

;
TicketReopenLogs::$meta['table'] = TABLE_PREFIX . 'ticket_reopen_logs';
/*
 * By akash kumar for custom search from other table
 */

class TicketOData extends VerySimpleModel {

    static $meta = array(
        'pk' => array('ticket_id'),
        'joins' => array(
            'ticket' => array(
                'constraint' => array('ticket_id' => 'TicketModel.ticket_id'),
            ),
        ),
    );

    function getEntityIds($entity) {
        return $this->ht[$entity . '_ids'];
    }

    function setEntityIds($entity_ids, $entity) {
        $this->ht[$entity . '_ids'] = $entity_ids;
    }

}

TicketOData::$meta['table'] = TABLE_PREFIX . 'ticket__data';

//Ends here

class Ticket extends TicketModel implements RestrictedAccess, Threadable {

    static $meta = array(
        'select_related' => array('topic', 'staff', 'user', 'team', 'dept', 'sla', 'thread',
            'user__default_email'),
    );
    var $lastMsgId;
    var $last_message;
    var $owner;     // TicketOwner
    var $_user;      // EndUser
    var $_answers;
    var $collaborators;
    var $active_collaborators;
    var $recipients;
    var $lastrespondent;

    function __onload() {
        $this->loadDynamicData();
    }

    function loadDynamicData() {
        if (!isset($this->_answers)) {
            $this->_answers = array();
            foreach (DynamicFormEntryAnswer::objects()
                    ->filter(array(
                        'entry__object_id' => $this->getId(),
                        'entry__object_type' => 'T'
                    )) as $answer
            ) {
                $tag = mb_strtolower($answer->field->name)
                        ? : 'field.' . $answer->field->id;
                $this->_answers[$tag] = $answer;
            }
        }
        return $this->_answers;
    }

    function hasState($state) {
        return strcasecmp($this->getState(), $state) == 0;
    }

    // Custom Function to check is user is admin - AKASH
    function isSuperuser($staff) {
        global $cfg;
        $superUsers = explode(",", $cfg->config['ost_super_user']['value']);
        if (in_array($staff->getId(), $superUsers))
            return true;
    }

    function isOpen() {
        return $this->hasState('open');
    }

    function isReopened() {
        return null !== $this->getReopenDate();
    }

    function isReopenable() {
        return $this->getStatus()->isReopenable();
    }

    function isClosed() {
        return $this->hasState('closed');
    }

    function isArchived() {
        return $this->hasState('archived');
    }

    function isSolved() {
        return $this->hasState('solved');
    }

    function isDeleted() {
        return $this->hasState('deleted');
    }

    function isAssigned() {
        return $this->isOpen() && ($this->getStaffId() || $this->getTeamId());
    }

    function isOverdue() {
        return $this->ht['isoverdue'];
    }

    function isAnswered() {
        return $this->ht['isanswered'];
    }

    function isLocked() {
        return null !== $this->getLock();
    }

    function checkStaffPerm($staff, $perm = null) {
        // Must be a valid staff
        if (!$staff instanceof Staff && !($staff = Staff::lookup($staff)))
            return false;

        //CUSTOM- To allow all tickets for SuperUser - AKASH
        if ($this->isSuperuser($staff) && $staff->getId() != $this->getStaffId()) {
            //$this->staff_id = $staff->getId() ;
        }
        if ($staff->checkIfLoginUserOfEDDepartment() || $staff->checkIfLoginUserOfRRDepartment()) {
            return true;
        }

        // Check access based on department or assignment
        if (($staff->showAssignedOnly() || !$staff->canAccessDept($this->getDeptId()))
                // only open tickets can be considered assigned
                && $this->isOpen() && $staff->getId() != $this->getStaffId() && !$staff->isTeamMember($this->getTeamId())
        ) {
            return false;
        }

        // At this point staff has view access unless a specific permission is
        // requested
        if ($perm === null)
            return true;

        // Permission check requested -- get role.
        if (!($role = $staff->getRole($this->getDeptId())))
            return false;

        // Check permission based on the effective role
        return $role->hasPerm($perm);
    }

    function checkUserAccess($user) {
        if (!$user || !($user instanceof EndUser))
            return false;

        // Ticket Owner
        if ($user->getId() == $this->getUserId())
            return true;

        // Collaborator?
        // 1) If the user was authorized via this ticket.
        if ($user->getTicketId() == $this->getId() && !strcasecmp($user->getUserType(), 'collaborator')
        ) {
            return true;
        }
        // 2) Query the database to check for expanded access...
        if (Collaborator::lookup(array(
                    'user_id' => $user->getId(),
                    'thread_id' => $this->getThreadId()))
        ) {
            return true;
        }
        return false;
    }

    // Getters
    function getNumber() {
        return $this->number;
    }

    function getOwnerId() {
        return $this->user_id;
    }

    function getOwner() {
        if (!isset($this->owner)) {
            $this->owner = new TicketOwner(new EndUser($this->user), $this);
        }
        return $this->owner;
    }

    function getEmail() {
        if ($o = $this->getOwner()) {
            return $o->getEmail();
        }
        return null;
    }

    function getReplyToEmail() {
        //TODO: Determine the email to use (once we enable multi-email support)
        return $this->getEmail();
    }

    // Deprecated
    function getOldAuthToken() {
        # XXX: Support variable email address (for CCs)
        return md5($this->getId() . strtolower($this->getEmail()) . SECRET_SALT);
    }

    function getName() {
        if ($o = $this->getOwner()) {
            return $o->getName();
        }
        return null;
    }

    function getSubject() {
        return (string) $this->_answers['subject'];
    }

    /* Help topic title  - NOT object -> $topic */

    function getHelpTopic() {
        if ($this->topic)
            return $this->topic->getFullName();
    }

    function getCreateDate() {
        return $this->created;
    }

    function getOpenDate() {
        return $this->getCreateDate();
    }

    function getReopenDate() {
        return $this->reopened;
    }

    function getUpdateDate() {
        return $this->updated;
    }

    function getEffectiveDate() {
        return $this->lastupdate;
    }

    function getDueDate() {
        return $this->duedate;
    }

    function getSLADueDate() {
        if ($sla = $this->getSLA()) {
            $dt = new DateTime($this->getCreateDate());
            $dueDate = $dt
                    ->add(new DateInterval('PT' . $sla->getGracePeriod() . 'H'))
                    ->format('Y-m-d H:i:s');
            return SLA::adjustHolidays($dueDate);
        }
    }

    function updateEstDueDate() {
        $this->est_duedate = $this->getEstDueDate();
        $this->save();
    }

    function getEstDueDate() {

        // Real due date
        if ($this->est_duedate) {
            return $this->est_duedate;
        }
        if ($duedate = $this->getDueDate()) {
            return $duedate;
        }
        // return sla due date (If ANY)
        return $this->getSLADueDate();
    }

    function getCloseDate() {
        return $this->closed;
    }

    function getSolvedDate() {
        return $this->solvedDateTime;
    }

    function getStatusId() {
        return $this->status_id;
    }

    /**
     * setStatusId
     *
     * Forceably set the ticket status ID to the received status ID. No
     * checks are made. Use ::setStatus() to change the ticket status
     */
    // XXX: Use ::setStatus to change the status. This can be used as a
    //      fallback if the logic in ::setStatus fails.
    function setStatusId($id) {
        $this->status_id = $id;
        return $this->save();
    }

    function getStatus() {
        return $this->status;
    }

    function getState() {
        if (!$this->getStatus()) {
            return '';
        }
        return $this->getStatus()->getState();
    }

    function getDeptId() {
        return $this->dept_id;
    }

    function getDeptName() {
        if ($this->dept instanceof Dept)
            return $this->dept->getFullName();
    }

    function getPriorityId() {
        global $cfg;

        if (($a = $this->_answers['priority']) && ($b = $a->getValue())
        ) {
            return $b->getId();
        }
        return $cfg->getDefaultPriorityId();
    }

    function getPriority() {
        if (($a = $this->_answers['priority']) && ($b = $a->getValue()))
            return $b->getDesc();
        return '';
    }

    function getOrder() {
        if (($a = $this->_answers['order_id']) && ($b = $a->getValue()))
            return $b;
        return '';
    }

    function getEntityType() {
        return TicketUtilityFunctions::getMandatoryInfoInWhichTicketHasBeenMade($this->getId());
    }

    function getEntities($entity) {
        if($entity=="batch_upload" || $entity=="batch_update"){
                $entity="batch";
        }      
        
        $ticketOdataObject = $this->odata;
        if (isset($ticketOdataObject)) {
            return ($ticketOdataObject->getEntityIds($entity));
        }
        $entity_ids = TicketOData::objects()->filter(array("ticket_id" => $this->getId()))->values($entity . '_ids')->all()[0][$entity . '_ids'];
        if (!empty($entity_ids))
            return $entity_ids;
        return '';
    }

    function getMandatoryInfoInWhichTicketCanBeMade() {
        return array(1 => 'order', 2 => 'manifest', 3 => 'product',4=>'batch_upload');
    }

    function setEntities($entity_ids, $entity) {
        $ticketOdataObject = $this->odata;
        $ticketOdataObject->setEntityIds($entity_ids, $entity);
    }

    function getTasks($fieldArr) {

        if (!empty($fieldArr)) {
            $tasks = Task::objects()->filter(array("object_id" => $this->getId()))->values($fieldArr)->all();
        } else {
            $tasks = Task::objects()->filter(array("object_id" => $this->getId()))->values()->all();
        }
        return $tasks;
    }

    function getAttachment() {

        $ticket_id = $this->getId();
        $sql = "SELECT A.id,A.object_id,A.type as entity_type,F.type as file_type,F.name,A.user_id,A.staff_id,"
                . "F.size,F.key,F.signature,F.created,F.path FROM mst_attachment A "
                . "INNER JOIN mst_file F ON (F.id=A.file_id) "
                . "WHERE A.ticket_id=$ticket_id and A.type!='T' and A.type !='H'";
        $res = db_query($sql);
        if (!$res) {
            return array();
        }
        while ($row = db_fetch_array($res)) {
            $result[] = $row;
        }
        return $result;
    }

    function getReturn() {
        if (($a = $this->_answers['retrn_id']) && ($b = $a->getValue()))
            return $b;
        return '';
    }

    // Get order no for ticket:Used  in ticket preview:added by vishal

    function getOdataField($fieldArr) {
        if ($fieldArr)
            $ticketdata = Ticket::objects()->filter(array("ticket_id" => $this->getId()))->values(implode(",", $fieldArr))->all();
        foreach ($fieldArr as $field) {
            $val[$field] = $ticketdata[0][$field];
        }

        return $val;
    }

    function getuser_id() {
        return $this->getOdataField(array("user_id"))["user_id"];
    }

    function getOrderStatus() {
        global $statusPerform;
        if ($statusPerform[$this->getId()])
            return $statusPerform[$this->getId()];
        else if (($orderStatus = $this->getOdataField(array("ord_status"))["ord_status"]))
            return $orderStatus;
        else {
            $orderId = TicketOData::objects()->filter(array("ticket_id" => $this->getId()))->values("order_id")->all()[0]["order_id"];
            if ($orderId)
                $orderStatus = CscartOrders::objects()->filter(array("order_id" => $orderId))->values("status")->limit(1)->all();
            return $orderStatus[0]["status"]? : 0;
        }
    }

    function getOrderStatusFromOrderIds($orderIds) {
        $orderStatusOrderIds = CscartOrders::objects()->filter(array("order_id__in" => $orderIds))->values("status", "order_id")->all();
        $orderIdStatusMap = [];
        foreach ($orderStatusOrderIds as $orderStatusOrderId) {
            $orderIdStatusMap[$orderStatusOrderId['order_id']] = $orderStatusOrderId['status'];
        }
        return $orderIdStatusMap;
    }

    function getCurrentReturnStatus() {
        $returnId = TicketOData::objects()->filter(array("ticket_id" => $this->getId()))->values("retrn_id")->all()[0]["retrn_id"];
        if ($returnId)
            $returnStatus = CscartRmaReturns::objects()->filter(array("return_id" => $returnId))->values("status")->limit(1)->all();
        return $returnStatus[0]["status"]? : 0;
    }

    function getCurrentRefundStatus() {
        $orderId = TicketOData::objects()->filter(array("ticket_id" => $this->getId()))->values("order_id")->all()[0]["order_id"];
        if ($orderId)
            $refundStatus = CluesRefunds::objects()->filter(array("order_id" => $orderId))->values("status")->order_by("-id")->limit(1)->all();
        return $refundStatus[0]["status"]? : 0;
    }

    /* function getOrderStatus() {
      if (($a = $this->_answers['ord_status']) && ($b = $a->getValue()))
      return $b;
      return '';
      }
     */

    /**
     * Function to get custom field from $ticket object
     * @param string $field - name of field
     * @return value of custom field
     * @author Akash Kumar
     */
    function getCustomField($field) {
        if (($a = $this->_answers[$field]) && ($b = $a->getValue()))
            return $b;
        return '';
    }

    function getPhoneNumber() {
        return (string) $this->getOwner()->getPhoneNumber();
    }

    function getSource() {
        return $this->source;
    }

    function getIP() {
        return $this->ip_address;
    }

    function getHashtable() {
        return $this->ht;
    }

    function getUpdateInfo() {
        return array(
            'source' => $this->getSource(),
            'topicId' => $this->getTopicId(),
            'slaId' => $this->getSLAId(),
            'user_id' => $this->getOwnerId(),
            'duedate' => $this->getDueDate() ? Format::date($this->getDueDate()) : '',
            'time' => $this->getDueDate() ? (Format::date($this->getDueDate(), true, 'HH:mm')) : '',
        );
    }

    function getLock() {
        return $this->lock;
    }

    function acquireLock($staffId, $lockTime) {

        if (!$staffId or ! $lockTime) //Lockig disabled?
            return null;

        // Check if the ticket is already locked.
        if (($lock = $this->getLock()) && !$lock->isExpired()) {
            if ($lock->getStaffId() != $staffId) //someone else locked the ticket.
                return null;

            //Lock already exits...renew it
            $lock->renew($lockTime); //New clock baby.

            return $lock;
        }
        // No lock on the ticket or it is expired
        $this->lock = Lock::acquire($staffId, $lockTime); //Create a new lock..

        if ($this->lock) {
            $this->save();
        }

        // load and return the newly created lock if any!
        return $this->lock;
    }

    function releaseLock($staffId = false) {
        if (!($lock = $this->getLock()))
            return false;

        if ($staffId && $lock->staff_id != $staffId)
            return false;

        if (!$lock->delete())
            return false;

        $this->lock = null;
        return $this->save();
    }

    function getDept() {
        global $cfg;

        return $this->dept ? : $cfg->getDefaultDept();
    }

    function getUserId() {
        return $this->getOwnerId();
    }

    function getUser() {
        if (!isset($this->_user) && $this->user) {
            $this->_user = new EndUser($this->user);
        }
        return $this->_user;
    }

    function getStaffId() {
        return $this->staff_id;
    }

    function getStaff() {
        return $this->staff;
    }

    function getClosedByStaff() {
        if (!isset($this->closed_staff_id)) {
            $ticketArray = Ticket::objects()->filter(array('ticket_id' => $this->ticket_id))->values('closed_staff_id', 'solved_staff_id')->limit(1)->all();
            $this->closed_staff_id = $ticketArray[0]['closed_staff_id'];
            $this->solved_staff_id = $ticketArray[0]['solved_staff_id'];
        }
        return Staff::objects()->filter(array('staff_id' => $this->closed_staff_id))->first();
    }

    function getSolvedByStaff() {
        if (!isset($this->solved_staff_id)) {
            $ticketArray = Ticket::objects()->filter(array('ticket_id' => $this->ticket_id))->values('closed_staff_id', 'solved_staff_id')->limit(1)->all();
            $this->closed_staff_id = $ticketArray[0]['closed_staff_id'];
            $this->solved_staff_id = $ticketArray[0]['solved_staff_id'];
        }
        return Staff::objects()->filter(array('staff_id' => $this->solved_staff_id))->first();
    }

	function getTeamId() {
        return $this->team_id;
    }

    function getTeam() {
        return $this->team;
    }

    function getAssignee() {

        if ($staff = $this->getStaff())
            return $staff->getName();
        if ($team = $this->getTeam())
            return $team->getName();

        return '';
    }

    function getAssignees() {

        $assignees = array();
        if ($staff = $this->getStaff())
            $assignees[] = $staff->getName();

        if ($team = $this->getTeam())
            $assignees[] = $team->getName();

        return $assignees;
    }

    function getAssigned($glue = '/') {
        $assignees = $this->getAssignees();
        return $assignees ? implode($glue, $assignees) : '';
    }

    function getTopicId() {
        return $this->topic_id;
    }

    function getTopic() {
        return $this->topic;
    }

    function getSLAId() {
        return $this->sla_id;
    }

    function getSLA() {
        return $this->sla;
    }

    function getLastRespondent() {
        if (!isset($this->lastrespondent)) {
            $this->lastrespondent = Staff::objects()
                            ->filter(array(
                                'staff_id' => static::objects()
                                ->filter(array(
                                    'thread__entries__type' => 'R',
                                    'thread__entries__staff_id__gt' => 0
                                ))
                                ->values_flat('thread__entries__staff_id')
                                ->order_by('-thread__entries__id')
                                ->limit(1)
                            ))
                            ->first()
                    ? : false;
        }
        return $this->lastrespondent;
    }

    function getLastMessageDate() {
        return $this->thread->lastmessage;
    }

    function getLastMsgDate() {
        return $this->getLastMessageDate();
    }

    /*
      get last response date from thread. This function should have been made private
     */

    function getLastResponseDate() {
        return $this->thread->lastresponse;
    }

    /*
      This function calls above function only.
     */

    function getLastRespDate() {
        return $this->getLastResponseDate();
    }

    /*
      3 fields in mst_ticket table. last_response_staff,last_response_customer and last_response_internal. So according to the requirement, change this function.
     */

    function getLastResponded() {
        return $this->last_response_staff;
    }

    function getLastMsgId() {
        return $this->lastMsgId;
    }

    function getLastMessage() {
        if (!isset($this->last_message)) {
            if ($this->getLastMsgId())
                $this->last_message = MessageThreadEntry::lookup(
                                $this->getLastMsgId(), $this->getThreadId());

            if (!$this->last_message)
                $this->last_message = $this->getThread()->getLastMessage();
        }
        return $this->last_message;
    }

    function getNumTasks() {
        // FIXME: Implement this after merging Tasks
        return count($this->tasks);
    }

    function getThreadId() {
        if ($this->thread)
            return $this->thread->id;
    }

    function getThread() {
        return $this->thread;
    }

    function getThreadCount() {
        return $this->getClientThread()->count();
    }

    function getNumMessages() {
        return $this->getThread()->getNumMessages();
    }

    function getNumResponses() {
        return $this->getThread()->getNumResponses();
    }

    function getNumNotes() {
        return $this->getThread()->getNumNotes();
    }

    function getMessages() {
        return $this->getThreadEntries(array('M'));
    }

    function getResponses() {
        return $this->getThreadEntries(array('R'));
    }

    function getNotes() {
        return $this->getThreadEntries(array('N'));
    }

    function getClientThread() {
        return $this->getThreadEntries(array('M', 'R'));
    }

    function getThreadEntry($id) {
        return $this->getThread()->getEntry($id);
    }

    function getThreadEntries($type = false) {
        $entries = $this->getThread()->getEntries();
        if ($type && is_array($type))
            $entries->filter(array('type__in' => $type));

        return $entries;
    }

    //UserList of recipients  (owner + collaborators)
    function getRecipients() {
        if (!isset($this->recipients)) {
            $list = new UserList();
            $list->add($this->getOwner());
            if ($collabs = $this->getThread()->getActiveCollaborators()) {
                foreach ($collabs as $c)
                    $list->add($c);
            }
            $this->recipients = $list;
        }
        return $this->recipients;
    }

    function hasClientEditableFields() {
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            foreach ($form->getFields() as $field) {
                if ($field->isEditableToUsers())
                    return true;
            }
        }
    }

    function getMissingRequiredFields() {
        $returnArray = array();
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            foreach ($form->getFields() as $field) {
                if ($field->isRequiredForClose()) {
                    if (!$field->answer || !$field->answer->get('value')) {
                        array_push($returnArray, $field->getLocal('label'));
                    }
                }
            }
        }
        return $returnArray;
    }

    function getMissingRequiredField() {
        $fields = $this->getMissingRequiredFields();
        return $fields[0];
    }

    function addCollaborator($user, $vars, &$errors, $event = true) {

        if (!$user || $user->getId() == $this->getOwnerId())
            return null;

        if ($c = $this->getThread()->addCollaborator($user, $vars, $errors, $event)) {
            $this->collaborators = null;
            $this->recipients = null;
        }

        return $c;
    }

    //XXX: Ugly for now
    function updateCollaborators($vars, &$errors) {
        global $thisstaff;

        if (!$thisstaff)
            return;

        //Deletes
        if ($vars['del'] && ($ids = array_filter($vars['del']))) {
            $collabs = array();
            foreach ($ids as $k => $cid) {
                if (($c = Collaborator::lookup($cid)) && $c->getTicketId() == $this->getId() && $c->delete())
                    $collabs[] = (string) $c;
            }

            $this->logEvent('collab', array('del' => $collabs));
        }

        //statuses
        $cids = null;
        if ($vars['cid'] && ($cids = array_filter($vars['cid']))) {
            $this->getThread()->collaborators->filter(array(
                'thread_id' => $this->getThreadId(),
                'id__in' => $cids
            ))->update(array(
                'updated' => SqlFunction::NOW(),
                'isactive' => 1,
            ));
        }

        if ($cids) {
            $this->getThread()->collaborators->filter(array(
                'thread_id' => $this->getThreadId(),
                Q::not(array('id__in' => $cids))
            ))->update(array(
                'updated' => SqlFunction::NOW(),
                'isactive' => 0,
            ));
        }

        unset($this->active_collaborators);
        $this->collaborators = null;

        return true;
    }

    function getAuthToken($user, $algo = 1) {

        //Format: // <user type><algo id used>x<pack of uid & tid><hash of the algo>
        $authtoken = sprintf('%s%dx%s', ($user->getId() == $this->getOwnerId() ? 'o' : 'c'), $algo, Base32::encode(pack('VV', $user->getId(), $this->getId())));

        switch ($algo) {
            case 1:
                $authtoken .= substr(base64_encode(
                                md5($user->getId() . $this->getCreateDate() . $this->getId() . SECRET_SALT, true)), 8);
                break;
            default:
                return null;
        }

        return $authtoken;
    }

    function sendAccessLink($user) {
        global $ost;

        if (!($email = $ost->getConfig()->getDefaultEmail()) || !($content = Page::lookupByType('access-link')))
            return;

        $vars = array(
            'url' => $ost->getConfig()->getBaseUrl(),
            'ticket' => $this,
            'user' => $user,
            'recipient' => $user,
        );

        $lang = $user->getLanguage(UserAccount::LANG_MAILOUTS);
        $msg = $ost->replaceTemplateVariables(array(
            'subj' => $content->getLocalName($lang),
            'body' => $content->getLocalBody($lang),
                ), $vars);

        $email->send($user, Format::striptags($msg['subj']), $msg['body']);
    }

    /* -------------------- Setters --------------------- */

    function setLastMsgId($msgid) {
        return $this->lastMsgId = $msgid;
    }

    function setLastMessage($message) {
        $this->last_message = $message;
        $this->setLastMsgId($message->getId());
    }

    function setDueDate($dateTime) {
        $this->est_duedate = $dateTime;
        $this->duedate = $dateTime;
        $this->save();
    }

    //DeptId can NOT be 0. No orphans please!
    function setDeptId($deptId) {
        // Make sure it's a valid department
        if ($deptId == $this->getDeptId() || !($dept = Dept::lookup($deptId))) {
            return false;
        }
        $this->dept = $dept;
        return $this->save();
    }

    // Set staff ID...assign/unassign/release (id can be 0)
    function setStaffId($staffId) {
        if (!is_numeric($staffId))
            return false;

        $this->staff = Staff::lookup($staffId);
        return $this->save();
    }

    function setSLAId($slaId) {
        if ($slaId == $this->getSLAId())
            return true;

        $this->sla = Sla::lookup($slaId);
        return $this->save();
    }

    function setPriority($priorityId) {
        if (!is_numeric($priorityId))
            return false;
        $this->odata->priority = $priorityId;
        return $this->odata->save();
    }

    function updateLastResponse($type) {
        switch ($type) {
            case 'reply':
                $this->last_response_staff = new SqlFunction("NOW");
                break;
            case 'user':
                $this->last_response_customer = new SqlFunction("NOW");
                break;
            case 'note':
                $this->last_response_internal = new SqlFunction("NOW");
                break;
        }
    }

    /**
     * Selects the appropriate service-level-agreement plan for this ticket.
     * When tickets are transfered between departments, the SLA of the new
     * department should be applied to the ticket. This would be useful,
     * for instance, if the ticket is transferred to a different department
     * which has a shorter grace period, the ticket should be considered
     * overdue in the shorter window now that it is owned by the new
     * department.
     *
     * $trump - if received, should trump any other possible SLA source.
     *          This is used in the case of email filters, where the SLA
     *          specified in the filter should trump any other SLA to be
     *          considered.
     */
    function selectSLAId($trump = null) {
        global $cfg;
        # XXX Should the SLA be overridden if it was originally set via an
        #     email filter? This method doesn't consider such a case
        if ($trump && is_numeric($trump)) {
            $slaId = $trump;
        } elseif ($this->getDept() && $this->getDept()->getSLAId()) {
            $slaId = $this->getDept()->getSLAId();
        } elseif ($this->getTopic() && $this->getTopic()->getSLAId()) {
            $slaId = $this->getTopic()->getSLAId();
        } else {
            $slaId = $cfg->getDefaultSLAId();
        }

        return ($slaId && $this->setSLAId($slaId)) ? $slaId : false;
    }

    //Set team ID...assign/unassign/release (id can be 0)
    function setTeamId($teamId) {
        if (!is_numeric($teamId))
            return false;

        $this->team = Team::lookup($teamId);
        return $this->save();
    }

    // Function to get triaged data
    function getTriagedData() {
        global $thisstaff;
        global $cfg;
        $edDepartmentId = $cfg->getEDDepartmentId();
        $ticketDeptId = $this->getDeptId();
        $sql = "select * from " . TRIAGE_TABLE . " WHERE `ticket_id`=" . $this->getId();
        $res = db_query($sql);
        while ($row = db_fetch_array($res)) {
            $results = $row;
        }
        $triagedData = array();
        $staticTriageData = getCachedTriageFields();
        $showEdDepartmentTriageFields = ($thisstaff->checkIfLoginUserOfEDDepartment() && $edDepartmentId == $ticketDeptId);
        $staticTriageData['Priority Flags'] = $this->getPriorityFlags($staticTriageData);
        ksort($staticTriageData['Priority Flags']);
        $triagedData["priority"] = $staticTriageData['Priority Flags'];

        ksort($staticTriageData['Communication Flags']);
        $triagedData["communication"] = $staticTriageData['Communication Flags'];

        ksort($staticTriageData['Triage Status']);
        $triagedData["triage"] = $staticTriageData['Triage Status'];
        if ($showEdDepartmentTriageFields) {
            $triagedData["communication_selected"] = $cfg->getEDTriageDefaultCommunicationId();
            $triagedData["call_customer_selected"] = $cfg->getEDTriageDefaultCallCustomerId();
        } else {

            $triagedData["communication_selected"] = $results["communication"];
            $triagedData["call_customer_selected"] = $results["call_customer"];
        }

        $triagedData["priority_selected"] = $results["priority"];
        $triagedData["customerUpdate_selected"] = $results["customer_update"];
        $triagedData["triage_selected"] = 2;
        //SLA
        $slas = SLA::getSLAs();
        foreach ($slas as $id => $slaName) {
            $triagedData["sla"][$id] = $slaName;
        }
        $triagedData["sla_selected"] = $results["sla"];

        $triagedData["comment"] = array(1 => "Read", 2 => "Not Read");
        $triagedData["comment_selected"] = 1;

        $triagedData["customerUpdate"] = array("1" => "Auto");


        $triagedData["call_customer"] = array(1 => "No", 2 => "Yes");


        $triagedData["task_status"] = array(1 => "Auto", 2 => "Manual");
        $triagedData["task_status_selected"] = $results["task_status"];

        return $triagedData;
    }

    function getPriorityFlags($staticTriageData) {
        global $thisstaff;
        $staticTriageDataUpdated["Priority Flags"] = [];
        $showEdDepartmentTriageFields = ($thisstaff->checkIfLoginUserOfEDDepartment() && $edDepartmentId == $ticketDeptId);
        foreach ($staticTriageData['Priority Flags'] as $id => $value) {
            $propertyToBeShownOnEdPanel = $staticTriageData["PropName_Priority Flags"][$id]["showOnEDPanel"];
            $propertyToBeShownOnNormalPanel = $staticTriageData["PropName_Priority Flags"][$id]["showOnNormalPanel"];
            if (($showEdDepartmentTriageFields && $propertyToBeShownOnEdPanel)) {
                $staticTriageDataUpdated["Priority Flags"][$id] = $value;
                //show on ed panel
            }
            if (!$showEdDepartmentTriageFields && $propertyToBeShownOnNormalPanel) {
                $staticTriageDataUpdated["Priority Flags"][$id] = $value;
                //show on normal panel
            }
        };
        return $staticTriageDataUpdated["Priority Flags"];
    }

    /**
     * change triage on user new comment
     */
    function changeTriage($poster,$source='mail') {
                 // 2 sources more info , from mail
        /**
         * source =more info
         * triage==4 where
         * source = more info then update to 4 if not 5
         * source = mail then update to 3 if not 5
         * status=4 for triage again by more info
         * status=5 for triage again by both more info and email
         */
        $sourceToTriageMap=array('mail'=>3,'more_info'=>4,'both'=>5);
        $sourceToTriageMapText=array('mail'=>"Changed to Triage Again By merchant Reply",
            'more_info'=>"Changed to Triage Again By More Info"
            );

        // triage again by more info , triage again by mail , not triage
        $triageObject=Triage::objects()->filter(array('triage__in'=>array('2','3','4'),'ticket_id'=>$this->getId()))->first();
        if(!($triageObject instanceof Triage)){
            return;
        }
        switch ($triageObject->triage){
            case '2':
                $triageObject->triage=$sourceToTriageMap[$source];
                $triageObject->triage_again=SqlFunction::NOW();
                break;
            case '3':
                if($source=='more_info'){
                    $triageObject->triage=$sourceToTriageMap['both'];
                }
                break;
            case '4':
                if($source=='mail'){
                    $triageObject->triage=$sourceToTriageMap['both'];
                }
                break;
        }
        $triageText=$sourceToTriageMapText[$source];
        $triageObject->save();
        $this->logNote("Triage again", $triageText, $poster);
 


 /**
        $sql = "Update " . TRIAGE_TABLE . " SET `triage_again`=NOW() WHERE `ticket_id`=" . $this->getId() . " AND `triage`=2";
        $res = db_query($sql);
        $sql = "Update " . TRIAGE_TABLE . " SET `triage`=3 WHERE `ticket_id`=" . $this->getId() . " AND `triage`!=1";
        $res = db_query($sql);
        $sql = "select * from " . TRIAGE_TABLE . " WHERE `ticket_id`=" . $this->getId() . " AND `triage`=3";
        $res = db_query($sql);
        while ($row = db_fetch_array($res)) {
            $results = $row;
        }
        if (count($results) > 0) {
            $this->logNote("Triage again", "Changed to Triage Again By customer Reply", $poster);
        }
      */
    }

    function hook($action) {
        $action = 'On' . ucfirst($action);
        $this->$action();
    }

    function OnClosed() {
        $tasks = Task::objects()->filter(array("object_type" => "T", "object_id" => $this->getId()));
        foreach ($tasks as $task) {
            if ($task->isOpen()) {
                $task->setStatus('closed', 'On Ticket Close - Task is closed By System');
                $data = array("taskId" => $task->id, "taskStatus" => 5, "taskAgent" => "System Auto", "email" => "system.auto@shopclues.com");
                $statusChange = $task->updateTask($data, $error);
            }
        }
        $this->onCloseTicket();
    }

    //Status helper.

    function setStatus($status, $comments = '', &$errors = array(), $set_closing_agent = false, $titleNote = "", $set_solving_agent = false) {
        global $thisstaff;
        if ($thisstaff && !($role = $thisstaff->getRole($this->getDeptId())))
            return false;

        if ($status && is_numeric($status))
            $status = TicketStatus::lookup($status);

        if (!$status || !$status instanceof TicketStatus)
            return false;

        // Double check permissions (when changing status)
        if ($role && $this->getStatusId()) {
            switch ($status->getState()) {
                case 'closed':
                    if (!($role->hasPerm(TicketModel::PERM_CLOSE)))
                        return false;
                    break;
                case 'deleted':
                    // XXX: intercept deleted status and do hard delete
                    if ($role->hasPerm(TicketModel::PERM_DELETE)) {
                        return $this->delete($comments);
                    }
                    // Agent doesn't have permission to delete  tickets
                    return false;
                    break;
            }
        }

        $hadStatus = $this->getStatusId();
        if ($this->getStatusId() == $status->getId())
            return true;

        // Perform checks on the *new* status, _before_ the status changes
        $ecb = null;
	global $cfg;
        switch ($status->getState()) {
            case 'solved':
                $this->solvedDateTime = $this->lastupdate = SqlFunction::NOW();
		if(is_object($thisstaff)){
			$this->solved_staff_id=$thisstaff->getId();
		}else{
		     $systemAutoStaffId=$cfg->config['system_auto_staff_id']['value'];
		     $this->solved_staff_id=$systemAutoStaffId;
		}
                if ($thisstaff && $set_solving_agent)
                    $this->staff = $thisstaff;
                $ecb = function($t) use ($status) {
                    $t->logEvent('solved', array('status' => array($status->getId(), $status->getName())));
                };
                break;
            case 'reopened':
                $this->reopenedDateTime = $this->lastupdate = SqlFunction::NOW();
                $solvedDateTime = $this->getSolvedDate();
                $ticketReopenLogs = TicketReopenLogs::create();
                $ticketReopenLogs->ticket_id = $this->getId();
                $ticketReopenLogs->solvedDateTime = $solvedDateTime;
                $ticketReopenLogs->reopenDateTime = $this->reopenedDateTime;
                $ticketReopenLogs->save();
                $this->solvedDateTime = null;
                $ecb = function($t) use ($status) {
                    $t->logEvent('reopened', array('status' => array($status->getId(), $status->getName())));
                };
                break;
            case 'closed':
                if ($this->getMissingRequiredFields()) {
                    $errors['err'] = sprintf(__(
                                    'This ticket is missing data on %s one or more required fields %s and cannot be closed'), '', '');
                    return false;
                }
                $this->closed = $this->lastupdate = SqlFunction::NOW();
		global $cfg;
		$systemAutoStaff_id=$cfg->config['system_auto_staff_id']['value'];
		$this->closed_staff_id=$systemAutoStaff_id;
                $this->duedate = null;
                if ($thisstaff && $set_closing_agent)
                    $this->staff = $thisstaff;
                $this->clearOverdue(false);

                $ecb = function($t) use ($status) {
                    $t->logEvent('closed', array('status' => array($status->getId(), $status->getName())));
                    $t->deleteDrafts();
                };
                break;
            case 'open':
                // TODO: check current status if it allows for reopening
                if ($this->isClosed()) {
                    if ($cfg->config['AllowTicketReOpen']['value']) {
                        $this->closed = $this->lastupdate = $this->reopened = SqlFunction::NOW();
                        $ecb = function ($t) {
                            $t->logEvent('reopened', false, null, 'closed');
                        };
                    } else {
                        return false;
                    }
                }
                
                if($this->getState()=='solved'){
                  $this->lastupdate = $this->reopened = SqlFunction::NOW();
                  $ecb = function ($t) {
                      $t->logEvent('reopened', false, null, 'closed');
                        };
                }else{
//return false;
}

                // If the ticket is not open then clear answered flag
                if (!$this->isOpen())
                    $this->isanswered = 0;
                break;
            default:
                return false;
        }

        $this->status = $status;
        if (!$this->save())
            return false;
        $cluesMerchantObject=CluesMerchantTicket::objects()->filter(array('ticket_number'=>$this->getNumber()))->first();
        if ($cluesMerchantObject instanceof CluesMerchantTicket) {
            $cluesMerchantObject->ticket_status = $status->getState();
            $cluesMerchantObject->status = $status->getId();
            $cluesMerchantObject->save();
        }
        if ($status->getState() == "closed") {
            $this->hook('closed');
        }
        // Log status change b4 reload - if currently has a status. (On new
        // ticket, the ticket is opened and thereafter the status is set to
        // the requested status).
        if ($hadStatus) {
            $alert = false;
            if ($comments) {
                // Send out alerts if comments are included
                $alert = true;
                if (!$titleNote)
                    $titleNote = __('Status Changed');
                $this->logNote($titleNote, $comments, $thisstaff, $alert);
            }
        }
        // Log events via callback
        if ($ecb)
            $ecb($this);
        elseif ($hadStatus)
        // Don't log the initial status change
            $this->logEvent('edited', array('status' => $status->getId()));

        return true;
    }

    function setState($state, $alerts = false) {
        switch (strtolower($state)) {
            case 'open':
                return $this->setStatus('open');
            case 'closed':
                return $this->setStatus('closed');
            case 'answered':
                return $this->setAnsweredState(1);
            case 'unanswered':
                return $this->setAnsweredState(0);
            case 'overdue':
                return $this->markOverdue();
            case 'notdue':
                return $this->clearOverdue();
            case 'unassined':
                return $this->unassign();
        }
        // FIXME: Throw and excception and add test cases
        return false;
    }

    function setAnsweredState($isanswered) {
        $this->isanswered = $isanswered;
        return $this->save();
    }

    function reopen() {
        global $cfg;

        if (!$cfg->config['AllowTicketReOpen']['value'])
            return false;

        if (!$this->isClosed())
            return false;

        // Set status to open based on current closed status settings
        // If the closed status doesn't have configured "reopen" status then use the
        // the default ticket status.
        if (!($status = $this->getStatus()->getReopenStatus()))
            $status = $cfg->getDefaultTicketStatusId();

        return $status ? $this->setStatus($status) : false;
    }

    function onNewTicket($message, $autorespond = true, $alertstaff = true, $vars = '') {
        global $cfg;

        //Log stuff here...

        if (!$autorespond && !$alertstaff)
            return true; //No alerts to send.

            /* ------ SEND OUT NEW TICKET AUTORESP && ALERTS ---------- */

        if (!$cfg || !($dept = $this->getDept()) || !($tpl = $dept->getTemplate()) || !($email = $dept->getAutoRespEmail())
        ) {
            return false;  //bail out...missing stuff.
        }

        $options = array();
        if ($message instanceof ThreadEntry) {
            $options += array(
                'inreplyto' => $message->getEmailMessageId(),
                'references' => $message->getEmailReferences(),
                'thread' => $message
            );
        } else {
            $options += array(
                'thread' => $this->getThread(),
            );
        }

        //Send auto response - if enabled.
        if ($autorespond && $cfg->autoRespONNewTicket() && $dept->autoRespONNewTicket()) {
            if ($vars["return_id"] && $vars["isReturn"]) {  // for new Return tickets
                $msg = $tpl->getReturnAutoRespMsgTemplate();
                $msg = $msg->asArray();
                $msg = $this->replaceVars(
                        $msg, array('message' => $message,
                    'recipient' => $this->getOwner(),
                    'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : '')
                );
            } else {  // for other new tickets
                $msg = $tpl->getAutoRespMsgTemplate();

                $msg = $this->replaceVars(
                        $msg->asArray(), array('message' => $message,
                    'recipient' => $this->getOwner(),
                    'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : '',
		    'sla'=>$vars['sla'])
                );
            }
            //      $return_id = $vars['return_id'] ? $vars['return_id'] : 0;
            //          $msg['body'] = Canned::getOrderInfoForTicket($return_id, $vars['order_id'], $msg['body']);
            //        $msg['subj'] = Canned::getOrderInfoForTicket($return_id, $vars['order_id'], $msg['subj']);
            $email->sendAutoReply($this->getOwner(), $msg['subj'], $msg['body'], null, $options);
            $autoSentMessage["note"] = "Subject - " . $msg['subj'] . "<br>" . $msg['body'];
            $this->postNote($autoSentMessage);
        }
        // Send alert to out sleepy & idle staff.
        if ($alertstaff && $cfg->alertONNewTicket() && ($email = $dept->getAlertEmail()) && ($msg = $tpl->getNewTicketAlertMsgTemplate())
        ) {
            $msg = $this->replaceVars($msg->asArray(), array('message' => $message));
            $recipients = $sentlist = array();
            // Exclude the auto responding email just incase it's from staff member.
            if ($message instanceof ThreadEntry && $message->isAutoReply())
                $sentlist[] = $this->getEmail();

            // Alert admin??
            if ($cfg->alertAdminONNewTicket()) {
                $alert = $this->replaceVars($msg, array('recipient' => 'Admin'));
                $email->sendAlert($cfg->getAdminEmail(), $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $cfg->getAdminEmail();
            }

            // Only alerts dept members if the ticket is NOT assigned.
            if ($cfg->alertDeptMembersONNewTicket() && !$this->isAssigned()) {
                if ($members = $dept->getMembersForAlerts())
                    $recipients = array_merge($recipients, $members);
            }

            if ($cfg->alertDeptManagerONNewTicket() && $dept && ($manager = $dept->getManager()))
                $recipients[] = $manager;

            // Account manager
            if ($cfg->alertAcctManagerONNewMessage() && ($org = $this->getOwner()->getOrganization()) && ($acct_manager = $org->getAccountManager())
            ) {
                if ($acct_manager instanceof Team)
                    $recipients = array_merge($recipients, $acct_manager->getMembers());
                else
                    $recipients[] = $acct_manager;
            }

            foreach ($recipients as $k => $staff) {
                if (!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)
                ) {
                    continue;
                }
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }
        return true;
    }

    function onCloseTicket($autorespond = true, $alertstaff = true) {
        global $cfg;

        //Log stuff here...
        if (!$autorespond && !$alertstaff)
            return true; //No alerts to send.

        if (!$cfg->config['ticketCloseAutoReply']['value'])
            return true; //No alerts to send.

        if (!$cfg || !($dept = $this->getDept()) || !($tpl = $dept->getTemplate()) || !($email = $dept->getAutoRespEmail())
        ) {
            return false;  //bail out...missing stuff.
        }

        //Send auto response - if enabled.
        if ($autorespond && ($msg = $tpl->getTicketCloseMsgTemplate())
        ) {
            $msg = $this->replaceVars(
                    $msg->asArray(), array('message' => $message,
                'recipient' => $this->getOwner(),
                'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : ''
                    )
            );
            $options = array();
            $email->sendAutoReply($this->getOwner(), $msg['subj'], $msg['body'], null, $options);
            $autoSentMessage["note"] = "Subject - " . $msg['subj'] . "<br>" . $msg['body'];
            $this->postNote($autoSentMessage);
        }
    }

    function onOpenLimit($sendNotice = true) {
        global $ost, $cfg;

        //Log the limit notice as a warning for admin.
        $msg = sprintf(_S('Maximum open tickets (%1$d) reached for %2$s'), $cfg->getMaxOpenTickets(), $this->getEmail());
        $ost->logWarning(sprintf(_S('Maximum Open Tickets Limit (%s)'), $this->getEmail()), $msg);

        if (!$sendNotice || !$cfg->sendOverLimitNotice())
            return true;

        //Send notice to user.
        if (($dept = $this->getDept()) && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getOverlimitMsgTemplate()) && ($email = $dept->getAutoRespEmail())
        ) {
            $msg = $this->replaceVars(
                    $msg->asArray(), array('signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : '')
            );

            $email->sendAutoReply($this->getOwner(), $msg['subj'], $msg['body']);
        }

        $user = $this->getOwner();

        // Alert admin...this might be spammy (no option to disable)...but it is helpful..I think.
        $alert = sprintf(__('Maximum open tickets reached for %s.'), $this->getEmail()) . "\n"
                . sprintf(__('Open tickets: %d'), $user->getNumOpenTickets()) . "\n"
                . sprintf(__('Max allowed: %d'), $cfg->getMaxOpenTickets())
                . "\n\n" . __("Notice sent to the user.");

        $ost->alertAdmin(__('Overlimit Notice'), $alert);

        return true;
    }

    function onResponse($response, $options = array()) {
        $this->isanswered = 1;
        $this->save();

        $vars = array_merge($options, array(
            'activity' => _S('New Response'),
            'threadentry' => $response
                )
        );
        $this->onActivity($vars);
    }

    /*
     * Notify collaborators on response or new message
     *
     */

    function notifyCollaborators($entry, $vars = array()) {
        global $cfg;

        if (!$entry instanceof ThreadEntry || !($recipients = $this->getRecipients()) || !($dept = $this->getDept()) || !($tpl = $dept->getTemplate()) || !($msg = $tpl->getActivityNoticeMsgTemplate()) || !($email = $dept->getEmail())
        ) {
            return;
        }
        // Who posted the entry?
        $skip = array();
        if ($entry instanceof Message) {
            $poster = $entry->getUser();
            // Skip the person who sent in the message
            $skip[$entry->getUserId()] = 1;
            // Skip all the other recipients of the message
            foreach ($entry->getAllEmailRecipients() as $R) {
                foreach ($recipients as $R2) {
                    if (0 === strcasecmp($R2->getEmail(), $R->mailbox . '@' . $R->host)) {
                        $skip[$R2->getUserId()] = true;
                        break;
                    }
                }
            }
        } else {
            $poster = $entry->getStaff();
            // Skip the ticket owner
            $skip[$this->getUserId()] = 1;
        }

        $vars = array_merge($vars, array(
            'message' => (string) $entry,
            'poster' => $poster ? : _S('A collaborator'),
                )
        );

        $msg = $this->replaceVars($msg->asArray(), $vars);

        $attachments = $cfg->emailAttachments() ? $entry->getAttachments() : array();
        $options = array('inreplyto' => $entry->getEmailMessageId(),
            'thread' => $entry);
        foreach ($recipients as $recipient) {
            // Skip folks who have already been included on this part of
            // the conversation
            if (isset($skip[$recipient->getUserId()]))
                continue;
            $notice = $this->replaceVars($msg, array('recipient' => $recipient));
            $email->send($recipient, $notice['subj'], $notice['body'], $attachments, $options);
        }
    }

    function onMessage($message, $autorespond = true) {
        global $cfg;

        $this->isanswered = 0;
        $this->lastupdate = SqlFunction::NOW();
        $this->save();


        // Reopen if closed AND reopenable
        // We're also checking autorespond flag because we don't want to
        // reopen closed tickets on auto-reply from end user. This is not to
        // confused with autorespond on new message setting
        if ($autorespond && $this->isClosed() && $this->isReopenable()) {
            $this->reopen();

            // Auto-assign to closing staff or last respondent
            // If the ticket is closed and auto-claim is not enabled then put the
            // ticket back to unassigned pool.
            if (!$cfg->autoClaimTickets()) {
                $this->setStaffId(0);
            } elseif (!($staff = $this->getStaff()) || !$staff->isAvailable()) {
                // Ticket has no assigned staff -  if auto-claim is enabled then
                // try assigning it to the last respondent (if available)
                // otherwise leave the ticket unassigned.
                if (($lastrep = $this->getLastRespondent()) && $lastrep->isAvailable()
                ) {
                    $data['claim'] = true;
                    $data['staff'] = $lastrep->getId();
                    $this->logEvent('assigned', $data);
                    $this->setStaffId($lastrep->getId()); //direct assignment;
                } else {
                    // unassign - last respondent is not available.
                    $this->setStaffId(0);
                }
            }
        }

        // Figure out the user
        if ($this->getOwnerId() == $message->getUserId())
            $user = new TicketOwner(
                    User::lookup($message->getUserId()), $this);
        else
            $user = Collaborator::lookup(array(
                        'user_id' => $message->getUserId(),
                        'thread_id' => $this->getThreadId()));

        /*         * ********   double check auto-response  *********** */
        if (!$user)
            $autorespond = false;
        elseif ($autorespond && (Email::getIdByEmail($user->getEmail())))
            $autorespond = false;
        elseif ($autorespond && ($dept = $this->getDept()))
            $autorespond = $dept->autoRespONNewMessage();

        if (!$autorespond || !$cfg->autoRespONNewMessage() || !$message
        ) {
            return;  //no autoresp or alerts.
        }

        $dept = $this->getDept();
        $email = $dept->getAutoRespEmail();

        // If enabled...send confirmation to user. ( New Message AutoResponse)
        if ($email && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getNewMessageAutorepMsgTemplate())
        ) {
            $msg = $this->replaceVars($msg->asArray(), array(
                'recipient' => $user,
                'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : ''
                    )
            );
            $options = array(
                'inreplyto' => $message->getEmailMessageId(),
                'thread' => $message
            );
            $email->sendAutoReply($user, $msg['subj'], $msg['body'], null, $options);
        }
    }

    function onActivity($vars, $alert = true) {
        global $cfg, $thisstaff;

        //TODO: do some shit

        if (!$alert // Check if alert is enabled
                || !$cfg->alertONNewActivity() || !($dept = $this->getDept()) || !($email = $cfg->getAlertEmail()) || !($tpl = $dept->getTemplate()) || !($msg = $tpl->getNoteAlertMsgTemplate())
        ) {
            return;
        }

        // Alert recipients
        $recipients = array();

        //Last respondent.
        if ($cfg->alertLastRespondentONNewActivity())
            $recipients[] = $this->getLastRespondent();

        // Assigned staff / team
        if ($cfg->alertAssignedONNewActivity()) {
            if (isset($vars['assignee']) && $vars['assignee'] instanceof Staff)
                $recipients[] = $vars['assignee'];
            elseif ($this->isOpen() && ($assignee = $this->getStaff()))
                $recipients[] = $assignee;

            if ($team = $this->getTeam())
                $recipients = array_merge($recipients, $team->getMembers());
        }

        // Dept manager
        if ($cfg->alertDeptManagerONNewActivity() && $dept && $dept->getManagerId())
            $recipients[] = $dept->getManager();

        $options = array();
        $staffId = $thisstaff ? $thisstaff->getId() : 0;
        if ($vars['threadentry'] && $vars['threadentry'] instanceof ThreadEntry) {
            $options = array(
                'inreplyto' => $vars['threadentry']->getEmailMessageId(),
                'references' => $vars['threadentry']->getEmailReferences(),
                'thread' => $vars['threadentry']);

            // Activity details
            if (!$vars['comments'])
                $vars['comments'] = $vars['threadentry'];

            // Staff doing the activity
            $staffId = $vars['threadentry']->getStaffId() ? : $staffId;
        }

        $msg = $this->replaceVars($msg->asArray(), array(
            'note' => $vars['threadentry'], // For compatibility
            'activity' => $vars['activity'],
            'comments' => $vars['comments']));

        $isClosed = $this->isClosed();
        $sentlist = array();
        foreach ($recipients as $k => $staff) {
            if (!is_object($staff)
                    // Don't bother vacationing staff.
                    || !$staff->isAvailable()
                    // No need to alert the poster!
                    || $staffId == $staff->getId()
                    // No duplicates.
                    || isset($sentlist[$staff->getEmail()])
                    // Make sure staff has access to ticket
                    || ($isClosed && !$this->checkStaffPerm($staff))
            ) {
                continue;
            }
            $alert = $this->replaceVars($msg, array('recipient' => $staff));
            $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
            $sentlist[$staff->getEmail()] = 1;
        }
    }

    function onAssign($assignee, $comments, $alert = true) {
        global $cfg, $thisstaff;

        if ($this->isClosed())
            $this->reopen(); //Assigned tickets must be open - otherwise why assign?
        // Assignee must be an object of type Staff or Team
        if (!$assignee || !is_object($assignee))
            return false;

        $user_comments = (bool) $comments;
        $comments = $comments ? : _S('Ticket assignment');
        $assigner = $thisstaff ? : _S('SYSTEM (Auto Assignment)');

        //Log an internal note - no alerts on the internal note.
        if ($user_comments) {
            $note = $this->logNote(
                    sprintf(_S('Ticket Assigned to %s'), $assignee->getName()), $comments, $assigner, false);
        }

        // See if we need to send alerts
        if (!$alert || !$cfg->alertONAssignment())
            return true; //No alerts!

        $dept = $this->getDept();
        if (!$dept || !($tpl = $dept->getTemplate()) || !($email = $dept->getAlertEmail())
        ) {
            return true;
        }

        // Recipients
        $recipients = array();
        if ($assignee instanceof Staff) {
            if ($cfg->alertStaffONAssignment())
                $recipients[] = $assignee;
        } elseif (($assignee instanceof Team) && $assignee->alertsEnabled()) {
            if ($cfg->alertTeamMembersONAssignment() && ($members = $assignee->getMembers()))
                $recipients = array_merge($recipients, $members);
            elseif ($cfg->alertTeamLeadONAssignment() && ($lead = $assignee->getTeamLead()))
                $recipients[] = $lead;
        }

        // Get the message template
        if ($recipients && ($msg = $tpl->getAssignedAlertMsgTemplate())
        ) {
            $msg = $this->replaceVars($msg->asArray(), array('comments' => $comments,
                'assignee' => $assignee,
                'assigner' => $assigner
                    )
            );
            // Send the alerts.
            $sentlist = array();
            $options = $note instanceof ThreadEntry ? array(
                'inreplyto' => $note->getEmailMessageId(),
                'references' => $note->getEmailReferences(),
                'thread' => $note) : array();
            foreach ($recipients as $k => $staff) {
                if (!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)
                ) {
                    continue;
                }
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }
        return true;
    }

    function onOverdue($whine = true, $comments = "") {
        global $cfg;

        if ($whine && ($sla = $this->getSLA()) && !$sla->alertOnOverdue())
            $whine = false;

        // Check if we need to send alerts.
        if (!$whine || !$cfg->alertONOverdueTicket() || !($dept = $this->getDept())
        ) {
            return true;
        }
        // Get the message template
        if (($tpl = $dept->getTemplate()) && ($msg = $tpl->getOverdueAlertMsgTemplate()) && ($email = $dept->getAlertEmail())
        ) {
            $msg = $this->replaceVars($msg->asArray(), array('comments' => $comments)
            );
            // Recipients
            $recipients = array();
            // Assigned staff or team... if any
            if ($this->isAssigned() && $cfg->alertAssignedONOverdueTicket()) {
                if ($this->getStaffId()) {
                    $recipients[] = $this->getStaff();
                } elseif ($this->getTeamId() && ($team = $this->getTeam()) && ($members = $team->getMembers())
                ) {
                    $recipients = array_merge($recipients, $members);
                }
            } elseif ($cfg->alertDeptMembersONOverdueTicket() && !$this->isAssigned()) {
                // Only alerts dept members if the ticket is NOT assigned.
                if ($members = $dept->getMembersForAlerts())
                    $recipients = array_merge($recipients, $members);
            }
            // Always alert dept manager??
            if ($cfg->alertDeptManagerONOverdueTicket() && $dept && ($manager = $dept->getManager())
            ) {
                $recipients[] = $manager;
            }
            $sentlist = array();
            foreach ($recipients as $k => $staff) {
                if (!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)
                ) {
                    continue;
                }
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null);
                $sentlist[] = $staff->getEmail();
            }
        }
        return true;
    }

    // TemplateVariable interface
    function asVar() {
        return $this->getNumber();
    }

    function getVar($tag) {
        global $cfg;

        switch (mb_strtolower($tag)) {
            case 'phone':
            case 'phone_number':
                return $this->getPhoneNumber();
                break;
            case 'auth_token':
                return $this->getOldAuthToken();
                break;
            case 'client_link':
                return sprintf('%s/view.php?t=%s', $cfg->getBaseUrl(), $this->getNumber());
                break;
            case 'staff_link':
                return sprintf('%s/scp/tickets.php?id=%d', $cfg->getBaseUrl(), $this->getId());
                break;
            case 'create_date':
                return new FormattedDate($this->getCreateDate());
                break;
            case 'due_date':
                if ($due = $this->getEstDueDate())
                    return new FormattedDate($due);
                break;
            case 'close_date':
                if ($this->isClosed())
                    return new FormattedDate($this->getCloseDate());
                break;
            case 'last_update':
                return new FormattedDate($this->last_update);
            case 'user':
                return $this->getOwner();
            case 'entity_type':
                return ucfirst($this->getEntityType());
                break;
            case 'entity_id':
                return $this->getEntities($this->getEntityType());
                break;
	     case 'subject':
                return $this->getOdataField(array('odata__subject'))['odata__subject'];
            default:
                if (isset($this->_answers[$tag]))
                // The answer object is retrieved here which will
                // automatically invoke the toString() method when the
                // answer is coerced into text
                    return $this->_answers[$tag];
        }
    }

    static function getVarScope() {
        $base = array(
            'assigned' => __('Assigned agent and/or team'),
            'close_date' => array(
                'class' => 'FormattedDate', 'desc' => __('Date Closed'),
            ),
            'create_date' => array(
                'class' => 'FormattedDate', 'desc' => __('Date created'),
            ),
            'dept' => array(
                'class' => 'Dept', 'desc' => __('Department'),
            ),
            'due_date' => array(
                'class' => 'FormattedDate', 'desc' => __('Due Date'),
            ),
            'email' => __('Default email address of ticket owner'),
            'name' => array(
                'class' => 'PersonsName', 'desc' => __('Name of ticket owner'),
            ),
            'number' => __('Ticket number'),
            'phone' => __('Phone number of ticket owner'),
            'priority' => array(
                'class' => 'Priority', 'desc' => __('Priority'),
            ),
            'recipients' => array(
                'class' => 'UserList', 'desc' => __('List of all recipient names'),
            ),
            'source' => __('Source'),
            'status' => array(
                'class' => 'TicketStatus', 'desc' => __('Status'),
            ),
            'staff' => array(
                'class' => 'Staff', 'desc' => __('Assigned/closing agent'),
            ),
            'subject' => 'Subject',
            'team' => array(
                'class' => 'Team', 'desc' => __('Assigned/closing team'),
            ),
            'thread' => array(
                'class' => 'TicketThread', 'desc' => __('Ticket Thread'),
            ),
            'topic' => array(
                'class' => 'Topic', 'desc' => __('Help topic'),
            ),
            // XXX: Isn't lastreponse and lastmessage more useful
            'last_update' => array(
                'class' => 'FormattedDate', 'desc' => __('Time of last update'),
            ),
            'user' => array(
                'class' => 'User', 'desc' => __('Ticket owner'),
            ),
        );

        $extra = VariableReplacer::compileFormScope(TicketForm::getInstance());
        return $base + $extra;
    }

    /*  //Replace base variables.
      function replaceVars($input, $vars = array()) {
      global $ost; $input=array();
      file_put_contents('filename.txt',print_r($input,true));
      $orderId=$this->getOrder();
      $returnId=TicketOData::objects()->filter(array("order_id"=>$orderId))->values('retrn_id')->all()[0]['retrn_id'];
      $vars = array_merge($vars, array('ticket' => $this));
      file_put_contents('filename6.txt',print_r($input,true));
      foreach($input as $Key=>$value){file_put_contents('filename2.txt',$key);  file_put_contents('filename3.txt',$value);
      $resp= $ost->replaceTemplateVariables($value, $vars);
      $cann[$key]= Canned::getOrderInfoForTicket($returnId,$orderId,$value);
      }
      file_put_contents('filename1.txt',print_r($resp,true));
      return $cann;
      }
     */

    function replaceVars($input, $vars = array()) {
        global $ost;
        $ticketId = $this->getId();
        if (empty($vars['entityType'])) {
            $entityType = $this->getEntityType();
        } else {
            $entityType = $vars['entityType'];
        }
        if (empty($vars['entityId'])) {
            $commaSeperatedEntityIds = $this->getEntities($entityType);
            $entityIDsInWhichTicketHasBeenMade = explode(',', $commaSeperatedEntityIds);
        } else {
            $commaSeperatedEntityIds = $vars['entityId'];
            $entityIDsInWhichTicketHasBeenMade = explode(',', $vars['entityId']);
        }
        if (count($entityIDsInWhichTicketHasBeenMade) == 1) {
            switch ($entityType) {
                case 'order':
                    $orderDetailsObject = OrderDetails::objects()->filter(array($entityType . '_id' => $entityIDsInWhichTicketHasBeenMade[0]))->first();
                    break;
            }
        }

        $vars = array_merge($vars, array('ticket' => $this, 'entity_id' => $commaSeperatedEntityIds,
            'entity_type' => $entityType, 'order' => $orderDetailsObject)
        );
        $input = $ost->replaceTemplateVariables($input, $vars);
        //     $input['subj'] = $canned->getOrderInfoForTicket($returnId, $orderId, $input['subj']);
        //   $input['body'] = $canned->getOrderInfoForTicket($returnId, $orderId, $input['body']);
        return $input;
    }

    function markUnAnswered() {
        return (!$this->isAnswered() || $this->setAnsweredState(0));
    }

    function markAnswered() {
        return ($this->isAnswered() || $this->setAnsweredState(1));
    }

    function markOverdue($whine = true) {
        global $cfg;

        if ($this->isOverdue())
            return true;

        $this->isoverdue = 1;
        if (!$this->save())
            return false;

        $this->logEvent('overdue');
        $this->onOverdue($whine);

        return true;
    }

    function clearOverdue($save = true) {
        if (!$this->isOverdue())
            return true;

        //NOTE: Previously logged overdue event is NOT annuled.

        $this->isoverdue = 0;

        // clear due date if it's in the past
        if ($this->getDueDate() && Misc::db2gmtime($this->getDueDate()) <= Misc::gmtime())
            $this->duedate = null;

        // Clear SLA if est. due date is in the past
        if ($this->getSLADueDate() && Misc::db2gmtime($this->getSLADueDate()) <= Misc::gmtime())
            $this->sla = null;

        return $save ? $this->save() : true;
    }

    //Dept Tranfer...with alert.. done by staff
    function transfer($deptId, $comments, $alert = true, $priority) {
        global $cfg, $thisstaff;
        if (!$this->checkStaffPerm($thisstaff, TicketModel::PERM_TRANSFER))
            return false;

        $currentDept = $this->getDeptName(); //Current department

        if (!$deptId || !$this->setDeptId($deptId))
            return false;

        // Reopen ticket if closed
        if ($this->isClosed())
            $this->reopen();

        $dept = $this->getDept();

        // Set SLA of the new department
        if (!$this->getSLAId() || $this->getSLA()->isTransient())
            $this->selectSLAId();

        // Make sure the new department allows assignment to the
        // currently assigned agent (if any)
        if ($this->isAssigned() && ($staff = $this->getStaff()) && $dept->assignMembersOnly() && !$dept->isMember($staff)
        ) {
            $this->setStaffId(0);
        }
        $oldpriority = $this->getPriority();
        $this->setPriority($priority);
        $staticFields = getCachedTicketFields();
        $newpriority = $staticFields['priority'][$priority]['name'];
        $data = array("old_priority" => $oldpriority, "new_priority" => $newpriority);
        $data = json_encode($data);
        $this->logEvent('priority', $data);
        /*         * * log the transfer comments as internal note - with alerts disabled - ** */
        $title = sprintf(_S('Ticket transferred from %1$s to %2$s'), $currentDept, $this->getDeptName());

        if ($comments) {
            $note = $this->logNote($title, $comments, $thisstaff, false);
        }
        $comments = $comments ? : $title;

        $this->logEvent('transferred');

        //Send out alerts if enabled AND requested
        if (!$alert || !$cfg->alertONTransfer())
            return true; //no alerts!!

        if (($email = $dept->getAlertEmail()) && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getTransferAlertMsgTemplate())
        ) {
            $msg = $this->replaceVars($msg->asArray(), array('comments' => $comments, 'staff' => $thisstaff));
            // Recipients
            $recipients = array();
            // Assigned staff or team... if any
            if ($this->isAssigned() && $cfg->alertAssignedONTransfer()) {
                if ($this->getStaffId())
                    $recipients[] = $this->getStaff();
                elseif ($this->getTeamId() && ($team = $this->getTeam()) && ($members = $team->getMembers())
                ) {
                    $recipients = array_merge($recipients, $members);
                }
            } elseif ($cfg->alertDeptMembersONTransfer() && !$this->isAssigned()) {
                // Only alerts dept members if the ticket is NOT assigned.
                if ($members = $dept->getMembersForAlerts())
                    $recipients = array_merge($recipients, $members);
            }

            // Always alert dept manager??
            if ($cfg->alertDeptManagerONTransfer() && $dept && ($manager = $dept->getManager())
            ) {
                $recipients[] = $manager;
            }
            $sentlist = array();
            if ($note) {
                $options += array(
                    'inreplyto' => $note->getEmailMessageId(),
                    'references' => $note->getEmailReferences(),
                    'thread' => $note);
            }
            foreach ($recipients as $k => $staff) {
                if (!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)
                ) {
                    continue;
                }
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }
        return true;
    }

    function claim($force = 0) {
        global $thisstaff;

        if (!$thisstaff || !$this->isOpen() || (!$force && $this->isAssigned()))
            return false;

        $dept = $this->getDept();
        if ($dept->assignMembersOnly() && !$dept->isMember($thisstaff))
            return false;

        return $this->assignToStaff($thisstaff->getId(), null, false);
    }

    function assignToStaff($staff, $note, $alert = true) {

        if (!is_object($staff) && !($staff = Staff::lookup($staff)))
            return false;

        if (!$staff->isAvailable() || !$this->setStaffId($staff->getId()))
            return false;

        $this->onAssign($staff, $note, $alert);

        global $thisstaff;
        $data = array();
        if ($thisstaff && $staff->getId() == $thisstaff->getId())
            $data['claim'] = true;
        else
            $data['staff'] = $staff->getId();

        $this->logEvent('assigned', $data);

        return true;
    }

    function assignToTeam($team, $note, $alert = true) {

        if (!is_object($team) && !($team = Team::lookup($team)))
            return false;

        if (!$team->isActive() || !$this->setTeamId($team->getId()))
            return false;

        //Clear - staff if it's a closed ticket
        //  staff_id is overloaded -> assigned to & closed by.
        if ($this->isClosed())
            $this->setStaffId(0);

        $this->onAssign($team, $note, $alert);
        $this->logEvent('assigned', array('team' => $team->getId()));

        return true;
    }

    //Assign ticket to staff or team - overloaded ID.
    function assign($assignId, $note, $alert = true) {
        global $thisstaff;

        $rv = 0;
        $id = preg_replace("/[^0-9]/", "", $assignId);
        if ($assignId[0] == 't') {
            $rv = $this->assignToTeam($id, $note, $alert);
        } elseif ($assignId[0] == 's' || is_numeric($assignId)) {
            $alert = ($alert && $thisstaff && $thisstaff->getId() == $id) ? false : $alert; //No alerts on self assigned tickets!!!
            // We don't care if a team is already assigned to the ticket -
            // staff assignment takes precedence
            $rv = $this->assignToStaff($id, $note, $alert);
        }
        return $rv;
    }

    // Unassign primary assignee
    function unassign() {
        // We can't release what is not assigned buddy!
        if (!$this->isAssigned())
            return true;

        // We can only unassigned OPEN tickets.
        if ($this->isClosed())
            return false;

        // Unassign staff (if any)
        if ($this->getStaffId() && !$this->setStaffId(0))
            return false;

        // Unassign team (if any)
        if ($this->getTeamId() && !$this->setTeamId(0))
            return false;

        return true;
    }

    function release() {
        return $this->unassign();
    }

    //Change ownership
    function changeOwner($user) {
        global $thisstaff;

        if (!$user || ($user->getId() == $this->getOwnerId()) || !($this->checkStaffPerm($thisstaff, TicketModel::PERM_EDIT))
        ) {
            return false;
        }

        $this->user_id = $user->getId();
        if (!$this->save())
            return false;

        unset($this->user);
        $this->collaborators = null;
        $this->recipients = null;

        // Remove the new owner from list of collaborators
        $c = Collaborator::lookup(array(
                    'user_id' => $user->getId(),
                    'thread_id' => $this->getThreadId()
        ));
        if ($c)
            $c->delete();

        $this->logEvent('edited', array('owner' => $user->getId()));

        return true;
    }

    // Insert message from client
    function postMessage($vars, $origin = '', $alerts = true) {
        //Array ( [userId] => 13 [poster] => navneet singh [message] => hello [cannedattachments] => Array ( ) [draft_id] => 338570 )
        global $cfg, $thisstaff;

        if ($origin)
            $vars['origin'] = $origin;
        if (isset($vars['ip']))
            $vars['ip_address'] = $vars['ip'];
        elseif (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $errors = array();
        if (!($message = $this->getThread()->addMessage($vars, $errors)))
            return null;

        $this->setLastMessage($message);

        // Add email recipients as collaborators...
        if ($vars['recipients'] && (strtolower($origin) != 'email' || ($cfg && $cfg->addCollabsViaEmail()))
                //Only add if we have a matched local address
                && $vars['to-email-id']
        ) {
            //New collaborators added by other collaborators are disable --
            // requires staff approval.
            $info = array(
                'isactive' => ($message->getUserId() == $this->getUserId()) ? 1 : 0);
            $collabs = array();
            foreach ($vars['recipients'] as $recipient) {
                // Skip virtual delivered-to addresses
                if (strcasecmp($recipient['source'], 'delivered-to') === 0)
                    continue;

                if (($user = User::fromVars($recipient)))
                    if ($c = $this->addCollaborator($user, $info, $errors, false))
                    // FIXME: This feels very unwise - should be a
                    // string indexed array for future
                        $collabs[$c->user_id] = array(
                            'name' => $c->getName()->getOriginal(),
                            'src' => $recipient['source'],
                        );
            }
            // TODO: Can collaborators add others?
            if ($collabs) {
                $this->logEvent('collab', array('add' => $collabs), $message->user);
            }
        }
        $this->logCount('user');

        if ($message->getUserId()) {
            $userObj = User::lookup($message->getUserId());
            $by = $userObj->getName()->name;
        } elseif ($thisstaff)
            $by = $thisstaff->getName()->name;
        else
            $by = "Customer";

        $this->changeTriage($by);
        $ccgTasks = Task::objects()->filter(array("flags" => 1, "object_id" => $this->getId(), "dept_id" => $cfg->config['deptToAlertWithTriageAgain']['value']? : 0));
        foreach ($ccgTasks as $tasks) {
            $updationData = array("taskId" => $tasks->getId(), "taskDue" => date("Y-m-d H:i:s", time() + $cfg->config['TimeToTriageAlert']['value']), "taskAgent" => "On Customer Reply", "email" => "system.auto@shopclues.com");
            $tasks->updateTask($updationData);
        }

        if (!$alerts)
            return $message; //Our work is done...








            
// Do not auto-respond to bounces and other auto-replies
        $autorespond = isset($vars['flags']) ? !$vars['flags']['bounce'] && !$vars['flags']['auto-reply'] : true;
        if ($autorespond && $message->isAutoReply())
            $autorespond = false;

        $this->onMessage($message, $autorespond); //must be called b4 sending alerts to staff.

        if ($autorespond && $cfg && $cfg->notifyCollabsONNewMessage())
            $this->notifyCollaborators($message, array('signature' => ''));

        $dept = $this->getDept();
        $variables = array(
            'message' => $message,
            'poster' => ($vars['poster'] ? $vars['poster'] : $this->getName())
        );
        $options = array(
            'inreplyto' => $message->getEmailMessageId(),
            'references' => $message->getEmailReferences(),
            'thread' => $message
        );
        // If enabled...send alert to staff (New Message Alert)
        if ($cfg->alertONNewMessage() && ($email = $dept->getAlertEmail()) && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getNewMessageAlertMsgTemplate())
        ) {
            $msg = $this->replaceVars($msg->asArray(), $variables);
            // Build list of recipients and fire the alerts.
            $recipients = array();
            //Last respondent.
            if ($cfg->alertLastRespondentONNewMessage() || $cfg->alertAssignedONNewMessage())
                $recipients[] = $this->getLastRespondent();

            //Assigned staff if any...could be the last respondent
            if ($cfg->alertAssignedONNewMessage() && $this->isAssigned()) {
                if ($staff = $this->getStaff())
                    $recipients[] = $staff;
                elseif ($team = $this->getTeam())
                    $recipients = array_merge($recipients, $team->getMembers());
            }

            // Dept manager
            if ($cfg->alertDeptManagerONNewMessage() && $dept && ($manager = $dept->getManager())
            ) {
                $recipients[] = $manager;
            }

            // Account manager
            if ($cfg->alertAcctManagerONNewMessage() && ($org = $this->getOwner()->getOrganization()) && ($acct_manager = $org->getAccountManager())) {
                if ($acct_manager instanceof Team)
                    $recipients = array_merge($recipients, $acct_manager->getMembers());
                else
                    $recipients[] = $acct_manager;
            }

            $sentlist = array(); //I know it sucks...but..it works.
            foreach ($recipients as $k => $staff) {
                if (!$staff || !$staff->getEmail() || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)
                ) {
                    continue;
                }
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }
        return $message;
    }

    function postCannedReply($canned, $message, $alert = true) {
        global $ost, $cfg;

        if ((!is_object($canned) && !($canned = Canned::lookup($canned))) || !$canned->isEnabled()
        ) {
            return false;
        }
        $files = array();
        foreach ($canned->attachments->getAll() as $file)
            $files[] = $file['id'];

        if ($cfg->isRichTextEnabled())
            $response = new HtmlThreadEntryBody(
                    $this->replaceVars($canned->getHtml()));
        else
            $response = new TextThreadEntryBody(
                    $this->replaceVars($canned->getPlainText()));

        $info = array('msgId' => $message instanceof ThreadEntry ? $message->getId() : 0,
            'poster' => __('SYSTEM (Canned Reply)'),
            'response' => $response,
            'cannedattachments' => $files
        );
        $errors = array();
        if (!($response = $this->postReply($info, $errors, false)))
            return null;

        $this->markUnAnswered();

        if (!$alert)
            return $response;

        $dept = $this->getDept();

        if (($email = $dept->getEmail()) && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getAutoReplyMsgTemplate())
        ) {
            if ($dept && $dept->isPublic())
                $signature = $dept->getSignature();
            else
                $signature = '';

            $msg = $this->replaceVars($msg->asArray(), array(
                'response' => $response,
                'signature' => $signature,
                'recipient' => $this->getOwner(),
                    )
            );

            $attachments = ($cfg->emailAttachments() && $files) ? $response->getAttachments() : array();
            $options = array(
                'inreplyto' => $response->getEmailMessageId(),
                'references' => $response->getEmailReferences(),
                'thread' => $response);
            $email->sendAutoReply($this, $msg['subj'], $msg['body'], $attachments, $options);
        }
        return $response;
    }

    function triageTicket($data) {
        $triageObjArr = Triage::objects()->filter(array("ticket_id" => $data["ticket_id"]))->count();
        if ($triageObjArr < 1) {
            $triageObj = Triage::create();
            $triageObj->ticket_id = $data["ticket_id"];
            $triageObj->priority = $data["priority"];
            $triageObj->communication = $data["communication"];
            $triageObj->triage = $data["triage"];
            $triageObj->call_customer = $data["call_customer"];
            $triageObj->save();
        }
    }

    /* public */

    function postReply($vars, &$errors, $alert = true) {
        global $thisstaff, $cfg;
        if (!$vars['poster'] && $thisstaff)
            $vars['poster'] = $thisstaff;

        if (!$vars['staffId'] && $thisstaff)
            $vars['staffId'] = $thisstaff->getId();

        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        if (!($response = $this->getThread()->addResponse($vars, $errors)))
            return null;

        $assignee = $this->getStaff();
        // Set status - if checked.
        if ($vars['reply_status_id'] && $vars['reply_status_id'] != $this->getStatusId()
        ) {
            $this->setStatus($vars['reply_status_id']);
        }

        // Claim on response bypasses the department assignment restrictions
        if ($thisstaff && $this->isOpen() && !$this->getStaffId() && $cfg->autoClaimTickets()
        ) {
            $data['claim'] = true;
            $data['staff'] = $thisstaff->getId();
            $this->logEvent('assigned', $data);
            $this->setStaffId($thisstaff->getId()); //direct assignment;
        }

        $this->lastrespondent = $response->staff;

        $this->onResponse($response, array('assignee' => $assignee)); //do house cleaning..

        $this->logCount('reply');
        /* email the user??  - if disabled - then bail out */
        if (!$alert)
            return $response;

        $dept = $this->getDept();

        if ($thisstaff && $vars['signature'] == 'mine')
            $signature = $thisstaff->getSignature();
        elseif ($vars['signature'] == 'dept' && $dept && $dept->isPublic())
            $signature = $dept->getSignature();
        else
            $signature = '';

        $variables = array(
            'response' => $response,
            'signature' => $signature,
            'staff' => $thisstaff,
            'poster' => $thisstaff
        );
        $options = array(
            'inreplyto' => $response->getEmailMessageId(),
            'references' => $response->getEmailReferences(),
            'thread' => $response
        );

        $entityType = $this->getEntityType();
        $entityIds = implode(',', $vars['entityReply']);
        $this->setEntities($entityIds, $entityType);
        if (($email = $dept->getEmail()) && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getReplyMsgTemplate())) {
$msg = $this->replaceVars($msg->asArray(), $variables + array('recipient' => $this->getOwner())
 );
            if ($cfg->config['use_mail_queue_post_reply']['value']) {
                $objects = serialize(array('ticket' => $this, 'email' => $email, 'response' => $response));
                $attachments = $cfg->emailAttachments() ? $response->getAttachments() : array();
                $dataToBePassed = array('objects' => $objects, 'msg' => $msg, 'attachments' => $attachments, 'options' => $options);
                addMailToMailingQueue('postReply', $dataToBePassed, 2);
            } else {
                $email->send($this->getOwner(), $msg['subj'], $msg['body'], $attachments, $options);
            }
        }

        if ($vars['emailcollab']) {
            $this->notifyCollaborators($response, array('signature' => $signature)
            );
        }
        return $response;
    }

    //Activity log - saved as internal notes WHEN enabled!!
    function logActivity($title, $note) {
        return $this->logNote($title, $note, 'SYSTEM', false);
    }

    // History log -- used for statistics generation (pretty reports)
    function logEvent($state, $data = null, $user = null, $annul = null) {
        $this->getThread()->getEvents()->log($this, $state, $data, $user, $annul);
    }

    //Insert Internal Notes
    function logNote($title, $note, $poster = 'SYSTEM', $alert = true) {
        // Unless specified otherwise, assume HTML
        if ($note && is_string($note))
            $note = new HtmlThreadEntryBody($note);

        $errors = array();
        return $this->postNote(
                        array(
                    'title' => $title,
                    'note' => $note,
                        ), $errors, $poster, $alert
        );
    }

    function logCount($countOf) {

        switch ($countOf) {
            case 'reply':
                $this->agent_thread_count = $this->agent_thread_count + 1;
                break;
            case 'user':
                $this->user_thread_count = $this->user_thread_count + 1;
                break;
            case 'note':
                $this->agent_note_count = $this->agent_note_count + 1;
                break;
        }
        $this->updateLastResponse($countOf);
        $this->save();
    }

    function postNote($vars, &$errors='', $poster = false, $alert = true) {
        global $cfg, $thisstaff;
        //Who is posting the note - staff or system?
        $vars['staffId'] = 0;
        if ($poster && is_object($poster)) {
            $vars['staffId'] = $poster->getId();
            $vars['poster'] = $poster->getName();
        } elseif ($poster) { //string
            $vars['poster'] = $poster;
        } elseif (!isset($vars['poster'])) {
            $vars['poster'] = 'SYSTEM';
        }
        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        if (!($note = $this->getThread()->addNote($vars, $errors)))
            return null;

        $alert = $alert && (
                isset($vars['flags'])
                // No alerts for bounce and auto-reply emails
                        ? !$vars['flags']['bounce'] && !$vars['flags']['auto-reply'] : true
                );

        $this->logCount('note');
        // Get assigned staff just in case the ticket is closed.
        $assignee = $this->getStaff();

        if ($vars['note_status_id'] && ($status = TicketStatus::lookup($vars['note_status_id']))
        ) {
            $this->setStatus($status);
        }

        $activity = $vars['activity'] ? : _S('New Internal Note');
        $this->onActivity(array(
            'activity' => $activity,
            'threadentry' => $note,
            'assignee' => $assignee
                ), $alert);

        return $note;
    }

    // Threadable interface
    function postThreadEntry($type, $vars, $options = array()) {
        $errors = array();
        switch ($type) {
            case 'M':
                return $this->postMessage($vars, $vars['origin']);
            case 'N':
                return $this->postNote($vars, $errors);
            case 'R':
                return $this->postReply($vars, $errors);
        }
    }

    // Print ticket... export the ticket thread as PDF.
    function pdfExport($psize = 'Letter', $notes = false) {
        global $thisstaff;

        require_once(INCLUDE_DIR . 'class.pdf.php');
        if (!is_string($psize)) {
            if ($_SESSION['PAPER_SIZE'])
                $psize = $_SESSION['PAPER_SIZE'];
            elseif (!$thisstaff || !($psize = $thisstaff->getDefaultPaperSize()))
                $psize = 'Letter';
        }

        $pdf = new Ticket2PDF($this, $psize, $notes);
        $name = 'Ticket-' . $this->getNumber() . '.pdf';
        Http::download($name, 'application/pdf', $pdf->Output($name, 'S'));
        //Remember what the user selected - for autoselect on the next print.
        $_SESSION['PAPER_SIZE'] = $psize;
        exit;
    }

    function delete($comments = '') {
        global $ost, $thisstaff;

        //delete just orphaned ticket thread & associated attachments.
        // Fetch thread prior to removing ticket entry
        $t = $this->getThread();

        if (!parent::delete())
            return false;

        $t->delete();

        foreach (DynamicFormEntry::forTicket($this->getId()) as $form)
            $form->delete();

        $this->deleteDrafts();

        if ($this->cdata)
            $this->cdata->delete();

        // Log delete
        $log = sprintf(__('Ticket #%1$s deleted by %2$s'), $this->getNumber(), $thisstaff ? $thisstaff->getName() : __('SYSTEM')
        );
        if ($comments)
            $log .= sprintf('<hr>%s', $comments);

        $ost->logDebug(
                sprintf(__('Ticket #%s deleted'), $this->getNumber()), $log
        );
        return true;
    }

    function deleteDrafts() {
        Draft::deleteForNamespace('ticket.%.' . $this->getId());
    }

    function save($refetch = false) {
        if ($this->dirty) {
            $this->updated = SqlFunction::NOW();
        }
        return parent::save($this->dirty || $refetch);
    }

    function update($vars, &$errors) {
        global $cfg, $thisstaff;

        if (!$cfg || !($this->checkStaffPerm($thisstaff, TicketModel::PERM_EDIT))
        ) {
            return false;
        }
        $fields = array();
        $fields['topicId'] = array('type' => 'int', 'required' => 1, 'error' => __('Help topic selection is required'));
        $fields['slaId'] = array('type' => 'int', 'required' => 0, 'error' => __('Select a valid SLA'));
        $fields['duedate'] = array('type' => 'date', 'required' => 0, 'error' => __('Invalid date format - must be MM/DD/YY'));

        $fields['user_id'] = array('type' => 'int', 'required' => 0, 'error' => __('Invalid user-id'));

        if (!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] = __('Missing or invalid data - check the errors and try again');

        if ($vars['duedate']) {
            if ($this->isClosed())
                $errors['duedate'] = __('Due date can NOT be set on a closed ticket');
            elseif (!$vars['time'] || strpos($vars['time'], ':') === false)
                $errors['time'] = __('Select a time from the list');
            elseif (strtotime($vars['duedate'] . ' ' . $vars['time']) === false)
                $errors['duedate'] = __('Invalid due date');
            // FIXME: Using time() violates database and user timezone
            elseif (strtotime($vars['duedate'] . ' ' . $vars['time']) <= time())
                $errors['duedate'] = __('Due date must be in the future');
        }

        // Validate dynamic meta-data
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            // Don't validate deleted forms
            if (!in_array($form->getId(), $vars['forms']))
                continue;
            $form->setSource($_POST);
            if (!$form->isValid(function($f) {
                        return $f->isVisibleToStaff() && $f->isEditableToStaff();
                    })) {
                $errors = array_merge($errors, $form->errors());
            }
        }

        if ($errors)
            return false;

        $this->topic_id = $vars['topicId'];
        $this->sla_id = $vars['slaId'];
        $this->source = $vars['source'];
        $this->duedate = $vars['duedate'] ? date('Y-m-d G:i', Misc::dbtime($vars['duedate'] . ' ' . $vars['time'])) : null;

        if ($vars['user_id'])
            $this->user_id = $vars['user_id'];
        if ($vars['duedate'])
        // We are setting new duedate...
            $this->isoverdue = 0;

        $changes = array();
        foreach ($this->dirty as $F => $old) {
            switch ($F) {
                case 'topic_id':
                case 'user_id':
                case 'source':
                case 'duedate':
                case 'sla_id':
                    $changes[$F] = array($old, $this->{$F});
            }
        }

        if (!$this->save())
            return false;

        if ($vars['note'])
            $this->logNote(_S('Ticket Updated'), $vars['note'], $thisstaff);

        // Decide if we need to keep the just selected SLA
        $keepSLA = ($this->getSLAId() != $vars['slaId']);

        // Update dynamic meta-data
        foreach ($forms as $f) {
            if ($C = $f->getChanges())
                $changes['fields'] = ($changes['fields'] ? : array()) + $C;
            // Drop deleted forms
            $idx = array_search($f->getId(), $vars['forms']);
            if ($idx === false) {
                $f->delete();
            } else {
                $f->set('sort', $idx);
                $f->save();
            }
        }

        if ($changes)
            $this->logEvent('edited', $changes);

        // Reselect SLA if transient
        if (!$keepSLA && (!$this->getSLA() || $this->getSLA()->isTransient())
        ) {
            $this->selectSLAId();
        }

        // Update estimated due date in database
        $estimatedDueDate = $this->getEstDueDate();
        $this->updateEstDueDate();

        // Clear overdue flag if duedate or SLA changes and the ticket is no longer overdue.
        if ($this->isOverdue() && (!$estimatedDueDate //Duedate + SLA cleared
                || Misc::db2gmtime($estimatedDueDate) > Misc::gmtime() //New due date in the future.
                )) {
            $this->clearOverdue();
        }

        Signal::send('model.updated', $this);
        return $this->save();
    }

    /* ============== Static functions. Use Ticket::function(params); =============nolint */

    static function getIdByNumber($number, $email = null, $ticket = false) {

        if (!$number)
            return 0;

        $query = static::objects()
                ->filter(array('number' => $number));

        if ($email)
            $query->filter(array('user__emails__address' => $email));


        if (!$ticket) {
            $query = $query->values_flat('ticket_id');
            if ($row = $query->first())
                return $row[0];
        }
        else {
            return $query->first();
        }
    }

    static function lookupByNumber($number, $email = null) {
        return static::getIdByNumber($number, $email, true);
    }

    static function isTicketNumberUnique($number) {
        return 0 === static::objects()
                        ->filter(array('number' => $number))
                        ->count();
    }

    /* Quick staff's tickets stats */

    function getStaffStats($staff) {
        global $cfg;
        $edDepartmentId = $cfg->getEDDepartmentId();
        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;

        if (!$stats) {


            $where2 = '';

            if (($teams = $staff->getTeams()))
                $where[] = ' ( ticket.team_id IN(' . implode(',', db_input(array_filter($teams)))
                        . ') AND status.state="open")';

            $depts = $staff->getDepts();
            foreach ($depts as $dep_id) {
                $role = $staff->getRole($dep_id);
                if ($role->hasPerm(TicketModel::FULL_DEPT))
                    $where[] = 'ticket.dept_id = "' . $dep_id . '" ';
            }
            if (!$cfg || !($cfg->showAssignedTickets() || $staff->showAssignedTickets()))
                $where2 = ' AND (ticket.staff_id=0 OR ticket.team_id=0)';
            $where = implode(' or ', $where);

            if ($where)
                $where = 'AND ( ' . $where . ' ) ';
            if ($staff->checkIfLoginUserOfPrimaryCCGDepartment()) {
                $departmentIdsNotToBeShownOnCCG = $cfg->getDepartmentsNotToShowOnCCg();
                if($staff->managerCheck()) {
		    $staffWhereCond = ' And ticket.dept_id not in (' . $departmentIdsNotToBeShownOnCCG . ') ';
		    $where = '';
		}
                else $staffWhereCond = ' And (ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '")' . ' )';
            } else {
                $staffPrimaryDepartmentId = $staff->getDeptId();
                if($staff->managerCheck()) {
		    $staffWhereCond = ' And ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '")';
		    $where = '';
		}
                else $staffWhereCond = ' And ticket.dept_id in (' . $staffPrimaryDepartmentId . ') and (ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '")' . ' ) ';
            }
            $sql = 'SELECT status.name, count( ticket.ticket_id ) AS tickets '
                    . 'FROM ' . TICKET_TABLE . ' ticket '
                    . 'INNER JOIN ' . TICKET_STATUS_TABLE . ' status
                    ON (ticket.status_id=status.id)'
                    . 'WHERE 1 ' . ($where3 ? $where3 : $where) . $staffWhereCond
                    . 'GROUP BY  status.name ';//_pr($sql); die;
            $res = db_query($sql);
            $stats = array();
            while ($row = db_fetch_row($res)) {
                $stats[$row[0]] = $row[1];
            }
        }
        return $stats;
    }

    function isTicketEDDepartmentTicket() {
        global $cfg;
        $edDepartmentId = $cfg->getEDDepartmentID();
        $ticketDepartmentId = $this->getDeptId();
        if ($edDepartmentId == $ticketDepartmentId) {
            return 1;
        }
        return 0;
    }

    function getTicketChannelForEdDepartment() {
        $triageObj = Triage::lookup($this->getId());
        $triagePriority = $triageObj->priority;
        $triageFields = getCachedTriageFields();
        if ($this->isTicketEDDepartmentTicket()) {
            $ticketChannel = $triageFields["Priority Flags"][$triagePriority];
            return $ticketChannel;
        }
        return '';
    }

    /* Quick staff's tickets stats */

    function getStaffTriageStats($staff) {
        global $cfg;

        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;

        if ($cfg && ($cfg->config['filterOnAdvance']['value']) && $_SESSION['advsearch']) {
            $where3 = ' AND ' . getAdvanceSearchQuery();
            $where3 = str_replace('triage.triage', "triageTable.triage", $where3);
        }

        if (!$triagedStats) {
            //$where = array('(ticket.staff_id=' . db_input($staff->getId()) . ')');
            // $where = array('(ticket.staff_id IN ("' . implode('","',$staff->getAgents()) . '"))');
            $where2 = '';
            //if (0 && ($teams = $staff->getTeams()))
            //  $where[] = ' ( ticket.team_id IN(' . implode(',', db_input(array_filter($teams)))
            //        . ') AND status.state="open")';
            /*
              $depts = $staff->getDepts();
              foreach($depts as $dep_id){
              $role  = $staff->getRole($dep_id);
              if($role->hasPerm(TicketModel::FULL_DEPT))
              $where[] = 'ticket.dept_id = "'.$dep_id.'" ';
              }
             * 
             */
            if (!$cfg || !($cfg->showAssignedTickets() || $staff->showAssignedTickets()))
                $where2 = ' AND (ticket.staff_id=0 OR ticket.team_id=0)';
            $where = implode(' OR ', $where);
            if ($where)
                $where = 'AND ( ' . $where . ' ) ';

            $triagedData[""] = "Not Triage";
            $TriageStatusObj = DynamicList::lookup(array("name" => "Triage Status"));
            if ($TriageStatusObj && $TriageStatusObj->getId()) {
                $TriageStatusListObj = DynamicListItem::objects()->filter(array('list_id' => $TriageStatusObj->getId()))->order_by('sort');
                foreach ($TriageStatusListObj as $st) {
                    $triagedData[$st->extra] = $st->value;
                }
            }
            $sql = 'SELECT case when triage is null then 1 else triage end as triage,count(*)'
                    . ',sum((created<NOW() - INTERVAL ' . $cfg->config['TimeToTriage']['value'] . ' SECOND AND ((triage!=2 and triage not in (3,4,5)) or triage is null)) OR (triage_again<NOW() - INTERVAL ' . $cfg->config['TimeToTriage']['value'] . ' SECOND AND triage in (3,4,5))) AS pending ,'
                    . 'sum((((created>(NOW() - INTERVAL ' . ($cfg->config['TimeToTriage']['value']) . ' SECOND) AND created<(NOW() - INTERVAL ' . ($cfg->config['TimeToTriageAlert']['value']) . ' SECOND))) AND (triage=1 or triage is null)) OR (((triage_again>(NOW() - INTERVAL ' . $cfg->config['TimeToTriage']['value'] . ' SECOND) AND triage_again<(NOW() - INTERVAL ' . ($cfg->config['TimeToTriageAlert']['value']) . ' SECOND))) AND triage in (3,4,5))) AS alert '
                    . 'FROM ' . TICKET_TABLE . ' ticket '
                    . 'LEFT JOIN ' . TRIAGE_TABLE . ' triageTable
                    ON (ticket.ticket_id=triageTable.ticket_id) ';
            if ($where3)
                $sql .= ' LEFT JOIN ' . TICKET_ODATA_TABLE . ' odata ON ticket.ticket_id=odata.ticket_id ';
            $sql .= ' WHERE 1 ';
            if ($staff->checkIfLoginUserOfPrimaryCCGDepartment()) {
                $departmentIds = $cfg->getDepartmentsNotToShowOnCCg();
                $departmentCheckClause = " And ticket.dept_id not in ($departmentIds) ";
            } else {
                $staffPrimaryDepartmentId = $staff->getDeptId();
                $departmentCheckClause = " And ticket.dept_id in ($staffPrimaryDepartmentId) ";
            }
            $sql.=$departmentCheckClause;
            if ($where3) {
                $sql .= $where3 . "GROUP BY triageTable.triage";
            } else {
                $staticFields = getCachedTicketFields();
                if (count($staticFields["state"]["open"]) > 0)
                    $sql .= " AND ticket.status_id IN ('" . implode("','", $staticFields["state"]["open"]) . "')";
                $sql .= $where . $where2 . "GROUP BY triageTable.triage";
            }
            $res = db_query($sql);
            $stats = array();
            while ($row = db_fetch_row($res)) {
                $stats[$triagedData[$row[0]]]["OSLA"] += $row[2];
                $stats[$triagedData[$row[0]]]["OSLA Alert"] += $row[3];
                $stats[$triagedData[$row[0]]]["Within SLA"] += ($row[1] - $row[2] - $row[3]);
            }

            foreach ($triagedData as $k => $v) {

                if ($stats[$v])
                    $triagedStats[$v]["stat"] = $stats[$v];
                else {
                    $triagedStats[$v]["stat"] = array("OSLA" => 0, "OSLA Alert" => 0, "Within SLA" => 0);
                }
                $triagedStats[$v]["total"] = array_sum($triagedStats[$v]["stat"]);
                $triagedStats[$v]["id"] = $k;
                if ($k == 2) {// 2 for Triage
                    $triagedStats[$v]['stat'] = 0;
                }
            }
        }
        return $triagedStats;
    }

    function getUnassignedTicketStats($staff) {

        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;
        global $cfg;
        $staffPrimaryDepartmentId = $staff->getDeptId();
        $edDeptId = $cfg->getEDDepartmentId();
        $departmentIds = $cfg->getDepartmentsNotToShowOnCCg();
        $departmentIDArray = explode(',', $departmentIds);
        if ($staff->checkIfLoginUserOfPrimaryCCGDepartment()) {
            $withinSlaFilterCondition = array("status__state" => "open", "staff_id" => 0, "staff_id" => 0, 'est_duedate__gt' => SqlFunction::NOW(), 'dept_id__notin' => $departmentIDArray);
            $OslaFilterCondition = array("status__state" => "open", "staff_id" => 0, "staff_id" => 0, 'est_duedate__lt' => SqlFunction::NOW(), 'dept_id__notin' => $departmentIDArray);
        } else {
            $withinSlaFilterCondition = array("status__state" => "open", "staff_id" => 0, "staff_id" => 0, 'est_duedate__gt' => SqlFunction::NOW(), 'dept_id__in' => array($staffPrimaryDepartmentId));
            $OslaFilterCondition = array("status__state" => "open", "staff_id" => 0, "staff_id" => 0, 'est_duedate__lt' => SqlFunction::NOW(), 'dept_id__in' => array($staffPrimaryDepartmentId));
        }
        if (!$stats) {
            $withSLA = Ticket::objects()->filter($withinSlaFilterCondition);
            $OSLA = Ticket::objects()->filter($OslaFilterCondition);
            foreach ($_SESSION['searchORM'] as $search) {
                $withSLA->filter($search);
                $OSLA->filter($search);
            }
            $withSLA = $withSLA->count();
            $OSLA = $OSLA->count();
            $stats = array("All" => array("count" => ($withSLA + $OSLA), "unassigned" => "all"), "Within SLA" => array("count" => $withSLA, "unassigned" => "sla"), "OSLA" => array("count" => $OSLA, "unassigned" => "osla"));
        }
        return $stats;
    }

    function getEDStaffTicketStats($staff) {
        global $cfg;

        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;
        $where = array('(ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '"))');
        $where2 = '';

        if (($teams = $staff->getTeams()))
            $where[] = ' ( ticket.team_id IN(' . implode(',', db_input(array_filter($teams)))
                    . '))';
        if (!$cfg || !($cfg->showAssignedTickets() || $staff->showAssignedTickets()))
            $where2 = ' AND (ticket.staff_id=0 OR ticket.team_id=0)';

        if ($cfg && ($cfg->config['filterOnAdvance']['value']) && $_SESSION['advsearch'])
            $where3 = ' AND ' . getAdvanceSearchQuery();

        $where = implode(' OR ', $where);
        $edDepartmentId = $cfg->getEDDepartmentId();
        if ($where)
            $where = 'AND ( ' . $where . ' ) ';
        $sql = "SELECT 
                SUM(
                  CASE
                    WHEN ticket.`closed` IS NULL 
                    THEN 
                    CASE
                      WHEN ticket.`est_duedate` >= NOW() 
                      THEN 1 
                      ELSE 0 
                    END 
                    ELSE 0 
                  END
                ) AS ticketsOpenWithinSla,
                SUM(
                  CASE
                    WHEN ticket.`closed` IS NULL 
                    THEN 
                    CASE
                      WHEN ticket.`est_duedate` < NOW() 
                      THEN 1 
                      ELSE 0 
                    END 
                    ELSE 0 
                  END
                ) AS ticketsOpenWithinOSla ,
                triage.`priority` 
              FROM
                mst_ticket ticket 
                JOIN `mst_triage_data` triage 
                  ON ticket.`ticket_id` = triage.`ticket_id` 
                LEFT JOIN `mst_ticket__data` odata 
                  ON odata.`ticket_id` = ticket.`ticket_id` 
              WHERE ticket.`dept_id` = $edDepartmentId 
             AND ticket.`isanswered`=0";
        $staticFields = getCachedTicketFields();
        $triageFields = getCachedTriageFields();
        if ($where3) {
            $sql .= $where3;
        } else {
            if (count($staticFields["state"]["open"]) > 0)
                $sql .= " AND ticket.status_id IN ('" . implode("','", $staticFields["state"]["open"]) . "')";
            $sql .= $where . $where2;
        }
        $sql.=" GROUP BY triage.`priority` ";

        $ticketWiseSourceDetails = [];
        $rs = db_query($sql);
        $rowIndex = 0;
        while ($row = db_fetch_array($rs)) {
            $ticketWiseSourceDetails[] = $row;
            $ticketWiseSourceDetails[$rowIndex]['channel'] = $triageFields['Priority Flags'][$row['priority']];
            $rowIndex++;
        }
        return $ticketWiseSourceDetails;
    }

    /* Quick staff's tickets stats */

    function getStaffTicketUpdateStats($staff) {
        global $cfg;

        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;

        $edDepartmentID = $cfg->getEDDepartmentId();
        if (!$stats) {

            $staticTriageData = getCachedTriageFields();
            $listArr["communication"] = array_flip($staticTriageData["Communication Flags"]);
            $listArr["priority"] = array_flip($staticTriageData["Priority Flags"]);

            foreach ($listArr["priority"] as $priority => $pval) {
                foreach ($listArr["communication"] as $communication => $cval) {
                    $treeList[$priority][$communication] = 1;
                }
            }

            $where = array('(ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '"))');
            /*if(!$staff->managerCheck()) {
                $where = array('(ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '"))');
            }*/
            $where2 = '';

            if (($teams = $staff->getTeams()))
                $where[] = ' ( ticket.team_id IN(' . implode(',', db_input(array_filter($teams)))
                        . '))';
            /*
              $depts = $staff->getDepts();
              foreach($depts as $dep_id){
              $role  = $staff->getRole($dep_id);
              if($role->hasPerm(TicketModel::FULL_DEPT))
              $where[] = 'ticket.dept_id = "'.$dep_id.'" ';
              }
             */


            if (!$cfg || !($cfg->showAssignedTickets() || $staff->showAssignedTickets()))
                $where2 = ' AND (ticket.staff_id=0 OR ticket.team_id=0)';

            if ($cfg && ($cfg->config['filterOnAdvance']['value']) && $_SESSION['advsearch'])
                $where3 = ' AND ' . getAdvanceSearchQuery();

            $where = implode(' OR ', $where);
            if ($where)
                $where = 'AND ( ' . $where . ' ) ';
            $mapping = json_decode($cfg->config['customerUpdateFilters']['value'], true);

            foreach ($mapping as $map) {
                $time = explode("_", $map["range"]);
                $gt = 'now() + interval [X] hour';
                if ($time[0] <= 0)
                    $gt = str_replace("+", "-", $gt);
                $gt = str_replace("[X]", abs($time[0]), $gt);

                $lt = 'now() + interval [X] hour';
                if ($time[1] <= 0)
                    $lt = str_replace("+", "-", $lt);

                $lt = str_replace("[X]", abs($time[1]), $lt);

                $sqlCondition[] = 'sum(est_duedate > (' . $gt . ') and  est_duedate < (' . $lt . ')) `' . $map["label"] . '`';
                foreach ($treeList as $priority => $pData) {
                    $condition = " and triage.priority=" . $listArr["priority"][$priority] . " ";
                    $conditionName = "-priority[" . $priority . "]-";
                    $sqlCondition[] = 'sum(est_duedate > (' . $gt . ') and  est_duedate < (' . $lt . ') ' . $condition . ') `' . $map["label"] . $conditionName . '`';
                    foreach ($pData as $comm => $cData) {
                        $comm_condition = $condition . " and triage.communication=" . $listArr["communication"][$comm] . " ";
                        $comm_conditionName = $conditionName . "-communication[" . $comm . "]-";
                        $sqlCondition[] = 'sum(est_duedate > (' . $gt . ') and  est_duedate < (' . $lt . ') ' . $comm_condition . ') `' . $map["label"] . $comm_conditionName . '`';
                    }
                    $comm_condition = $condition . " and triage.call_customer=2 ";
                    $comm_conditionName = $conditionName . "-communication[call]-";
                    $sqlCondition[] = 'sum(est_duedate > (' . $gt . ') and  est_duedate < (' . $lt . ') ' . $comm_condition . ') `' . $map["label"] . $comm_conditionName . '`';
                }
            }
            $sql = 'SELECT ';
            $sql .= implode(", ", $sqlCondition);
            $sql .= 'FROM ' . TICKET_TABLE . ' ticket '
                    . ' LEFT JOIN ' . TRIAGE_TABLE . ' triage'
                    . ' on ticket.ticket_id=triage.ticket_id ';
            if ($where3)
                $sql .= ' LEFT JOIN ' . TICKET_ODATA_TABLE . ' odata ON ticket.ticket_id=odata.ticket_id ';
            $sql .= ' WHERE 1 ';
            $staticFields = getCachedTicketFields();
            if ($where3) {
                $sql .= $where3;
            } else {
                if (count($staticFields["state"]["open"]) > 0)
                    $sql .= " AND ticket.status_id IN ('" . implode("','", $staticFields["state"]["open"]) . "') ";
                $sql .= $where . $where2;
            }
            $res = db_query($sql);
            $stats = array();
            while ($row = db_fetch_array($res)) {
                foreach ($mapping as $key => $responseRange) {
                    $stats[$responseRange["label"]] = array("id" => $key, "count" => ($row[$responseRange["label"]] ? $row[$responseRange["label"]] : 0));
                }
                foreach ($row as $key => $val) {
                    /* $keys = array_filter(explode("-",$key));
                      if(count($keys)>1){
                      print_r($keys);die;} */
                    $other[trim(str_replace("--", "-", $key), "-")] = $val;
                }
                //MAP in tickets.inc.php
            }
            //echo "<pre>";print_r($other);echo "</pre>";die;
            $statistics["main"] = $stats;
            $statistics["priority"] = $other;
        }
        // print_r($statistics);die;
        return $statistics;
    }

    /* Quick client's tickets stats
      @email - valid email.
     */

    function getUserStats($user) {
        if (!$user || !($user instanceof EndUser))
            return null;

        $sql = 'SELECT count(open.ticket_id) as open, count(closed.ticket_id) as closed '
                . ' FROM ' . TICKET_TABLE . ' ticket '
                . ' LEFT JOIN ' . TICKET_TABLE . ' open
                ON (open.ticket_id=ticket.ticket_id AND open.status=\'open\') '
                . ' LEFT JOIN ' . TICKET_TABLE . ' closed
                ON (closed.ticket_id=ticket.ticket_id AND closed.status=\'closed\')'
                . ' WHERE ticket.user_id = ' . db_input($user->getId());

        return db_fetch_array(db_query($sql));
    }

    protected function filterTicketData($origin, $vars, $forms, $user = false) {
        global $cfg;

        // Unset all the filter data field data in case things change
        // during recursive calls
        foreach ($vars as $k => $v)
            if (strpos($k, 'field.') === 0)
                unset($vars[$k]);

        foreach ($forms as $F) {
            if ($F) {
                $vars += $F->getFilterData();
            }
        }

        if (!$user) {
            $interesting = array('name', 'email');
            $user_form = UserForm::getUserForm()->getForm($vars);
            // Add all the user-entered info for filtering
            foreach ($interesting as $F) {
                $field = $user_form->getField($F);
                $vars[$F] = $field->toString($field->getClean());
            }
            // Attempt to lookup the user and associated data
            $user = User::lookupByEmail($vars['email']);
        }

        // Add in user and organization data for filtering
        if ($user) {
            $vars += $user->getFilterData();
            $vars['email'] = $user->getEmail();
            $vars['name'] = $user->getName()->getOriginal();
            if ($org = $user->getOrganization()) {
                $vars += $org->getFilterData();
            }
        }
        // Don't include org information based solely on email domain
        // for existing user instances
        else {
            // Unpack all known user info from the request
            foreach ($user_form->getFields() as $f) {
                $vars['field.' . $f->get('id')] = $f->toString($f->getClean());
            }
            // Add in organization data if one exists for this email domain
            list($mailbox, $domain) = explode('@', $vars['email'], 2);
            if ($org = Organization::forDomain($domain)) {
                $vars += $org->getFilterData();
            }
        }

        try {
            // Make sure the email address is not banned
            if (($filter = Banlist::isBanned($vars['email']))) {
                throw new RejectedException($filter, $vars);
            }

            // Init ticket filters...
            $ticket_filter = new TicketFilter($origin, $vars);
            $ticket_filter->apply($vars);
        } catch (FilterDataChanged $ex) {
            // Don't pass user recursively, assume the user has changed
            return self::filterTicketData($origin, $ex->getData(), $forms);
        }
        return $vars;
    }

static function cloneTicket($ticket_id, $msg) {
        global $cfg;
 $ticketData = Ticket::objects()->filter(array("ticket_id" => $ticket_id))->values('status_id', 'staff_id', 'user_id', 'dept_id', 'topic_id', 'odata__order_ids','odata__attachment', 'odata__manifest_ids','odata__product_ids','odata__subject', 'odata__priority', 'user__emails__address', 'user__name', 'triage__communication', 'triage__call_customer', 'triage__priority', 'triage__triage')->all()[0];
	if ($ticketData["status_id"] != 2) {
            return $ticket_id;
        }
	$duplicateData['topicId']=$ticketData["topic_id"];
	$duplicateData['order_ids']=$ticketData["odata__order_ids"];
	$duplicateData['product_ids']=$ticketData["odata__product_ids"];
	$duplicateData['manifest_ids']=$ticketData["odata__manifest_ids"];
	$duplicateTicketNumber=Ticket::checkDupicateTicket($duplicateData);
	if($duplicateTicketNumber==false){
	$msg=serialize($msg);
	$data = array(
            'name' => $ticketData["user__name"], // from name aka User/Client Name
            'email' => $ticketData["user__emails__address"], // from email aka User/Client Email
            'subject' => $ticketData["odata__subject"],
            'message' => $msg,
            'deptId' => $ticketData["dept_id"],
            'issue_id' => 1,
            'topicId' => $ticketData["topic_id"],
            'order_ids' => $ticketData["odata__order_ids"],
            'manifest_ids' => $ticketData["odata__manifest_ids"]
            
        );
        if($ticketData["odata__attachment"]) {
		$data['attachment']=(unserialize($ticketData["odata__attachment"]));
	}
        include_once ROOT_DIR . 'api/class.osTicketAPIClient.php';
        $osticket_api_url = $cfg->config['helpdesk_url']['value'] . '/api/http.php/tickets.json';
        $osticket_api_key = $cfg->config['API_KEY_CLONE']['value']; //'90350BD8ED5FED056DD74583E4621BB4';
	$objTicket = new osTicketAPIClient($osticket_api_key, $osticket_api_url);
        $new_ticket_number = $objTicket->osTicketAPI($data);
	}else{
	 $msg=$msg->body;
   	 $new_ticket_number=$duplicateTicketNumber;
	 $newTicket = Ticket::lookupByNumber($new_ticket_number);
	 $vars['title'] = ''; //Use the initial subject as title of the post.
         $vars['userId'] = $newTicket->getUserId();
	 $vars['message']=$msg;
         $newTicket->postMessage($vars,'email',false);
	}
        $newTicket = Ticket::lookupByNumber($new_ticket_number);
        $oldTicket = Ticket::lookup($ticket_id);

        //Triaging New Ticket
        if ($ticketData["triage__triage"]) {
            $triageExistObjCount = Triage::objects()->filter(array("ticket_id" => $newTicket->ticket_id))->count();
            if (!$triageExistObjCount) {
                $triageExistObj = Triage::create();
                $triageExistObj->ticket_id = $newTicket->ticket_id;
                $triageExistObj->communication = $ticketData["triage__communication"];
                $triageExistObj->call_customer = $ticketData["triage__call_customer"];
                $triageExistObj->priority = $ticketData["triage__priority"];
                $triageExistObj->triage = 1;
                $triageExistObj->save();
            }
        }
        $info = json_encode(array("new" => $new_ticket_number, "old" => $oldTicket->number));
	if($duplicateTicketId==false){
     	   $newTicket->logEvent('child', $info);
       	   $oldTicket->logEvent('parent', $info);
	}
        return $newTicket->number;
    }

    /*
     * The mother of all functions...You break it you fix it!
     *
     *  $autorespond and $alertstaff overrides config settings...
     */

    static function create(&$vars, &$errors, $origin, $autorespond = true, $alertstaff = true) {
        global $ost, $cfg, $thisclient, $thisstaff,$APIKey;
 if(@unserialize($vars['message']) !== false){
$vars['message']=unserialize($vars['message']);

}       

// Don't enforce form validation for email
$field_filter = function($type) use ($origin) {
            return function($f) use ($origin, $type) {
                // Ultimately, only offer validation errors for web for
                // non-internal fields. For email, no validation can be
                // performed. For other origins, validate as usual
                switch (strtolower($origin)) {
                    case 'email':
                        return false;
                    case 'staff':
                        // Required 'Contact Information' fields aren't required
                        // when staff open tickets
                        return $f->isVisibleToStaff();
                    case 'web':
                        return $f->isVisibleToUsers();
                    default:
                        return true;
                }
            };
        };
        $reject_ticket = function($message) use (&$errors) {
            global $ost;
            $errors = array(
                'errno' => 403,
                'err' => __('This help desk is for use by authorized users only'));
            $ost->logWarning(_S('Ticket Denied'), $message, false);
            return 0;
        };

        Signal::send('ticket.create.before', null, $vars);

        // Create and verify the dynamic form entry for the new ticket
        $form = TicketForm::getNewInstance();
        $form->setSource($vars);

        // If submitting via email or api, ensure we have a subject and such
        if (!in_array(strtolower($origin), array('web', 'staff'))) {
            foreach ($form->getFields() as $field) {
                $fname = $field->get('name');
                if ($fname && isset($vars[$fname]) && !$field->value)
                    $field->value = $field->parse($vars[$fname]);
            }
        }

        if ($vars['email']) {
            $email = UserEmail::objects()->filter(array("address" => $vars['email']))->values("user_id")->limit(1)->all();
            if ($email[0]["user_id"]) {
                $vars['uid'] = $email[0]["user_id"];
            }
        }
        if ($vars['uid'])
            $user = User::lookup($vars['uid']);

        $id = 0;
        $fields = array();
        switch (strtolower($origin)) {
            case 'web':
                $fields['topicId'] = array('type' => 'int', 'required' => 1, 'error' => __('Select a help topic'));
                break;
            case 'staff':
                $fields['deptId'] = array('type' => 'int', 'required' => 0, 'error' => __('Department selection is required'));
                $fields['topicId'] = array('type' => 'int', 'required' => 1, 'error' => __('Help topic selection is required'));
                $fields['duedate'] = array('type' => 'date', 'required' => 0, 'error' => __('Invalid date format - must be MM/DD/YY'));
            case 'api':
                $fields['source'] = array('type' => 'string', 'required' => 1, 'error' => __('Indicate ticket source'));
                break;
            case 'email':
                $fields['emailId'] = array('type' => 'int', 'required' => 1, 'error' => __('Unknown system email'));
                break;
            default:
                # TODO: Return error message
                $errors['err'] = $errors['origin'] = __('Invalid ticket origin given');
        }

        if (!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] = __('Missing or invalid data - check the errors and try again');

        //Make sure the due date is valid
        if ($vars['duedate']) {
            if (!$vars['time'] || strpos($vars['time'], ':') === false)
                $errors['time'] = __('Select a time from the list');
            elseif (strtotime($vars['duedate'] . ' ' . $vars['time']) === false)
                $errors['duedate'] = __('Invalid due date');
            elseif (strtotime($vars['duedate'] . ' ' . $vars['time']) <= time())
                $errors['duedate'] = __('Due date must be in the future');
        }

        $topic_forms = array();
        if (!$errors) {

            // Handle the forms associate with the help topics. Instanciate the
            // entries, disable and track the requested disabled fields.
            if ($vars['topicId']) {
                if ($__topic = Topic::lookup($vars['topicId'])) {
                    foreach ($__topic->getForms() as $idx => $__F) {
                        $disabled = array();
                        foreach ($__F->getFields() as $field) {
                            if (!$field->isEnabled() && $field->hasFlag(DynamicFormField::FLAG_ENABLED))
                                $disabled[] = $field->get('id');
                        }
                        // Special handling for the ticket form -Â disable fields
                        // requested to be disabled as per the help topic.
                        if ($__F->get('type') == 'T') {
                            foreach ($form->getFields() as $field) {
                                if (false !== array_search($field->get('id'), $disabled))
                                    $field->disable();
                            }
                            $form->sort = $idx;
                            $__F = $form;
                        }
                        else {
                            $__F = $__F->instanciate($idx);
                            $__F->setSource($vars);
                            $topic_forms[] = $__F;
                        }
                        // Track fields currently disabled
                        $__F->extra = JsonDataEncoder::encode(array(
                                    'disable' => $disabled
                        ));
                    }
                }
            }

            try {
                $vars = self::filterTicketData($origin, $vars, array_merge(array($form), $topic_forms), $user);
            } catch (RejectedException $ex) {
                return $reject_ticket(
                        sprintf(_S('Ticket rejected (%s) by filter "%s"'), $ex->vars['email'], $ex->getRejectingFilter()->getName())
                );
            }


            //Make sure the open ticket limit hasn't been reached. (LOOP CONTROL)
            if ($cfg->getMaxOpenTickets() > 0 && strcasecmp($origin, 'staff') && ($_user = TicketUser::lookupByEmail($vars['email'])) && ($openTickets = $_user->getNumOpenTickets()) && ($openTickets >= $cfg->getMaxOpenTickets())) {

                $errors = array('err' => __("You've reached the maximum open tickets allowed."));
                $ost->logWarning(sprintf(_S('Ticket denied - %s'), $vars['email']), sprintf(_S('Max open tickets (%1$d) reached for %2$s'), $cfg->getMaxOpenTickets(), $vars['email']), false);

                return 0;
            }

            // Allow vars to be changed in ticket filter and applied to the user
            // account created or detected
            if (!$user && $vars['email'])
                $user = User::lookupByEmail($vars['email']);

            if (!$user) {
                // Reject emails if not from registered clients (if
                // configured)
                if (strcasecmp($origin, 'email') === 0 && !$cfg->acceptUnregisteredEmail()) {
                    list($mailbox, $domain) = explode('@', $vars['email'], 2);
                    // Users not yet created but linked to an organization
                    // are still acceptable
                    if (!Organization::forDomain($domain)) {
                        return $reject_ticket(
                                sprintf(_S('Ticket rejected (%s) (unregistered client)'), $vars['email']));
                    }
                }

                $user_form = UserForm::getUserForm()->getForm($vars);
                $can_create = !$thisstaff || $thisstaff->hasPerm(User::PERM_CREATE);
                if (!$user_form->isValid($field_filter('user')) || !($user = User::fromVars($user_form->getClean(), $can_create))
                ) {
                    $errors['user'] = $can_create ? __('Incomplete client information') : __('You do not have permission to create users.');
                }
            }
        }

        if (!$form->isValid($field_filter('ticket')))
            $errors += $form->errors();

        if ($vars['topicId']) {
            if ($topic = Topic::lookup($vars['topicId'])) {
                foreach ($topic_forms as $topic_form) {
                    $TF = $topic_form->getForm($vars);
                    if (!$TF->isValid($field_filter('topic')))
                        $errors = array_merge($errors, $TF->errors());
                }
            }
            else {
                $errors['topicId'] = 'Invalid help topic selected';
            }
        }

        // Any error above is fatal.
        if ($errors) {
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D1"];
            $module = $logConfig["module"][$domain]["D1_M1"];
            $error = $logConfig["error"]["E1"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $logConfig["level"]["ERROR"], $error, "title:Ticket Creation Error", "log_location:class.ticket.php", "", "", substr(json_encode($errors) . json_encode($vars), 0, 2000));
            return 0;
        }

        Signal::send('ticket.create.validated', null, $vars);

        # Some things will need to be unpacked back into the scope of this
        # function
        if (isset($vars['autorespond']))
            $autorespond = $vars['autorespond'];

        # Apply filter-specific priority
        if ($vars['priorityId']) {
            $form->setAnswer('priority', null, $vars['priorityId']);
        }
        // If the filter specifies a help topic which has a form associated,
        // and there was previously either no help topic set or the help
        // topic did not have a form, there's no need to add it now as (1)
        // validation is closed, (2) there may be a form already associated
        // and filled out from the original  help topic, and (3) staff
        // members can always add more forms now
        // OK...just do it.
        $statusId = $vars['statusId'];
        $deptId = $vars['deptId']; //pre-selected Dept if any.
        $source = ucfirst($vars['source']);

        // Apply email settings for emailed tickets. Email settings should
        // trump help topic settins if the email has an associated help
        // topic
        if ($vars['emailId'] && ($email = Email::lookup($vars['emailId']))) {
            $deptId = $deptId ? : $email->getDeptId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $email->getPriorityId());
            if ($autorespond)
                $autorespond = $email->autoRespond();
            if (!isset($topic) && ($T = $email->getTopic()) && ($T->isActive())) {
                $topic = $T;
            }
            $email = null;
            $source = 'Email';
        }

        if (!isset($topic)) {
            // This may return NULL, no big deal
            $topic = $cfg->getDefaultTopic();
        }

        // Intenal mapping magic...see if we need to override anything
        if (isset($topic)) {
            $deptId = $deptId ? : $topic->getDeptId();
            $statusId = $statusId ? : $topic->getStatusId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $topic->getPriorityId());
            if ($autorespond)
                $autorespond = $topic->autoRespond();

            //Auto assignment.
            if (!isset($vars['staffId']) && $topic->getStaffId())
                $vars['staffId'] = $topic->getStaffId();
            elseif (!isset($vars['teamId']) && $topic->getTeamId())
                $vars['teamId'] = $topic->getTeamId();

            //set default sla.
            if (isset($vars['slaId']))
                $vars['slaId'] = $vars['slaId'] ? : $cfg->getDefaultSLAId();
            elseif ($topic && $topic->getSLAId())
                $vars['slaId'] = $topic->getSLAId();
        }

        // Auto assignment to organization account manager
        if (($org = $user->getOrganization()) && $org->autoAssignAccountManager() && ($code = $org->getAccountManagerId())) {
            if (!isset($vars['staffId']) && $code[0] == 's')
                $vars['staffId'] = substr($code, 1);
            elseif (!isset($vars['teamId']) && $code[0] == 't')
                $vars['teamId'] = substr($code, 1);
        }

        // Last minute checks
        $priority = $form->getAnswer('priority');
        if (!$priority || !$priority->getIdValue())
            $form->setAnswer('priority', null, $cfg->getDefaultPriorityId());
        $deptId = $deptId ? : $cfg->getDefaultDeptId();
        $statusId = $statusId ? : $cfg->getDefaultTicketStatusId();
        $topicId = isset($topic) ? $topic->getId() : 0;
        $ipaddress = $vars['ip'] ? : $_SERVER['REMOTE_ADDR'];
        $source = $source ? : 'Web';
        $source_extra = $APIKey->getNotes();
        //We are ready son...hold on to the rails.
        $number = 1010101010; //$topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
        $ticket = parent::create(array(
                    'created' => SqlFunction::NOW(),
                    'lastupdate' => SqlFunction::NOW(),
                    'number' => $number,
                    'user' => $user,
                    'dept_id' => $deptId,
                    'topic_id' => $topicId,
                    'ip_address' => $ipaddress,
                    'source' => $source,
                    'source_extra' =>$source_extra,
        ));
        $ticket->mandate_info=$vars['mandateType'];
        if (isset($vars['emailId']) && $vars['emailId'])
            $ticket->email_id = $vars['emailId'];
        if ($vars["source_extra"])
            $ticket->source_extra = $vars["source_extra"];
        //Make sure the origin is staff - avoid firebug hack!
        if ($vars['duedate'] && !strcasecmp($origin, 'staff'))
           $ticket->duedate = date('Y-m-d G:i', Misc::dbtime($vars['duedate'] . ' ' . $vars['time']));
           
        if (!$ticket->save())
            return null;
        //Ticket Number logic correction

        $number = 500000 + $ticket->getId();
        $ticket->number = $number;
        $ticket->save();
        if (!($thread = TicketThread::create($ticket->getId())))
            return null;

        // For ticket Odata updation
//        $newTicket = Ticket::lookup($ticket->getId());
//        $odata["ticket_id"] = $ticket->getId();
//        if ($vars["order_id"])
//            $odata['order_id'] = $vars["order_id"];
//        if ($vars["return_id"])
//            $odata["retrn_id"] = $vars["return_id"];
//        if ($vars["subject"])
//            $odata["subject"] = $vars["subject"];//$newTicket->getSubject();
//        if ($vars["priorityId"])
//            $odata["priority"] = $vars["priorityId"]?:$newTicket->getPriorityId();
//        if ($source)
//            $odata["ticket_channel"] = $source;
//        if (is_array($vars['additional_fields'])) {
//            foreach ($vars['additional_fields'] as $field => $field_data) {
//             //   $odata[$field] = $field_data;
//            }
//        }
//        $ticketOdataObj = TicketOData::create($odata);
//        $ticketOdataObj->save();
        //end
        //navneet


        $newTicket = Ticket::lookup($ticket->getId());
//        $topicData=Topic::objects()->filter(array("topic_id" => $this->getId()))->values($fieldArr)->all();
        $mdata["ticket_id"] = $ticket->getId();

        if ($vars["order_ids"])
            $mdata['order_ids'] = $vars["order_ids"];
        if ($vars["manifest_ids"])
            $mdata['manifest_ids'] = $vars["manifest_ids"];
        if ($vars["product_ids"])
            $mdata['product_ids'] = $vars["product_ids"];
        if ($vars["batch_ids"])
            $mdata['batch_ids'] = $vars["batch_ids"];

        //subject
            $vars["subject"]=$newTicket->getHelpTopic();

        if ($vars["subject"])
            $mdata["subject"] = $vars["subject"];
        
        if ($vars["carrier_id"])
            $mdata["carrier_id"] = $vars["carrier_id"];
        if ($vars["billing_name"])
            $mdata["billing cycle"] = $vars["billing_name"];
        if ($vars["LBH"])
            $mdata["lbh"] = $vars["LBH"];
        if ($vars["merchant_id"])
            $mdata["merchant_id"] = $vars["merchant_id"];
        if ($vars["merchant_type"])
            $mdata["merchant_type"] = $vars["merchant_type"];
        if ($vars["email"])
            $mdata["email"] = $vars["email"];
        if ($vars["phone"])
            $mdata["phone"] = $vars["phone"];
        if ($vars["trackingNo"])
            $mdata["tracking_no"] = $vars["trackingNo"];
        if ($vars["attachment"])
            $mdata["attachment"] = serialize($vars["attachment"]);

	$isBGUser = $ticket->checkPriority($mdata["ticket_id"],$vars['uid']);
        if($isBGUser) {
	    $mdata["priority"] = '3';
	    $ticket->is_bg = '1';
            $ticket->save();
	}

        $ticketdataObj = TicketOData::create($mdata);
        $ticketdataObj->save();
        
	$ticket->insertEntryIntoCommonTable($ticketdataObj);
    
        //end

        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D1"];
        $module = $logConfig["module"][$domain]["D1_M1"];
        /**
          $ticketCreationEmailQueue = json_decode($cfg->config['TicketCreationEmailQueue']['value'],true);
          if(QUEUE_ENABLED  && (($ticketCreationEmailQueue['abtest_email']==1) ||  (in_array($vars['email'],$ticketCreationEmailQueue['abtest_email'])))){

          $dataForQueue = array("ticket_id"=>$ticket->getId(),"message"=>$vars["message"]->body,"autorespond"=>true,"alertstaff"=>false,"vars"=>$vars);
          $exchange = $ticketCreationEmailQueue["exchange"];
          $key =  $ticketCreationEmailQueue["key"];
          $queue =  $ticketCreationEmailQueue["queue"];

          AddToQueue(json_encode($dataForQueue), $exchange, $key);

          $logObj = new Analog_logger();
          $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TICKET CREATION EMAIL QUEUE", "ticker_id:".$ticket->getId(),"", "", "VARS:".substr(json_encode($vars),0,2000));
          $mailSent = 1;
          }
         * 
         */
        /* -------------------- POST CREATE ------------------------ */

        // Save the (common) dynamic form
        // Ensure we have a subject
        $subject = $form->getAnswer('subject');
        if ($subject && !$subject->getValue()) {
            if ($topic) {
                $form->setAnswer('subject', $topic->getFullName());
            }
        }

        $form->setTicketId($ticket->getId());
        $form->save();

        // Save the form data from the help-topic form, if any
        foreach ($topic_forms as $topic_form) {
            $topic_form->setTicketId($ticket->getId());
            $topic_form->save();
        }

        $ticket->loadDynamicData();

        $dept = $ticket->getDept();

        // Start tracking ticket lifecycle events (created should come first!)
        $ticket->logEvent('created', null, $thisstaff ? : $user);

        // Add organizational collaborators
        if ($org && $org->autoAddCollabs()) {
            $pris = $org->autoAddPrimaryContactsAsCollabs();
            $members = $org->autoAddMembersAsCollabs();
            $settings = array('isactive' => true);
            $collabs = array();
            foreach ($org->allMembers() as $u) {
                if ($members || ($pris && $u->isPrimaryContact())) {
                    if ($c = $ticket->addCollaborator($u, $settings, $errors)) {
                        $collabs[] = (string) $c;
                    }
                }
            }
            //TODO: Can collaborators add others?
            if ($collabs) {
                $ticket->logEvent('collab', array('org' => $org->getId()));
            }
        }

        //post the message.
        $vars['title'] = $vars['subject']; //Use the initial subject as title of the post.
        $vars['userId'] = $ticket->getUserId();
	global $merchantMessage;
	$merchantMessage=$ticket->postMessage($vars,$origin,false);
	/**
         * get all attachments to be attached on thread
         */
        $ticketCreationAttachments=[];
        if(isset($vars["attachment"]["batch"])){
            $ticketCreationAttachments=$vars["attachment"]["batch"];
        }else if(isset($vars["attachment"]["order"])){
            $ticketCreationAttachments=$vars["attachment"]["order"];
        }else if(isset($vars["attachment"]["manifest"])){
            $ticketCreationAttachments=$vars["attachment"]["manifest"];
        }else if(isset($vars["attachment"]["product"])){
            $ticketCreationAttachments=$vars["attachment"]["product"];
        }else if(isset($vars["attachment"]["other"])){
            $ticketCreationAttachments=$vars["attachment"]["other"];
        }
//	_pr($merchantMessage->ht);die;
        foreach ($ticketCreationAttachments as $entityId=>$attachmentDetails){
	 if(isset($attachmentDetails['path'])){
		$attachmentDetails=$ticketCreationAttachments;
	 } 
 	 foreach($attachmentDetails as $attachmentDetail){
            $filePath=$attachmentDetail['path'];
            $fileType=$attachmentDetail['type'];
            $fileName=$attachmentDetail['name'];
            $fileSize=$attachmentDetail['size'];
//	    _pr($attachmentDetail);die;	
            $key = 'TicketThread' . time() . rand(1, 99999);
            $signature = md5('signature');
            if(empty($fileName)){
                $fileName = pathinfo($filePath, PATHINFO_FILENAME);
            }
            $fileParam = array("ft" => "T", "bk" => "F", "type" => $fileType,
                "size" => $fileSize, "key" => $key, "signature" => $signature,
                "name" => $fileName, "path" => $filePath, "cdn" => 1);
            $fileParamObj = AttachmentModel::create($fileParam);
            $fileParamObj->created = SqlFunction::NOW();
            $fileParamObj->save();
            $attachParam = array("object_id" => $merchantMessage->ht['id'],
                "file_id" => $fileParamObj->id, "type" => "H", "inline" => 0, "staff_id" => 0,
                "user_id" => $vars["merchant_id"], "ticket_id" => $ticket->getId());

            $attachParamObj = Attachment::create($attachParam);
            $attachParamObj->created = SqlFunction::NOW();
            $attachParamObj->save();}
        }
        // Configure service-level-agreement for this ticket
        $ticket->selectSLAId($vars['slaId']);

        // Assign ticket to staff or team (new ticket by staff)
        if ($vars['assignId']) {
            $ticket->assign($vars['assignId'], $vars['note']);
        } else {
            // Auto assign staff or team - auto assignment based on filter
            // rules. Both team and staff can be assigned
            if ($vars['staffId'])
                $ticket->assignToStaff($vars['staffId'], false);
            if ($vars['teamId'])
            // No team alert if also assigned to an individual agent
                $ticket->assignToTeam($vars['teamId'], false, !$vars['staffId']);
        }

        // Update the estimated due date in the database
        $ticket->updateEstDueDate();

        // Apply requested status - this should be done AFTER assignment,
        // because if it is requested to be closed, it should not cause the
        // ticket to be reopened for assignment.
        if ($statusId) {
            if (!$ticket->setStatus($statusId, false, $errors, false)) {
                // Tickets _must_ have a status. Forceably set one here
                $ticket->setStatusId($cfg->getDefaultTicketStatusId());
            }
        }

	/*fir tagging */
        $cause=array(227=>334);
        $cause_id=$cause[$topicId];

        if($cause_id && $vars['order_ids']){
        $query="SELECT type FROM `clues_exception_causes_list` WHERE `id`=$cause_id";
        $res=db_query($query);
        $type=db_fetch_row($res);
        $cause_type=$type[0];
        $ids=explode(",",$vars['order_ids']);
        foreach($ids as $k=>$v){
        $query="SELECT status FROM `cscart_orders` WHERE `order_id`=$v";
        $res=db_query($query);
        $status=db_fetch_row($res);
        $order_status=$status[0];
        $part .= " ($cause_id,$v, '$order_status', NOW(), '$cause_type',1,0),";
        }
        $sql = "INSERT into clues_exception_causes_order_rel (cause_id,order_id,order_status,datetime,type,latest,auth) VALUES $part";
        $sql=rtrim($sql,",");
        db_query($sql);
        }

        return $ticket;
    }

    public function sendMail($ticket, $vars,$message, $sla = '') {
        
        global $cfg;
        if(empty($sla)) $sla = $cfg->config['default_task_sla_id']['value'];
        
        $vars['sla'] = $sla;
        
        $alertstaff = (bool) (isset($vars['alert']) ? $vars['alert'] : true);
        $autorespond = (bool) (isset($vars['autorespond']) ? $vars['autorespond'] : true);
        $origin = $vars['source'];
        $dept = $ticket->getDept();
        
    //    $message = $ticket->postMessage($vars, $origin, false);

        // If a message was posted, flag it as the orignal message. This
        // needs to be done on new ticket, so as to otherwise separate the
        // concept from the first message entry in a thread.
        if ($message instanceof ThreadEntry) {
            $message->setFlag(ThreadEntry::FLAG_ORIGINAL_MESSAGE);
            $message->save();
        }
        /*         * ********   double check auto-response  *********** */
        //Override auto responder if the FROM email is one of the internal emails...loop control.
        if ($autorespond && (Email::getIdByEmail($ticket->getEmail())))
            $autorespond = false;

        # Messages that are clearly auto-responses from email systems should
        # not have a return 'ping' message
        if (isset($vars['flags']) && $vars['flags']['bounce'])
            $autorespond = false;
        if ($autorespond && $message instanceof ThreadEntry && $message->isAutoReply())
            $autorespond = false;

        // Post canned auto-response IF any (disables new ticket auto-response).
        if ($vars['cannedResponseId'] && $ticket->postCannedReply($vars['cannedResponseId'], $message, $autorespond)) {
            $ticket->markUnAnswered(); //Leave the ticket as unanswred.
            $autorespond = false;
        }

        // Log Info for creation
        $error = $logConfig["error"]["E2"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $logConfig["level"]["INFO"], $error, "title:Ticket Created", "log_location:class.ticket.php", "Ticket:" . $ticket->getId(), "", substr(json_encode($errors) . json_encode($vars), 0, 2000));
        // Check department's auto response settings
        // XXX: Dept. setting doesn't affect canned responses.
        if ($autorespond && $dept && !$dept->autoRespONNewTicket())
            $autorespond = false;

        // Don't send alerts to staff when the message is a bounce
        // this is necessary to avoid possible loop (especially on new ticket)
        if ($alertstaff && $message instanceof ThreadEntry && $message->isBounce())
            $alertstaff = false;
        if (!$mailSent) {
            if ($cfg->config['use_mail_queue_ticket_create']['value']) {
                $objects = (array('message' => $message, 'ticket' => $ticket));
                $queueData = array('objects' => serialize($objects), 'vars' => $vars, 'autorespond' => $autorespond, 'alertstaff' => $alertstaff);
                addMailToMailingQueue('ticketCreation', $queueData, 1);
            } else {
                $ticket->onNewTicket($message, $autorespond, $alertstaff, $vars);
            }
        }
   /*         * ********** check if the user JUST reached the max. open tickets limit ********* */
        if ($cfg->getMaxOpenTickets() > 0 && ($user = $ticket->getOwner()) && ($user->getNumOpenTickets() == $cfg->getMaxOpenTickets())
        ) {
            $ticket->onOpenLimit($autorespond && strcasecmp($origin, 'staff'));
        }

        // Fire post-create signal (for extra email sending, searching)
        Signal::send('ticket.created', $ticket);
    }

    public function insertEntryIntoCommonTable($ticketODataObj) {
        $cachedTicketFields = getCachedTicketFields();
        $statusName = $cachedTicketFields['status'][$this->ht['status_id']];
        $departmentName = $cachedTicketFields['dept'][$this->ht['dept_id']];
        $cluesMerchantTicket = CluesMerchantTicket::create();
        $cluesMerchantTicket->order_ids = $ticketODataObj->ht['order_ids'];
        $cluesMerchantTicket->product_ids = $ticketODataObj->ht['product_ids'];
        //$cluesMerchantTicket->batch_ids = $ticketODataObj->ht['batch_ids'];    
        $cluesMerchantTicket->manifest_ids = $ticketODataObj->ht['manifest_ids'];
        $cluesMerchantTicket->user_id = $ticketODataObj->ht['user_id'];
        $cluesMerchantTicket->merchant_email = $this->getOwner()->getEmail()->address;
        $cluesMerchantTicket->date = $this->created;
        $cluesMerchantTicket->ticket_number = $this->ht['number'];
        $cluesMerchantTicket->status = $this->ht['status_id'];
        $cluesMerchantTicket->ticket_status = $statusName;
        $cluesMerchantTicket->dept_id = $this->ht['dept_id'];
        $cluesMerchantTicket->dept_name = $departmentName;
        $cluesMerchantTicket->topic_id = $this->ht['topic_id'];
        $issuesRelatedInfo = getIssuesRelatedInfoFromTopicId($cachedTicketFields, $this->ht['topic_id']);
        $cluesMerchantTicket->issue = $issuesRelatedInfo['issueType'];
        $cluesMerchantTicket->sub_issue = $issuesRelatedInfo['issueSubType'];
        $cluesMerchantTicket->sub_sub_issue = $issuesRelatedInfo['issueSubSubType'];
        $cluesMerchantTicket->save();
    }

    /* routine used by staff to open a new ticket */

    static function open($vars, &$errors) {
        global $thisstaff, $cfg;

        if ($vars['deptId'] && $thisstaff && !$thisstaff->getRole($vars['deptId'])
                        ->hasPerm(TicketModel::PERM_CREATE)
        ) {
            $errors['err'] = __('You do not have permission to create a ticket in this department');
            return false;
        }

        if ($vars['source'] && !in_array(
                        strtolower($vars['source']), array('email', 'phone', 'other'))
        ) {
            $errors['source'] = sprintf(
                    __('Invalid source given - %s'), Format::htmlchars($vars['source'])
            );
        }

        if (!$vars['uid']) {
            // Special validation required here
            if (!$vars['email'] || !Validator::is_email($vars['email']))
                $errors['email'] = __('Valid email address is required');

            if (!$vars['name'])
                $errors['name'] = __('Name is required');
        }

        if (!$thisstaff->hasPerm(TicketModel::PERM_ASSIGN))
            unset($vars['assignId']);

        // TODO: Deny action based on selected department.

        $create_vars = $vars;
        $tform = TicketForm::objects()->one()->getForm($create_vars);
        $create_vars['cannedattachments'] = $tform->getField('message')->getWidget()->getAttachments()->getClean();

        if (!($ticket = Ticket::create($create_vars, $errors, 'staff', false)))
            return false;

        $vars['msgId'] = $ticket->getLastMsgId();

        // Effective role for the department
        $role = $thisstaff->getRole($ticket->getDeptId());

        // post response - if any
        $response = null;
        if ($vars['response'] && $role->hasPerm(TicketModel::PERM_REPLY)) {
            $vars['response'] = $ticket->replaceVars($vars['response']);
            // $vars['cannedatachments'] contains the attachments placed on
            // the response form.
            $response = $ticket->postReply($vars, $errors, false);
        }

        // Not assigned...save optional note if any
        if (!$vars['assignId'] && $vars['note']) {
            if (!$cfg->isRichTextEnabled()) {
                $vars['note'] = new TextThreadEntryBody($vars['note']);
            }
            $ticket->logNote(_S('New Ticket'), $vars['note'], $thisstaff, false);
            $ticket->save();
        }

        if (!$cfg->notifyONNewStaffTicket() || !isset($vars['alertuser']) || !($dept = $ticket->getDept())
        ) {
            return $ticket; //No alerts.
        }
        // Send Notice to user --- if requested AND enabled!!
        if (($tpl = $dept->getTemplate()) && ($msg = $tpl->getNewTicketNoticeMsgTemplate()) && ($email = $dept->getEmail())
        ) {
            $message = (string) $ticket->getLastMessage();
            if ($response) {
                $message .= ($cfg->isRichTextEnabled()) ? "<br><br>" : "\n\n";
                $message .= $response->getBody();
            }

            if ($vars['signature'] == 'mine')
                $signature = $thisstaff->getSignature();
            elseif ($vars['signature'] == 'dept' && $dept && $dept->isPublic())
                $signature = $dept->getSignature();
            else
                $signature = '';

            $attachments = ($cfg->emailAttachments() && $response) ? $response->getAttachments() : array();
            if (strstr($msg->ht['body'], '%{')) {
                //echo "sdfdfd";exit;
                $msg->ht['subject'] = $ticket->replaceCannedVariables($ticket->ht['ticket_id'], $msg->ht['subject']);
                $msg->ht['body'] = $ticket->replaceCannedVariables($ticket->ht['ticket_id'], $msg->ht['body']);
            }
            //var_dump($msg);exit;
            $msg = $ticket->replaceVars($msg->asArray(), array(
                'message' => $message,
                'signature' => $signature,
                'response' => ($response) ? $response->getBody() : '',
                'recipient' => $ticket->getOwner(), //End user
                'staff' => $thisstaff,
                    )
            );
            $references = array();
            $message = $ticket->getLastMessage();
            if (isset($message))
                $references[] = $message->getEmailMessageId();
            if (isset($response))
                $references[] = $response->getEmailMessageId();
            $options = array(
                'references' => $references,
                'thread' => $message ? : $ticket->getThread(),
            );
            $email->send($ticket->getOwner(), $msg['subj'], $msg['body'], $attachments, $options);
        }
        return $ticket;
    }

    /**
     * Function to replace canned response variables
     * @param int $ticketId Id of ticket
     * @param string $message
     */
    function replaceCannedVariables($id, $message) {

        $variableString = explode("%{", $message);
        $variableString = array_slice($variableString, 1);
        foreach ($variableString as $var) {
            $variableName[] = explode("}", $var)[0];
        }
        $sql = "SELECT * from " . TICKET_ODATA_TABLE . " WHERE ticket_id=" . $id . "";
        $res = db_query($sql);
        while ($row = db_fetch_array($res)) {
            $orderDetails = $row;
        }
        foreach ($variableName as $var) {
            $v = explode(".", $var);
            if ($orderDetails[$v[1]]) {
                $message = str_replace("%{" . $var . "}", $orderDetails[$v[1]], $message);
            }
        }
        return $message;
    }

    static function checkOverdue() {
        /*
          $overdue = static::objects()
          ->filter(array(
          'isoverdue' => 0,
          Q::any(array(
          Q::all(array(
          'reopened__isnull' => true,
          'duedate__isnull' => true,

          Punt for now
         */

        $sql = 'SELECT ticket_id FROM ' . TICKET_TABLE . ' T1 '
                . ' INNER JOIN ' . TICKET_STATUS_TABLE . ' status
                ON (status.id=T1.status_id AND status.state="open") '
                . ' LEFT JOIN ' . SLA_TABLE . ' T2 ON (T1.sla_id=T2.id AND T2.flags & 1 = 1) '
                . ' WHERE isoverdue=0 '
                . ' AND ((reopened is NULL AND duedate is NULL AND TIME_TO_SEC(TIMEDIFF(NOW(),T1.created))>=T2.grace_period*3600) '
                . ' OR (reopened is NOT NULL AND duedate is NULL AND TIME_TO_SEC(TIMEDIFF(NOW(),reopened))>=T2.grace_period*3600) '
                . ' OR (duedate is NOT NULL AND duedate<NOW()) '
                . ' ) ORDER BY T1.created LIMIT 50'; //Age upto 50 tickets at a time?

        if (($res = db_query($sql)) && db_num_rows($res)) {
            while (list($id) = db_fetch_row($res)) {
                if ($ticket = Ticket::lookup($id))
                    $ticket->markOverdue();
            }
        } else {
            //TODO: Trigger escalation on already overdue tickets - make sure last overdue event > grace_period.
        }
    }

    static function updateTicket($vars, &$errors, $origin, $autorespond = true, $alertstaff = true) {
        global $thisstaff;
        $fields = array();
        $result = array();

        $fields['ticket_id'] = array('type' => 'int', 'required' => 1, 'error' => __('Ticket Id not exist'));
        $fields['agent'] = array('type' => 'string', 'required' => 1, 'error' => __('Invalid Agent Display Name'));
        $fields['email'] = array('type' => 'string', 'required' => 1, 'error' => __('Invalid Email'));

        $fields['topicId'] = array('type' => 'int', 'required' => 0, 'error' => __('Help topic selection is required'));
        $fields['assignTo'] = array('type' => 'int', 'required' => 0, 'error' => __('Ticket Id not exist'));
        $fields['duedate'] = array('type' => 'date', 'required' => 0, 'error' => __('Invalid date format - must be MM/DD/YY'));
        $fields['user_id'] = array('type' => 'int', 'required' => 0, 'error' => __('Invalid user-id'));
        $fields['order_id'] = array('type' => 'int', 'required' => 0, 'error' => __('Invalid order-id'));
        $fields['return_id'] = array('type' => 'int', 'required' => 0, 'error' => __('Invalid order-id'));
        $fields['attach'] = array('type' => 'array', 'required' => 0, 'error' => __('Invalid attachment'));


        $validation = Validator::process($fields, $vars, $errors);

        if ($validation != 1 || sizeof($errors) > 0) {
            $errors['err'] = __('Missing or invalid data - check the errors and try again');
            $result['error'] = $errors['err'];
            $output = array("status" => "error", "msg" => $result['error']);
            return $output;
            exit;
        }
        $posterStaffObj = Staff::lookup($vars['email']);
        if (!$posterStaffObj) {
            global $cfg;
            $emailName = explode("@", $vars["email"])[0];
            $adminAgentConfig = json_decode($cfg->config['adminAgentConfigSingleSignon']['value'], true);
            $defaultPassDec = $adminAgentConfig['password']['decoded']; //'welcome';
            $defaultPassEnc = $adminAgentConfig['password']['encoded']; //; // Setting Password as welcome
            $newStaff = Staff::create();
            $newStaff->username = $vars["email"];
            $newStaff->firstname = ucfirst(explode(".", $emailName)[0]);
            $newStaff->lastname = ucfirst(explode(".", $emailName)[1]);
            $newStaff->isactive = $adminAgentConfig['default_active'];
            $newStaff->created = SqlFunction::NOW();
            $newStaff->permissions = json_encode($priv);
            $newStaff->isadmin = $adminAgentConfig['default_admin'];
            $newStaff->dept_id = $adminAgentConfig['default_dept_id'];
            $newStaff->role_id = $adminAgentConfig['default_role_id'];
            $newStaff->passwd = $defaultPassEnc;
            $newStaff->max_page_size = 50;
            $newStaff->email = $vars["email"];
            $newStaff->save();
        }
        $posterStaffObj = Staff::lookup($vars['email']);
        if (!$posterStaffObj) {
            $errors[] = array("err" => "Invalid Agent");
            return $errors;
        } elseif (!$posterStaffObj->hasPerm(TicketModel::PERM_EDIT)) {
            $errors[] = array("err" => "Not permitted");
            return $errors;
        }
        /*         * ******* Ticket updation ********* */
        try {

            $ticket = Ticket::lookup($vars["ticket_id"]);
            $ticketOdataObj = TicketOData::lookup($vars["ticket_id"]);
            // Update dynamic meta-data 
            if (!$ticket || !$ticketOdataObj) {
                $errors[] = array("err" => "Ticket does not Exists");
                return $errors;
            }
            if (!empty($vars["assignTo"])) {

                $newStaffId = $vars["assignTo"];
                $staffObj = Staff::lookup($newStaffId);
                if ($staffObj && is_numeric($newStaffId)) {
                    $ticket->assignToStaff($vars['assignTo']);
                } else {
                    $errors[] = array("err" => "Such Staff not Exists");
                    return $errors;
                }
            }
            if (!empty($vars["attach"])) {

                //navneet
                $numFiles = 0;
                foreach ($vars["attach"] as $files) {
                    $key = 'Ticket' . time() . rand(1, 99999);
                    $signature = md5('signature');
                    if (empty($files["name"])) {
                        $files["name"] = pathinfo($files["path"], PATHINFO_FILENAME);
                    }
                    $fileParam = array("ft" => "T", "bk" => "F", "type" => $files["type"], "size" => $files["size"], "key" => $key, "signature" => $signature, "name" => $files["name"], "path" => $files["path"]);
                    $fileParamObj = AttachmentModel::create($fileParam);
                    $fileParamObj->created = SqlFunction::NOW();
                    $fileParamObj->save();

                    $threadObj = Thread::objects()->filter(array("object_id" => $vars["ticket_id"], 'entries__type' => 'M'))->values('entries__id')->order_by('-entries__id')->limit(1)->all();
                    $threadId = $threadObj[0]["entries__id"];
                    $attachParam = array("object_id" => $threadId, "file_id" => $fileParamObj->id, "type" => "H", "inline" => 0,'ticket_id'=>$ticket->getId());

                    $attachParamObj = Attachment::create($attachParam);
                    $attachParamObj->save();
                    //Attachment
                    $numFiles++;
                }
            }
            if (!empty($vars["order_id"])) {

                if (is_numeric($vars["order_id"])) {
                    $oldOrder_id = $ticketOdataObj->order_id;
                    $ticketOdataObj->order_id = $vars["order_id"];
                    $ticketOdataObj->save();

                    $data = json_encode(array("order_id_new" => $order_id, "order_id_old" => $oldOrder_id));
                    $ticket->logEvent('orderlink', json_encode($data), $posterStaffObj);
                } else {
                    $errors[] = array("err" => "Invalid Order Id");
                    return $errors;
                }
            }
            if (!empty($vars["return_id"])) {

                if (is_numeric($vars["return_id"])) {
                    $oldOrder_id = $ticketOdataObj->retrn_id;
                    $ticketOdataObj->retrn_id = $vars["return_id"];
                    $ticketOdataObj->save();

                    $data = json_encode(array("return_id_new" => $order_id, "return_id_old" => $oldOrder_id));
                    $ticket->logEvent('returnlink', json_encode($data), $posterStaffObj);
                } else {
                    $errors[] = array("err" => "Invalid Return Id");
                    return $errors;
                }
            }
            if (!empty($vars["duedate"])) {

                if (strtotime($vars["duedate"]) > time()) {
                    $old_duedate = $ticket->duedate;
                    $ticket->setDueDate($vars["duedate"]);
                    $changes = array("duedate" => $old_duedate, "new_duedate" => $vars["duedate"]);
                    $ticket->logEvent('edited', $changes, $posterStaffObj);
                } else {
                    $errors[] = array("err" => "Invalid DueDate");
                    return $errors;
                }
            }
            if (!empty($vars["topicId"])) {

                $topicObj = Topic::lookup($vars["topicId"]);
                if ($topicObj && is_numeric($vars["topicId"])) {
                    $oldTopic_id = $ticket->topic_id;
                    $ticket->retrn_id = $vars["return_id"];
                    $ticket->save();

                    $data = json_encode(array("old" => $helpTopicChange[$existingIssue], "new" => $helpTopicChange[$topic]));
                    $ticket->logEvent('issueedit', $data, $thisstaff);
                } else {
                    $errors[] = array("err" => "Invalid Topic Id");
                    return $errors;
                }
            }
            if (!empty($vars["priority"])) {

                if (is_numeric($vars["priority"])) {
                    $oldpriority = $ticketOdataObj->priority;
                    $ticketOdataObj->priority = $vars["priority"];
                    $ticketOdataObj->save();

                    $data = json_encode(array("priority_old" => $oldpriority, "priority" => $vars["priority"]));
                    $ticket->logEvent('edited', $data, $thisstaff);
                } else {
                    $errors[] = array("err" => "Invalid Topic Id");
                    return $errors;
                }
            }


            $output = array("status" => "success", "msg" => 1);
            return $output;
        } catch (Exception $e) {
            $output = array("status" => "error", "msg" => "exception occured");
            return $output;
        }
    }

    function searchForLeftPanelFilters($req) {

        global $thisstaff, $cfg, $ost;
        $result = array();
        $criteria = array();

        $select = 'SELECT ticket.ticket_id';
        $from = ' FROM ' . TICKET_TABLE . ' ticket
                  LEFT JOIN ' . TICKET_STATUS_TABLE . ' status
                    ON (status.id = ticket.status_id) ';
        //Access control.
        $where = ' WHERE ( (ticket.staff_id=' . db_input($thisstaff->getId())
                . ' AND status.state="open" )';

        if (($teams = $thisstaff->getTeams()) && count(array_filter($teams)))
            $where.=' OR (ticket.team_id IN (' . implode(',', db_input(array_filter($teams)))
                    . ' ) AND status.state="open" )';

        if (!$thisstaff->showAssignedOnly() && !$_REQUEST['deptId'] && ($depts = $thisstaff->getDepts())) {
            $where.=' OR ticket.dept_id IN (' . implode(',', db_input($depts)) . ')';
        } else {
            $dept = $_REQUEST['deptId'];
            $where.=' OR ticket.dept_id=' . db_input($dept);
        }

        $where.=' ) ';
        //Department
        if ($req['deptId']) {
            $where.=' AND ticket.dept_id=' . db_input($req['deptId']);
            $criteria['dept_id'] = $req['deptId'];
        }

        //Help topic
        if ($req['topicId']) {
            $where.=' AND ticket.topic_id=' . db_input($req['topicId']);
            $criteria['topic_id'] = $req['topicId'];
        }

        // Status
        if ($req['statusId'] && ($status = TicketStatus::lookup($req['statusId']))) {
            $where .= sprintf(' AND status.id="%d" ', $status->getId());
            $criteria['status_id'] = $status->getId();
        }

        // Flags
        if ($req['flag']) {
            switch (strtolower($req['flag'])) {
                case 'answered':
                    $where .= ' AND ticket.isanswered =1 ';
                    $criteria['isanswered'] = 1;
                    $criteria['state'] = 'open';
                    $where .= ' AND status.state="open" ';
                    break;
                case 'overdue':
                    $where .= ' AND ticket.isoverdue =1 ';
                    $criteria['isoverdue'] = 1;
                    $criteria['state'] = 'open';
                    $where .= ' AND status.state="open" ';
                    break;
            }
        }

        //Assignee
        if ($req['assignee'] && strcasecmp($req['status'], 'closed')) { # assigned-to
            $id = preg_replace("/[^0-9]/", "", $req['assignee']);
            $assignee = $req['assignee'];
            $where.= ' AND ( ( status.state="open" ';
            if ($assignee[0] == 't') {
                $where.=' AND ticket.team_id=' . db_input($id);
                $criteria['team_id'] = $id;
            } elseif ($assignee[0] == 's' || is_numeric($id)) {
                $where.=' AND ticket.staff_id=' . db_input($id);
                $criteria['staff_id'] = $id;
            }

            $where.=')';

            if ($req['staffId'] && !$req['status']) //Assigned TO + Closed By
                $where.= ' OR (ticket.staff_id=' . db_input($req['staffId']) .
                        ' AND status.state IN("closed")) ';
            elseif ($req['staffId']) // closed by any
                $where.= ' OR status.state IN("closed") ';

            $where.= ' ) ';
        } elseif ($req['staffId']) { # closed-by
            $where.=' AND (ticket.staff_id=' . db_input($req['staffId']) . ' AND
                status.state IN("closed")) ';
            $criteria['state__in'] = array('closed');
            $criteria['staff_id'] = $req['staffId'];
        }

        //dates
        $startTime = ($req['startDate'] && (strlen($req['startDate']) >= 8)) ? strtotime($req['startDate']) : 0;
        $endTime = ($req['endDate'] && (strlen($req['endDate']) >= 8)) ? strtotime($req['endDate']) : 0;
        if ($endTime)
        // $endTime should be the last second of the day, not the first like $startTime
            $endTime += (60 * 60 * 24) - 1;
        if (($startTime && $startTime > time()) or ( $startTime > $endTime && $endTime > 0))
            $startTime = $endTime = 0;

        if ($startTime) {
            $where.=' AND ticket.created>=FROM_UNIXTIME(' . $startTime . ')';
            $criteria['created__gte'] = $startTime;
        }

        if ($endTime) {
            $where.=' AND ticket.created<=FROM_UNIXTIME(' . $endTime . ')';
            $criteria['created__lte'] = $startTime;
        }

        // Dynamic fields
        $cdata_search = false;
        foreach (TicketForm::getInstance()->getFields() as $f) {
            if (isset($req[$f->getFormName()]) && ($val = $req[$f->getFormName()])) {
                $name = $f->get('name') ? $f->get('name') : 'field_' . $f->get('id');
                if (is_array($val)) {
                    $cwhere = '(' . implode(' OR ', array_map(
                                            function($k) use ($name) {
                                        return sprintf('FIND_IN_SET(%s, `%s`)', db_input($k), $name);
                                    }, $val)
                            ) . ')';
                    $criteria["cdata.{$name}"] = $val;
                } else {
                    $cwhere = "cdata.`$name` LIKE '%" . db_real_escape($val) . "%'";
                    $criteria["cdata.{$name}"] = $val;
                }
                $where .= ' AND (' . $cwhere . ')';
                $cdata_search = true;
            }
        }
        if ($cdata_search)
            $from .= 'LEFT JOIN ' . TABLE_PREFIX . 'ticket__cdata '
                    . " cdata ON (cdata.ticket_id = ticket.ticket_id)";

        //Query
        $joins = array();
        if ($req['query']) {
            // Setup sets of joins and queries
            if ($s = $ost->searcher)
                return $s->find($req['query'], $criteria, 'Ticket');
        }

        $sections = array();
        foreach ($joins as $j) {
            $sections[] = "$select $from {$j['from']} $where AND ({$j['where']})";
        }
        if (!$joins)
            $sections[] = "$select $from $where";

        $sql = implode(' union ', $sections);
        if (!($res = db_query($sql)))
            return TicketForm::dropDynamicDataView();

        $tickets = array();
        while ($row = db_fetch_row($res))
            $tickets[] = $row[0];

        return $tickets;
    }

    // Function for getting order info from soa apis


    function getOrderInfoFromSoaApis($orderid, $text = "") {
        require_once '../scp/soa.inc.php';
        $token = soa_get_access_token();
        $orderData = getOrderInfo($orderid, $token, $text);
        return Format::json_encode($orderData);
    }

    function getOrderInfoFromCustomApi($orderid) {
        require_once SCP_DIR . 'CustomApi.php';
        $orderData = getOrderDetails($orderid);
        return Format::json_encode($orderData);
    }

    function getSellerInfoFromCustomApi($email = '') {
        require_once SCP_DIR . 'CustomApi.php';
        $sellerData = getSellerDetails($email);
        return Format::json_encode($sellerData);
    }

    function getOrderInfoFromReturnApi($returnId, $orderid, $email = '') {
        require_once SCP_DIR . 'ReturnApi.php';
        $returnData = getReturnDetails($returnId, $orderid, $email = '');
        return Format::json_encode($returnData);
    }

    // Function for getting canned response text
    function getCannedText($id) {
        $sql = "SELECT response FROM mst_canned_response WHERE canned_id=" . $id;
        $result = db_query($sql);
        $res = db_fetch_array($result);
        return $res['response'];
    }

    // function to get statuses and stats for status filters
    function getStatsCount() {
        global $thisstaff, $cfg, $ost;
        $stats = $thisstaff->getTicketsStats();
        return $stats;
    }
    
    function getDepartmentWiseTaskBreakUp(){
        global $thisstaff;
        $depts=$thisstaff->getDepartments();
        $inclause=UtilityFunctions::getInClauseFromArray($depts);
        $sql="SELECT 
                mt.dept_id,
                COUNT(1) AS departmentTaskCount 
              FROM
                mst_task mt 
                JOIN mst_ticket mtt 
                  ON mtt.`ticket_id` = mt.`object_id` 
                  AND mtt.`status_id` IN (1,6,9) and mt.flags=1 
                  and mt.dept_id in ($inclause)
              GROUP BY dept_id ;";
        $rs=db_query($sql);
        $data=[];
        while($row=  db_fetch_array($rs)){
            $data[$row['dept_id']]=$row['departmentTaskCount'];
        }
        return $data;
    }


    function getOSLASLACountForDepartment($deptId) {
        $deptId = (int) $deptId;
        $sql = "SELECT 
                COUNT(1) AS totalNumberOfTickets,
                SUM(CASE
                    WHEN
                        ost.`closed` IS NULL
                    THEN
                        CASE
                            WHEN ost.`est_duedate` >= NOW() THEN 1
                            ELSE 0
                        END
                    ELSE 0
                END) AS ticketsOpenWithinSla,
                SUM(CASE
                    WHEN
                        ost.`closed` IS NULL
                    THEN
                        CASE
                            WHEN ost.`est_duedate` < NOW() THEN 1
                            ELSE 0
                        END
                    ELSE 0
                END) AS ticketsOpenWithinOSla
            FROM
                mst_ticket ost
            WHERE
                ost.dept_id = $deptId
                     and ost.`isanswered` = 0 
                    AND ost.status_id IN (1 , 6) group by ost.dept_id;";
        $rs = db_query($sql);
        $ticketOSLAStats = [];
        while ($row = db_fetch_array($rs)) {
            $ticketOSLAStats[] = $row;
        };
        return $ticketOSLAStats;
    }

    function getRRCountForTickets() {
        $sql = "";
    }

    static function checkDupicateTicket(&$data) {
        global $cfg;
        $mdata = Ticket::objects()->filter(array("topic_id" => $data["topicId"], "status_id__neq" => $cfg->config['closeId']['value']))->values('ticket_id', 'odata__order_ids','number', 'odata__manifest_ids', 'odata__product_ids')->all();
 foreach ($mdata as $k => $v) {
            if (!empty($data['order_ids'])) {
                $input_ids = array_unique(explode(",", $data['order_ids']));
		$data['order_ids']=implode(',',$input_ids);
                $output_ids = explode(",", $v['odata__order_ids']);
                $intersect = array_intersect($input_ids, $output_ids);
                if ($intersect === $input_ids) {
                return $v['number'];
		}
            } elseif (!empty($data['product_ids'])) {
                $input_ids = array_unique(explode(",", $data['product_ids']));
		$data['product_ids']=implode(',',$input_ids);
                $output_ids = explode(",", $v['odata__products_ids']);
                $intersect = array_intersect($input_ids, $output_ids);
                if ($intersect === $input_ids) {
		return $v['number'];                
		}		
            } elseif (!empty($data['manifest_ids'])) {
                $input_ids = array_unique(explode(",", $data['manifest_ids']));
		$data['manifest_ids']=implode(',',$input_ids);
                $output_ids = explode(",", $v['odata__manifest_ids']);
                $intersect = array_intersect($input_ids, $output_ids);
                if ($intersect === $input_ids) {
		return $v['number'];               
		 }
            }
        }
        return false;
    }

    static function validateTicket($data, &$error) {
        global $mandateInfoCreation;
        global $mandateDataCreation;
        global $APIKey;
	global $cfg;
	$disAllowTicketIds=explode(',',$cfg->config['ticket_creation_disable']['value']);
	if(in_array($data['topicId'],$disAllowTicketIds)){
	        $error['err'] = "Ticket creation not allowed for this topic";
                return true;		
	}
        if (empty($data)) {
            $error['err'] = "Empty Data";
            return true;
        }
        if ($data['order_ids'] && $data['manifest_ids']) {
            $error['err'] = "Both manifest and order id present";
            return true;
        }
        if (isset($data['order_ids'])) {
            $input = $data['order_ids'];
            $mandateInfoCreation = "order";
            $mandateDataCreation = $data['order_ids'];
        } elseif (isset($data['manifest_ids'])) {
            $input = $data['manifest_ids'];
            $mandateInfoCreation = "manifest";
            $mandateDataCreation = $data['manifest_ids'];
        } elseif (isset($data['product_ids'])) {
            $input = $data['product_ids'];
            $mandateInfoCreation = "product";
            $mandateDataCreation = $data['product_ids'];
        }elseif(isset($data['batch_ids'])){
            
            $input = $data['batch_ids'];
            $mandateInfoCreation=TicketUtilityFunctions::getMandateInfoInWhichTicketAccordingToTopic($data['topicId']);
            if(empty($mandateInfoCreation)){
                throw new Exception("Topic not mapped to batch correctly");
            }
            $mandateDataCreation = $data['batch_ids'];
        }
        
        if (!preg_match_all("/^ *([0-9]+( *, *[0-9]+)*)? *$/", $input)) {
             $error['err'] = "Mandatory id format not correct";
             return true;
        }
        $input = rtrim($input, ",");
        $matches = substr_count($input, ",") + 1;

        if (($matches) > 25) {
            $error['err'] = "Mandatory id limit(25) exceeded";
            return true;
        }
        $topic = Topic::lookup($data['topicId']);
           if(!$topic){
           $error['err'] = "Invalid help topic selected";
            return true;

        }
        $capabilities = $topic->getCapabilities();
        $capabilities = json_decode($capabilities, true);
        if ($capabilities['isRequired']) {
            
            //check requiredParam
            if (($capabilities['requiredParam'] == 'orderId' && empty($data['order_ids'])) || ($capabilities['requiredParam'] == 'manifestId' && empty($data['manifest_ids'])) || ($capabilities['requiredParam'] == 'productId' && empty($data['product_ids'])) || ($capabilities['requiredParam'] == 'batchId' && empty($data['batch_ids']))) {
                $error['err'] = "Please enter mandatory id";
                return true;
            }
            if ( $APIKey->getNotes()!='MCR' &&  $capabilities['isAttachment']['flag'] && empty($data['attachment'])) {
                $error['err'] = "Attachment required";
                return true;
            }
            if (($capabilities['extra']['courierName'] && empty($data['carrier_id'])) || ($capabilities['extra']['trackingNo'] && empty($data['trackingNo'])) || ($capabilities['extra']['manifestId'] && empty($data['manifestId']))) {
                $error['err'] = "Courier related info missing";
                return true;
            }

          if( $APIKey->getNotes()!='MCR' && $capabilities['extra']['bulk'] && empty($data['attachment']['other'])){
                $error['err'] = "Bulk attachment missing";
                return true;    
               }
          if( $APIKey->getNotes()!='MCR' && $capabilities['extra']['attachmentLabel'] && empty($data['attachment']['other'])){
                $error['err'] = "Attachment required";
                return true;
               }

        }

        return false;
    }

    function getTicketAttachments() {
        $attachment = Attachment::objects()->filter(array(
                    'ticket_id' => $this->getId(), 'type__notin' => array('H','A')
                ))->all();
        return $attachment;
//                
//        $mdata = TicketMData::objects()->filter(array("ticket_id" => $this->getId()))->values('attachment')->all();
//        $val=$mdata[0];
//        return $val['attachment'];       
    }

    function getAtttachmentEntityIds() {
        $sql = "SELECT order_ids,manifest_ids,product_ids,batch_ids from " . TICKET_MDATA_TABLE . " WHERE ticket_id=" . $this->getId() . " LIMIT 1";
        $rs = db_query($sql);
       
        $id_array=array();
        $row = db_fetch_array($rs);
        foreach ($row as $key => $value) {
            if (!is_null($value) || $value != '') {
                $id_array = explode(",", $value);
                break;
            }
        }
        $map = array('order_ids' => 'OrderId', 'manifest_ids' => 'ManifestId', 'product_ids' => 'ProductId',"batch_ids"=>'BatchId');
        $array_key = $map[$key];

        if(empty($id_array)){
         $array_key='EntityId';
        }

        return array($array_key => $id_array);
    }

    public function checkPriority($ticketId , $userId) {
        global $cfg;
        $merchantId = UserAccount::objects()->filter(array('user_id'=>$userId))->values('merchant_id')->all()[0]['merchant_id'];
        $busiGuru = json_decode($cfg->config['business_guru']['value'],true);
        $res = CluesBizGuru::objects()->filter(array('company_id'=>$merchantId,'product_id__in'=>$busiGuru,'expiry_date__gt'=>'NOW()'))->values('id')->all();
        if(!empty($res)){
            return true;
        }
        return false;
    }

}

class Triage extends VerySimpleModel {

    static $meta = array(
        'table' => TRIAGE_TABLE,
        'pk' => array('ticket_id'),
    );

}

class OrderStatusDescription extends VerySimpleModel {

    static $meta = array(
        'table' => ORDER_STATUS_DESCRIPTION_TABLE,
        'pk' => array('status'),
    );

}
Class ConsumerErrorLogs extends VerySimpleModel{
    static $meta = array(
        'table' => CONSUMER_ERROR_LOG_TABLE,
        'pk' => array('status'),
    );
}

class TicketFlow extends VerySimpleModel {

    static $meta = array(
        'table' => TICKET_FLOW_TABLE,
        'pk' => array('ticketflow_id'),
    );

}

class OrderReportingMaster extends VerySimpleModel {

    static $meta = array(
        'table' => CLUES_ORDER_REPORTING_MASTER_TABLE,
        'pk' => array('order_id'),
    );

}

Class OrderManifestDetails extends VerySimpleModel {

    static $meta = array(
        'table' => CLUES_ORDER_MANIFEST_DETAILS_TABLE,
        'pk' => array('manifest_id'),
    );

}

Class ProductDetails extends VerySimpleModel {

    static $meta = array(
        'table' => CLUES_ORDER_PRODUCT_DETAILS_TABLE,
        'pk' => array('product_id'),
    );

}

Class CareerLookup extends VerySimpleModel {

    static $meta = array(
        'table' => CLUES_CAREER_LOOKUP_TABLE,
        'pk' => array('carrier_id'),
    );

}

Class OrderManifest extends VerySimpleModel {

    static $meta = array(
        'table' => CLUES_ORDER_MANIFEST_TABLE,
        'pk' => array('manifest_id'),
    );

}

class TaskWithTicket extends VerySimpleModel {

    static $meta = array(
        'table' => TICKET_TASK_MAP_MANAGER,
        'pk' => array('id'),
    );

}

class AdminOSTConfig extends VerySimpleModel {

    static $meta = array(
        'table' => ADMIN_OST_CONFIG_TABLE,
        'pk' => array('id'),
    );

}
Class TicketUtilityFunctions {

    static $entityType = null;
    
    static  function getMandateInfoInWhichTicketAccordingToTopic($topicId){
        $topicRelatedInfo=Topic::objects()->filter(array("topic_id"=>$topicId))->values(array("topictype__topic_type_name","topic_id",
            "topic"))->all();
        $topic_type_name=trim($topicRelatedInfo[0]['topictype__topic_type_name']);
        switch ($topic_type_name){
            case 'Batch Update':
                return 'batch_update';
            case 'Batch Upload':
                return 'batch_upload';
            default :
                return 'batch_upload';
        }
        return '';
    }

    /**
     * 
     * @param type $ticketId mandatory
     * @param type $ticketDetail optional , if ticketDetail object passed from client then uses the same array 
     * otherwise query the database.
     * @return string
     * @throws Exception
     */
    public static function getMandatoryInfoInWhichTicketHasBeenMade($ticketId, $ticketDetail = array()) {
        $ticket=Ticket::lookup($ticketId);
        return $ticket->ht['mandate_info'];
        $ticketId = (int) $ticketId;
        if ($ticketId <= 0) {
            throw new Exception("Empty ticket id passed");
        }
        if (!is_null(self::$entityType)) {
            return self::$entityType;
        }
        
        if (is_array($ticketDetail) && count($ticketDetail) == 0) {
            $ticketDetail = TicketOData::objects()->filter(array('ticket_id' => $ticketId))->values('order_ids', 'manifest_ids', 'product_ids','batch_ids', 'ticket_id')->first();
        }
        
        if (count($ticketDetail) == 0) {
            throw new Exception("Ticket entry not found in ticket data. Query:-" . last_query());
        }
        if (!empty($ticketDetail['manifest_ids'])) {
            self::$entityType = "manifest";
            return "manifest";
        }
        if (!empty($ticketDetail['order_ids'])) {
            self::$entityType = "order";
            return "order";
        }
        if (!empty($ticketDetail['product_ids'])) {
            self::$entityType = "product";
            return "product";
        }
        if (!empty($ticketDetail['batch_ids'])) {
            self::$entityType = "batch";
            return "batch";
        }
        return "";
    }

    public static function getAttachmentFromTicket($ticketId, $ticketDetails = array()) {
        $ticketId = (int) $ticketId;
        if ($ticketId <= 0) {
            throw new Exception("Empty ticket id passed");
        }
        if (is_array($ticketDetail) && count($ticketDetail) == 0) {
            $ticketDetail = TicketOData::objects()->filter(array('ticket_id' => $ticketId))->values('order_ids', 'manifest_ids', 'product_ids', 'ticket_id')->first();
        }
    }

    public static function checkIfMandatoryInfoValid($mandatoryInfo, $mandatoryIds = array()) {
	global $cfg;
	$checkForMandateInfo=$cfg->config['validate_mandate_id']['value'];
	if($checkForMandateInfo==0){
		return array();	
	}
        if (!is_array($mandatoryIds) || count($mandatoryIds) == 0) {
            return 0;
        }
        $mandateDBIds = [];
        switch ($mandatoryInfo) {
            case 'order':
                $orderIdFromDBs = OrderReportingMaster::objects()->filter(array("order_id__in" => $mandatoryIds))->values('order_id')->all();
                foreach ($orderIdFromDBs as $orderIdFromDB) {
                    $mandateDBIds[] = $orderIdFromDB['order_id'];
                }
                break;
            case 'manifest':
                $manifestIdFromDBs = OrderManifestDetails::objects()->filter(array("manifest_id__in" => $mandatoryIds))->values('manifest_id')->all();
                foreach ($manifestIdFromDBs as $manifestIdFromDB) {
                    $mandateDBIds[] = $manifestIdFromDB['manifest_id'];
                }
                break;
            case 'product':
                $productIdsFromDB=ProductDetails::objects()->filter(array('product_id__in'=>$mandatoryIds))->values('product_id')->all();
                foreach ($productIdsFromDB as $productIdFromDB) {
                    $mandateDBIds[] = $productIdFromDB['product_id'];
                }
                break;
            case 'batch':
            case 'batch_update':
            case 'batch_upload':
                $bulkUploadsFromDB=
                        BulkBatchUpload::objects()->filter(array('batch_id__in'=>$mandatoryIds))->values(array('batch_id'))->all();
                foreach ($bulkUploadsFromDB as $bulkUploadFromDB){
                     $mandateDBIds[] = $bulkUploadFromDB['batch_id'];
                }
                break;
        }
        return array_diff($mandatoryIds, $mandateDBIds);
    }
    
    public static function setMandatoryInfoForTicket($mandateInfo, $ticket_id, $mandateIds,$ticket) {
        if (!is_array($mandateIds) && count($mandateIds) == 0) {
            throw new Exception('must pass some mandate ids');
        }
        $commaSeperatedMandateIds = implode(',', $mandateIds);
        $commaSeperatedMandateIds = db_input($commaSeperatedMandateIds);
        $ticket_id = (int) $ticket_id;
        $sql = "";
        switch ($mandateInfo) {
            case 'order':
                $sql = "update " . TABLE_PREFIX . 'ticket__data' . " set order_ids=$commaSeperatedMandateIds where ticket_id=$ticket_id";
                db_query($sql);
                $sql = "update " . TABLE_PREFIX . 'ticket' . " set mandate_info='order' where ticket_id=$ticket_id";
                db_query($sql);
                $cluesMerchantObject = CluesMerchantTicket::objects()->filter(array('ticket_number' => $ticket->getNumber()))->first();
                if($cluesMerchantObject instanceof CluesMerchantTicket){
                $cluesMerchantObject->order_ids =$commaSeperatedMandateIds;
                $cluesMerchantObject->save();
                }
                break;
            case 'manifest':
                $sql = "update " . TABLE_PREFIX . 'ticket__data' . " set manifest_ids=$commaSeperatedMandateIds where ticket_id=$ticket_id";
                db_query($sql);
                $sql = "update " . TABLE_PREFIX . 'ticket' . " set mandate_info='manifest' where ticket_id=$ticket_id";
                db_query($sql);
                $cluesMerchantObject = CluesMerchantTicket::objects()->filter(array('ticket_number' => $ticket->getNumber()))->first();
                if($cluesMerchantObject instanceof CluesMerchantTicket){
                $cluesMerchantObject->manifest_ids = $commaSeperatedMandateIds;
                $cluesMerchantObject->save();
                }
                break;
            case 'product':
                $sql = "update " . TABLE_PREFIX . 'ticket__data' . " set product_ids=$commaSeperatedMandateIds where ticket_id=$ticket_id";
                db_query($sql);
                $sql = "update " . TABLE_PREFIX . 'ticket' . " set mandate_info='product' where ticket_id=$ticket_id";
                db_query($sql);
                $cluesMerchantObject = CluesMerchantTicket::objects()->filter(array('ticket_number' => $ticket->getNumber()))->first();
                if($cluesMerchantObject instanceof CluesMerchantTicket){
                $cluesMerchantObject->manifest_ids = $commaSeperatedMandateIds;
                $cluesMerchantObject->save();
                }
                break;
             case 'batch_upload':
             case 'batch_update':
                $batchNameToBeUsed=$mandateInfo;
                $sql = "update " . TABLE_PREFIX . 'ticket__data' . " set batch_ids=$commaSeperatedMandateIds where ticket_id=$ticket_id";
                db_query($sql);
                $sql = "update " . TABLE_PREFIX . 'ticket' . " set mandate_info='$batchNameToBeUsed' where ticket_id=$ticket_id";
                db_query($sql);
                $cluesMerchantObject = CluesMerchantTicket::objects()->filter(array('ticket_number' => $ticket->getNumber()))->first();
                if($cluesMerchantObject instanceof CluesMerchantTicket){
                $cluesMerchantObject->batch_ids = $commaSeperatedMandateIds;
                $cluesMerchantObject->save();
                }
                break;
        }
    }
    static function getProductStatusFromProductIds($productIds){
         $productStatusForProductIds=ProductDetails::objects()->filter(array('product_id__in'=>$productIds))->values('product_id','status')->all();
         $productIdStatusMap=[];
         foreach ($productStatusForProductIds as $productIdStatus){
             $productIdStatusMap[$productIdStatus['product_id']]=$productIdStatus['status'];
         }
         return $productIdStatusMap;
    }
    static function getBatchStatusFromBatchIds($batchIds){
          $batchStatusIDs=
                        BulkBatchUpload::objects()->filter(array('batch_id__in'=>$batchIds))->values(array('batch_id','status'))->all();
          $batchStatusIdMap=[];
          foreach ($batchStatusIDs as $batchStatusId){
              $batchStatusIdMap[$batchStatusId['batch_id']]=$batchStatusId['status'];
          }
          return $batchStatusIdMap;
    }

}
