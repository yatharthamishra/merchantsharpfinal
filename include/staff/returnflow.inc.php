<table class="list" width="100%";>
    <form name="returnFlow" action="returnflow.php" method="POST">
        <thead>
            <tr><th colspan="8"><?php echo __('Return Flow machine - Auto Responser'); ?></th></tr>
        </thead>
        <tbody>
            <tr><th><?php echo __('Status Type'); ?></th>
                <th><?php echo __('Return Status'); ?></th>
                <th><?php echo __('Return Action'); ?></th>
                <th><?php echo __('Value Range'); ?></th>
                <th><?php echo __('Tag Name'); ?></th>
                <th><?php echo __('Sub reason'); ?></th>
                <th><?php echo __('Dept'); ?></th>
                <th><?php echo __('Task'); ?></th>
                <th style='background: #060; color:#fff;'><?php echo __('Response'); ?></th>
                <th style='background: #060; color:#fff;'><?php echo __('Communnication'); ?></th>
                <th ><?php echo __('Remove'); ?></th>
            </tr>
        </tbody>
        
        <?php
        $emailTemp = EmailTemplateNew::objects()->filter(array("code_name__startswith"=>"order"))->values("id","subject")->all();
         foreach ($emailTemp as $template) { 
             $emailTempArr[$template["id"]] = $template["subject"];
         }
        $staticFields = getCachedTicketFields();
        $taskStaticFields = getCachedTaskFields();
        $returnStaticFields = getCachedReturnFlowFields();
        $flowMapFields = getFlowMapFields();
        $isExport = FALSE;
        if($_GET['EXPORT']=='exportTrue')
        $isExport = TRUE;

        global $cfg;
        $returnFlow = ReturnFlow::objects()->filter(array("flowstatus"=>1));
        $page_limit = PAGE_LIMIT;
        $bulkPageLimit = json_decode($cfg->config['customLimits']["value"],true);
        $bulkPageLimit = isset($bulkPageLimit["bulkTickets"])?$bulkPageLimit["bulkTickets"]:5000;
        $page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
        $count = $returnFlow->count();
        $returnFlowExport= clone $returnFlow;
        if ($_GET['pl']) {
            $page_limit = $_GET['pl'];
        }

        if($_SESSION['bulk'])
            $pageNav = new Pagenate($count, $page, $bulkPageLimit);
        else
            $pageNav = new Pagenate($count, $page, $page_limit);
        $pageNav->setURL('returnflow.php', $args);
        $returnFlow = $pageNav->paginate($returnFlow);

        if($_SESSION['bulk'])
            $returnFlow->limit($bulkPageLimit);


        foreach ($returnFlow->all() as $flow) {
            $taskIdArr[] = $flow->flow_id;
            $flowArr[] = $flow;
        }
        $start = $count>0?($page-1)*$page_limit+1:0;
        $last = ($page*$page_limit)<$count?($page*$page_limit):$count;
        $showing = "Showing $start to $last of $count";

if($isExport) {
ob_get_clean();
$fp = fopen("php://output", "w");
$header = array('Status Type','Return Status','Return Action','Value Range','Tag Name','Sub reason','Dept','Task','Response','Communnication');
fputcsv($fp, $header);

$returnFlowExport->values()->all();

foreach ($returnFlowExport as $flow) { 
              $statType=$flow['status_type'];
              $ordStatus=$staticFields["orderStatus"][$flow['status_type']][$flow['status']]?:"Any";
              $returnAction= $returnStaticFields["return_action"][$flow['return_action']];
              $valRange= $flowMapFields["valueRange"][$flow['value_min']."-".$flow['value_max']];
              $retTag=$returnStaticFields["tagName"][$flow['tag_name']]?:"-";
              $retSubReason= $returnStaticFields["sub_reason"][$flow['sub_reason']];
              $dept= $staticFields["department"][$flow['dept']]["name"]?:"All Department";
              $taskType= $taskStaticFields["Task Types"][$flow['task_type']]?:"Any Task";                $taskResp= $taskStaticFields["Task Response"]; 
              $communication=$emailTempArr[$flow['communication']]?:"No communication";
   
              $dataInCsv = array($statType,$ordStatus,$returnAction,$valRange,$retTag,$retSubReason,$dept,$taskType,$taskresp,$communication);
             fputcsv($fp,$dataInCsv);            
}
fclose($fp);
die;
echo '<div id="exportTicketData" style="display:none">YES</div>';
}    
        
        ?>
        <caption><span class="pull-left" style="display:inline-block;vertical-align:middle"><?php
                 echo $showing; ?></span>
        </caption>
        <?php 
        foreach ($flowArr as $flow) { ?>
            <tbody>
                <tr>
                    <td><?php echo $flow->status_type; ?></td>
                    <td><?php echo $staticFields["orderStatus"][$flow->status_type][$flow->status]?:"Any"; ?></td>
                    <td><?php echo $returnStaticFields["return_action"][$flow->return_action]; ?></td>
                    <td><?php echo $flowMapFields["valueRange"][$flow->value_min."-".$flow->value_max]; ?></td>
                    <td><?php echo $returnStaticFields["tagName"][$flow->tag_name]?:"-"; ?></td>
                    <td><?php echo $returnStaticFields["sub_reason"][$flow->sub_reason]; ?></td>
                    <td><?php echo $flow->dept?$staticFields["department"][$flow->dept]["name"]:"All Department"; ?></td>
                    <td><?php echo $flow->task_type?$taskStaticFields["Task Types"][$flow->task_type]:"Any Task"; ?></td>
                    <td><select class='response' flowId="<?php echo $flow->flow_id; ?>" style="width:150px;">
                            <?php
                            $completeSelected = "selected";
                            foreach ($taskStaticFields["Task Response"] as $id=>$data) {
                                if ($flow->task_response == $id) {
                                    $selected = "selected";
                                    $completeSelected = "";
                                }
                                ?>
                                <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $data ?></option>
                                    <?php $selected = "";
                                }; ?>
                        </select></td>
                    <td><select class='communication' flowId="<?php echo $flow->flow_id; ?>" style="width:150px;">
                            <?php
                            $completeSelected = "selected";
                            foreach ($emailTempArr as $id=>$data) {
                                if ($flow->communication == $id) {
                                    $selected = "selected";
                                    $completeSelected = "";
                                }
                                ?>
                                <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $data ?></option>
        <?php $selected = "";
    }; ?>
                            <option value='0' <?php echo $completeSelected; ?>>No Communication</option>
                        </select>
                    </td>
                    <td><input type="submit" value="X" name="Remove-<?php echo $flow->flow_id; ?>"></td>
                </tr>
            </tbody>
                        <?php } ?>
        <tbody>
            <tr><th colspan="7"><br><br></th></tr>
        </tbody>
        <tbody>
            <tr>
                <td><select name='type'  id='statusType' style="width:150px;" readonly>
                        <?php foreach ($staticFields["orderStatus"] as $type => $os) { ?>
                        <option value='<?php echo $type; ?>' <?php if($type=="Return"){?> selected="selected" <?php }else{ ?>  disabled="disabled" <?php }?> ><?php echo $type; ?></option>
                        <?php }; ?>
                    </select></td>
                <td>   
                    <select class='dependentOptions' name='returnStatus[]' id='Return' multiple="multiple" style="width:150px;">
                           <option value='0'>Any</option>
                        <?php foreach ($staticFields["orderStatus"]["Return"] as $id => $statusName) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                    <?php } ?>
                    </select>
                </td>
                <td>   
                    <select name='returnAction[]' id="returnAction"  multiple="multiple" style="width:60px;">
                    <?php foreach ($returnStaticFields["return_action"] as $id => $statusName) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                        <?php } ?>
                    </select>

                </td>
                <td><select name='valueRange[]' id='valueRange' multiple="multiple" style="width:60px;">
                        <?php foreach ($flowMapFields["valueRange"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='tagName[]' id='tagName' multiple="multiple" style="width:60px;">
                        <?php foreach ($returnStaticFields["tagName"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='subReason[]' id='subReason' multiple="multiple" style="width:60px;">
                        <?php foreach ($returnStaticFields["sub_reason"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='taskDept[]' id='taskDept' multiple="multiple" style="width:150px;">
                       <?php foreach ($staticFields["department"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data["name"] ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='taskType[]' id='taskType' multiple="multiple" style="width:150px;">
                        <?php foreach ($taskStaticFields["Task Types"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='response' style="width:150px;">
                        <?php foreach ($taskStaticFields["Task Response"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
<?php }; ?>
                    </select>
                <td><select name='communication' style="width:150px;">
                         <option value='0'>No Communication</option>
                        <?php
                        
                        foreach ($emailTempArr as $id=>$tem) { ?>
                            <option value='<?php echo $id ?>'><?php echo $tem ?></option>
<?php }; ?>
                    </select></td>
                <td></td>
            </tr>
        </tbody>
        <tbody>
            <tr><th colspan="7"><input type='submit' value="Add Flow"></th></tr>
        </tbody>
    </form>
</table>
<?php
$getParam = explode('?', $_SERVER['REQUEST_URI']);
    if(!empty($getParam[1]))
        $appendInUrl = $getParam[1].'&EXPORT=exportTrue';
    else 
        $appendInUrl='EXPORT=exportTrue';

echo '<div class="paging">&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;';  
echo '<div class="export_bttm pull-right"><a class="export-csv no-pjax" href="?'.$appendInUrl.'">Export</a></div>';
?>
<script>
    $('.communication').change(function () {
        var flowId = $(this).attr("flowId");
        var communication = $(this).val();
        var data = {"flowType":"Return","flowId": flowId, "value": communication,"field":"communication"};
        var url = 'ajax.php/tasks/flowupdate';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (resp)
            {
                alert(resp);
            }
        });
    });
        $('.response').change(function () {
        var flowId = $(this).attr("flowId");
        var response = $(this).val();
        var data = {"flowType":"Return","flowId": flowId,"field":"task_response","value": response};
        var url = 'ajax.php/tasks/flowupdate';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (resp)
            {
                alert(resp);
            }
        });
    });
    $("#statusType").change(function(){
        var statusType = $("#statusType").val();
        $(".dependentOptions").hide();
        $("#"+statusType).show();
        
    });
$(function() {
        $("#Return").select2({'placeholder':'Return Status'});
        $("#returnAction").select2({'placeholder':'Return Action'});
        $("#valueRange").select2({'placeholder':'Value Range'});
        $("#tagName").select2({'placeholder':'Tag Name'});
        $("#subReason").select2({'placeholder':'Sub Reason'});
        $("#taskDept").select2({'placeholder':'Task Dept'});
        $("#taskType").select2({'placeholder':'Task Type'});
    })
</script>
