<div class="table_with_links left_tickets_link grid2 float_left">
     
    <div class="left_slide_bar_report">
        <ul>
            <li>
                <h3><a href="tickets.php?report=1&type=triage&subtype=all" <?php if($flag=="t_all") echo "class='active'"; ?>>Triaging</a></h3> 
                <ul>
                    <li>
                        <a href="tickets.php?report=1&type=triage&subtype=sla adherence&subsubtype=agent level" <?php if($flag=="t_sla") echo "class='active'"; ?>>SLA Adherence</a>
                    </li>
                    <li>
                        <a href="tickets.php?report=1&type=triage&subtype=aht&subsubtype=agent level" <?php if($flag=="t_aht") echo "class='active'"; ?>>Average Handling Time</a>
                    </li>
                    <!-- <li>
                        <a href="tickets.php?report=1&type=triage&subtype=productivity" <?php if($flag=="t_prod") echo "class='active'"; ?>>Productivity</a>
                    </li> -->
                    <li>
                        <a href="tickets.php?report=1&type=triage&subtype=pendency" <?php if($flag=="t_pend") echo "class='active'"; ?>>Pendency</a>
                    </li>
                </ul>
            </li>
            <!-- <li>
                <h3><a href="tickets.php?report=1&type=ops&subtype=all">OPS</a></h3>
                <ul>
                    <li>
                        <a href="tickets.php?report=1&type=ops&subtype=sla adherence&subsubtype=agent level">SLA Adherence</a>
                    </li>
                    <li>
                        <a href="tickets.php?report=1&type=ops&subtype=aht&subsubtype=agent level">Average Handling Time</a>
                    </li>
                </ul>
            </li> -->
        </ul>
    </div>
</div>