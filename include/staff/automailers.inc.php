<?php
if(!defined('OSTADMININC') || !$thisstaff->isAdmin()) die('Access Denied');

$qs = array();
$sortOptions = array(
        'email' => 'email',
        'dept' => 'dept__name',
        'priority' => 'priority__priority_desc',
        'created' => 'created',
        'updated' => 'updated');


$orderWays = array('DESC'=>'DESC', 'ASC'=>'ASC');
$sort = ($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])]) ?  strtolower($_REQUEST['sort']) : 'email';
if ($sort && $sortOptions[$sort]) {
        $order_column = $sortOptions[$sort];
}

$order_column = $order_column ? $order_column : 'email';

if ($_REQUEST['order'] && isset($orderWays[strtoupper($_REQUEST['order'])]))
{
        $order = $orderWays[strtoupper($_REQUEST['order'])];
} else {
        $order = 'ASC';
}

$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$count = EmailAutoMailer::objects()->count();
$pageNav = new Pagenate($count, $page, PAGE_LIMIT);
$qs += array('sort' => $_REQUEST['sort'], 'order' => $_REQUEST['order']);
$pageNav->setURL('automailers.php', $qs);
$showing = $pageNav->showing().' '._N('email', 'emails', $count);
$qstr = '&amp;order='.($order=='DESC' ? 'ASC' : 'DESC');

$def_dept_id = $cfg->getDefaultDeptId();
$def_dept_name = $cfg->getDefaultDept()->getName();
$def_priority = $cfg->getDefaultPriority()->getDesc();
$emailTemp = EmailTemplateNew::objects()->filter(array("code_name__startswith"=>"automail"))->values("id","subject","code_name")->all();
?>

<form action="emails.php" method="POST" name="emails">

<div class="pull-left" style="padding-top:5px;">
 <h2><?php echo __('Auto Mailers');?></h2>
 </div>
<?php csrf_token(); ?>
 <input type="hidden" name="do" value="mass_process" >
 <input type="hidden" id="action" name="a" value="" >
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th width="400"><a <?php echo $email_sort; ?> href="emails.php?<?php echo $qstr; ?>&sort=email"><?php echo __('Entity Id');?></a></th>
            <th width="400"><a <?php echo $email_sort; ?> href="emails.php?<?php echo $qstr; ?>&sort=email"><?php echo __('Auto Mailer');?></a></th>
            <th width="120"><a  <?php echo $priority_sort; ?> href="emails.php?<?php echo $qstr; ?>&sort=priority"><?php echo __('Auto Mailer Type');?></a></th>
            <th width="250"><a  <?php echo $dept_sort; ?> href="emails.php?<?php echo $qstr; ?>&sort=dept"><?php echo __('Template Mapped');?></a></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $ids = ($errors && is_array($_POST['ids'])) ? $_POST['ids'] : null;
        if ($count):
            $defaultId=$cfg->getDefaultEmailId();
            $autoMailers = EmailAutoMailer::objects()
                ->limit($pageNav->getLimit())
                ->offset($pageNav->getStart());

            foreach ($autoMailers as $autoMailer) {
                $id = $autoMailer->getId();
                ?>
        <tr>
            <input type="hidden" value="<?php echo $id; ?>" id="id_auto_mail">
            <td> <?php echo $id;?></td>
            <td> <?php echo $autoMailer->getAutoMailerName();?></td>
            <td><?php echo $autoMailer->getAutoMailerEntityType();?></td>
            <td>
            <select class="autoCron" id="autoTemplate">
                <option value="0">--Select--</option>
            <?php 
                        foreach ($emailTemp as $emailTemplateDetail){
                ?>
                <option  <?php if($autoMailer->getTemplateCodeMapped()==$emailTemplateDetail['code_name']){echo 'selected';}?> value="<?php echo $emailTemplateDetail['code_name']."|".$emailTemplateDetail['id']."|".$id;; ?>"><?php echo $emailTemplateDetail['subject'];?></option>
                        <?php }?>
            </select>    
            </td>
        </tr>
            <?php
            } //end of while.
        endif; ?>
    
</table>
<?php
if ($count):
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
?>

<?php
endif;
?>
</form>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo sprintf(__('Are you sure you want to DELETE %s?'),
            _N('selected email', 'selected emails', 2)) ;?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered.');?>
    </p>
    <div><?php echo __('Please confirm to continue.');?></div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="button" value="<?php echo __('No, Cancel');?>" class="close">
        </span>
        <span class="buttons pull-right">
            <input type="button" value="<?php echo __('Yes, Do it!');?>" class="confirm">
        </span>
     </p>
    <div class="clear"></div>
</div>
<script type="text/javascript"> 
$(".autoCron").on('change',function(){
    var templateRelatedData=$(this).val();
    var templateArray=templateRelatedData.split('|');
    var code_name=templateArray[0];
    var template_id =templateArray[1];
    var auto_mailer_id=templateArray[2];
    var url = 'ajax.php/content/automail_update';
          var data = {
              'code_name':code_name,
              'template_id':template_id,
              'auto_mailer_id':auto_mailer_id
          };
          console.log(code_name);
          console.log(template_id);
          console.log(auto_mailer_id);
            $.ajax({
                type: "POST",
                url: url, 
                data: data,
                cache: false,
                success: function(resp)
                {
                    resp=resp.trim();
                    alert(resp);
                }
            });
    console.log(code_name);
    console.log(template_id);
    console.log(auto_mailer_id);
});
</script>
