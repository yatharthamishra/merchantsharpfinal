<?php
if (!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin())
    die('Access Denied');

$dept_id = $_GET["dept"]?$_GET["dept"]:1;
$staff = Staff::objects()->filter(Q::any(array("dept_id" => $dept_id, 'dept_access__dept_id' => $dept_id)))->values('staff_id', 'firstname','role__grade','role__name', 'lastname', 'dept_id')->order_by('role__grade')->distinct('staff_id')->all();
foreach ($staff as $s) {
    $staffTotal[] = $s["staff_id"];
    $staffDetails[$s["staff_id"]] = $s;
    if($s["dept_id"]==$dept_id && $s["role__grade"]){
        $staffDetails[$s["staff_id"]]["grade"]=$s["role__grade"];
        $staffDetails[$s["staff_id"]]["name"]=$s["role__name"];
    }    
}
$staff = Staff::objects()->filter(Q::any(array("dept_id" => $dept_id, 'dept_access__dept_id' => $dept_id)))->values('staff_id', 'firstname','role__grade','role__name', 'dept_access__role__grade', 'dept_access__role__name', 'dept_access__dept_id', 'lastname', 'dept_id', 'dept_access__dept_id')->order_by('role__grade','dept_access__role__grade')->distinct('staff_id')->all();
foreach ($staff as $s) {
    $staffTotal[] = $s["staff_id"];
    $staffDetails[$s["staff_id"]] = $s;
    if($s["dept_id"]==$dept_id && $s["role__grade"]){
        $staffDetails[$s["staff_id"]]["grade"]=$s["role__grade"];
        $staffDetails[$s["staff_id"]]["name"]=$s["role__name"];
    }elseif($s["dept_access__dept_id"]==$dept_id && $s["dept_access__role__grade"]){
        $staffDetails[$s["staff_id"]]["grade"]=$s["dept_access__role__grade"];
        $staffDetails[$s["staff_id"]]["name"]=$s["dept_access__role__name"];
    }   
}

$FMroles = StaffManager::objects()->filter(array('dept_id' => $dept_id))->values('agent_id', 'staff__firstname')->all();
$roles = StaffManager::objects()->filter(array('dept_id' => $dept_id))->values('agent_id', 'manager_id', 'staff__firstname', 'manager__firstname')->all();
/*$roles = StaffMap::objects()->order_by('depth')->values('agent_id','manager_id','staff__firstname','manager__firstname','depth')->all();
$roles = Staff::objects()->order_by('dept_access__role__grade')->values('staff_id', 'dept_id', 'role__name', 'role__grade', 'dept_access__dept_id', 'dept_access__role__name', 'dept_access__role__grade', 'firstname', 'lastname', 'staff_id')->all();
foreach ($roles as $r) {
    if ($r['dept_id'] == $dept_id)
        $staffArr[$r["staff_id"]]["grade"] = $r["role__grade"];
    if ($r['dept_access__dept_id'] == $dept_id)
        $staffArr[$r["staff_id"]]["grade"] = $r["dept_access__role__grade"];
}
*/

foreach ($FMroles as $role) {
    $role["manager_id"] = 0;
    $roleArr[$role["agent_id"]] = $role;
    $staffExist[] = $role["agent_id"];
}
foreach ($roles as $role) {
    $roleArr[$role["agent_id"]] = $role;
    $staffExist[] = $role["agent_id"];
}
$staffAvailable = array_diff($staffTotal, $staffExist);
//echo "<pre>";print_r($roleArr);die;
?>

<table>
    <tr>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <tr></tr>

    <div id="orgChartAka"></div>
</table>
<?php foreach (array_unique($staffAvailable) as $staffId) { 
                $options[$staffDetails[$staffId]["grade"]] .= "<option value='$staffId'>".$staffDetails[$staffId]["firstname"] . " " . $staffDetails[$staffId]["lastname"]."</option>";
}

?>
<script>
    var agents = <?php echo json_encode($options);?>;
    var testData = [
<?php foreach ($roleArr as $role => $roleDetails) { 
    if($staffDetails[$roleDetails["agent_id"]]["name"]){?>
            {id: <?php echo $roleDetails["agent_id"]; ?>, name: '<?php echo $roleDetails["staff__firstname"]." (".$staffDetails[$roleDetails["agent_id"]]["name"].")"; ?>', parent: <?php echo $roleDetails["manager_id"]; ?>},
    <?php }} ?>
    ];

    $(function () {
        org_chart = $('#orgChartAka').orgChart({
            data: testData, // your data
            showControls: false, // display add or remove node button.
            allowEdit: false, // click the node's title to edit
            onAddNode: function (node) {
                $(".org-add-button").hide();
            },
            onDeleteNode: function (node) {},
            onClickNode: function (node) {},
            newNodeText: 'Reportee' // text of add button
        });
    });
    $("body").on('change', '.org-input', function () {
        var nodeId = $(this).parent(".node").attr("node-id");
        var parentNodeId = $(this).parent(".node").attr("parent-node-id");
        var value = $(this).val();
        $.ajax({
            type: "POST",
            url: "ajax.php/staff/addSubAgent",
            data: {"agent_id": value, "manager_id": parentNodeId, "dept_id":<?php echo $dept_id; ?>},
            cache: false,
            success: function (response) {
                console.log(response);
                if (0 && response == 1) {
                    window.location.reload();
                }
            }
        });

    });


</script>


