<?php
global $cfg;
$staticFields = getCachedTicketFields();
$taskStaticFields = getCachedTaskFields();
$isExport = FALSE;
if($_GET['EXPORT']=='exportTrue')
  $isExport = TRUE;
?>
<center>
    <div style="margin-bottom:5px;">
        <h2 style="cursor:pointer;" id="filter">Filter</h2>
        <form id="formFilter" name="formFilter" action="taskflow.php" method="get">
            <?php echo __('Department'); ?>
            <select name="departmentFilter">
                <option value="0">None</option>
                <?php
                foreach ($staticFields["department"] as $key => $value) {
                    $text = '<option value="'.$key.'"';
                    if($_GET['departmentFilter']==$key) 
                        $text.= "selected";
                    $text.= '>'.$value['name'].'</option>';
                    echo $text;
                }
                ?>
            </select>
            <input type="submit" value="Filter">
        </form>
    </div>
</center>
<form name="taskFlow" action="taskflow.php" method="POST">
<table class="list" width="100%;">
    <thead>
    <tr><th colspan="8"><?php echo __('Task Flow machine'); ?></th></tr>
</thead>
<tbody>
    <tr><th colspan="1"></th>
    <th colspan="4"><?php echo __('Task Having these attributes'); ?></th>
    <th colspan="2" style='background: #060; color:#fff;'><?php echo __('Changes in Ticket and new task creation'); ?></th></tr>
    <th colspan="1"></th></tr>
</tbody>
<tbody>
    <tr><th><?php echo __('Task Id'); ?></th>
        <th><?php echo __('Department'); ?></th>
        <th><?php echo __('Type'); ?></th>
        <th><?php echo __('Status'); ?></th>
        <th><?php echo __('Response'); ?></th>
        <th style='background: #060; color:#fff;'><?php echo __('Ticket Status'); ?></th>
        <th style='background: #060; color:#fff;'><?php echo __('Next Task'); ?></th>
        <th ><?php echo __('Communication'); ?></th>
        <th ><?php echo __('Remove'); ?></th>
    </tr>
</tbody>
<?php
$emailTemp = EmailTemplateNew::objects()->filter(array("code_name__startswith"=>"order"))->values("id","templateGroup__template_name","subject")->all();
 foreach ($emailTemp as $template) {
     if(empty($template["templateGroup__template_name"])){
	  $emailTempArr[$template["id"]] = $template["subject"];
	}else{
	  $emailTempArr[$template["id"]] = $template["templateGroup__template_name"];
	}
 }

if($_GET && $_GET['departmentFilter']!=0){
    $urlargs="?departmentFilter=".$_GET['departmentFilter'];
    $filter = array('dept_id' =>$_GET['departmentFilter'],"flowstatus"=>1);
}
else
    $filter = array("flowstatus"=>1);

$taskFlowModel = TaskFlow::objects();
$taskFlowModelNew = clone $taskFlowModel;
$taskFlowModel->filter($filter);
$taskFlowModelNew->filter(array("flowstatus"=>1));


$page_limit = PAGE_LIMIT;
$bulkPageLimit = json_decode($cfg->config['customLimits']["value"],true);
$bulkPageLimit = isset($bulkPageLimit["bulkTickets"])?$bulkPageLimit["bulkTickets"]:5000;
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$count = $taskFlowModel->count();
if ($_GET['pl']) {
    $page_limit = $_GET['pl'];
}

if($_SESSION['bulk'])
    $pageNav = new Pagenate($count, $page, $bulkPageLimit);
else
    $pageNav = new Pagenate($count, $page, $page_limit);
$pageNav->setURL('taskflow.php'.$urlargs, $args);
$taskFlowModelNew1 = clone $taskFlowModel;
$taskFlowModel = $pageNav->paginate($taskFlowModel);

if($_SESSION['bulk'])
    $taskFlowModel->limit($bulkPageLimit);

foreach($taskFlowModel->all() as $flow){ 
    //$taskIdArr[]=$flow->task_id; 
    $flowArr[]=$flow;  
}
foreach ($taskFlowModelNew->all() as $flow) {
    $taskIdArr[]=$flow->task_id; 
}
$start = $count>0?($page-1)*$page_limit+1:0;
$last = ($page*$page_limit)<$count?($page*$page_limit):$count;
$showing = "Showing $start to $last of $count";

if($isExport) {
    ob_get_clean();
    $fp = fopen("php://output", "w");
    $header = array('Task Id','Department','Task Type','Task Status','Response','Ticket Status','Next Task','Communication');
    fputcsv($fp, $header);
   
        
         foreach ($taskFlowModelNew1->all() as $flow) {
            $task_id= $flow->task_id; 
            $dept= $staticFields["department"][$flow->dept_id]["finalPath"];
            $ttype=$taskStaticFields["Task Types"][$flow->type]?:"Any"; 
            $tstatus=$taskStaticFields["Task Status"][$flow->task_status];
            $taskresp= $taskStaticFields["Task Response"][$flow->task_response];
            $tickstatus=$staticFields["status"][$flow->ticket_status];
          $nexttask='complete';
            foreach ($taskIdArr as $data){
                 if($flow->next_task_id==$data){
                     $nexttask=$data;
                    break;   
                 }
            }
            $communication=$emailTempArr[$flow->communication]?:"No communication";
            $dataInCsv = array($task_id,$dept,$ttype,$tstatus,$taskresp,$tickstatus,$nexttask,$communication);
           fputcsv($fp,$dataInCsv);
        
         }
    
    fclose($fp);
    die;
    echo '<div id="exportTicketData" style="display:none">YES</div>';
}
?>
<caption>
    <span class="pull-left" style="display:inline-block;vertical-align:middle"><?php
         echo $showing; ?></span>
</caption>
<?php
foreach($flowArr as $flow){?>
<tbody>
    <tr><td><?php echo $flow->task_id; ?></td>
        <td><?php echo $staticFields["department"][$flow->dept_id]["finalPath"]; ?></td>
        <td>
            <?php if(0){?>
        <?php echo  $taskStaticFields["Task Types"][$flow->type]?:"Any"; ?>
        <?php 
        }else{
        ?>
            <select data-id="<?php echo $flow->ht['task_id']; ?>" class='temp_task' style="width:150px;">
           <option value='0'>Select</option> 
            <?php foreach($taskStaticFields["Task Types"] as $id=>$type){ ?>
            <option 
                <?php if($id==$flow->type){
                                 echo ' selected ';   
                                }?>value='<?php echo $id?>'><?php echo $type?></option>
        <?php }; ?>
            </select>
        <?php } ?>
        </td>
        <td><?php echo  $taskStaticFields["Task Status"][$flow->task_status]; ?></td>
        <td><?php echo $taskStaticFields["Task Response"][$flow->task_response]; ?></td>
        <td><?php echo $staticFields["status"][$flow->ticket_status]; ?></td>
        <td><select class='next_task' flowId="<?php echo $flow->task_id; ?>" style="width:150px;">
            <?php
            $completeSelected="selected";
            foreach($taskIdArr as $data){ 
                if($flow->next_task_id==$data){$selected="selected";$completeSelected="";}
                ?>
            <option value='<?php echo $data?>' <?php echo $selected; ?>><?php echo $data?></option>
        <?php $selected=""; }; ?>
            <option value='' <?php echo $completeSelected; ?>>Complete</option>
            </select>
            </td>
        <td>
            <select class='communication' flowId="<?php echo $flow->task_id; ?>" style="width:150px;">
                <?php
                $completeSelected = "selected";
                foreach ($emailTempArr as $id=>$data) {
                    if ($flow->communication == $id) {
                        $selected = "selected";
                        $completeSelected = "";
                    }
                    ?>
                    <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $data ?></option>
<?php $selected = "";
} ?>
                <option value='0' <?php echo $completeSelected; ?>>No Communication</option>
            </select>
        </td>             
        <td><input type="submit" value="X" name="Remove-<?php echo $flow->task_id; ?>"></td>
    </tr>
</tbody>
<?php } ?>
<tbody>
    <tr><th colspan="7"><br><br></th></tr>
</tbody>
<tbody>
    <tr><td></td>
        <td><select name='department' style="width:150px;">
            <?php foreach($staticFields["department"] as $id=>$dep){ ?>
            <option value='<?php echo $id?>'><?php echo $dep["finalPath"]?></option>
        <?php }; ?>
            </select>
        </td>
        <td><select name='type' style="width:150px;">
            <option value='0'>Any</option>
            <?php foreach($taskStaticFields["Task Types"] as $id=>$type){ ?>
            <option value='<?php echo $id?>'><?php echo $type?></option>
        <?php }; ?>
            </select></td>
        <td><select name='status' style="width:150px;">
            <?php foreach($taskStaticFields["Task Status"] as $id=>$data){ ?>
            <option value='<?php echo $id?>'><?php echo $data?></option>
        <?php }; ?>
            </select>
            </td>
        <td><select name='response' style="width:150px;">
            <?php foreach($taskStaticFields["Task Response"] as $id=>$data){ ?>
            <option value='<?php echo $id?>'><?php echo $data?></option>
        <?php }; ?>
            </select>
        <td><select name='ticket_status' style="width:150px;">
            <?php foreach($staticFields["status"] as $id=>$data){ ?>
            <option value='<?php echo $id?>'><?php echo $data?></option>
        <?php }; ?>
            </select></td>
        <td><select name='next_task' style="width:150px;">
            <?php foreach($taskIdArr as $data){ ?>
            <option value='<?php echo $data?>'><?php echo $data?></option>
        <?php }; ?>
            <option value=''>Complete</option>
            </select></td>           
    </tr>
</tbody>
<tbody>
    <tr><th colspan="7"><input type='submit' value="Add Flow"></th></tr>
</tbody>
</table>
</form>    
<?php
$getParam = explode('?', $_SERVER['REQUEST_URI']);
    if(!empty($getParam[1]))
        $appendInUrl = $getParam[1].'&EXPORT=exportTrue';
    else 
        $appendInUrl='EXPORT=exportTrue';

echo '<div class="paging">&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;'; 
echo '<div class="export_bttm pull-right"><a class="export-csv no-pjax" href="?'.$appendInUrl.'">Export</a></div>';
?>
<script>
  $(document).ready(function(){
        $(".temp_task").select2({'placeholder': 'Select'});
	$("select[name='response']").select2({'placeholder': 'Select'});
        $("select[name='type']").select2({'placeholder': 'Select'});
    });
$('.next_task').change(function(){
    var flowId = $(this).attr("flowId");
    var nextTask = $(this).val();
    var data = {"flowId":flowId,"nextTask":nextTask,"task":"next"};
    var url = 'ajax.php/tasks/nextTaskFlow';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false, 
        success: function(resp)
        {
            alert(resp);
        }
    });
});
$('.communication').change(function () {
    var flowId = $(this).attr("flowId");
    var communication = $(this).val();
    var data = {"flowId": flowId, "communication": communication,"task":"communication"};
    var url = 'ajax.php/tasks/nextTaskFlow';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false,
        success: function (resp)
        {
            alert(resp);
        }
    });
});
$('.temp_task').change(function () {
       var taskTypeExtra=$(this).val();
       var flowId=$(this).attr('data-id');
        var data = {"flowId": flowId, "taskExtra": taskTypeExtra};
        var url = 'ajax.php/tasks/updateTaskFlow';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (resp)
            {
            if(resp){
                alert('Success');
            }else{
                alert('Failure');
            }
               
            }
        });
    });
 
$("#filter").on("click",function(){
    $("#formFilter").slideToggle();
});
</script>
