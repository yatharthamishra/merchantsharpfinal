<?php
$taskStaticFields = getCachedTaskFields();
        $isExport = FALSE;
        if($_GET['EXPORT']=='exportTrue'){
        $isExport = TRUE;
        }
?>
<center>
    <div style="margin-bottom:5px;">
        <h2 style="cursor:pointer;" id="filter">Filter</h2>
        <form id="formFilter" name="formFilter" action="tasksla.php" method="get">
            <?php echo __('Task Type'); ?>
            <select name="taskFilter">
                <option value="0">All</option>
                <?php
                foreach ($taskStaticFields["Task Types"] as $key => $value) {
                    $text = '<option value="'.$key.'"';
                    if($_GET['taskFilter']==$key) 
                        $text.= "selected";
                    $text.= '>'.$value.'</option>';
                  if(!$isExport) 
                       echo $text;
                }              
                ?>                
            </select>
            <input type="submit" value="Filter">
        </form>
    </div>
</center>
<table class="list" width="100%">
    <form name="taskFlow" action="orderflow.php" method="POST">
        <thead>
            <tr><th colspan="8"><?php echo __('TASK SLA Control Center'); ?></th></tr>
        </thead>
        <tbody>
            <tr>
                <th><?php echo __('Task Id'); ?></th>
                <th><?php echo __('Task Type'); ?></th>
                <th><?php echo __('SLA'); ?></th>
                <th><?php echo __('ED SLA'); ?></th>
                <th><?php echo __('API'); ?></th>
            </tr>
        </tbody>
        <?php
        
        $taskFlow = TaskSLA::objects()->values('task_type', 'sla_id', 'ed_sla', 'api_sla')->all();
        foreach ($taskFlow as $flow) {
            $taskFlowArr[$flow["task_type"]]["sla"] = $flow["sla_id"];
            $taskFlowArr[$flow["task_type"]]["ed_sla"] = $flow["ed_sla"];
            $taskFlowArr[$flow["task_type"]]["api"] = $flow["api_sla"];
        }
        
        $slaFlow = SLA::objects()->values('id', 'name', 'grace_period')->all();
        foreach ($slaFlow as $flow) {
            $slaArr[$flow["id"]]["name"] = $flow["name"];
            $slaArr[$flow["id"]]["grace"] = $flow["grace_period"];
        }

        $yesNo = array("No","Yes");
        ksort($taskStaticFields["Task Types"]);
        if($_GET['taskFilter']){
            $tasktype = $taskStaticFields["Task Types"][$_GET['taskFilter']];
            $data = array($_GET['taskFilter'] => $tasktype);
        }
        else
            $data = $taskStaticFields["Task Types"];
        $page_limit = PAGE_LIMIT;
        $page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
        $count = count($data);
        $no_of_page = ($count/$page_limit)?:1;
        if ($_GET['pl']) {
            $page_limit = $_GET['pl'];
        }
        $start = $count>0?($page-1)*$page_limit+1:0;
        $last = ($page*$page_limit)<$count?($page*$page_limit):$count;
        $showing = "Showing $start to $last of $count";
          if($isExport) {
              ob_clean();
    $fp = fopen("php://output", "w");
$header = array('Task Id','Task Type','SLA','ED SLA','API');
fputcsv($fp, $header);
         foreach ($data as $id => $task) {
                    $idExport=$id;
                    $taskExp=$task; 
                    foreach ($slaArr as $slaId => $sla) {
                            if ($slaId == $taskFlowArr[$id]["sla"]) {
                               $slaExp=$sla["grace"] . " hr - " . $sla["name"];
                            }
                    }
                        foreach ($slaArr as $slaId => $sla) {
                         if ($slaId == $taskFlowArr[$id]["ed_sla"]) {
                             $EdslaExport=$sla["grace"] . " hr - " . $sla["name"];
                            }
                        }
                        foreach ($yesNo as $k => $yn) {
                            if ($k == $taskFlowArr[$id]["api"]) {
                                $yesNo=$yn;
                            }
                        }
                        
       $dataInCsv = array($idExport,$taskExp,$slaExp,$EdslaExport,$yesNo);
       fputcsv($fp,$dataInCsv);
               }
  
    fclose($fp);
    die;
    echo '<div id="exportTicketData" style="display:none">YES</div>';
}
        
        ?>
        <caption><span class="pull-left" style="display:inline-block;vertical-align:middle"><?php
                 echo $showing; ?></span>
        </caption>
        <?php
        $data = array_slice(array_flip($data), ($page-1)*$page_limit,$page_limit);
        $data = array_flip($data);
        foreach ($data as $id => $task) {
            ?>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $task; ?></td>
                <td><select  class='tasksla changetask' task='<?php echo $id; ?>' taction='sla' width='300px;' name='sla'>
                        <?php
                        foreach ($slaArr as $slaId => $sla) {
                            $selected = '';
                            if ($slaId == $taskFlowArr[$id]["sla"]) {
                                $selected = 'selected="selected"';
                            }
                            ?>
                            <option value='<?php echo $slaId; ?>' <?php echo $selected; ?>><?php echo $sla["grace"] . " hr - " . $sla["name"]; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td><select  class='taskedsla changetask' task='<?php echo $id; ?>' taction='edsla' width='300px;' name='edsla'>
                        <?php
                        foreach ($slaArr as $slaId => $sla) {
                            $selected = '';
                            if ($slaId == $taskFlowArr[$id]["ed_sla"]) {
                                $selected = 'selected="selected"';
                            }
                            ?>
                            <option value='<?php echo $slaId; ?>' <?php echo $selected; ?>><?php echo $sla["grace"] . " hr - " . $sla["name"]; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td><select class='taskapi changetask' task='<?php echo $id; ?>' taction='api'  width='300px;' name='sla'>
                        <?php
                        foreach ($yesNo as $k => $yn) {
                            $selected = '';
                            if ($k == $taskFlowArr[$id]["api"]) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value='<?php echo $k; ?>' <?php echo $selected; ?>><?php echo $yn; ?></option>
                        <?php } ?>
                    </select></td>
            </tr>
        <?php } ?>
    </form>
</table>
<?php
$getParam = explode('?', $_SERVER['REQUEST_URI']);
     if(!empty($getParam[1]))
        $appendInUrl = $getParam[1].'&EXPORT=exportTrue';
    else 
        $appendInUrl='EXPORT=exportTrue';

echo '<div class="paging">&nbsp;'.__('Page').':';
echo '<div class="export_bttm pull-right"><a class="export-csv no-pjax" href="?'.$appendInUrl.'">Export</a></div>';
for($i=1;$i<=$no_of_page;$i++){
    if($i==$page)
        echo $i.'&nbsp;';
    else
        echo '<a href="?p='.$i.'">'.$i.'</a>'.'&nbsp;';
}
echo '&nbsp;'; 
?>
<script>
    $(function () {
        $(".taskapi").select2({'placeholder': 'SLA API'});
        $(".tasksla").select2({'placeholder': 'SLA'});
        $(".taskedsla").select2({'placeholder': 'ED SLA'});

        $(".changetask").on('change', function () {
            var Id = $(this).attr("task");
            var action = $(this).attr("taction");
            var response = $(this).val();
            var data = {"id": Id, "action": action, "response": response};
            var url = 'ajax.php/tasks/slaupdate';
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                cache: false,
                success: function (resp)
                {
                    alert(resp);
                }
            });
        });
    });
    $("#filter").on("click",function(){
        $("#formFilter").slideToggle();
    });
</script>