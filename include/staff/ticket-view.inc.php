<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff || !is_object($ticket) || !$ticket->getId()) die('Invalid path');

//Re-use the post info on error...savekeyboards.org (Why keyboard? -> some people care about objects than users!!)
$info=($_POST && $errors)?Format::input($_POST):array();

//Auto-lock the ticket if locking is enabled.. If already locked by the user then it simply renews.
if($cfg->getLockTime() && !$ticket->acquireLock($thisstaff->getId(),$cfg->getLockTime()))
    $warn.=__('Unable to obtain a lock on the ticket');
global $allowedToCreateTaskAndTriage;
//Get the goodies.
$dept  = $ticket->getDept();  //Dept
$role  = $thisstaff->getRole($dept);
$staff = $ticket->getStaff(); //Assigned or closed by..
$user  = $ticket->getOwner(); //Ticket User (EndUser)
$team  = $ticket->getTeam();  //Assigned team.
$sla   = $ticket->getSLA();
$lock  = $ticket->getLock();  //Ticket lock obj
$mylock = ($lock && $lock->getStaffId() == $thisstaff->getId()) ? $lock : null;
$id    = $ticket->getId();    //Ticket ID.

$mandateType=$ticket->getEntityType();
if(!empty($mandateType)){
    $mandateIds=$ticket->getEntities($ticket->getEntityType());    
}

$staticFields = getCachedTicketFields();                      
//Useful warnings and errors the user might want to know!
if ($ticket->isClosed() && !$ticket->isReopenable())
    $warn = sprintf(
            __('Current ticket status (%s) does not allow the end user to reply.'),
            $ticket->getStatus());
elseif ($ticket->isAssigned()
        && (($staff && $staff->getId()!=$thisstaff->getId())
            || ($team && !$team->hasMember($thisstaff))
        ))
    $warn.= sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
            sprintf(__('Ticket is assigned to %s'),
                implode('/', $ticket->getAssignees())
                ));

if (!$errors['err']) {

    if ($lock && $lock->getStaffId()!=$thisstaff->getId())
        $errors['err'] = sprintf(__('This ticket is currently locked by %s'),
                $lock->getStaffName());
    elseif (($emailBanned=Banlist::isBanned($ticket->getEmail())))
        $errors['err'] = __('Email is in banlist! Must be removed before any reply/response');
    elseif (!Validator::is_valid_email($ticket->getEmail()))
        $errors['err'] = __('EndUser email address is not valid! Consider updating it before responding');
}

$unbannable=($emailBanned) ? BanList::includes($ticket->getEmail()) : false;

if($ticket->isOverdue())
    $warn.='&nbsp;&nbsp;<span class="Icon overdueTicket">'.__('Marked overdue!').'</span>';

?>
<?php
$imageTypeToDisplay=getCachedAPIFields();
global  $cfg;
$imageTaskPopUpMap=$cfg->config['image_task_mapping']['value'];
$imageTaskPopUpMapArray=  json_decode($imageTaskPopUpMap,true);
?> 
  <script type="text" id="imageSelector">
<div class="popup-wrapper" style="min-width: 400px;">
      

      <div class="popup-content">
        <div class="form-group">
           <label>Choose Image Type : </label>
<select style="height: 24px;width: 181px;margin-left: 16px;border: 1px solid #656d71;border: 1px solid #656d71;height: 32px;/* margin-left: 4px; */" class="select2 multiplefield select2-hidden-accessible" id="imageOptions" multiple placeholder="Task Image Type" >
            <?php 
foreach ($imageTypeToDisplay['Default_Task Image Type'] as $taskImageId=>$taskImageName){
    $properties =  json_decode($imageTypeToDisplay['Default_Prop_Task Image Type'][$taskImageId],1);
    $propertySet=array_pop(array_reverse($properties));
    if(is_null($propertySet)){
        $attachment_required=0;                                                   
    }else{
        $attachment_required=$propertySet;
    }
    if(empty($attachment_required)){
        $attachment_required=0;
    }
    ?>
        <option data-atchment="<?php echo $attachment_required; ?>" value="<?php echo $taskImageId;?>"> <?php echo $taskImageName;?></option>
        <?php
}
            ?>
</select>
        </div>
      <button class="btn-success imgb" style="/* display: inline; *//* width: 50%; */">Continue</button>
      <button class="btn-cancel imgb" style="/* width: 50%; */">Cancel</button></div>
      <span id="imageErrorMsg" style="display:none;font-size: 84%;text-align: center;color: red;/* margin-left: auto; */float: right;margin-right: 134px;margin-bottom: 12px;">Please Select Image Type</span>
    </div>
  </script>
<div id="mandateIdPopup" style="display:none">
     <div class="popup-wrapper">
      <div class="popup-header" style="    color: white;
    background: #337AB7;"> <?php echo "Entity Type :- ".$mandateType; ?> </div>
      <div class="popup-content"> <?php echo $mandateIds;?></div>
      <div class="popup-footer">
        <button data-modal-toggle="close" class="btn-default">Close</button>
      </div>
    </div>
</div>
<input type='hidden' id='mandateType' value='<?php echo $mandateType ?>'> 
<input type='hidden' id='mandateIds' value='<?php echo $mandateIds?>'>
<div class="has_bottom_border">
    <div class="sticky bar">
       <div class="content action_box">
       
        <div class="pull-right flush-right">
            <?php
            if ($thisstaff->hasPerm(Email::PERM_BANLIST)
                    || $role->hasPerm(TicketModel::PERM_EDIT)
                    || ($dept && $dept->isManager($thisstaff))) { ?>
            <span class="action-button pull-right" data-dropdown="#action-dropdown-more">
                <i class="icon-caret-down pull-right"></i>
                <span ><i class="icon-cog"></i> <?php echo __('More');?></span>
            </span>
            <?php
            }
            // Status change options
            //echo TicketStatus::status_options();

            if (0 && $role->hasPerm(TicketModel::PERM_EDIT)) { ?>
                <a class="action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
            <?php
            }
            if ($ticket->isOpen()
                    && !$ticket->isAssigned()
                    && $role->hasPerm(TicketModel::PERM_ASSIGN)
                    && $ticket->getDept()->isMember($thisstaff)) {?>
                <a id="ticket-claim" class="action-button pull-right confirm-action" href="#claim"><i class="icon-user"></i> <?php
                    echo __('Claim'); ?></a>

            <?php
            }?>
            <span class="action-button pull-right" data-dropdown="#action-dropdown-print">
                <i class="icon-caret-down pull-right"></i>
                <a id="ticket-print" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print"><i class="icon-print"></i> <?php
                    echo __('Print'); ?></a>
            </span>
            <div id="action-dropdown-print" class="action-dropdown anchor-right">
              <ul>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=0"><i
                 class="icon-file-alt"></i> <?php echo __('Ticket Thread'); ?></a>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=1"><i
                 class="icon-file-text-alt"></i> <?php echo __('Thread + Internal Notes'); ?></a>
              </ul>
            </div>
            <div id="action-dropdown-more" class="action-dropdown anchor-right">
              <ul>
                <?php
                 if ($role->hasPerm(TicketModel::PERM_EDIT)) { ?>
                    <li><a class="change-user" href="#tickets/<?php
                    echo $ticket->getId(); ?>/change-user"><i class="icon-user"></i> <?php
                    echo __('Change Owner'); ?></a></li>
                <?php
                 }
  
                 if($ticket->isOpen() && ($dept && $dept->isManager($thisstaff))) {

                    if($ticket->isAssigned()) { ?>
                        <li><a  class="confirm-action" id="ticket-release" href="#release"><i class="icon-user"></i> <?php
                            echo __('Release (unassign) Ticket'); ?></a></li>
                    <?php
                    }

                    if(!$ticket->isOverdue()) { ?>
                        <li><a class="confirm-action" id="ticket-overdue" href="#overdue"><i class="icon-bell"></i> <?php
                            echo __('Mark as Overdue'); ?></a></li>
                    <?php
                    }

                    if($ticket->isAnswered()) { ?>
                    <li><a class="confirm-action" id="ticket-unanswered" href="#unanswered"><i class="icon-circle-arrow-left"></i> <?php
                            echo __('Mark as Unanswered'); ?></a></li>
                    <?php
                    } else { ?>
                    <li><a class="confirm-action" id="ticket-answered" href="#answered"><i class="icon-circle-arrow-right"></i> <?php
                            echo __('Mark as Answered'); ?></a></li>
                    <?php
                    }
                } ?>
                <li><a href="#ajax.php/tickets/<?php echo $ticket->getId();
                    ?>/forms/manage" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                    ><i class="icon-paste"></i> <?php echo __('Manage Forms'); ?></a></li>

<?php           if ($thisstaff->hasPerm(Email::PERM_BANLIST)) {
                     if(!$emailBanned) {?>
                        <li><a class="confirm-action" id="ticket-banemail"
                            href="#banemail"><i class="icon-ban-circle"></i> <?php echo sprintf(
                                Format::htmlchars(__('Ban Email <%s>')),
                                $ticket->getEmail()); ?></a></li>
                <?php
                     } elseif($unbannable) { ?>
                        <li><a  class="confirm-action" id="ticket-banemail"
                            href="#unbanemail"><i class="icon-undo"></i> <?php echo sprintf(
                                Format::htmlchars(__('Unban Email <%s>')),
                                $ticket->getEmail()); ?></a></li>
                    <?php
                     }
                  }
                  if ($role->hasPerm(TicketModel::PERM_DELETE)) {
                     ?>
                    <li class="danger"><a class="ticket-action" href="#tickets/<?php
                    echo $ticket->getId(); ?>/status/delete"
                    data-href="tickets.php"><i class="icon-trash"></i> <?php
                    echo __('Delete Ticket'); ?></a></li>
                <?php
                 }
                ?>
              </ul>
            </div>
        </div>
        
    </div>
  </div>
</div>
<div id="ticketCompleteDetailContainer" class="container_section grid-row">
     <?php include('custom-filter.php');?>
    <div class="grid8 float_left">
        <tr>
            
            <td>
               <div class="clear"></div>
               
    <?php if($errors['err']) { ?>
    <div id="msg_error"><?php echo $errors['err']; ?></div>
<?php }elseif($msg) { ?>
    <div id="msg_notice"><?php echo $msg; ?></div>
<?php }elseif($warn) { ?>
    <div id="msg_warning"><?php echo $warn; ?></div>
<?php } ?>            
    <div id="ticketMainDetailContainer" class="ticket_status">
    <table class="ticket_info" cellspacing="0" cellpadding="0"  border="0">

        <tr>
            <td>
                <div class="flush-left">
             <h2><a href="tickets.php?id=<?php echo $ticket->getId(); ?>"
             title="<?php echo __('Reload'); ?>"><i class="icon-refresh"></i>
             <?php echo sprintf(__('Ticket #%s'), $ticket->getNumber()); ?></a></h2>
        </div>
                <table border="0" cellspacing="" cellpadding="4">
                    <tr>
                        <th style="width:132px;"><?php echo __('Status');?>:</th>
                        <td><?php echo ($S = $ticket->getStatus()) ? $S->getLocalName() : ''; ?></td>
                    </tr>
                    <tr>
                        <th><?php echo __('Priority');?>:</th>
                        <td><?php echo $staticFields["priority"][$ticket->odata->priority]["name"]; ?></td>
                    </tr>
                    
                                    <tr>
                                        <th><?php echo __('Source'); ?>:</th>
                                        <td><?php
                                            echo Format::htmlchars(($ticket->source_extra ? $ticket->source_extra : $ticket->getSource()));

                            if($ticket->getIP())
                                echo '&nbsp;&nbsp; <span class="faded">('.$ticket->getIP().')</span>';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Department');?>:</th>
                        <td><?php echo Format::htmlchars($ticket->getDeptName()); ?></td>
                    </tr>
                                    <tr>
                        <th><?php echo __('Triaged Status');?>:</th>
                        <td><?php echo $ticket->getTraigeInfo(); ?></td>
                    </tr>
                    
                </table>
            </td>
            <td>
                <table border="0" cellspacing="" cellpadding="0">
                    <tr>
                        <th><?php echo __('Merchant name'); 
                        $link=$cfg->config['customUserDetails']['value'];
                        ?>:</th>
                        <td><span href="#tickets/<?php echo $ticket->getId(); ?>/user"><i class="icon-user"></i> <span id="user-<?php echo $ticket->getOwnerId(); ?>-name"
                                                                                                                       ><a href="<?php echo $link.$ticket->getuser_id(); ?>" target="_blank"><?php echo Format::htmlchars($ticket->getName());
                                ?></a></span></span>
                            <?php
                            if ($user) { ?>
                                
                                
                                
                                <div id="action-dropdown-stats" class="action-dropdown anchor-right">
                                    <ul>
                                        <?php
                                        if(($open=$user->getNumOpenTickets()))
                                            echo sprintf('<li><a href="tickets.php?a=search&status=open&uid=%s"><i class="icon-folder-open-alt icon-fixed-width"></i> %s</a></li>',
                                                    $user->getId(), sprintf(_N('%d Open Ticket', '%d Open Tickets', $open), $open));

                                        if(($closed=$user->getNumClosedTickets()))
                                            echo sprintf('<li><a href="tickets.php?a=search&status=closed&uid=%d"><i
                                                    class="icon-folder-close-alt icon-fixed-width"></i> %s</a></li>',
                                                    $user->getId(), sprintf(_N('%d Closed Ticket', '%d Closed Tickets', $closed), $closed));
                                        ?>
                                        <li><a href="tickets.php?a=search&uid=<?php echo $ticket->getOwnerId(); ?>"><i class="icon-double-angle-right icon-fixed-width"></i> <?php echo __('All Tickets'); ?></a></li>
    <?php   if ($thisstaff->hasPerm(User::PERM_DIRECTORY)) { ?>
                                        <li><a href="users.php?id=<?php echo
                                        $user->getId(); ?>"><i class="icon-user
                                        icon-fixed-width"></i> <?php echo __('Manage User'); ?></a></li>
    <?php   } ?>
                                    </ul>
                                </div>
    <?php                   } # end if ($user) ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th><?php echo __('Merchant email'); ?>:</th>
                        <td>
                            <span id="user-<?php echo $ticket->getOwnerId(); ?>-email"><?php echo $ticket->getEmail(); ?></span>
                        </td>
                    </tr>
    <?php   if (!empty($ticket->odata->ht['merchant_id'])) { ?>
                    <tr>
                        <th><?php echo __('Merchant Id'); ?>:</th>
                        <td>
                            <span><?php echo $ticket->odata->ht['merchant_id']; ?></span>
                        </td>
                    </tr>
    <?php   } ?>
    <?php   if ($user->getOrgId()) { ?>
                    <tr>
                        <th><?php echo __('Organization'); ?>:</th>
                        <td><i class="icon-building"></i>
                        <?php echo Format::htmlchars($user->getOrganization()->getName()); ?>
                            <a href="tickets.php?<?php echo Http::build_query(array(
                                'status'=>'open', 'a'=>'search', 'orgid'=> $user->getOrgId()
                            )); ?>" title="<?php echo __('Related Tickets'); ?>"
                            data-dropdown="#action-dropdown-org-stats">
                            (<b><?php echo $user->getNumOrganizationTickets(); ?></b>)
                            </a>
                                <div id="action-dropdown-org-stats" class="action-dropdown anchor-right">
                                    <ul>
    <?php   if ($open = $user->getNumOpenOrganizationTickets()) { ?>
                                        <li><a href="tickets.php?<?php echo Http::build_query(array(
                                            'a' => 'search', 'status' => 'open', 'orgid' => $user->getOrgId()
                                        )); ?>"><i class="icon-folder-open-alt icon-fixed-width"></i>
                                        <?php echo sprintf(_N('%d Open Ticket', '%d Open Tickets', $open), $open); ?>
                                        </a></li>
    <?php   }
            if ($closed = $user->getNumClosedOrganizationTickets()) { ?>
                                        <li><a href="tickets.php?<?php echo Http::build_query(array(
                                            'a' => 'search', 'status' => 'closed', 'orgid' => $user->getOrgId()
                                        )); ?>"><i class="icon-folder-close-alt icon-fixed-width"></i>
                                        <?php echo sprintf(_N('%d Closed Ticket', '%d Closed Tickets', $closed), $closed); ?>
                                        </a></li>
                                        <li><a href="tickets.php?<?php echo Http::build_query(array(
                                            'a' => 'search', 'orgid' => $user->getOrgId()
                                        )); ?>"><i class="icon-double-angle-right icon-fixed-width"></i> <?php echo __('All Tickets'); ?></a></li>
    <?php   }
            if ($thisstaff->hasPerm(User::PERM_DIRECTORY)) { ?>
                                        <li><a href="orgs.php?id=<?php echo $user->getOrgId(); ?>"><i
                                            class="icon-building icon-fixed-width"></i> <?php
                                            echo __('Manage Organization'); ?></a></li>
    <?php   } ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
    <?php   } # end if (user->org) ?>
                    
                    
                     <tr>
                        <th><?php echo __('Create Date');?>:</th>
                        <td><?php echo Format::datetime($ticket->getCreateDate()); ?></td>
                    </tr>
                    
                    <tr>
                            <th><?php echo __('Promised SLA'); ?>:</th>
                            <td>
                                <?php echo Format::datetime($ticket->getEstDueDate()); ?>
                            </td>
                        </tr>
                    <tr>
                        <th><?php echo __('Ticket Timer');?>:</th>
                        <td><?php $timeDiff = floor((strtotime($ticket->getEstDueDate())-time())/(3600*24));
                        if($timeDiff>1){$timeDiffWords="Respond in ".$timeDiff." days";}
                        elseif($timeDiff<=1 && $timeDiff>=0){$timeDiffWords="Respond in ".floor((strtotime($ticket->getEstDueDate())-time())/3600)."hours";}
                        else{$timeDiffWords="Response Delayed";}
                        echo $timeDiffWords;
                        ?>
                        </td>
                    </tr>

                                     
                                     <?php 
                    if($ticket->isTicketEDDepartmentTicket()){
                    ?>
                      <tr>
                        <th><?php echo __('Ticket Channel');?>:</th>
                        <td><?php echo $ticket->getTicketChannelForEdDepartment(); ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($mandateType)){
                        $entityText=  ucwords($mandateType." Entities")?>
                     <tr>
                        <th><?php echo __($entityText);?>:
                        <td><a style="cursor:pointer" id='showEntityPopUp'><?php echo $entityText;?></a></td>
                        </th>
                        
                    </tr>
                    <?php }?>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table class="ticket_info">
        <tr>
            <td>
    <div class="grid-row">
            <div  class="grid6 float_left">
                <table cellspacing="0" cellpadding="4"  border="0">
                    <?php
                    if($ticket->isOpen()) { ?>
                    <tr>
                        <th><?php echo __('Assigned To');?>:</th>
                        <td>
                            <?php
                            if($ticket->isAssigned())
                                echo Format::htmlchars(implode('/', $ticket->getAssignees()));
                            else
                                echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                            ?>
                            <?php  if($ticket->staff_id != $thisstaff->getId() && !$thisstaff->hasPerm(User::PERM_ONLY_READ_COMMENT)){ ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;-<a href='' class='assignToMe' class='font-size:6px;'>Assign to me</a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    } else if($ticket->isClosed()){ ?>
                     <tr>
                        <th><?php echo __('Solved By');?>:</th>
                        <td>
                            <?php
                            if(($staff = $ticket->getSolvedByStaff()))
                                echo Format::htmlchars($staff->getName());
                            else
                                echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                            ?>
                        </td>
                    </tr>
                                        <tr>
                        <th><?php echo __('Solved Date');?>:</th>
                        <td><?php echo Format::
                               datetime($ticket->getSolvedDate()); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo __('Closed By');?>:</th>
                        <td>
                            <?php
                            if(($staff = $ticket->getClosedByStaff()))
                                echo Format::htmlchars($staff->getName());
                            else
                                echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                            ?>
                        </td>
                    </tr>
                                        <tr>
                        <th><?php echo __('Close Date');?>:</th>
                        <td><?php echo Format::
                               datetime($ticket->getCloseDate()); ?></td>
                    </tr>
                    
                    <?php
                    } else if($ticket->isSolved()){ ?>
                                        <tr>
                        <th><?php echo __('Solved By');?>:</th>
                        <td>
                            <?php
                            if(($staff = $ticket->getSolvedByStaff()))
                                echo Format::htmlchars($staff->getName());
                            else
                                echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                            ?>
                        </td>
                    </tr>
                                        <tr>
                        <th><?php echo __('Solved Date');?>:</th>
                        <td><?php echo Format::
                               datetime($ticket->getSolvedDate()); ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
            <div  class="grid6 float_left">
                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                    <tr>
                        <th><?php echo __('Issue type');?>:</th>
                        <td><span class='issue_type'><?php echo Format::htmlchars($ticket->getHelpTopic());
                        if (in_array($ticket->triage->priority,$editableTicketChannel["channel"]) && $ticket->isOpen() &&  $role->hasPerm(TicketModel::PERM_TOPIC_CHANGE)) { ?>
                            </span>
                            <select  id='ticketIssue' style='display:none; width:150px;' name="ticketIssue">
                                <option value=''>Select</option>
                            <?php 
                            
                            foreach($staticFields['helpTopic'] as $id=>$value){
                                if($id>0){
                                    $helpTopicChange[$id] .= $staticFields['helpTopic']["parent_id"][$id]?(($helpTopicChange[$staticFields['helpTopic']["parent_id"][$id]]?:$staticFields['helpTopic'][$staticFields['helpTopic']["parent_id"][$id]])."/"):'';
                                    $helpTopicChange[$id] .= $value; 
                                }
                              }
                               foreach($helpTopicChange as $id=>$value){
                                  echo "<option value='".$id."'>".$value."</option>";
                               }
                              ?>
                    </select><i class="changeIssue icon-edit"></i>
                        <?php } ?></td>
                    </tr>
                    <tr>
                        <th nowrap><?php echo __('Last Message');?>:</th>
                        <td><?php echo Format::datetime($ticket->getLastMsgDate()); ?></td>
                    </tr>
                    <tr>
                        <th nowrap><?php echo __('Last Response');?>:</th>
                        <td><?php echo Format::datetime($ticket->getLastRespDate()); ?></td>
                    </tr>
                </table>
            </div>
    </div>
   
</tr>
</table>
    <br>
    
    <!-- Task Details -->
    <?php
    $staticTaskFields = getCachedTaskFields();
    $TaskStatusFormObj = DynamicFormField::lookup(array("name"=>"taskStatus"));
    $TaskSourceFormObj = DynamicFormField::lookup(array("name"=>"taskSource"));
    
    $taskStatus = $staticTaskFields['Task Status'];
    $taskStatusId = array_flip($staticTaskFields['id_extra_Task Status']);
    $taskStateMachine = array();
    foreach ($staticTaskFields['Prop_Task Status'] as $key => $value) {
        $taskStateMachine[$key] = explode(",",array_values(json_decode($value,true))[0]);
    }

    $TaskTypeFormObj = DynamicFormField::lookup(array("name"=>"taskType"));
 
    $taskResponseMachine = array();
    foreach ($staticTaskFields['Prop_Task Types'] as $key => $value) {
        $taskResponseMachine[$key] = array_values(json_decode($value,true))[0];
    }
    $TaskResponseFormObj = DynamicFormField::lookup(array("name"=>"taskResponse"));
    
    $comment_field_id = DynamicFormField::lookup(array("name"=>"taskComment"));

    $entityType = $ticket->getEntityType();
    $entityFilter = $entityType;
    if($entityFilter == 'batch_upload' || $entityFilter == 'batch_update') $entityFilter = 'batch';
    if(!empty($entityFilter)){
    $taskArray = Task::objects()->filter(array("object_type"=>"T","object_id"=>$ticket->getId()))->order_by(array('-flags','data__'.$entityFilter.'_id','id'))->all();
}else{
 $taskArray = Task::objects()->filter(array("object_type"=>"T","object_id"=>$ticket->getId()))->order_by(array('-flags','id'))->all();
}
    $flag = FALSE;
    foreach ($taskArray as $val) {
        $sortTaskIDs[] = $val->ht['id'];
        if(!$val->ht['flags']) $flag = TRUE;
    }
    sort($sortTaskIDs);
    $j = 1;
    foreach ($sortTaskIDs as $val) {
        $taskNumPreserve[$val] = $j;
        $j++;
    }
    $tasks = Task::getTaskGrouping($taskArray,$taskNumPreserve,$entityFilter);
    
    $attachments = $ticket->getAttachment();
    
    $process_attachment = array();
    $entity_attachment = array();
    foreach ($attachments as $key => $value) {
        if($value['entity_type'] == 'A'){
            $process_attachment[$value['object_id']][] = $attachments[$key];
        }elseif(in_array($value['entity_type'], array('O','M','P'))){
            $entity_attachment[$value['object_id']][] = $attachments[$key];
        }
    }
    $showTaskButton = $cfg->config['showTaskButton']['value'];

    if($entityType=="batch_upload" || 
       $entityType=="batch_update" ){
        $entityType="batch";
       }
    
    ?>
    
    <?php if($showTaskButton && $flag) {?><div><input class="buttons" id="showClosed" type="button" value="<?php echo __('Show Closed Tasks');?>"></div><?php } ?>
    
    <?php
    $staticData = getCachedTicketFields();
    $staticTaskData = getCachedTaskFields();
    foreach ($tasks as $k => $val) {
    foreach ($val['tasks'] as $key => $task) {
    $source = $staticTaskData['Default_Task Sources'][$task->getTaskSource()];
    $currentStatus = $staticTaskData['Default_Task Status'][$task->getTaskStatus()];
    $currentStatus = $currentStatus ? $currentStatus : "New";
    $access = $thisstaff->canAccessALLDept($task->getDeptId());
    $taskDept = $staticFields["dept"][$task->getDeptId()];
    $status = $currentStatus;
    $task_id = $task->getId();
    $type = $task->getTaskType();
    $typeId = $task->getTaskTypeId();
    $entity_id = $task->_answers[$entityType.'_id'];
    //Create
    $create = $task->getcreateStatus();
    $createReturn = $task->getcreateReturnStatus();
    $createRefund = $task->getcreateRefundStatus();
    
    //Close
    $closure = $task->getclosureStatus();
    $closureReturn = $task->getclosureReturnStatus();
    $closureRefund = $task->getclosureRefundStatus();
    $autocommunication = json_decode($cfg->config['autocommunication']['value'],True)[$task->_answers['autocommunication']];
    $CreateAgent = $task->getCreateAgent();
    $CloseAgent = $task->getCloseAgent();
    $resp_ID = $task->getTaskResponse();
    $response = $staticTaskData['Default_Task Response'][$resp_ID];
    $typeID = $staticTaskData['Default_Task Types'][$typeId];
    $taskTypes[$task_id] = $typeID;
    $updatedBy = TasksThread::getlastupdate($task_id);
    
    $title = Format::htmlchars(Format::truncate($task->getTitle(), 40));
    ?>
    <?php
    if($separator != $k) { 
        $separator = $k; ?>
        <br><hr class="<?php if ($showTaskButton && !$task->isOpen()) {?> show_closed <?php }?>"style="height:3px;border:none;color:#333;background-color:#333; <?php if ($showTaskButton && !$task->isOpen()) {?> display:none <?php }?>"><br>
    <?php }else{
                if($showTaskButton){
                        if($task->isOpen()) echo '<br>';
                        else {?><br class="<?php if (!$task->isOpen()) {?> show_closed <?php }?>" style="<?php if (!$task->isOpen()) {?> display:none <?php }?>">
         <?php } }else {echo '<br>';} }?>
                
    <table class="ticket_info <?php if ($showTaskButton && !$task->isOpen()) {?> show_closed <?php }?>" cellspacing="0" cellpadding="0" width="940" border="0" <?php if ($showTaskButton && !$task->isOpen()) {?> style="display:none" <?php }?>>
                            <tr  class='displayClosedDetails'><td colspan='4' style="padding: 10px;"><a href="tasks.php?id=<?php echo $task_id; ?>"><b>Task </b><?php echo'<strong>' . $task->ht['display_number'] . '</strong>'; ?></a><?php echo ' <b>( ' . $taskDept . " : " . $typeID . ' )</b> '; ?> 
                                    <?php if ($task->parent_task_id && $taskTypes[$task->parent_task_id]) { ?> <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Created by ".$taskTypes[$task->parent_task_id].""; }?>
                                <?php if (!$task->isOpen()) { ?> <span  class='displayClosedDetailsArrow'><i class="arrow"></i></span> <?php } ?></td><?php echo $task->isOpen() ? '<td style="text-align:right;" class="close_task" taskId=' . $task_id . ' change="status" value="59-52-Close With Exception-0"><i>x</i></td></tr>' : '' ?>
                            <tr class="hiddenTaskData" style="display:<?php echo $task->isOpen() ? "" : "none"; ?>">
                                <td width="50%">
                                    <table cellspacing="0" cellpadding="4" width="100%" border="0">

                                        <?php if($entityType == 'manifest' && !empty($entity_id)) {?>
                                        <tr>
                                            <th><?php echo __(ucfirst($entityType).' Id'); ?>:</th>
                                            <td><?php echo $entity_id;?></td>
                                        </tr>
                                        <tr>
                                            <th><?php echo __('Order Id'); ?>:</th>
                                            <td><a class="OrderId" href='javascript:void(0)' value="<?php echo $task->_answers['order_id'];?>"><?php echo $task->_answers['order_id'];?></a></td>
                                        </tr>
                                        <?php } else if(!empty($entity_id)){?>
                                        <tr>
                                            <th><?php echo __(ucfirst($entityType).' Id'); ?>:</th>
                                            <?php 
                                            if($entityType=="order"){
                                            ?> 
                                            <td><a class="OrderId" href='javascript:void(0)' value="<?php echo $entity_id;?>"><?php echo $entity_id;?></a></td>
                                            <?php 
                                            }else{
                                                ?>
                                            <td><p class="entityIDD" href='javascript:void(0)' value="<?php echo $entity_id;?>"><?php echo $entity_id;?></p></td>
                                            <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <th><?php echo __('Task Response'); ?>:</th>
                                            <td class='taskResponseBox<?php echo $task_id; ?> existing-info-box'><div class='current-info'><?php $response = (string) $response; echo $response; ?></div> 
                                                <?php if ($access && $task->isOpen() && is_array($taskResponseMachine["" . $typeID . ""]) && count(array_filter($taskResponseMachine["" . $typeID . ""])) > 0) { ?>
                                                    <select class='task-status-selector change-selector' taskId="<?php echo $task_id; ?>" change="response" style='<?php if ($response) { ?> display:none; <?php } ?> width:155px;' data-task-type-id="<?php echo $type;?>">
                                                        <option val='select'>select</option>
                                                        <?php foreach ($taskResponseMachine["" . $typeID . ""] as $r => $v) { ?>
                                                            <option value='<?php echo $TaskResponseFormObj->id . "-" . $r . "-" . $v; ?>'><?php echo $v; ?></option>

                                                        <?php } ?>
                                                    </select>
                                                <?php
                                                } else {
                                                    if (!$response) {
                                                        echo "--";
                                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php echo __('Comments'); ?>:</th>
                                            <td class="taskCommentBox<?php echo $task_id; ?> existing-info-box"><div class='current-info'><?php $comment = (string) $task->getTaskComments();
                                                if(strlen($comment) > 50)
                                            { $commentToDisplay = substr($comment,0,47)."...";?>
                                            <div title="<?php echo $comment;?>"><?php echo $commentToDisplay;?></div><?php  
                                            } else { echo $task->getTaskComments();} ?></div><?php if ($access) { ?><textarea rows='1' cols='10' type='text' taskId='<?php echo $task_id; ?>' fieldId='<?php echo $comment_field_id->id; ?>' class='taskComment' style='<?php if ($comment) { ?> display:none; <?php } ?>' name='taskComment' placeholder='Comment (50 words)'></textarea><?php } else {
                                                    if (!$comment) {
                                                        echo "--";
                                                    }
                                                } ?></td>
                                        </tr>
                                        <tr>
                                        <th><?php echo __('Attachments');?>: </th>
					<td>
<form>
                                    <div id="reply_form_attachments" class="attachments" style="width: 183px;
    font-size: 10px;
    padding-top: 10px;">
                                                <?php
                                                $option = array("attachmentDisplay" => "true", "id" => "reply_form_attachments_task".$task->getId());
                                                print $response_form->getField('attachments')->render($option);
                                                ?>
                                                </div>
                                   <input type="hidden" name="task_id" value="<?php echo $task->getId();?>"/> 

                                          <button type="button" id ="task_upload" onclick="task_update_attachment(this)">Upload</button>
                                        </form></td>
                                        </tr>
                                        <tr>
                                            <th><?php echo __('Assigned To'); ?>:</th>
                    <!--                        <td><?php echo $task->staff ? Format::truncate($task->staff->getName(), 40) : "Unassigned";
                                            echo "<div id='ajaxAssign'>Assign to me</div>"; ?></td> -->
                        
                        <td>
                            <?php
                            //echo "fhjbdf<pre>"; print_r($thisstaff->getId()); echo '</pre>';
                            if($task->isAssigned())
                                echo Format::htmlchars(implode('/', $task->getAssignees()));
                            else
                                echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                            ?>
                            <?php 
                            global $thisstaff;
                            $depIds=$thisstaff->getDepts();
                            if($task->staff_id != $thisstaff->getId() 
                                    && in_array($task->dept_id, $depIds)){ ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;-<a href='' class='assignToMetask' id='<?php echo $task_id; ?>' style='font-size:10px;'>Assign to me</a>
                            <?php } ?>
                        </td>
                        
                        
                    </tr>
                    
                                                          <?php 
                                                           
                $enitityType=$ticket->getEntityType();
                                                          if ($create) {
   
                                            ?>
                                            <tr>
                                                <th><?php echo __('Create Status'); ?>:</th>
                                                <td><?php 
                                                  switch ($enitityType){
                                                        case 'order':
                                                            echo $staticData["orderStatus"]["Order"]["$create"];
                                                            break;
                                                        case 'manifest':
                                                            echo $staticData["orderStatus"]["Order"]["$create"];
                                                            break;
                                                        case 'product':
                                                            echo $staticData["orderStatus"]["Product"]["$create"];
                                                            break;
                                                        case 'batch_upload':
							case 'batch_update':
							case 'batch':
 							    echo $staticData["orderStatus"]["batchUploadStatus"]["$create"];
                                                            break;
                                                    }
                                                ?></td>
                                            </tr>
                                        <?php } ?>
                    <?php if ($source == "Manual" && $CreateAgent) {
        ?>
                                            <tr>
                                                <th><?php echo __('Task Created By'); ?>:</th>
                                                <td><?php echo $staticData["staff"]["$CreateAgent"]; ?></td>
                                            </tr>
                                        <?php } ?>

    <?php if ($updatedBy) {
        ?> 
                                            <tr>
                                                <th><?php echo __('Task Updated By'); ?>:</th>
                                                <td><?php echo $staticData["staff"]["$updatedBy"]; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($closure) {
                                            ?>
                                            <tr>
                                                <th><?php echo __('Closure Status'); ?>:</th>
                                                <td><?php
                                                echo $staticData["orderStatus"]["Order"]["$closure"];
                                                if($staticData["orderStatus"]["Return"]["$closureReturn"])
                                                    echo ", ".$staticData["orderStatus"]["Return"]["$closureReturn"];
                                                if($staticData["orderStatus"]["Refund"]["$closureRefund"])
                                                    echo ", ".$staticData["orderStatus"]["Refund"]["$closureRefund"];
                                                ?></td>

                                            </tr>
    <?php } ?>

    <?php if ($CloseAgent) {
        ?>
                                            <tr> 
                                                <th><?php echo __('Task Closed By'); ?>:</th>
                                                <td><?php echo $staticData["staff"]["$CloseAgent"]; ?></td>
                                            </tr>
    <?php } ?>
                   
                    <!--Attachment for task  -->   
           
                     
                <?php 
                if(!empty($process_attachment[$task_id]))
                {?>
                <tr>
                    <th><?php echo __('Process Owners Attachment ');?>:</th>
                    <td>  <?php 
                    
                        foreach($process_attachment[$task_id] as $A){  $size = '';
                                $size = sprintf('<small class="filesize faded">%s</small>', Format::file_size($A['size'])); if ( strlen(Format::htmlchars($A['name'])) <= 15 ){ $file=Format::htmlchars($A['name']);}else{$file=str_pad(substr(Format::htmlchars($A['name']),0,12),15,".");
                        }  ?>
                        <span class="attachment-info" style="display: inline-table;">
                        <i class="icon-paperclip icon-flip-horizontal"></i>
                        <a class="no-pjax truncate filename" href="<?php echo $A['path'];
                           ?>" target="_blank"><?php echo $file;
                        ?></a>
                        </span><br>
              <?php } ?>
                    </td>
                </tr>
             <?php } ?>  
             
             <?php
                if(!empty($entity_attachment[$entity_id]))
                {?>
                <tr>
                    <th><?php echo __(ucfirst($entityType).' Id Attachment ');?>:</th>
                    <td>  <?php 
                       
                        foreach($entity_attachment[$entity_id] as $A){  $size = '';
                                $size = sprintf('<small class="filesize faded">%s</small>', Format::file_size($A['size'])); if ( strlen(Format::htmlchars($A['name'])) <= 15 ){ $file=Format::htmlchars($A['name']);}else{$file=str_pad(substr(Format::htmlchars($A['name']),0,12),15,".");
                        }  ?>
                        <span class="attachment-info" style="display: inline-table;">
                        <i class="icon-paperclip icon-flip-horizontal"></i>
                        <a class="no-pjax truncate filename" href="<?php echo $A['path'];
                           ?>" target="_blank"><?php echo $file;
                        ?></a>
                        </span><br>
              <?php } ?>
                    </td>
                </tr>
             <?php } ?>
                    
</table>
            </td>
            <td width="50%">
                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                    <tr>
                        <th width="100"><?php echo __('Task Status');?>:</th>
                        <td class='taskStatusBox<?php echo $task_id;?> existing-info-box'><div class='current-info'><?php echo $status; ?></div> 
                            <?php 
                            //_pr($access); die;
                            if($task->isOpen() && is_array($taskStateMachine["".$currentStatus.""]) && count(array_filter($taskStateMachine["".$currentStatus.""]))>0){ ?>
                                <select class='task-status-selector change-selector' taskId="<?php echo $task_id; ?>" change="status" style='display:none;' width='100px'>
                                        <option val='select'>select</option>
                                <?php foreach($taskStateMachine["".$currentStatus.""] as $s){
                                    if($access || $s==8){ //Allow Close with Exception for all ?>
                                        <option value='<?php echo $TaskStatusFormObj->id."-".$taskStatusId[$s]."-".$taskStatus[$s]."-".count(array_filter($taskStateMachine[$taskStatus[$s]]));?>'><?php echo $taskStatus[$s];?></option>

                                    <?php } } ?>
                                </select>
                        <?php } ?>
                        </td>
                    </tr>
               
                    
                    <tr>
                         <th nowrap><?php echo __('Source');?>:</th>
                     <td>
                            <?php echo "<div class='task-list-source'>".$source."</div>"; ?>
                        </td>
                    
                    </tr>        
                 <tr>
                        <th nowrap><?php echo __('Created');?>:</th>
                        <td><?php echo Format::datetime($task->getCreateDate())." (".  Format::relativeTime(Misc::db2gmtime($task->getCreateDate())).")";?></td>
                    </tr>
                    <tr>
                        <th><?php echo __('Updated on');?>:</th>
                        <td><?php echo Format::datetime($task->updated); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo __('Auto Communication ');?>:</th>
                        <td><?php echo $autocommunication ?></td>
                    </tr>
                    <tr>
                        <th nowrap><?php echo __('Promised SLA');?>:</th>
                         <td>
                             <div class="current-info promisedSLA<?php echo $task_id;?>"><?php echo Format::datetime($task->getPromisedDueDate()); ?></div>
                         </td>
                    </tr>
                 
                    <tr>
                        <th nowrap><?php echo __('Updated SLA');?>:</th>
                        <td class="taskDuedateBox<?php echo $task_id;?> existing-info-box"><div class="current-info">
                            <?php if($task->getDueDate()!=$task->getPromisedDueDate()){ echo Format::datetime($task->getDueDate());}else{echo "Not Changed";}if($task->isOpen() && $access){ ?>
                                <i class="icon-edit"></i></div>
                            <input type='text' taskId='<?php echo $task_id;?>' class='taskDueDate datetimepicker' style='display:none;'  name='taskDueDate' >
                        <?php }else{ ?></div><?php } ?></td>
                    </tr>
<?php if (!empty($task->_answers['customer_comments'])) {
?>
                                            <tr>
                                                <th><?php echo __('Merchant Comments'); ?>:</th>
                                                <td><?php echo $task->_answers['customer_comments']; ?></td>
                                            </tr>
                                        <?php } ?>
                    <!-- 1800 = 30 min is added to ceil the time -->
                    <tr>
                        <?php if($task->isOpen()){ ?>
                        <th nowrap><?php echo __('Task Timer');?>:</th>
                        <td><?php $timeDiff = floor((strtotime($task->getDueDate())-time()+1800)/(3600*24));
                        
                            if($timeDiff>1){$timeDiffWords="Respond in ".$timeDiff." days";}
                            elseif($timeDiff<=1 && $timeDiff>=0){$timeDiffWords="Respond in ".floor((strtotime($task->getDueDate())-time()+1800)/3600)."hours";}
                            else{$timeDiffWords="Response Delayed";}
                            echo $timeDiffWords;
                            ?>
                        </td>
                            
                         <?php }else{ ?>
                             <th nowrap><?php echo __('Closed');?>:</th>
                            <td><?php 
                                 if($task->closed)
                                        $timeDiffWords = Format::datetime($task->closed);
                                 else
                                       $timeDiffWords = "Responded";
                                echo $timeDiffWords;
                                ?>
                            </td>
                       <?php
                      
                        }
                        
                        if(!$suggestedTime || ($suggestedTime && $suggestedTime<strtotime($task->getDueDate()))){
                            $suggestedTime = strtotime($task->getDueDate());
                        }
                        ?>
                        
                    </tr>


                   
                        
                        </table>

            </td>
        </tr>
    </table>
   <?php } } ?>
    <!-- Task Details Ends -->
    
    <!-- Removed Code for custom fields -->
    </div>
    </td>
    
</tr>

<div id="ticketData" class="tab_panel container_section">
<div class="clear"></div>
<h2><?php echo Format::htmlchars($ticket->getHelpTopic()); ?></h2>
<?php
$tcount = $ticket->getThreadEntries($types)->count();
?>
<ul class="tabs clean threads" id="ticket_tabs" >
    <li class="active"><a href="#ticket_thread"><?php echo sprintf(__('Ticket Thread (%d)'), $tcount); ?></a></li>
    <li   style="display:none;"><a id="ticket-tasks-tab" href="#tasks"
            data-url="<?php
        echo sprintf('#tickets/%d/tasks', $ticket->getId()); ?>"><?php
        echo __('Tasks');
        if ($ticket->getNumTasks())
            echo sprintf('&nbsp;(%d)', $ticket->getNumTasks());
        ?></a></li>
</ul>

<!-- ticket widget - Akash Kumar -->
<div id="ticket_details_container"  class="widget customWidget">
    <?php 
     $configWidget = json_decode($cfg->config['customWidgets']["value"],true);
    if($configWidget["ticket"]["basic"]){ ?>
    <table>
        <tr><td class="flight" colspan='2'><?php echo Format::datetime($ticket->getCreateDate()); ?></td></tr>
        <tr><td  class="helpATopic" colspan='2'><?php echo Format::htmlchars($ticket->getHelpTopic()); ?></td></tr>
        <tr><span class='widgetImportant widgetPriority' style="display:none;"><?php echo $ticket->getPriority();?></span><td colspan='2' class='widgetImportant'><?php echo ($S = $ticket->getStatus()) ? $S->getLocalName() : ''; ?></td></tr>
        <tr><td colspan='2'><?php echo Format::htmlchars($ticket->getDeptName()); ?></td></tr>
        <tr><td colspan='2'><?php
                        if($ticket->isAssigned())
                            echo substr (Format::htmlchars(implode('/', $ticket->getAssignees())),0,15);
                        else
                            echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                        ?></td></tr>
        <tr><td></td><td></td></tr>
        <tr><td class="flight" colspan='2'><?php echo Format::datetime($ticket->getLastRespDate()); ?></td></tr>
    </table>
    <?php }?>
</div>
<!-- order widget - Akash Kumar -->
<!-- order_detaails_romove_here -->
<!-- order_detaails_end -->
<div id="ticket_tabs_container">
<div id="ticket_thread" class="tab_content">
<?php
    // Render ticket thread
    $ticket->getThread()->render(
            array('M', 'R', 'N'),
            array(
                'html-id' => 'ticketThread',
                'mode' => Thread::MODE_STAFF)
            );
?>
<div class="clear"></div>

<div class="sticky bar stop actions text_pluging_bar" id="response_options">
    <ul class="tabs">
        <?php
        if ($role->hasPerm(TicketModel::PERM_REPLY)) { ?>
        <li class="active"><a href="#reply"><?php echo __('Post Reply');?></a></li>
        <?php
        } ?>
        <li><a href="#note"><?php echo __('Post Internal Note');?></a></li>
        <?php if ($role->hasPerm(TicketModel::PERM_CUSTOMER_BEHALF)) { ?>
        <li><a href="#customernote"><?php echo __('Post On Merchant Behalf');?></a></li>
        <?php
        } ?>
        <?php
        if ($role->hasPerm(TicketModel::PERM_TRANSFER)) { ?>
        <li><a href="#transfer"><?php echo __('Department Transfer');?></a></li>
        <?php
        }

        if ($role->hasPerm(TicketModel::PERM_ASSIGN)) { ?>
        <li><a href="#assign"><?php
            echo $ticket->isAssigned()?__('Reassign Ticket'):__('Assign Ticket'); ?></a></li>
        <?php
        } ?>
    </ul>
    <?php
    if ($role->hasPerm(TicketModel::PERM_REPLY)) { ?>
    <form id="reply" class="tab_content spellcheck plugin_bar" action="tickets.php?id=<?php
        echo $ticket->getId(); ?>" name="reply" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="orderid" value="<?php echo $ticket->getOrder(); ?>">
        <input type="hidden" name="msgId" value="<?php echo $msgId; ?>">
        <input type="hidden" name="a" value="reply">
        <input type="hidden" name="lockCode" value="<?php echo ($mylock) ? $mylock->getCode() : ''; ?>">
        <span class="error"></span>
        <table style="width:100%" border="0" cellspacing="0" cellpadding="3">
           <tbody id="to_sec">
            <tr>
                <td>
                    <label><strong><?php echo __('To'); ?>:</strong></label>
                </td>
                <td>
                    <?php
                    # XXX: Add user-to-name and user-to-email HTML ID#s
                    $to =sprintf('%s &lt;%s&gt;',
                            Format::htmlchars($ticket->getName()),
                            $ticket->getReplyToEmail());
                    $emailReply = (!isset($info['emailreply']) || $info['emailreply']);
                    ?>
                    <select id="emailreply" name="emailreply">
                        <option value="1" <?php echo $emailReply ?  'selected="selected"' : ''; ?>><?php echo $to; ?></option>
                        <option value="0" <?php echo !$emailReply ? 'selected="selected"' : ''; ?>
                        >&mdash; <?php echo __('Do Not Email Reply'); ?> &mdash;</option>
                    </select>
                </td>
            </tr>
            </tbody>
            <?php
            if(1) { //Make CC optional feature? NO, for now.
                ?>
            <tbody id="cc_sec"
                style="display:<?php echo $emailReply?  'table-row-group':'none'; ?>;">
             <tr>
                <td width="120">
                    <label><strong><?php echo __('Collaborators'); ?>:</strong></label>
                </td>
                <td>
                    <input type='checkbox' value='1' name="emailcollab" id="emailcollab"
                        <?php echo ((!$info['emailcollab'] && !$errors) || isset($info['emailcollab']))?'checked="checked"':''; ?>
                        style="display:<?php echo $ticket->getThread()->getNumCollaborators() ? 'inline-block': 'none'; ?>;"
                        >
                    <?php
                    $recipients = __('Add Recipients');
                    if ($ticket->getThread()->getNumCollaborators())
                        $recipients = sprintf(__('Recipients (%d of %d)'),
                                $ticket->getThread()->getNumActiveCollaborators(),
                                $ticket->getThread()->getNumCollaborators());

                    echo sprintf('<span class="add_recipients"><a class="collaborators preview"
                            href="#thread/%d/collaborators"><span id="t%d-recipients">%s</span></a></span>',
                            $ticket->getThreadId(),
                            $ticket->getThreadId(),
                            $recipients);
                   ?>
                </td>
             </tr>
            </tbody>
            <?php
            } ?>
            <tbody id="resp_sec">
            <?php
            if($errors['response']) {?>
            <tr><td width="120">&nbsp;</td><td class="error"><?php echo $errors['response']; ?>&nbsp;</td></tr>
            <?php
            }?>
            <tr>
                <td>
                    <label><strong><?php echo __('Mandatory Ids'); ?>:</strong></label>
                </td>
                <td>
                    <select style="height: 24px;width: 181px;margin-left: 16px;border: 1px solid #656d71;border: 1px solid #656d71;/* margin-left: 4px; */" 
                            class="select2 multiplefield select2-hidden-accessible" id="entityReply" name="entityReply[]" multiple placeholder="Select Entity Type" >
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $("#entityReply").select2();
                        });
                    </script>
                    <?php
                    $entityIds = explode(',', $ticket->getEntities($entityType));
                    asort($entityIds);
                    foreach($entityIds as $key => $value) {?>
                            <option value="<?php echo $value;?>"> <?php echo $value;?></option>
                    <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Response');?>:</strong></label>
                </td>
                <td>
<?php if ($cfg->isCannedResponseEnabled()) { ?>
                    <select id="departmentforcannedResp" name="departmentforcannedResp">
                        <option value="0" selected="selected"><?php echo __('Select Department');?></option>
                        <?php
                       
                        $departments=$staticData['dept'];
                        asort($departments);
                        foreach($departments as $key => $value)
                        {
                            echo "<option value='".$key."'>".$value."</option>";
                        } ?>
                    </select>
                    <select id="cannedResp" name="cannedResp" style="width: 300px; display:none;">
                        
                    </select>
                    <br>
<?php } # endif (canned-resonse-enabled)
                    $signature = '';
                    switch ($thisstaff->getDefaultSignatureType()) {
                    case 'dept':
                        if ($dept && $dept->canAppendSignature())
                           $signature = $dept->getSignature();
                       break;
                    case 'mine':
                        $signature = $thisstaff->getSignature();
                        break;
                    } ?>
                    <input type="hidden" name="draft_id" value=""/>
                    <textarea name="response" id="response" cols="50"
                        data-signature-field="signature" data-dept-id="<?php echo $dept->getId(); ?>"
                        data-signature="<?php
                            echo Format::htmlchars(Format::viewableImages($signature)); ?>"
                        placeholder="<?php echo __(
                        'Start writing your response here. Use canned responses from the drop-down above'
                        ); ?>"
                        rows="9" wrap="soft"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> draft draft-delete" <?php
    list($draft, $attrs) = Draft::getDraftAndDataAttrs('ticket.response', $ticket->getId(), $info['response']);
    echo $attrs; ?>><?php echo $draft ?: $info['response'];
                    ?></textarea>
                <div id="reply_form_attachments" class="attachments">
                <?php
                    $option = array("attachmentDisplay"=>"true","id"=>"reply_form_attachments");
                    print $response_form->getField('attachments')->render($option);
                ?>
                </div>
                </td>
            </tr>
            <tr>
                <td width="120">
                    <label for="signature" class="left"><?php echo __('Signature');?>:</label>
                </td>
                <td>
                    <?php
                    $info['signature']=$info['signature']?$info['signature']:$thisstaff->getDefaultSignatureType();
                    ?>
                    <label><input type="radio" name="signature" value="none" checked="checked"> <?php echo __('None');?></label>
                    <?php
                    if($thisstaff->getSignature()) {?>
                    <label><input type="radio" name="signature" value="mine"
                        <?php echo ($info['signature']=='mine')?'checked="checked"':''; ?>> <?php echo __('My Signature');?></label>
                    <?php
                    } ?>
                    <?php
                    if($dept && $dept->canAppendSignature()) { ?>
                    <label><input type="radio" name="signature" value="dept"
                        <?php echo ($info['signature']=='dept')?'checked="checked"':''; ?>>
                        <?php echo sprintf(__('Department Signature (%s)'), Format::htmlchars($dept->getName())); ?></label>
                    <?php
                    } ?>
                </td>
            </tr>
            <tr style="display: none;">
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Ticket Status');?>:</strong></label>
                </td>
                <td>
                    <?php
                    if ($outstanding = $ticket->getMissingRequiredFields()) { ?>
                    <div class="warning-banner"><?php echo sprintf(__(
                        'This ticket is missing data on %s one or more required fields %s and cannot be closed'),
                        "<a href=\"tickets.php?id={$ticket->getId()}&a=edit\">",
                        '</a>'
                    ); ?></div>
<?php               } ?>

                    <select name="reply_status_id">
                    <?php
		
                    $statusId = $info['reply_status_id'] ?: $ticket->getStatusId();
		    $states = array('open','solved');
                    if ($role->hasPerm(TicketModel::PERM_CLOSE) && !$outstanding)
                        $states = array_merge($states, array('closed'));

                    foreach (TicketStatusList::getStatuses(
                                array('states' => $states)) as $s) {
                        if (!$s->isEnabled()) continue;
                        $selected = ($statusId == $s->getId());
                        echo sprintf('<option value="%d" %s>%s%s</option>',
                                $s->getId(),
                                $selected
                                 ? 'selected="selected"' : '',
                                __($s->getName()),
                                $selected
                                ? (' ('.__('current').')') : ''
                                );
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <?php
            
                $suggestedTime = strtotime(SLA::adjustHolidays(date('Y-m-d H:i:s',$suggestedTime)));
                $diffSuggested = ($suggestedTime-time())/3600*24;
                if($diffSuggested>7){
                    $diffSuggestedDays = 3;
                }else{
                    $diffSuggestedDays = 2;
                }
                $suggestedTime = time()+$diffSuggestedDays*24*3600;
            ?>
            <tr>
                <td colspan="2">
                    <table><tr>
                    <td width="20%" style="vertical-align:top">
                        <label><strong><?php echo __('Next Customer Communication');?>:</strong></label>
                    </td>
                    <td width="25%">
                        <input class="datetimepicker" name='nextCustomerUpdate' type="text" value='<?php echo date('Y/m/d H:i',$suggestedTime);?>'>
                    </td>
                    <td width="20%" style="vertical-align:top">
                        <label><strong><?php echo __('Suggested Communication');?>:</strong></label>
                    </td>
                    <td width="25%">
                        <?php
                        echo Format::datetime(date("Y/m/d H:i",$suggestedTime))."<br>(".$diffSuggestedDays." days)"; ?>
                    </td>
                        </tr></table>
                </td>
            </tr>
         </tbody>
        </table>
        <p style="padding-left:116px; padding-top: 20px;" >
            <input class="save pending buttons" type="submit" value="<?php echo __('Submit Reply');?>">
            <input class="buttons" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
    <?php if ($role->hasPerm(TicketModel::PERM_CUSTOMER_BEHALF)) { ?>
        <form id="customernote" class="hidden tab_content spellcheck plugin_bar" action="tickets.php?id=<?php
        echo $ticket->getId(); ?>#customernote" name="note" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime() * 60; ?>">
        <input type="hidden" name="a" value="customernote">
        <input type="hidden" name="behalf" value="customer">
        <input type="hidden" name="lockCode" value="<?php echo ($mylock) ? $mylock->getCode() : ''; ?>">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['customernote']) {?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['customernote']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Customer Statement'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <div>
                        <div class="faded" style="padding-left:0.15em"><?php
                        echo __('Customer Note title - summary of the note (optional)'); ?></div>
                        <input type="text" name="title" id="title" size="60" value="<?php echo $info['title']; ?>" >
                        <br/>
                        <span class="error">&nbsp;<?php echo $errors['title']; ?></span>
                    </div>
                    <br/>
                    <div class="error"><?php echo $errors['note']; ?></div>
                    <textarea name="note" id="internal_note" cols="80"
                        placeholder="<?php echo __('Note details'); ?>"
                        rows="9" wrap="soft"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> draft draft-delete" <?php
    list($draft, $attrs) = Draft::getDraftAndDataAttrs('ticket.note', $ticket->getId(), $info['note']);
    echo $attrs; ?>><?php echo $draft ?: $info['note'];
                        ?></textarea>
                <div class="attachments" id="customernoteAttachment">
                <?php
                    $option = array("attachmentDisplay"=>"true","id"=>"customernoteAttachment");
                    print $note_form->getField('attachments')->render($option);
                ?>
                </div>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr style='display:none;'>
                <td width="120">
                    <label><?php echo __('Ticket Status');?>:</label>
                </td>
                <td>
                    <div class="faded"></div>
                    <select name="note_status_id">
                        <?php
                        $statusId = $info['note_status_id'] ?: $ticket->getStatusId();
                        $states = array('open');
                        if ($role->hasPerm(TicketModel::PERM_CLOSE))
                            $states = array_merge($states, array('closed'));
                        foreach (TicketStatusList::getStatuses(
                                    array('states' => $states)) as $s) {
                            if (!$s->isEnabled()) continue;
                            $selected = $statusId == $s->getId();
                            echo sprintf('<option value="%d" %s>%s%s</option>',
                                    $s->getId(),
                                    $selected ? 'selected="selected"' : '',
                                    __($s->getName()),
                                    $selected ? (' ('.__('current').')') : ''
                                    );
                        }
                        ?>
                    </select>
                    &nbsp;<span class='error'>*&nbsp;<?php echo $errors['note_status_id']; ?></span>
                </td>
            </tr>
        </table>

       <p  style="padding-left:165px;">
           <input class="save pending buttons" type="submit" value="<?php echo __('Post Note');?>">
           <input class="buttons" type="reset" value="<?php echo __('Reset');?>">
       </p>
   </form>
    <?php
    }    ?>
    <form id="note" class="hidden tab_content spellcheck plugin_bar" action="tickets.php?id=<?php
        echo $ticket->getId(); ?>#note" name="note" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime() * 60; ?>">
        <input type="hidden" name="a" value="postnote">
        <input type="hidden" name="lockCode" value="<?php echo ($mylock) ? $mylock->getCode() : ''; ?>">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['postnote']) {?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['postnote']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Internal Note'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <div>
                        <div class="faded" style="padding-left:0.15em"><?php
                        echo __('Note title - summary of the note (optional)'); ?></div>
                        <input type="text" name="title" id="title" size="60" value="<?php echo $info['title']; ?>" >
                        <br/>
                        <span class="error">&nbsp;<?php echo $errors['title']; ?></span>
                    </div>
                    <br/>
                    <div class="error"><?php echo $errors['note']; ?></div>
                    <textarea name="note" id="internal_note" cols="80"
                        placeholder="<?php echo __('Note details'); ?>"
                        rows="9" wrap="soft"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> draft draft-delete" <?php
    list($draft, $attrs) = Draft::getDraftAndDataAttrs('ticket.note', $ticket->getId(), $info['note']);
    echo $attrs; ?>><?php echo $draft ?: $info['note'];
                        ?></textarea>
                <div class="attachments" id="noteAttachment">
                <?php
                    $option = array("attachmentDisplay"=>"true","id"=>"noteAttachment");
                    print $note_form->getField('attachments')->render($option);
                ?>
                </div>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr style="display:none;">
                <td width="120">
                    <label><?php echo __('Ticket Status');?>:</label>
                </td>
                <td>
                    <div class="faded"></div>
                    <select name="note_status_id">
                        <?php
                        $statusId = $info['note_status_id'] ?: $ticket->getStatusId();
                        $states = array('open');
                        if ($role->hasPerm(TicketModel::PERM_CLOSE))
                            $states = array_merge($states, array('closed'));
                        foreach (TicketStatusList::getStatuses(
                                    array('states' => $states)) as $s) {
                            if (!$s->isEnabled()) continue;
                            $selected = $statusId == $s->getId();
                            echo sprintf('<option value="%d" %s>%s%s</option>',
                                    $s->getId(),
                                    $selected ? 'selected="selected"' : '',
                                    __($s->getName()),
                                    $selected ? (' ('.__('current').')') : ''
                                    );
                        }
                        ?>
                    </select>
                    &nbsp;<span class='error'>*&nbsp;<?php echo $errors['note_status_id']; ?></span>
                </td>
            </tr>
        </table>

       <p  style="padding-left:165px;">
           <input class="save pending buttons" type="submit" value="<?php echo __('Post Note');?>">
           <input class="buttons" type="reset" value="<?php echo __('Reset');?>">
       </p>
   </form>
    <?php
    if ($role->hasPerm(TicketModel::PERM_TRANSFER)) { ?>
    <form id="transfer" class="hidden tab_content spellcheck plugin_bar" action="tickets.php?id=<?php
        echo $ticket->getId(); ?>#transfer" name="transfer" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="ticket_id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="transfer">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['transfer']) {
                ?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['transfer']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120">
                    <label for="deptId"><strong><?php echo __('Department');?>:</strong></label>
                </td>
                <td>
                    <?php
                        echo sprintf('<span class="faded">'.__('Ticket is currently in <b>%s</b> department.').'</span>', $ticket->getDeptName());
                    ?>
                    <br>
                    <select id="deptId" name="deptId" data-quick-add="department">
                        <option value="0" selected="selected">&mdash; <?php echo __('Select Target Department');?> &mdash;</option>
                        <?php
                        if($depts=Dept::getDepartments()) {
                            foreach($depts as $id =>$name) {
                                if($id==$ticket->getDeptId()) continue;
                                echo sprintf('<option value="%d" %s>%s</option>',
                                        $id, ($info['deptId']==$id)?'selected="selected"':'',$name);
                            }
                        }
                        ?>
                        <option value="0" data-quick-add>- <?php echo __('Add New'); ?> -</option>
                    </select>&nbsp;<span class='error'>*&nbsp;<?php echo $errors['deptId']; ?></span>
                </td>
            </tr>
            <tr>
                <td width="120">
                    <label for="priority"><strong><?php echo __('Priority');?>:</strong></label>
                </td>
                <td>
                    <select name="ticketPriority">
                        <option value="<?php echo $cfg->getDefaultPriorityId(); ?>" selected="selected">&mdash; <?php echo __('Default Priority (').$staticFields['priority'][$cfg->getDefaultPriorityId()]['name'].")";?> &mdash;</option>
                        <?php
                        foreach ($staticFields['priority'] as $key => $value) {
                            if($key!=$cfg->getDefaultPriorityId()){
                                ?>
                                <option value="<?php echo $key ?>"><?php echo $value['name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Comments'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <textarea name="transfer_comments" id="transfer_comments"
                        placeholder="<?php echo __('Enter reasons for the transfer'); ?>"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> no-bar" cols="80" rows="7" wrap="soft"><?php
                        echo $info['transfer_comments']; ?></textarea>
                    <span class="error"><?php echo $errors['transfer_comments']; ?></span>
                </td>
            </tr>
        </table>
        <p style="padding-left:165px;">
           <input class="save pending buttons" type="submit" value="<?php echo __('Transfer');?>">
           <input class="buttons" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
    <?php
    if ($role->hasPerm(TicketModel::PERM_ASSIGN)) { ?>
    <form id="assign" class="hidden tab_content spellcheck plugin_bar" action="tickets.php?id=<?php
         echo $ticket->getId(); ?>#assign" name="assign" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="assign">
        <table style="width:100%" border="0" cellspacing="0" cellpadding="3">

            <?php
            if($errors['assign']) {
                ?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['assign']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label for="assignId"><strong><?php echo __('Assignee');?>:</strong></label>
                </td>
                <td>
                    <select id="assignId" name="assignId">
                        <option value="0" selected="selected">&mdash; <?php echo __('Select an Agent OR a Team');?> &mdash;</option>
                        <?php
                        if ($ticket->isOpen()
                                && !$ticket->isAssigned()
                                && $ticket->getDept()->isMember($thisstaff))
                            echo sprintf('<option value="%d">'.__('Claim Ticket (comments optional)').'</option>', $thisstaff->getId());

                        $sid=$tid=0;

                        if ($dept->assignMembersOnly())
                            $users = $dept->getAvailableMembers();
                        else
                            $users = Staff::getAvailableStaffMembers();

                        if ($users) {
                            echo '<OPTGROUP label="'.sprintf(__('Agents (%d)'), count($users)).'">';
                            $staffId=$ticket->isAssigned()?$ticket->getStaffId():0;
                            foreach($users as $id => $name) {
                                if($staffId && $staffId==$id)
                                    continue;

                                if (!is_object($name))
                                    $name = new AgentsName($name);

                                $k="s$id";
                                echo sprintf('<option value="%s" %s>%s</option>',
                                        $k,(($info['assignId']==$k)?'selected="selected"':''), $name);
                            }
                            echo '</OPTGROUP>';
                        }

                        if(($teams=Team::getActiveTeams())) {
                            echo '<OPTGROUP label="'.sprintf(__('Teams (%d)'), count($teams)).'">';
                            $teamId=(!$sid && $ticket->isAssigned())?$ticket->getTeamId():0;
                            foreach($teams as $id => $name) {
                                if($teamId && $teamId==$id)
                                    continue;

                                $k="t$id";
                                echo sprintf('<option value="%s" %s>%s</option>',
                                        $k,(($info['assignId']==$k)?'selected="selected"':''),$name);
                            }
                            echo '</OPTGROUP>';
                        }
                        ?>
                    </select>&nbsp;<span class='error'>*&nbsp;<?php echo $errors['assignId']; ?></span>
                    <?php
                    if ($ticket->isAssigned() && $ticket->isOpen()) { ?>
                        <div class="faded"><?php echo sprintf(__('Ticket is currently assigned to %s'),
                            sprintf('<b>%s</b>', $ticket->getAssignee())); ?></div> <?php
                    } elseif ($ticket->isClosed()) { ?>
                        <div class="faded"><?php echo __('Assigning a closed ticket will <b>reopen</b> it!'); ?></div>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Comments');?>:</strong><span class='error'>&nbsp;</span></label>
                </td>
                <td>
                    <textarea name="assign_comments" id="assign_comments"
                        cols="80" rows="7" wrap="soft"
                        placeholder="<?php echo __('Enter reasons for the assignment or instructions for assignee'); ?>"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> no-bar"><?php echo $info['assign_comments']; ?></textarea>
                    <span class="error"><?php echo $errors['assign_comments']; ?></span><br>
                </td>
            </tr>
        </table>
        <p  style="padding-left:165px;">
            <input class="save pending buttons" type="submit" value="<?php echo $ticket->isAssigned()?__('Reassign'):__('Assign'); ?>">
            <input class="buttons" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
 </div>
 </div>
</div>
<div style="display:none;" class="dialog" id="print-options">
    <h3><?php echo __('Ticket Print Options');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>"
        method="post" id="print-form" name="print-form" target="_blank">
        <?php csrf_token(); ?>
        <input type="hidden" name="a" value="print">
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <fieldset class="notes">
            <label class="fixed-size" for="notes"><?php echo __('Print Notes');?>:</label>
            <label class="inline checkbox">
            <input type="checkbox" id="notes" name="notes" value="1"> <?php echo __('Print <b>Internal</b> Notes/Comments');?>
            </label>
        </fieldset>
        <fieldset>
            <label class="fixed-size" for="psize"><?php echo __('Paper Size');?>:</label>
            <select id="psize" name="psize">
                <option value="">&mdash; <?php echo __('Select Print Paper Size');?> &mdash;</option>
                <?php
                  $psize =$_SESSION['PAPER_SIZE']?$_SESSION['PAPER_SIZE']:$thisstaff->getDefaultPaperSize();
                  foreach(Export::$paper_sizes as $v) {
                      echo sprintf('<option value="%s" %s>%s</option>',
                                $v,($psize==$v)?'selected="selected"':'', __($v));
                  }
                ?>
            </select>
        </fieldset>
        <hr style="margin-top:3em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="reset" value="<?php echo __('Reset');?>">
                <input type="button" value="<?php echo __('Cancel');?>" class="close">
            </span>
            <span class="buttons pull-right">
                <input type="submit" value="<?php echo __('Print');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="claim-confirm">
        <?php echo __('Are you sure you want to <b>claim</b> (self assign) this ticket?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="answered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>answered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unanswered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>unanswered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="overdue-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <font color="red"><b>overdue</b></font>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="banemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>ban</b> %s?'), $ticket->getEmail());?> <br><br>
        <?php echo __('New tickets from the email address will be automatically rejected.');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unbanemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>remove</b> %s from ban list?'), $ticket->getEmail()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="release-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>unassign</b> ticket from <b>%s</b>?'), $ticket->getAssigned()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="changeuser-confirm">
        <span id="msg_warning" style="display:block;vertical-align:top">
        <?php echo sprintf(Format::htmlchars(__('%s <%s> will longer have access to the ticket')),
            '<b>'.Format::htmlchars($ticket->getName()).'</b>', Format::htmlchars($ticket->getEmail())); ?>
        </span>
        <?php echo sprintf(__('Are you sure you want to <b>change</b> ticket owner to %s?'),
            '<b><span id="newuser">this guy</span></b>'); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo __('Are you sure you want to DELETE this ticket?');?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered, including any associated attachments.');?>
    </p>
    <div><?php echo __('Please confirm to continue.');?></div>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>" method="post" id="confirm-form" name="confirm-form">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="process">
        <input type="hidden" name="do" id="action" value="">
        <hr style="margin-top:1em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="button" value="<?php echo __('Cancel');?>" class="close">
            </span>
            <span class="buttons pull-right">
                <input type="submit" value="<?php echo __('OK');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
</div>
</div>
    <div class="grid2 float_left">
         <?php
        getUserHistory($ticket->getOwnerId(),$ticket->getOrder(),$ticket->getNumber());
        ?>
        <!-- order_details_add_here-->
        <div id="order_details_container" class="customWidget order_details_container">
    
        </div>
        <!-- order_details_add_end_here-->
       <!-- ticketRightSideBar_add_here -->
       <div id="ticketRightSideBar" class="ticket_right_sidebar">
    <?php
    if($ticketTriagedData = $ticket->getTriagedData()) {
     
        
        
        
        
        
         if($ticket->getState()=="open" && ($thisstaff->hasPerm(TaskModel::PERM_CREATE))){
             ?>
           <div class='widgetHead'>Create Task</div>
           <center  id="taskLoader" style="
    background-color: #fff;
    padding: 10px;
    font-size: 12px;
    display:none
"><img style="margin-top: 100px; margin-bottom: 100px; display: inline;" src="./images/FhHRx-Spinner.gif" >
           </center>
    <div class="widget ticket_form" id="taskCreator">
        <table>
            <tr>
                <td>Department</td>
                <td>
                    <select  id='taskDepartment' name="taskDepartment">
                              <?php 
                     echo "<option value=''>Select</option>";
               foreach(Dept::objects() as $id=>$value){
                    echo "<option value='".$value->id."'>".$value->getFullName()."</option>";
                } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Task Type</td>
                <td>
                    <select  id='taskType' name="taskType" style='width:115px;'>
                              <?php 
                    echo "<option value=''>Select</option>";
                     ?>
                    </select>
                </td>
            </tr>
              
                <input type="hidden" name="mandateType" value="<?php echo $entityType; ?>">
                    <?php
                     
                $entityIds=$ticket->getEntities($entityType);
                $defaultTextToBeShownOnTask=  ucfirst($entityType)." ids";
                
             ?>
          <?php 
          if(!$entityType){?>
            <tr>
                <td>Mandatory Info Ticket</td>
                <td>
                    <select  id='man-info' name="mandateType" style='width:115px;'>
                              <?php 
                                                   
                             $mandatoryInfo=$ticket->getMandatoryInfoInWhichTicketCanBeMade();
                           ?>
                               <option value="0" >--Select--</option><?php
                             foreach ($mandatoryInfo as $mandateId=>$mandate){
                                 ?>
                                <option value="<?php echo $mandate?>" ><?php echo $mandate ?></option>
                           <?php  }
                           
                     ?>
                    </select>
                </td>
            </tr>
          <?php }?>
             <tr>
                <td id="mandateIDsText"><?php if($entityType){ echo $defaultTextToBeShownOnTask;}
                else{
                    echo "Mandatory Info Ids";
                    
                } ?></td>
                <td>
                    
                              <?php 
    ?>
                                <input name="mandateID" style="
                                                width: 115px;
                                                height: 30px;
                                            " placeholder="comma seperated entries">
      <?php                     
                     ?>
                    
                </td>
            </tr>
           
            <tr style="display:none;">
                <td>Task Status</td>
                <td><select  id='taskStatusCreation' name="Task_status">
                <?php              
                $staticTaskFields = getCachedTaskFields();
                foreach($staticTaskFields["Default_Task Status"] as $id=>$value){
                    if(in_array($value,array("New","Response Pending"))){
                        echo "<option value='".$staticTaskFields["id_extra_Task Status"][$id]."-".$id."-".$value."'>".$value."</option>";
                    }
                } ?>
                    </select>
                    <input type="hidden" value="<?php echo $id;?>" name='ticketId'>
                </td>
            </tr>
             
                <td colspan="2"><input name="submit" id='createTask' type="button" value="Create Task">
                </td>
            </tr>
            
        </table>
    </div>
        
    <?php }    
        
        
        
        
        
        
        if ($ticket->getState()=="open" && ($thisstaff->hasPerm(User::PERM_READ_COMMENT) && $allowedToCreateTaskAndTriage)){
          ?>
           <div class='widgetHead'>Triage Form</div>
    <div class="widget ticket_form" id="widgetTriage">
        <table>
            <tr>
                <td>Priority Flag</td>
                <td><select id='priorityFlag' name="priority" style="width:120px;">
                        <?php 
                      
                foreach($ticketTriagedData["priority"] as $id=>$value){
                    $selected = ($id==$ticketTriagedData["priority_selected"])?"selected":"";
                    echo "<option value='".$id."' $selected>".$value."</option>";
                }
                 ?>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>Communication</td>
                <td><select id='commFlag' name="communication">
                        <?php 
                       
               foreach($ticketTriagedData['communication'] as $id=>$value){
                      $selected = ($id==$ticketTriagedData["communication_selected"])?"selected":"";
                    echo "<option value='".$id."' $selected>".$value."</option>";
                } ?>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>Call Customer</td>
                <td>
                    <select id='callCustomerFlag' name="call_customer">
                      <?php 
               foreach($ticketTriagedData['call_customer'] as $id=>$value){
                      $selected = ($id==$ticketTriagedData["call_customer_selected"])?"selected":"";
                    echo "<option value='".$id."' $selected>".$value."</option>";
                } ?>
                    </select>
                </td>
            </tr>
            
            <tr style='display:none;'>
                <td>Triage Status</td>
                <td><select id='TriageStatusFlag' name="triage">
                               <?php 
               foreach($ticketTriagedData['triage'] as $id=>$value){
                     $selected = ($id==$ticketTriagedData["triage_selected"])?"selected":"";
                    echo "<option value='".$id."' $selected>".$value."</option>";
                } ?>
                    </select>
                </td>
            </tr>
            
            <tr style='display:none;'>
                <td>Promised SLA</td>
                <td><select id='slaFlag' name="sla"  style='width:160px'>
                           <?php 
               foreach($ticketTriagedData['sla'] as $id=>$value){
                    $selected = ($id==$ticketTriagedData["sla_selected"])?"selected":"";
                    echo "<option value='".$id."' $selected>".$value."</option>";
                } ?>
                    </select>
                </td>
            </tr>
            
            <tr style='display:none;'>
                <td>Customer Update Status</td>
                <td>
                    <select id='customerUpdateStatusFlag' name="customer_update_status">
                              <?php 
               foreach($ticketTriagedData['customerUpdate'] as $id=>$value){
                    $selected = ($id==4)?"selected":""; //4 for Triage
                    echo "<option value='".$id."' $selected>".$value."</option>";
                } ?>
                    </select>
                </td>
            </tr>
            
            <tr style='display:none;'>
                <td>Task Status</td>
                <td><select id='taskStatusFlag' name="Task_status">
                                       <?php 
               foreach($ticketTriagedData['task_status'] as $id=>$value){
                    $selected = ($id==$ticketTriagedData["task_status_selected"])?"selected":"";
                    echo "<option value='".$id."' $selected>".$value."</option>";
                } ?>
                    </select>
                </td>
            </tr>
             <tr>
                <td>Comment read</td>
                <td>
                    <input id='commentReadFlag' name="commentRead" type="checkbox">
                    <input id='ticketId' name="ticketId" value="<?php echo $ticket->getId();?>" type="hidden">
                    <input id='ticketStatus' name="ticketStatus" value="<?php echo $ticket->getStatus(); ?>" type="hidden">
                </td>
            </tr>
            <tr id="triageDate">
                <td colspan="2">
                    <input class="datetimepicker" name='nextCustomerUpdate' type="text" placeholder="Next Comm. Date" >
                </td>
            </tr>
            <tr>
                <td colspan="2"><input name="submit" id='triageForm' type="button" value="Submit">
                </td>
            </tr>
<!--            <tr>
                <td colspan="2" style="text-align: right;">
                    <i class="icon-calendar" id="triageDate-icon"></i>
                </td>
            </tr>-->
            
        </table>
        </div>
    
    <?php } } ?>
</div>
       <!-- ticketRightSideBar_add_end_here -->
               <div class='widget user_ticket_bar' style='width:265px;left: 0px;top: 0px;margin-top: 0px;font-size:14px;background:#fff;'><h4 style='margin-bottom: 9px;'><center>Merchant Attachments</center></h4>
            <div class='widgetLimitHeight'><table style='border-spacing: 0;width: 100%;border-collapse: collapse;'>
                    <tbody>
                        <?php $entity_ids = $ticket->getAtttachmentEntityIds(); 
 $type=  ucfirst(key($entity_ids)[0]);
                        
?>
                        <tr style="background:#F4FAFF;;margin-bottom:4px;">
                            <td style="padding:2px;width:200px;font-size:10px;font-weight: bold;">Attachment</td>
                            <td style="padding:2px;width:200px;font-size:10px;font-weight: bold;"><?php echo key($entity_ids);?></td>
                        </tr>
                                                <?php
                                                $entity_ids=current($entity_ids);
                        $attachment_list = $ticket->getTicketAttachments();
                        foreach ($attachment_list as $attach) {
                            $object_id = $attach->ht['object_id'];
                            ?>
                            <tr style="background:#F4FAFF;;margin-bottom:4px;">
                                <td style=" padding:2px;width:200px;font-size:10px;">
                                    <a href="<?php echo $attach->file->path; ?>" style="word-wrap: break-word;width: 200px;">
                                        <b> <?php echo $attach->file->name; ?></b>
                                    </a>
                                </td>
                                <td style="font-size: 12px; color: rgb(119, 119, 119);">
                                    <form>
                                    <input type="hidden" name="attach_id" class="attach_id" value="<?php echo $attach->ht['id'];?>"/>
                                    <input type="hidden" name="attach_type" class="attach_type" value="<?php echo $type; ?>"/>
                                    <select name="entity_id" class="entity_id" onchange="update_attachment(this)"
                                        style="
                                        background-color: #ffffff;
                                        background-image: none;
                                        border: 1px solid #cacaca;
                                        color: #333333;
                                        display: inline-block;
                                        font-size: 11px;
                                        height: 18px;
                                        line-height: 1.42857;
                                        -webkit-appearance: none;
                                        -moz-appearance: none;
                                        -ms-appearance: none;
                                        -o-appearance: none;
                                        outline: none;
                                        margin: 5px 0;
                                        width:70px;
                                        ">
<option value=""></option>
<?php foreach($entity_ids as $entity_id){ ?>
                                        <option value="<?php echo $entity_id; ?>" <?php if($entity_id==$object_id){
                                            echo "selected";      
                                        }?>
                                        ><?php echo $entity_id; ?></option>
                                        <?php }?>
                                    </select>
                                    </form>
                                </td>
                            </tr>
<?php } ?>
                    </tbody>
                </table>




            </div>
        </div>
    </div>
</div>

<!--ticketRightSideBar_remove_her -->
<!--ticketRightSideBar_end_her -->

<!-- popup to show tracking info -->
<div id="tracking_popup" >
        <p align="right">
            <a onclick="close_tracking_pop('p_refund_popup');">
            </a>
        </p>
        <div id="tracking_popup_details">

        </div>
    </div>  
<?php 
    /**
     * User History log widget.
     * @param int $userid
     * @return null but prints the widget
     */
    function getUserHistory($userId,$ticketOrderId, $ticketNumber){
        	if($userId && $user=User::lookup($userId)){
                    $tickets = TicketModel::objects();
                    $tickets->filter(array('user_id' => $user->getId()));
                    $tickets->values('status_id', 'status__name', 'number', 'cdata__subject', 'ticket_id', 'source', 'dept_id', 'dept__name', 'odata__order_ids')->order_by('-ticket_id');
                    global $cfg;
                    $widget = json_decode($cfg->config['pluginWidget']['value']);
                        
                    if($tickets->count()>0){
                        $subject_field = TicketForm::objects()->one()->getField('subject');
                        $tableRow = '<tr style="background:[background];margin-bottom:4px;"><td style=" padding:8px;width:200px;font-size:12px;"><a href="tickets.php?id=[id]" style="word-wrap: break-word;width: 200px;"><b>#[ticketId]</b>-[subject]</a></td><td style="font-size: 12px; color: rgb(119, 119, 119);">[status]</td><tr>';
                        $displayWidget = "<div class='widget user_ticket_bar' style='width:265px;left: 0px;top: 0px;margin-top: 0px;font-size:14px;background:#fff;'><h4 style='margin-bottom: 9px;'><center>Seller previous Tickets</center></h4><div class='widgetLimitHeight'><table style='border-spacing: 0;width: 100%;border-collapse: collapse;'>";
                        //$sameOrderTicketsWidget = "<div class='widget user_ticket_bar' style='width:265px;left: 0px;top: 0px;margin-top: 0px;font-size:14px;background:#fff;'><h4 style='margin-bottom: 9px;'><center>Order Previous Tickets</center></h4><div class='widgetLimitHeight'><table style='border-spacing: 0;width: 100%;border-collapse: collapse;'>";
                        $ticketCount=0;
                        $SameOrdTicketCount=0;
                        $maxCountForTicket = isset($widget["userHistory"]["max"])?$widget["userHistory"]["max"]:2;
                        
                        foreach($tickets as $T){
                            $tnum = $T['number'];
                            $tid = $T['ticket_id'];
                            $status = TicketStatus::getLocalById($T['status_id'], 'value', $T['status__name']);
                            $subject = $subject_field->display($subject_field->to_php($T['cdata__subject']));

                            if($ticketCount <= $maxCountForTicket && $tnum != $ticketNumber) {
                                $ticketCount++;
                                $background = ($ticketCount%2==0)?"#fff":"#F4FAFF;";
                                $displayWidget .= str_replace("[background]",$background,str_replace("[id]", $tid,str_replace("[status]", $status,str_replace("[subject]", $subject,str_replace("[ticketId]", $tnum, $tableRow)))));
                            }

                            if($T['odata__order_id'] == $ticketOrderId && 
                                $tnum != $ticketNumber && 
                                $SameOrdTicketCount <= $maxCountForTicket) {
                                $SameOrdTicketCount++;
                                $background = ($SameOrdTicketCount%2==0)?"#fff":"#F4FAFF;";
                                //$sameOrderTicketsWidget .= str_replace("[background]",$background,str_replace("[id]", $tid,str_replace("[status]", $status,str_replace("[subject]", $subject,str_replace("[ticketId]", $tnum, $tableRow)))));
                            }
                        }
                        if($ticketCount == 0) {
                            $displayWidget .= "No Previous Tickets";
                        }
                        if($SameOrdTicketCount == 0) {
                            //$sameOrderTicketsWidget .= "No Previous Tickets";
                        }
                        $displayWidget .='</table></div>';
                        $displayWidget .= '</div>';
                        //$sameOrderTicketsWidget .='</table></div>';
                        //$sameOrderTicketsWidget .= '</div>';
                        
                        print_r($displayWidget);
                        //print_r($sameOrderTicketsWidget);
                    }
                    else{
                        return;
                    }
                }
	}
?>
<style type="text/css">
    .select2-results{
    min-width:228px;
    margin-left: -88px;
    background-color: white;
}
.select2-dropdown .select2-search{
    background-color: white;
    min-width:228px;
    margin-left: -88px;
}

 .select2-results li{
    border-bottom: black 1px solid;

}
ul.select2-results__options{
    border-left: solid 1px #000;
    border-bottom: solid 1px #000;
}
.imgb {
    background-color: #1976d2;
    margin: 9px;
    width: 43%;
}
textarea.taskComment {
    height: 2em;
    width: 90%;
    padding: 3px;
    transition: all 0.3s ease;
}
textarea.taskComment:focus{
    height: 6em;
}
</style>
<script type="text/javascript">
$(function() {
    <?php if($timeForTracking){ ?>
        var timeForTracking = <?php echo $timeForTracking; ?>;
        <?php } ?>
    $(document).on('click','.close_task', function(){
        var element = $(this);
        $("#overlay").show();

        if(!$(".close_task_popup").length)
        {
            var html="<div class='close_task_popup'>Do you really want to close this task?</div>";
            $("body").append(html);
            $(".close_task_popup").dialog({
                autoOpen: false,
                open: function(event,ui){
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                },
                title:"close task",
                modal:true,
                dialogClass: "close_task_dialog dialog",
                buttons: [
                    {
                        text: "OK",
                        click: function() {
                            var change = element.attr("change");
                            var taskId = element.attr("taskId");
                            var valueOfField = element.attr("value");
                            valueOfField= valueOfField.split('-');
                            var FieldId = valueOfField[0];
                            var nameOfField = valueOfField[1];
                            valueOfField= valueOfField[2];
                            var moreInfo="close"; 
                            var data = {"taskId":taskId,"change":change,"valueOfField":valueOfField,"FieldId":FieldId,"nameOfField":nameOfField,"moreInfo":moreInfo,"closeOnly":"true","timeForTracking":timeForTracking};
                            $.ajax({
                                url:"ajax.php/tasks/changeField",
                                type:"POST",
                                data:data,
                                success: function(data){
                                    console.log(data);
                                    
                                    location.reload();
                                },
                                beforeSend: function(){
                                    $("#loading").show();
                                },
                                error:function(){

                                }
                            });  
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $("#overlay").hide();
                            $(".close_task_popup").dialog('close');
                        }
                    }
                ]
            });
        }
        $(".close_task_popup").dialog('open');

        
    });
    $(document).on('click', 'a.change-user', function(e) {
        e.preventDefault();
        var tid = <?php echo $ticket->getOwnerId(); ?>;
        var cid = <?php echo $ticket->getOwnerId(); ?>;
        var url = 'ajax.php/'+$(this).attr('href').substr(1);
        $.userLookup(url, function(user) {
            if(cid!=user.id
                    && $('.dialog#confirm-action #changeuser-confirm').length) {
                $('#newuser').html(user.name +' &lt;'+user.email+'&gt;');
                $('.dialog#confirm-action #action').val('changeuser');
                $('#confirm-form').append('<input type=hidden name=user_id value='+user.id+' />');
                $('#overlay').show();
                $('.dialog#confirm-action .confirm-action').hide();
                $('.dialog#confirm-action p#changeuser-confirm')
                .show()
                .parent('div').show().trigger('click');
            }
        });
    });
<?php
    // Set the lock if one exists
    if ($mylock) { ?>
!function() {
  var setLock = setInterval(function() {
    if (typeof(window.autoLock) === 'undefined')
      return;
    clearInterval(setLock);
    autoLock.setLock({
      id:<?php echo $mylock->getId(); ?>,
      time: <?php echo $cfg->getLockTime() * 60; ?>}, 'acquire');
  }, 50);
}();
<?php } ?>
});
$(window).load(function(){
        setTimeout(function(){ $(".orderNo").click(); }, 100); 
        if($(".widgetPriority").text()=="Emergency")
        {
            $(".widgetPriority").css("color","red");
            $(".helpATopic").attr("id","msg_warning");
        }else{
            $(".helpATopic").attr("id","msg_info");
        }
      })
      
      <?php include('script-custom-filter.inc.php');?>
      
      
       var taskImageMapping=<?php echo json_encode($imageTaskPopUpMapArray); ?>;
      $(function() {
           $("#taskType").select2({'placeholder':'Task Type'});
           $("#priorityFlag").select2({'placeholder':'Priority Channel'});
           
          $("#triageForm").on('click',function(){
    var ticketId = $("#ticketId").val();
    var callCustomer = $("#callCustomerFlag").val();
    var estDuedate = $('#triageDate td .datetimepicker').val();
        if(callCustomer ==='2'){ 
    if(!estDuedate){
        alert("Please Select Date before Submitting");  
        return;
    }
    }

        var taskStatus = $("#taskStatusFlag").val();
    var customerUpdate = $("#customerUpdateStatusFlag").val();
    var sla = $("#slaFlag").val();
    var triage = 4;
    var comment = $("#commentReadFlag:checked").length;
    if(comment<1){
        alert("Please Read the Comment before Submitting");
        return;
    }
    var communication = $("#commFlag").val();
    var priority = $("#priorityFlag").val();
    var ticketStatus = $('#ticketStatus').val();
    
    var data = {"ticketId":ticketId,"ticketStatus":ticketStatus,"callCustomer":callCustomer,"taskStatus":taskStatus,"customerUpdate":customerUpdate,"sla":sla,"triage":triage,"comment":comment,"communication":communication,"priority":priority,"estDuedate":estDuedate,"timeForTracking":timeForTracking};
    var url = 'ajax.php/tickets/triageSubmit';
    $.ajax({
        type: "POST",
        url: url, 
        data: data,
        cache: false, 
        beforeSend: function() {
            $("#widgetTriage table").slideUp('normal',function(){
                $("#widgetTriage h2").text();
                $("#widgetTriage").prepend("<h2><center>Submitting....</center></h2>");
            });
        },
        success: function(resp)
        {
            if(resp.indexOf("success")!=-1){
             setTimeout(function(){ 
                 $("#widgetTriage h2").remove();
                 $("#widgetTriage").prepend("<h2><center>Updated Successfully</center></h2>"); }, 1000);
               //  location.reload();
                <?php
                $stopReload = ($cfg->config['stopReload']['value'])?:0;
                if(!$stopReload){
                    ?>
                     window.history.go(-1);
                     <?php
                    }
                ?>
          
            }else{
                setTimeout(function(){ 
                $("#widgetTriage h2 center").text("Error- Try Again");
                $("#widgetTriage").css("border","solid 1px #F00");
                $("#widgetTriage table").slideDown('normal'); }, 1000);
            }
        },
        error: function($err){
            setTimeout(function(){ 
            $("#widgetTriage").css("border","solid 1px #F00");
            $("#widgetTriage h2 center").text("Error- Try Again");
            $("#widgetTriage table").slideDown('normal'); }, 1000);
        }
    });
    
});
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function showPopUpForImageSelection(data,selectedObject){
    var html = $('#imageSelector').html();
      tiModal.create(html,{
        events: {
          'click .btn-success': function(e){
            console.log(this);
            var attachmentRequired=0;
            var selectedImageValue=$('#imageOptions').val();
            var imageTaskOptions=$('#imageOptions').val();
                if(!imageTaskOptions){
                    $("#imageErrorMsg").show();
                    return ; 
                }
            $("select#imageOptions option").each(function(){
                if(inArray($(this).val(),selectedImageValue)){
                attachmentRequired=attachmentRequired || $(this).attr('data-atchment');
                }
             });
             console.log("attachment"+attachmentRequired);
            data.imageOption=imageTaskOptions;
            data.attachmentRequired=attachmentRequired;
            sendChangeRequest(data,selectedObject);
            this.close();
          },
          'click .btn-cancel': function(e){
            $("#imageErrorMsg").hide();
            this.close();
          }
        },
        modal: true
      }).show();
       $('#imageOptions').select2();
}

if($("#showEntityPopUp")){
$("#showEntityPopUp").click(function(){
showPopUpForMandateIds();
});
}
function showPopUpForMandateIds(){
    var mandateIds=$('#mandateIds').text();
    var html = $('#mandateIdPopup').html();
    tiModal.create(html).show();
}

          $("#taskDepartment").on('change',function(){
            var dept_id = $("#taskDepartment").val();
            $.ajax({
                url:"ajax.php/tasks/taskType",
                type:"get",
                data:{"dept_id":dept_id},
                success:function(data){
                    $("#taskType").html(data);
                },
                error:function(){

                }
            })
          });
          $("#man-info").change(function(){
                var mandateVal=$(this).val() + " ids";
                $("#mandateIDsText").text(mandateVal.capitalizeFirstLetter());
                $("input[name='mandateID']").text('');
          });
          $('.datetimepicker').datetimepicker({
            formatDate:'d/m/Y H:i',
            minDate:'0'});
          $("#createTask").on('click',function(e){
              var typeVal = $("#taskType").val();
              var type=typeVal.split("-")[1];
              var title = type.split("-")[2];
              var dept = $("#taskDepartment").val();
              var mandateIds=$("input[name='mandateID']").val();
              var mandateType=$("#man-info").val();
              if(0 && mandateType && !mandateIds){
                  alert("Select Ids corresponding to mandate info");
                  return;
              }
              if(!dept)
              {
                alert("Select a department");
                return;
              }
              if(!type)
              {
                alert("Select task type");
                return;
              }
              var statusVal = $("#taskStatusCreation").val();
              var status = statusVal.split("-")[1];
              var ticketId = <?php echo $ticket->getId();?>;
              var data = {"mandateType":mandateType,"mandateId":mandateIds, "type":type,"dept":dept,"status":status,"ticketId":ticketId,"title":title,"timeForTracking":timeForTracking};
              var url = 'ajax.php/tasks/createTask';
            $.ajax({
                type: "POST",
                url: url, 
                data: data,
                cache: false,
                beforeSend:function(){
                    $("#taskLoader").show();
                 $("#taskCreator").hide();
              
                },
                success: function(resp)
                {
                 console.log(resp);
                    $("#taskLoader").hide();
                    $("#taskCreator").show();
                    if (resp) {
                        $("#taskCreator").slideUp('normal', function () {
                            $("#taskCreator").html("<h2>"+resp+"</h2>");
                            $("#taskCreator").slideDown();
                        });
                        if(resp.indexOf("uccess")!=-1){
                            <?php
                            $stopReload = ($cfg->config['stopReload']['value'])?:0;
                            if(!$stopReload){
                                ?>
                                location.reload();
                                <?php
                            }
                            ?>
                        }
                    }else {
                        alert("Something went wrong");
                    }
                },
                error:function(resp){
                    $("#taskLoader").hide();
                    $("#taskCreator").show();
                    console.log(resp.responseText);
                    alert(resp.responseText);
                    return;
                    }
            });
              
          });
    $(".taskComment").keydown(function(e){
        if(e.keyCode==13){
            var taskId = $(this).attr("taskId");
            var comment = $(this).val();
            var FieldId =  $(this).attr("fieldId");
            var selectFieldSelector = this;
            if(comment.length<5){
                alert("Please enter comment");
                return false;
            }
            var data = {"taskId":taskId,"change":"comment","valueOfField":comment,"FieldId":FieldId,"timeForTracking":timeForTracking};
            var url = 'ajax.php/tasks/changeField';
            $.ajax({
                type: "POST",
                url: url, 
                data: data,
                cache: false,
                success: function(resp)
                { console.log(resp);
                    if(resp!=0){
                        resp = JSON.parse(resp);
                        if(resp.comment.length>0)
                            $(".taskCommentBox"+taskId+" .current-info").show().text(resp.comment);
                     if(resp.response.length>0)
                        $(".taskResponseBox"+taskId+" .current-info").show().text(resp.response);
                     if(resp.status.length>0)
                        $(".taskStatusBox"+taskId+" .current-info").show().text(resp.status);
                    if(resp.duedate.length>0)
                        $(".taskDuedateBox"+taskId+" .current-info").show().text(resp.duedate);
                    if(resp.updatedTime)
                        timeForTracking = resp.updatedTime;
                     $(selectFieldSelector).hide();

                    }else{
                        alert("error");
                    }
                }
            });
        }
    });
    
    $(".taskDueDate").keydown(function(e){
        if(e.keyCode==13){
            var taskId = $(this).attr("taskId");
            var dueDate = $(this).val();
            var selectFieldSelector = this;
            if(dueDate.length<5){
                alert("Please enter Next Promised SLA");
                return false;
            }
            var data = {"taskId":taskId,"change":"duedate","valueOfField":dueDate,"timeForTracking":timeForTracking};
            var url = 'ajax.php/tasks/changeField';
            $.ajax({
                type: "POST",
                url: url, 
                data: data,
                cache: false,
                success: function(resp)
                {console.log(resp);
                    if(resp!=0){
                        resp = JSON.parse(resp);
                        if(resp.comment.length>0)
                            $(".taskCommentBox"+taskId+" .current-info").show().text(resp.comment);
                     if(resp.response.length>0)
                        $(".taskResponseBox"+taskId+" .current-info").show().text(resp.response);
                     if(resp.status.length>0){
                     //   $(".taskStatusBox"+taskId+" .current-info").show().text(resp.status);
                      //  $(".taskStatusBox"+taskId+" .change-selector").remove();
                     }
                    if(resp.duedate.length>0)
                        $(".taskDuedateBox"+taskId+" .current-info").show().text(resp.duedate);
                    if(resp.updatedTime)
                        timeForTracking = resp.updatedTime;
                     $(selectFieldSelector).hide();

                    }else{
                        alert("error");
                    }
                }
            });
        }
    });

    
    $(".change-selector").on('change',function(){

        var change = $(this).attr("change");
        var taskId = $(this).attr("taskId");
        var taskTypeId=$(this).attr('data-task-type-id');
        var valueOfField = $(this).val();
        if(valueOfField=="select"){
            return;
        }
        if(change=="Dept"){
            valueOfField = valueOfField.split("-");
            var data = {"taskId":taskId,"change":change,"valueOfField":valueOfField[0]};
            valueOfField = valueOfField[1];
        }else{
            valueOfField = valueOfField.split("-");
            var FieldId = valueOfField[0];
            var nameOfField = valueOfField[1];
            //console.log(valueOfField);
            var moreInfo="open";
            if(change=="status"){
		if(nameOfField == 47){
                    var updatedSLA = $.trim($(".taskDuedateBox"+taskId+" .current-info").text());
                    var promisedSLA = $.trim($(".promisedSLA"+taskId).text());
                    console.log(updatedSLA); console.log(promisedSLA);
                    if(updatedSLA == "Not Changed" || updatedSLA == promisedSLA){
                        alert("Please update the 'Updated SLA' field first");
			$(this).find("option[val='select']").prop('selected',true);
                        return;
                    }
                }
                moreInfo = valueOfField[3];
	    }
            
            valueOfField = valueOfField[2];
            if(moreInfo=="0"){
                moreInfo="close";
            }
            var data = {"taskId":taskId,"change":change,"valueOfField":valueOfField,"FieldId":FieldId,"nameOfField":nameOfField,"moreInfo":moreInfo,"timeForTracking":timeForTracking};
            var selectFieldSelector = this;
            if(change=="response"){
                for(var i in taskImageMapping){
                    if(taskImageMapping[i]['taskTypeId']==taskTypeId && taskImageMapping[i]['responseId']==nameOfField){
                        showPopUpForImageSelection(data,selectFieldSelector);
                        return;
                        
                  }
                  }
            }
        }
        var selectFieldSelector = this;
	if(change == "response"){
	   checkResponseStatus(data,selectFieldSelector);
	}else{
	   sendChangeRequest(data,selectFieldSelector); 
	}
    });

    function checkResponseStatus(data,selectorField){

	var url = 'ajax.php/tasks/getStatus';
        var respData = {"taskId":data['taskId'],"responseId":data['nameOfField']};
        $.ajax({
            type: "POST", url: url, data: respData, cache: false, aysnc: false,
            success: function(resp)
            {
                if(resp == 3){
                    var updatedSLA = $.trim($(".taskDuedateBox"+data['taskId']+" .current-info").text());
                    var promisedSLA = $.trim($(".promisedSLA"+data['taskId']).text());
                    console.log(updatedSLA); console.log(promisedSLA);
                    if(updatedSLA == "Not Changed" || updatedSLA == promisedSLA){
			$(selectorField).find("option[val='select']").prop('selected',true);
                        alert("Please update the 'Updated SLA' field first");
                        return;
                    }else{
			sendChangeRequest(data,selectorField);
		    }
                }else{
		    sendChangeRequest(data,selectorField);
		}
            }
        });
    }
    
    function sendChangeRequest(data,selectorField){

        if(data.valueOfField){
                var url = 'ajax.php/tasks/changeField';
                $.ajax({
                    type: "POST",
                    url: url, 
                    data: data,
                    cache: false,
                    success: function(resp)
                    { 
                        if(resp!=0){
                           resp = JSON.parse(resp);
                         if(resp.comment.length>0)
                                $(".taskCommentBox"+data.taskId+" .current-info").show().text(resp.comment);
                         if(resp.response.length>0)
                            $(".taskResponseBox"+data.taskId+" .current-info").show().text(resp.response);
                         if(resp.status.length>0){
                            $(".taskStatusBox"+data.taskId+" .current-info").show().text(resp.status);
                            $(".taskStatusBox"+data.taskId+" .change-selector").remove();
                        }
                        if(resp.duedate.length>0)
                            $(".taskDuedateBox"+data.taskId+" .current-info").show().text(resp.duedate);
                        if(resp.updatedTime)
                            timeForTracking = resp.updatedTime;
                         $(selectorField).hide();

                         if(resp.close)
                             window.location.reload();
                        }else{
                            alert("error");
                        }
                    }
                });

            }
    }
    $(".existing-info-box").on('click',function(){
        if($(this).children(".change-selector").length>0){
            $(this).removeClass("existing-info-box").addClass("existing-info-box-select").off('click');
            $(this).children(".change-selector").show();
            $(this).children(".current-info").hide();
        }else if($(this).children(".taskComment").length>0){
            $(this).children(".taskComment").show();
            $(this).children(".current-info").hide();
        }else if($(this).children(".taskDueDate").length>0){
            $(this).children(".taskDueDate").show();
            $(this).children(".current-info").hide();
        }
        else{
            alert("Cannot be changed due to anyone of the reason-\n1. Task Is closed\n2. You do not have Access\n\nContact admin for support.");
        }
    });
    
    var widget = new Array();
      $(".OrderId").on('click',function(){
        var tid  = parseInt($("#content .flush-left a").attr("href").replace("tickets.php?id=",""));
          $("#order_details_container").html("");
          var orderId = $(this).attr('value');
          if(widget[orderId]){
              $("#order_details_container").html(widget[orderId]);
              return false;
          }
          if(orderId !='' && orderId !='0')
            var url = 'ajax.php/tickets/'+tid+'/order-details/'+orderId+'.json';

          $.ajax({
           type: "GET",
           url: url, 
           cache: false, 
           beforeSend: function() {
            showLoader("#order_details_container");
            $("#order_details_container").slideDown();
           },
           success: function(resp)
           {
               if(resp.response){
                    $("#order_details_container table").html("");
                    widget[orderId] = '';
                    var orderInfo = resp.response;

                    var info= [];
                    var Heading=[];

                    $.each(orderInfo,function(fieldName,valueObj){
                        info[""+fieldName+""]="";
                        $.each(valueObj,function(field,value){
                            if(field!="Head")
                                info[""+fieldName+""] += "<tr><td>"+value+"</td></tr>";
                            else
                                Heading[""+fieldName+""] = value;
                        });

                        $("#loader").slideUp(1000,function(){
                                $("#order_details_container").append("<div class='widgetHead a' style='cursor: pointer;'>"+Heading[fieldName]+"<i class='arrow' style='border-color:white;position: relative;float: right;top: 4px;'></i></div><div id='"+fieldName+"' class='order_details_widget' style='display:none;'><table></table></div>");
                                $("#order_details_container #"+fieldName+" table").html(info[fieldName]);
                                //$("#order_details_container #"+fieldName+"").slideDown("slow");
                                widget[orderId] = widget[orderId] + "<div class='widgetHead a' style='cursor: pointer;'>"+Heading[fieldName]+"<i class='arrow' style='border-color:white;position: relative;float: right;top: 4px;'></i></div><div id='"+fieldName+"' class='order_details_widget' style='display:none;'><table>";
                                widget[orderId] = widget[orderId] + $("#order_details_container #"+fieldName+" table").html();
                                widget[orderId] = widget[orderId] + "</table></div>";
                        });
                    });
                    
                }else{ 
                    $("#order_details_container").slideUp("slow",function(){
                        $("#order_details_container").show().html("<div style='margin-top:50px; margin-bottom:50px; text-align:center; padding:10px;' >Invalid Order Id</div>");
                        $("#order_details_container").slideDown();

                    });
                }
           }

          });
      });
      
      function showLoader(divId){
          var loader = "<div id='loader' class='order_details_widget'><table><div id='customLoader'><center><img style='margin-top:100px; margin-bottom:100px;' src='./images/FhHRx-Spinner.gif'></center></div></div></div>"
          $(divId).append(loader);
          $("#loader").show();
      }
      
      $(".displayClosedDetails").on('click',function(){
          $(this).closest('table').children("tbody").children(".hiddenTaskData").slideDown(2000);
      });
      
      $(".assignToMe").on('click',function(e){
          e.preventDefault();
          var div = $(this);
          var url = 'ajax.php/tickets/claim';
          var data = {'tid':<?php echo $ticket->getId();?>};
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                cache: false,
                success: function(resp)
                {
                    div.text(resp).removeClass('assignToMe');
                    window.location.reload();
                }
            });
            return false;
      });
      
      
      
      $(".assignToMetask").on('click',function(e){
          var div = $(this);
           var taskId = $(this).attr('id');
          e.preventDefault();
          var url = 'ajax.php/tasks/claim';
          var data = {'taskId':taskId};
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                cache: false,
                success: function(resp)
                {
                    div.text(resp).removeClass('assignToMetask');
                    window.location.reload();
                }
            });
            return false;
      });
      
        $("#ticketIssue").on('change',function(e){
            var topicId = $(this).val();
            var topicIdText = $("#ticketIssue option:selected").text();
            if(!topicId)
              return;
            //e.preventDefault();

            $("#overlay").show();

            if(!$(".change_issue_alert").length)
            {
                var html="<div class='change_issue_alert'>Do you really want to update issue type/sub issue type to "+topicIdText+" ?</div>";
                $("body").append(html);
                $(".change_issue_alert").dialog({
                    autoOpen: false,
                    open: function(event,ui){
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    },
                    title:"Update issue type/sub issue type",
                    modal:true,
                    dialogClass: "change_issue_dialog dialog",
                    buttons: [
                        {
                            text: "OK",
                            click: function() {
                                var topicId = $("#ticketIssue").val();
                                var url = 'ajax.php/tickets/topicChange';
                                var data = {'tid':<?php echo $ticket->getId();?>,'issue':topicId};
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    cache: false,
                                    success: function(resp)
                                    {
                                        $("#ticketIssue").hide();
                                        $(".issue_type").show();
                                        if(resp.message){
                                            $(".issue_type").html(resp.message);
                                            $("#overlay").hide();
                                            $(".change_issue_alert").dialog('close');
                                            $('#ticketIssue').prop('selectedIndex',0);
                                            $('.changeIssue').html('');
                                            $('.changeIssue').addClass('icon-edit');
                                        }
                                        else{
                                            alert("Something went wrong");
                                        }
                                    }
                                });
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $("#overlay").hide();
                                $(".change_issue_alert").dialog('close');
                                $('#ticketIssue').prop('selectedIndex',0);
                            }
                        }
                    ]
                });
            }
            else{
                var html="Do you really want to update issue type/sub issue type to "+topicIdText+" ?";
                $(".change_issue_alert").html(html);
            }
            $(".change_issue_alert").dialog('open');
        });
      
      
      // $('.order-link .close').on('click',function(){
      //       $('.order-link').slideUp("normal");
      //   });
      //   $('.link-order').on('click',function(){
      //       $('.order-link').slideDown('normal');
      //   });
        function resetOrderReturnIdField(){
            $('.change-orderid').val($('#ticketorder_id').html());
            if($('#ticketreturn_id').length)
                $('.change-returnid').val($('#ticketreturn_id').html());
            else
                $('.change-returnid').val('');
        }
        $('.link-order').on('click',function(){
        $(".edit-orderId").toggle();
        $(".ord_rtn_id_value").toggle();
        if($(this).hasClass('icon-edit')){
            $(this).removeClass('icon-edit');
            $(this).html('X');
        }
        else{
            resetOrderReturnIdField();
            $(this).html('');
            $(this).addClass('icon-edit');
        }
        });
        
        $('.change-ord-rtn-id').keydown(function(e){
            if(e.keyCode==13){
                e.preventDefault();
                var order = $('.change-orderid').val();
                if(order=='' || order==0){
                    alert("Invalid Order Id");
                    return;
                }
                var returnId = $('.change-returnid').val();
                if(returnId=='')
                    returnId=0;
                $("#overlay").show();

                if(!$(".change_orderid_alert").length)
                {
                    var html="<div class='change_orderid_alert'>Do you really want to update order id="+order+" and return id="+returnId+" ?</div>";
                    $("body").append(html);
                    $(".change_orderid_alert").dialog({
                        autoOpen: false,
                        open: function(event,ui){
                            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                        },
                        title:"Update Order id and return id",
                        modal:true,
                        dialogClass: "change_orderid_dialog dialog",
                        buttons: [
                            {
                                text: "OK",
                                click: function() {
                                    var order = $('.change-orderid').val();
                                    if(order=='')
                                        order=1000000;
                                    var returnId = $('.change-returnid').val();
                                    if(returnId=='')
                                        returnId=0;
                                    var url = 'ajax.php/tickets/orderidchange';
                                    var data = {'tid':<?php echo $ticket->getId();?>,'orderid':order,'returnid':returnId};
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: data,
                                        cache: false,
                                        success: function(resp)
                                        {
                                            if(resp.status){
                                                //$('.order-link .close').click();
                                                //$(".link-order").text(order).removeClass("link-order");
                                                window.location.reload();
                                            }
                                            else{
                                                resetOrderReturnIdField();
                                                $("#overlay").hide();
                                                $(".change_orderid_alert").dialog('close');
                                                alert(resp.msg);
                                            }
                                        }
                                    });
                                }
                            },
                            {
                                text: "Cancel",
                                click: function() {
                                    resetOrderReturnIdField();
                                    $("#overlay").hide();
                                    $(".change_orderid_alert").dialog('close');

                                }
                            }
                        ]
                    });
                }
                else{
                    var html="Do you really want to update order id="+order+" and return id="+returnId+" ?";
                    $(".change_orderid_alert").html(html);
                }
                $(".change_orderid_alert").dialog('open');
            }
        });
    
});
$(document).on('click','.changeIssue',function(){
        if($(this).hasClass('icon-edit')){
            $(this).removeClass('icon-edit');
            $(this).html('X');
        }
        else{
            $(this).html('');
            $(this).addClass('icon-edit');
        }
        $(".issue_type").toggle();
        $("#ticketIssue").toggle();
});

$(document).on('click','.a',function(){
        $(this).next().slideToggle("slow");
});

$(document).on('click','#tracking_no',function(){
    var tracking_number = $(this).html().trim();
    var carrier_id = $(this).attr("data-billing-carrier");
    show_tracking_scans(tracking_number, carrier_id);
});

$('#showClosed').on('click',function(){
	var text = $('#showClosed').val();
        if (text == 'Show Closed Tasks'){
            $('#showClosed').css("background-color","black");
            $('#showClosed').val("Hide Closed Tasks");  
        }else{
            $('#showClosed').css("background-color","#337ab7");
            $('#showClosed').val("Show Closed Tasks");
        }
	$('.show_closed').toggle();
});

function show_tracking_scans(tracking_number, carrier_id) {
    var url='ajax.php/tickets/track_shipment';
    $.ajax({
        type: "GET",
        url: url,
        data: { tracking_number:tracking_number, carrier_id:carrier_id},
        success: function(html){
            $("#loading").hide();
            if(html.data.scans) {
                var scansData = html.data.scans;
                var firstKey = Object.keys(scansData)[0];
                var text = '<div class="shp_table"><table cellspacing="20">';
                var firstKeyData =  scansData[firstKey];
                var obj = Object.keys(firstKeyData)[0];
                //var i=0;
                var field = [];
                $.each(firstKeyData[obj],function(obje,val){
                        field.push(obje);
                    });
                text = text + "<tr>";
                for(var i=0;i<field.length;i++)
                {
                    text = text + "<th>" + field[i] + "</th>";
                }
                text = text + "</tr>";
                $.each(firstKeyData,function(field,value){
                    text = text + "<tr>";
                    $.each(value,function(obj,val){
                        text = text + "<td>" + val + "</td>";
                    });
                    text = text + "</tr>";
                });
                text = text + '</table></div>';
                
                $('#tracking_popup_details').html(text);

                $('#tracking_popup').show();
            } else if(html.data.url){
                var urlData = html.data.url;
                var firstKey = Object.keys(urlData)[0];
                var firstKeyData = urlData[firstKey];
                window.open(firstKeyData, '_blank');
                $("#overlay").hide();
            } else {
               alert("Tracking_not_possible");
               $("#overlay").hide();
            }   
        },
        error: function(data){
            console.log("error");
        },
        beforeSend: function(){
            $('#loading').show();
            $('#overlay').show();
        }
    });
}

function close_tracking_pop() {
    var tp = document.getElementById("tracking_popup");
    tp.style.display = "none";
    $("#overlay").hide();
}

function update_attachment(e){
   
   var url = 'ajax.php/tickets/update_attachment';
    $.ajax({
           type: "POST",
           url: url,
           data: $(e).closest("form").serialize(), 
           success: function(data)
           {
                if (data == 1) {
                    alert("Updated successfully"); // show response from the php script.
                } else {
                    alert("Updation failed");
                }
           },
          error: function ()
            {
                alert("Something went wrong");
            }
         });
}

function task_update_attachment(e) {


        var url_link = 'ajax.php/tasks/task_update_attachment';
        $.ajax({
            type: "POST",
            url: url_link,
            data:$(e).closest("form").serialize(),
            success: function (Data)
            {
                if (Data == 1) {
                    alert("Updated successfully"); // show response from the php script.
                } else {
                    alert("Updation failed");
                }
            },
            error: function ()
            {
                alert("Something went wrong");
            }
        });
    }

</script>
