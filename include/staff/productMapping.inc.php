<?php
global $cfg;
$staticFields = getCachedTicketFields();
$taskStaticFields = getCachedTaskFields();
$isExport = FALSE;
if($_GET['EXPORT']=='exportTrue')
  $isExport = TRUE;
?>
<center>
    <div style="margin-bottom:5px;">
        <h2 style="cursor:pointer;" id="filter">Filter</h2>
        <form id="formFilter" name="formFilter" action="productMapping.php" method="get">
            <?php echo __('Department'); ?>
            <select name="departmentFilter">
                <option value="-1">No Filter</option>
                <option value="0" <?php if(isset($_GET['departmentFilter']) && $_GET['departmentFilter']==0) echo "selected" ?>>All</option>
                <?php
                foreach ($staticFields["department"] as $key => $value) {
                    $text = '<option value="'.$key.'"';
                    if($_GET['departmentFilter']==$key) 
                        $text.= "selected";
                    $text.= '>'.$value['name'].'</option>';
                    echo $text;
                }
                ?>
            </select>
            <input type="submit" value="Filter">
        </form>
    </div>
</center>
<form name="taskMap" action="productMapping.php" method="post">
    <table class="list" width="100%";>
        <input id="topicID" type='hidden' name="topicId" value="0">
        <thead>
            <tr><th colspan="8"><?php echo __('Product Mapping machine'); ?></th></tr>
        </thead>
        <tbody>
            <tr><th width="60%"><?php echo __('Topic'); ?></th>
                <th width="60%"><?php echo __('Product Status'); ?></th>
                <th width="60%"><?php echo __('Batch Upload Status'); ?></th>
                <!--<th width="60%"><?php echo __('Batch Update Status'); ?></th>-->
                <th width="60%"><?php echo __('Dept'); ?></th>
                <th width="60%"><?php echo __('Task'); ?></th>
                <th width="60%"><?php echo __('Remove'); ?></th>
            </tr>
        </tbody>
        <?php
        if(isset($_GET['departmentFilter'])){
            if($_GET['departmentFilter']!=-1){
                $urlargs="?departmentFilter=".$_GET['departmentFilter'];
                $filter = array('dept_id' =>$_GET['departmentFilter'],'task_status_id'=>1,'entity_type'=>'product');
                $filterSet=1;
            }
            else
                $filterSet=0;
        }
        else
            $filterSet=0;
        if($filterSet)
            $taskMap = TaskMapping::objects()->filter($filter);
        else
            $taskMap = TaskMapping::objects()->filter(array("task_status_id"=>1,'entity_type'=>'product'));
        
        $page_limit = PAGE_LIMIT;
        $bulkPageLimit = json_decode($cfg->config['customLimits']["value"],true);
        $bulkPageLimit = isset($bulkPageLimit["bulkTickets"])?$bulkPageLimit["bulkTickets"]:5000;
        $page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
        $count = $taskMap->count();
        $taskMapAll=clone $taskMap;
        if ($_GET['pl']) {
            $page_limit = $_GET['pl'];
        }

        if($_SESSION['bulk'])
            $pageNav = new Pagenate($count, $page, $bulkPageLimit);
        else
            $pageNav = new Pagenate($count, $page, $page_limit);
        $pageNav->setURL('productMapping.php'.$urlargs, $args);
        $taskMap = $pageNav->paginate($taskMap);

        if($_SESSION['bulk'])
            $taskMap->limit($bulkPageLimit);


        foreach ($taskMap->all() as $flow) {
            $taskIdArr[] = $flow->id;
            $flowArr[] = $flow;
            $issues_names = getIssuesRelatedInfoFromTopicId($staticFields,$flow->topic_id);
            $issuesArr[] = $issues_names;
        }
        
        $start = $count>0?($page-1)*$page_limit+1:0;
        $last = ($page*$page_limit)<$count?($page*$page_limit):$count;
        $showing = "Showing $start to $last of $count";
        
    if($isExport) { 
    ob_get_clean();
    $fp = fopen("php://output", "w");
    $header = array('Issue Type','Sub Issue','Sub Sub Issue','Sub Sub Sub Issue','Order Status','Dept','Task');
    fputcsv($fp, $header);
   
        $taskMapAll->values()->all();
        $count = 0;
         foreach ($taskMapAll as $flow) {
            $issueType= $issuesArr[$count]['issueType'];
            $issueSubType= $issuesArr[$count]['issueSubType'];
            $issueSubSubType= $issuesArr[$count]['issueSubSubType'];
            $issueSubSubSubType= $issuesArr[$count]['issueSubSubSubType'];
            $proStatus= $staticFields["orderStatus"]['Product'][$flow['product_status']]?:"Any";
            $uploadtSatus= $staticFields["orderStatus"]["batchUploadStatus"][$flow['batch_upload_status']]?:"Any";
            //$updateStatus= $staticFields["batchUpdateStatus"][$flow['batch_update_status']]?:"Any";
            $dept = $staticFields["department"][$flow['dept_id']]["name"]?:"All Department";
            $taskType=  $taskStaticFields["Task Types"][$flow['task_type_id']]?:"Any Task";
            $dataInCsv = array($issueType,$issueSubType,$issueSubSubType,$issueSubSubSubType,$proStatus,$uploadtSatus,$dept,$taskType);
            fputcsv($fp,$dataInCsv);
            $count++;
        }
    fclose($fp);
    die;
    echo '<div id="exportTicketData" style="display:none">YES</div>';
}    
        ?>
        <caption><span class="pull-left" style="display:inline-block;vertical-align:middle"><?php
                 echo $showing; ?></span>
        </caption>
        <?php
        $issue_count = 0;
        foreach ($flowArr as $flow) {
            ?>
            <tbody>
                <tr>
                    <td style="width:100px; text-align:center;"><?php echo $issueSubIssueDictionary[$flow->topic_id]; ?></td>
                    <td style="width:150px; text-align:center;"><?php echo $staticFields["orderStatus"]['Product'][$flow->product_status]?:"Any"; ?></td>
                    <td style="width:150px; text-align:center;"><?php echo $staticFields["orderStatus"]["batchUploadStatus"][$flow->batch_upload_status]?:"Any"; ?></td>
                    <!--<td style="width:150px; text-align:center;"><?php echo $staticFields["batchUpdateStatus"][$flow->batch_update_status]?:"Any"; ?></td>-->
                    <td style="width:150px; text-align:center;"><?php echo $flow->dept_id?$staticFields["department"][$flow->dept_id]["name"]:"All Department"; ?></td>
                    <td style="width:150px; text-align:center;"><?php echo $flow->task_type_id?$taskStaticFields["Task Types"][$flow->task_type_id]:"Any Task"; ?></td>
                    <td style="width:150px; text-align:center;"><input type="submit" value="X" name="Remove-<?php echo $flow->id; ?>"></td>
                </tr>
            </tbody>
                        <?php $issue_count++; } ?>
        <tbody>
            <tr><th colspan="7"><br><br></th></tr>
        </tbody>
        <tbody>
            <tr>
            
                <td style="width:150px;">   
                    <select id='topicValues' name='topicId' style="width:500px;">
                        <option value='<?php 0 ?>'>--Select--</option>
                                <?php foreach ($issueSubIssueDictionary as $id => $name) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $name; ?></option>
                                <?php }; ?>
                    </select>
                </td>
            
          	<td style="width:150px;">   
                    <select id='productStatus' name='productStatus[]' multiple='multiple' style="width:250px;">
			    <option value='-1'>Any</option>
                                <?php foreach ($staticFields["orderStatus"]["ProductUpper"] as $id => $statusName) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                            <?php }; ?>
                    </select>
                </td>
                 <td>   
                    <select name='batchUploadStatus' id='batchUpload' style="width:150px;">
                        <option value='-1'>Any</option>
                        <?php foreach ($staticFields["orderStatus"]["batchUploadStatusUpper"] as $id => $statusName) { ?>
                            <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <!--<td>   
                    <select name='batchUpdateStatus' id='batchUpdate' style="width:150px;">
                        <option value='-1'>Any</option>
                        <?php foreach ($staticFields["orderStatus"]["batchUpdateStatus"] as $id => $statusName) { ?>
                            <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                        <?php } ?>
                    </select>
                </td>-->
                <td><select name='taskDept' id="taskdept" style="width:150px;">
                        <option value='0'>Select</option>
                        <?php foreach ($staticFields["department"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data["name"] ?></option>
                        <?php }; ?>
                    </select></td>
                <td><select name='taskType' id='tasktype' style="width:150px;">
                        <option value='0'>Select</option>
                        <?php foreach ($taskStaticFields["Task Types"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select></td>
                <td></td>
            </tr>
        </tbody>
        <tbody>
            <tr><th colspan="7"><input id="Submit" type='submit' value="Add Flow"></th></tr>
        </tbody>
    </table>
</form>
<?php
$getParam = explode('?', $_SERVER['REQUEST_URI']);
     if(!empty($getParam[1]))
        $appendInUrl = $getParam[1].'&EXPORT=exportTrue';
    else 
        $appendInUrl='EXPORT=exportTrue';
    
echo '<div class="paging">&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;'; 
echo '<div class="export_bttm pull-right"><a class="export-csv no-pjax" href="?'.$appendInUrl.'">Export</a></div>';
?>
<script>
    
    $("#filter").on("click",function(){
        $("#formFilter").slideToggle();
    });
    
    $(document).ready(function () {
        
        $('#Submit').on("click",function(){
            var topicValues=parseInt($("#topicValues").val());
	    var taskDept=parseInt($("#taskdept").val());
	    var taskType=parseInt($("#tasktype").val());
	    if(!(topicValues && taskDept && taskType)){
		alert("Select all the values for mapping");
		return false;
		} 

        });
    });
    
    $(function () {
        $("#batchUpload").select2({'placeholder': 'Select'});
        $("#taskdept").select2({'placeholder': 'Select'});
        $("#tasktype").select2({'placeholder': 'Select'});
        //$("#batchUpdate").select2({'placeholder': 'Select'});
        $("#topicValues").select2({'placeholder': 'Select'});
        $("#productStatus").select2({'placeholder': 'Select'});
    });
    
    
</script>
