<head>
<link rel="stylesheet" href="../scp/css/report.css">
  <script src="../scp/js/jquery-ui.js"></script> 
  <script src="../scp/js/ui.js"></script> 
</head>

<?php
$search = SavedSearch::create();
$tickets = Ticket::objects();
$staticFields = getCachedTicketFields();
//echo"<pre>";print_r($staticFields);die;
//$staffArray = array();
// foreach ($staticFields['staff'] as $key => $value) {
//     $staffArray[] = $key;
// }
// $obj = new Tracking;
// $event_name = $_REQUEST['event_name'];

// $Data = $obj->objects()->filter(array("staff_id__in"=>$staffArray,"event_name"=>$event_name))->values()->all();


// $sum = 0;
// foreach ($Data as $key => $val) {
//     $staffData[$val['staff_id']]["data"][] = $val;
// }
// foreach ($staffData as $key => $value) {
//     $sum=0;
//     $recordcount = count($value['data']);
//     $staffData[$key]['operation_no_of_tickets'] = $recordcount;
//     foreach ($value['data'] as $index => $val) {
//         $last_timestamp = strtotime($val['last_timestamp']);
//         $new_timestamp = strtotime($val['new_timestamp']);
//         $diff = $new_timestamp - $last_timestamp;
//         $sum = $sum + $diff;
//     }
//     $sum = $sum/$recordcount;
//     if($sum<=60)
//     {
//         $staffData[$key]['average time in '.$event_name] = round($sum)." sec";
//     }
//     else{
//         $min = intval($sum/60);
//         $mod = $sum%60;
//         $staffData[$key]['average time in '.$event_name] = $min." min ".$mod." sec";
//     }
// }
?>
<div class="clear"></div>
<div onclick="javascript:        $.dialog('ajax.php/tickets/search', 201);" style="cursor:pointer;margin-bottom:20px; padding-top:10px;text-align: center; position:relative;">
<?php
/**
 * Display advance search parameters
 * @author Akash Kumar
 */
$searchParam = getSearchParameters($search,$form);
if($searchParam){
        print_r($searchParam);
}

$showassigned = isset($_GET["unassigned"])?$showassigned:0;

?>
</div>
<div class="clear"></div>

  <!--*******************************************
      Code for displaying customized reports
     -->
  <div class="container_section grid-row">

<?php
$filterConfig=$reportFilterConfig;
$fields=$reportFields;
$fieldNotToBeDisplayed=$reportFieldNotToBeDisplayed;
$reportType = $_GET['type']?:'triage';
$reportSubtype = $_GET['subtype']?:'sla adherence';
$reportSubSubtype = $_GET['subsubtype']?:0;
if(!$_GET['subtype'])
    $reportSubSubtype = 'agent level';
$filterString='';
if($_GET['filters']){
    $filters = explode(',', $_GET['filters']);
    $field = array();
    foreach ($filters as $value) {
        //print_r($value);die;
        $fieldsvalue = explode('_', $value);
        //print_r($fieldsvalue);die;
        if(stripos($fieldsvalue[0],"date")!==false)
        {
            //$dates = explode('_', $fieldsvalue[1]);
            $field[$fieldsvalue[0]][] = $fieldsvalue[1];
            $field[$fieldsvalue[0]][] = $fieldsvalue[2];
            $filterString .= "and DATE(B.created) >".$fieldsvalue[1]. "and DATE(B.created)<".$fieldsvalue[2];
        }
        else{
            $field[$fieldsvalue[0]] = $fieldsvalue[1];
            $filterString .= " and ".$fieldsvalue[0]."=".$fieldsvalue[1];
        }
    }
}
switch ($reportType) {
    case 'triage':
        switch ($reportSubtype) {
            case 'all':
                $flag = "t_all";
                $field['date range'][0] = $field['date range'][0]?:'0000-00-00 00:00:00';
                $field['date range'][1] = $field['date range'][1]?:date("Y-m-d H:i:s",time());
                $sql = "select staff_id,email,manager_id from mst_staff left join mst_staff_reporting on staff_id=agent_id";
                $res = db_query($sql);
                while($result = db_fetch_array($res)){
                    $staffArray[$result['staff_id']]['email'] = $result['email'];
                    $staffArray[$result['staff_id']]['manager'] = $result['manager_id'];
                }
                $tickets->filter(
                                array('created__gt' =>$field["date range"][0],
                                        'created__lt' => $field["date range"][1] 
                                    ))->order_by("-created")->values(
                                                        "number",
                                                        "source_extra",
                                                        'status__name',
                                                        "odata__ticket_channel",
                                                        "odata__priority",
                                                        "created",
                                                        "odata__order_id",
                                                        "staff_id",
                                                        "triage__triage",
                                                        "triage__sla",
                                                        "triage__triage_time",
                                                        "triage__triage_again",
                                                        "staff__email",
                                                        "staff__firstname",
                                                        "staff__lastname",
                                                        "status__name",
                                                        "isoverdue",
                                                        "closed",
                                                        "topic_id");
                                    //echo  "<pre>";print_r($tickets);die;
                //$sql= "select issue_id,sub_issue_id,sub_sub_issue_id from clues_customer_queries where mst_ticket_id IN ()";
                //$result = db_query($sql);
                break;
            case 'sla adherence':
                $flag = "t_sla";
                switch ($reportSubSubtype) {
                    case 'agent level':
                        global $cfg;
                        $tickets = array();
                        $sql = "select staff_id,email,manager_id from mst_staff left join mst_staff_reporting on staff_id=agent_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            $staffArray[$result['staff_id']]['email'] = $result['email'];
                            $staffArray[$result['staff_id']]['manager'] = $result['manager_id'];
                        }
                        $TimeToTriage = $cfg->config['TimeToTriage']["value"];
                        $sql = "select count(*) as ticket_count,DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body,B.staff_id as staff_id,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within from mst_ticket A left join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') ".$filterString." group by DATE(B.created),B.staff_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            //print_r($result);
                            if($result['staff_id']){
                                $tickets[$result['thread_date']][$result['staff_id']]['triage_handled'] = $result['ticket_count'];
                                $tickets[$result['thread_date']][$result['staff_id']]["triage_handled_wsla"] = $result['within'];
                                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
                            }
                        }
                        //die;
                        $sql = "select count(*) as ticket_count,DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body,B.staff_id as staff_id,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within,poster as staff_name from mst_ticket A inner join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') ".$filterString." group by DATE(B.created),B.staff_id,poster";
                        $res = db_query($sql);
                        //print_r($res);die;
                        while($result = db_fetch_array($res)){
                        //echo"<pre>";print_r($result);
                            if($result['staff_id']){
                                if($result['title']=='Triage field chage'){
                                $tickets[$result['thread_date']][$result['staff_id']]['triage_again_handled'] += $result['ticket_count'];
                                $tickets[$result['thread_date']][$result['staff_id']]["triage_again_handled_wsla"] += $result['within'];
                                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
                                }
                            }
                            else{
                                //echo $result['staff_name'];
                                //print_r($staticFields['staff']);die;
                                $staff_id=array_search($result['staff_name'],$staticFields['staff']);
//echo $staff_id;die;
                                $tickets[$result['thread_date']][$staff_id]['triage_again_handled'] += $result['ticket_count'];
                                $tickets[$result['thread_date']][$staff_id]["triage_again_handled_wsla"] += $result['within'];
                                $tickets[$result['thread_date']][$staff_id]['email'] = $staffArray[$staff_id]['email'];
                                $tickets[$result['thread_date']][$staff_id]['manager'] = $staffArray[$staff_id]['manager'];

                            }
                        } 
                        break;
                    case 'issue type':
                        global $cfg;
                        $tickets = array();
                        $TimeToTriage = $cfg->config['TimeToTriage']["value"];
                        $sql = "select count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') ".$filterString." group by DATE(B.created),topic_id";
                        $res = db_query($sql);
                        while ($result = db_fetch_array($res)) {
                            $tickets[$result['thread_date']][$result['topic_id']]['triage']['ticket_count'] = $result['ticket_count'];
                            $tickets[$result['thread_date']][$result['topic_id']]['triage']['wsla'] = $result['within'];
                        }
                        $sql = "select count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') ".$filterString." group by DATE(B.created),topic_id";
                        $res = db_query($sql);
                        while ($result = db_fetch_array($res)) {
                            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['ticket_count'] = $result['ticket_count'];
                            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['wsla'] = $result['within'];
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 'aht':
                $flag = "t_aht";
                switch ($reportSubSubtype) {
                    case 'agent level':
                        global $cfg;
                        $tickets = array();
                        $sql = "select staff_id,email,manager_id from mst_staff left join mst_staff_reporting on staff_id=agent_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            $staffArray[$result['staff_id']]['email'] = $result['email'];
                            $staffArray[$result['staff_id']]['manager'] = $result['manager_id'];
                        }
                        $TimeToTriage = $cfg->config['TimeToTriage']["value"];
                        $sql = "select group_concat(A.ticket_id) as ids,count(*) as ticket_count,DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body,B.staff_id as staff_id,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within from mst_ticket A left join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') ".$filterString." group by DATE(B.created),B.staff_id";
                        $res = db_query($sql);
                        $count=0;
                        $ids = '';
                        while($result = db_fetch_array($res)){
                            if($ids!='')
                                $ids = $ids . ',' .$result['ids'];
                            else
                                $ids = $result['ids'];
                            $count++;
                            //print_r($result);
                            if($result['staff_id']){
                                $tickets[$result['thread_date']][$result['staff_id']]['triage_handled'] = $result['ticket_count'];
                                $tickets[$result['thread_date']][$result['staff_id']]["triage_handled_wsla"] = $result['within'];
                                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
                            }
                        }
                        if($filterString){
                            $newString = $filterString;
                            str_replace('B.created', 'last_timestamp', $newString);
                        }
                        $sql = "select count(*) as ticket_count,DATE(last_timestamp) as date,staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff from mst_tracking where event_name like '%Triaging Ticket%' and ticket_id in (".$ids.") ".$newString." group by DATE(last_timestamp),staff_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            $tickets[$result['date']][$result['staff_id']]['triage']['timediff'] = $result['timediff'];
                            $tickets[$result['date']][$result['staff_id']]['triage']['no_for_aht'] = $result['ticket_count'];
                            $tickets[$result['date']][$result['staff_id']]['triage_handled_aht'] = $result['timediff']/$result['ticket_count'];
                        }
                        $sql = "select group_concat(A.ticket_id) as ids,count(*) as ticket_count,DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body,B.staff_id as staff_id,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within,poster as staff_name from mst_ticket A inner join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') ".$filterString." group by DATE(B.created),B.staff_id,poster";
                        $res = db_query($sql);
                        $ids = '';
                        while($result = db_fetch_array($res)){
                            if($ids!='')
                                $ids = $ids . ',' .$result['ids'];
                            else
                                $ids = $result['ids'];
                            if($result['staff_id']){
                                if($result['title']=='Triage field chage'){
                                $tickets[$result['thread_date']][$result['staff_id']]['triage_again_handled'] += $result['ticket_count'];
                                $tickets[$result['thread_date']][$result['staff_id']]["triage_again_handled_wsla"] += $result['within'];
                                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
                                }
                            }
                            else{
                                //echo $result['staff_name'];
                                //print_r($staticFields['staff']);die;
                                $staff_id=array_search($result['staff_name'],$staticFields['staff']);
//echo $staff_id;die;
                                $tickets[$result['thread_date']][$staff_id]['triage_again_handled'] += $result['ticket_count'];
                                $tickets[$result['thread_date']][$staff_id]["triage_again_handled_wsla"] += $result['within'];
                                $tickets[$result['thread_date']][$staff_id]['email'] = $staffArray[$staff_id]['email'];
                                $tickets[$result['thread_date']][$staff_id]['manager'] = $staffArray[$staff_id]['manager'];

                            }
                        }
                        $sql = "select count(*) as ticket_count,DATE(last_timestamp) as date,staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff from mst_tracking where event_name like '%Triaging Ticket%' and ticket_id in (".$ids.") ".$newString." group by DATE(last_timestamp),staff_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            $tickets[$result['date']][$result['staff_id']]['triage_again']['timediff'] = $result['timediff'];
                            $tickets[$result['date']][$result['staff_id']]['triage_again']['no_for_aht'] = $result['ticket_count'];
                            $tickets[$result['date']][$result['staff_id']]['triage_again_handled_aht'] = $result['timediff']/$result['ticket_count'];
                        }
                        // $tickets->annotate(array('ticket_count'=>SqlAggregate::COUNT('triage__ticket_id')))->filter()->distinct("staff_id")->values("staff__firstname","staff__email","staff_id");
                        //echo"<pre>";print_r($tickets);die;
                        break;
                    case 'issue type':
                        global $cfg;
                        $tickets = array();
                        $TimeToTriage = $cfg->config['TimeToTriage']["value"];
                        $sql = "select group_concat(A.ticket_id),count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') ".$filterString." group by DATE(B.created),topic_id";
                        $res = db_query($sql);
                        $ids = '';
                        while ($result = db_fetch_array($res)) {
                            if($ids!='')
                                $ids = $ids . ',' .$result['ids'];
                            else
                                $ids = $result['ids'];
                            $tickets[$result['thread_date']][$result['topic_id']]['triage']['ticket_count'] = $result['ticket_count'];
                            $tickets[$result['thread_date']][$result['topic_id']]['triage']['wsla'] = $result['within'];
                        }
                        if($filterString){
                            $newString = $filterString;
                            str_replace('B.created', 'last_timestamp', $newString);
                        }
                        $sql = "select count(*) as ticket_count,DATE(last_timestamp) as date,staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff,topic_id from mst_ticket A inner join mst_tracking B on A.ticket_id=B.ticket_id where event_name like '%Triaging Ticket%' and B.ticket_id in (".$ids.") ".$newString." group by DATE(last_timestamp),topic_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            $tickets[$result['date']][$result['topic_id']]['triage']['timediff'] = $result['timediff'];
                            $tickets[$result['date']][$result['topic_id']]['triage']['no_for_aht'] = $result['ticket_count'];
                            $tickets[$result['date']][$result['topic_id']]['triage']['aht'] = $result['timediff']/$result['ticket_count'];
                        }
                        $sql = "select group_concat(A.ticket_id),count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<".$TimeToTriage.") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread_entry B on A.ticket_id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') ".$filterString." group by DATE(B.created),topic_id";
                        $res = db_query($sql);
                        $ids = '';
                        while ($result = db_fetch_array($res)) {
                            if($ids!='')
                                $ids = $ids . ',' .$result['ids'];
                            else
                                $ids = $result['ids'];
                            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['ticket_count'] = $result['ticket_count'];
                            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['wsla'] = $result['within'];
                        }
                        $sql = "select count(*) as ticket_count,DATE(last_timestamp) as date,staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff,topic_id from mst_ticket A inner join mst_tracking B on A.ticket_id=B.ticket_id where event_name like '%Triaging Ticket%' and B.ticket_id in (".$ids.") ".$newString." group by DATE(last_timestamp),topic_id";
                        $res = db_query($sql);
                        while($result = db_fetch_array($res)){
                            $tickets[$result['date']][$result['topic_id']]['triage_again']['timediff'] = $result['timediff'];
                            $tickets[$result['date']][$result['topic_id']]['triage_again']['no_for_aht'] = $result['ticket_count'];
                            $tickets[$result['date']][$result['topic_id']]['triage_again']['aht'] = $result['timediff']/$result['ticket_count'];
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 'pendency':
                $tickets = array();
                $flag = "t_pend";
                $sql = "select DATE(created) as date,count(*) as ticket_count from mst_ticket group by DATE(created)";
                $res = db_query($sql);
                $count=0;
                while($result = db_fetch_array($res)){
                    $count++;
                    $tickets[$result['date']]['triage_total'] = $result['ticket_count'];
                }
                $sql = "select DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body from mst_ticket A left join mst_thread_entry B on A.ticket_id=B.thread_id where DATE(A.created)=DATE(B.created) ".$filterString." and type='N';";
                $res = db_query($sql);
                while($result = db_fetch_array($res)){
                   if(($result['title']=='Status Changed' && strpos($result['body'],'New to Active on Triaged')!==false) || $result['title']=='Triaged'){
                           $tickets[$result['thread_date']]['triage_handled']++;
                    }
                    
                }
                $sql = "select DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body from mst_thread_entry B where type='N' ".$filterString;
                $res = db_query($sql);
                while($result = db_fetch_array($res)){
                    if(strpos($result['title'],'Triage again')!==false){
                        $tickets[$result['thread_date']]['triage_again_total']++;
                    }

                }
                 $sql = "select count(distinct B.thread_id) as thread_count,DATE(A.created) as thread_date,A.title as title1,B.title as title2  from mst_thread_entry A inner join mst_thread_entry B on A.thread_id=B.thread_id where DATE(A.created)=DATE(B.created) and A.id!=B.id and A.type=B.type and A.type='N' ".$filterString." group by DATE(A.created),A.title,B.title";
                $res = db_query($sql);
                while($result = db_fetch_array($res)){
                        if($result['title1']=='Triage again' && ($result['title2']=='Triage field chage' || $result['title2']=='Triage field change'))
                        $tickets[$result['thread_date']]['triage_again_handled'] += $result['thread_count'];

                }
                break;
            default:
                break;
        }
        break;
    
    default:
        # code...
        break;
}
$isExport = FALSE;
if($_GET['EXPORT']=='exportTrue')
    $isExport = TRUE;
//print_r($tickets);
if ($isExport) {
    //$ticketForImport = clone $tickets;
    //$ticketForImport->values('staff_id','staff__firstname','staff__lastname','isoverdue', 'team_id', 'ticket_id', 'number', 'source', 'status_id','status__name', 'dept_id','dept__name' ,'created','lastupdate', 'isanswered','thread_count','attach_count','odata__ord_fname','odata__ord_lname','odata__subject','odata__order_id','odata__priority');
$fp = fopen('../TempFile/TicketsData.csv', 'w+');

$header = array();
foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'] as $value => $filterdata) {
    $header[] = $value;
}

fputcsv($fp,$header);

if($flag=="t_pend"){
    foreach ($tickets as $key => $value) {
        $triage_pending = $value['triage_total']-$value['triage_handled'];
        $triage_again_pending = $value['triage_again_total']-$value['triage_again_handled'];
        $dataInCsv = array($key,$value['triage_total'],$value['triage_again_total'],$value['triage_handled'],$value['triage_again_handled'],$triage_pending,$triage_again_pending);
        fputcsv($fp,$dataInCsv);
    }
}
elseif ($flag=='t_sla') {
    if($reportSubSubtype=="agent level"){
        foreach ($tickets as $key => $value) {
            foreach ($value as $k => $val) {
                unset($triage_handled_total);
                unset($sla_total);
                unset($sla_triage);
                unset($sla_triage_again);
                $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                if(isset($val['triage_handled']) || isset($val['triage_again_handled']))
                $sla_total = ($val['triage_handled_wsla']?:0+$val['triage_again_handled_wsla']?:0)*100/($val['triage_handled']?:0+$val['triage_again_handled']?:0);
                
                if(isset($val['triage_handled'])){
                    $sla_triage = ($val['triage_handled_wsla']?:0)*100/$val['triage_handled'];
                }
                if(isset($val['triage_again_handled'])){
                    $sla_triage_again = ($val['triage_again_handled_wsla']?:0)*100/$val['triage_again_handled'];
                }
                
                $dataInCsv = array($key,$staticFields['staff'][$k],$val['email'],$staticFields['staff'][$val['manager']],$val['triage_handled'],$val['triage_again_handled'],$val['triage_handled'],$val['triage_again_handled'],$triage_handled_total,$sla_triage,$sla_triage_again,$sla_total);
                fputcsv($fp,$dataInCsv);
            }
        }
        
    }
    elseif ($reportSubSubtype == "issue type" )
    {
        foreach ($tickets as $key => $value) {
            foreach ($value as $topic_id => $val) {
                unset($triage_handled_total);
                unset($sla_total);
                unset($sla_triage);
                unset($sla_triage_again);
                $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                if(isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count']))
                {
                    $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                    $sla_total = ($val['triage']['wsla']?:0+$val['triage_again']['wsla']?:0)*100/($val['triage']['ticket_count']?:0+$val['triage_again']['ticket_count']?:0);
                }
                if(isset($k['triage_handled'])){
                    $sla_triage = ($val['triage']['wsla']?:0)*100/$val['triage']['ticket_count'];
                }
                if(isset($val['triage_again']['ticket_count'])){
                    $sla_triage_again = ($val['triage_again']['wsla']?:0)*100/$val['triage_again']['ticket_count'];
                }
                
                $dataInCsv = array($key,$staticTicketFields["helpTopic"][$parent_parent_id],$staticFields['helpTopic'][$parent_id],$staticFields['helpTopic'][$topic_id],$val['triage']['ticket_count'],$val['triage_again']['ticket_count'],$triage_handled_total,$sla_triage,$sla_triage_again,$sla_total);
                fputcsv($fp,$dataInCsv);
            }
        }        

            
        
    }
}

elseif ($flag == 't_aht'){
    if($reportSubSubtype=="agent level"){
        foreach ($tickets as $key => $value) {
            foreach ($value as $k => $val) {
                unset($triage_handled_total);
                if(isset($val['triage_handled']) || isset($val['triage_again_handled']))
                {
                    $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                }
                $aht_total = ($val['triage']['timediff']+$val['triage_again']['timediff'])/($val['triage']['no_for_aht']+$val['triage_again']['no_for_aht']);
                $dataInCsv = array($key,$staticFields['staff'][$k],$val['email'],$staticFields['staff'][$val['manager']],$val['triage_handled'],$val['triage_again_handled'],$triage_handled_total,$val['triage_handled_aht'],$val['triage_again_handled_aht'],$aht_total);
                fputcsv($fp,$dataInCsv);
            }
        }   
    }
    elseif ($reportSubSubtype == "issue type" )
    {
        foreach ($tickets as $key => $value) {
            foreach ($value as $topic_id => $val) {
                unset($triage_handled_total);
                $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                if(isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count']))
                {
                    $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                }
                $aht_total = ($val['triage']['timediff']+$val['triage_again']['timediff'])/($val['triage']['no_for_aht']+$val['triage_again']['no_for_aht']);
                $dataInCsv = array($key,$staticTicketFields["helpTopic"][$parent_parent_id],$staticFields['helpTopic'][$parent_id],$staticFields['helpTopic'][$topic_id],$val['triage']['ticket_count'],$val['triage_again']['ticket_count'],$triage_handled_total,$val['triage']['aht'],$val['triage_again']['aht'],$aht_total);
                fputcsv($fp,$dataInCsv);
            }
        } 
    }
}
elseif ($flag=="t_all") {
    $ticketForImport = clone $tickets;
    $ticketForImport->values('number','source_extra','status__name','odata__priority','created','odata__order_id','staff_id','staff__email','triage__triage_time','triage__triage','staff__firstname','staff__lastname','isoverdue','closed','topic_id');
    $count=$ticketForImport->count();

$chunkSize = 5000;
$chunkOffset=0;
while($chunkOffset<($count/$chunkSize)){
    $ticketForImport = clone $tickets;
    $ticketForImport->values('number','source_extra','status__name','odata__priority','created','odata__order_id','staff_id','staff__email','triage__triage_time','triage__triage','staff__firstname','staff__lastname','isoverdue','closed','topic_id');
    $ticketForImportTemp = $ticketForImport->limit($chunkSize)->offset($chunkSize*$chunkOffset)->all();
    $chunkOffset++;

    foreach ($ticketForImportTemp as $T){
        $topic_id = $T['topic_id'];
        $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
        $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
        $priorityName = $staticFields["priority"][$T["odata__priority"]]["name"];
        if(empty($priorityName))
            $priorityName = 'N/A';
        $staffName = trim($T['staff__firstname'].' '.$T['staff__lastname']);
        if(empty($staffName))
            $staffName = 'N/A';
        $orderedplacedBy = trim($T['odata__ord_fname'].' '.$T['odata__ord_lname']);
        if(empty($orderedplacedBy))
            $orderedplacedBy = 'N/A';
        if(empty($T['odata__subject']))
            $T['odata__subject'] = 'Without Subject';
        if(empty($T['odata__order_id']))
            $T['odata__order_id'] = 'N/A';
        $team_leader = $staticFields['staff'][$staffArray[$T['staff_id']]['manager']]?:'N/A';
        $isoverdue = ($T['isoverdue']==1)?'OSLA':'SLA';
        $dataInCsv = array($T['number'],$T['source_extra'],$T['status__name'],$priorityName,$T['created'],$T['odata__order_id'],$staticFields['helpTopic'][$parent_parent_id],$staticFields['helpTopic'][$parent_id],$staticFields['helpTopic'][$topic_id],'-',$staffName,$T['staff__email'],$team_leader,$T['triage__triage_time'],$T['triage__triage'],'-',$isoverdue);
        fputcsv($fp,$dataInCsv);
        unset($dataInCsv);
unset($isoverdue);
unset($parent_id);
unset($topic_id);
unset($isAnswered );
unset($staffName);
unset($priorityName);

    }
}
}
    

fclose($fp);
    echo '<div id="exportTicketData" style="display:none">YES</div>';
}
if($flag=="t_all"){
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$count = $tickets->count();
//echo $count;die;
$args = array();
parse_str($_SERVER['QUERY_STRING'], $args);

$pageNav = new Pagenate($count, $page, PAGE_LIMIT);

$pageNav->setURL('tickets.php', $args);
$tickets = $pageNav->paginate($tickets);
}
include('report-filter.php');

?>
<div id="customDivContainer" class="grid10 table_section float_right">
<div style="padding:5px;">
<div class="sticky bar opaque">
    <div class="content heading_action_section grid-row padding-container">
        <?php
        //echo stripos($reportSubtype,"hello");die;
        if(stripos($reportSubtype,"sla")!==false || stripos($reportSubtype,"aht")!==false){
        ?>
        <div class="pull-left flush-left" style="width:100%;">
            <label class="pull-left flush-left">
                <h3>Show activity by:</h3>
                
            </label>
            <div  class="">
                <!--
                <a class="type buttons">
                    All
                </a>
                -->
                <a href="tickets.php?report=1&type=<?php echo $reportType; ?>&subtype=<?php echo $reportSubtype ?>&subsubtype=agent level" class="type buttons <?php if(stripos($reportSubSubtype,'agent')!==false) echo 'checked';?>">
                    Agent Summary Report
                </a>
                <a href="tickets.php?report=1&type=<?php echo $reportType; ?>&subtype=<?php echo $reportSubtype ?>&subsubtype=issue type" class="type buttons <?php if(stripos($reportSubSubtype,'issue')!==false) echo 'checked';?>">
                    Issue Type Summary Report
                </a>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="clear"></div>
        <?php if(!empty($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['filters'])){ ?>
        <div class="pull-left flush-left" style="width:100%;">
            <label class="pull-left flush-left">
                <h3>Filter by:</h3>
            </label>
            <div class="">
                <?php
                    foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['filters'] as $value => $filterdata) {
                        ?>
                        <button filter-data='<?php echo implode('-',explode(' ',$value)); ?>' class='filters <?php if($field[strtolower($value)]) echo "selected"; ?>'>
                            <?php echo $value; ?>
                        </button>
                        <?php
                    }
                ?>
            </div>

            <?php
            foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['filters'] as $value => $filterdata){
                $getParam = explode('?', $_SERVER['REQUEST_URI']);
                if(!empty($getParam[1])){
                    
                    $appendInUrl = $getParam[1].'&';
                    
                }
            ?>
                <form action="?<?php echo $appendInUrl; ?>" style="display:none;padding-top:10px;" class="filterdata" id="<?php echo implode('-',explode(' ',$value)); ?>" class="pull-left flush-left">
                    <?php
                    if(stripos($value,'date')!==false){
                        foreach ($filterdata as $key => $val) {
                            echo $val."<input type='date'>";
                        
                            
                        }
                    }
                    else
                    {
                        foreach ($filterdata as $key => $val) {
                            echo $val."<select></select>";
                        }
                    }
                    ?>
                </form>
                <?php
            }
            ?>
        </div>
        <?php } ?>
    </div>
</div>
<div class="clear"></div>

<form action="tickets.php" method="POST" name='tickets' id="tickets">
<?php csrf_token(); ?>
 <input type="hidden" name="a" value="mass_process" >
 <input type="hidden" name="do" id="action" value="" >
 <input type="hidden" name="status" value="<?php echo
 Format::htmlchars($_REQUEST['status'], true); ?>" >
 <table class="list" border="0" cellspacing="1" cellpadding="2" width="940">
    <thead>
        <tr>
            <?php
            foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'] as $value => $val) {
                echo "<th >".$value."</th>";
            }
            ?>
    
        </tr>
        
     </thead>
     <tbody>
        <?php
        $total=0;
        if($flag=="t_sla"){
            if($reportSubSubtype=="agent level"){
                foreach ($tickets as $key => $value) {
                    $total++;
                    foreach ($value as $k => $val) {
                        unset($triage_handled_total);
                        unset($sla_total);
                        unset($sla_triage);
                        unset($sla_triage_again);
                    ?>    
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $staticFields['staff'][$k]?:'-'; ?></td>
                        <td><?php echo $val['email']?:'-'; ?></td>
                        <td><?php echo $staticFields['staff'][$val['manager']]?:'-'; ?></td>
                        <td><?php echo $val['triage_handled']?:'-'; ?></td>
                        <td><?php echo $val['triage_again_handled']?:'-'; ?></td>
                        <?php
                        if(isset($val['triage_handled']) || isset($val['triage_again_handled']))
                        {
                            $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                            $sla_total = (($val['triage_handled_wsla']?:0)+($val['triage_again_handled_wsla']?:0))*100/(($val['triage_handled']?:0)+($val['triage_again_handled']?:0));
                        }
                        if(isset($val['triage_handled'])){
                            $sla_triage = ($val['triage_handled_wsla']?:0)*100/$val['triage_handled'];
                        }
                        if(isset($val['triage_again_handled'])){
                            $sla_triage_again = ($val['triage_again_handled_wsla']?:0)*100/$val['triage_again_handled'];
                        }
                        ?>
                        <td><?php echo isset($triage_handled_total)?$triage_handled_total:'-'; ?></td>
                        <td><?php echo isset($sla_triage)?round($sla_triage,2):'-'; ?></td>
                        <td><?php echo isset($sla_triage_again)?round($sla_triage_again,2):'-'; ?></td>
                        <td><?php echo isset($sla_total)?round($sla_total,2):'-'; ?></td>
                    </tr>
                    <?php
                    }
                }
            }
            elseif ($reportSubSubtype =="issue type"){
                foreach ($tickets as $key => $value) {
                    $total++;
                    foreach ($value as $topic_id => $val) {
                        unset($triage_handled_total);
                        unset($sla_total);
                        unset($sla_triage);
                        unset($sla_triage_again);
                        $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                        $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                        ?>
                        <tr>
                            <td><?php echo $key; ?></td>
                            <td><?php echo $staticTicketFields["helpTopic"][$parent_parent_id]; ?></td>
                            <td><?php echo $staticFields['helpTopic'][$parent_id]; ?></td>
                            <td><?php echo $staticFields['helpTopic'][$topic_id]; ?></td>
                            <td><?php echo $val['triage']['ticket_count']?:'-'; ?></td>
                            <td><?php echo $val['triage_again']['ticket_count']?:'-'; ?></td>
                            <?php
                            if(isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count']))
                            {
                                $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                                $sla_total = (($val['triage']['wsla']?:0)+($val['triage_again']['wsla']?:0))*100/(($val['triage']['ticket_count']?:0)+($val['triage_again']['ticket_count']?:0));
                            }
                            if(isset($val['triage']['ticket_count'])){
                                $sla_triage = ($val['triage']['wsla']?:0)*100/$val['triage']['ticket_count'];
                            }
                            if(isset($val['triage_again']['ticket_count'])){
                                $sla_triage_again = ($val['triage_again']['wsla']?:0)*100/$val['triage_again']['ticket_count'];
                            }
                            ?>
                            <td><?php echo isset($triage_handled_total)?$triage_handled_total:'-'; ?></td>
                            <td><?php echo isset($sla_triage)?round($sla_triage,2):'-'; ?></td>
                            <td><?php echo isset($sla_triage_again)?round($sla_triage_again,2):'-'; ?></td>
                            <td><?php echo isset($sla_total)?round($sla_total,2):'-'; ?></td>
                            
                        </tr>
                        <?php
                    }
                }        
            }
        }
        elseif ($flag=="t_aht") {
            if($reportSubSubtype =="agent level"){
                foreach ($tickets as $key => $value) {
                    $total++;
                    foreach ($value as $k => $val) {
                        unset($triage_handled_total);
                    ?>    
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $staticFields['staff'][$k]?:'-'; ?></td>
                        <td><?php echo $val['email']?:'-'; ?></td>
                        <td><?php echo $staticFields['staff'][$val['manager']]?:'-'; ?></td>
                        <td><?php echo $val['triage_handled']?:'-'; ?></td>
                        <td><?php echo $val['triage_again_handled']?:'-'; ?></td>
                        <?php
                        if(isset($val['triage_handled']) || isset($val['triage_again_handled']))
                        {
                            $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                        }
                        ?>
                        <td><?php echo isset($triage_handled_total)?$triage_handled_total:'-'; ?></td>
                        <td><?php echo $val['triage_handled_aht']?:'-'; ?></td>
                        <td><?php echo $val['triage_again_handled_aht']?:'-'; ?></td>
                        <?php $aht_total = ($val['triage']['timediff']+$val['triage_again']['timediff'])/($val['triage']['no_for_aht']+$val['triage_again']['no_for_aht']); ?>
                        <td><?php echo isset($aht_total)?round($aht_total,2):'-'; ?></td>
                    </tr>
                    <?php
                    }
                }
            }
            elseif ($reportSubSubtype =="issue type") {
                foreach ($tickets as $key => $value) {
                    $total++;
                    foreach ($value as $topic_id => $val) {
                        unset($triage_handled_total);
                        unset($sla_total);
                        unset($sla_triage);
                        unset($sla_triage_again);
                        $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                        $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                        ?>
                        <tr>
                            <td><?php echo $key; ?></td>
                            <td><?php echo $staticTicketFields["helpTopic"][$parent_parent_id]; ?></td>
                            <td><?php echo $staticFields['helpTopic'][$parent_id]; ?></td>
                            <td><?php echo $staticFields['helpTopic'][$topic_id]; ?></td>
                            <td><?php echo $val['triage']['ticket_count']?:'-'; ?></td>
                            <td><?php echo $val['triage_again']['ticket_count']?:'-'; ?></td>
                            <?php
                            if(isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count']))
                            {
                                $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                            }
                            
                            ?>
                            <td><?php echo isset($triage_handled_total)?$triage_handled_total:'-'; ?></td>
                            <td><?php echo $val['triage']['aht']?:'-'; ?></td>
                            <td><?php echo $val['triage_again']['aht']?:'-'; ?></td>
                            <?php $aht_total = ($val['triage']['timediff']+$val['triage_again']['timediff'])/($val['triage']['no_for_aht']+$val['triage_again']['no_for_aht']); ?>
                            <td><?php echo isset($aht_total)?round($aht_total,2):'-'; ?></td>
                            
                        </tr>
                        <?php
                    }
                }        
            }
        }
        else if($flag=="t_pend")
        {
            foreach ($tickets as $key => $value) {
                $total++;
                ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php echo $value['triage_total']?:'-'; ?></td>
                    <td><?php echo $value['triage_again_total']?:'-'; ?></td>
                    <td><?php echo $value['triage_handled']?:'-'; ?></td>
                    <td><?php echo $value['triage_again_handled']?:'-'; ?></td>
                    <?php
                    $triage_pending = $value['triage_total']-$value['triage_handled'];
                    $triage_again_pending = $value['triage_again_total']-$value['triage_again_handled'];
                    ?>
                    <td><?php echo $triage_pending?:'-'; ?></td>
                    <td><?php echo $triage_again_pending?:'-'; ?></td>
                </tr>
                <?php
            }
        }
        else{
            foreach ($tickets as $key => $value) { 
                $total++;
                //echo"<pre>";print_r($value);die;
                ?>
                <tr>
                <?php
                foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'] as $k => $val) {
                    if($val){
                        
                        if($val == "triage__triage"){
                            if(!empty($value[$val])|| $value[$val]==0){
                                if($value[$val.'_again']=='0000-00-00 00:00:00'){
                                    echo "<td>".$staticFields['Triage']['Triage Status'][$value[$val]]."</td>";
                                }
                                else{
                                    echo "<td>Triage Again</td>";
                                }
                            }
                            else{
                                echo "<td>Not Triaged</td>";
                            }
                        }
                        else if($val == "isoverdue")
                        {
                            if($value['closed']==NULL || $value['closed']=='0000-00-00 00:00:00'){
                                if($value["est_duedate"]>date('Y-m-d G:i:s',time()))
                                {
                                    echo "<td>SLA</td>";
                                }
                                else
                                {
                                    echo "<td>OSLA</td>";
                                }
                            }
                            else
                            {
                                echo "<td>-</td>";
                            }
                        }
                        else
                            echo "<td>".$value[$val]."</td>";
                        
                    }
                    else
                        echo "<td>-</td>";
                }
                ?>
                </tr>
                <?php
            }
        }
        ?>
        <?php
        

         if($viewAccessDept=='0' || $viewAccessTeam=='0'){
//echo '<tr><td colspan="7">'.__('There are no tickets matching your criteria.!').'</td></tr>'; //-- commented by akash Kumar for javascript loading issue
         }
        // Setup Subject field for display
        $class = "row1";
        $ids=($errors && $_POST['tids'] && is_array($_POST['tids']))?$_POST['tids']:null;
//        echo '<pre>';print_r($tickets);
        //custom fields - Akash
        // foreach ($tickets as $T) { 
        //     $tidArr[$T["ticket_id"]] = $T["number"];
        // }
        $i=0;
        if($customFieldsToDisplay)
            $allCustomDataArr=getTicketCustomFields($tidArr);
        //echo "<pre>";print_r($staffData);
        if (!$total)
            $ferror=__('There are no tickets matching your criteria.');
        ?>
    </tbody>
    <tfoot>
     <tr>
        <td colspan="7">
            <?php if($total && $thisstaff->canManageTickets()){ ?>
            <?php echo __('Select');?>:&nbsp;
            <a id="selectAll" href="#ckb"><?php echo __('All');?></a>&nbsp;&nbsp;
            <a id="selectNone" href="#ckb"><?php echo __('None');?></a>&nbsp;&nbsp;
            <a id="selectToggle" href="#ckb"><?php echo __('Toggle');?></a>&nbsp;&nbsp;
            <?php }else{
                echo '<i>';
                echo $ferror?Format::htmlchars($ferror):__('Query returned 0 results.');
                echo '</i>';
            } ?>
        </td>
     </tr>
    </tfoot>
    </table>
    <?php
    $getParam = explode('?', $_SERVER['REQUEST_URI']);
    //print_r($_SERVER['REQUEST_URI']);die;
    if(!empty($getParam[1])){
        //echo"hello";die;
        $appendInUrl = $getParam[1].'&';
        //echo $appendInUrl;die;
    }
    if ($total>0) { //if we actually had any tickets returned.

        echo '<div class="paging">&nbsp;'.__('Page').':&nbsp;';
        
        echo '<div class="export_bttm"><a class="export-csv no-pjax" href="?'.$appendInUrl.Http::build_query(array(
                        'EXPORT' => 'exportTrue')).'">Export</a>';
        echo '&nbsp;<i class="help-tip icon-question-sign" href="#export"></i></div></div>';
    } ?>
    </form>  
    </div>  
</div>
</div>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="mark_overdue-confirm">
        <?php echo __('Are you sure you want to flag the selected tickets as <font color="red"><b>overdue</b></font>?');?>
    </p>
    <div><?php echo __('Please confirm to continue.');?></div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="button" value="<?php echo __('No, Cancel');?>" class="close">
        </span>
        <span class="buttons pull-right">
            <input type="button" value="<?php echo __('Yes, Do it!');?>" class="confirm">
        </span>
     </p>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    $('.datetimepicker').datetimepicker({
 formatDate:'d/m/Y H:i'});
    // $('.filters').on('click',function(){
    //  var element = $(this);
 //        $(".filterdata").hide();
    //  $("#"+element.attr('filter-data')).show();
    // });

</script>

<?php
/**
 * function to give search parameters of advanced search
 * @param type $search search Object
 * @param type $customForm form Object
 * @return string of search parameters to be displayed
 * @author Akash Kumar
 */
function getSearchParameters($search,$customForm){
    if(!stristr($_SERVER['QUERY_STRING'],"advance"))
            return;
    $searchBasedParam = $search->getSearchFields($customForm);
    foreach($searchBasedParam as $searchCat){
            if(!$searchCat["active"])
                continue;
            foreach($searchCat["value"] as $k=>$param){
               if(isset($param["name"])){
                   $searchFields = $searchFields." ".$param["name"];
               }else{ 
                 $searchFields = $searchFields." ".$param;
               }
           }
           $searchBasedOn[$searchCat["field"]->getLabel()]["method"] = $searchCat["method"];
           $searchBasedOn[$searchCat["field"]->getLabel()]["value"] = $searchFields;
           $searchParamCustom .= "<span class='asField'>".$searchCat["field"]->getLabel()."</span> <span class='asMethod'>".$searchCat["method"]."</span> <span class='asValue'>".$searchFields."</span> <br>";
           $searchFields="";
    }
    return $searchParamCustom;
}
