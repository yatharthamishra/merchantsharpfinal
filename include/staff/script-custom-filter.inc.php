$(function() { 
    accorditionSetUp();
  });
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
  
  function accorditionSetUp(){
      $( "#accordion" ).accordion();
    $(".ui-accordion-content").css("height","");
    var triage_id =<?php echo isset($_REQUEST['triageId']) ? $_REQUEST['triageId'] : 0 ?>;
    var sub_triage =<?php echo isset($_REQUEST['subTriage']) ? $_REQUEST['subTriage'] : 0 ?>;
    var updateId = <?php echo isset($_REQUEST['update']) ? $_REQUEST['update'] : 0 ?>;
    var priorityId = <?php echo isset($_REQUEST['priority']) ? $_REQUEST['priority'] : 0 ?>;
    var commId = '<?php echo isset($_REQUEST['communication']) ? $_REQUEST['communication'] : (isset($_REQUEST['call']) ? $_REQUEST['call'].$_REQUEST['call'] : 0) ?>';
    var status = '<?php echo isset($_REQUEST['status']) ? $_REQUEST['status'] : 0 ?>';
    var depId = <?php echo isset($_REQUEST['deptId']) ? $_REQUEST['deptId'] : 0 ?>;
    var task = <?php echo isset($_REQUEST['task']) ? $_REQUEST['task'] : 0 ?>;
    var unType = '<?php echo isset($_REQUEST['type']) ? $_REQUEST['type'] : 0 ?>';
    var taskDepartment = '<?php echo isset($_REQUEST['taskdept']) ? $_REQUEST['taskdept'] : 0 ?>';

 
    var accDept = '<?php echo isset($acc["dept"]) ? $acc["dept"] : 0 ?>';//4;
    var accUnassigned = '<?php echo isset($acc["unassigned"]) ? $acc["unassigned"] : 0 ?>';//0;
    var accTicket = '<?php echo isset($acc["ticket"]) ? $acc["ticket"] : 0 ?>';//1;
    var accTask = '<?php echo isset($acc["Unassigned Tasks"]) ? $acc["Unassigned Tasks"] : 0 ?>';//2;
    var accTaskRespond = '<?php echo isset($acc["Tasks Respond"]) ? $acc["Tasks Respond"] : 0 ?>';//2;
    var accTriage = '<?php echo isset($acc["triage"]) ? $acc["triage"] : 0 ?>';//3;
    var accStatus = '<?php echo isset($acc["status"]) ? $acc["status"] : 0 ?>';//5;
    var rrsla = '<?php echo isset($acc["rrsla"]) ? $acc["rrsla"] : 0 ?>';//5;
    var accTaskDepartment='<?php echo isset($acc["taskDept"]) ? $acc["taskDept"] : 0 ?>';//6;   
        console.log(window.location.toString());
    if(window.location.toString().search("taskdept")!=-1){
        $("#ui-accordion-accordion-header-"+accTaskDepartment).click();
        $($( "a[taskDept='"+taskDepartment+"']" ).closest("ul:hidden")).show();
        $($($($($( "a[taskDept='"+taskDepartment+"']" ).addClass('active').closest("ul"))[0]).parent("li")).closest("ul")).show();
    }else if(window.location.toString().search("dept")!=-1){
        $("#ui-accordion-accordion-header-"+accDept).click();
        $($( "a[dep='"+depId+"']" ).closest("ul:hidden")).show();
        $($($($($( "a[dep='"+depId+"']" ).addClass('active').closest("ul"))[0]).parent("li")).closest("ul")).show();
    }
    else if(window.location.toString().search("unassigned=")!=-1){
        $("#ui-accordion-accordion-header-"+accUnassigned).click();
        $( "a[update='un"+unType+"']" ).addClass('active');
    }
    else if(window.location.toString().search("respond")!=-1 && window.location.toString().search("ticket=1")!=-1){
        $("#ui-accordion-accordion-header-"+accTicket).click();
        $($( "a[update='p"+updateId+""+priorityId+"']" ).addClass('active').closest("ul:hidden")).show();
        if(commId)
            $($( "a[update='c"+updateId+""+priorityId+""+commId+"']" ).addClass('active').closest("ul:hidden")).show();
        $($($($($( "a[update='"+updateId+"']" ).addClass('active').closest("ul"))[0]).parent("li")).closest("ul")).show();
    }
    else if(window.location.toString().search("edRespond")!=-1 ){
        $("#ui-accordion-accordion-header-"+accTicket).click();
        var ticketSource=getUrlParameter('source');
        if(ticketSource!=undefined){
        $("a[data-link='"+ticketSource+"']").addClass('active');
        }
        if(getUrlParameter('ticketState')!=undefined){
            var dataLink=getUrlParameter('ticketState');
            $("a[data-link='"+ticketSource+dataLink+"']").addClass('active');
        };
    }
    else if(window.location.toString().search("rr")!=-1 ){
        $("#ui-accordion-accordion-header-"+rrsla).click();
        var rrSLA='rr'+getUrlParameter('rr');
        if(rrSLA!=undefined){
        $("a[data-link='"+rrSLA+"']").addClass('active');
        }
    }
    else if(window.location.toString().search("respond")!=-1 && window.location.toString().search("task=1")!=-1 && window.location.toString().search("selfStaff=1")!=-1){
        $("#ui-accordion-accordion-header-"+accTaskRespond).click();
        $($("#ui-accordion-accordion-panel-"+accTaskRespond).find("a[update='task_p"+updateId+""+priorityId+"']" ).addClass('active').closest("ul:hidden")).show();  
        if(commId)
            $($("#ui-accordion-accordion-panel-"+accTaskRespond).find("a[update='c"+updateId+""+priorityId+""+commId+"']").addClass('active').closest("ul:hidden")).show();
        $($($($($("#ui-accordion-accordion-panel-"+accTaskRespond).find("a[update='task_"+updateId+"']").addClass('active').closest("ul"))[0]).parent("li")).closest("ul")).show();
    }
    else if(window.location.toString().search("respond")!=-1 && window.location.toString().search("task=1")!=-1){
        $("#ui-accordion-accordion-header-"+accTask).click();
        $($("#ui-accordion-accordion-panel-"+accTask).find("a[update='task_p"+updateId+""+priorityId+"']" ).addClass('active').closest("ul:hidden")).show();  
        if(commId)
            $($("#ui-accordion-accordion-panel-"+accTask).find("a[update='c"+updateId+""+priorityId+""+commId+"']").addClass('active').closest("ul:hidden")).show();
        $($($($($("#ui-accordion-accordion-panel-"+accTask).find("a[update='task_"+updateId+"']").addClass('active').closest("ul"))[0]).parent("li")).closest("ul")).show();
    }
    else if(window.location.toString().search("triageId=")!=-1){
        $("#ui-accordion-accordion-header-"+accTriage).click();    
        //$("#triage-"+triage_id+" a").addClass('active');
        if(sub_triage){
            $( "a[update='t"+triage_id+""+sub_triage+"']" ).addClass('active');
        }else{
            $( "a[update='t"+triage_id+"']" ).addClass('active');
        }
    }
    else if(window.location.toString().search("status=")!=-1){
         $("#ui-accordion-accordion-header-"+accStatus).click();
         $("#"+status+" a").addClass('active');
    }
  }
  $(".ui-accordion-header").click(function(){
      //accorditionSetUp();
  })
  
  $(".linkOnList").click(function(){
      var dep = $(this).attr("dep");
      if($(".dep"+dep).length>0 && $(".dep"+dep+":visible").length==0){
        $(".dep"+dep).slideDown();
        return false;
      }else{
          return true;
      }
  });
  $(".linkOnUpdateList").click(function(){
	if($(this).hasClass('nodown')){
		return true;
	}
      var up = $(this).attr("update");
      if($(".update"+up).length>0 && $(".update"+up+":visible").length==0){
        $(".update"+up).slideDown();
        return false;
      }else{
          return true;
      }
  });
