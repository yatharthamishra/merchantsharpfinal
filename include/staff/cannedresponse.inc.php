<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Access Denied');
$info=$qs = array();
if($canned && $_REQUEST['a']!='add'){
    $title=__('Update Canned Response');
    $action='update';
    $submit_text=__('Save Changes');
    $info=$canned->getInfo();
    $info1=$canned->getCustomInfo($info['canned_id']); // For fetching custom fieldsss
    //--------------------------------
    // IF no staff found then show the list of staff corrosponding to department
    $cnd =empty($info1['staff_id']);
    if($cnd)
    {
      $info2 = $canned->getAssigneeForDept($info['dept_id']);
      if(!empty($info1))
      {
        $info2 = json_decode($info1,true);
      }
    }
    //---------------------------------
    $info['id']=$canned->getId();
    $qs += array('id' => $canned->getId());
    // Replace cid: scheme with downloadable URL for inline images
    $info['response'] = $canned->getResponseWithImages();
    $info['notes'] = Format::viewableImages($info['notes']);
}else {
    $title=__('Add New Canned Response');
    $action='create';
    $submit_text=__('Add Response');
    $info['isenabled']=isset($info['isenabled'])?$info['isenabled']:1;
    $qs += array('a' => $_REQUEST['a']);
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);

?>
<form action="canned.php?<?php echo Http::build_query($qs); ?>" method="post" id="save" enctype="multipart/form-data">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
 <h2><?php echo __('Canned Response')?>
 &nbsp;<i class="help-tip icon-question-sign" href="#canned_response"></i></h2>
 <table class="form_table fixed" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr><td></td><td></td></tr> <!-- For fixed table layout -->
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
                <em><?php echo __('Canned response settings');?></em>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required"><?php echo __('Status');?>:</td>
            <td>
                <label><input type="radio" name="isenabled" value="1" <?php
                    echo $info['isenabled']?'checked="checked"':''; ?>>&nbsp;<?php echo __('Active'); ?>&nbsp;</label>
                <label><input type="radio" name="isenabled" value="0" <?php
                        echo !$info['isenabled']?'checked="checked"':''; ?>>&nbsp;<?php echo __('Disabled'); ?>&nbsp;</label>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['isenabled']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Department');?>:</td>
            <td>

                <select name="dept_id[]" multiple="multiple" onchange="getAssignee()" id="dept_id" class="select2-hidden-accessible">
                    <!-- <option value="0">&mdash; <?php echo __('Select Departments');?> &mdash;</option> -->
                    <?php
                    if (($depts=Dept::getDepartments())) {
                      echo "<optgroup label='Select Departments'>";
                        foreach($depts as $id => $name) {
                            $deptIds = explode(',', $info['extra_dept_id']);
                            $selected=($info['dept_id'] && ($id==$info['dept_id'] || in_array($id, $deptIds)))?'selected="selected"':'';
                            echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                        }
                      echo "</optgroup>";
                    }
                    ?>
                </select>
                
                
                
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['dept_id']; ?></span>
            </td>
        </tr>
        <!-- Assignee drop down menu's :by vishal -->
        <tr id="assignee_staff_title" style="display:none;">
          <td width="180" class="required"><?php echo __('Assignee');?>:</td>
        <td>
        <select name="assignee_staff" id="assignee_staff">
          <option value="0">&mdash; <?php echo __('Select Assignee');?> &mdash;</option>
          <?php 
          if(!$cnd)
          {
            ?>
          <option value=<?php echo $info1['staff_id']; ?> selected="selected">&mdash; <?php echo $info1['firstname'].' '.$info1['lastname'] ;?> &mdash;</option>
          <?php
          }
          else{
            foreach ($info2 as $k=> $v)
            {
             ?>
          <option value=<?php echo $v['assignee_id']; ?>>&mdash; <?php echo $v['firstname'].' '.$v['lastname'] ;?> &mdash;</option>
          <?php
            }
          }
            ?>
        </select>
        </td>
        </tr>
          <tr>
          <!-- Status Drop down menu's -->  
          <td width="180" class="required"><?php echo __('Status');?>:</td>
          <td>
          <select name="status" id="status">
           <option value="0">&mdash; <?php echo __('Select Status');?> &mdash;</option>
           <?php
           $sql = "SELECT id,name FROM ".TABLE_PREFIX."ticket_status";
           if(($res=db_query($sql)) && db_num_rows($res))
           {
             while(list($id,$name)=db_fetch_row($res))
             {
              $selected=($info1['status'] && $id==$info1['status'])?'selected="selected"':'';
              echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
              
             }
           }
           ?>
          </select>
          </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Canned Response');?></strong>: <?php echo __('Make the title short and clear.');?>&nbsp;</em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
                <div><b><?php echo __('Title');?></b><span class="error">*&nbsp;<?php echo $errors['title']; ?></span></div>
                <input type="text" size="70" name="title" value="<?php echo $info['title']; ?>">
                <br><br>
                <div style="margin-bottom:0.5em"><b><?php echo __('Canned Response'); ?></b>
                    <font class="error">*&nbsp;<?php echo $errors['response']; ?></font>
                    &nbsp;&nbsp;&nbsp;(<a class="tip" href="#ticket_variables"><?php echo __('Ticket Variables'); ?></a>
                    | <a class="tip" href="#order_variables"><?php echo __('Order Variables'); ?></a>                  | <a class="tip" href="#return_variables"><?php echo __('Return Variables'); ?></a>)
                    </div><input type="text" id="copyTarget" value="" style="color: #fff;border: #fff;width: 1px;height: 1px"> 
                <textarea name="response" cols="21" rows="12"
                    data-root-context="cannedresponse"
                    style="width:98%;" class="richtext draft draft-delete" <?php
    list($draft, $attrs) = Draft::getDraftAndDataAttrs('canned',
        is_object($canned) ? $canned->getId() : false, $info['response']);
    echo $attrs; ?>><?php echo $draft ?: $info['response'];
                ?></textarea>
                <div><h3><?php echo __('Canned Attachments'); ?> <?php echo __('(optional)'); ?>
                &nbsp;<i class="help-tip icon-question-sign" href="#canned_attachments"></i></h3>
                <div class="error"><?php echo $errors['files']; ?></div>
                </div>
                <?php
                $attachments = $canned_form->getField('attachments');
                if ($canned && $attachments) {
                    $attachments->setAttachments($canned->attachments);
                }
                print $attachments->render(); ?>
                <br/>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Internal Notes');?></strong>: <?php echo __('Notes about the canned response.');?>&nbsp;</em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
                <textarea class="richtext no-bar" name="notes" cols="21"
                    rows="8" style="width: 80%;"><?php echo $info['notes']; ?></textarea>
            </td>
        </tr>
    </tbody>
</table>
 <?php if ($canned && $canned->getFilters()) { ?>
    <br/>
    <div id="msg_warning"><?php echo __('Canned response is in use by email filter(s)');?>: <?php
    echo implode(', ', $canned->getFilters()); ?></div>
 <?php } ?>
<p style="padding-left:225px;">
 <input type="submit" id="newCannsubmit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="<?php echo __('Reset'); ?>" onclick="javascript:
        $(this.form).find('textarea.richtext')
            .redactor('deleteDraft');
        location.reload();" />
    <input type="button" name="cancel" value="<?php echo __('Cancel'); ?>" onclick='window.location.href="canned.php"'>
</p>
</form>
<div class="alert-box-copy copySuccess">Text copied to clipboard, paste on required position!!</div>


<style>
    .alert-box-copy {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;  
    }

    .copySuccess {
    color: #000;
    background-color: rgba(76, 195, 74, 0.99);
    border-color: #d6e9c6;
    top: 190px;
    left: 12%;
    font-size: 20px;
    position: fixed;
    height: 30px;
    width: 641px;
    display: none;
    }
</style>
<!-- Java-script for getting assignee for canned responses:by vishal -->
<script type="text/javascript">
$(document).ready(function(){
  $('#dept_id').select2({ 
    "placeholder" : "Select Departments",
    'width': '350px'});
});
$("body").on("click", ".variables td", function(){
    if($(this).text().indexOf("%{")!=-1){
        $(".icon-remove-circle").click();
        $("#copyTarget").val($(this).text());
        copyToClipboard(document.getElementById("copyTarget"));
        $("div.copySuccess").fadeIn(300).delay(1300).fadeOut(400);
        //var textBoxField = $(".redactor-editor").html();
        //textBoxField = textBoxField+$(this).text();
        //$(".redactor-editor").html(textBoxField);
    }
});


function copyToClipboard(elem) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

  // Check if id param exists (i.e canned response is in edit mode or not)
    var params = getSearchParameters();
    var keys=Object.keys(params); 
    if(keys=='id')
    {
     $("#assignee_staff_title").show();
    } 
  
   function getAssignee()
   {
      var deptIds = $("#dept_id").val();
      if(deptIds!= null)
        var deptId = deptIds.shift();
      else
        var deptId = null;
     //var deptId = document.getElementById("dept_id").value;
     if(deptId==='0')
     {
       alert("Please Select Department");
       return false;
     }
     if(deptId != null)
     {
        $.ajax({
           url:'ajax.php/kb/getAss-Canned/'+deptId,
           data:{"other_dept_id":deptIds},
           type:"get",
           success: function(json)
           {
             if(json !='[]')
             {
                var resData = JSON.parse(json, function (key, value) {
                var type;
                if (value && typeof value === 'object') {
                type = value.type;
                if (typeof type === 'string' && typeof window[type] === 'function') {
                    return new (window[type])(value);
                }
               }
              return value;
             });
             
             // Loop throgh result to add dynamic assignees
             var len = resData.length;
             $('select#assignee_staff').html('<option value="0">Select Assignee</option>');
             for(i=0; i<len; i++)
             {
               var assigneeid=resData[i].assignee_id;
               var fname=resData[i].firstname;
               var lname=resData[i].lastname;
               $('select#assignee_staff').append('<option value='+assigneeid+'>'+fname+' '+lname+'</option>');
               $("#assignee_staff_title").show();
               $("#assignee_staff").show();
             }
              
             }
             else
             {
               $('#assignee_staff').html('<option value="0">Select Assignee</option>');
               alert("No assignee for selected department.");
             }
           }
           
        });
     }
     else
     {
       alert("Error in Passing Department Id");
     }
   }
   
   function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

// Basic validations
$("#newCannsubmit").click(function()
{
  var statusId = document.getElementById("status").value;
  var dept_id = document.getElementById("dept_id").value;
  if(!dept_id)
  {
    alert("please select a department");
    return false;
  }
  else if(statusId=='0')
  {
    alert("Please choose status");
    return false;
  }
});

</script>