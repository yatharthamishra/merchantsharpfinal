<table class="list" width="100%";>
    <form name="ticketFlow" action="ticketflow.php" method="POST">
        <thead>
            <tr><th colspan="8"><?php echo __('Ticket Completion machine'); ?></th></tr>
        </thead>
        <tbody>
            <tr><th><?php echo __('Help Topic'); ?></th>
                <th><?php echo __('Status Type'); ?></th>
                <th><?php echo __('Order Status'); ?></th>
                <th ><?php echo __('Remove'); ?></th>
            </tr>
        </tbody>
        <?php
        $emailTemp = EmailTemplateNew::objects()->filter(array("code_name__startswith"=>"order"))->values("id","subject")->all();
         foreach ($emailTemp as $template) { 
             $emailTempArr[$template["id"]] = $template["subject"];
         }
        $staticFields = getCachedTicketFields();
        $taskStaticFields = getCachedTaskFields();
        ?>

        <?php foreach (TicketFlow::objects()->all() as $flow) {
            $ticketIdArr[] = $flow->ticketflow_id;
            $ticketArr[] = $flow;
        } ?>
<?php foreach ($ticketArr as $flow) { ?>
            <tbody>
                <tr>
                    <td><?php echo $staticFields["helpTopic"][$flow->help_topic]; ?></td>
                    <td><?php echo $flow->status_type; ?></td>
                    <td><?php echo $staticFields["orderStatus"][$flow->status_type][$flow->status]; ?></td>
                    
                    <td><input type="submit" value="X" name="Remove-<?php echo $flow->ticketflow_id; ?>"></td>
                </tr>
            </tbody>
                        <?php } ?>
        <tbody>
            <tr><th colspan="7"><br><br></th></tr>
        </tbody>
        <tbody>
            <tr>
                <td><select name='topic'  id='HelpTopic' style="width:150px;">
                        <?php foreach ($staticFields["helpTopic"] as $id => $topic) { ?>
                        <option value='<?php echo $id; ?>'><?php echo $topic; ?></option>
                        <?php }; ?>
                    </select></td>
                <td><select name='type'  id='statusType' style="width:150px;">
                        <?php foreach ($staticFields["orderStatus"] as $type => $os) { ?>
                        <option value='<?php echo $type; ?>' <?php if($type=="Order"){?> selected="selected" <?php }?>><?php echo $type; ?></option>
                        <?php }; ?>
                    </select></td>
                <td>   
                    <?php foreach ($staticFields["orderStatus"] as $type => $os) { ?>
                        <select class='dependentOptions' name='<?php echo $type; ?>Status' id='<?php echo $type; ?>' style="width:150px; <?php if($type!="Order"){?> display:none; <?php }?>">
                                <?php foreach ($os as $id => $statusName) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                            <?php }; ?>
                        </select>
                        <?php } ?>

                </td>

                <td></td>
            </tr>
        </tbody>
        <tbody>
            <tr><th colspan="7"><input type='submit' value="Add Flow"></th></tr>
        </tbody>
    </form>
</table>

<script>
    $("#statusType").change(function(){
        var statusType = $("#statusType").val();
        $(".dependentOptions").hide();
        $("#"+statusType).show();
        
    });
</script>