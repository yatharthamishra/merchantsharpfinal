<?php
if(!$bool){
?>
<div style="color:red;"><?php echo $error; ?></div>
<table class="list" width="100%"; style="font-size:15px;">
    <form name="dataProcessing" action="" method="POST">
        <thead>
            <tr><th colspan="8"><?php echo __('DATA REPROCESSING'); ?></th></tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?php echo __('Select search param'); ?>
                </td>
                <td>
                    <select name="searchParam" class="searchParam select2" value="<?php echo $_POST['searchParam']; ?>">
                        <option value="ticket_id">Ticket Id</option>
                        <option value="ticket_num">Ticket Number</option>
                        <option value="order_id">Order Id</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Enter value of search param'); ?></td>
                <td><span style="padding: 3px 5px;vertical-align: -webkit-baseline-middle;"><input type="checkbox" name="allTickets" id="allTickets">*All</span><input type="text" id="ticketIds" required name="paramValue" value="<?php echo $_POST['paramValue']; ?>" placeholder="Ticket Ids"></td>
            </tr>
            <tr>
                <td><?php echo __('Enter date range'); ?></td>
                <td>
                    <input type="datetime" id="datetimepickerFrom" required placeholder="From Date" name="fromDate" value="<?php echo $_POST['fromDate']; ?>">
                    <input type="datetime" id="datetimepickerTo" required placeholder="To Date" name="toDate" value="<?php echo $_POST['toDate']; ?>">
                </td>
            </tr>
            <tr>
                <td><?php echo __('Select the operation'); ?></td>
                <td>
                    <select id="operation" required multiple name="operation[]" class="select2">
                        <option value="all">1. Reprocess Tickets Complete (2,3,4 and 5)</option>
                        <option value="openTickets">2. Open Tickets</option>
                        <option value="setNegativeTicketIdForTasks">3. Set negative object id for tasks</option>
                        <option value="openLastTasks">4. Open Last Tasks before time range(not manual)</option>
                        <option value="reprocessTickets">5. Reprocess Tickets</option>
                        <option value="closeTickets">6. Close Tickets</option>
                        <option value="changeToTriageAgain">7. Change Status to Triage Again</option>
                        <option value="openLastTasksOverall">8. Open overall last task of ticket(not manual)</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Enter Password'); ?></td>
                <td><input type="password" name="password" required placeholder="Password"></td>
            </tr>
            <tr>
                <td colspan="2"><input id="formSubmit" type="submit" value="Submit"></td>
            </tr>
        </tbody>
    </form>
</table>
<div style="background: rgba(255, 0, 0, 0.08);">
    <h1><u>steps</u></h1>
    <h3>
        1. tickets open (status_id 6)<br>
        2. set object_id of the tasks created between time range to be -1* object_id<br>
        3. open last task of these tickets before the start time of the time range<br> 
        4. find order_id and return_id of these tickets and reprocess them.<br>
        5. Data Reprocessing logs are maintained in <a href="<?php echo ROOT_PATH.'logs/dataReprocessing.txt'; ?>">logs/dataReprocessing.txt</a> <br>
        6. To revert back the changes made in this process, two of the queries are given in <a href="<?php echo ROOT_PATH.'logs/revert.txt'; ?>">logs/revert.txt</a>. The 2 queries are for closing the tickets again and for making the object id of the task positive between the time range.
    </h3>
    <h1>*<u>NOTE</u></h1>
    <h3>
        1. All option is available for following operations<br>
            i) open tickets<br>
            ii) close tickets<br>
            iii) changeToTriageAgain<br>
            iv) reprocess tickets complete<br>
    </h3>
</div>

<script>
    $(function () {
        $("#allTickets").on('click', function(){
            if($("#allTickets").is(":checked")){
                $("#ticketIds").hide();
                $("#ticketIds").removeAttr("required");
            }
            else{
                $("#ticketIds").show();
                $("#ticketIds").attr("required","required");
            }
        });
        
        $(".searchParam").on('change',function(){
            $("#ticketIds").attr("placeholder",$(this).val());
        });
        $(".select2").select2({});
        $("#operation").select2({
            placeholder:'Operation'
        });
        $('#datetimepickerFrom').datetimepicker({
        });
        $('#datetimepickerTo').datetimepicker({
            useCurrent: false
        });
        // $("#datetimepickerFrom").on("change", function (e) {
        //     $('#datetimepickerTo').data("DateTimePicker").minDate($("#datetimepickerFrom").val());
        // });
        // $("#datetimepickerTo").on("change", function (e) {
        //     $('#datetimepickerFrom').data("DateTimePicker").maxDate(e.date);
        // });

        $("#formSubmit").on('click',function(){
            if($("#allTickets").is(":checked")){
                
            }
        });
    });
</script>
<?php }
else{
    ?>
    <a href="dataReprocessing.php">Back</a>
    <?php
}
?>