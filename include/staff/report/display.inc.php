<?php
if (!$_GET["export"]) {
$ticketsDataHead = array_keys($ticketArr[key($ticketArr)])
?>

<table style='box-shadow: 1px 3px 11px #000; background: #fff;width:99%;margin:0px auto;'>
    <tr style='background:rgb(81, 130, 130) none repeat scroll 0% 0%;color:#fff;'>
        <?php
        foreach ($ticketsDataHead as $head) {
            echo "<th style='padding: 10px;'><center>" . $head . "</center></th>";
        }
        ?>
    </tr>
    <?php
    $count = 0;
    foreach ($ticketArr as $ticket) {

        if ($count % 2 == 1)
            echo "<tr>";
        else
            echo "<tr style='background:#ffffc1;'>";
        $count++;
        foreach ($ticket as $ticketField) {
            echo "<td width='150px'><center>" . $ticketField . "</center></td>";
        }
        echo "</tr>";
    }
    ?>

</table>
<?php }?>


<?php
if ($_GET["export"]) {
    $dir = STAFFINC_DIR.'report/temp/';
download_send_headers($dir.$page . date("Y-m-d") . ".csv");
echo array2csv($ticketArr);

$interval = strtotime('-24 hours');//files older than 24hours
foreach (glob($dir."*") as $file) 
    //delete if older
    if (filemtime($file) <= $interval ) unlink($file);
die();
}
?>