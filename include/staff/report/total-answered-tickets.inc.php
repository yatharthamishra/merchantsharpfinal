<?php
$staticTicketFields = getCachedTicketFields();
$from = strtotime($_GET["from"]);
$to = strtotime($_GET["to"]);

if($from==$to){
    $to = $from + 24*60*60;
}
echo "Report for tickets Answered From ".date("Y-m-d H:i:s",$from)." to ".date("Y-m-d H:i:s",$to)."<br><br>";
$AnsweredArr = ThreadEntry::objects()->filter(array("type"=>"R","user_id"=>0,"created__gt"=>date("Y-m-d H:i:s",$from),"created__lt"=>date("Y-m-d H:i:s",$to)))->values('id','thread__object_id','poster','created','body')->all();
foreach($AnsweredArr as $answer){
    if(!stristr($answer['body'],'Support ticket opened')){
        $ticketArrAdd[$answer["thread__object_id"]][$answer["id"]]["Staff"]= $answer["poster"];
        $ticketArrAdd[$answer["thread__object_id"]][$answer["id"]]["Answer Time"]= $answer["created"];
        $ticketIds[] = $answer["thread__object_id"];
    }
}

$ticketsDataArr = Ticket::objects()->filter(array('ticket_id__in'=>$ticketIds))->values("ticket_id","number","created","closed","status_id","staff_id","topic_id","agent_thread_count","user_thread_count","source_extra")->distinct('ticket_id')->all();
foreach($ticketsDataArr as $ticket){
    $tempTicketArr[$ticket["ticket_id"]]["Ticket Number"] = $ticket["number"];
    $tempTicketArr[$ticket["ticket_id"]]["Created On"] = $ticket["created"];
    $tempTicketArr[$ticket["ticket_id"]]["closed"] = $ticket["closed"];
    $tempTicketArr[$ticket["ticket_id"]]["Source"] = $ticket["source_extra"];
    $tempTicketArr[$ticket["ticket_id"]]["Status"] = $staticTicketFields["status"][$ticket["status_id"]]?:"-";
    $tempTicketArr[$ticket["ticket_id"]]["Issue Type"] = $staticTicketFields["helpTopic"][$ticket["topic_id"]]?:"-";
    $tempTicketArr[$ticket["ticket_id"]]["Staff"] = $staticTicketFields["staff"][$ticket["staff_id"]]?:"-";
    $tempTicketArr[$ticket["ticket_id"]]["Customer Replies"] = $ticket["user_thread_count"];
    $tempTicketArr[$ticket["ticket_id"]]["Agent Replies"] = $ticket["agent_thread_count"];
    $tempTicketArr[$ticket["ticket_id"]]["Last Task"]="-";
    $ticketIds[] = $ticket["ticket_id"];
}
if($ticketIds){
    $tasks = Task::objects()->filter(array("object_id__in"=>$ticketIds))->values('object_id','data__taskType')->all();
    foreach($tasks as $task){
        $tempTicketArr[$task["object_id"]]["Last Task"]= explode(",",$task["data__taskType"])[1];
    }
}

$i=0;
foreach($ticketArrAdd as $tid=>$data){
    foreach($data as $id=>$vData){
        foreach($vData as $k=>$v){
            $ticketArr[$i][$k] = $v;
        }
        foreach($tempTicketArr[$tid] as $k=>$v){
            $ticketArr[$i][$k] = $v;
        }
        $i++;
    }
    
}
    