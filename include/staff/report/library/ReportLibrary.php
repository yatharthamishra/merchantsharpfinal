<?php
include REPORT_MODEL_PATH.'ExcelModel.php';

class ReportLibrary {
    
    private static $modelObject=null;
    private $model;
    private $filterApplied;


    public function __construct() {
        if(!self::$modelObject){
            self::$modelObject=new ExcelModel();
        }
        $this->model=self::$modelObject;
    }
 
    public function printRawOpsDataIntoExcel($filePointer,$staticFields,$filtersApplied,$headerFields=[]){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $bucketNumber=0;
        $staffManagerMapping=$this->model->getStaffManagerMapping();
        set_time_limit(RAW_OPS_DUMP_TIMEOUT);
        $excelArray=[];
        $header=false;
        while(1){
            $data=  $this->model->getTicketsAndTaskRelatedInfo($bucketNumber,$modelRepresentedFilter);
		$taskIds=[];
            foreach ($data as $rows) {
                $taskId=$rows['task_id'];
                $excelArray[$taskId]['Ticket-Number'] = $rows['number'];
                $excelArray[$taskId]['TicketCreateDateTime'] = formatDateTimeToDateFormat($rows['ticketCreateDateTime']);
                $excelArray[$taskId]['TicketSource'] = $rows['source_extra'];
                $issueSubIssue = getIssuesRelatedInfoFromTopicId($staticFields, $rows['topic_id']);
                $excelArray[$taskId]['Issue-Type'] = $issueSubIssue['issueType'];
                $excelArray[$taskId]['Issue-Sub-Type'] = $issueSubIssue['issueSubType'];
                $excelArray[$taskId]['Issue-Sub-Sub-Type'] = $issueSubIssue['issueSubSubType'];
                $excelArray[$taskId]['Order-Number'] = $rows['order_id'];
		$excelArray[$taskId]['Manifest-Id'] = $rows['manifest_id'];
		$excelArray[$taskId]['Product-Id'] = $rows['product_id'];
		$excelArray[$taskId]['Batch-Number'] = $rows['batch_id'];
                $excelArray[$taskId]['Order-Status'] = $staticFields['orderStatus']['Order'][$rows['orderStatus']];
                $excelArray[$taskId]['Task-Name'] = getTaskTypeToDisplay($rows['taskType']);
                $excelArray[$taskId]['Task-Id'] = $rows['task_id'];
                $excelArray[$taskId]['Task-Source'] = count(explode(',', $rows['taskSource'])) > 0 ? explode(',', $rows['taskSource'])[1] : '';
                $excelArray[$taskId]['Task-Create-Date-Time'] = formatDateTimeToDateFormat($rows['taskCreateDateTime']);
                //$excelArray[$taskId]['Task-Type'] = getTaskTypeToDisplay($rows['taskType']);
                $agentRelatedDetail=getAgentAndManagerDetailsFromAgentId($staffManagerMapping, $rows['staff_id']);
                $excelArray[$taskId]['Agent-Name']=$agentRelatedDetail['agentName'];
                $excelArray[$taskId]['Agent-Email']=$agentRelatedDetail['agentEmail'];
                $excelArray[$taskId]['Team-Leader']=$agentRelatedDetail['managerName'];
                $departmentName=getDepartmentNameFromId($staticFields, $rows['dept_id']);
                $excelArray[$taskId]['Department-Name']=$departmentName;
               // $excelArray[$taskId]['Task-Assign-Date-Time']=formatDateTimeToDateFormat($rows['taskCreateDateTime']); // unknown
                $excelArray[$taskId]['Task-Close-Date-Time']=formatDateTimeToDateFormat($rows['taskCloseDateTime']);
                $excelArray[$taskId]['SLA-Status']=  $this->_getTaskSLAOSLAStatus($rows['taskSlaStatus']);
                $excelArray[$taskId]['Return-Id']=  $rows['retrn_id'];
                $excelArray[$taskId]['Task-Status']=  getTaskStatusToDisplay($rows['taskStatus']);
                $excelArray[$taskId]['Task-Response']=getTaskResponseForDisplay($rows['taskResponse']);
                $excelArray[$taskId]['Handling-Time(In seconds)']=  getHoursSpendBetweenTwoDates($rows['taskCreateDateTime'], $rows['taskCloseDateTime']);
                $taskIds[]=$taskId;

            }
            $numberOfTimesTaskUpdated=$this->model->getNumberOfTimesTaskUpdated($taskIds);
            
            foreach ($excelArray as $taskId=>$excelDetails){
                if($numberOfTimesTaskUpdated[$taskId]>0){
                    $excelArray[$taskId]['Number-Of-Times-Task-Updated']=$numberOfTimesTaskUpdated[$taskId]['numberOfUpdates'];
                }else{
                    $excelArray[$taskId]['Number-Of-Times-Task-Updated']=0;
                }
                if(!$header){
                    if($headerFields){
                        fputcsv($filePointer, $headerFields);
                    }
                    else{
                        fputcsv($filePointer, array_keys($excelArray[$taskId]));
                    }
                    $header=TRUE;
                }
                if($headerFields){
                    foreach ($headerFields as $val) {
                        $dataForCsv[]=$excelArray[$taskId][$val];
                    }
                    fputcsv($filePointer,$dataForCsv);
                }
                else{
                    fputcsv($filePointer,$excelArray[$taskId]);
                }
                unset($dataForCsv);
              
            }
            
            if(count($data)<OPS_DATA_BUCKET_SIZE){
                break;
            }
            
            unset($excelArray);
            unset($numberOfTimesTaskUpdated);
            $bucketNumber++;
        }
        fclose($filePointer);

        
        
    }
    public function printOverallTicketWiseData($filePointer,$staticFields,$filterApplied){
        
        $bucketNumber=0;
        $header=false;
        $ccgStaffIds=$staticFields['departmentStaffMap'][CCG_DEPARTMENT_ID];
        $excelFieldArray=[];
        $priorityIdNameMap=getPriorityIdNameMap();
        set_time_limit(OVERALL_TICKET_DUMP_TIMEOUT);
        $modelUnderStandableWhereClause=  $this->_processFilters($filterApplied);
        while(1){
            $data=$this->model->getOverallTicketDump($bucketNumber,$ccgStaffIds,$modelUnderStandableWhereClause);
            foreach ($data as $ticketId=>$ticketData){
                $excelFieldArray['Ticket Number']=$ticketData['number'];
                $excelFieldArray['Ticket Source']=$ticketData['source_extra'];
                $excelFieldArray['Customer Email Address']=$ticketData['customerEmailAddress'];
                $excelFieldArray['Order IDS']=$ticketData['order_ids'];
		$excelFieldArray['Manifest IDS']=$ticketData['manifest_ids'];
		$excelFieldArray['Batch IDS']=$ticketData['batch_ids'];
		$excelFieldArray['Product IDS']=$ticketData['product_ids'];
		$excelFieldArray['Mandate Info']=$ticketData['mandateType'];
                $excelFieldArray['Ticket Created Date']=$ticketData['created'];
                $excelFieldArray['Call Customer']=$ticketData['call_customer']==2?'Yes':'No';
                $excelFieldArray['Ticket Closed Date']=$ticketData['closed'];
                $excelFieldArray['Number Of Tasks']=$ticketData['Number Of Tasks'];
                if(is_null($ticketData['closed'])){
                    $excelFieldArray['Ticket Status']='Open';
                }else{
                    $excelFieldArray['Ticket Status']='Closed';
                }
                $issueRelatedInfo=getIssuesRelatedInfoFromTopicId($staticFields, $ticketData['topic_id']);
                $excelFieldArray['Issue']=$issueRelatedInfo['issueType'];
                $excelFieldArray['Sub Issue']=$issueRelatedInfo['issueSubType'];
                $excelFieldArray['Sub Sub Issue']=$issueRelatedInfo['issueSubSubType'];
                $excelFieldArray['Priority']=$priorityIdNameMap[$ticketData['priority']];
                $excelFieldArray['Agent Name']=$staticFields['staff'][$ticketData['staff_id']];
                $callCommunicationToBeAdded=0;
                if($ticketData['call_customer']==2){
                    $callCommunicationToBeAdded=1;
                }
                $excelFieldArray['Number Of Tasks Created']=$ticketData['Number Of Tasks'];
                $excelFieldArray['Total Number Of communication']=$ticketData['communicationCountByAgent']+$callCommunicationToBeAdded+$ticketData['Number Of Auto communication sent'];
                $excelFieldArray['Number Of Auto Communication sent']=$ticketData['Number Of Auto communication sent'];
                $excelFieldArray['Number Of Manual Communication']=$excelFieldArray['Total Number Of communication']-$excelFieldArray['Number Of Auto Communication sent'];
                $excelFieldArray['Number Of Email Communication']=$excelFieldArray['Number Of Auto Communication sent']+$excelFieldArray['Number Of Manual Communication'];
                $excelFieldArray['Number Of Communication By Customer']=$ticketData['communicationByCustomer'];
                $excelFieldArray['First Response Time(in hours)']=getNumberOfHoursBetweenTwoDates($ticketData['created'],$ticketData['First Response Time']);
                $excelFieldArray['Latest Update By Customer']=$ticketData['Latest Update By Customer'];
                $excelFieldArray['Latest Update By CCG Agent Date And Time']=$ticketData['Latest Update By CCG Agent Date And Time'];
                $excelFieldArray['Assigned Date Of Ticket']=$ticketData['assignedDate'];
                $excelFieldArray['Handling Time(In seconds)']='';
                if(!is_null($ticketData['closed'])){
                    $excelFieldArray['Handling Time(In seconds)']=   getHoursSpendBetweenTwoDates($ticketData['created'], $ticketData['closed']);
                }
                if(!$header){
                    fputcsv($filePointer, array_keys($excelFieldArray));
                    $header=TRUE;
                }
                fputcsv($filePointer,$excelFieldArray);
            }
            $bucketNumber++;
            if(count($data)<OVERALL_TICKET_BUCKET_SIZE){
                break;
            }
         
            unset($data);
            unset($excelFieldArray);
        }
    }
    public function printSLARevisedEntriesTaskIntoExcel($filePointer,$staticFields,$filterApplied){
        set_time_limit(SLA_REV_DUMP_TIMEOUT);
        $bucketNumber=0;
        $header=false;
        $modelUnderstandableFilters=$this->_processFilters($filterApplied);
        while(1){
            $excelArray=[];
            $taskIds=[];
            $data=$this->model->getSLAUpdatedTasksInfo($bucketNumber,$modelUnderstandableFilters);
            foreach ($data as $opData){
                $taskId=$opData['taskId'];
                $taskIds[]=$taskId;
                $excelArray[$taskId]['Ticket Id']=$opData['ticketId'];
                $excelArray[$taskId]['Task Created']=$opData['created'];
                $excelArray[$taskId]['Task Updated']=$opData['taskUpdated'];
                $excelArray[$taskId]['New Sla Given']=$opData['NewSlaGiven'];
                $excelArray[$taskId]['Promised DueDate']=$opData["PromisedDueDate"];
                $excelArray[$taskId]['TaskId']=$taskId;
                $excelArray[$taskId]['Task Name']=  getTaskTypeToDisplay($opData['taskType']);
                $excelArray[$taskId]['Department Name']=  getDepartmentNameFromId($staticFields, $opData['dept_id']);
            }
            $numberOfTimesTaskUpdated=$this->model->getNumberOfTimesTaskUpdated($taskIds);
            foreach ($excelArray as $taskID=>$taskDetail){
                if(!isset($numberOfTimesTaskUpdated[$taskID])){
                    continue;
                }
                $excelArray[$taskID]['NumberOfTimesTaskUpdated']=$numberOfTimesTaskUpdated[$taskID]['numberOfUpdates'];
                if(is_numeric($numberOfTimesTaskUpdated[$taskID]['agentId'])){
                    $excelArray[$taskID]['Agent Name']=$staticFields['staff'][$numberOfTimesTaskUpdated[$taskID]['agentId']];
                }else{
                    $excelArray[$taskID]['Agent Name']=$numberOfTimesTaskUpdated[$taskID]['agentId'];
                }
                if(!$header){
                    fputcsv($filePointer, array_keys($excelArray[$taskID]));
                    $header=TRUE;
                }
                fputcsv($filePointer, $excelArray[$taskID]);
            }
            $bucketNumber++;
            if(count($excelArray)<SLA_REVISED_BUCKET_SIZE){
                break;
            }
            unset($excelArray);
            unset($data);
        }
        fclose($filePointer);
        
        
    }

    private function _getTaskSLAOSLAStatus($slaOslaField) {

        switch ($slaOslaField) {
            case 'taskOpenWithinSla':
                $fieldTask = "Within SLA";
                break;
            case 'taskOpenWithinOSla':
                $fieldTask = "OSLA";
                break;
            case 'taskClosedWithinSla':
                $fieldTask = "Within SLA";
                break;
            case 'taskClosedWithinOSla':
                $fieldTask = "OSLA";
                break;
        }
        return $fieldTask;
    }

    public static function getModelInstance(){
        if(!self::$modelObject){
            self::$modelObject=new ExcelModel();
        }
        return self::$modelObject;
    }
    
    public function getDepartmentWiseTaskInfo($staticFields,$filtersApplied){
        $modelUnderstandableFilterWithCondition=array();
        if($filtersApplied){
            $modelUnderstandableFilterWithCondition=  $this->_processFilters($filtersApplied);
        }
        $departmentWiseInfo=$this->model->getDepartmentWiseTaskInfo($filtersApplied,$modelUnderstandableFilterWithCondition);
        foreach ($departmentWiseInfo as $key=>$departmentWise){
            $departmentWiseInfo[$key]['department']=  getDepartmentNameFromId($staticFields, $departmentWise['dept_id']);
            $departmentWiseInfo[$key]['slaPercentage']=  getSLAPercentage($departmentWise['taskClosedWithinSla'], $departmentWise['taskOpenWithinSla'],$departmentWise['totalNumberOfTasks']);
            if(empty($departmentWiseInfo[$key]['department'])){
                unset($departmentWiseInfo[$key]);
            }
        }
        sort2dArrayAlphabeticallyAccordingToKey($departmentWiseInfo, "department")  ;  
        return $departmentWiseInfo;
    }
    
 
    
    private function _getOSLATicketCondition(&$modelUnderStandableWhereClause){
        $modelUnderStandableWhereClause['customWhereClause'][]=" AND ((
                ticket.`closed` IS NULL 
                AND ticket.`est_duedate` < NOW()
              ) 
              OR ticket.`est_duedate` < ticket.`closed` )";
    }

    private function _getSLATicketCondition(&$modelUnderStandableWhereClause){
        $modelUnderStandableWhereClause['customWhereClause'][]=" AND (ticket.`est_duedate` >= ticket.`closed` 
  OR (
    ticket.`est_duedate` IS NULL 
    AND ticket.`est_duedate` >= NOW()
  )) ";
    }
private function _getOSLATaskCondition(&$modelUnderStandableWhereClause){
        $modelUnderStandableWhereClause['customWhereClause'][]=" AND ((
                task.`closed` IS NULL 
                AND task.`duedate` < NOW()
              ) 
              OR task.`duedate` < task.`closed`) ";
    }

    private function _getSLATaskCondition(&$modelUnderStandableWhereClause){
        $modelUnderStandableWhereClause['customWhereClause'][]=" AND (task.`duedate` >= task.`closed` 
  OR (
    task.`closed` IS NULL 
    AND task.`duedate` >= NOW()
  ) ) ";
    }

    public function getTaskWiseSLAReport($staticFields,$filtersApplied){
        $modelUnderstandableFilterWithCondition=array();
        if($filtersApplied){
            $modelUnderstandableFilterWithCondition=  $this->_processFilters($filtersApplied);
        }
        $taskWiseInfo=$this->model->getTaskWiseSLAReport($modelUnderstandableFilterWithCondition);
        
        foreach ($taskWiseInfo as $key=>$tasks){
            $taskWiseInfo[$key]['department']=  getDepartmentNameFromId($staticFields, $tasks['deptId']);
            $taskWiseInfo[$key]['slaPercentage']=  getSLAPercentage($tasks['taskClosedWithinSla'], $tasks['taskOpenWithinSla'],$tasks['totalNumberOfTasks']);
            $taskWiseInfo[$key]['taskName']=  getTaskTypeToDisplay($taskWiseInfo[$key]['taskType']);
            if(empty($taskWiseInfo[$key]['taskName'])){
                unset($taskWiseInfo[$key]);
            }
        }
        sort2dArrayAlphabeticallyAccordingToKey($taskWiseInfo, "taskName")  ;  
        return $taskWiseInfo;
    }
    public function getIssueWiseSummaryReportData($staticFields,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $issueWiseSummary=  $this->model->getIssueWiseSummaryReportData($modelRepresentedFilter);
        $serialNumber=1;
        foreach ($issueWiseSummary as $key=>$issues){
            $topicId=$issues['topicId'];
            $issueSubIssue=getIssuesRelatedInfoFromTopicId($staticFields, $topicId);
            $issueWiseSummary[$key]['issueType']=$issueSubIssue['issueType'];
            $issueWiseSummary[$key]['issueSubType']=$issueSubIssue['issueSubType'];
            $issueWiseSummary[$key]['issueSubSubType']=$issueSubIssue['issueSubSubType'];
            $issueWiseSummary[$key]['ticketsClosed']=$issues['ticketsClosedWithinOSla']+$issues['ticketsClosedWithinSla']+$issues['ticketsClosedButUnknownStatus'];
            $issueWiseSummary[$key]['ticketsOpen']=$issues['ticketsOpenWithinOSla']+$issues['ticketsOpenWithinSla'];
            $issueWiseSummary[$key]['SNumber']=$serialNumber;
            $issueWiseSummary[$key]['slaPercentage']=getSLAPercentage($issues['ticketsClosedWithinSla'],$issues['ticketsOpenWithinSla'],$issues['totalNumberOfTickets']);
            $serialNumber++;
            }
            $serialNumber=1;
         sort2dArrayAlphabeticallyAccordingToKey($issueWiseSummary, "issueType")  ;  
         foreach ($issueWiseSummary as $key=>$issues){
             $issueWiseSummary[$key]['SNumber']=$serialNumber;
             $serialNumber++;
         }
        return $issueWiseSummary;
    }
    public function getSourceWiseSumaryReportData($filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $sourceWiseSummary=  $this->model->getSourceWiseTicketData($modelRepresentedFilter);
        foreach ($sourceWiseSummary as $key=>$sources){
            $sourceWiseSummary[$key]['ticketsClosed']=$sources['ticketsClosedWithinOSla']+$sources['ticketsClosedWithinSla']+$sources['ticketsClosedButUnknownStatus'];
            $sourceWiseSummary[$key]['ticketsOpen']=$sources['ticketsOpenWithinOSla']+$sources['ticketsOpenWithinSla'];      
            $sourceWiseSummary[$key]['slaPercentage']=getSLAPercentage($sources['ticketsClosedWithinSla'],$sources['ticketsOpenWithinSla'],$sources['totalNumberOfTickets']);
            }
            sort2dArrayAlphabeticallyAccordingToKey($sourceWiseSummary, "source_extra");
        return $sourceWiseSummary;
    }
    public function getProductivitySummaryReportData($staticFields,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $productivityAgentInfo=  $this->model->getProductivityInfoByTask($modelRepresentedFilter);
        $staffManagerMapping=$this->model->getStaffManagerMapping();
        foreach ($productivityAgentInfo as $key=>$agentProductivityInfo){
            $agentId=$agentProductivityInfo['staff_id'];
            $agentRelatedInfo=getAgentAndManagerDetailsFromAgentId($staffManagerMapping, $agentId);
            $productivityAgentInfo[$key]['agentName']=$agentRelatedInfo['agentName'];
            $productivityAgentInfo[$key]['managerName']=$agentRelatedInfo['managerName'];
            $productivityAgentInfo[$key]['agentEmail']=$agentRelatedInfo['agentEmail'];
            $productivityAgentInfo[$key]['taskhandled']=$agentProductivityInfo['taskClosedWithinSla']+$agentRelatedInfo['taskClosedWithinOSla']+$agentRelatedInfo['taskClosedButUnknownStatus'];
        }
        return $productivityAgentInfo;
    }
    private function _filterFiltersApplied($filtersApplied){
        $fApplied=[];
        if($filtersApplied){
            foreach ($filtersApplied as $filterKey=>$filterVal){
                if($filterVal=="all" || $filterVal=="All"){
                    continue;
                }
                $fApplied[$filterKey]=$filterVal;
            }
            return $fApplied;
        }
        
        return $filtersApplied;
    }
       private function _getFiltersForTickets($filtersApplied){
           
        $modelUnderStandableWhereClause=array();
        $topicFilterApplied=0;
        foreach ($filtersApplied as $filterName=>$filterValue){
            switch ($filterName){
                case 'issueType':
                case 'issueSubType':
                case 'issueSubSubType':
                    if(!$topicFilterApplied){
                        global $staticFields;
                        
                          $topicIds=getTopicIdFromIssueIdAndSubSubIssueId($staticFields,array($filtersApplied['issueType']),array($filtersApplied['issueSubType'])
                           ,$filtersApplied['issueSubSubType']);
                          $modelUnderStandableWhereClause['ticket__topic_id__in']=$topicIds;
                          $topicFilterApplied=1;

                    }
                    break;
                case 'agent':
                    if(!isset($modelUnderStandableWhereClause['ticket__staff_id__in'])){
                        $modelUnderStandableWhereClause['ticket__staff_id__in']=[];
                    }
                    foreach ($filtersApplied['agent'] as $agentId){
                        $modelUnderStandableWhereClause['ticket__staff_id__in'][]=$agentId;
                    }
                    break;
                case 'teamLeader':
                    if(!isset($modelUnderStandableWhereClause['ticket__staff_id__in'])){
                        $modelUnderStandableWhereClause['ticket__staff_id__in']=[];
                    }
                    $staffManagerMapping=$this->model->getStaffManagerMapping();
                    $staffIdList=[];
                    foreach ($filtersApplied['teamLeader'] as $teamLeader){
                        foreach ($staffManagerMapping['managerToStaffMap'][$teamLeader] as $staffID){
                            $staffIdList[]=$staffID;
                        };
                        $staffIdList[]=$teamLeader;
                    }
                   
                    foreach ($staffIdList as $staffidd){
                        $modelUnderStandableWhereClause['ticket__staff_id__in'][]=$staffidd;
                    }
                    break;
                case 'department':
                    $modelUnderStandableWhereClause['ticket__dept_id__in']=$filtersApplied['department'];
                    break;
                case 'ticketSlaStatus':
                    
                    switch ($filtersApplied['ticketSlaStatus']){
                        case 0:
                            $this->_getOSLATicketCondition($modelUnderStandableWhereClause);
                            break;
                        case 1:
                            $this->_getSLATicketCondition($modelUnderStandableWhereClause);
                            break;
                    }
                    break;
                case 'ticketCreateFromdate':
                case 'ticketCreateTodate':
                            
                            $modelUnderStandableWhereClause=$this->_getTicketCreationTimeWhereClause($filtersApplied['ticketCreateFromdate'],
                                    $filtersApplied['ticketCreateTodate'],$modelUnderStandableWhereClause);
                    break;
                case 'threadCreateFromdate':
                case 'threadCreateTodate':
                            
                            $modelUnderStandableWhereClause=$this->_getTicketCreationTimeWhereClause($filtersApplied['threadCreateFromdate'],
                                    $filtersApplied['threadCreateTodate'],$modelUnderStandableWhereClause);
                    break;
                
            }
        }
        return $modelUnderStandableWhereClause;
    }
    private function _getFiltersForTaskData($filtersApplied) {
        $modelUnderStandableWhereClause = array();
        foreach ($filtersApplied as $filterName => $filterValue) {
            switch ($filterName) {
                case 'taskType':
                    $modelUnderStandableWhereClause['taskData__taskType__in'] = $filterValue;
                    break;
            }
        }
        return $modelUnderStandableWhereClause;
    }

    private function _getFiltersForTask($filtersApplied){
         
         $modelUnderStandableWhereClause=array();
         foreach ($filtersApplied as $filterName=>$filterValue){
             switch ($filterName){
                 case 'taskStatus':
                     $this->_getTaskStatusFilter($filtersApplied[$filterName],$modelUnderStandableWhereClause);
                     break;
                 case 'taskDepartment':
                     $modelUnderStandableWhereClause['task__dept_id__in']=$filterValue;
                     break;
                 case 'taskCreateFromDate':
                 case 'taskCreateToDate':
                     $modelUnderStandableWhereClause=  $this->_getTaskCreationTimeWhereClause($filtersApplied['taskCreateFromDate'], 
                             $filtersApplied['taskCreateToDate']);
                     
                     break;
                 case 'taskslaStatus':
                     switch($filterValue){
                        case 0:
                            $this->_getOSLATaskCondition($modelUnderStandableWhereClause);
                            break;
                        case 1:
                            $this->_getSLATaskCondition($modelUnderStandableWhereClause);
                            break;
                     }
                     break;
             }
         }
         return $modelUnderStandableWhereClause;
    }
    private function _getTaskStatusFilter($taskStatus,&$modelUnderStandableWhereClause){
        switch ($taskStatus){
            case 0:
                $modelUnderStandableWhereClause['customWhereClause'][]=" AND task.closed is null ";
                //task open
                break;
            case 1:
                //task closed
                $modelUnderStandableWhereClause['customWhereClause'][]=" AND task.closed is not null ";
                break;
        }
        
    }

    private function _getFiltersForThreadEntry($filtersApplied){
           
        $modelUnderStandableWhereClause=array();
        $topicFilterApplied=0;
        foreach ($filtersApplied as $filterName=>$filterValue){
            switch ($filterName){
                case 'threadAgent':
                    if(!isset($modelUnderStandableWhereClause['ticketThreadEntry__staff_id__in'])){
                        $modelUnderStandableWhereClause['ticketThreadEntry__staff_id__in']=[];
                    }
                    foreach ($filtersApplied['threadAgent'] as $agentId){
                        $modelUnderStandableWhereClause['ticketThreadEntry__staff_id__in'][]=$agentId;
                    }
                    break;
                case 'threadTeamLeader':
                    if(!isset($modelUnderStandableWhereClause['ticketThreadEntry__staff_id__in'])){
                        $modelUnderStandableWhereClause['ticketThreadEntry__staff_id__in']=[];
                    }
                    $staffManagerMapping=$this->model->getStaffManagerMapping();
                    $staffIdList=[];
                    foreach ($filtersApplied['threadTeamLeader'] as $teamLeader){
                        foreach ($staffManagerMapping['managerToStaffMap'][$teamLeader] as $staffID){
                            $staffIdList[]=$staffID;
                        };
                        $staffIdList[]=$teamLeader;
                    }
                    foreach ($staffIdList as $staffidd){
                        $modelUnderStandableWhereClause['ticketThreadEntry__staff_id__in'][]=$staffidd;
                    }
                    break;
                case 'threadCreateFromdate':
                case 'threadCreateTodate':
                    
                    $this->_getThreadEntryCreationTimeWhereClause($filtersApplied['threadCreateFromdate'],
                            $filtersApplied['threadCreateTodate'],$modelUnderStandableWhereClause);
                    break;
                case 'threadOnlyCreateFromdate':
                case 'threadOnlyCreateTodate':
                    
                    $this->_getThreadEntryCreationTimeWhereClause($filtersApplied['threadOnlyCreateFromdate'],
                            $filtersApplied['threadOnlyCreateTodate'],$modelUnderStandableWhereClause);
                    break;
                case 'triageType':
                    if($filtersApplied['triageType']==1)
                        $modelUnderStandableWhereClause['ticketThreadEntry__title__=']='Triaged';
                    else
                        $modelUnderStandableWhereClause['ticketThreadEntry__title__=']='Triage field change';
                    break;
            }
        }
        return $modelUnderStandableWhereClause;
    }
    private function _getFiltersForTracking($filtersApplied){
        $modelUnderStandableWhereClause=array();
        $topicFilterApplied=0;
        foreach ($filtersApplied as $filterName=>$filterValue){
            switch ($filterName){
                case 'threadAgent':
                    if(!isset($modelUnderStandableWhereClause['tracking__staff_id__in'])){
                        $modelUnderStandableWhereClause['tracking__staff_id__in']=[];
                    }
                    foreach ($filtersApplied['threadAgent'] as $agentId){
                        $modelUnderStandableWhereClause['tracking__staff_id__in'][]=$agentId;
                    }
                    break;
                case 'threadTeamLeader':
                    if(!isset($modelUnderStandableWhereClause['tracking__staff_id__in'])){
                        $modelUnderStandableWhereClause['tracking__staff_id__in']=[];
                    }
                    $staffManagerMapping=$this->model->getStaffManagerMapping();
                    $staffIdList=[];
                    foreach ($filtersApplied['threadTeamLeader'] as $teamLeader){
                        foreach ($staffManagerMapping['managerToStaffMap'][$teamLeader] as $staffID){
                            $staffIdList[]=$staffID;
                        };
                    }
                    foreach ($staffIdList as $staffidd){
                        $modelUnderStandableWhereClause['tracking__staff_id__in'][]=$staffidd;
                    }
                    break;
                case 'threadCreateFromdate':
                case 'threadCreateTodate':
                    
                    $this->_getTrackingTimeWhereClause($filtersApplied['threadCreateFromdate'],
                            $filtersApplied['threadCreateTodate'],$modelUnderStandableWhereClause);
                    break;
                
            }
        }
        return $modelUnderStandableWhereClause;
    }
    private function _getThreadEntryCreationTimeWhereClause($ticketFromDate,$ticketToDate,&$modelUnderStandableWhereClause){
        $ticketFromDateValid=  validateDate($ticketFromDate);
        $ticketToDateValid=  validateDate($ticketToDate);
        if($ticketFromDateValid && $ticketToDateValid){
            $modelUnderStandableWhereClause['ticketThreadEntry__created__btw']=array($ticketFromDate,$ticketToDate);
        }elseif($ticketFromDateValid){
            $ticketToDate="9999-12-31";
            $modelUnderStandableWhereClause['ticketThreadEntry__created__btw']=array($ticketFromDate,$ticketToDate);
        }elseif($ticketToDateValid){
             $ticketFromDate="1000-01-01";
             $modelUnderStandableWhereClause['ticketThreadEntry__created__btw']=array($ticketFromDate,$ticketToDate);
        }
    }
    private function _getTrackingTimeWhereClause($ticketFromDate,$ticketToDate,&$modelUnderStandableWhereClause){
        $ticketFromDateValid=  validateDate($ticketFromDate);
        $ticketToDateValid=  validateDate($ticketToDate);
        if($ticketFromDateValid && $ticketToDateValid){
            $modelUnderStandableWhereClause['tracking__new_timestamp__btw']=array($ticketFromDate,$ticketToDate);
        }elseif($ticketFromDateValid){
            $ticketToDate="9999-12-31";
            $modelUnderStandableWhereClause['tracking__new_timestamp__btw']=array($ticketFromDate,$ticketToDate);
        }elseif($ticketToDateValid){
             $ticketFromDate="1000-01-01";
             $modelUnderStandableWhereClause['tracking__new_timestamp__btw']=array($ticketFromDate,$ticketToDate);
        }
    }
    private function _getTicketCreationTimeWhereClause($ticketFromDate,$ticketToDate){
        $ticketFromDateValid=  validateDate($ticketFromDate);
        $ticketToDateValid=  validateDate($ticketToDate);
        if($ticketFromDateValid && $ticketToDateValid){
            $modelUnderStandableWhereClause['ticket__created__btw']=array($ticketFromDate,$ticketToDate);
        }elseif($ticketFromDateValid){
            $ticketToDate="9999-12-31";
            $modelUnderStandableWhereClause['ticket__created__btw']=array($ticketFromDate,$ticketToDate);
        }elseif($ticketToDateValid){
             $ticketFromDate="1000-01-01";
             $modelUnderStandableWhereClause['ticket__created__btw']=array($ticketFromDate,$ticketToDate);
        }
        return $modelUnderStandableWhereClause;
    }
    private function _getTaskCreationTimeWhereClause($taskFromDate,$taskToDate){
        $taskFromDateValid=  validateDate($taskFromDate);
        $taskToDateValid=  validateDate($taskToDate);
        
        if($taskFromDateValid && $taskToDateValid){
            $modelUnderStandableWhereClause['task__created__btw']=array($taskFromDate,$taskToDate);
        }elseif($taskFromDateValid){
            $taskToDate="9999-12-31";
            $modelUnderStandableWhereClause['task__created__btw']=array($taskFromDate,$taskToDate);
        }elseif($taskToDateValid){
             $taskFromDate="1000-01-01";
             $modelUnderStandableWhereClause['task__created__btw']=array($taskFromDate,$taskToDate);
        }
        return $modelUnderStandableWhereClause;
    }


    private function _processFilters($filtersApplied){
        if($filtersApplied){
        $filtersApplied=  $this->_filterFiltersApplied($filtersApplied);
        $filterForTickets=  $this->_getFiltersForTickets($filtersApplied);
        $filtersForTask=  $this->_getFiltersForTask($filtersApplied);
        $filtersForTaskData=$this->_getFiltersForTaskData($filtersApplied);
        $filtersForThreadEntry=$this->_getFiltersForThreadEntry($filtersApplied);
        $filtersForTracking=$this->_getFiltersForTracking($filtersApplied);
        $filters['filtersForTickets']=$filterForTickets;
        $filters['filtersForTriageData']=  $this->_getFiltersForTriageData($filtersApplied);
        $filters['filtersForTask']=$filtersForTask;
        $filters['filtersForTaskData']=$filtersForTaskData;
        $filters['filtersForThreadEntry']=$filtersForThreadEntry;
        $filters['filtersForTracking']=$filtersForTracking;
        return $filters;
        }
        return array();
    }
    private function _getFiltersForTriageData($filtersApplied) {
        $modelUnderStandableWhereClause = array();
        foreach ($filtersApplied as $filterName => $filterValue) {
            switch ($filterName) {
                case 'priority':
                    $modelUnderStandableWhereClause['ticketTriage__priority__in'] = $filtersApplied['priority'];
                    break;
            }
        }
        return $modelUnderStandableWhereClause;
    }
    public function triage_dump($filePointer,$staticFields,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $staffArray = $this->model->staff_data();
        $chunkSize = TRIAGE_DATA_BUCKET_SIZE;
        $chunkOffset = 0;
        while (1) {
            $res = $this->model->getTicketsTriageDataForDump($chunkOffset,$modelRepresentedFilter,$chunkSize);
            $count = $res->num_rows;
            $chunkOffset++;
            while($T = db_fetch_array($res)){
                $topic_id = $T['topic_id'];
                $issueMap = getIssuesRelatedInfoFromTopicId($staticFields, $topic_id);
                $priorityName = $staticFields["priority"][$T["priority"]]["name"];
                if (empty($priorityName))
                    $priorityName = 'N/A';
                $staffName = $staticFields['staff'][$T['staff_id']];
                if (empty($staffName))
                    $staffName = 'N/A';
                if (empty($T['order_id']))
                    $T['order_id'] = 'N/A';
                if(empty($T['triage__triage']))
                    $T['triage__triage'] = 1;
                $team_leader = $staticFields['staff'][$staffArray[$T['staff_id']]['manager']]? : 'N/A';
                $dataInCsv = array($T['number'], $T['source_extra'], $priorityName, $T['created'], $T['order_id'], $issueMap['issueType'], $issueMap['issueSubType'], $issueMap['issueSubSubType'], '-', $staffName, $staffArray[$T['staff_id']]['email'], $team_leader, $T['triage_date'], $staticFields['Triage']['Triage Status'][$T['triage_status']], $T['updated'], $T['aht'], $T['sla_status']);
                fputcsv($filePointer, $dataInCsv);
                unset($dataInCsv);
                unset($parent_id);
                unset($topic_id);
                unset($staffName);
                unset($priorityName);
            }
            if($count<$chunkSize)
                break;
        }
    }
    public function triage_sla_agent_level(&$tickets,$filtersApplied){
        $staffArray = $this->model->staff_data();
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $res = $this->model->getTriageHandledAgentWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {

            if ($result['staff_id']) {
                $tickets[$result['thread_date']][$result['staff_id']]['triage_handled'] = $result['ticket_count'];
                $tickets[$result['thread_date']][$result['staff_id']]["triage_handled_wsla"] = $result['within'];
                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
            }
        }

        $res = $this->model->getTriageAgainHandledAgentWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {

            if ($result['staff_id']) {
                $tickets[$result['thread_date']][$result['staff_id']]['triage_again_handled'] += $result['ticket_count'];
                $tickets[$result['thread_date']][$result['staff_id']]["triage_again_handled_wsla"] += $result['within'];
                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
            } else {

                $staff_id = array_search($result['staff_name'], $staticFields['staff']);
                $tickets[$result['thread_date']][$staff_id]['triage_again_handled'] += $result['ticket_count'];
                $tickets[$result['thread_date']][$staff_id]["triage_again_handled_wsla"] += $result['within'];
                $tickets[$result['thread_date']][$staff_id]['email'] = $staffArray[$staff_id]['email'];
                $tickets[$result['thread_date']][$staff_id]['manager'] = $staffArray[$staff_id]['manager'];
            }
        }
    }
    private function _getTicketsReceivedTriageWiseinlasttwodays(&$tickets,$triageType,$modelRepresentedFilter,$flag){
        $TimeToTriage = $this->model->getTimeToTriage();
        $res = $this->model->getTicketsReceivedTriageWise($triageType,$modelRepresentedFilter,$flag);
        while ($result = db_fetch_array($res)) {
            $create_date = strtotime($result['date']);
            $current_time = time();
            for ($i=$create_date; $i<=$create_date+$TimeToTriage  ; $i=$i+1*24*60*60) { 
                if($i<=$current_time || (strtotime(date('Y-m-d',$i))==strtotime(date('Y-m-d',$current_time)))){
                    $tickets[date('Y-m-d',$i)][$result['topic_id']][$triageType]['ticket_count_overall']++;
                }
            }
        }
    }
    public function triage_sla_issue_type(&$tickets,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $res = $this->model->getTriageHandledIssueWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['thread_date']][$result['topic_id']]['triage']['ticket_count'] = $result['ticket_count'];
            $tickets[$result['thread_date']][$result['topic_id']]['triage']['wsla'] = $result['within'];
        }
        $res = $this->model->getTriageAgainHandledIssueWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['ticket_count'] = $result['ticket_count'];
            //$tickets[$result['thread_date']][$result['topic_id']]['triage_again']['wsla'] = $result['within'];
        }

        $res = $this->model->getTriageAgainHandledWithinSla($modelRepresentedFilter,1);
        while ($result = db_fetch_array($res)) {
            $t[$result['thread_id']]['topic_id'] = $result['topic_id'];
            if($result['title']=='Triage again'){
                $create_dates = explode(',', $result['create_dates']);
                $t[$result['thread_id']]['created'] = $create_dates;
            }
            else{
                $triage_dates = explode(',', $result['create_dates']);
                $t[$result['thread_id']]['triaged'] = $triage_dates;
            }
            //$tickets[$result['date']]['triage_again_handled_wsla'] = $result['within'];
        }

        foreach ($t as $thread_id => $value) {
            $create_dates = $value['created'];
            $triage_dates = $value['triaged'];
            for ($i=0; $i <min(sizeof($create_dates),sizeof($triage_dates)) ; $i++) { 
                for ($j=0; $j <sizeof($triage_dates) ; $j++) { 
                    if(strtotime($triage_dates[$j])>strtotime($create_dates[$i])){
                        if(strtotime($triage_dates[$j])-strtotime($create_dates[$i])<=getTimeToTriage()){
                            $tickets[date('Y-m-d',strtotime($triage_dates[$j]))][$value['topic_id']]['triage_again']['wsla']++; 
                        }
                        break;
                    }
                }
            }
        }
        $this->_getTicketsReceivedTriageWiseinlasttwodays($tickets,'triage',$modelRepresentedFilter,1);
        $this->_getTicketsReceivedTriageWiseinlasttwodays($tickets,'triage_again',$modelRepresentedFilter,1);
        $this->getTicketsOpenSlaTriageWise($tickets,'triage',$modelRepresentedFilter,1);
        $this->getTicketsOpenSlaTriageWise($tickets,'triage_again',$modelRepresentedFilter,1);
        krsort($tickets);
        foreach ($tickets as $date => $value) {
            foreach ($value as $topic_id => $val) {
                if(!$val['triage']['ticket_count'] && !$val['triage_again']['ticket_count']){
                    unset($tickets[$date][$topic_id]);
                }
            }
        }
    }
    public function triage_aht_agent_level(&$tickets,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $staffArray = $this->model->staff_data();
        $TimeToTriage = $this->model->getTimeToTriage();
        $res = $this->model->getTriageHandledAgentWise($modelRepresentedFilter);
        $count = 0;
        while ($result = db_fetch_array($res)) {
            
            $count++;
            //print_r($result);
            if ($result['staff_id']) {
                $tickets[$result['thread_date']][$result['staff_id']]['triage_handled'] = $result['ticket_count'];
                $tickets[$result['thread_date']][$result['staff_id']]["triage_handled_wsla"] = $result['within'];
                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
            }
        }
        $res = $this->model->getAhtForTriagingAgentWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['staff_id']]['triage']['timediff'] = $result['timediff'];
            $tickets[$result['date']][$result['staff_id']]['triage']['no_for_aht'] = $result['ticket_count'];
            $tickets[$result['date']][$result['staff_id']]['triage_handled_aht'] = $result['timediff'] / $result['ticket_count'];
        }
        $res = $this->model->getTriageAgainHandledAgentWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            
            if ($result['staff_id']) {
                //if ($result['title'] == 'Triage field chage' || $result['title'] == 'Triage field change') {
                    $tickets[$result['thread_date']][$result['staff_id']]['triage_again_handled'] += $result['ticket_count'];
                    $tickets[$result['thread_date']][$result['staff_id']]["triage_again_handled_wsla"] += $result['within'];
                    $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                    $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
                //}
            } else {
                $staff_id = array_search($result['staff_name'], $staticFields['staff']);
                $tickets[$result['thread_date']][$staff_id]['triage_again_handled'] += $result['ticket_count'];
                $tickets[$result['thread_date']][$staff_id]["triage_again_handled_wsla"] += $result['within'];
                $tickets[$result['thread_date']][$staff_id]['email'] = $staffArray[$staff_id]['email'];
                $tickets[$result['thread_date']][$staff_id]['manager'] = $staffArray[$staff_id]['manager'];
            }
        }
        $res = $this->model->getAhtForTriageAgainAgentWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['staff_id']]['triage_again']['timediff'] = $result['timediff'];
            $tickets[$result['date']][$result['staff_id']]['triage_again']['no_for_aht'] = $result['ticket_count'];
            $tickets[$result['date']][$result['staff_id']]['triage_again_handled_aht'] = $result['timediff'] / $result['ticket_count'];
        }
    }
    public function triage_aht_issue_type(&$tickets,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $TimeToTriage = $this->model->getTimeToTriage();
        $res = $this->model->getTriageHandledIssueWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['thread_date']][$result['topic_id']]['triage']['ticket_count'] = $result['ticket_count'];
            $tickets[$result['thread_date']][$result['topic_id']]['triage']['wsla'] = $result['within'];
        }
        
        $res = $this->model->getAhtForTriagingIssueWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['topic_id']]['triage']['timediff'] = $result['timediff'];
            $tickets[$result['date']][$result['topic_id']]['triage']['no_for_aht'] = $result['ticket_count'];
            $tickets[$result['date']][$result['topic_id']]['triage']['aht'] = $result['timediff'] / $result['ticket_count'];
        }

        $res = $this->model->getTriageAgainHandledIssueWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['ticket_count'] = $result['ticket_count'];
            $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['wsla'] = $result['within'];
        }
        $res = $this->model->getAhtForTriageAgainIssueWise($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['topic_id']]['triage_again']['timediff'] = $result['timediff'];
            $tickets[$result['date']][$result['topic_id']]['triage_again']['no_for_aht'] = $result['ticket_count'];
            $tickets[$result['date']][$result['topic_id']]['triage_again']['aht'] = $result['timediff'] / $result['ticket_count'];
        }
    }
    

    public function getTriagePendencyData(&$tickets, $filtersApplied) {
        $currentDate = date('Y-m-d');
        $calculateCurrentDateData = 0;
        if (($currentDate >= $filtersApplied['threadCreateFromdate'] || empty($filtersApplied['threadCreateFromdate'])) &&
                ($currentDate <= $filtersApplied['threadCreateTodate'] || empty($filtersApplied['threadCreateTodate']))) {
            $calculateCurrentDateData = 1;
        }
        $pendencyData = $this->model->getPendencySavedData($filtersApplied);
        foreach ($pendencyData as $pendency) {
            $dateKey = date('Y-m-d', strtotime($pendency['date']));
            $tickets[$dateKey]['triage_total'] = $pendency['fresh_tickets_received_untriaged'];
            $tickets[$dateKey]['triage_again_total'] = $pendency['fresh_tickets_received_triage_again'];
            $tickets[$dateKey]['old_ticket_triage'] = $pendency['old_tickets_untriaged'];
            $tickets[$dateKey]['old_ticket_triage_again'] = $pendency['old_tickets_triage_again'];
            $tickets[$dateKey]['triage_handled'] = $pendency['tickets_handled_untriaged'];
            $tickets[$dateKey]['triage_again_handled'] = $pendency['tickets_handled_triage_again'];
            $tickets[$dateKey]['triage_pending'] = $pendency['tickets_pending_untriaged'];
            $tickets[$dateKey]['triage_again_pending'] = $pendency['tickets_pending_triage_again'];
        }
        if ($calculateCurrentDateData) {
            $filtersApplied['threadCreateFromdate'] = $currentDate;
            $filtersApplied['threadCreateTodate'] = $currentDate;
            $modelRepresentedFilter = $this->_processFilters($filtersApplied);
            $res = $this->model->getTicketsReceivedUntriagedPerDate($modelRepresentedFilter);
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_total'] = $result['ticket_count'];
            }
            //todo: exception condition filter to be applied mst_ticket closed field
            $res = $this->model->getTicketsNotTriagedClosedPerDate($modelRepresentedFilter);
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_handled'] +=$result['ticket_count'];
            }
            $res = $this->model->getTicketsTriagedPerDate($modelRepresentedFilter);
            while ($result = db_fetch_array($res)) {

                $tickets[$result['date']]['triage_handled'] +=$result['ticket_count'];
            }
            $res = $this->model->getTicketsReceivedTriageAgainPerDate($modelRepresentedFilter);
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_again_total'] +=$result['ticket_count'];
            }
            $res = $this->model->getTicketsHandledTriageAgainPerDate($modelRepresentedFilter);
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_again_handled'] +=$result['ticket_count'];
            }
            //todo: exception condition filter to be applied mst_ticket closed field
            $res = $this->model->getTicketsNotTriageAgainClosedPerDate($modelRepresentedFilter);
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_again_handled'] +=$result['ticket_count'];
            }
            $triagePendingAndTriageAgainPendingCount = $this->model->getTicketsPendingUnTriagedAndTriageAgainPending();
            $tickets[date('Y-m-d')]['triage_pending'] = $triagePendingAndTriageAgainPendingCount['triage_pending'];
            $tickets[date('Y-m-d')]['triage_again_pending'] = $triagePendingAndTriageAgainPendingCount['triage_again_pending'];
            $previousDate = date('Y-m-d',strtotime('-1 day',time()));
            $tickets[date('Y-m-d')]['old_ticket_triage'] = $tickets[$previousDate]['old_ticket_triage'];
            $tickets[date('Y-m-d')]['old_ticket_triage_again'] = $tickets[$previousDate]['old_ticket_triage_again'];
        }
        krsort($tickets);
    }

    public function triage_pendency(&$tickets,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $res = $this->model->getTicketsReceivedUntriagedPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_total'] = $result['ticket_count'];
        }
        $res = $this->model->getTicketsNotTriagedClosedPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_handled'] +=$result['ticket_count'];
        }
        $res = $this->model->getTicketsTriagedPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_handled'] +=$result['ticket_count'];
        }
        $res = $this->model->getTicketsReceivedTriageAgainPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_again_total'] +=$result['ticket_count'];
        }
        $res = $this->model->getTicketsHandledTriageAgainPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_again_handled'] +=$result['ticket_count'];
        }
        //todo: exception condition filter to be applied mst_ticket closed field
        $res = $this->model->getTicketsNotTriageAgainClosedPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_again_handled'] +=$result['ticket_count'];
        }
        $res = $this->model->getTicketPendingUntriaged($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $endTime = $result['triage_date'] ? strtotime($result['triage_date']) : ($result['close_date'] ? strtotime($result['close_date']) : '');
            $createDateOnlyTime = strtotime(date('Y-m-d', strtotime($result['create_date'])));
            if ($endTime) {
                $endDateOnlyTime = strtotime(date('Y-m-d', $endTime));
                for ($i = $createDateOnlyTime; $i < $endDateOnlyTime; $i = $i + (1 * 24 * 3600)) {
                    $tickets[date('Y-m-d', $i)]['triage_pending']++;
                }
            } else {
                $finalDateTime = strtotime(date('Y-m-d', strtotime('+1 day', time())));
                for ($i = $createDateOnlyTime; $i < $finalDateTime; $i = $i + (1 * 24 * 3600)) {
                    if ($result['status_id'] == 1 || $result['status_id'] == 6) {
                        $tickets[date('Y-m-d', $i)]['triage_pending']++;
                    }
                }
            }
            unset($endTime);
        }
        $filterFromDate=$filtersApplied['threadCreateFromdate'];
        $filterToDate=$filtersApplied['threadCreateTodate'];
        global $reportRelatedConfig;
        $fromDateInTime = $filterFromDate?strtotime($filterFromDate):strtotime($reportRelatedConfig['report_start_date']);
        $toDate = $filterToDate?:date("Y-m-d");
        $toDateInTime = $filterToDate?strtotime($filterToDate):strtotime(date("Y-m-d"));
        $dayDiff = ($toDateInTime-$fromDateInTime)/(60*60*24);
        if($dayDiff<0){
            $toDateInTime = strtotime(date("Y-m-d",strtotime("+1 day")));
            $dayDiff = 1;
        }
        $res = $this->model->getTicketPendingTriageAgain($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            for($i=-1;$i<=$dayDiff;$i++){
                $date = strtotime(date("Y-m-d",strtotime($toDate."-".$i." day")));
                if(strtotime($result['create_date'])<$date && (($result['triage']==3 && strtotime($result['triage_again'])<$date) || ($result['triage']==2 && strtotime($result['triage_time'])>$date)) && ($result['status_id'] == 1 || $result['status_id'] == 6 || strtotime($result['closed'])>$date)){
                    if(!$ticketsss[date('Y-m-d',$date)][$result['thread_id']]){
                        $ticketss[date('Y-m-d',$date)]['old_ticket_triage_again'] ++;
                        $ticketsss[date('Y-m-d',$date)][$result['thread_id']]=1;
                    }
                }
            }
            //unset($ticketsss);
        }     
        unset($ticketsss);
        $triage_pending = 0;
        $triage_again_pending = 0;
        foreach ($tickets as $date => $value) {
            $nextDay = date("Y-m-d",strtotime($date."+1 day"));
            $prevDay = date("Y-m-d",strtotime($date."-1 day"));
            $tickets[$date]['old_ticket_triage_again'] = $ticketss[$date]['old_ticket_triage_again'];
            $tickets[$date]['triage_again_pending'] = $ticketss[$nextDay]['old_ticket_triage_again'];
            $tickets[$date]['old_ticket_triage'] = $tickets[$prevDay]['triage_pending'];
        }
        foreach ($tickets as $date => $value) {
            $thisDate  = strtotime($date);
            if($thisDate<$fromDateInTime || $thisDate>$toDateInTime)
                unset($tickets[$date]);
        }
    }
    public function getTicketsOpenSlaTriageWise(&$tickets,$triageType,$modelRepresentedFilter,$flag){
        set_time_limit(1000);
        $TimeToTriage = $this->model->getTimeToTriage();
        $res = $this->model->getDataToCalTicketsOpenSlaTriageWise($triageType,$modelRepresentedFilter,$flag);
        while ($result = db_fetch_array($res)) {
            for($i=strtotime($result['create_date']); $i<=(strtotime($result['create_date'])+$TimeToTriage); $i=$i+(1*24*3600)) {
                    $triageDate = $result['triage_date']?strtotime(date('Y-m-d',strtotime($result['triage_date']))):($i+5*$TimeToTriage);
                    $closeDate = $result['close_date']?strtotime(date('Y-m-d',strtotime($result['close_date']))):($i+5*$TimeToTriage);
                    $tTime = ($triageDate<$closeDate)?$triageDate:$closeDate;
                    if($i<=$tTime && strtotime(date('Y-m-d',$i))<=strtotime(date('Y-m-d'))){
                        if((strtotime(date('Y-m-d H:i:s'))<=(strtotime($result['create_date'])+$TimeToTriage)) || (strtotime(date('Y-m-d',$i))<strtotime(date('Y-m-d'))))
                        {
                            if($flag){
                                $tickets[date('Y-m-d',$i)][$result['topic_id']][$triageType]['open_wsla']++;
                            }
                            else{
                                $tickets[date('Y-m-d',$i)][$triageType.'_open_wsla']++;
                            }
                        }
                    }

            }

        }
        unset($ticket);
        

    }
    public function getTriageSlaSummaryData(&$tickets,$filtersApplied){
        $current_date = date('Y-m-d',time());
        $calculateCurrentDateData = 0;
        /*
        This checks whether the current date is included in the filter
        */
        if (($current_date >= $filtersApplied['threadCreateFromdate'] || empty($filtersApplied['threadCreateFromdate'])) &&
                ($current_date <= $filtersApplied['threadCreateTodate'] || empty($filtersApplied['threadCreateTodate']))) {
            $calculateCurrentDateData = 1;
        }
        /*
        This function gets data of previous dates from DB.
        */
        $res = $this->model->getSlaSummarySavedData($filtersApplied);
        while ($result = db_fetch_array($res)) {
            $dateKey = $result['date'];
            $tickets[$dateKey]['triage_total_overall'] =$result['tickets_received_untriaged'];
            $tickets[$dateKey]['triage_again_total_overall'] =$result['tickets_received_triage_again'];
            $tickets[$dateKey]['triage_handled_wsla'] =$result['tickets_handled_within_sla_untriaged'];
            $tickets[$dateKey]['triage_again_handled_wsla'] =$result['tickets_handled_within_sla_triage_again'];
            $tickets[$dateKey]['triage_open_wsla'] = $result['tickets_open_within_sla_untriaged'];
            $tickets[$dateKey]['triage_again_open_wsla'] = $result['tickets_open_within_sla_triage_again'];
        }
        /*
        function call ends
        */

        /*
        Here the values are calculated for the current date
        */
        if($calculateCurrentDateData){
            $filtersApplied['threadCreateFromdate'] = $current_date;
            $filtersApplied['threadCreateTodate'] = $current_date;
            $modelRepresentedFilter = $this->_processFilters($filtersApplied);
            /*
            This section calculates tickets open within sla both (triage and traige again)
            */
            $res = $this->model->getTicketsOpenWithinSlaCurrentDate();
            while ($result = db_fetch_array($res)) {
                if($result['triage']==1){
                    $tickets[$current_date]['triage_open_wsla']=$result['ticket_count']-$result['pending'];
                }
                else if($result['triage']==3){
                    $tickets[$current_date]['triage_again_open_wsla']=$result['ticket_count']-$result['pending'];
                }
                
            }
            /*
            section ends
            */
            $res = $this->model->getTicketsReceivedUntriagedPerDate($modelRepresentedFilter,"current");
            $total_count = 0;
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_total_overall'] =  $result['ticket_count'];
            }

            $res = $this->model->getTicketsReceivedTriageAgainPerDate($modelRepresentedFilter,"current");
            $total_count = 0;
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_again_total_overall'] =  $result['ticket_count'];
            }

            /*
            calculation of triage handled within sla
            Triage handled within sla = tickets triaged within sla + tickets not triaged but closed within sla
            */
            $res = $this->model->getTriagedHandledWithinSla($modelRepresentedFilter,"current");
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_handled_wsla'] = $result['within'];
            }
            $res = $this->model->getTicketsNotTriagedClosedPerDate($modelRepresentedFilter,"current");
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_handled_wsla'] += $result['within'];
            }
            /*
            calculation of triage handled within sla ends
            */



            /*
            This function calculates tickets handled within sla triage again taking the last value of the date when the tickets comes in triage again.
            Tickets handled within sla triage again = tickets triaged within sla of triage again + tickets not triaged but closed of triage again
            */
            $res = $this->model->getTriageAgainHandledWithinSlaCurrentDate();
            while ($result = db_fetch_array($res)) {
                $tickets[$current_date]['triage_again_handled_wsla'] = $result['ticket_count'];
            }
            
            $res = $this->model->getTicketsNotTriageAgainClosedPerDate($modelRepresentedFilter,"current");
            while ($result = db_fetch_array($res)) {
                $tickets[$result['date']]['triage_again_handled_wsla'] += $result['within'];
            }
            $last_date = date("Y-m-d",strtotime("-1 day"));
            $tickets[$current_date]['triage_total_overall'] +=$tickets[$last_date]['triage_total_overall'];
            $tickets[$current_date]['triage_again_total_overall'] +=$tickets[$last_date]['triage_again_total_overall'];
            $tickets[$current_date]['triage_handled_wsla'] +=$tickets[$last_date]['triage_handled_wsla'];
            $tickets[$current_date]['triage_again_handled_wsla'] +=$tickets[$last_date]['triage_again_handled_wsla'];
            /*
            Tickets handled within sla triage again code ends
            */
        }
        krsort($tickets);
        
    }
    public function triage_sla_summary(&$tickets,$filtersApplied){
        $modelRepresentedFilter=$this->_processFilters($filtersApplied);
        $this->getTicketsOpenSlaTriageWise($tickets,'triage',$modelRepresentedFilter,0);
        $this->getTicketsOpenSlaTriageWise($tickets,'triage_again',$modelRepresentedFilter,0);
        $res = $this->model->getTicketsReceivedUntriagedPerDate($modelRepresentedFilter);
        $total_count = 0;
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_total_overall'] =  $result['ticket_count'];
        }

        $res = $this->model->getTicketsReceivedTriageAgainPerDate($modelRepresentedFilter);
        $total_count = 0;
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_again_total_overall'] =  $result['ticket_count'];
        }
        $res = $this->model->getTriagedHandledWithinSla($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_handled_wsla'] = $result['within'];
        }
        $res = $this->model->getTicketsNotTriagedClosedPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_handled_wsla'] += $result['within'];
        }
        $res = $this->model->getTriageAgainHandledWithinSla($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            if($result['title']=='Triage again'){
                $create_dates = explode(',', $result['create_dates']);
                $t[$result['thread_id']]['created'] = $create_dates;
            }
            else{
                $triage_dates = explode(',', $result['create_dates']);
                $t[$result['thread_id']]['triaged'] = $triage_dates;
            }
        }
        foreach ($t as $thread_id => $value) {
            $create_dates = $value['created'];
            $triage_dates = $value['triaged'];
            for ($i=0; $i <min(sizeof($create_dates),sizeof($triage_dates)) ; $i++) { 

                for ($j=0; $j <sizeof($triage_dates) ; $j++) { 

                    if(strtotime($triage_dates[$j])>strtotime($create_dates[$i])){
                        if(strtotime($triage_dates[$j])-strtotime($create_dates[$i])<=getTimeToTriage()){
                            $tickets[date('Y-m-d',strtotime($triage_dates[$j]))]['triage_again_handled_wsla']++; 
                        }
                        break;
                    }
                }
            }
        }

        $res = $this->model->getTicketsNotTriageAgainClosedPerDate($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']]['triage_again_handled_wsla'] += $result['within'];
        }
        ksort($tickets);
        $filterFromDate=$filtersApplied['threadCreateFromdate'];
        $filterToDate=$filtersApplied['threadCreateTodate'];
        global $reportRelatedConfig;
        $fromDateInTime = $filterFromDate?strtotime($filterFromDate):strtotime($reportRelatedConfig['report_start_date']);
        $toDateInTime = $filterToDate?strtotime($filterToDate):strtotime(date("Y-m-d"));
        foreach ($tickets as $date => $value) {
            $thisDate  = strtotime($date);
            if($thisDate<$fromDateInTime || $thisDate>$toDateInTime)
                unset($tickets[$date]);
        }
        $pTriage_total_overall=0;
        $pTriage_again_total_overall=0;
        $pTriage_handled_wsla=0;
        $pTriage_again_handled_wsla=0;
        foreach ($tickets as $date=>$value){
            $tickets[$date]['triage_total_overall']+=$pTriage_total_overall;
            $tickets[$date]['triage_again_total_overall']+=$pTriage_again_total_overall;
            $tickets[$date]['triage_handled_wsla']+=$pTriage_handled_wsla;
            $tickets[$date]['triage_again_handled_wsla']+=$pTriage_again_handled_wsla;
            $pTriage_total_overall=$tickets[$date]['triage_total_overall'];
            $pTriage_again_total_overall=  $tickets[$date]['triage_again_total_overall'];
            $pTriage_handled_wsla=$tickets[$date]['triage_handled_wsla'];
            $pTriage_again_handled_wsla=$tickets[$date]['triage_again_handled_wsla'];
        }
        krsort($tickets);
        
    }

    public function _getTasksOpenWsla(&$tickets,$dept_id,$whereClauseArray){
        $res = $this->model->getSlaTaskDataOpen($dept_id,$whereClauseArray);
        while ($result = db_fetch_array($res)) {
            $endTime = $result['close_date']?strtotime($result['close_date']):($result['duedate']?strtotime($result['duedate']):time());
            for ($i=strtotime($result['create_date']); ($result['close_date']?$i <$endTime:$i <=$endTime) ; $i=$i+1*24*60*60) { 
                if($i<=strtotime($result['duedate'])){
                    $tickets[date('Y-m-d',$i)][$result['staff_id']]['task_open_sla']++;
                }
                else{
                    $tickets[date('Y-m-d',$i)][$result['staff_id']]['task_open_osla']++;
                }
            }
        }
    }

    public function ccg_crg_sla(&$tickets,$dept_id,$filtersApplied){
        if($filtersApplied){
            $modelRepresentedFilter = $this->_processFilters($filtersApplied);
        }
        $staffArray = $this->model->staff_data();
        $res = $this->model->getSlaTaskData($dept_id,$modelRepresentedFilter,"all");
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['staff_id']]['total_tasks'] = $result['task_count'];
            $tickets[$result['date']][$result['staff_id']]['total_tasks_overall'] = $t[$result['staff_id']]+$result['task_count'];
            $t[$result['staff_id']] +=$result['task_count'];
        }
        $res = $this->model->getSlaTaskData($dept_id,$modelRepresentedFilter,"solved");
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['staff_id']]['task_solved_sla'] = $result['within'];
            $tickets[$result['date']][$result['staff_id']]['task_solved_osla'] = $result['task_count'] - $result['within'];
        }
        $this->_getTasksOpenWsla($tickets,$dept_id,$modelRepresentedFilter);
        $fromTime = strtotime($_GET['tcfd'])?:0;
        $toTime = strtotime($_GET['tctd'])?:time();
        foreach ($tickets as $date => $value) {
            if(strtotime($date)<$fromTime || strtotime($date)>$toTime)
                unset($tickets[$date]);
            else{
                foreach ($value as $staff => $val) {
                    $tickets[$date][$staff]['email'] = $staffArray[$staff]['email'];
                    $tickets[$date][$staff]['manager'] = $staffArray[$staff]['manager'];
                }
            }
        }
        krsort($tickets);
    }
    public function ccg_prod(&$tickets,$filtersApplied){
        if($filtersApplied){
            $modelRepresentedFilter = $this->_processFilters($filtersApplied);
        }
        $staffArray = $this->model->staff_data();
        $res = $this->model->getEmailTasksHandled($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            
            $tickets[$result['date']][$result['staff_id']]['email_task_handled'] = $result['ticket_count'];
            $tickets[$result['date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
            $tickets[$result['date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
            //_pr($tickets);die;
        }
        
    }

    public function ccg_pendency(&$tickets,$filtersApplied){
        if($filtersApplied){
            $modelRepresentedFilter = $this->_processFilters($filtersApplied);
        }
        $staffArray = $this->model->staff_data();
        // $res = $this->model->getEmailTasksCreated($modelRepresentedFilter);
        // while ($result = db_fetch_array($res)) {
        //     $tickets[$result['date']][$result['staff_id']]['email_task_created'] = $result['ticket_count'];
        // }
        $res = $this->model->getCallTasksCreated($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['staff_id']]['call_task_created'] = $result['ticket_count'];
        }
        // $res = $this->model->getEmailTasksHandled($modelRepresentedFilter);
        // while ($result = db_fetch_array($res)) {
        //     $tickets[$result['date']][$result['staff_id']]['email_task_handled'] = $result['ticket_count'];
        // }
        $res = $this->model->getEmailTasksPending($modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            if($result['type']=='R'){
                $tickets[$result['date']][$result['staff_id']]['email_task_handled']+=$result['ticket_count'];
                $tickets[$result['date']][$result['staff_id']]['email_task_handled_overall'] += $t[$result['staff_id']]['email_task_handled_overall']+$result['ticket_count'];
                $t[$result['staff_id']]['email_task_handled_overall'] += $result['ticket_count'];
                
            }
            else{
                $tickets[$result['date']][$result['staff_id']]['email_task_created']+=$result['ticket_count'];
                $tickets[$result['date']][$result['staff_id']]['email_task_created_overall'] += $t[$result['staff_id']]['email_task_created_overall']+$result['ticket_count'];
                $t[$result['staff_id']]['email_task_created_overall'] += $result['ticket_count'];
            }
            $tickets[$result['date']][$result['staff_id']]['email_task_pending'] = $t[$result['staff_id']]['email_task_created_overall']-$t[$result['staff_id']]['email_task_handled_overall'];
        }
        ksort($tickets);
        $fromTime = strtotime($_GET['thofd'])?:0;
        $toTime = strtotime($_GET['thotd'])?:time();
        foreach ($tickets as $date => $value) {
            if(strtotime($date)<$fromTime || strtotime($date)>$toTime){
                unset($tickets[$date]);
            }
            else{
                foreach ($value as $staff => $val) {
                    $tickets[$date][$staff]['email'] = $staffArray[$staff]['email'];
                    $tickets[$date][$staff]['manager'] = $staffArray[$staff]['manager'];
                }
            }
        }
        krsort($tickets);
        //print_r($tickets);die;
    }
    public function crg_aht(&$tickets,$filtersApplied){
        if($filtersApplied){
            $modelRepresentedFilter = $this->_processFilters($filtersApplied);
        }
        $dept_id = 14;
        $res = $this->model->getTasksHandledDirectly($dept_id,$modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $tickets[$result['date']][$result['staff_id']]['task_handled'] = $result['count'];
            $tickets[$result['date']][$result['staff_id']]['timediff'] = $result['timediff'];
            $tickets[$result['date']][$result['staff_id']]['aht'] = ($result['timediff']/$result['count']);

        }
        $res = $this->model->getTasksHandledIndirectly($dept_id,$modelRepresentedFilter);
        while ($result = db_fetch_array($res)) {
            $initial_count = $tickets[$result['date']][$result['staff_id']]['task_handled'];
            $tickets[$result['date']][$result['staff_id']]['aht'] = ($result['timediff']+$tickets[$result['date']][$result['staff_id']]['aht']*$initial_count)/($initial_count + $result['count']);
            $tickets[$result['date']][$result['staff_id']]['task_handled'] += $result['count'];
        }
    }

    public function crg_prod(&$tickets,$dept_id,$filtersApplied){
        
    }

}

?>
