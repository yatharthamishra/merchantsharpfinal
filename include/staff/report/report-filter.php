<div class="table_with_links left_tickets_link grid2 float_left">
     
    <div class="left_slide_bar_report">
        <ul>
            <li>
                <?php
                $url = "reports.php?report=cs-report"; 
                ?>
                <h3><a href="<?php echo $url.'&type=triage&subtype=all'; ?>" <?php if($flag=="t_all") echo "class='active'"; ?>>Triaging</a></h3> 
                <ul>
                    <li>
                        <a href="<?php echo $url.'&type=triage&subtype=sla adherence&subsubtype=agent level'; ?>" <?php if($flag=="t_sla") echo "class='active'"; ?>>SLA Adherence</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=triage&subtype=aht&subsubtype=agent level'; ?>" <?php if($flag=="t_aht") echo "class='active'"; ?>>Average Handling Time</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=triage&subtype=pendency'; ?>" <?php if($flag=="t_pend") echo "class='active'"; ?>>Pendency</a>
                    </li>
                </ul>
            </li>
            <li>
                <h3><a href="<?php echo $url.'&type=ccg&subtype=all'; ?>" <?php if($flag=="ccg_all") echo "class='active'"; ?>>CCG</a></h3>
                <ul>
                    <li>
                        <a href="<?php echo $url.'&type=ccg&subtype=sla adherence&subsubtype=agent level'; ?>" <?php if($flag=="ccg_sla") echo "class='active'"; ?>>SLA Adherence</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=ccg&subtype=productivity'; ?>" <?php if($flag=="ccg_prod") echo "class='active'"; ?>>Productivity</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=ccg&subtype=pendency'; ?>" <?php if($flag=="ccg_pend") echo "class='active'"; ?>>Pendency</a>
                    </li>
                </ul>
            </li>
                <li>
                <h3><a href="<?php echo $url.'&type=crg&subtype=all'; ?>" <?php if($flag=="crg_all") echo "class='active'"; ?>>CRG</a></h3>
                <ul>
                    <li>
                        <a href="<?php echo $url.'&type=crg&subtype=sla adherence&subsubtype=agent level'; ?>" <?php if($flag=="crg_sla") echo "class='active'"; ?>>SLA Adherence</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=crg&subtype=aht&subsubtype=agent level'; ?>" <?php if($flag=="crg_aht") echo "class='active'"; ?>>AHT</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=crg&subtype=productivity'; ?>" <?php if($flag=="crg_prod") echo "class='active'"; ?>>Productivity</a>
                    </li>
                </ul>
            </li>
            <li>
                <h3><a href="<?php echo $url.'&type=ops&subtype=all'; ?>" <?php if($flag=="ops_all") echo "class='active'"; ?>>OPS</a></h3>
                <ul>
                    <li>
                        <a class="export-csv no-pjax" href="<?php echo $url.'&type=ops&subtype=departmentwise sla'; ?>" <?php if($flag=="ops_dept") echo "class='active'"; ?>>Departmentwise Sla</a>
                    </li>
                    <li>
                        <a href="<?php echo $url.'&type=ops&subtype=taskwise sla'; ?>" <?php if($flag=="ops_task") echo "class='active'"; ?>>Taskwise Sla</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>