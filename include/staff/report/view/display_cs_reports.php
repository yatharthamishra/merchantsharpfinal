<div class="clear"></div>
<div onclick="javascript:        $.dialog('ajax.php/tickets/search', 201);" style="cursor:pointer;margin-bottom:20px; padding-top:10px;text-align: center; position:relative;">
</div>
<div class="clear"></div>

<!--*******************************************
    Code for displaying customized reports
-->
<div class="container_section grid-row">
<?php
include('report-filter1.php');
?>
    <div id="customDivContainer" class="grid10 table_section float_right">
        <div style="padding:5px;">
            <div class="sticky bar stop opaque">
                <div class="content heading_action_section grid-row padding-container">
    <?php
    if ((stripos($reportSubtype, "sla") !== false || stripos($reportSubtype, "aht") !== false) && ($reportType!='ops'&& $reportType!='bus' && $reportType!="ove")) {
        
        ?>
                    <div class="pull-left flush-left" style="width:100%;">
                        <label class="pull-left flush-left">
                            <h3>Show activity by:</h3>

                        </label>
                        <div  class="">
                            <a href="<?php echo $url . '&type=' . $reportType . '&subtype=' . $reportSubtype . '&subsubtype=agent level'; ?>" class="type buttons <?php if (stripos($reportSubSubtype, 'agent') !== false) echo 'checked'; ?>">
                                Agent Level Summary Report
                            </a>
                            <?php
                            if($flag=="t_sla" || $flag=="t_aht"){
                                ?>
                                <a href="<?php echo $url . '&type=' . $reportType . '&subtype=' . $reportSubtype . '&subsubtype=issue type'; ?>" class="type buttons <?php if (stripos($reportSubSubtype, 'issue') !== false) echo 'checked'; ?>">
                                    Issue Type Summary Report
                                </a>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
        <?php
    }
    ?>
                    <div class="clear"></div>
                    <?php 
                    include REPORT_VIEWS_PATH.'filter.phtml';
                    ?>
                </div>
            </div>
            <div class="clear"></div>

            <form action="tickets.php" method="POST" name='tickets' id="tickets">
                    <?php csrf_token(); ?>
                <input type="hidden" name="a" value="mass_process" >
                <input type="hidden" name="do" id="action" value="" >
                <input type="hidden" name="status" value="<?php echo
                    Format::htmlchars($_REQUEST['status'], true);
                    ?>" >
                <table class="list" border="0" cellspacing="1" cellpadding="2" width="940">
                    <?php if(stripos($flag,"all")===false){ ?>
                    <thead>
                        <tr>
                                <?php
                                foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'] as $value => $val) {
                                   if(!in_array($value, $fieldNotToBeDisplayed)){
                                       echo "<th >" . $value . "</th>";
                                   } 
                                }
                                ?>

                        </tr>

                    </thead>
                    <?php } ?>
                    <tbody>
                        <?php
                        $total = 0;
                        if ($flag == "t_sla") {
                            if ($reportSubSubtype == "agent level") {
                                foreach ($tickets as $key => $value) {
                                    foreach ($value as $k => $val) {
                                        unset($triage_handled_total);
                                        unset($sla_total);
                                        unset($sla_triage);
                                        unset($sla_triage_again);
                                        ?>    
                                        <tr>
                                            <td class="<?php echo implode('_', explode(' ', $array_keys[0])); ?>"><?php echo $key; ?></td>
                                            <td class="<?php echo implode('_', explode(' ', $array_keys[2])); ?>"><?php echo $staticFields['staff'][$k]? : '-'; ?></td>
                                            <td><?php echo $val['email']? : '-'; ?></td>
                                            <td class="<?php echo implode('_', explode(' ', $array_keys[3])); ?>"><?php echo $staticFields['staff'][$val['manager']]? : '-'; ?></td>
                                            <td><?php echo $val['triage_handled']? : '-'; ?></td>
                                            <td><?php echo $val['triage_again_handled']? : '-'; ?></td>
                                            <?php
                                            if (isset($val['triage_handled']) || isset($val['triage_again_handled'])) {
                                                $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                                                $sla_total = (($val['triage_handled_wsla']? : 0) + ($val['triage_again_handled_wsla']? : 0)) * 100 / (($val['triage_handled']? : 0) + ($val['triage_again_handled']? : 0));
                                            }
                                            if (isset($val['triage_handled'])) {
                                                $sla_triage = ($val['triage_handled_wsla']? : 0) * 100 / $val['triage_handled'];
                                            }
                                            if (isset($val['triage_again_handled'])) {
                                                $sla_triage_again = ($val['triage_again_handled_wsla']? : 0) * 100 / $val['triage_again_handled'];
                                            }
                                            ?>
                                            <td><?php echo isset($triage_handled_total) ? $triage_handled_total : '-'; /*?></td>
                                            <td><?php echo isset($sla_triage) ? round($sla_triage, 2) : '-'; ?></td>
                                            <td><?php echo isset($sla_triage_again) ? round($sla_triage_again, 2) : '-'; ?></td>
                                            <td><?php echo isset($sla_total) ? round($sla_total, 2) : '-'; */?></td>
                                        </tr>
                <?php
                $i++;
                $total++;
            }
        }
    } elseif ($reportSubSubtype == "issue type") {
        //print_r($tickets);die;
        foreach ($tickets as $key => $value) {
            $total++;
            foreach ($value as $topic_id => $val) {
                unset($triage_handled_total);
                unset($sla_total);
                unset($sla_triage);
                unset($sla_triage_again);
                $issueMap = getIssuesRelatedInfoFromTopicId($staticFields, $topic_id)
                // $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                // $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                ?>
                                        <tr>
                                            <td><?php echo $key; ?></td>
                                            <td><?php echo $issueMap['issueType']; ?></td>
                                            <td><?php echo $issueMap['issueSubType']; ?></td>
                                            <td><?php echo $issueMap['issueSubSubType']; ?></td>
                                            <td><?php echo $val['triage']['ticket_count']? : '-'; ?></td>
                                            <td><?php echo $val['triage_again']['ticket_count']? : '-'; ?></td>
                                        <?php
                                        if (isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count'])) {
                                            $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                                            $sla_total = (($val['triage']['wsla']? : 0) + ($val['triage_again']['wsla']? : 0)) * 100 / (($val['triage']['ticket_count_overall']? : 0) + ($val['triage_again']['ticket_count_overall']? : 0)-($val['triage']['open_wsla']+$val['triage_again']['open_wsla']));
                                        }
                                        if (isset($val['triage']['ticket_count'])) {
                                            $sla_triage = ($val['triage']['wsla']? : 0) * 100 / ($val['triage']['ticket_count_overall']-$val['triage']['open_wsla']);
                                        }
                                        if (isset($val['triage_again']['ticket_count'])) {
                                            $sla_triage_again = ($val['triage_again']['wsla']? : 0) * 100 / ($val['triage_again']['ticket_count_overall']-$val['triage_again']['open_wsla']);
                                        }
                                        ?>
                                            <td><?php echo isset($triage_handled_total) ? $triage_handled_total : '-'; ?></td>
                                            <td><?php echo isset($sla_triage) ? round($sla_triage) : '-'; ?></td>
                                            <td><?php echo isset($sla_triage_again) ? round($sla_triage_again) : '-'; ?></td>
                                            <td><?php echo isset($sla_total) ? round($sla_total) : '-'; ?></td>

                                        </tr>
                                            <?php
                                        }
                                    }
                                }
                            } elseif ($flag == "t_aht") {
                                if ($reportSubSubtype == "agent level") {
                                    foreach ($tickets as $key => $value) {
                                        $total++;
                                        foreach ($value as $k => $val) {
                                            unset($triage_handled_total);
                                            ?>    
                                        <tr>
                                            <td><?php echo $key; ?></td>
                                            <td><?php echo $staticFields['staff'][$k]? : '-'; ?></td>
                                            <td><?php echo $val['email']? : '-'; ?></td>
                                            <td><?php echo $staticFields['staff'][$val['manager']]? : '-'; ?></td>
                                            <td><?php echo $val['triage_handled']? : '-'; ?></td>
                                            <td><?php echo $val['triage_again_handled']? : '-'; ?></td>
                                        <?php
                                        if (isset($val['triage_handled']) || isset($val['triage_again_handled'])) {
                                            $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                                        }
                                        ?>
                                            <td><?php echo isset($triage_handled_total) ? $triage_handled_total : '-'; ?></td>
                                            <td><?php echo round($val['triage_handled_aht'])? : '-'; ?></td>
                                            <td><?php echo round($val['triage_again_handled_aht'])? : '-'; ?></td>
                                        <?php $aht_total = ($val['triage']['timediff'] + $val['triage_again']['timediff']) / ($val['triage']['no_for_aht'] + $val['triage_again']['no_for_aht']); ?>
                                            <td><?php echo isset($aht_total) ? round($aht_total) : '-'; ?></td>
                                        </tr>
                <?php
            }
        }
    } elseif ($reportSubSubtype == "issue type") {
        foreach ($tickets as $key => $value) {
            $total++;
            foreach ($value as $topic_id => $val) {
                unset($triage_handled_total);
                unset($sla_total);
                unset($sla_triage);
                unset($sla_triage_again);
                $issueMap = getIssuesRelatedInfoFromTopicId($staticFields, $topic_id)
                // $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                // $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                ?>
                                        <tr>
                                            <td><?php echo $key; ?></td>
                                            <td><?php echo $issueMap['issueType']; ?></td>
                                            <td><?php echo $issueMap['issueSubType']; ?></td>
                                            <td><?php echo $issueMap['issueSubSubType']; ?></td>
                                            <td><?php echo $val['triage']['ticket_count']? : '-'; ?></td>
                                            <td><?php echo $val['triage_again']['ticket_count']? : '-'; ?></td>
                <?php
                if (isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count'])) {
                    $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                }
                ?>
                                            <td><?php echo isset($triage_handled_total) ? $triage_handled_total : '-'; ?></td>
                                            <td><?php echo round($val['triage']['aht'])? : '-'; ?></td>
                                            <td><?php echo round($val['triage_again']['aht'])? : '-'; ?></td>
                                        <?php $aht_total = ($val['triage']['timediff'] + $val['triage_again']['timediff']) / ($val['triage']['no_for_aht'] + $val['triage_again']['no_for_aht']); ?>
                                            <td><?php echo isset($aht_total) ? round($aht_total) : '-'; ?></td>

                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        } else if ($flag == "t_pend") {
                            foreach ($tickets as $key => $value) {
                                $total++;
                                ?>
                                <tr>
                                    <td><?php echo $key; ?></td>
                                    <td><?php echo $value['triage_total']? : '-'; ?></td>
                                    <td><?php echo $value['triage_again_total']? : '-'; ?></td>
                                    <td><?php echo $value['old_ticket_triage'] ?></td>
                                    <td><?php echo $value['old_ticket_triage_again'] ?></td>
                                    <td><?php echo $value['triage_handled']? : '-'; ?></td>
                                    <td><?php echo $value['triage_again_handled']? : '-'; ?></td>
                                    <td><?php echo $value['triage_pending']? : '-'; ?></td>
                                    <td><?php echo $value['triage_again_pending']? : '-'; ?></td>
                                </tr>
                                    <?php
                            }
                        } elseif ($flag == "t_sla_summary") {
                            foreach ($tickets as $key => $value) {
                                $total++;
                                ?>
                                <tr>
                                    <td><?php echo $key; ?></td>
                                    <td><?php echo $value['triage_total_overall']? : '-'; ?></td>
                                    <td><?php echo $value['triage_again_total_overall']? : '-'; ?></td>
                                    <td><?php echo $value['triage_handled_wsla'] ?></td>
                                    <td><?php echo $value['triage_again_handled_wsla'] ?></td>
                                    <td><?php echo $value['triage_open_wsla']? : '-'; ?></td>
                                    <td><?php echo $value['triage_again_open_wsla']? : '-'; ?></td>
                                    <?php
                                        $sla_triage =  $value['triage_handled_wsla']*100/($value['triage_total_overall']-$value['triage_open_wsla']);
                                        // if($sla_triage>100)
                                        //     $sla_triage = '';
                                        $sla_triage_again = $value['triage_again_handled_wsla']*100/($value['triage_again_total_overall']-$value['triage_again_open_wsla']);
                                        // if($sla_triage_again>100)
                                        //     $sla_triage_again = '';
                                        $sla_total = ($value['triage_handled_wsla']+$value['triage_again_handled_wsla'])*100/($value['triage_total_overall']+$value['triage_again_total_overall']-$value['triage_open_wsla']-$value['triage_again_open_wsla']);
                                        // if($sla_total>100)
                                        //     $sla_total = '';
                                    ?>
                                    <td><?php echo round($sla_triage)? : '-'; ?></td>
                                    <td><?php echo round($sla_triage_again)? : '-'; ?></td>
                                    <td><?php echo round($sla_total)? : '-'; ?></td>
                                </tr>
                                    <?php
                            }
                        } else if ($flag == "t_all") {
                            $total=1;
                            //$total = $tickets->count();
                        } elseif ($flag == "ccg_sla") {
                            foreach ($tickets as $date => $value) {
                                foreach ($value as $staff => $val) {
                                    $total++;
                                    ?>
                                <tr>
                                    <td><?php echo $date; ?></td>
                                    <td><?php echo $staticFields['staff'][$staff]; ?></td>
                                    <td><?php echo $val['email']; ?></td>
                                    <td><?php echo $staticFields['staff'][$val['manager']]; ?></td>
                                    <td><?php echo $val['total_tasks']; ?></td>
                                    <td><?php echo $val['task_solved_sla']; ?></td>
                                    <td><?php echo $val['task_solved_osla']; ?></td>
                                    <td><?php echo $val['task_open_sla']; ?></td>
                                    <td><?php echo $val['task_open_osla']; ?></td>
                                    <?php
                                        $sla_adherence = ($val['task_solved_sla']*100/($val['total_tasks_overall']-$val['task_open_sla']));
                                    ?>
                                    <td><?php echo round($sla_adherence); ?></td>
                                </tr>
                                <?php
                                }
                            }
                        } elseif($flag == "ccg_all"){
                            $total = 1;
                        } elseif ($flag == "ccg_pend") {
                            $total=0;
                            foreach ($tickets as $date => $value) {
                                foreach ($value as $staff => $val) {
                                    $total++;
                                    ?>
                                    <tr>
                                        <td><?php echo $date; ?></td>
                                        <td><?php echo $staticFields['staff'][$staff]; ?></td>
                                        <td><?php echo $val['email']; ?></td>
                                        <td><?php echo $staticFields['staff'][$val['manager']]; ?></td>
                                        <td><?php echo $val['email_task_created']; ?></td>
                                        <td><?php echo $val['call_task_created']; ?></td>
                                        <td><?php echo $val['email_task_created']+$val['call_task_created']; ?></td>
                                        <td><?php echo $val['email_task_handled']; ?></td>
                                        <td><?php echo $val['call_task_handled']; ?></td>
                                        <td><?php echo $val['email_task_handled']+$val['call_task_handled']; ?></td>
                                        <td><?php echo $val['email_task_pending']; ?></td>
                                        <td><?php echo $val['call_task_pending']; ?></td>
                                        <td><?php echo $val['email_task_pending']+$val['call_task_pending']; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        } elseif ($flag=="ccg_prod") {
                            $total=0;
                            foreach ($tickets as $date => $value) {
                                foreach ($value as $staff => $val) {
                                    $total++;
                                    ?>
                                    <tr>
                                        <td><?php echo $date ?></td>
                                        <td><?php echo $staticFields['staff'][$staff]; ?></td>
                                        <td><?php echo $val['email']; ?></td>
                                        <td><?php echo $staticFields['staff'][$val['manager']]; ?></td>
                                        <td><?php echo $val['email_task_handled']; ?></td>
                                        <td><?php echo $val['call_task_handled']; ?></td>
                                        <td><?php ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        } elseif ($flag == "crg_all") {
                            $total=1;
                        } elseif ($flag == "crg_sla") {
                            foreach ($tickets as $date => $value) {
                                foreach ($value as $staff => $val) {
                                    $total++;
                                    ?>
                                <tr>
                                    <td><?php echo $date; ?></td>
                                    <td><?php echo $staticFields['staff'][$staff]; ?></td>
                                    <td><?php echo $val['email']; ?></td>
                                    <td><?php echo $staticFields['staff'][$val['manager']]; ?></td>
                                    <td><?php echo $val['total_tasks']; ?></td>
                                    <td><?php echo $val['task_solved_sla']; ?></td>
                                    <td><?php echo $val['task_solved_osla']; ?></td>
                                    <td><?php echo $val['task_open_sla']; ?></td>
                                    <td><?php echo $val['task_open_osla']; ?></td>
                                    <td><?php echo '-'; ?></td>
                                </tr>
                                <?php
                                }
                            }
                        } elseif ($flag == "crg_aht") {
                            foreach ($tickets as $date => $value) {
                                foreach ($value as $staff => $val) {
                                    $total++;
                                    ?>
                                <tr>
                                    <td><?php echo $date; ?></td>
                                    <td><?php echo $staticFields['staff'][$staff]; ?></td>
                                    <td><?php echo $val['email']; ?></td>
                                    <td><?php echo $staticFields['staff'][$val['manager']]; ?></td>
                                    <td><?php echo $val['task_handled']; ?></td>
                                    <td><?php echo $val['aht']; ?></td>
                                </tr>
                                <?php
                                }
                            }
                        }elseif($flag=="ops_all"){
                            
                                $total=1;
                            }
                            elseif($flag=="ops_dept"){
                                
                                include REPORT_VIEWS_PATH.'tableTemplate.phtml';
                                
                            }elseif($flag=="ops_task"){
                                
                                  include REPORT_VIEWS_PATH.'tableTemplate.phtml';
                                  
                            }elseif($flag=="business_issue"){
                                
                                  include REPORT_VIEWS_PATH.'tableTemplate.phtml';
                                  
                            }else if($flag=="business_ticket"){
                                  include REPORT_VIEWS_PATH.'tableTemplate.phtml';
                                  
                            }else if($flag=="business_prod"){
                                
                                  include REPORT_VIEWS_PATH.'tableTemplate.phtml';
                                  
                            }

                                    ?>
                                    <?php
                                    if ($viewAccessDept == '0' || $viewAccessTeam == '0') {
//echo '<tr><td colspan="7">'.__('There are no tickets matching your criteria.!').'</td></tr>'; //-- commented by akash Kumar for javascript loading issue
                                    }
                                    // Setup Subject field for display
                                    $class = "row1";
                                    $ids = ($errors && $_POST['tids'] && is_array($_POST['tids'])) ? $_POST['tids'] : null;

                                    $i = 0;
                                    if ($customFieldsToDisplay)
                                        $allCustomDataArr = getTicketCustomFields($tidArr);
                                    if (!$total)
                                        $ferror = __('There are no tickets matching your criteria.');
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                        <?php 
                                        if (1) {
                                        } else {
                                            echo '<i>';
                                            echo $ferror ? Format::htmlchars($ferror) : __('Query returned 0 results.');
                                            echo '</i>';
                                        }
                                        ?>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                                    <?php
                                    
                                    if ($total > 0 && $flag!="t_all" && $flag!="ops_all" && $flag!="tck_all"  && $flag!="ccg_all"  && $flag!="crg_all") { //if we actually had any tickets returned.
                                        echo '<div class="paging">&nbsp;' . stripos($flag, 'all')===false? __('Page').':':'' . '&nbsp;';
                                        if(stripos($flag, 'all'))
                                        {
                                            ?>
                                            <style>
                                                .export_bttm{
                                                    float:none;
                                                    text-align: center;
                                                }
                                                .export_bttm a:after{
                                                    content: " dump";
                                                }
                                            </style>
                                            <?php
                                        }

                                        echo '<div class="export_bttm"><a class="export-csv no-pjax ';
                                        if(stripos($flag, "all"))
                                            echo 'buttons';
                                        echo '" href="' . $appendInUrl . Http::build_query(array(
                                            'EXPORT' => 'exportTrue')) . '">Export</a>';

                                        echo '&nbsp;<i class="help-tip icon-question-sign" href="#export"></i></div></div>';
                                    }
                                    ?>
                        </form>  
                    </div>  
                </div>
            </div>
<script type="text/css">
span.select2.select2-container.select2-container--default {
    width: 190px;
}
</script>
            <script type="text/javascript">
                $('.datetimepicker').datetimepicker({
                    formatDate: 'd/m/Y H:i'});
                $('.filters').on('click',function(){
                 var element = $(this);
                       $(".filterdata").hide();
                 $("#"+element.attr('filter-data')).show();
                });
                var filters = [];
                <?php
                foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['filters'] as $value => $filterdata) {
                    ?>
                    $("#<?php echo implode('_', explode(' ', $value)); ?>").on("change",function(){
                        var filterArr = filters;
                        filters=[];
                        $("tr").show();
                        $("tr").attr("data","1");
                        for(var i=0;i<filterArr.length && filterArr[i]!="";i++)
                        {
                            
                            ele = $("#"+filterArr[i]);
                            var valu = $(this).val();
                            var ele = $( "."+filterArr[i]);
                            ele.each(function(){
                                if(($(this).text()  == valu || valu=="all") && $(this).parent().attr("data")=="1"){
                                    $(this).parent().show();
                                    //$(this).attr("data","1");
                                }
                                else{
                                    $(this).parent().hide();
                                    $(this).parent().attr("data","0");
                                }
                            });
                            //filters = filters.concat(filterArr[i]);
                            if(filters.indexOf(filterArr[i])==-1)
                            filters.push(filterArr[i]);
                        }
                        //console.log("hello");
                        var valu = $(this).val();
                        var ele = $( ".<?php echo implode('_', explode(' ', $value)); ?>" );
                            ele.each(function(){

                                if(($(this).text()  == valu || valu=="all") && $(this).parent().attr("data")=="1")
                                $(this).parent().show();
                            else{
                                $(this).parent().hide();
                                $(this).parent().attr("data","0");
                            }
                            });
                            if(filters.indexOf("<?php echo implode('_', explode(' ', $value)); ?>")==-1)
                            filters.push("<?php echo implode('_', explode(' ', $value)); ?>");
                        
                        //console.log(ele);
                    });
                    <?php
                }
                ?>

            </script>