<?php
//die("hgrg");
function staff_data(){
    $sql = "SELECT 
                `staff_id`, `email`, `manager_id`
            FROM
                mst_staff
                    LEFT JOIN
                mst_staff_reporting ON `staff_id` = `agent_id`";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $staffArray[$result['staff_id']]['email'] = $result['email'];
        $staffArray[$result['staff_id']]['manager'] = $result['manager_id'];
    }
}
function triage_dump($tickets){
    $field['date range'][0] = $_GET['from'] ? date('Y-m-d H:i:s', strtotime($_GET['from'])) : '0000-00-00 00:00:00';
                $field['date range'][1] = $_GET['to'] ? date('Y-m-d H:i:s', strtotime($_GET['to'])) : date("Y-m-d H:i:s", time());
                $staffArray = staff_data();
                $tickets->filter(
                        array('created__gt' => $field["date range"][0],
                            'created__lt' => $field["date range"][1]
                ))->order_by("-created")->values(
                        "number", "source_extra", 'status__name', "odata__ticket_channel", "odata__priority", "created", "odata__order_id", "staff_id", "triage__triage", "triage__sla", "triage__triage_time", "triage__triage_again", "staff__email", "staff__firstname", "staff__lastname", "status__name", "isoverdue", "closed", "topic_id");
}
function triage_sla_agent_level($tickets){
    global $cfg;
    $staffArray = staff_data();
    $TimeToTriage = $cfg->config['TimeToTriage']["value"];
    $sql = "SELECT count(*) AS ticket_count,DATE(A.`created`) as created_date,DATE(B.`created`) as thread_date,B.`type` as type,B.`title` as title,B.`body` as body,B.`staff_id` as staff_id,sum(TIMESTAMPDIFF(SECOND, A.`created`,B.`created`)<" . $TimeToTriage . ") as within FROM mst_ticket A left join mst_thread C on A.`ticket_id`=C.`object_id` left join mst_thread_entry B on C.`id`=B.`thread_id` where B.`type`='N' and ((B.`title`='Status Changed' and `body` like '%New to Active on Triaged%') or B.`title`='Triaged') " . $filterString . " group by DATE(B.`created`),B.`staff_id`";

    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {

        if ($result['staff_id']) {
            $tickets[$result['thread_date']][$result['staff_id']]['triage_handled'] = $result['ticket_count'];
            $tickets[$result['thread_date']][$result['staff_id']]["triage_handled_wsla"] = $result['within'];
            $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
            $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
        }
    }

    $sql = "SELECT count(*) as ticket_count,DATE(A.`created`) as created_date,DATE(B.`created`) as thread_date,B.`type` as type,B.`title` as title,B.`body` as body,B.`staff_id` as staff_id,sum(TIMESTAMPDIFF(SECOND, A.`created`,`B.created`)<" . $TimeToTriage . ") as within,`poster` as staff_name FROM mst_ticket A INNER JOIN mst_thread C on A.`ticket_id`=C.`object_id` INNER JOIN mst_thread_entry B on C.`id`=B.`thread_id` where B.`type`='N' and (B.`title`='Triage field chage' or B.`title`='Triage field change') " . $filterString . " GROUP BY DATE(B.`created`),B.`staff_id`,`poster`";
    $res = db_query($sql);

    while ($result = db_fetch_array($res)) {

        if ($result['staff_id']) {
            if ($result['title'] == 'Triage field chage' || $result['title'] == 'Triage field change') {
                $tickets[$result['thread_date']][$result['staff_id']]['triage_again_handled'] += $result['ticket_count'];
                $tickets[$result['thread_date']][$result['staff_id']]["triage_again_handled_wsla"] += $result['within'];
                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
            }
        } else {

            $staff_id = array_search($result['staff_name'], $staticFields['staff']);
            $tickets[$result['thread_date']][$staff_id]['triage_again_handled'] += $result['ticket_count'];
            $tickets[$result['thread_date']][$staff_id]["triage_again_handled_wsla"] += $result['within'];
            $tickets[$result['thread_date']][$staff_id]['email'] = $staffArray[$staff_id]['email'];
            $tickets[$result['thread_date']][$staff_id]['manager'] = $staffArray[$staff_id]['manager'];
        }
    }
}
function triage_sla_issue_type($tickets){
    global $cfg;
    $TimeToTriage = $cfg->config['TimeToTriage']["value"];
    $sql = "select count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<" . $TimeToTriage . ") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread C on A.ticket_id=C.object_id inner join mst_thread_entry B on C.id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') " . $filterString . " group by DATE(B.created),topic_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['thread_date']][$result['topic_id']]['triage']['ticket_count'] = $result['ticket_count'];
        $tickets[$result['thread_date']][$result['topic_id']]['triage']['wsla'] = $result['within'];
    }
    $sql = "select count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<" . $TimeToTriage . ") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread C on A.ticket_id=C.object_id inner join mst_thread_entry B on C.id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') " . $filterString . " group by DATE(B.created),topic_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['ticket_count'] = $result['ticket_count'];
        $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['wsla'] = $result['within'];
    }
}
function triage_aht_agent_level($tickets){
    global $cfg;
    $staffArray = staff_data();
    $TimeToTriage = $cfg->config['TimeToTriage']["value"];
    $sql = "select group_concat(A.ticket_id) as ids,count(*) as ticket_count,DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body,B.staff_id as staff_id,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<" . $TimeToTriage . ") as within from mst_ticket A left join mst_thread C on A.ticket_id=C.object_id left join mst_thread_entry B on C.id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') " . $filterString . " group by DATE(B.created),B.staff_id";
    $res = db_query($sql);
    $count = 0;
    $ids = '';
    while ($result = db_fetch_array($res)) {
        if ($ids != '')
            $ids = $ids . ',' . $result['ids'];
        else
            $ids = $result['ids'];
        $count++;
        //print_r($result);
        if ($result['staff_id']) {
            $tickets[$result['thread_date']][$result['staff_id']]['triage_handled'] = $result['ticket_count'];
            $tickets[$result['thread_date']][$result['staff_id']]["triage_handled_wsla"] = $result['within'];
            $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
            $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
        }
    }
    if ($filterString) {
        $newString = $filterString;
        $newString = str_replace('B.created', 'last_timestamp', $newString);
    }
    $sql = "select count(*) as ticket_count,DATE(new_timestamp) as date,staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff from mst_tracking where event_name like '%Triaging Ticket%' " . $newString . " group by DATE(new_timestamp),staff_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['triage']['timediff'] = $result['timediff'];
        $tickets[$result['date']][$result['staff_id']]['triage']['no_for_aht'] = $result['ticket_count'];
        $tickets[$result['date']][$result['staff_id']]['triage_handled_aht'] = $result['timediff'] / $result['ticket_count'];
    }
    $sql = "select group_concat(A.ticket_id) as ids,count(*) as ticket_count,DATE(A.created) as created_date,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body,B.staff_id as staff_id,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<" . $TimeToTriage . ") as within,poster as staff_name from mst_ticket A inner join mst_thread C on A.ticket_id=C.object_id inner join mst_thread_entry B on C.id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') " . $filterString . " group by DATE(B.created),B.staff_id,poster";
    $res = db_query($sql);
    $ids = '';
    while ($result = db_fetch_array($res)) {
        if ($ids != '')
            $ids = $ids . ',' . $result['ids'];
        else
            $ids = $result['ids'];
        if ($result['staff_id']) {
            if ($result['title'] == 'Triage field chage' || $result['title'] == 'Triage field change') {
                $tickets[$result['thread_date']][$result['staff_id']]['triage_again_handled'] += $result['ticket_count'];
                $tickets[$result['thread_date']][$result['staff_id']]["triage_again_handled_wsla"] += $result['within'];
                $tickets[$result['thread_date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
                $tickets[$result['thread_date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
            }
        } else {
            $staff_id = array_search($result['staff_name'], $staticFields['staff']);
            $tickets[$result['thread_date']][$staff_id]['triage_again_handled'] += $result['ticket_count'];
            $tickets[$result['thread_date']][$staff_id]["triage_again_handled_wsla"] += $result['within'];
            $tickets[$result['thread_date']][$staff_id]['email'] = $staffArray[$staff_id]['email'];
            $tickets[$result['thread_date']][$staff_id]['manager'] = $staffArray[$staff_id]['manager'];
        }
    }
    $sql = "select count(*) as ticket_count,DATE(new_timestamp) as date,staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff from mst_tracking where event_name like '%Triaging Ticket%' " . $newString . " group by DATE(new_timestamp),staff_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['triage_again']['timediff'] = $result['timediff'];
        $tickets[$result['date']][$result['staff_id']]['triage_again']['no_for_aht'] = $result['ticket_count'];
        $tickets[$result['date']][$result['staff_id']]['triage_again_handled_aht'] = $result['timediff'] / $result['ticket_count'];
    }
}

function triage_aht_issue_type($tickets){
    global $cfg;
    
    $TimeToTriage = $cfg->config['TimeToTriage']["value"];
    $sql = "select group_concat(A.ticket_id) as ids,count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<" . $TimeToTriage . ") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread C on A.ticket_id=C.id inner join mst_thread_entry B on C.id=B.thread_id where B.type='N' and ((B.title='Status Changed' and body like '%New to Active on Triaged%') or B.title='Triaged') " . $filterString . " group by DATE(B.created),topic_id order by DATE(B.created) desc";
    $res = db_query($sql);
    $ids = '';
    while ($result = db_fetch_array($res)) {
        if ($ids != '')
            $ids = $ids . ',' . $result['ids'];
        else
            $ids = $result['ids'];
        $tickets[$result['thread_date']][$result['topic_id']]['triage']['ticket_count'] = $result['ticket_count'];
        $tickets[$result['thread_date']][$result['topic_id']]['triage']['wsla'] = $result['within'];
    }
    if ($filterString) {
        $newString = $filterString;
        $newString = str_replace('B.created', 'last_timestamp', $newString);
    }
    
    $sql = "select count(*) as ticket_count,DATE(new_timestamp) as date,B.staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff,topic_id from mst_ticket A inner join mst_tracking B on A.ticket_id=B.ticket_id where event_name like '%Triaging Ticket%' " . $newString . " group by DATE(new_timestamp),topic_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['topic_id']]['triage']['timediff'] = $result['timediff'];
        $tickets[$result['date']][$result['topic_id']]['triage']['no_for_aht'] = $result['ticket_count'];
        $tickets[$result['date']][$result['topic_id']]['triage']['aht'] = $result['timediff'] / $result['ticket_count'];
    }

    $sql = "select group_concat(A.ticket_id) as ids,count(*) as ticket_count,sum(TIMESTAMPDIFF(SECOND, A.created,B.created)<" . $TimeToTriage . ") as within,A.topic_id as topic_id,DATE(B.created) as thread_date from mst_ticket A inner join mst_thread C on A.ticket_id=C.id inner join mst_thread_entry B on C.id=B.thread_id where B.type='N' and (B.title='Triage field chage' or B.title='Triage field change') " . $filterString . " group by DATE(B.created),topic_id";
    $res = db_query($sql);
    $ids = '';
    while ($result = db_fetch_array($res)) {
        if ($ids != '')
            $ids = $ids . ',' . $result['ids'];
        else
            $ids = $result['ids'];
        $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['ticket_count'] = $result['ticket_count'];
        $tickets[$result['thread_date']][$result['topic_id']]['triage_again']['wsla'] = $result['within'];
    }
    $sql = "select count(*) as ticket_count,DATE(new_timestamp) as date,B.staff_id,sum(TIMESTAMPDIFF(SECOND, last_timestamp,new_timestamp)) as timediff,topic_id from mst_ticket A inner join mst_tracking B on A.ticket_id=B.ticket_id where event_name like '%Triaging Ticket%' " . $newString . " group by DATE(new_timestamp),topic_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['topic_id']]['triage_again']['timediff'] = $result['timediff'];
        $tickets[$result['date']][$result['topic_id']]['triage_again']['no_for_aht'] = $result['ticket_count'];
        $tickets[$result['date']][$result['topic_id']]['triage_again']['aht'] = $result['timediff'] / $result['ticket_count'];
    }
}
function triage_pendency($tickets){
    
    global $filterString;
    $pos = strpos($filterString, 'and ');
    if ($pos !== false) {
        $newString = substr_replace($filterString, 'where ', $pos, strlen('and '));
    }
    $sql = "select DATE(created) as date,count(*) as ticket_count from mst_ticket B " . $newString . " group by DATE(created)";
    $res = db_query($sql);
    $count = 0;
    $total_count = 0;
    while ($result = db_fetch_array($res)) {
        $count++;
        $tickets[$result['date']]['triage_total'] = $result['ticket_count'];
        $tickets[$result['date']]['triage_total_overall'] = $total_count + $result['ticket_count'];
        $total_count = $tickets[$result['date']]['triage_total_overall'];
    }
    $sql = "select DATE(A.created) as created_date,DATE(A.closed) as closed,DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body from mst_ticket A left join mst_thread C on A.ticket_id=C.object_id left join mst_thread_entry B on C.id=B.thread_id where DATE(A.created)=DATE(B.created) and type='N' " . $filterString;
    //echo $sql;die;
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        if (($result['title'] == 'Status Changed' && strpos($result['body'], 'New to Active on Triaged') !== false) || $result['title'] == 'Triaged' || ($result['closed']==$result['thread_date'] && $result['closed']!=NULL && $result['closed']!='0000-00-00') ) {
            $tickets[$result['thread_date']]['triage_handled'] ++;
        }
    }
    $sql = "select DATE(B.created) as thread_date,B.type as type,B.title as title,B.body as body from mst_thread_entry B where type='N' " . $filterString;
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        if (strpos($result['title'], 'Triage again') !== false) {
            $tickets[$result['thread_date']]['triage_again_total'] ++;
        }
    }
    $sql = "select count(distinct B.thread_id) as thread_count,DATE(A.created) as thread_date,A.title as title1,B.title as title2  from mst_thread_entry A inner join mst_thread_entry B on A.thread_id=B.thread_id where DATE(A.created)=DATE(B.created) and A.id!=B.id and A.type=B.type and A.type='N' " . $filterString . " group by DATE(A.created),A.title,B.title";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        if ($result['title1'] == 'Triage again' && ($result['title2'] == 'Triage field chage' || $result['title2'] == 'Triage field change'))
            $tickets[$result['thread_date']]['triage_again_handled'] += $result['thread_count'];
    }
    // $closedstatus = implode(',', $staticFields['state']['closed']);
    // $sql = "select (CASE WHEN ((A.closed is not null and DATE(A.closed)!='0000-00-00') and A.closed<B.created) THEN DATE(A.closed) ELSE DATE(B.created) END) as date,count(distinct A.ticket_id) as ticket_count from mst_ticket A left join mst_thread C on A.ticket_id=C.object_id left join mst_thread_entry B on C.id=B.thread_id where ((title='Status Changed' and body like '%New to Active on Triaged%') or title='Triaged') or status_id IN (".$closedstatus.") group by (CASE WHEN ((A.closed is not null and DATE(A.closed)!='0000-00-00') and A.closed<B.created) THEN DATE(A.closed) ELSE DATE(B.created) END)";
    // $res = db_query($sql);
    // $triage_handled_count=0;
    // while ($result = db_fetch_array($res)) {
    //     $tickets[$result['date']]['triage_handled_overall'] = $triage_handled_count+$result['ticket_count'];
    //     $triage_handled_count=$tickets[$result['date']]['triage_handled_overall'];
    // }
    // $sql = "select DATE(created) as date,count(distinct thread_id) as ticket_count from mst_thread_entry where title='Triage field chage' or title='Triage field change' group by DATE(created)";
    // $res = db_query($sql);
    // $triage_again_handled_count = 0;
    // while ($result = db_fetch_array($res)) {
    //     $tickets[$result['date']]['triage_again_handled_overall'] = $triage_again_handled_count + $result['ticket_count'];
    //     $triage_again_handled_count = $tickets[$result['date']]['triage_again_handled_overall'];
    // }
    // $triage_again_total_count = 0;
    // $closed_ticket_count = 0;
    // foreach ($tickets as $date => $value) {
    //     // $tickets[$date]['closed_tickets_overall'] = $closed_ticket_count + $tickets[$date]['closed_ticket'];
    //     // $closed_ticket_count = $tickets[$date]['closed_tickets_overall'];
    //     $tickets[$date]['triage_again_total_overall'] = $triage_again_total_count + $tickets[$date]['triage_again_total'];
    //     $triage_again_total_count = $tickets[$date]['triage_again_total_overall'];
        
    //     $tickets[$date]['triage_pending'] = $tickets[$date]['triage_total_overall'] - $tickets[$date]['triage_handled_overall'] ;
    //     $tickets[$date]['triage_again_pending'] = $tickets[$date]['triage_again_total_overall'] - $tickets[$date]['triage_again_handled_overall'];
    // }
}
function ccg_all($tickets){
    $field['date range'][0] = $_GET['from'] ? date('Y-m-d H:i:s', strtotime($_GET['from'])) : '0000-00-00 00:00:00';
    $field['date range'][1] = $_GET['to'] ? date('Y-m-d H:i:s', strtotime($_GET['to'])) : date("Y-m-d H:i:s", time());
    $tickets = array();
    $sql = "select (CASE WHEN C.closed is not null THEN TIMESTAMPDIFF(SECOND, C.created,C.closed) ELSE 0) as timediff, A.number as ticket_num, source_extra, channel, A.created as created, order_no, D.title as task_name, C.id as task_id, topic_id, A.staff_id as staff_id, C.created as task_creation, C.closed as task_closure, (CASE WHEN C.closed is not null and DATE(C.closed)!='0000-00-00' THEN C.closed<C.duedate ELSE NOW()<C.duedate END) as sla, C.updated as task_updated from mst_ticket A inner join mst_ticket__odata B on A.ticket_id=B.ticket_id inner join mst_task C on A.ticket_id=C.object_id inner join mst_task__data D on C.id=D.task_id where C.dept_id=1";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        ccg_dump(&$tickets[$result['ticket_num']][$result['task_id']]);
    }
}
function ccg_sla($tickets){
    $sql = "select DATE(B.created) as date,count(*) as task_count,A.staff_id as staff_id,B.flags as flag,sum(B.duedate>(CASE WHEN B.closed is not null THEN B.closed ELSE NOW() END)) as within from mst_ticket A inner join mst_task B on A.ticket_id=B.object_id where B.dept_id=1 group by DATE(B.created),A.staff_id,B.flags";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        if ($result['flag'] == 0) {
            $tickets[$result['date']][$result['staff_id']]['task_solved_sla'] = $result['within'];
            $tickets[$result['date']][$result['staff_id']]['task_solved_osla'] = $result['task_count'] - $result['within'];
        } else {
            $tickets[$result['date']][$result['staff_id']]['task_open_sla'] = $result['within'];
            $tickets[$result['date']][$result['staff_id']]['task_open_osla'] = $result['task_count'] - $result['within'];
        }
        $tickets[$result['date']][$result['staff_id']]['total_tasks'] += $result['task_count'];
        $tickets[$result['date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
        $tickets[$result['date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
    }
}
function ccg_prod($tickets){
    global $cfg;
    global $staffArray;
    $sql = "select DATE(B.created) as date,count(*) as ticket_count,B.staff_id as staff_id from mst_ticket A inner join mst_thread C on A.ticket_id=C.object_id inner join mst_thread_entry B on C.id=B.thread_id where type='R' group by DATE(B.created),B.staff_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['email_task_handled'] = $result['ticket_count'];
        $tickets[$result['date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
        $tickets[$result['date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
    }
}
function ccg_pendency($tickets){
    $tickets = array();
    $sql = "select DATE(B.created) as date,count(*) as ticket_count,B.staff_id as staff_id from mst_thread_entry B where body like '%Communication%' and B.staff_id!=0 group by DATE(B.created),B.staff_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['email_task_created'] = $result['ticket_count'];
    }
    $sql = "select DATE(B.created) as date,count(*) as ticket_count,B.staff_id as staff_id from mst_thread_entry B where body like '%Call customer</b> Yes%' and B.staff_id!=0 group by DATE(B.created),B.staff_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['call_task_created'] = $result['ticket_count'];
    }
    $sql = "select DATE(B.created) as date,count(*) as ticket_count,B.staff_id as staff_id from mst_thread_entry B inner join mst_thread_entry A on A.thread_id=B.thread_id where B.type='R' and B.staff_id!=0 and DATE(A.created)=DATE(B.created) and A.body like '%Communication%' and A.staff_id!=0 group by DATE(B.created),B.staff_id";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['email_task_handled'] = $result['ticket_count'];
    }
    foreach ($tickets as $date => $value) {
        foreach ($value as $staff => $val) {
            $tickets[$date][$staff]['email_task_pending'] = $tickets[$date][$staff]['email_task_created'] - $tickets[$date][$staff]['email_task_handled'];
            $tickets[$date][$staff]['call_task_pending'] = $tickets[$date][$staff]['call_task_created'] - $tickets[$date][$staff]['call_task_handled'];
            $tickets[$date][$staff]['email'] = $staffArray[$staff]['email'];
            $tickets[$date][$staff]['manager'] = $staffArray[$staff]['manager'];
        }
    }
}
function crg_all($tickets){
    $field['date range'][0] = $_GET['from'] ? date('Y-m-d H:i:s', strtotime($_GET['from'])) : '0000-00-00 00:00:00';
    $field['date range'][1] = $_GET['to'] ? date('Y-m-d H:i:s', strtotime($_GET['to'])) : date("Y-m-d H:i:s", time());
    $tickets = array();
    $sql = "SELECT 
                (CASE
                    WHEN
                        C.`closed` is not null
                    THEN
                        TIMESTAMPDIFF(SECOND,
                            C.`created`,
                            C.`closed`)
                    ELSE 0
                END) AS AStimediff,
                A.`number` AS ticket_num,
                A.`source_extra` AS source,
                B.`channel` AS ticket_channel,
                A.`created` AS created,
                B.`order_id` AS order_no,
                D.`title` AS task_name,
                C.`id` AS task_id,
                A.`topic_id` AS topic_id,
                A.`staff_id` AS staff_id,
                C.`created` AS task_creation,
                C.`closed` AS task_closure,
                (CASE
                    WHEN
                        C.`closed` is not null
                            AND DATE(C.`closed`) != '0000-00-00'
                    THEN
                        C.`closed` < C.`duedate`
                    ELSE NOW() < C.`duedate`
                END) AS sla,
                C.`updated` AS task_updated,
                B.`retrn_id` AS return_id,
                D.`taskSource` AS task_source,
                D.`taskStatus` AS task_status,
                D.`taskResponse` AS task_response
                FROM
                mst_ticket A
                    INNER JOIN
                mst_ticket__odata B ON A.`ticket_id` = B.`ticket_id`
                    INNER JOIN
                mst_task C ON A.`ticket_id` = C.`object_id`
                    INNER JOIN
                mst_task__data D ON C.`id` = D.`task_id`
                WHERE
                C.`dept_id` = 14";
    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        crg_dump(&$tickets[$result['ticket_num']][$result['task_id']]);
    }
}
function crg_sla($tickets){
    //echo "hello";die;
    global $staffArray;
    $sql = "SELECT
                DATE(B.`created`) AS date,
                count(*) AS task_count,
                A.`staff_id` AS staff_id,
                B.`flags` AS flag,
                sum(B.`duedate` > (CASE
                    WHEN B.`closed` is not null THEN B.`closed`
                    ELSE NOW()
                END)) AS within
            FROM
                mst_ticket A
                    INNER JOIN
                mst_task B ON A.`ticket_id` = B.`object_id`
            where
                B.`dept_id` = 14
            group by DATE(B.`created`) , A.`staff_id` , B.`flags`";

    $res = db_query($sql);print_r($res);die;
    while ($result = db_fetch_array($res)) {
        if ($result['flag'] == 0) {
            $tickets[$result['date']][$result['staff_id']]['task_solved_sla'] = $result['within'];
            $tickets[$result['date']][$result['staff_id']]['task_solved_osla'] = $result['task_count'] - $result['within'];
        } else {
            $tickets[$result['date']][$result['staff_id']]['task_open_sla'] = $result['within'];
            $tickets[$result['date']][$result['staff_id']]['task_open_osla'] = $result['task_count'] - $result['within'];
        }
        $tickets[$result['date']][$result['staff_id']]['total_tasks'] += $result['task_count'];
        $tickets[$result['date']][$result['staff_id']]['email'] = $staffArray[$result['staff_id']]['email'];
        $tickets[$result['date']][$result['staff_id']]['manager'] = $staffArray[$result['staff_id']]['manager'];
    }
}
function crg_aht($tickets){
    $sql = "SELECT 
                DATE(`closed`) AS date,
                A.`staff_id`,
                count(*) AS count,
                sum(TIMESTAMPDIFF(SECOND,
                    `last_timestamp`,
                    `new_timestamp`)) AS timediff
            FROM
                mst_task A
                    LEFT JOIN
                mst_tracking B ON A.`id` = B.`task_id`
            WHERE
                `dept_id` = 14 AND `flags` = 0
            GROUP BY DATE(`created`) , DATE(`closed`) , A.`staff_id`";

    $res = db_query($sql);
    while ($result = db_fetch_array($res)) {
        $tickets[$result['date']][$result['staff_id']]['task_handled'] = $result['count'];

    }
}
function crg_dump($T){
    global $staffArray;
    $T = $result;
    $task_source = explode(',',$result['task_source']);
    $T['task_source'] = $task_source[1];
    $task_response = explode(',',$result['task_response']);
    $T['task_response'] = $task_response[1];
    $task_status = explode(',',$result['task_status']);
    $T['task_status'] = $task_status[1];
    $T['sla'] = $result['within']==1?'SLA':'OSLA';
    $T['email'] = $staffArray[$result['staff_id']]['email'];
    $T['manager'] = $staffArray[$result['staff_id']]['manager'];
    $T['aht'] = ($result['timediff']==0)?'-':round($result['timediff'],2);
}

function ccg_dump($T){
    global $staffArray;
    $T = $result;
    $T['sla'] = $result['within']==1?'SLA':'OSLA';
    $T['email'] = $staffArray[$result['staff_id']]['email'];
    $T['manager'] = $staffArray[$result['staff_id']]['manager'];
    $T['aht'] = $result['timediff']==0?'-':round($result['timediff'],2);
}
switch ($reportType) {
    case 'triage':
        switch ($reportSubtype) {
            case 'all':
                $flag = "t_all";
                triage_dump($tickets);
                break;
            case 'sla adherence':
                $flag = "t_sla";
                $tickets = array();
                switch ($reportSubSubtype) {
                    case 'agent level':
                        triage_sla_agent_level(&$tickets);
                        break;
                    case 'issue type':
                        triage_sla_issue_type(&$tickets);
                        break;
                    default:
                        break;
                }
                break;
            case 'aht':
                $flag = "t_aht";
                $tickets = array();
                switch ($reportSubSubtype) {
                    case 'agent level':
                        triage_aht_agent_level(&$tickets);
                        break;
                    case 'issue type':
                        triage_aht_issue_type(&$tickets);
                        break;
                    default:
                        break;
                }
                break;
            case 'pendency':
                $tickets = array();
                $flag = "t_pend";
                triage_pendency(&$tickets);
                
                break;
            default:
                break;
        }
        break;
    case 'ccg':
    echo "heello";die("aaaaaa");
        $tickets = array();
        $staffArray = staff_data();
        //echo"<pre>";print_r($staffArray);die;
        switch ($reportSubtype) {
            case 'all':
                $flag = 'ccg_all';
                ccg_all(&$tickets);
                break;
            case 'sla adherence':
                $tickets = array();
                $flag = 'ccg_sla';
                ccg_sla(&$tickets);
                //echo "<pre>";print_r($tickets);die;
                break;
            case 'productivity':
                $flag = 'ccg_prod';
                ccg_prod(&$tickets);

                break;
            case 'pendency':
                $flag = 'ccg_pend';
                ccg_pendency(&$tickets);
                break;
        }
        break;
    case 'crg':
        //echo "h";die;
        $tickets = array();
        $staffArray = staff_data();
        switch ($reportSubtype) {
            case 'all':
                $flag = 'crg_all';
                crg_all(&$tickets);
                break;
            case 'sla adherence':
                $flag = 'crg_sla';
                echo "hello";die;
                crg_sla(&$tickets);
                break;
            case 'aht':
                $flag = 'crg_aht';
                //todo : decide the staff id
                crg_aht(&$tickets);
                break;
            case 'productivity':
                $flag = 'crg_prod';
                break;
        }
        break;
    case 'ops':
        switch ($reportSubtype) {
            case 'all':
                $flag = 'ops_all';
                break;
            case 'departmentwise sla':
                $flag = 'ops_dept';
                break;
            case 'taskwise sla':
                $flag = 'ops_task';
                break;
        }
        break;
    case 'business':
        switch ($reportSubtype) {
            case 'issue type':
                # code...
                break;
            case 'ticket source':
                # code...
                break;
            case 'ticket channel':
                # code...
                break;
            case 'sla adherence':
                # code...
                break;
            case 'productivity':
                # code...
                break;
            default:
                # code...
                break;
        }
        break;
    default:
        # code...
        break;
}

?>