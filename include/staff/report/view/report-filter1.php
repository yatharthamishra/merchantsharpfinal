<?php
$url = "reports.php?report=cs-report"; 
$type=$_GET['type']?$_GET['type']:'';
$subType=$_GET['subtype']?$_GET['subtype']:'';
$subsubType=$_GET['subsubtype']?$_GET['subsubtype']:'';
$typeString=$type?"type=$type":'';
$subTypeString=$subType?"&subtype=$subType":'';
$subsubTypeString=$subsubType?"&subsubtype=$subsubType":'';
$getValString='&'.$typeString.$subTypeString.$subsubTypeString;

?>
<div class="table_with_links left_tickets_link grid2 float_left">
     
    <div class="left_slide_bar_report">
        <ul>
            <?php
            foreach ($fields['navigation'] as $key => $value) {
                ?>
                <li>
                    <h3>
                    <?php 
                    if(empty($value['link'])){
                        ?>
                        <p><?php echo $key; ?></p>
                    <?php
                    }else{
                        ?>
                    <a href="<?php echo $url.$value['link']; ?>" <?php if($_GET['type']==strtolower($key) && strtolower($_GET['subtype'])=='all') echo "class='active'"; ?>><?php echo $key; ?></a>
                    <?php
                    }
                    ?>
                    </h3> 
                    <ul>
                        <?php
                        foreach ($value['subnav'] as $k => $val) {
                            ?>
                            <li>
                                <a href="<?php echo $url.$val; ?>" <?php if($getValString==$val) echo "class='active'"; ?>><?php echo $k; ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>