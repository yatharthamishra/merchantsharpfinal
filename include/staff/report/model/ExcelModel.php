<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExcelModel
 *
 * @author shopclues
 */
class ExcelModel {

    //put your code here

    /**
     * Please write queries with aliases corresponding to values of this variable . This will help
     * you use functions used to find and parse filters corresponding to each entity table . 
     * 
     */
    private $internalToExposedTableAlias = array(
        'ticket' => 'ost',
        'task' => 'ot',
        'ticketData' => 'otod',
        'taskData' => 'otd',
        'ticketThread' => 'ostt',
        'ticketThreadEntry' => 'ostte',
        'taskThread' => 'ott',
        'taskThreadEntry' => 'otte',
        'ticketTriage' => 'osttr',
        'tracking' => 'otr'
    );
    static $staffManagerMapping=null;
    static $distinctManagerId=null;
    public function __construct() {
//        _set_db_connection('report'); // set report slave db as default
    }
/**
 * 
 * This function expects entity condition and value as input array . Example :- 
 * array(ticket__created__in)=>"2016-06-06" will apply filter on mst_ticket with ost as its 
 * alias (as shown in previous variable)
 * @param type $whereClauseArray
 * @return string
 */
    private function parseWhereClause($whereClauseArray) {
        $whereClause = "";
        foreach ($whereClauseArray as $tableAndColumn => $value) {
            if($tableAndColumn=='customWhereClause'){
                foreach ($whereClauseArray['customWhereClause'] as $whereIndex=>$customWhereClause){
                    foreach ($this->internalToExposedTableAlias as $key=>$mappingVal){
                        
                    $whereClauseArray['customWhereClause'][$whereIndex]=str_replace($key, $mappingVal, $whereClauseArray['customWhereClause'][$whereIndex]);
                  
                   
                }
                  $whereClause.=$whereClauseArray['customWhereClause'][$whereIndex];
                }
                continue;
            }
            $tableExternalAliasAndColumn = explode("__", $tableAndColumn);
            $tableAlias = $this->internalToExposedTableAlias[$tableExternalAliasAndColumn[0]];
            $columnName = $tableExternalAliasAndColumn[1];
            $operationToBePerformed=$tableExternalAliasAndColumn[2];
            switch ($operationToBePerformed){
                case '=':
                    $whereClause.=" And $tableAlias.$columnName=" . db_input($value);
                    break;
                case 'in':
                    foreach ($value as $key=>$v){
                    if(is_int($v)){
                        $value[$key]="'".$v."'";
                        
                    }
                        $value[$key]=  db_input($v);
                    }
                    $value=  array_unique($value);
                    $commaSeperatedInClause=implode(",", $value);
                    $whereClause.=" And $tableAlias.$columnName IN ($commaSeperatedInClause)";
                    break;
                case 'btw':
                    $startValue=  db_input($value[0]);
                    $endValue=  db_input($value[1]);
                    if(validateDate($value[0]) && validateDate($value[1])){
                       $whereClause.=" And DATE($tableAlias.$columnName) BETWEEN $startValue and $endValue";
                    }else{
                        $whereClause.=" And $tableAlias.$columnName BETWEEN $startValue and $endValue";
                    }
                    
                    break;
                default :
                    $whereClause.=" And $tableAlias.$columnName=" . db_input($value);
                    break;
            }
           
        }
        return $whereClause;
    }
    public function getTicketsAndTaskRelatedInfo($bucketNumber,$whereClauseArray) {
        $whereClause=$this->_getWhereClauseForTicketsAndTask($whereClauseArray);
        $joinCondition="";
        if(isset($whereClauseArray['filtersForTriageData']) && 
                count($whereClauseArray['filtersForTriageData'])>0){
            $joinCondition=  $this->_getTriageDataWithTicketJoinCondition();
        }
        $offset = $bucketNumber * OPS_DATA_BUCKET_SIZE;
        $sql = "SELECT 
                ost.`number` ,
                ost.`created`  AS ticketCreateDateTime,
                ost.`source_extra` ,
                ost.`topic_id` ,
                otd.`order_id`,
		otd.`manifest_id`,
		otd.`product_id`,
		otd.`batch_id`,
                otd.`createStatus` as orderStatus,
                otd.`title`,
                otd.`task_id` ,
                ot.`dept_id`  ,
                ost.`staff_id` ,
                (ot.`created`)  AS taskCreateDateTime,
                (ot.`closed`) AS taskCloseDateTime,
                (ot.`promised_duedate`) AS taskDueDate ,
                ot.`flags` AS isTaskOpen,
                otd.`taskResponse`,
                otd.`taskType`,
                CASE
                WHEN ot.`closed` IS NULL 
                THEN 
                CASE
                  WHEN ot.`promised_duedate` >= NOW() 
                  THEN 'taskOpenWithinSla' 
                  ELSE 
                  CASE
                    WHEN ot.`promised_duedate` < NOW() 
                    THEN 'taskOpenWithinOSla' 
                    ELSE 
                    CASE
                      WHEN ot.`promised_duedate` IS NULL 
                      THEN 'taskUnknownStatus' 
                    END 
                  END 
                END 
                ELSE 
                CASE
                  WHEN ot.`closed` IS NOT NULL 
                  THEN 
                  CASE
                    WHEN ot.`promised_duedate` >= ot.`closed` 
                    THEN 'taskClosedWithinSla' 
                    ELSE 
                    CASE
                      WHEN ot.`promised_duedate` < ot.`closed` 
                      THEN 'taskClosedWithinOSla' 
                      ELSE 
                      CASE
                        WHEN ot.`promised_duedate` IS NULL 
                        THEN 'taskClosedButUnknownStatus' 
                      END 
                    END 
                  END 
                END 
              END AS taskSlaStatus,
                otd.`taskSource` AS taskSource,
                otd.taskStatus 
                FROM
                `mst_ticket` ost 
                left JOIN `mst_task` ot 
                  ON ost.`ticket_id` = ot.`object_id` 
                JOIN `mst_task__data` otd 
                  ON otd.`task_id` = ot.`id` 
                JOIN `mst_ticket__data` otod ON 
                otod.`ticket_id`=ost.`ticket_id`    
                $joinCondition
                where 1 $whereClause order by otd.`task_id` desc 
                limit $offset,  
            ".OPS_DATA_BUCKET_SIZE.";";
          $res = db_query($sql);
          $set=array();
          while ($result = db_fetch_array($res)) {
               $set[]=$result;
          }
          return $set;
    }

    public function getTaskRelatedThreadInfo() {
        
    }

    public function getNumberOfTimesTaskUpdated($taskIds) {
        $taskIds = array_map('intval', $taskIds);
        if (count($taskIds) == 0) {
            return array();
        }
        $ids = join("','", $taskIds);
        $sql = "SELECT 
                COUNT(1) AS numberOfUpdates ,
                ott.`object_id` as taskId,
                CASE
               WHEN otte.staff_id = 0 
               THEN otte.poster 
               ELSE otte.staff_id 
             END AS agentId
              FROM
                mst_task_thread ott 
                JOIN `mst_task_thread_entry` otte 
                  ON ott.`id` = otte.`thread_id` 
              WHERE ott.`object_id` IN ('$ids') and otte.title='SLA Updated' 
              GROUP BY ott.`object_id` ;";
          $res = db_query($sql);
          $set=array();
          while ($result = db_fetch_array($res)) {
               $set[$result['taskId']]=$result;
          }
          return $set;
    }

    public function getDepartmentWiseTaskInfo($filtersApplied,$whereClauseArray) {
        $whereClause=$this->_getWhereClauseForTicketsAndTask($whereClauseArray);
        $slaConditionForTask = $this->getSLAQueryPartForTask('ot');
        $joinCondition=  $this->_getJoinConditionForTicketsInTaskAccordingToFilter($whereClauseArray);
        $sql = "SELECT 
                ot.`dept_id`,
                COUNT(1) AS totalNumberOfTasks ,
                $slaConditionForTask
                FROM
                  mst_task ot 
                  join mst_task__data otd 
                  on ot.id=otd.task_id 
                  $joinCondition
                  Where 1 
                  $whereClause
                GROUP BY ot.`dept_id` ;
";
          $res = db_query($sql);
          $set=array();
          while ($result = db_fetch_array($res)) {
               $set[]=$result;
          }
          return $set;
    }
    private function _getJoinConditionForTaskData($whereClauseArray){
        $joinCondition="";
        if(isset($whereClauseArray['filtersForTaskData']) && count($whereClauseArray['filtersForTaskData'])>0){
            $joinCondition=" JOIN `mst_task__data` otd 
              ON ot.`id` = otd.`task_id` ";
        }
        
        return $joinCondition;
    }
    private function _getJoinConditionForTicketsInTaskAccordingToFilter($whereClauseArray){
        $joinCondition="";
        $ticketTableJoined=0;
        if(isset($whereClauseArray['filtersForTriageData']) && is_array($whereClauseArray['filtersForTriageData'])
                 && count($whereClauseArray['filtersForTriageData'])>0){
            $joinCondition.=$this->_getTicketWithTaskJoinCondition();
            $joinCondition.=$this->_getTriageDataWithTicketJoinCondition();
            $ticketTableJoined=1;
            
        }
        if(isset($whereClauseArray['filtersForTickets']) && is_array($whereClauseArray['filtersForTickets'])
                 && count($whereClauseArray['filtersForTickets'])>0 && $ticketTableJoined==0){
            $joinCondition.=$this->_getTicketWithTaskJoinCondition();
        }
       
        return $joinCondition;
    }


    private function _getTicketWithTaskJoinCondition(){
        $sql=" JOIN `mst_ticket` ost ON ot.`object_id`=ost.`ticket_id` ";
        return $sql;
    }
    private function _getTriageDataWithTicketJoinCondition(){
        $sql=" JOIN `mst_triage_data` osttr ON osttr.`ticket_id`=ost.`ticket_id` ";
        return $sql;
    }

    public function getTaskWiseSLAReport($whereClauseArray) {
        
        $slaConditionForTask = $this->getSLAQueryPartForTask('ot');
        $whereClause=$this->_getWhereClauseForTicketsAndTask($whereClauseArray);
        $joinCondition=  $this->_getJoinConditionForTicketsInTaskAccordingToFilter($whereClauseArray);
        $sql = "SELECT 
            otd.`title` AS taskName,
            COUNT(1) AS totalNumberOfTasks ,
            ot.dept_id as deptId,
            otd.`taskType`,
           $slaConditionForTask
          FROM
            `mst_task` ot 
            JOIN `mst_task__data` otd 
              ON ot.`id` = otd.`task_id` 
              $joinCondition
              Where 1 $whereClause
              GROUP BY otd.`taskType`  ;
        ";
          $res = db_query($sql);
          $set=array();
          while ($result = db_fetch_array($res)) {
               $set[]=$result;
          }
          return $set;
    }

    public function getSLAQueryPartForTask($tableAliasForTask) {
        $slaConditionQueryForTask = " SUM(
              CASE
                WHEN $tableAliasForTask.`closed` IS NULL 
                THEN 
                CASE
                  WHEN $tableAliasForTask.`promised_duedate` >= NOW() 
                  THEN 1 
                  ELSE 0 
                END 
                ELSE 0 
              END
            ) AS taskOpenWithinSla,
            SUM(
              CASE
                WHEN $tableAliasForTask.`closed` IS NULL 
                THEN 
                CASE
                  WHEN $tableAliasForTask.`promised_duedate` < NOW() 
                  THEN 1 
                  ELSE 0 
                END 
                ELSE 0 
              END
            ) AS taskOpenWithinOSla,
            SUM(
              CASE
                WHEN $tableAliasForTask.`closed` IS NOT NULL 
                THEN 
                CASE
                  WHEN $tableAliasForTask.`promised_duedate` >= $tableAliasForTask.`closed` 
                  THEN 1 
                  ELSE 0 
                END 
                ELSE 0 
              END
            ) AS taskClosedWithinSla,
            SUM(
              CASE
                WHEN $tableAliasForTask.`closed` IS NOT NULL 
                THEN 
                CASE
                  WHEN $tableAliasForTask.`promised_duedate` < $tableAliasForTask.`closed` 
                  THEN 1 
                  ELSE 0 
                END 
                ELSE 0 
              END
            ) AS taskClosedWithinOSla ,
                SUM(
                CASE
                  WHEN $tableAliasForTask.`closed` IS NULL 
                  AND $tableAliasForTask.`promised_duedate` IS NULL 
                  THEN 1 
                  ELSE 0 
                END
              ) AS taskUnknownStatus,
              SUM(
                CASE
                  WHEN $tableAliasForTask.closed IS NOT NULL 
                  AND $tableAliasForTask.promised_duedate IS NULL 
                  THEN 1 
                  ELSE 0 
                END
              ) AS taskClosedButUnknownStatus ";
        return $slaConditionQueryForTask;
    }
    private function _getWhereClauseForTickets($whereClauseArray){

        $whereClause="";
        $whereClause.=$this->parseWhereClause($whereClauseArray['filtersForTickets']);
        $whereClause.=$this->parseWhereClause($whereClauseArray['filtersForTriageData']);
        return $whereClause;
    }
    private function _getWhereClauseForTask($whereClauseArray){
        $whereClause=  $this->parseWhereClause($whereClauseArray['filtersForTask']);
        $whereClause.=$this->parseWhereClause($whereClauseArray['filtersForTaskData']);
        return $whereClause;
    }
   
    private function _getWhereClauseForTicketsAndTask($whereClauseArray){
        $whereClause="";
        $whereClauseForTickets.=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClauseForTask=  $this->_getWhereClauseForTask($whereClauseArray);
        $whereClause=$whereClauseForTickets.$whereClauseForTask;
        return $whereClause;
    }
    private function _getWhereClauseForThreadEntry($whereClauseArray){
        $whereClause="";
        $whereClause.=$this->parseWhereClause($whereClauseArray['filtersForThreadEntry']);
        return $whereClause;
    }
    private function _getWhereClauseForTracking($whereClauseArray){
        $whereClause="";
        $whereClause.=$this->parseWhereClause($whereClauseArray['filtersForTracking']);
        return $whereClause;
    }
    private function _getJoinClauseForTicketWiseReport($whereClauseArray){
        $sql="";
        if(isset($whereClauseArray['filtersForTriageData']) && is_array($whereClauseArray['filtersForTriageData'])
                && count($whereClauseArray['filtersForTriageData'])>0){
            $sql.=$this->_getTriageDataWithTicketJoinCondition();
        }
        return $sql;
    }
    public function getTicketWiseSummaryReportByDifferentParams($parameter,$whereClauseArray) {
        $whereClause=  $this->_getWhereClauseForTicketsAndTask($whereClauseArray);
        $joinCondition=  $this->_getJoinClauseForTicketWiseReport($whereClauseArray);
        switch ($parameter) {
            case 'source':
                $groupByAndSelectParameter = 'source_extra';
                break;
            case 'issue':
                $groupByAndSelectParameter = 'topic_id';
                break;
            default :
                $groupByAndSelectParameter = 'topic_id';
                break;
        }
        $sql = "SELECT 
                ost.`$groupByAndSelectParameter`,
                COUNT(1) AS totalNumberOfTickets,
                ost.topic_id as topicId,
                ost.source_extra,
                SUM(
                    CASE
                      WHEN ost.`closed` IS NOT NULL 
                      AND ost.`created` IS NOT NULL 
                      THEN (
                        UNIX_TIMESTAMP(ost.`closed`) - UNIX_TIMESTAMP(ost.`created`)
                      ) 
                      ELSE 0 
                    END
                  ) AS `totalTimeSpent`,
                SUM(
                  CASE
                    WHEN ost.`closed` IS NULL 
                    THEN 
                    CASE
                      WHEN ost.`est_duedate` >= NOW() 
                      THEN 1 
                      ELSE 0 
                    END 
                    ELSE 0 
                  END
                ) AS ticketsOpenWithinSla,
                SUM(
                  CASE
                    WHEN ost.`closed` IS NULL 
                    THEN 
                    CASE
                      WHEN ost.`est_duedate` < NOW() 
                      THEN 1 
                      ELSE 0 
                    END 
                    ELSE 0 
                  END
                ) AS ticketsOpenWithinOSla,
                SUM(
                  CASE
                    WHEN ost.`closed` IS NOT NULL 
                    THEN 
                    CASE
                      WHEN ost.`est_duedate` >= ost.`closed` 
                      THEN 1 
                      ELSE 0 
                    END 
                    ELSE 0 
                  END
                ) AS ticketsClosedWithinSla,
                SUM(
                  CASE
                    WHEN ost.`closed` IS NOT NULL 
                    THEN 
                    CASE
                      WHEN ost.`est_duedate` < ost.`closed` 
                      THEN 1 
                      ELSE 0 
                    END 
                    ELSE 0 
                  END
                ) AS ticketsClosedWithinOSla,
                SUM(
                  CASE
                    WHEN ost.`closed` IS NULL 
                    AND ost.`est_duedate` IS NULL 
                    THEN 1 
                    ELSE 0 
                  END
                ) AS ticketsUnknownStatus,
                SUM(
                  CASE
                    WHEN ost.closed IS NOT NULL 
                    AND ost.est_duedate IS NULL 
                    THEN 1 
                    ELSE 0 
                  END
                ) AS ticketsClosedButUnknownStatus 
              FROM
                `mst_ticket` ost $joinCondition
                Where 1 $whereClause
              GROUP BY ost.`$groupByAndSelectParameter` ;
";
        
          $res = db_query($sql);
          $set=array();
          while ($result = db_fetch_array($res)) {
              $result['aht']=round((($result['totalTimeSpent']/$result['totalNumberOfTickets'])/3600)/24);
              $set[]=$result;
          }
          return $set;
    }

    public function getIssueWiseSummaryReportData($whereClause) {
        $issueWiseSummaryReport=$this->getTicketWiseSummaryReportByDifferentParams('issue',$whereClause);
        return $issueWiseSummaryReport;
    }

    public function getSourceWiseTicketData($whereClause) {
        $sourceWiseSummaryReport=$this->getTicketWiseSummaryReportByDifferentParams('source',$whereClause);
        return $sourceWiseSummaryReport;
    }

    public function getProductivityInfoByTask($whereClauseArray) {
        $whereClause=$this->_getWhereClauseForTicketsAndTask($whereClauseArray);
        $joinCondition=  $this->_getJoinConditionForTicketsInTaskAccordingToFilter($whereClauseArray);
        $slaConditionForTask = $this->getSLAQueryPartForTask('ot');
        $sql = "SELECT 
                ot.`staff_id`,
                COUNT(1) AS totalNumberOfTasks,
                otd.`taskType`,
               $slaConditionForTask
              FROM
                `mst_task` ot 
                JOIN `mst_task__data` otd 
                  ON ot.`id` = otd.`task_id` 
                  $joinCondition
                  Where 1 $whereClause
              GROUP BY ot.`staff_id` ,otd.`taskType`;

";
   
          $res = db_query($sql);
          $set=array();
          while ($result = db_fetch_array($res)) {
               $set[]=$result;
          }
          return $set;
    }

    public function getStaffManagerMapping() {
        if(!is_null(self::$staffManagerMapping)){
            return self::$staffManagerMapping;
        }
        $memcacheObj = new MemcacheStorage(7200);
        $sql = "SELECT 
            staff_id,
            email,
            manager_id ,
            firstname,
            lastname
          FROM
            mst_staff 
            LEFT JOIN mst_staff_reporting 
              ON staff_id = agent_id 
              Where 1 ";
        $memcacheKey='Report-Module'.md5($sql);
        $staffManMapping=$memcacheObj->getValue($memcacheKey);
        if($staffManMapping && !$_GET['flush']){
            return $staffManMapping;
        }
        $res = db_query($sql);
        $staffArray=[];
        while ($result = db_fetch_array($res)) {
            $staffArray[$result['staff_id']]['email'] = $result['email'];
            $staffArray[$result['staff_id']]['name'] = $result['firstname'].' '.$result['lastname'];
            $staffArray[$result['staff_id']]['manager_id'] = $result['manager_id'];
            $staffArray['managerToStaffMap'][$result['manager_id']][]=$result['staff_id'];
        }
        $memcacheObj->setValue($memcacheKey, $staffArray);
        self::$staffManagerMapping=$staffArray;
        return $staffArray;
    }
    public function getDistinctManagerId() {
        if(!is_null(self::$distinctManagerId)){
            return self::$distinctManagerId;
        }
        $sql = "SELECT DISTINCT 
                manager_id 
              FROM
                mst_staff_reporting 
              WHERE manager_id > 0 ";
        $res = db_query($sql);
        $managerIds = [];
        while ($result = db_fetch_array($res)) {
            $managerIds[] = $result['manager_id'];
        }
        self::$distinctManagerId=$managerIds;
        return $managerIds;
    }

    /*
    Divyanshu's functions
    */

    /*
    triage report functions start
    */
    public function staff_data(){
        $sql = "SELECT 
                    `staff_id`, `email`, `manager_id`
                FROM
                    mst_staff
                        LEFT JOIN
                    mst_staff_reporting ON `staff_id` = `agent_id`";
        $res = db_query($sql);
        while ($result = db_fetch_array($res)) {
            $staffArray[$result['staff_id']]['email'] = $result['email'];
            $staffArray[$result['staff_id']]['manager'] = $result['manager_id'];
        }
        return $staffArray;
    }

    public function getTimeToTriage(){
        global $cfg;
        $TimeToTriage = $cfg->config['TimeToTriage']["value"];
        return $TimeToTriage;
    }
    public function getTicketsTriageDataForDump($bucketNumber,$whereClauseArray,$ChunkSize){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $offset = $bucketNumber * $ChunkSize;
        $TimeToTriage = getTimeToTriage();
        $sql = "SELECT 
                    ost.`number`,
                    ost.`source_extra`,
                    osttr.`priority` as priority,
                    ost.`created`,
                    otod.`order_ids` as order_ids,
                    ost.`topic_id`,
                    ostte.`staff_id` AS staff_id,
                    ostte.`created` as triage_date,
                    CASE WHEN ostte.`title`='Triage field change' THEN 3
                        WHEN ostte.`title`='Triaged' THEN 1 END AS triage_status,
                    CASE
                        WHEN
                            (TIMESTAMPDIFF(SECOND,
                                CASE WHEN ostte.`title`='Triaged' THEN ost.`created`
                                      WHEN ostte.`title`='Triage field change' THEN osttr.`triage_again` END,
                                CASE WHEN ostte.`created` is not null THEN ostte.`created` ELSE NOW() END) < $TimeToTriage )
                        THEN
                            'SLA'
                        ELSE 'OSLA'
                    END AS sla_status,
                    CASE WHEN osttr.`triage_again` is not null and osttr.`triage_again`!='0000-00-00 00:00:00' THEN osttr.`triage_again`
                        ELSE osttr.`triage_time` END AS updated,
                    TIMESTAMPDIFF(SECOND,
                        otr.`last_timestamp`,
                        otr.`new_timestamp`) as aht
                from
                    mst_ticket ost
                        join
                    mst_ticket__data otod ON ost.`ticket_id` = otod.`ticket_id`
                        join
                    mst_triage_data osttr ON ost.`ticket_id` = osttr.`ticket_id`
                        join
                    mst_thread ostt ON ost.`ticket_id` = ostt.`object_id`
                        join
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                        join
                    mst_tracking otr on (ost.`ticket_id`=otr.`ticket_id` and (CASE WHEN ostte.`title`='Triage field change' THEN otr.`event_name`='Triage again' WHEN ostte.`title`='Triaged' THEN otr.`event_name`='Triaging ticket' END))
                where (ostte.`title`='Triage field change' or ostte.`title`='Triaged') $whereClause
                        limit $offset,$ChunkSize";
                       
        $res = db_query($sql);
        return $res;
    }

    public function getTriageHandledAgentWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $TimeToTriage = $this->getTimeToTriage();
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    DATE(ost.`created`) AS created_date,
                    DATE(ostte.`created`) As thread_date,
                    ostte.`staff_id` AS staff_id,
                    sum(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ostte.`created`) <  $TimeToTriage ) AS within
                FROM
                    mst_ticket ost
                        LEFT JOIN
                    mst_thread ostt ON ost.`ticket_id` = ostt.`object_id`
                        LEFT JOIN
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                WHERE
                    ostte.`title` = 'Triaged' $whereClause
                GROUP BY DATE(ostte.`created`) , ostte.`staff_id` 
                ORDER BY DATE(ostte.`created`) DESC";    
        $res = db_query($sql);
        return $res;
    }

    public function getTriageHandledIssueWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $TimeToTriage = $this->getTimeToTriage();
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    sum(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ostte.`created`) <  $TimeToTriage ) AS within,
                    ost.`topic_id` AS topic_id,
                    DATE(ostte.`created`) AS thread_date
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_thread ostt ON ost.`ticket_id` = ostt.`object_id`
                        INNER JOIN
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                WHERE
                    ostte.`title` = 'Triaged' $whereClause
                GROUP BY DATE(ostte.`created`) , ost.`topic_id`
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }

    public function getTriageAgainHandledAgentWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $TimeToTriage = $this->getTimeToTriage();
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    DATE(ost.`created`) AS created_date,
                    DATE(ostte.`created`) AS thread_date,
                    ostte.`staff_id` AS staff_id,
                    sum(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ostte.`created`) <  $TimeToTriage ) As within,
                    ostte.`poster` AS staff_name
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_thread ostt ON ost.`ticket_id` = ostt.`object_id`
                        INNER JOIN
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                WHERE
                    ostte.`title` = 'Triage field change' $whereClause
                GROUP BY DATE(ostte.`created`) , ostte.`staff_id` , ostte.`poster`
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }

    

    

    public function getTriageAgainHandledIssueWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $TimeToTriage = $this->getTimeToTriage();
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    sum(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ostte.`created`) <  $TimeToTriage ) AS within,
                    ost.`topic_id` AS topic_id,
                    DATE(ostte.`created`) AS thread_date
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_thread ostt ON ost.`ticket_id` = ostt.`object_id`
                        INNER JOIN
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                WHERE
                    ostte.`title` = 'Triage field change' $whereClause
                GROUP BY DATE(ostte.`created`) , ost.`topic_id`
                ORDER BY DATE(ostte.`created`) DESC";
                
        $res = db_query($sql);
        return $res;
    }

    

    public function getAhtForTriagingAgentWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTracking($whereClauseArray);
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    DATE(otr.`new_timestamp`) AS date,
                    otr.`staff_id`,
                    sum(TIMESTAMPDIFF(SECOND,
                        otr.`last_timestamp`,
                        otr.`new_timestamp`)) as timediff
                FROM
                    mst_tracking otr
                WHERE
                    otr.`event_name` = 'Triaging Ticket' $whereClause
                GROUP BY DATE(otr.`new_timestamp`) , otr.`staff_id`
                ORDER BY DATE(otr.`new_timestamp`) DESC";
        $res = db_query($sql);
        return $res;
    }

    public function getAhtForTriageAgainAgentWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTracking($whereClauseArray);
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    DATE(otr.`new_timestamp`) AS date,
                    otr.`staff_id`,
                    sum(TIMESTAMPDIFF(SECOND,
                        otr.`last_timestamp`,
                        otr.`new_timestamp`)) AS timediff
                FROM
                    mst_tracking otr
                WHERE
                    otr.`event_name` = 'Triage again' $whereClause
                GROUP BY DATE(otr.`new_timestamp`) , otr.`staff_id`
                ORDER BY DATE(otr.`new_timestamp`) DESC";
        $res = db_query($sql);
        return $res;
    }

    

    public function getAhtForTriagingIssueWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForTracking($whereClauseArray);
        $sql = "SELECT 
                    count(*) as ticket_count,
                    DATE(otr.`new_timestamp`) as date,
                    otr.`staff_id`,
                    sum(TIMESTAMPDIFF(SECOND,
                        otr.`last_timestamp`,
                        otr.`new_timestamp`)) as timediff,
                    ost.`topic_id`
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_tracking otr ON ost.`ticket_id` = otr.`ticket_id`
                WHERE
                    otr.`event_name` = 'Triaging Ticket' $whereClause
                GROUP BY DATE(otr.`new_timestamp`) , ost.`topic_id`
                ORDER BY DATE(otr.`new_timestamp`) DESC";
        $res = db_query($sql);
        return $res;
    }

    public function getAhtForTriageAgainIssueWise($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForTracking($whereClauseArray);
        $sql = "SELECT 
                    count(*) AS ticket_count,
                    DATE(otr.`new_timestamp`) AS date,
                    otr.`staff_id`,
                    sum(TIMESTAMPDIFF(SECOND,
                        otr.`last_timestamp`,
                        otr.`new_timestamp`)) AS timediff,
                    ost.`topic_id`
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_tracking otr ON ost.`ticket_id` = otr.`ticket_id`
                WHERE
                    otr.`event_name` = 'Triage again' $whereClause
                GROUP BY DATE(otr.`new_timestamp`) , ost.`topic_id`
                ORDER BY DATE(otr.`new_timestamp`) DESC";
        $res = db_query($sql);
        return $res;
    }

    

    /*
    tickets handled triaged = getTicketsNotTriagedClosedPerDate() +getTicketsTriagedPerDate()
    */
    public function getTicketsNotTriagedClosedPerDate($whereClauseArray,$flag="all"){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause=str_replace('created', 'closed', $whereClause);
        $TimeToTriage = $this->getTimeToTriage();
        if($flag=="current"){
          $whereClause=" AND DATE(ost.`closed`)=DATE(NOW())";
        }
        $sql = "SELECT 
                    count(*) AS ticket_count, 
                    DATE(ost.`closed`) AS date,
                    SUM(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ost.`closed`) <  $TimeToTriage ) as within
                FROM
                    `mst_ticket` ost
                        left join
                    `mst_triage_data` osttr ON ost.`ticket_id` = osttr.`ticket_id`
                WHERE
                    (ost.`status_id` IN (2,3)
                        and (osttr.`triage` is null or osttr.`triage` = 1)) $whereClause
                group by DATE(ost.`closed`)
                ORDER BY DATE(ost.`closed`) DESC";
        $res = db_query($sql);
        return $res;
    }
    public function getTicketsNotTriageAgainClosedPerDate($whereClauseArray,$flag="all"){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $whereClause=str_replace('created', 'closed', $whereClause);
        $TimeToTriage = getTimeToTriage();
        if($flag=="current"){
          $whereClause=" AND DATE(ost.`closed`)=DATE(NOW())";
        }

        $sql = "SELECT 
                    count(*) AS ticket_count, 
                    DATE(ost.`closed`) AS date,
                    SUM(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ost.`closed`) <  $TimeToTriage ) as within
                FROM
                    `mst_ticket` ost
                        left join
                    `mst_triage_data` osttr ON ost.`ticket_id` = osttr.`ticket_id`
                WHERE
                    ost.`status_id` IN (2,3)
                        and (osttr.`triage` = 3) $whereClause
                group by DATE(ost.`closed`)";
        $res = db_query($sql);
        return $res;
    }
    public function getTicketsTriagedPerDate($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $sql = "SELECT count(*) AS ticket_count,
                    DATE(ostte.`created`) AS date
                FROM
                    mst_thread_entry ostte
                WHERE
                    ostte.`title` = 'Triaged' $whereClause
                group by DATE(ostte.`created`)
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }
    public function getTicketsReceivedUntriagedPerDate($whereClauseArray,$flag){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        if($flag=="current"){
          $whereClause=" AND DATE(ost.`created`)=DATE(NOW())";
        }
        $sql = "SELECT 
                    DATE(ost.`created`) AS date, 
                    count(*) AS ticket_count
                FROM
                    mst_ticket ost
                where 1 $whereClause
                GROUP BY DATE(ost.`created`)
                ORDER BY DATE(ost.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }
    public function getTicketsReceivedTriageWise($triageType,$whereClauseArray,$flag){
        
        if($flag){
          $issueTypeCond = ', ost.`topic_id` as topic_id';
          $groupByCond = ',ost.`topic_id`';
        }
        if($triageType=="triage"){
            $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
            $sql = "SELECT 
                        ost.`created` AS date, 
                        ost.`ticket_id` as ticket_id $issueTypeCond
                    FROM
                        mst_ticket ost
                    where 1 $whereClause";
        }
        else{
            $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
            $sql = "SELECT 
                        ostte.`created` AS date,
                        ost.`ticket_id` as ticket_id $issueTypeCond
                    FROM
                        mst_thread_entry ostte
                          JOIN
                        mst_thread ostt on ostte.`thread_id`=ostt.`id`
                          JOIN
                        mst_ticket ost on ostt.`object_id`=ost.`ticket_id`
                    WHERE
                        ostte.`title` = 'Triage again' $whereClause";
        }
        $res = db_query($sql);
        return $res;

    }
    //todo : to be removed
    public function getTicketsReceivedTriageAgainPerDateIssueWise($whereClauseArray){
        //$whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $sql = "SELECT 
                    COUNT(*) AS ticket_count, 
                    DATE(ostte.`created`) AS date,
                    ost.`topic_id` as topic_id
                FROM
                    mst_thread_entry ostte
                      JOIN
                    mst_thread ostt on ostte.`thread_id`=ostt.`id`
                      JOIN
                    mst_ticket ost on ostt.`object_id`=ost.`ticket_id`
                WHERE
                    ostte.`title` = 'Triage again' $whereClause
                GROUP BY DATE(ostte.`created`),ost.`topic_id`
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }
    public function getTicketsReceivedTriageAgainPerDate($whereClauseArray,$flag){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        if($flag=="current"){
          $whereClause=" AND DATE(ostte.`created`)=DATE(NOW())";
        }
        $sql = "SELECT 
                    COUNT(*) AS ticket_count, DATE(ostte.`created`) AS date
                FROM
                    mst_thread_entry ostte
                WHERE
                    ostte.`title` = 'Triage again' $whereClause
                GROUP BY DATE(ostte.`created`)
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }
    public function getTicketsHandledTriageAgainPerDate($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $sql = "SELECT 
                    count(distinct ostte.`thread_id`) AS ticket_count,
                    DATE(ostte.`created`) AS date
                 
                FROM
                    
                    mst_thread_entry ostte
                WHERE
                    ostte.`title`='Triage field change' $whereClause
                GROUP BY DATE(ostte.`created`)
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }
    
    public function getTicketPendingUntriaged($whereClauseArray){
        //$whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
         $sql = "SELECT 
                        ost.`ticket_id` as ticket_id,
                        ost.`created` as create_date,
                        ost.`closed` as close_date,
                        ostte.`created` as triage_date,
                        ost.status_id
                    from
                        mst_ticket as ost
                            left join
                        mst_thread AS ostt ON ost.`ticket_id` = ostt.`object_id`
                            LEFT JOIN
                        mst_thread_entry AS ostte ON (ostt.`id` = ostte.`thread_id`
                        AND ostte.`title` = 'Triaged')
                    where 1 $whereClause";
        $res = db_query($sql);
        return $res;
    }

    public function getTicketPendingTriageAgain(){
      ///check
      $whereClause = "";
        //$whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $sql = "SELECT 
                    date(ostte.`created`) as create_date,
                    ostr.`triage_again` as triage_again,
                    ostr.`triage_time` as triage_time,
                    ost.`closed` as closed,
                    ostr.`triage` as triage,
                    ost.`status_id` as status_id,
                    ostte.`thread_id` as thread_id
                from
                    mst_thread_entry ostte
                        LEFT JOIN
                    mst_thread ostt ON ostte.`thread_id` = ostt.`id`
                        LEFT JOIN
                    mst_triage_data ostr ON ostr.`ticket_id` = ostt.`object_id`
                        LEFT JOIN
                    mst_ticket ost ON ost.`ticket_id` = ostr.`ticket_id`
                where
                    ostte.`title` = 'triage again'
                        and (ostr.`triage` IN (2 , 3)) $whereClause";
        $res = db_query($sql);
        return $res;
    }
    
    public function getTriagedHandledWithinSla($whereClauseArray,$flag){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray); 
        $TimeToTriage = $this->getTimeToTriage();
        if($flag=="current"){
          $whereClause=" AND DATE(ostte.`created`)=DATE(NOW())";
        }
        $sql = "SELECT 
                    count(*) as ticket_count,
                    sum(TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        ostte.`created`) <  $TimeToTriage ) as within,
                    DATE(ostte.`created`) AS date
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_thread ostt ON ost.`ticket_id` = ostt.`id`
                        INNER JOIN
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                WHERE
                    ostte.`title` = 'Triaged' $whereClause
                GROUP BY DATE(ostte.`created`)
                ORDER BY DATE(ostte.`created`) DESC";
        $res = db_query($sql);
        return $res;
    }

    public function getTriageAgainHandledWithinSla($whereClauseArray,$flag=0){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray); 
        $TimeToTriage = $this->getTimeToTriage();
        if($flag){
            $joinCondition = "JOIN mst_thread ostt ON ostt.id=ostte.`thread_id` JOIN mst_ticket ost ON ost.`ticket_id`=ostt.`object_id`";
            $selectCond = ", ost.`topic_id`";
        }
        $sql = "SELECT 
                    ostte.`thread_id`, 
                    ostte.`title`, 
                    group_concat(ostte.`created` ORDER BY ostte.`created` ASC) as create_dates $selectCond
                FROM
                    `mst_thread_entry` ostte $joinCondition
                WHERE
                    ostte.`title` = 'Triage again'
                        OR 
                    ostte.`title` = 'Triage field change'
                GROUP BY ostte.`thread_id` , ostte.`title`";
        $res = db_query($sql);
        return $res;
    }

    function getTriageAgainHandledWithinSlaCurrentDate(){
        $TimeToTriage = getTimeToTriage();
        $sql = "SELECT 
                    count(*) as ticket_count
                  FROM
                    mst_ticket AS ost 
                    JOIN mst_thread AS ostt 
                      ON ost.`ticket_id` = ostt.`object_id` 
                    JOIN mst_triage_data otd 
                      ON otd.`ticket_id` = ost.`ticket_id` 
                    JOIN 
                      (SELECT 
                        MAX(id) AS idmax,
                        thread_id,
                        MAX(created) AS created,
                        title 
                      FROM
                        mst_thread_entry 
                      WHERE title = 'Triage field change'
                      GROUP BY thread_id 
                      ORDER BY id DESC) ostte1 
                      ON (
                        ostt.`id` = ostte1.`thread_id` 
                        AND otd.`triage_again` < ostte1.`created`
                      ) where TIMESTAMPDIFF(SECOND,otd.`triage_again`,ostte1.`created`)<=$TimeToTriage AND DATE(ostte1.`created`)=DATE(NOW())";
        $res = db_query($sql);
        return $res;
    }
    /*
    This function gives count of tickets open within sla of both triage and triage again for current date
    */
    public function getTicketsOpenWithinSlaCurrentDate(){
        $sql = "SELECT 
                    case
                        when osttr.`triage` is null then 1
                        else osttr.`triage`
                    end as triage,
                    count(*) as ticket_count,
                    sum((ost.`created` < NOW() - INTERVAL 172800 SECOND
                        AND ((osttr.`triage` != 2 and osttr.`triage` != 3)
                        or osttr.`triage` is null))
                        OR (osttr.`triage_again` < NOW() - INTERVAL 172800 SECOND
                        AND osttr.`triage` = 3)) AS pending,
                    sum((((ost.`created` > (NOW() - INTERVAL 172800 SECOND)
                        AND ost.`created` < (NOW() - INTERVAL 151200 SECOND)))
                        AND (osttr.`triage` != 2 or osttr.`triage` is null))
                        OR (((osttr.`triage_again` > (NOW() - INTERVAL 172800 SECOND)
                        AND osttr.`triage_again` < (NOW() - INTERVAL 151200 SECOND)))
                        AND osttr.`triage` = 3)) AS alert
                FROM
                    mst_ticket ost
                        LEFT JOIN
                    mst_triage_data osttr ON (ost.`ticket_id` = osttr.`ticket_id`)
                WHERE
                    1 AND ost.`status_id` IN ('1' , '6')
                GROUP BY osttr.`triage`";
        $res = db_query($sql);
        return $res;
    }
    public function getDataToCalTicketsOpenSlaTriageWise($triageType,$whereClauseArray,$flag){
        if($flag)
          $issueTypeCond = ",ost.`topic_id` as topic_id";
        if($triageType=='triage'){
            //$whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
            $sql = "SELECT 
                        ost.`ticket_id` as ticket_id,
                        ost.`created` as create_date,
                        ost.`closed` as close_date,
                        ostte.`created` as triage_date $issueTypeCond
                    from
                        mst_ticket as ost
                            left join
                        mst_thread AS ostt ON ost.`ticket_id` = ostt.`object_id`
                            LEFT JOIN
                        mst_thread_entry AS ostte ON (ostt.`id` = ostte.`thread_id`
                        AND ostte.`title` = 'Triaged')
                    where 1 $whereClause";
        }
        else{
            //$whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);            
            $sql = "SELECT 
                ost.`ticket_id` AS ticket_id,
                otd.`triage_again` AS create_date,
                ost.`closed` AS close_date,
                ostte1.`created` AS triage_date $issueTypeCond
              FROM
                mst_ticket AS ost 
                JOIN mst_thread AS ostt 
                  ON ost.`ticket_id` = ostt.`object_id` 
                JOIN mst_triage_data otd 
                  ON otd.`ticket_id` = ost.`ticket_id` 
                JOIN 
                  (SELECT 
                    MAX(id) AS idmax,
                    thread_id,
                    MAX(created) AS created,
                    title 
                  FROM
                    `mst_thread_entry` 
                  WHERE `title` = 'triage again' 
                  GROUP BY thread_id 
                  ORDER BY id ASC) ostte 
                  ON (ostt.`id` = ostte.`thread_id`) 
                LEFT JOIN 
                  (SELECT 
                    MAX(id) AS idmax,
                    thread_id,
                    MAX(created) AS created,
                    title 
                  FROM
                    mst_thread_entry 
                  WHERE title = 'Triage field change' 
                    OR title = 'Triaged' 
                  GROUP BY thread_id 
                  ORDER BY id DESC) ostte1 
                  ON (
                    ostt.`id` = ostte1.`thread_id` 
                    AND ostte.`idmax` < ostte1.`idmax`
                  ) $whereClause";
        }
        $res = db_query($sql);
        return $res;
        
    }

	//todo:check whether this function is needed or not
    public function getDataToCalOpenTriageAgainWithinSla(){
        $TimeToTriage = $this->getTimeToTriage();
        $sql = "SELECT 
                    DATE(ost.`created`) AS create_date,
                    DATE(ostte.`created`) AS triage_date, 
                    count(*) AS ticket_count
                FROM
                    mst_ticket ost
                        left join
                    mst_thread ostt ON ost.`ticket_id` = ostt.`object_id`
                        left join
                    mst_thread_entry ostte ON ostt.`id` = ostte.`thread_id`
                WHERE
                    ((ostte.`title` = 'Triage field change')
                        or ostte.`type` is null)
                        and (TIMESTAMPDIFF(SECOND,
                        ost.`created`,
                        (CASE
                            when ostte.`created` is not null THEN ostte.`created`
                            else NOW()
                        end)) < " . $TimeToTriage . ") $whereClause
                GROUP BY DATE(ost.`created`),DATE(CASE
                            WHEN ostte.`created` is not null THEN ostte.`created`
                            ELSE NOW() END)
                ORDER BY create_date,triage_date";
        $res = db_query($sql);
        return $res; 
    }

    public function getSlaSummarySavedData($dateFilterApplied){
      $fromDate=$dateFilterApplied['threadCreateFromdate'];
      $toDate=$dateFilterApplied['threadCreateTodate'];
      $fromDateValid=  validateDate($fromDate);
      $toDateValid=  validateDate($toDate);
      $whereClause="";
      if($fromDate && $toDate){
          $whereClause="and date between '$fromDate' and '$toDate' ";
      }elseif($fromDateValid){
          $whereClause="and date between '$fromDate' and '9999-12-31'";
      }elseif($toDateValid){
          $whereClause="and date between '1000-01-01' and $toDate";
      }
      $whereClause.="and status_id=1";
      $sql = "SELECT 
                    ostsla.`date`,
                    ostsla.`tickets_received_untriaged`,
                    ostsla.`tickets_received_triage_again`,
                    ostsla.`tickets_handled_within_sla_untriaged`,
                    ostsla.`tickets_handled_within_sla_triage_again`,
                    ostsla.`tickets_open_within_sla_untriaged`,
                    ostsla.`tickets_open_within_sla_triage_again`
                FROM
                    mst_sla_summary_report ostsla where 1 $whereClause";
        $res = db_query($sql);
        return $res;
    }

    
    /*
    triage report functions end
    */

    public function getEmailTasksCreated($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForTickets($whereClauseArray);
        $sql = "SELECT 
                    DATE(ostte.`created`) AS date,
                    count(*) AS ticket_count,
                    ost.`staff_id` AS staff_id
                FROM
                    mst_thread_entry ostte
                      JOIN
                    mst_thread ostt ON ostte.`thread_id`=ostt.`id`
                      JOIN
                    mst_ticket ost ON ostt.`object_id`=ost.`ticket_id`
                WHERE
                    (ostte.`title`='Triaged' or ostte.`title`='Triage field change')
                        and ostte.`staff_id` != 0 $whereClause
                GROUP BY DATE(ostte.`created`) , ost.`staff_id`";
                //echo $sql;die;
        $res = db_query($sql);
        return $res;
    }

    public function getCallTasksCreated($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForTickets($whereClauseArray);
        $sql = "SELECT 
                    DATE(ostte.`created`) AS date,
                    count(*) AS ticket_count,
                    ost.`staff_id` AS staff_id
                FROM
                    mst_thread_entry ostte
                      JOIN
                    mst_thread ostt ON ostte.`thread_id`=ostt.`id`
                      JOIN
                    mst_ticket ost ON ostt.`object_id`=ost.`ticket_id`
                WHERE
                    ostte.`body` like '%Call customer</b> Yes%'
                        and ostte.`staff_id` != 0 $whereClause
                GROUP BY DATE(ostte.`created`) , ost.`staff_id`";
        $res = db_query($sql);
        return $res;
    }

    public function getEmailTasksHandled($whereClauseArray){
        $whereClause=$this->_getWhereClauseForThreadEntry($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForTickets($whereClauseArray);
        $sql = "SELECT 
                    DATE(ostte.`created`) AS date,
                    count(*) AS ticket_count,
                    ost.`staff_id` AS staff_id
                FROM
                    mst_thread_entry ostte
                      JOIN
                    mst_thread ostt ON ostte.`thread_id`=ostt.`id`
                      JOIN
                    mst_ticket ost ON ostt.`object_id`=ost.`ticket_id`
                WHERE
                    ostte.`type` = 'R' and ostte.`staff_id` != 0 $whereClause
                GROUP BY DATE(ostte.`created`) , ost.`staff_id`";
        $res = db_query($sql);
        return $res;
    }

    public function getEmailTasksPending($whereClauseArray){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        $sql = "SELECT 
                    DATE(ostte.`created`) AS date,
                    count(*) AS ticket_count,
                    ost.`staff_id` AS staff_id,
                    ostte.`type` AS type,
                    ostte.`title` AS title
                FROM
                    mst_thread_entry ostte
                      JOIN
                    mst_thread ostt ON ostte.`thread_id`=ostt.`id`
                      JOIN
                    mst_ticket ost ON ostt.`object_id`=ost.`ticket_id`
                WHERE
                    (ostte.`type` = 'R' OR ostte.`title`='Triaged' OR ostte.`title`='Triage field change') AND ostte.`staff_id` != 0 $whereClause
                GROUP BY DATE(ostte.`created`) , ost.`staff_id` , ostte.`type` , ostte.`title`
                ORDER BY DATE(ostte.`created`)";
        $res = db_query($sql);
        return $res; 
    }

    /*
    This function gives the number of task handled per day directly by agent i.e he/she changed the status of task directly.
    */
    public function getTasksHandledDirectly($dept_id,$whereClauseArray){
        $whereClause=$this->_getWhereClauseForTask($whereClauseArray);
        $sql = "SELECT 
                    DATE(ot.`closed`) AS date,
                    ot.`staff_id`,
                    count(*) AS count,
                    sum(TIMESTAMPDIFF(SECOND,
                        ot.`created`,
                        ot.`closed`)) AS timediff,
                       sum(TIMESTAMPDIFF(SECOND,
                        ot.`created`,
                        ot.`closed`))/ count(*) as aht
                FROM
                    mst_task ot
                WHERE
                    ot.`dept_id` = ". $dept_id ." AND ot.`flags` = 0 AND ot.`staff_id`!=0 $whereClause
                GROUP BY DATE(ot.`closed`) , ot.`staff_id`";

        $res = db_query($sql);
        return $res;    
    }
    /*
    This function gives the number of task handled per day indirectly by agent i.e he/she changed the reponse of task which lec to closure of task.
    */
    public function getTasksHandledIndirectly($dept_id,$whereClauseArray){
        $whereClause=$this->_getWhereClauseForTask($whereClauseArray);
        $sql = "SELECT 
                    DATE(`closed`) AS date,
                    otte.`staff_id` AS staff_id,
                    count(*) AS count,
                    sum(TIMESTAMPDIFF(SECOND,
                        ot.`created`,
                        otte.`created`)) AS timediff,
                    sum(TIMESTAMPDIFF(SECOND,
                        ot.`created`,
                        otte.`created`)) / count(*) AS aht
                FROM
                    mst_task ot
                        INNER JOIN
                    mst_task_thread ott ON ot.`id` = ott.`object_id`
                        inner JOIN
                    mst_task_thread_entry otte ON ott.`id` = otte.`thread_id`
                WHERE
                    otte.`title` = 'Task Response Updated'
                        and otte.`staff_id` != 0
                        and ot.`flags` = 0
                        and ot.`dept_id` = ". $dept_id ." $whereClause
                GROUP BY DATE(otte.`created`) , otte.`staff_id`";
        $res = db_query($sql);
        return $res;
    }

    public function getSlaTaskData($dept_id,$whereClauseArray,$type="all"){
        $whereClause=$this->_getWhereClauseForTask($whereClauseArray);
        $whereClause.=$this->_getWhereClauseForTickets($whereClauseArray);
        if($type=="solved"){
          $whereClause=str_replace('created', 'closed', $whereClause);
          $whereClause.= " and ot.`flags`=0 ";
          $selectCond = ",DATE(ot.`closed`) AS date,sum(ot.`promised_duedate` > ot.`closed`) AS within";
          $orderCond = "DATE(ot.`closed`)";
        }
        else{
          $selectCond = ",DATE(ot.`created`) AS date";
          $orderCond = "DATE(ot.`created`)";
        }
        $sql = "SELECT 
                    count(*) AS task_count,
                    ost.`staff_id` AS staff_id
                    $selectCond
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_task ot ON ost.`ticket_id` = ot.`object_id`
                WHERE
                    ot.`dept_id` = ".$dept_id." $whereClause
                GROUP BY DATE(ot.`created`) , ost.`staff_id`
                ORDER BY $orderCond";
        $res = db_query($sql);
        return $res;
    }
	//todo:check whether this function is needed or not
    public function getSlaTaskDataSolved($dept_id,$whereClauseArray){
        $whereClause=$this->_getWhereClauseForTask($whereClauseArray);
        $whereClause=str_replace('created', 'closed', $whereClause);
        //var_dump($whereClause);die;
        $sql = "SELECT 
                    count(*) AS task_count,
                    ost.`staff_id` AS staff_id,
                    DATE(ot.`closed`) AS date,
                    sum(ot.`promised_duedate` > ot.`closed`) AS within
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_task ot ON ost.`ticket_id` = ot.`object_id`
                WHERE
                    ot.`dept_id` = ".$dept_id." and ot.`flags`=0 $whereClause
                GROUP BY DATE(ot.`closed`) , ost.`staff_id`";
        $res = db_query($sql);
        return $res;
    }

    public function getSlaTaskDataOpen($dept_id,$whereClauseArray){
        $whereClause=$this->_getWhereClauseForTickets($whereClauseArray);
        // $whereClause=str_replace('created', 'closed', $whereClause);
        $sql = "SELECT 
                    ot.`created` AS create_date,
                    ot.`closed` AS close_date,
                    ot.`promised_duedate` AS duedate,
                    ost.`staff_id` AS staff_id
                FROM
                    mst_ticket ost
                        INNER JOIN
                    mst_task ot ON ost.`ticket_id` = ot.`object_id`
                WHERE
                    ot.`dept_id` = ".$dept_id." $whereClause";
        $res = db_query($sql);
        return $res;
    }
//    public function getDumpDataForCcgCrg($dept_id,$whereClauseArray,$bucketNumber){
//        $whereClause=$this->_getWhereClauseForTicketsAndTask($whereClauseArray);
//        $joinCondition="";
//        if(isset($whereClauseArray['filtersForTriageData']) && 
//                count($whereClauseArray['filtersForTriageData'])>0){
//            $joinCondition=  $this->_getTriageDataWithTicketJoinCondition();
//        }
//        $offset = $bucketNumber * OPS_DATA_BUCKET_SIZE;
//        $sql = "SELECT 
//                    (CASE
//                        WHEN
//                            ot.`closed` is not null
//                        THEN
//                            TIMESTAMPDIFF(SECOND,
//                                ot.`created`,
//                                ot.`closed`)
//                        ELSE 0
//                    END) AS timediff,
//                    ost.`number` AS ticket_num,
//                    ost.`source_extra` AS source,
//                    otod.`ticket_channel` AS ticket_channel,
//                    ost.`created` AS created,
//                    otod.`order_id` AS order_no,
//                    otd.`title` AS task_name,
//                    ot.`id` AS task_id,
//                    ost.`topic_id` AS topic_id,
//                    ost.`staff_id` AS staff_id,
//                    ot.`created` AS task_creation,
//                    ot.`closed` AS task_closure,
//                    (CASE
//                        WHEN
//                            ot.`closed` is not null
//                                AND DATE(ot.`closed`) != '0000-00-00'
//                        THEN
//                            ot.`closed` < ot.`duedate`
//                        ELSE NOW() < ot.`duedate`
//                    END) AS sla,
//                    ot.`updated` AS task_updated,
//                    otod.`retrn_id` AS return_id,
//                    otd.`taskSource` AS task_source,
//                    otd.`taskStatus` AS task_status,
//                    otd.`taskResponse` AS task_response
//                    FROM
//                    mst_ticket ost
//                        INNER JOIN
//                    mst_ticket__odata otod ON ost.`ticket_id` = otod.`ticket_id`
//                        INNER JOIN
//                    mst_task ot ON ost.`ticket_id` = ot.`object_id`
//                        INNER JOIN
//                    mst_task__data otd ON ot.`id` = otd.`task_id` $joinCondition
//                    WHERE
//                    ot.`dept_id` = ".$dept_id." ".$whereClause." limit $offset,
//                    ".OPS_DATA_BUCKET_SIZE.";";
//        $res = db_query($sql);
//        $set=array();
//        while ($result = db_fetch_array($res)) {
//            $set[]=$result;
//        }
//        return $set;
//    }
    
    public function getSLAUpdatedTasksInfo($bucketNumber,$filtersApplied){
        $whereClause=$this->_getWhereClauseForTicketsAndTask($filtersApplied);
        $offset=$bucketNumber*SLA_REVISED_BUCKET_SIZE;
        $sql="SELECT 
                ot.`id` AS taskId,
                ot.`object_id` AS ticketId,
                ot.`updated` AS taskUpdated,
                ot.`duedate` AS `NewSlaGiven`,
                ot.`promised_duedate` AS `PromisedDueDate`,
                otd.`taskType`,
                otd.`title`,
                ot.`dept_id`,
                ot.created
              FROM
                mst_task ot 
                JOIN `mst_task__data` otd 
                  ON ot.`id` = otd.`task_id`  
              WHERE ot.`duedate` <> ot.`promised_duedate`  $whereClause 
              order by ot.id desc LIMIT $offset,".SLA_REVISED_BUCKET_SIZE." ;";
        
        $res = db_query($sql);
        $taskWiseReport=array();
          while ($result = db_fetch_array($res)) {
              $taskWiseReport[$result['taskId']]=$result;
          }
          return $taskWiseReport;
    }
    public function getOverallTicketDump($bucketNumber,$ccgStaffIDs,$whereClauseArray) {
        $offset=$bucketNumber*OVERALL_TICKET_BUCKET_SIZE;
        $defaultWhereCondition="and ost.created>=NOW()-INTERVAL ".OVERALL_REPORT_DEFAULT_TIME_PERIOD." MONTH ";
        $whereClause="";
        if(isset($whereClauseArray['filtersForTickets']['ticket__created__btw']) && 
                count($whereClauseArray['filtersForTickets']['ticket__created__btw'])>0){
            $defaultWhereCondition=""; 
        }else{
            $whereClause.=$defaultWhereCondition;
        }
        $whereClause.=$this->_getWhereClauseForTickets($whereClauseArray);
        $sql = "SELECT 
                ost.`number`,
                ost.ticket_id,
                ost.`source_extra`,
                oue.`address` AS customerEmailAddress,
		ost.mandate_info as mandateType,
                otod.`order_ids`,
		otod.`manifest_ids`,
	        otod.`product_ids`,
   		otod.`batch_ids`,
                ost.`created`,
                ost.`closed`,
                ost.`topic_id`,
                osttr.`priority`,
                ost.`staff_id`,
                ost.`agent_thread_count` AS communicationCountByAgent,
                osttr.`call_customer`,
                ost.`user_thread_count` AS communicationByCustomer 
              FROM
                `mst_ticket` ost 
                JOIN `mst_ticket__data` otod 
                  ON ost.`ticket_id` = otod.`ticket_id` 
                LEFT JOIN `mst_triage_data` osttr 
                  ON osttr.`ticket_id` = ost.`ticket_id` 
                  and osttr.call_customer=2 
                LEFT JOIN `mst_user_email` oue 
                  ON oue.`user_id` = ost.`user_id` 
                where 1 $whereClause 
                    group by ost.`ticket_id` 
                  order by ost.ticket_id desc 
                  
              LIMIT $offset, " . OVERALL_TICKET_BUCKET_SIZE;
//	echo $sql;die;
   $res = db_query($sql);
        $ticketData = array();
        $ticketsIDs=[];
        while ($result = db_fetch_array($res)) {
            $ticketData[$result['ticket_id']] = $result;
            $ticketsIDs[]=$result['ticket_id'];
        }
        $numberOfTasksPerTicket=$this->getNumberOfTasksInTicket($ticketsIDs);
        $numberOfAutoCommunicationPerTicket=  $this->getNumberOfAutoCommunicationPerTicket($ticketsIDs);
        $firstAndLastResponseDateAndTime=$this->getFirstResponseAndLatestUpdatePerTicket($ticketsIDs);
        $latestUpdateTimeByCCG=$this->getLatestUpdateDateTimeByCCG($ccgStaffIDs, $ticketsIDs);
        $assignedDate=  $this->getTicketAssignedDate($ticketsIDs);
        foreach ($ticketData as $ticketId=>$ticket){
            $ticketData[$ticketId]['Number Of Tasks']=$numberOfTasksPerTicket[$ticketId];
            $ticketData[$ticketId]['Number Of Auto communication sent']=$numberOfAutoCommunicationPerTicket[$ticketId];
            $ticketData[$ticketId]['First Response Time']=$firstAndLastResponseDateAndTime[$ticketId]['R'];
            $ticketData[$ticketId]['Latest Update By Customer']=$firstAndLastResponseDateAndTime[$ticketId]['M'];
            $ticketData[$ticketId]['Latest Update By CCG Agent Date And Time']=$latestUpdateTimeByCCG[$ticketId];
            $ticketData[$ticketId]['assignedDate']=$assignedDate[$ticketId];
            
        }
        return $ticketData;
        
    }
    
    public function getNumberOfTasksInTicket($ticketIds) {
        $ticketIds = array_map('intval', $ticketIds);
        if (count($ticketIds) == 0) {
            return array();
        }
        $ids = join("','", $ticketIds);
        $sql = "SELECT 
            ot.`id` AS taskId,
            ost.`ticket_id` ,
            COUNT(1) AS numberOfTasks
          FROM
            `mst_ticket` ost 
            JOIN `mst_task` ot 
              ON ost.`ticket_id` = ot.`object_id` 
              WHERE ost.ticket_id IN ('$ids')
          GROUP BY ost.`ticket_id` ;
";
        
        $res = db_query($sql);
        $numberOfTasksData = array();
        while ($result = db_fetch_array($res)) {
            $numberOfTasksData[$result['ticket_id']] = $result['numberOfTasks'];
        }
        return $numberOfTasksData;
    }
    public function getNumberOfAutoCommunicationPerTicket($ticketIds){
        $ticketIds = array_map('intval', $ticketIds);
        if (count($ticketIds) == 0) {
            return array();
        }
        $ids = join("','", $ticketIds);
        $sql="SELECT 
                ot.`object_id` AS ticketId,
                COUNT(1) AS numberOfAutoCommunicationSent 
              FROM
                mst_thread ot 
                JOIN `mst_thread_entry` ote 
                  ON ot.`id` = ote.`thread_id` 
              WHERE ote.`user_id` = 0 
                AND ote.`staff_id` = 0 
                AND ote.`poster` = 'SYSTEM' AND ot.object_id in ('$ids') AND ote.title is null 
              GROUP BY ot.`object_id` ;";
        $res = db_query($sql);
        $numberOfAutoCommunicationSent = array();
        while ($result = db_fetch_array($res)) {
            $numberOfAutoCommunicationSent[$result['ticketId']] = $result['numberOfAutoCommunicationSent'];
        }
        return $numberOfAutoCommunicationSent;
    }
    public function getFirstResponseAndLatestUpdatePerTicket($ticketIds){
        $ticketIds = array_map('intval', $ticketIds);
        if (count($ticketIds) == 0) {
            return array();
        }
        $ids = join("','", $ticketIds);
        $sql="SELECT 
            ot.`object_id` AS ticketId,
            ote.`type`,
            CASE
              ote.type 
              WHEN 'M' 
              THEN MAX(ote.`created`) 
              ELSE MIN(ote.`created`) END AS `requiredDate`
          FROM
            `mst_thread` ot 
            JOIN `mst_thread_entry` ote 
              ON ot.`id` = ote.`thread_id` 
          WHERE ote.`type` IN ('M', 'R') and ot.object_id in ('$ids')
          GROUP BY ot.`object_id`,
            ote.`type` ;
";
        $res = db_query($sql);
        $firstAndLastResponsePerTicket = array();
        while ($result = db_fetch_array($res)) {
            $firstAndLastResponsePerTicket[$result['ticketId']][$result['type']] = $result['requiredDate'];
        }
        return $firstAndLastResponsePerTicket;
    }
    
    public function getLatestUpdateDateTimeByCCG($staffIds,$ticketIds){
        $ticketIds = array_map('intval', $ticketIds);
        if (count($ticketIds) == 0) {
            return array();
        }
        $ids = join("','", $ticketIds);
        $staffIds = array_map('intval', $staffIds);
        if (count($staffIds) == 0) {
            return array();
        }
        $stafids = join("','", $staffIds);
        
        $sql="SELECT 
                MAX(ote.`created`) AS latestUpdateByCCG ,
                ot.object_id as ticketId
              FROM
                `mst_thread` ot 
                JOIN `mst_thread_entry` ote 
                  ON ot.`id` = ote.`thread_id` 
              WHERE ote.`type` = 'R' 
                AND ote.`staff_id` IN ('$stafids') 
                    and ot.object_id in ('$ids') 
              GROUP BY ot.`object_id` ;";
        $res = db_query($sql);
        $latestTimeUpdatedByCCG = array();
        while ($result = db_fetch_array($res)) {
            $latestTimeUpdatedByCCG[$result['ticketId']] = $result['latestUpdateByCCG'];
        }
        return $latestTimeUpdatedByCCG;
    }
    public function getTicketAssignedDate($ticketIds) {
        $ticketIds = array_map('intval', $ticketIds);
        if (count($ticketIds) == 0) {
            return array();
        }
        $ids = join("','", $ticketIds);
        $sql = "SELECT 
            ot.`object_id` AS ticketId,
            MIN(ot.`created`) AS assignedDate 
          FROM
            `mst_thread` ot 
            JOIN `mst_thread_event` ote 
              ON ot.`id` = ote.`thread_id` 
          WHERE ote.`state` = 'assigned' and ot.object_id in ('$ids') 
          GROUP BY ot.`object_id` ;";
        $res = db_query($sql);
        $ticketAssigned = array();
        while ($result = db_fetch_array($res)) {
            $ticketAssigned[$result['ticketId']] = $result['assignedDate'];
        }
        return $ticketAssigned;
    }
    public function getPendencySavedData($dateFilterApplied){
        $fromDate=$dateFilterApplied['threadCreateFromdate'];
        $toDate=$dateFilterApplied['threadCreateTodate'];
        $fromDateValid=  validateDate($fromDate);
        $toDateValid=  validateDate($toDate);
        $whereClause="";
        if($fromDate && $toDate){
            $whereClause="and date between '$fromDate' and '$toDate' ";
        }elseif($fromDateValid){
            $whereClause="and date between '$fromDate' and '9999-12-31'";
        }elseif($toDateValid){
            $whereClause="and date between '1000-01-01' and $toDate";
        }
        $whereClause.=" and status_id=1 ";
        $sql="select * from mst_pendency_report where 1 $whereClause";
        $rs=db_query($sql);
        $pendencyData=[];
        while ($row=  mysqli_fetch_array($rs)){
            $pendencyData[]=$row;
        }
        return $pendencyData;
    }
    
    public function getTicketsPendingUnTriagedAndTriageAgainPending(){
        $sql="SELECT 
                CASE
                  WHEN triage IS NULL 
                  THEN 1 
                  ELSE triage 
                END AS triage,
                COUNT(*) as numberOfEntries,
                SUM(
                  (
                    created < NOW() - INTERVAL 172800 SECOND 
                    AND (
                      (triage != 2 
                        AND triage != 3) 
                      OR triage IS NULL
                    )
                  ) 
                  OR (
                    triage_again < NOW() - INTERVAL 172800 SECOND 
                    AND triage = 3
                  )
                ) AS pending,
                SUM(
                  (
                    (
                      (
                        created > (NOW() - INTERVAL 172800 SECOND) 
                        AND created < (NOW() - INTERVAL 151200 SECOND)
                      )
                    ) 
                    AND (triage != 2 
                      OR triage IS NULL)
                  ) 
                  OR (
                    (
                      (
                        triage_again > (NOW() - INTERVAL 172800 SECOND) 
                        AND triage_again < (NOW() - INTERVAL 151200 SECOND)
                      )
                    ) 
                    AND triage = 3
                  )
                ) AS alert 
              FROM
                mst_ticket ticket 
                LEFT JOIN mst_triage_data triageTable 
                  ON (
                    ticket.ticket_id = triageTable.ticket_id
                  ) 
              WHERE 1 
                AND ticket.status_id IN ('1', '6') 
              GROUP BY triageTable.triage ;";
        $rs=db_query($sql);
        $triageAgainAndTriagePendingCount=[];
        while ($row=  mysqli_fetch_array($rs)){
            if($row['triage']==1){
                $triageAgainAndTriagePendingCount['triage_pending']=$row['numberOfEntries'];
            }
            if($row['triage']==3){
                $triageAgainAndTriagePendingCount['triage_again_pending']=$row['numberOfEntries'];
            }
        }
        return $triageAgainAndTriagePendingCount;
    }

}

?>
