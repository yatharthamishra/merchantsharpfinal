<?php
$staticTicketFields = getCachedTicketFields();
$from = strtotime($_GET["from"]);
$to = strtotime($_GET["to"]);

if($from==$to){
    $to = $from + 24*60*60;
}
echo "Report From ".date("Y-m-d H:i:s",$from)." to ".date("Y-m-d H:i:s",$to)."<br><br>";
$ticketsDataArr = Ticket::objects()->filter(array("created__gt"=>date("Y-m-d H:i:s",$from),"created__lt"=>date("Y-m-d H:i:s",$to)))->values("ticket_id","number","created","status_id","staff_id","topic_id","agent_thread_count","user_thread_count","source_extra")->distinct('ticket_id')->all();
foreach($ticketsDataArr as $ticket){
    $ticketArr[$ticket["ticket_id"]]["Ticket Number"] = $ticket["number"];
    $ticketArr[$ticket["ticket_id"]]["Created On"] = $ticket["created"];
    $ticketArr[$ticket["ticket_id"]]["Created On"] = $ticket["created"];
    $ticketArr[$ticket["ticket_id"]]["Source"] = $ticket["source_extra"];
    $ticketArr[$ticket["ticket_id"]]["Status"] = $staticTicketFields["status"][$ticket["status_id"]]?:"-";
    $ticketArr[$ticket["ticket_id"]]["Issue Type"] = $staticTicketFields["helpTopic"][$ticket["topic_id"]]?:"-";
    $ticketArr[$ticket["ticket_id"]]["Staff"] = $staticTicketFields["staff"][$ticket["staff_id"]]?:"-";
    $ticketArr[$ticket["ticket_id"]]["Customer Replies"] = $ticket["user_thread_count"];
    $ticketArr[$ticket["ticket_id"]]["Agent Replies"] = $ticket["agent_thread_count"];
    $ticketArr[$ticket["ticket_id"]]["First Task"]="-";
    $ticketIds[] = $ticket["ticket_id"];
}
if($ticketIds){
    $tasks = Task::objects()->filter(array("object_id__in"=>$ticketIds))->values('object_id','data__taskType')->all();
    foreach($tasks as $task){
        if(strlen($ticketArr[$task["object_id"]]["First Task"])<3)
        $ticketArr[$task["object_id"]]["First Task"]= explode(",",$task["data__taskType"])[1];
        
    }
}
    