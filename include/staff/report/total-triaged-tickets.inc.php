<?php
$staticTicketFields = getCachedTicketFields();
$from = strtotime($_GET["from"]);
$to = strtotime($_GET["to"]);

if($from==$to){
    $to = $from + 24*60*60;
}

echo "Report for ticket which are triaged From ".date("Y-m-d H:i:s",$from)." to ".date("Y-m-d H:i:s",$to)."<br><br>";
$triageArr = ThreadEntry::objects()->filter(array('body__contains'=>"<b>Priority Flag</b>","created__gt"=>date("Y-m-d H:i:s",$from),"created__lt"=>date("Y-m-d H:i:s",$to)))->values('id','thread__object_id','poster','created','title')->all();
foreach($triageArr as $triage){
    $triageArray[$triage["thread__object_id"]][$triage["id"]]["Staff"]= $triage["poster"];
    $triageArray[$triage["thread__object_id"]][$triage["id"]]["Triage Time"]= $triage["created"];
    $triageArray[$triage["thread__object_id"]][$triage["id"]]["Type"]= ($triage["title"]=="Triage field change")?"Triage Again":"Triage";
    $ticketIds[] = $triage["thread__object_id"];
}

$ticketsDataArr = Ticket::objects()->filter(array("triage__triage__in"=>array(2,3),'ticket_id__in'=>$ticketIds))->values("ticket_id","number","created","closed","status_id","staff_id","topic_id","agent_thread_count","user_thread_count","source_extra","triage__communication","triage__call_customer")->distinct('ticket_id')->all();
foreach($ticketsDataArr as $ticket){
    $tempTicketArr[$ticket["ticket_id"]]["Ticket Number"] = $ticket["number"];
    $tempTicketArr[$ticket["ticket_id"]]["Created On"] = $ticket["created"];
    $tempTicketArr[$ticket["ticket_id"]]["closed"] = $ticket["closed"];
    $tempTicketArr[$ticket["ticket_id"]]["Source"] = $ticket["source_extra"];
    $tempTicketArr[$ticket["ticket_id"]]["communication"] = ($ticket["triage__communication"]==2)?"Custom":"Normal";
    $tempTicketArr[$ticket["ticket_id"]]["call"] = ($ticket["triage__call_customer"]==2)?"Yes":"No";
    $tempTicketArr[$ticket["ticket_id"]]["Status"] = $staticTicketFields["status"][$ticket["status_id"]]?:"-";
    $tempTicketArr[$ticket["ticket_id"]]["Issue Type"] = $staticTicketFields["helpTopic"][$ticket["topic_id"]]?:"-";
    $tempTicketArr[$ticket["ticket_id"]]["Customer Replies"] = $ticket["user_thread_count"];
    $tempTicketArr[$ticket["ticket_id"]]["Agent Replies"] = $ticket["agent_thread_count"];
}
$i=0;
foreach($triageArray as $tid=>$data){
    foreach($data as $id=>$vData){
        foreach($vData as $k=>$v){
		if($tempTicketArr[$tid]){
         	   $ticketArr[$i][$k] = $v;
	}
        }
        foreach($tempTicketArr[$tid] as $k=>$v){
            $ticketArr[$i][$k] = $v;
        }
        $i++;
    }
    
}

    