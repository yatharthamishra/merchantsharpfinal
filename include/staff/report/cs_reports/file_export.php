<?php
    $fp = fopen("php://output", "w");

    $header = array();
    foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'] as $value => $dataMap) {
        $header[] = $value;
        $dataMapping[]=$dataMap;
    }
$prepareHeader=true;
if($flag=="ops_all" || $flag=="ove_slarev_all" || $flag=="tck_all" || $flag == "ccg_all" || $flag == "crg_all"){
    $prepareHeader=false;
}
    if($prepareHeader){
        fputcsv($fp, $header);
    }
    


if ($flag == "t_pend") {
        foreach ($tickets as $key => $value) {
            $total++;
            $dataInCsv = array($key,$value['triage_total'],$value['triage_again_total'],$value['old_ticket_triage'],$value['old_ticket_triage_again'],$value['triage_handled'],$value['triage_again_handled'],$value['triage_pending'],$value['triage_again_pending']);
            fputcsv($fp, $dataInCsv);
        }
    } elseif ($flag == 't_sla') {
        if ($reportSubSubtype == "agent level") {
            foreach ($tickets as $key => $value) {
                foreach ($value as $k => $val) {
                    unset($triage_handled_total);
                    unset($sla_total);
                    unset($sla_triage);
                    unset($sla_triage_again);
                    $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                    if (isset($val['triage_handled']) || isset($val['triage_again_handled']))
                        $sla_total = ($val['triage_handled_wsla']? : 0 + $val['triage_again_handled_wsla']? : 0) * 100 / ($val['triage_handled']? : 0 + $val['triage_again_handled']? : 0);

                    if (isset($val['triage_handled'])) {
                        $sla_triage = ($val['triage_handled_wsla']? : 0) * 100 / $val['triage_handled'];
                    }
                    if (isset($val['triage_again_handled'])) {
                        $sla_triage_again = ($val['triage_again_handled_wsla']? : 0) * 100 / $val['triage_again_handled'];
                    }

                    $dataInCsv = array($key, $staticFields['staff'][$k], $val['email'], $staticFields['staff'][$val['manager']], $val['triage_handled'], $val['triage_again_handled'], $triage_handled_total);
                    fputcsv($fp, $dataInCsv);
                }
            }
        } elseif ($reportSubSubtype == "issue type") {
            foreach ($tickets as $key => $value) {
                foreach ($value as $topic_id => $val) {
                    unset($triage_handled_total);
                    unset($sla_total);
                    unset($sla_triage);
                    unset($sla_triage_again);
                    $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                    $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                    if (isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count'])) {
                        $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                        $sla_total = ($val['triage']['wsla']? : 0 + $val['triage_again']['wsla']? : 0) * 100 / ($val['triage']['ticket_count']? : 0 + $val['triage_again']['ticket_count']? : 0);
                    }
                    if (isset($k['triage_handled'])) {
                        $sla_triage = ($val['triage']['wsla']? : 0) * 100 / $val['triage']['ticket_count'];
                    }
                    if (isset($val['triage_again']['ticket_count'])) {
                        $sla_triage_again = ($val['triage_again']['wsla']? : 0) * 100 / $val['triage_again']['ticket_count'];
                    }

                    $dataInCsv = array($key, $staticTicketFields["helpTopic"][$parent_parent_id], $staticFields['helpTopic'][$parent_id], $staticFields['helpTopic'][$topic_id], $val['triage']['ticket_count'], $val['triage_again']['ticket_count'], $triage_handled_total, $sla_triage, $sla_triage_again, $sla_total);
                    fputcsv($fp, $dataInCsv);
                }
            }
        }
    } elseif ($flag == 't_aht') {
        if ($reportSubSubtype == "agent level") {
            foreach ($tickets as $key => $value) {
                foreach ($value as $k => $val) {
                    unset($triage_handled_total);
                    if (isset($val['triage_handled']) || isset($val['triage_again_handled'])) {
                        $triage_handled_total = $val['triage_handled'] + $val['triage_again_handled'];
                    }
                    $aht_total = ($val['triage']['timediff'] + $val['triage_again']['timediff']) / ($val['triage']['no_for_aht'] + $val['triage_again']['no_for_aht']);
                    $dataInCsv = array($key, $staticFields['staff'][$k], $val['email'], $staticFields['staff'][$val['manager']], $val['triage_handled'], $val['triage_again_handled'], $triage_handled_total, round($val['triage_handled_aht'],2), round($val['triage_again_handled_aht'],2), round($aht_total,2));
                    fputcsv($fp, $dataInCsv);
                }
            }
        } elseif ($reportSubSubtype == "issue type") {
            foreach ($tickets as $key => $value) {
                foreach ($value as $topic_id => $val) {
                    unset($triage_handled_total);
                    $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
                    $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
                    if (isset($val['triage']['ticket_count']) || isset($val['triage_again']['ticket_count'])) {
                        $triage_handled_total = $val['triage']['ticket_count'] + $val['triage_again']['ticket_count'];
                    }
                    $aht_total = ($val['triage']['timediff'] + $val['triage_again']['timediff']) / ($val['triage']['no_for_aht'] + $val['triage_again']['no_for_aht']);
                    $dataInCsv = array($key, $staticTicketFields["helpTopic"][$parent_parent_id], $staticFields['helpTopic'][$parent_id], $staticFields['helpTopic'][$topic_id], $val['triage']['ticket_count'], $val['triage_again']['ticket_count'], $triage_handled_total, round($val['triage']['aht'],2), round($val['triage_again']['aht'],2), round($aht_total,2));
                    fputcsv($fp, $dataInCsv);
                }
            }
        }
    } elseif ($flag == "t_sla_summary") {
        $total=0;
        foreach ($tickets as $key => $value) {
            $total++;
            $sla_triage =  $value['triage_handled_wsla']*100/($value['triage_total_overall']-$value['triage_open_wsla']);
            $sla_triage_again = $value['triage_again_handled_wsla']*100/($value['triage_again_total_overall']-$value['triage_again_open_wsla']);
            $sla_total = ($value['triage_handled_wsla']+$value['triage_again_handled_wsla'])*100/($value['triage_total_overall']+$value['triage_again_total_overall']-$value['triage_open_wsla']-$value['triage_again_open_wsla']);
            $dataInCsv = array($key,$value['triage_total_overall'],$value['triage_again_total_overall'],$value['triage_handled_wsla'],$value['triage_again_handled_wsla'],$value['triage_open_wsla'],$value['triage_again_open_wsla'],round($sla_triage,2),round($sla_triage_again,2),round($sla_total,2));
            fputcsv($fp, $dataInCsv); 
            
        }
    } 
    elseif ($flag == "t_all") {

        $reportLib=new ReportLibrary();
        $reportLib->triage_dump($fp,$staticFields,$filterApplied);
    }
    elseif ($flag == "ccg_sla") {
        foreach ($tickets as $date => $value) {
            foreach ($value as $staff => $val) {
                $sla_adherence = ($val['task_solved_sla']*100/($val['total_tasks']-$val['task_open_sla']));
                $dataInCsv = array($date, $staticFields['staff'][$staff], $val['email'], $staticFields['staff'][$val['manager']], $val['total_tasks'], $val['task_solved_sla'], $val['task_solved_osla'], $val['task_open_sla'], $val['task_open_osla'], round($sla_adherence,2));
                fputcsv($fp, $dataInCsv);
            }
        }
    }
    elseif ($flag == "ccg_prod") {
        $total=0;
        foreach ($tickets as $date => $value) {
            foreach ($value as $staff => $val) {
                $total++;
                $dataInCsv = array($total,$date,$staticFields['staff'][$staff],$val['email'],$staticFields['staff'][$val['manager']],$val['email_task_handled'],$val['call_task_handled'],'-');
                fputcsv($fp, $dataInCsv);
            }
        }
    }
    elseif ($flag == "ccg_pend") {
        $total=0;
        foreach ($tickets as $date => $value) {
            foreach ($value as $staff => $val) {
                $total++;
                $dataInCsv = array($total,$date,$staticFields['staff'][$staff],$val['email'],$staticFields['staff'][$val['manager']],$val['email_task_created'],$val['call_task_created'],$val['email_task_created']+$val['call_task_created'],$val['email_task_handled'],$val['call_task_handled'],$val['email_task_handled']+$val['call_task_handled'],$val['email_task_pending'],$val['call_task_pending'],$val['email_task_pending']+$val['call_task_pending']);
                fputcsv($fp, $dataInCsv);
            }
        }
    }
    elseif ($flag == "ccg_all") {
        $reportLib=new ReportLibrary();
        $filterConfig['filterApplied']['taskDepartment'][0]=1;
        $headerFields = $fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'];
        $displayData=$reportLib->printRawOpsDataIntoExcel($fp,$staticFields,$filterConfig['filterApplied'],$headerFields);
//        foreach ($tickets as $num => $value) {
//            foreach ($value as $task_id => $val) {
//                $topic_id = $val['topic_id'];
//                $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
//                $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
//                $dataInCsv = array($num, $val['source'], $val['channel'], $val['created'], $val['order_no'], $val['task_name'], $task_id, $staticTicketFields["helpTopic"][$parent_parent_id], $staticFields['helpTopic'][$parent_id], $staticFields['helpTopic'][$topic_id], $staticFields['staff'][$val['staff_id']], $val['email'], $staticFields['staff'][$val['manager']], $val['task_creation'], $val['task_closure'], $val['task_updated'], '-', '-', $val['sla'], '-', round($val['aht'],2));
//                fputcsv($fp, $dataInCsv);
//            }
//        }
    }
    elseif ($flag == "crg_all") {
        $reportLib=new ReportLibrary();
        $filterConfig['filterApplied']['taskDepartment'][0]=14;
        $headerFields = $fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'];
        $displayData=$reportLib->printRawOpsDataIntoExcel($fp,$staticFields,$filterConfig['filterApplied'],$headerFields);
//        foreach ($tickets as $ticket_num => $value) {
//            foreach ($value as $task_id => $val) {
//                $topic_id = $val['topic_id'];
//                $parent_id = $staticFields['helpTopic']['parent_id'][$topic_id];
//                $parent_parent_id = $staticFields['helpTopic']['parent_id'][$parent_id];
//                $total++;
//                $dataInCsv = array($total,$ticket_num,$val['created'],$val['source'],$val['channel'],$staticTicketFields["helpTopic"][$parent_parent_id], $staticFields['helpTopic'][$parent_id], $staticFields['helpTopic'][$topic_id],'-',$val['return_id'],$val['order_no'],$val['task_name'],$task_id,$val['task_source'],$val['task_creation'],'FRESH','',$staticFields['staff'][$val['staff_id']], $val['email'], $staticFields['staff'][$val['manager']],'-',$val['task_closure'],$val['task_status'],$val['task_response'],$val['aht'],$val['sla']);
//                //todo :calculate no of times task updated => value after FRESH
//                fputcsv($fp, $dataInCsv);
//            }
//        }
    }
    elseif ($flag == "crg_sla") {
        foreach ($tickets as $date => $value) {
            foreach ($value as $staff => $val) {
                $dataInCsv = array($date, $staticFields['staff'][$staff], $val['email'], $staticFields['staff'][$val['manager']], $val['total_tasks'], $val['task_solved_sla'], $val['task_solved_osla'], $val['task_open_sla'], $val['task_open_osla'], '-');
                fputcsv($fp, $dataInCsv);
            }
        }
    }
    elseif ($flag == "crg_aht") {
        
    }
    elseif ($flag == "crg_prod") {
        
    }elseif($flag=="ops_all"){
           $reportLib=new ReportLibrary();
           $displayData=$reportLib->printRawOpsDataIntoExcel($fp,$staticFields,$filterConfig['filterApplied']);
          
    }elseif($flag=="ove_slarev_all"){
        
        $reportLib=new ReportLibrary();
        $reportLib->printSLARevisedEntriesTaskIntoExcel($fp,$staticFields,$filterConfig['filterApplied']);
    }elseif ($flag=="tck_all") {
        $reportLib=new ReportLibrary();
        $reportLib->printOverallTicketWiseData($fp,$staticFields,$filterConfig['filterApplied']);
        
    }elseif($flag=="ops_dept" ||
            $flag=="ops_task" || 
            $flag=="business_issue" ||
            $flag=="business_ticket" ||
            $flag=="business_prod"
            ){
        foreach ($displayData as $row) {
            $finalRowData=[];
        foreach ($fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['fields'] as $fieldName => $dataMap) {
            if(isset($row[$dataMap])){
                $finalRowData[]=$row[$dataMap];
            }else{
                $finalRowData[]='';
            }
        }
        fputcsv($fp, $finalRowData);
    }
}
    fclose($fp);
   
    //echo '<div id="exportTicketData" style="display:none">YES</div>';
?>