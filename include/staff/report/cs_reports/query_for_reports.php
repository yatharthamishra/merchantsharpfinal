<?php
$reportLib=new ReportLibrary();
$filterApplied=$filterConfig['filterApplied'];
global $reportRelatedConfig;
$calculateDataOnRuntime=$reportRelatedConfig['calculateData'];
switch ($reportType) {
    case 'triaging':
        switch ($reportSubtype) {
            case 'all':
                $flag = "t_all";
                //$displayData=$reportLib->triage_dump($tickets);
                break;
            case 'sla adherence':
                $flag = "t_sla";
                $tickets = array();
                switch ($reportSubSubtype) {
                    case 'agent level':
                        $reportLib->triage_sla_agent_level($tickets,$filterApplied);
                        break;
                    case 'issue type':
                        $reportLib->triage_sla_issue_type($tickets,$filterApplied);
                        break;
                    default:
                        break;
                }
                break;
            case 'aht':
                $flag = "t_aht";
                $tickets = array();
                switch ($reportSubSubtype) {
                    case 'agent level':
                        $reportLib->triage_aht_agent_level($tickets,$filterApplied);
                        break;
                    case 'issue type':
                        $reportLib->triage_aht_issue_type($tickets,$filterApplied);
                        break;
                    default:
                        break;
                }
                break;
            case 'pendency':
                $tickets = array();
                $flag = "t_pend";
                if($calculateDataOnRuntime){
                    $reportLib->triage_pendency($tickets,$filterApplied);
                }else{
                    $reportLib->getTriagePendencyData($tickets,$filterApplied);
                }
                
                break;
            case 'sla summary':
                $flag = "t_sla_summary";
                $tickets = array();
                if($calculateDataOnRuntime){
                    $reportLib->triage_sla_summary($tickets,$filterApplied);
                }else{
                    $reportLib->getTriageSlaSummaryData($tickets,$filterApplied);
                }
                break;
            default:
                break;
        }
        break;
    case 'ccg':
        $dept_id = 1;
        $tickets = array();
        //echo"<pre>";print_r($staffArray);die;
        switch ($reportSubtype) {
            case 'all':
                $flag = 'ccg_all';
                //$reportLib->ccg_crg_dump($tickets,$dept_id,$filterApplied);
                break;
            case 'sla adherence':
                $tickets = array();
                $flag = 'ccg_sla';
                $reportLib->ccg_crg_sla($tickets,$dept_id,$filterApplied);
                break;
            case 'productivity':
                $flag = 'ccg_prod';
                $reportLib->ccg_prod($tickets,$filterApplied);

                break;
            case 'pendency':
                $flag = 'ccg_pend';
                $reportLib->ccg_pendency($tickets,$filterApplied);
                break;
        }
        break;
    case 'crg':
        $dept_id = 14;
        $tickets = array();
        switch ($reportSubtype) {
            case 'all':
                $flag = 'crg_all';
                //$reportLib->ccg_crg_dump($tickets,$dept_id,$filterApplied);
                break;
            case 'sla adherence':
                $flag = 'crg_sla';
                $reportLib->ccg_crg_sla($tickets,$dept_id,$filterApplied);
                break;
            case 'aht':
                $flag = 'crg_aht';
                $reportLib->crg_aht($tickets,$filterApplied);
                break;
            case 'productivity':
                $flag = 'crg_prod';
                //todo : write this function
                $reportLib->crg_prod($tickets,$dept_id,$filterApplied);
                break;
        }
        break;
    case 'ops':
        switch ($reportSubtype) {
            case 'all':
                $flag = 'ops_all';
                break;
            case 'departmentwise sla':
                
                $reportLib=new ReportLibrary();
                $displayData=$reportLib->getDepartmentWiseTaskInfo($staticFields,$filterApplied);
                
                $flag = 'ops_dept';
                break;
            case 'taskwise sla':
                $reportLib=new ReportLibrary();
                $displayData=$reportLib->getTaskWiseSLAReport($staticFields,$filterApplied);
                
                $flag = 'ops_task';
                break;
        }
        break;
    case 'bus':
        switch ($reportSubtype) {
            case 'issue':
                $flag="business_issue";
                $reportLib=new ReportLibrary();
                
                $displayData=$reportLib->getIssueWiseSummaryReportData($staticFields,$filterApplied);
                
                # code...
                break;
            case 'ticket':
                $flag="business_ticket";
                $reportLib=new ReportLibrary();
                
                $displayData=$reportLib->getSourceWiseSumaryReportData($filterApplied);
                # code...
                break;
            case 'prod':
                $flag="business_prod";
                $reportLib=new ReportLibrary();
                $displayData=$reportLib->getProductivitySummaryReportData($staticFields,$filterApplied);
                //var_dump($displayData);exit;
                # code...
                break;
               case 'departmentwise sla':
                
                
                $reportLib=new ReportLibrary();
                $displayData=$reportLib->getDepartmentWiseTaskInfo($staticFields,$filterApplied);
                
                $flag = 'ops_dept';
                break;
            case 'taskwise sla':
  
                $reportLib=new ReportLibrary();
                $displayData=$reportLib->getTaskWiseSLAReport($staticFields,$filterApplied);
                
                $flag = 'ops_task';
                break;
            default:
                # code...
                break;
        }
        break;
            case 'ove':
                switch ($reportSubtype){
                case 'slarev':
                    $flag="ove_slarev_all";
                    break;
                case 'dep':
                $reportLib=new ReportLibrary();
                $displayData=$reportLib->getDepartmentWiseTaskInfo($staticFields,$filterApplied);
                $flag = 'ops_dept';
                    break;
                case 'task':
                                   $reportLib=new ReportLibrary();
                $displayData=$reportLib->getTaskWiseSLAReport($staticFields,$filterApplied);
                
                $flag = 'ops_task';
 
                    break;
                case 'issue':
                                   $flag="business_issue";
                $reportLib=new ReportLibrary();
                
                $displayData=$reportLib->getIssueWiseSummaryReportData($staticFields,$filterApplied);
 
                    break;
                case 'taskdump':
                    $flag="ops_all";
                    break;
                case 'ticketdump':
                    $flag="tck_all";
                    break;
                }
                break;
    default:
        # code...
        break;
}

?>