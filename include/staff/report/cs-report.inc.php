<?php

Class CSReports {

    public function __construct() {
        ;
    }

    private function _setGlobalVariables() {
        global $search;
        global $tickets;
        global $staticFields;
        global $getParam;
        global $appendInUrl;
        global $reportType;
        global $reportSubtype;
        global $reportSubSubtype;
        global $agentIDfilter;
        global $fromdate;
        global $todate;
        global $filterString;
        global $isExport;
        global $taskTypeDictionary;
        global $appendCsvBaseUrl;
        global $reportRelatedConfig;
        $reportConfigObject=new CustomConfig(array( 'report'));
        $config=$reportConfigObject->getConfigInfo();
        $reportRelatedConfig=$config['report'];
        define('TRIAGE_DATA_BUCKET_SIZE',$reportRelatedConfig['triagedatabucketsize']);//triagedatabucketsize
        define('OPS_DATA_BUCKET_SIZE',$reportRelatedConfig['opsdatabucketsize']);//opsdatabucketsize 
//define('CRG_CCG_DUMP_BUCKET_SIZE',1000);
        define("SLA_REVISED_BUCKET_SIZE",$reportRelatedConfig['slarevisedbucketsize']); //slarevisedbucketsize
        define("OVERALL_TICKET_BUCKET_SIZE",$reportRelatedConfig['oveticketbucketsize']);//oveticketbucketsize
        define("CCG_DEPARTMENT_ID",$reportRelatedConfig['ccgdeptId']);//ccgdeptId
        define("OVERALL_TICKET_DUMP_TIMEOUT", $reportRelatedConfig['oveticketdumptimeout']);//oveticketdumptimeout
        //define("CRG_CCG_DUMP_TIMEOUT", 3600);
        define("RAW_OPS_DUMP_TIMEOUT", $reportRelatedConfig['rawopsdumptimeout']);//rawopsdumptimeout
        define("SLA_REV_DUMP_TIMEOUT", $reportRelatedConfig['slarevdumptimeout']);//slarevdumptimeout
        define("OVERALL_REPORT_DEFAULT_TIME_PERIOD", $reportRelatedConfig['overeportdefaulttimeperiod']);//overeportdefaulttimeperiod
        define("REPORT_CSV_SAVE_DIR",$reportRelatedConfig['reportsavecsvdir']);//reportsavecsvdir
        $appendCsvBaseUrl="/scp/downloadReportCsv.php";
        $cachedTaskFields=getCachedTaskFields();
        $taskTypeDictionary=$cachedTaskFields['Default_Task Types'];
        $search = SavedSearch::create();
        $tickets = Ticket::objects();
        $staticFields = getCachedTicketFields();
        $getParam = explode('?', $_SERVER['REQUEST_URI']);
        $currentRelativeUrl=$_SERVER['REQUEST_URI'];
        if (!empty($getParam[1])) {
            $appendInUrl = '/scp/downloadReportCsv.php?' . $getParam[1] . '&';
        }
// this file contains array of all fields and filters for report 
//These parameters determine which report to display
        $reportType = $_GET['type']? : 'triaging';
        $reportSubtype = $_GET['subtype']? : 'sla adherence';
        $reportSubSubtype = $_GET['subsubtype']? : 0;

        if (!$_GET['subtype'])
            $reportSubSubtype = 'agent level';
        $agentIDfilter = $_GET['Agent_ID'] ? 'and B.staff_id=' . $_GET['Agent_ID'] : '';

        $fromdate = date('Y-m-d', strtotime($_GET['from']));
        $todate = date('Y-m-d', $_GET['to'] ? strtotime($_GET['to']) : time());

        $filterString = "and DATE(B.created) >='" . $fromdate . "' and DATE(B.created)<='" . $todate . "'";
        $isExport = FALSE;
        if ($_GET['EXPORT'] == 'exportTrue') {
            $isExport = TRUE;
        }
        global $reportFilterConfig;
        global $reportFields;
        global $reportFieldNotToBeDisplayed;
        global $reportFilterDictionary;
        $filterConfig=$reportFilterConfig;
        $fields=$reportFields;
        $fieldNotToBeDisplayed=$reportFieldNotToBeDisplayed;
        $filterDictionary=$reportFilterDictionary;
        
        include REPORT_LIBRARY_PATH . "ReportLibrary.php";
        include REPORT_FILTERS_PATH . 'FilterData.php';
        include REPORT_FILTERS_PATH . 'FilterRequest.php';
        $this->updateFilterConfigVariable($fields,$filterConfig);
        include "cs_reports/query_for_reports.php";
        
       
        if ($isExport) {
            include "cs_reports/file_export.php";
        }
         //var_dump($filterConfig);exit;

       
        include "view/display_cs_reports.php";
    }

    public function updateFilterConfigVariable($fields,&$filterConfig) {

        /**
         * These lines update the filter config with the data according to the filter report type 
         * Updates $filterConfig['filterDictionary'] varible according to the request .
         */
        $filterDataObject = new FilterData();
        $filterDataObject->generateFilterData($fields,$filterConfig); // updates filter config data with data of filter required to render the same
        $filterRequestObject = new FilterRequest();
        $filterRequestObject->generateActiveAndUpdateSelectedFilters($filterConfig); // updates filter config with currently selected value of 
// the filter
       // var_dump($filterConfig);exit;
    }

    public function main() {
        
        $this->_setGlobalVariables();
        
    }

}

$csReport = new CSReports();
$csReport->main();