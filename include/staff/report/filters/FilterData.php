<?php

/**
 * 
 */
class FilterData {

    private $staticFields;
    private $model;

    public function __construct() {
        global $staticFields;
        $this->staticFields = $staticFields;

        $this->model = new ExcelModel();

        ;
    }

    /**
     * Used to populate filterConfigFileWithData On Demand
     * @return type
     */
    public function generateFilterData($fields, &$filterConfig) {
        global $reportType;
        global $reportSubtype;
        global $reportSubSubtype;
        $filtersApplicable = $fields['data'][$reportType][$reportSubtype][$reportSubSubtype]['filters'];

        foreach ($filtersApplicable as $key => $filter) {
            switch ($filter['filter']) {
                case 'priority':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getPriority();
                    break;
                case 'issueType':
                case 'issueSubType':
                case 'issueSubSubType':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getTypeSubTypeSubSubTypeMapping();
                    break;
                case 'agent':
                case 'teamLeader':
                case 'threadAgent':
                case 'threadTeamLeader':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] =
                            $this->_getAgentAndTeamLeadInfo($filter['filter']);
                    break;
                case 'department':
                case 'taskDepartment':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getDepartmentInfo();
                    break;
                case 'taskType':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getTaskType();
                    break;
                case 'taskStatus':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getTaskStatus();
                    break;
                case 'slaStatus':
                case 'taskslaStatus':
                case 'ticketSlaStatus':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getSLaStatusValues();
                    break;
                case 'date':
                    break;
                case 'taskName':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getTaskNamesInfo();
                    break;
                case 'triageType':
                    $filterConfig['filterDictionary'][$filter['filter']]['data'] = $this->_getTriageType();
                    break;
            }
        }
    }

    private function _getTaskType() {
        $cachedTaskFields = getCachedTaskFields();
        $requiredTaskTypeForm=array();
        
        foreach ($cachedTaskFields['Default_Task Types'] as $taskId=>$taskName){
            $taskName=trim($taskName);
            $valueToBeUsed="$taskId,$taskName";
            $requiredTaskTypeForm[$valueToBeUsed]=$taskName;
        }
        return $requiredTaskTypeForm;
    }

    private function _getSLaStatusValues() {
            return array(0 => 'OSLA', 1 => 'Within SLA');
    }

    private function _getPriority() {
	return [];
        $typeList = DynamicList::objects()->filter(array("name" => "Priority Flags"));
        foreach ($typeList as $list) {
            foreach ($list->items as $items) {
                $priorityData[$items->extra] = $items->value;
            }
        }
        return $priorityData;
    }

    private function _getTypeSubTypeSubSubTypeMapping() {
        global $staticFields;
        $issueSubIssueMapping=getIssueSubIssueInHeirachalForm($staticFields);
  
        return $issueSubIssueMapping;
    }

    private function _getAgentAndTeamLeadInfo($entityType) {

        switch ($entityType) {
            case 'threadTeamLeader':
            case 'teamLeader':
                $teamLead = [];
                $managerIds = $this->model->getDistinctManagerId();

                foreach ($managerIds as $managerId) {
                    if (isset($this->staticFields['staff'][$managerId])) {
                        $teamLead[$managerId] = $this->staticFields['staff'][$managerId];
                    }
                }
                asort($teamLead);
                return $teamLead;

            case 'threadAgent':
            case 'agent':
                asort($this->staticFields['staff']);
                return $this->staticFields['staff'];
        }
        return array();
    }

    private function _getDepartmentInfo() {
        $departmentData = array();
        foreach ($this->staticFields['dept'] as $departmentId => $departmentName) {
            $departmentData[$departmentId] = $departmentName;
        }
        return $departmentData;
    }

    private function _getTaskStatus() {
        return array(0 => 'Open', 1 => 'Closed');
    }

    private function _getTaskNamesInfo() {
        $cachedTaskFields = getCachedTaskFields();
        $taskTypeDictionary = $cachedTaskFields['Default_Task Types'];
        return $taskTypeDictionary;
    }

    private function _getTriageType(){
        return array(1 => 'Not Triage', 3 => 'Triage Again');
    }

}

?>
