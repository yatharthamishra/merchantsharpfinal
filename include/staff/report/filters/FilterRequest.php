<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilterRequest
 *
 * @author shopclues
 */
class FilterRequest {

    /**
     * Get Active Filters in the page
     * Updates selectedVal in config if filter is activated
     * @return type
     */
    public function generateActiveAndUpdateSelectedFilters(&$filterConfig) {
        
        foreach ($_GET as $key => $value) {
            if (isset($filterConfig['filterRequestMap'][$key])) {
                $filterConfig['filterDictionary'][$filterConfig['filterRequestMap'][$key]]['selectedValue'] = $value;
                $filterConfig['filterApplied'][$filterConfig['filterRequestMap'][$key]]=$value;
            }
            
        }
       
        
    }
    

}

?>
