<?php
$staticTicketFields = getCachedTicketFields();
$from = strtotime($_GET["from"]);
$to = strtotime($_GET["to"]);

if($from==$to){
    $to = $from + 24*60*60;
}
echo "Report for Repeated task SLA From ".date("Y-m-d H:i:s",$from)." to ".date("Y-m-d H:i:s",$to)."<br><br>";
$SLArr = TaskThreadEntry::objects()->filter(array("title"=>"SLA Updated","created__gt"=>date("Y-m-d H:i:s",$from),"created__lt"=>date("Y-m-d H:i:s",$to)))->values('thread__object_id','poster','body','created')->all();
foreach($SLArr as $sla){
    $date = explode("from ",$sla['body'])[1];
    $taskIds[] = $sla["thread__object_id"];
}
$NewSLArr = TaskThreadEntry::objects()->filter(array("title"=>"SLA Updated",'thread__object_id__in'=>$taskIds))->values('thread__object_id','poster','body','created')->all();
foreach($NewSLArr as $sla){
    $date = explode("to ",$sla['body'])[1];
    $taskArrAdd[$sla["thread__object_id"]]["Staff"]= $taskArrAdd[$sla["thread__object_id"]]["Staff"].$sla["poster"]."<br>";
    $taskArrAdd[$sla["thread__object_id"]]["Update on"]= $taskArrAdd[$sla["thread__object_id"]]["Update on"].$sla["created"]."<br>";
    $taskArrAdd[$sla["thread__object_id"]]["New SLA"]= $taskArrAdd[$sla["thread__object_id"]]["New SLA"].$date."<br>";
    $taskArrAdd[$sla["thread__object_id"]]["count"]++;
    $taskArrCount[$sla["thread__object_id"]]["count"]++;
    if($taskArrCount[$sla["thread__object_id"]]["count"]>1)
        $finalTaskIds[] = $sla["thread__object_id"];
}
$ticketsTaskArr = Task::objects()->filter(array('id__in'=>$finalTaskIds))->values("id","object_id","promised_duedate","data__taskType","dept_id")->all();
foreach($ticketsTaskArr as $task){
    $taskArrAdd[$task['id']]["Promised SLA"] = $task['promised_duedate'];
    $taskArrAdd[$task['id']]["Department"] = $staticTicketFields["department"][$task['dept_id']]["name"];
    $taskArrAdd[$task['id']]["Task"] = explode(",",$task['data__taskType'])[1];
    $ticketIds[] = $task['object_id'];
    $ticketTaskMap[$task['id']] = $task['object_id'];
}

foreach($taskArrAdd as $key=>$info){
    if($ticketTaskMap[$key]){
        $ticketArr[$key]["Ticket Number"] = $ticketTaskMap[$key];
        $ticketArr[$key]["Task Id"] = $key;
        foreach($info as $k=>$v){
            $ticketArr[$key][$k]=$v;
        }
    }
}
    