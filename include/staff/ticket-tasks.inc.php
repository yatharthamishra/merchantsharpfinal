<?php
global $thisstaff;

$role = $thisstaff->getRole($ticket->getDeptId());

$tasks = Task::objects()
    ->select_related('dept', 'staff', 'team')
    ->order_by('-created');

$tasks->filter(array(
            'object_id' => $ticket->getId(),
            'object_type' => 'T'));

$count = $tasks->count();
$pageNav = new Pagenate($count,1, 100000); //TODO: support ajax based pages
$showing = $pageNav->showing().' '._N('task', 'tasks', $count);

$staticTaskFields = getCachedTaskFields();
$taskStatus = $staticTaskFields['Task Status'];
$taskStatusId = array_flip($staticTaskFields['id_extra_Task Status']);
$taskStateMachine = array();
foreach ($staticTaskFields['Prop_Task Status'] as $key => $value) {
    $taskStateMachine[$key] = explode(",",array_values(json_decode($value,true))[0]);
} 
$taskResponseMachine = array();
    foreach ($staticTaskFields['Prop_Task Types'] as $key => $value) {
        $taskResponseMachine[$key] = array_values(json_decode($value,true))[0];
    }  

$TaskResponseListId = $staticTaskFields['List_Task Response'];

?>
<div id="tasks_content" style="display:block;" class="tasks_content">
<div class="pull-left showing_task">
   <?php
    if ($count) {
        echo '<strong>'.$showing.'</strong>';
    } else {
        echo sprintf(__('%s does not have any tasks'), $ticket? 'This ticket' :
                'System');
    }
   ?>
</div>
<div class="pull-right button_bar">
    <?php
    if ($role && $role->hasPerm(Task::PERM_CREATE)) { ?>
        <a
        class="green button action-button ticket-task-action"
        data-url="tickets.php?id=<?php echo $ticket->getId(); ?>#tasks"
        data-dialog-config='{"size":"large"}'
        href="#tickets/<?php
            echo $ticket->getId(); ?>/add-task">
            <i class="icon-plus-sign"></i> <?php
            print __('Add New Task'); ?></a>
    <?php
    }
    if ($count)
        Task::getAgentActions($thisstaff, array('morelabel' => __('Options')));
    ?>
</div>
<div class="clear"></div>
<div>
<?php
if ($count) { ?>
<form action="#tickets/<?php echo $ticket->getId(); ?>/tasks" method="POST"
    name='tasks' id="tasks" style="padding-top:7px;">
<?php csrf_token(); ?>
 <input type="hidden" name="a" value="mass_process" >
 <input type="hidden" name="do" id="action" value="" >
 <table class="list" border="0" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <?php
            if (1) {?>
            <th width="8px">&nbsp;</th>
            <?php
            } ?>
            <th width="70"><?php echo __('Number'); ?></th>
            <th width="100"><?php echo __('Date'); ?></th>
            <th width="100"><?php echo __('Type'); ?></th>
            <th width="200"><?php echo __('Status'); ?></th>
            <th width="100"><?php echo __('Response'); ?></th>
            <th width="200"><?php echo __('Title'); ?></th>
            <th width="200"><?php echo __('Department'); ?></th>
            <th width="100"><?php echo __('Assignee'); ?></th>
        </tr>
    </thead>
    <tbody class="tasks">
    <?php
    foreach($tasks as $task) {
        $id = $task->getId();
        $assigned='';
        if ($task->staff)
            $assigned=sprintf('<span class="Icon staffAssigned">%s</span>',
                    Format::truncate($task->staff->getName(),40));

        $currentStatus = $task->getTaskStatus()?  array_values(json_decode($task->getTaskStatus()->value,true))[0]:"New";
        $status=$currentStatus;
        $status .= "<br>(<b>";
        $status .= $task->isOpen() ? '<strong>open</strong>': 'closed';
        $status .= "</b>)";

        $type = $task->getTaskType();
        $staticTaskData=  getCachedTaskFields();
            $resp_ID = $task->getTaskResponse();
        $response=$staticTaskData['Default_Task Response'][$resp_ID];
      //  $response = $task->getTaskResponse();
        
        $title = Format::htmlchars(Format::truncate($task->getTitle(),40));
        $threadcount = $task->getThread() ?
            $task->getThread()->getNumEntries() : 0;

        $viewhref = sprintf('#tickets/%d/tasks/%d/view',
                $ticket->getId(), $id);

         $cond = $thisstaff->canAccessDept($task->getDeptId());
        ?>
        <tr id="<?php echo $id; ?>"  class="ticketCheckbox">
            <td align="center" class="nohover">
                <input class="ckb" type="checkbox" name="tids[]"
                value="<?php echo $id; ?>" <?php echo $sel?'checked="checked"':''; ?>>
            </td>
            <td align="center" nowrap>
              <a class="Icon no-pjax preview"
                title="<?php echo __('Preview Task'); ?>"
                href="<?php echo $viewhref; ?>"
                data-preview="#tasks/<?php echo $id; ?>/preview"
                ><?php echo $task->getNumber(); ?></a></td>
            <td align="center" nowrap><?php echo
            date("d-m-Y",strtotime($task->created)); ?></td>
            <td><?php echo $type; ?></td>
            <td class='existing-info-box'><div class='current-info'><?php echo $status; ?></div> 
                <?php if(is_array($taskStateMachine["".$currentStatus.""]) && count(array_filter($taskStateMachine["".$currentStatus.""]))>0){ ?>
                    <select class='task-status-selector change-selector' taskId="<?php echo $id; ?>" change="status" style='display:none;' width='100px'>
                            <option >select</option>
                    <?php foreach($taskStateMachine["".$currentStatus.""] as $s){ ?>
                            <option value='<?php echo $TaskStatusObj->id."-".$taskStatusId[$s]."-".$taskStatus[$s]."-".count(array_filter($taskStateMachine[$taskStatus[$s]]));?>'><?php echo $taskStatus[$s];?></option>

                    <?php } ?>
                    </select>
            <?php } ?>
            </td>
            <td class='existing-info-box'><div class='current-info'><?php echo $response; ?></div> 
                <?php   if(is_array($taskResponseMachine["".$type.""]) && count(array_filter($taskResponseMachine["".$type.""]))>0){ ?>
                <select class='task-status-selector change-selector' taskId="<?php echo $id; ?>" change="response" style='display:none;' width='100px'>
                            <option >select</option>
                    <?php foreach($taskResponseMachine["".$type.""] as $r=>$v){ ?>
                            <option value='<?php echo $TaskResponseListId."-".$r."-".$v;?>'><?php echo $v;?></option>

                    <?php } ?>
                    </select>
                    <?php } ?>
            </td>
            <td><a <?php if ($flag) { ?> class="no-pjax"
                    title="<?php echo ucfirst($flag); ?> Task" <?php } ?>
                    href="<?php echo $viewhref; ?>"><?php
                echo $title; ?></a>
                 <?php
                    if ($threadcount>1)
                        echo "<small>($threadcount)</small>&nbsp;".'<i
                            class="icon-fixed-width icon-comments-alt"></i>&nbsp;';
                    if ($row['collaborators'])
                        echo '<i class="icon-fixed-width icon-group faded"></i>&nbsp;';
                    if ($row['attachments'])
                        echo '<i class="icon-fixed-width icon-paperclip"></i>&nbsp;';
                ?>
            </td>
            <td class='existing-info-box'><div class='current-info'><?php echo Format::truncate($task->dept->getFullName() , 40); ?></div>
                <?php if($cond){?>
            <select class='task-department-selector change-selector' taskId="<?php echo $id; ?>" change="Dept" style='display:none;' width='100px'>
                    <option >select</option>
            <?php foreach(Dept::objects() as $d){ ?>
                    <option value='<?php echo $d->getId()."-".$d->getFullName();?>'><?php echo $d->getFullName();?></option>
                
            <?php } ?>
            </select>
             <?php }?></td>
            <td>&nbsp;<?php echo $assigned; ?></td>
        </tr>
   <?php
    }
    ?>
    </tbody>
</table>
</form>
<?php
 } ?>
</div>
</div>
<div id="task_content" style="display:none;">
</div>
<script type="text/javascript">
$(function() {
    $(".ticketCheckbox td.nohover").on('click',function(){
    $(this).children("input.ckb").click();
});
    $(".change-selector").on('change',function(){ 
        var change = $(this).attr("change");
        var taskId = $(this).attr("taskId");
        var valueOfField = $(this).val();
        
        if(change=="Dept"){
            valueOfField = valueOfField.split("-");
            var data = {"taskId":taskId,"change":change,"valueOfField":valueOfField[0]};
            valueOfField = valueOfField[1];
        }else{
            valueOfField = valueOfField.split("-");
            var FieldId = valueOfField[0];
            var nameOfField = valueOfField[1];
            var moreInfo="open";
            if(change=="status")
                moreInfo = valueOfField[3];
            
            valueOfField = valueOfField[2];
            if(moreInfo=="0"){
                moreInfo="close";
            }
            var data = {"taskId":taskId,"change":change,"valueOfField":valueOfField,"FieldId":FieldId,"nameOfField":nameOfField,"moreInfo":moreInfo};
        
        }
        var selectFieldSelector = this;
        
        if(valueOfField){
            var url = 'ajax.php/tasks/changeField';
            $.ajax({
                type: "POST",
                url: url, 
                data: data,
                cache: false,
                success: function(resp)
                {
                    if(resp==1){
                     $(selectFieldSelector).parent().children(".current-info").show().text(valueOfField);
                     $(selectFieldSelector).hide();

                    }else{
                        alert("error");
                    }
                }
            });
            
        }
    });
    $(".existing-info-box").on('click',function(){
        if($(this).children(".change-selector").length>0){
            $(this).removeClass("existing-info-box").addClass("existing-info-box-select").off('click');
            $(this).children(".change-selector").show();
            $(this).children(".current-info").hide();
        }
    });

    $(document).off('click.taskv');
    $(document).on('click.taskv', 'tbody.tasks a, a#reload-task', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var url = 'ajax.php/'+$(this).attr('href').substr(1);
        var $container = $('div#task_content');
        var $stop = $('ul#ticket_tabs').offset().top;
        $.pjax({url: url, container: $container, push: false, scrollTo: $stop})
        .done(
            function() {
            $container.show();
            $('.tip_box').remove();
            $('div#tasks_content').hide();
            });

        return false;
     });
    // Ticket Tasks
    $(document).off('.ticket-task-action');
    $(document).on('click.ticket-task-action', 'a.ticket-task-action', function(e) {
        e.preventDefault();
        var url = 'ajax.php/'
        +$(this).attr('href').substr(1)
        +'?_uid='+new Date().getTime();
        var $redirect = $(this).data('href');
        var $options = $(this).data('dialogConfig');
        $.dialog(url, [201], function (xhr) {
            var tid = parseInt(xhr.responseText);
            if (tid) {
                var url = 'ajax.php/tickets/'+<?php echo $ticket->getId();
                ?>+'/tasks/'+tid+'/view';
                var $container = $('div#task_content');
                $container.load(url, function () {
                    $('.tip_box').remove();
                    $('div#tasks_content').hide();
                }).show();
            } else {
                window.location.href = $redirect ? $redirect : window.location.href;
            }
        }, $options);
        return false;
    });
});
</script>
