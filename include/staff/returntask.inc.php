<style>
    td{
        vertical-align: middle !important;
        border:none !important;
        padding:0px;
    }
    table.borderInner{
        padding:0px;
        border-collapse: collapse;
        border-spacing:0;

    }
    .innerExpand{
        height: 450px;
        margin:0px auto;
        overflow: scroll;
    }
    table.list tbody td{
        padding:0px 0px;
    }
    td.rightborder,th.rightborder{
        width:130px;
        max-width: 130px;
        min-width: 130px !important;
    }
    th.rightborder{
        min-width: 0px;
    }
    .fixwidth100{
        max-width: 100px;
        min-width: 100px;
    }
    .fixwidth50{
        max-width: 50px;
        min-width: 50px;
    }
    table{padding: 0px;}
    .item{
        box-shadow: 0px 0px 3px 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: inline-block; padding: 4px; border-radius: 16px; cursor: pointer;
        overflow: hidden;margin:2px;

    }
    table.list tbody tr:nth-child(2n+1) td {
        all: revert;
    }
    table.innerTable tbody tr:nth-child(even) td {
        background-color: lightyellow;
    }
    .issue{
        background: lightseagreen !important;
        padding: 3px !important;
        color: rgb(255, 255, 255) !important;
    }
    table.subissue tbody tr.mainrow:nth-child(odd) td {
        background: #eee;
    }

    table.subinner tbody tr:nth-child(even) td {
        background: lightyellow !important;
    }
    table td.head{
        background: #20B2AA;
        color: rgb(255, 255, 255);
    }
    .buttoncolor{
        background-color: #2EC22E !important;
        color: white !important;
        border-radius: 50px !important;
    }
    .individual{
        display:none;
        position: fixed;
        width: 80%;
        top: 100px;
        background: white none repeat scroll 0% 0%;
        padding: 13px;
        z-index: 10000;
        left: 10%;
        box-shadow:0px 0px 215px 50px #000;
        min-height:100px;
    }
    .individual{
        padding: 5px !important;
    }
    .individualclose{bottom: 5px;
                     position: absolute;}
    .taskTypeChangeNoDesign .select2-selection,.select2-selection span{
        background-color: transparent !important;
        border: 0px solid #aaa !important;
        border-radius: 0px !important;
        white-space: inherit !important;
        line-height: 15px !important;
    }
    .taskTypeChangeNoDesign .select2-selection .select2-selection__arrow{
        display:none !important;
    }

</style>

<table class="listed" width="100%";>
    <form name="returnTaskFlow" action="returntask.php" method="POST">
        <thead>
            <tr><th colspan="8"><?php echo __('Return Task Assignment - First Task creation'); ?></th></tr>
        </thead>
        <tbody>
            <tr>

                <th class='issue'><?php echo __('Issue'); ?></th>
                <td class='head'><table><tr>
                            <th class='fixwidth100'><?php echo __('Sub Issue'); ?></th>
                            <th class='fixwidth100'><?php echo __('Sub Sub Issue'); ?></th>
                            <th class='fixwidth100'><?php echo __('Merchant Type'); ?></th>
                            <th class='rightborder'><?php echo __('category'); ?></th>
                            <th class='rightborder'><?php echo __('Return Status'); ?></th>
                            <th class='rightborder'><?php echo __('Value'); ?></th>
                            <th class='rightborder'><?php echo __('Sub Reason'); ?></th>
                            <th class='rightborder'><?php echo __('Return Action'); ?></th>
                            <th class='fixwidth100' style='background: #060; color:#fff;'><?php echo __('Department'); ?></th>
                            <th class='fixwidth100' style='background: #060; color:#fff;'><?php echo __('Task Type'); ?></th>
                            <th class='fixwidth100' ><?php echo __('Update'); ?></th>
                        </tr></table>    </td>            
            </tr>
        </tbody>
        <?php
        global $cfg;
        $staticFields = getCachedTicketFields();
        $taskStaticFields = getCachedTaskFields();
        $returnStaticFields = getCachedReturnFlowFields();
        $flowMapFields = getFlowMapFields();
        $returnTaskAssign = ReturnTaskAssign::objects();
        foreach ($returnTaskAssign->all() as $flow) {
            $taskIdArr[] = $flow->flow_id;
            $flowCombinedArr[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type][$flow->merchant_type][$flow->category][$flow->return_status? : 0][$flow->value_min . "-" . $flow->value_max][$flow->sub_reason][$flow->return_action]["merchant_type"][] = $flowMapFields["merchant_type"][$flow->merchant_type];
            $flowCombinedArr[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type][$flow->merchant_type][$flow->category][$flow->return_status? : 0][$flow->value_min . "-" . $flow->value_max][$flow->sub_reason][$flow->return_action]["category"][] = $flowMapFields["category"][$flow->category];
            $flowCombinedArr[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type][$flow->merchant_type][$flow->category][$flow->return_status? : 0][$flow->value_min . "-" . $flow->value_max][$flow->sub_reason][$flow->return_action]["returnStatus"][] = $staticFields["orderStatus"]["Return"][$flow->return_status? : 0]? : "Any";
            $flowCombinedArr[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type][$flow->merchant_type][$flow->category][$flow->return_status? : 0][$flow->value_min . "-" . $flow->value_max][$flow->sub_reason][$flow->return_action]["value_range"][] = $flowMapFields["valueRange"][$flow->value_min . "-" . $flow->value_max];
            $flowCombinedArr[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type][$flow->merchant_type][$flow->category][$flow->return_status? : 0][$flow->value_min . "-" . $flow->value_max][$flow->sub_reason][$flow->return_action]["sub_reason"][] = $returnStaticFields["sub_reason"][$flow->sub_reason]? : "Any";
            $flowCombinedArr[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type][$flow->merchant_type][$flow->category][$flow->return_status? : 0][$flow->value_min . "-" . $flow->value_max][$flow->sub_reason][$flow->return_action]["return_action"][] = $returnStaticFields["return_action"][$flow->return_action];
            
            $flowCombinedArr2[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type]["flowId"][] = $flow->flow_id;
            $flowCombinedArr2[$flow->issue_id][$flow->sub_issue_id][$flow->sub_sub_issue_id][$flow->dept][$flow->task_type]["flow"][] = $flow;
            $flowArr[] = $flow;
        }
        foreach ($flowCombinedArr as $flowIssueId => $flowIssue) { ?>
            <tbody>
                <tr>
                    <td class='issue'><?php echo $flowMapFields["issue_id"][$flowIssueId]; ?></td>
                    <td><table><tr>
                                <td><table class='subissue'>
                                        <?php foreach ($flowIssue as $flowSubIssueId => $flowSubIssue) { ?>
                                            <tr class='mainrow'>
                                                <td class='fixwidth100'><?php echo $flowMapFields["sub_issue_id"][$flowIssueId][$flowSubIssueId]; ?></td>
                                                <td> 
                                                    <?php foreach ($flowSubIssue as $flowSubSubIssueId => $flowSubSubIssue) { ?>
                                                        <table class='subsubissue'>
                                                            <td  class='fixwidth100'><?php echo $flowMapFields["sub_sub_issue_id"][$flowSubIssueId][$flowSubSubIssueId]; ?></td>
                                                            <td><table class='innerTable subinner'>
                                                                    <?php foreach ($flowSubSubIssue as $flowDeptId => $flowDept) { ?>
                                                                        <tr><td><table>
                                                                                    <?php foreach ($flowDept as $flowTaskId => $flowMerchant) { ?>
                                                                                        <tr><td><table class='borderInner'>
                                                                                                    <tr>

                                                                                                        <?php foreach ($flowMerchant as $flowMerchantId => $flowCat) { ?>
                                                                                                            <td class='fixwidth100'>
                                                                                                                <?php
                                                                                                                echo "<div class='item' title='" . $flowMapFields["merchant_type"][$flowMerchantId] . "'>" . $flowMapFields["merchant_type"][$flowMerchantId] . "</div>";
                                                                                                                ?>
                                                                                                            </td><td><table>
                                                                                                                    <?php foreach ($flowCat as $flowCatId => $flowReturnStatus) { ?>
                                                                                                                        <tr><td class='rightborder'><?php
                                                                                                                                echo "<div class='item' title='" . $flowMapFields["category"][$flowCatId] . "'>" . $flowMapFields["category"][$flowCatId] . "</div>";
                                                                                                                                ?>
                                                                                                                            </td><td><table>
                                                                                                                                    <?php foreach ($flowReturnStatus as $flowstatusId => $flowValue) { ?>
                                                                                                                                        <tr><td class='rightborder'><?php
                                                                                                                                                echo "<div class='item' title='" . $staticFields["orderStatus"]["Return"][$flowstatusId] . "'>" . ($staticFields["orderStatus"]["Return"][$flowstatusId]?:"Any") . "</div>";
                                                                                                                                                ?></td><td><table>
                                                                                                                                                    <?php foreach ($flowValue as $flowValueId => $flowSubReason) { ?>
                                                                                                                                                        <tr><td class='rightborder'><?php
                                                                                                                                                                echo "<div class='item' title='" . $flowMapFields["valueRange"][$flowValueId] . "'>" . $flowMapFields["valueRange"][$flowValueId] . "</div>";
                                                                                                                                                                ?></td><td><table>
                                                                                                                                                                    <?php foreach ($flowSubReason as $flowsubId => $flowAction) { ?>
                                                                                                                                                                        <tr><td class='rightborder'><?php
                                                                                                                                                                                echo "<div class='item' title='" . $returnStaticFields["sub_reason"][$flowsubId] . "'>" . $returnStaticFields["sub_reason"][$flowsubId] . "</div>";
                                                                                                                                                                                ?></td><td><table>
                                                                                                                                                                                    <?php foreach ($flowAction as $flowActionId => $flow) { ?>
                                                                                                                                                                                        <tr><td class='rightborder'><?php
                                                                                                                                                                                                echo "<div class='item' title='" . $returnStaticFields["return_action"][$flowActionId] . "'>" . $returnStaticFields["return_action"][$flowActionId] . "</div>";
                                                                                                                                                                                    }  ?>
                                                                                                                                                                                            </td></tr></table>
                                                                                                                                                                                <?php }
                                                                                                                                                                                ?>
                                                                                                                                                                        </td></tr></table>
                                                                                                                                                                <?php } ?>
                                                                                                                                                            </td></tr></table>
                                                                                                                                                <?php } ?>
                                                                                                                                            </td></tr></table>
                                                                                                                                <?php } ?>
                                                                                                                            </td></tr></table>
                                                                                                                <?php } ?>
                                                                                                            </td>
                                                                                                            <?php
                                                                                                        
                                                                                                        ?>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <div class='borderInner individual'>
                                                                                                    <div class='innerExpand'>
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <th class='fixwidth100'><?php echo __('Merchant Type'); ?></th>
                                                                                                                <th class='rightborder'><?php echo __('category'); ?></th>
                                                                                                                <th class='rightborder'><?php echo __('Return Status'); ?></th>
                                                                                                                <th class='rightborder'><?php echo __('Value'); ?></th>
                                                                                                                <th class='rightborder'><?php echo __('Sub Reason'); ?></th>
                                                                                                                <th class='rightborder'><?php echo __('Return Action'); ?></th>
                                                                                                                <th class='fixwidth100' style='background: #060; color:#fff;'><?php echo __('Department'); ?></th>
                                                                                                                <th class='fixwidth100' style='background: #060; color:#fff;'><?php echo __('Task Type'); ?></th>
                                                                                                                <th class='fixwidth50'><?php echo __('Remove'); ?></th>
                                                                                                            </tr>

                                                                                                            <?php foreach ($flowCombinedArr2[$flowIssueId][$flowSubIssueId][$flowSubSubIssueId][$flowDeptId][$flowTaskId]["flow"] as $flowAll) { ?>

                                                                                                                <tr>
                                                                                                                    <td><?php echo $flowMapFields["merchant_type"][$flowAll->merchant_type]; ?></td>
                                                                                                                    <td><?php echo $flowMapFields["category"][$flowAll->category]; ?></td>
                                                                                                                    <td><?php echo $staticFields["orderStatus"]["Return"][$flowAll->return_status]; ?></td>
                                                                                                                    <td><?php echo $flowMapFields["valueRange"][$flowAll->value_min . "-" . $flowAll->value_max]; ?></td>
                                                                                                                    <td><?php echo $returnStaticFields["sub_reason"][$flowAll->sub_reason]; ?></td>
                                                                                                                    <td><?php echo $returnStaticFields["return_action"][$flowAll->return_action]; ?></td>
                                                                                                                    <td>
                                                                                                                        <select name='taskDept' flow_id='<?php echo $flowAll->flow_id; ?>' field='dept' class='taskChange' style="width:100px;">
                                                                                                                            <?php
                                                                                                                            foreach ($staticFields["department"] as $id => $data) {
                                                                                                                                $selected = '';
                                                                                                                                if ($flowAll->dept == $id) {
                                                                                                                                    $selected = "selected='selected'";
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                                <option value='<?php echo $id ?>' <?php echo $selected; ?>><?php echo $data["name"] ?></option>
                                                                                                                            <?php }; ?>
                                                                                                                        </select>
                                                                                                                    </td>
                                                                                                                    <td class='taskTypeChangeNoDesign'><select name='taskType' flow_id='<?php echo $flowAll->flow_id; ?>' field='task_type' class='taskChange' style="width:100px;">
                                                                                                                            <?php
                                                                                                                            foreach ($taskStaticFields["Task Types"] as $id => $data) {
                                                                                                                                $selected = "";
                                                                                                                                if ($flowAll->task_type == $id) {
                                                                                                                                    $selected = "selected='selected'";
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                                <option value='<?php echo $id ?>'  <?php echo $selected; ?>><?php echo $data ?></option>
                                                                                                                            <?php }; ?>
                                                                                                                        </select></td>
                                                                                                                    <td><input class='buttoncolor' style='background:#000 !important;'  type="submit" value="X" name="Remove-<?php echo $flowAll->flow_id; ?>"></td>
                                                                                                                </tr>
                                                                                                            <?php } ?>
                                                                                                            <tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    <center><a href='javascript:void(0);' class='individualclose'>close</a></center>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td   class='fixwidth100'>
                                                                                                <select name='taskDept' flow_id='<?php echo implode(",", $flowCombinedArr2[$flowIssueId][$flowSubIssueId][$flowSubSubIssueId][$flowDeptId][$flowTaskId]["flowId"]); ?>' field='dept' class='taskDeptChange taskChange' style="width:100px;">
                                                                                                    <?php
                                                                                                    foreach ($staticFields["department"] as $id => $data) {
                                                                                                        $selected = '';
                                                                                                        if ($flowDeptId == $id) {
                                                                                                            $selected = "selected='selected'";
                                                                                                        }
                                                                                                        ?>
                                                                                                        <option value='<?php echo $id ?>' <?php echo $selected; ?>><?php echo $data["name"] ?></option>
                                                                                                    <?php }; ?>
                                                                                                </select>
                                                                                            </td>
                                                                                            <td  class='fixwidth100 taskTypeChangeNoDesign' >
                                                                                                <select name='taskType' flow_id='<?php echo implode(",", $flowCombinedArr2[$flowIssueId][$flowSubIssueId][$flowSubSubIssueId][$flowDeptId][$flowTaskId]["flowId"]); ?>' field='task_type' class='taskTypeChange taskChange' style="width:100px;">
                                                                                                    <?php
                                                                                                    foreach ($taskStaticFields["Task Types"] as $id => $data) {
                                                                                                        $selected = "";
                                                                                                        if ($flowTaskId == $id) {
                                                                                                            $selected = "selected='selected'";
                                                                                                        }
                                                                                                        ?>
                                                                                                        <option value='<?php echo $id ?>'  <?php echo $selected; ?>><?php echo $data ?></option>
                                                                                                    <?php }; ?>
                                                                                                </select>
                                                                                            </td>
                                                                                            <td class='fixwidth100'>
                                                                                                <input class='expand buttoncolor' type="submit" value="&#8634;" name="Expand-<?php echo implode(",", $flowCombinedArr2[$flowIssueId][$flowSubIssueId][$flowSubSubIssueId][$flowDeptId][$flowTaskId]["flowId"]); ?>">
                                                                                                <input class='buttoncolor' style='background:#000 !important;'  type="submit" value="X" name="Remove-<?php echo implode(",", $flowCombinedArr2[$flowIssueId][$flowSubIssueId][$flowSubSubIssueId][$flowDeptId][$flowTaskId]["flowId"]); ?>"></td></tr>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </table></td></tr>  

                                                                    <?php } ?>
                                                                </table></td>  
                                                        </table>                                                        
                                                    <?php } ?>

                                                </td>
                                            </tr>

                                        <?php } ?>
                                    </table></td>
                            </tr></table></td>
                </tr>

            </tbody>
        <?php } ?>
        <tbody>
            <tr><th colspan="7"><br><br></th></tr>
        </tbody>
</table>
<center>
    <table>
        <tbody>
            <tr>

                <td><select name='issue_id' id='issue_id' style="width:100px;">
                        <?php
                        foreach ($flowMapFields["issue_id"] as $id => $data) {
                            if ($id == "691") {
                                ?>
                                <option value='<?php echo $id ?>'><?php echo $data ?></option>
                                <?php
                            }
                        };
                        ?>
                    </select>
                </td>
                <td><select name='sub_issue_id' id='sub_issue_id' style="width:100px;">
                        <option value='0'>Any</option>
                        <?php foreach ($flowMapFields["sub_issue_id"]["691"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='sub_sub_issue_id' id='sub_sub_issue_id' style="width:120px;">
                        <option value='0'>Any</option>    
                        <?php
                        if ($flowMapFields["sub_sub_issue_id"][array_keys($flowMapFields["sub_issue_id"]["691"])[0]]) {
                            foreach ($flowMapFields["sub_sub_issue_id"][array_keys($flowMapFields["sub_issue_id"]["691"])[0]] as $id => $data) {
                                ?>
                                <option value='<?php echo $id ?>'><?php echo $data ?></option>
                                <?php
                            }
                        } else {
                            ?>

                        <?php } ?>
                    </select>
                </td>
                <td><select name='merchant_type[]' id='merchant_type' multiple="multiple" style="width:100px;">
                        <?php foreach ($flowMapFields["merchant_type"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='category[]' id='category'  multiple="multiple" style="width:100px;">
                        <?php foreach ($flowMapFields["category"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td>   
                    <select name='return_status[]' id='Return' multiple="multiple" style="width:150px;">
                        <option value='0'>Any</option>
                        <?php foreach ($staticFields["orderStatus"]["Return"] as $id => $statusName) { ?>
                            <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td><select name='valueRange[]' id='valueRange'  multiple="multiple" style="width:90px;">
                        <?php foreach ($flowMapFields["valueRange"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='sub_reason[]' id='sub_reason' multiple="multiple" style="width:90px;">
                        <?php foreach ($returnStaticFields["sub_reason"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td>   
                    <select name='return_action[]' id="return_action" multiple="multiple" style="width:90px;">
                        <?php foreach ($returnStaticFields["return_action"] as $id => $statusName) { ?>
                            <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                        <?php } ?>
                    </select>

                </td>
                <td><select name='taskDept' id='taskDept' style="width:100px;">
                        <?php foreach ($staticFields["department"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data["name"] ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='taskType' id='taskType' style="width:200px;">
                        <?php foreach ($taskStaticFields["Task Types"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td></td>
            </tr>
        </tbody>
        <tbody>
            <tr><th colspan="10"><input type='submit' value="Add Flow"></th></tr>
        </tbody>
        </form>
    </table>
</center>
<script>
    var issues = JSON.parse('<?php echo json_encode($flowMapFields["issue_id"]) ?>');
    var sub_issues = JSON.parse('<?php echo json_encode($flowMapFields["sub_issue_id"]) ?>');
    var sub_sub_issues = JSON.parse('<?php echo json_encode($flowMapFields["sub_sub_issue_id"]) ?>');
    $("#issue_id").on('change', function () {
        var subissues = issueFields(sub_issues[$(this).val()]);
        $("#sub_issue_id").html(subissues);
    });
    $("#sub_issue_id").on('change', function () {
        if (sub_sub_issues[$(this).val()])
            var subissues = issueFields(sub_sub_issues[$(this).val()]);
        subissues = "<option value='0'>Any</option>" + subissues;
        $("#sub_sub_issue_id").html(subissues);
    });

    function issueFields(jsonArr) {
        var field = '';
        $.each(jsonArr, function (i, item) {
            field += "<option value='" + i + "'>" + item + "</option>";
        });
        console.log(field);
        return field;
    }
    $('.taskChange').on('change', function () {
        var idToChange = $(this).attr('flow_id');
        var value = $(this).val();
        var field = $(this).attr('field');
        var data = {"flowId": idToChange, "flowType": "ReturnTask", "value": value, "field": field};
        var url = 'ajax.php/tasks/flowupdate';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (resp)
            {
                alert(resp);
            }
        });

    });

    $(function () {
        //$("#issue_id").select2({'placeholder': 'Issue'});
        //$("#sub_issue_id").select2({'placeholder': 'Sub Issue'});
        //$("#sub_sub_issue_id").select2({'placeholder': 'Sub Sub Issue'});
        $("#merchant_type").select2({'placeholder': 'Merchant Type'});
        $("#valueRange").select2({'placeholder': 'Value Range'});
        $("#sub_reason").select2({'placeholder': 'Sub Reason'});
        $("#return_action").select2({'placeholder': 'Return Action'});
        $("#category").select2({'placeholder': 'Category'});
        $("#Return").select2({'placeholder': 'Return Status'});
        $("#taskDept").select2({'placeholder': 'Department'});
        $("#taskType").select2({'placeholder': 'Task'});
        $(".taskDeptChange").select2({'placeholder': 'Department'});
        $(".taskTypeChange").select2({'placeholder': 'Task'});
    })
    $(".expand").on('click', function (e) {
        $('.individual').hide();
        $(this).closest('table').find('.individual').show();
        return false;
    });
    $(".individualclose").on('click', function (e) {
        $('.individual').hide();
        return false;
    });
</script>
