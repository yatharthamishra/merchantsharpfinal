<?php
$fields = array(
			'data' => array
				(
					'triage' => array
						(
							'all' => array
								(
									0 => array
									(
										'fields' => array
											(
												"Ticket No." => "number",
												"Ticket Source" => "source_extra",
												"Ticket Status" => "status__name",
												//"Ticket Channel" => "odata__ticket_channel",
												"Priority" => "odata__priority",
												"Creation Date" => "created",
												"Order Number" => "odata__order_id",
												"Type" => "",
												"Subtype" => "",
												"Subsubtype" => "",
												"Return Reason" => "",
												"Agent Name" => "staff__firstname",
												"Agent ID" => "staff__email",
												"Team Leader" => "",
												"Triaging Date" => "triage__triage_time",
												"Triaging Type" => "triage__triage",
												"AHT" => "",
												"SLA status" => "isoverdue"
											),
										'filters' => array
											(
													"Date Range" => array("From","To"),
													"Priority" => array("Value"),
													"Type" => array("Issue Type"),
													"Subtype" => array("Sub Issue type"),
													"Subsubtype" => array("Sub Sub Issue Type"),
													"Agent ID" => array("Agent ID"),
													"Team Leader" => array("Team Leader ID"),
													"Triaging Type" => array("Value"),
													"SLA status" => array("Value")								
											)
									)

								),
							'sla adherence' => array
									(
											'agent level' => array
												(
														'fields' => array
															(
																"Date" => "",
																"Agent Name" => "staff__firstname",
																"Agent ID" => "staff__email",
																"Team Leader" => "",
																"Tickets Handled - Untriaged" => "",
																"Tickets Handled - Triage Again" => "",
																"Total Tickets Triaged" => "ticket_count",
																"SLA - Untriaged" => "",
																"SLA - Triage Again" => "",
																"SLA - Total Triaged" => ""
															),
														'filters' => array
															(
																"Date Range" => array("From","To"),
																"Priority" => array(),
																"Agent ID" => array("Agent name"),
																"Team Leader" => array("Team Leader Name"),
																"Triaging Type" => array("Value")	
															)
												),

											'issue type' => array
												(
														'fields' => array
															(
																"Date" => "",
																"Type" => "",
																"Subtype" => "",
																"Subsubtype" => "",
																"Tickets Handled Untriaged" => "",
																"Tickets Handled Triage Again" => "",
																"Total Tickets Triaged" => "",
																"SLA - Untriaged" => "",
																"SLA - Triage Again" => "",
																"SLA - Total Triaged" => ""
															),
														'filters' => array
															(
																"Date Range" => array(),
																"Priority" => array(),
																"Type" => array(),
																"Subtype" => array(),
																"Subsubtype" => array(),
																"Triaging Type" => array()	
															)

												)

									),
							'aht' => array
								(
									'agent level' => array
										(
												'fields' => array
													(
														"Date" => "",
														"Agent Name" => "",
														"Agent ID" => "",
														"Team Leader" => "",
														"Tickets Handled - Untriaged" => "",
														"Tickets Handled - Triage Again" => "",
														"Total Tickets Triaged" => "",
														"AHT - Untriaged" => "",
														"AHT - Triage Again" => "",
														"AHT - Total Triaged" => ""
													),
												'filters' => array
													(
														"Date Range" => array(),
														"Priority" => array(),
														"Agent ID" => array(),
														"Team Leader" => array(),
														"Triaging Type" => array()
													)
									),

									'issue type' => array
										(
												'fields' => array
													(
														"Date" => "",
														"Type" => "",
														"Subtype" => "",
														"Subsubtype" => "",
														"Tickets Handled - Untriaged" => "",
														"Tickets Handled - Triage Again" => "",
														"Total Tickets Triaged" => "",
														"AHT - Untriaged" => "",
														"AHT - Triage Again" => "",
														"AHT - Total Triaged" => ""
													),
												'filters' => array
													(
														"Date Range" => array(),
														"Priority" => array(),
														"Type" => array(),
														"Subtype" => array(),
														"Subsubtype" => array(),
														"Triaging Type" => array()
													)

										)

								),
							// 'productivity' => array
							// 		(		
							// 				0 => array
							// 				(
							// 					'fields' => array
							// 						(
							// 							"Date",
							// 							"Agent Name",
							// 							"Agent ID",
							// 							"Team Leader",
							// 							"Tickets Handled - Untriaged",
							// 							"Tickets Handled - Triage Again",
							// 							"Total Tickets Triaged"
							// 						),
							// 					'filters' => array
							// 						(
							// 							"Date Range",
							// 							"Priority",
							// 							"Agent ID",
							// 							"Team Leader",
							// 							"Triaging Type"
							// 						)
							// 				)
							// 		),
							'pendency' => array
								(
										0 => array
											(
												'fields' => array
													(
														"Date" => "date",	
														"Tickets Received Untriaged" => "ticket_count",
														"Tickets Received Triage Again" => "date_ta",
														"Tickets Handled - Untriaged" => "",
														"Tickets Handled - Triage Again" => "",
														"Tickets Pending - Untriaged" => "",
														"Tickets Pending - Triage Again" => ""
													),
												'filters' => array
													(
														"Date Range" => array(),
														"Priority" => array(),
														"Type" => array(),
														"Subtype" => array(),
														"Subsubtype" => array(),
														"Triaging Type" => array()
													)
											)
								)
						),
					'ops' => array
						(
							'sla_adherence' => array(),
							'AHT' => array()
						)
				)
			);
?>