<?php
/* * *
 * Filename - settings-customWidget.inc.php
 * Purpose  - This file is used for internal setting configration
 * @author  - Akash Kumar
 */
if (!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config)
  die('Access Denied');
global $cfg;
$configSetting = json_decode($cfg->config['customSetting']["value"],true);

//custom form fields
$form=DynamicForm::lookup(2);
$customFields = $form->getDynamicFields();
foreach($customFields as $f){
    $fieldVarName = $f->get('name');
    $fieldName = str_replace(" ","_",  strtolower($f->get('label')));
if($fieldVarName!="subject" && $fieldVarName!="message" && $fieldVarName!="priority"){
        if(!$configSetting["ticketCustomData"][$fieldName])
            $configSetting["ticketCustomData"][$fieldName]=0;
    }
}
?>
<h2><?php echo __('Custom Setting'); ?></h2>
<form action="settings.php?t=setting" method="post" id="save">
  <?php csrf_token(); ?>
  <input type="hidden" name="t" value="setting" >
  <?php foreach($configSetting as $widget=>$widgetSetting){ ?>
  <table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
      <tr>
        <th colspan="2">
    <h4><?php echo __(ucfirst($widget).' settings'); ?></h4>
    <em><?php echo __("Setting custom features."); ?></em>
    </th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($widgetSetting as $wKey=>$wSetting){ 
            $wKey=$wKey; ?>
      <tr>
          <td width="180"><?php echo __(str_replace("_", " ", ucfirst($wKey))); ?>:</td>
        <td>
          <input type="checkbox" name="<?php echo $widget;?>-<?php echo $wKey; ?>" value="1" <?php echo $wSetting ? 'checked="checked"' : ''; ?> >
          <?php echo __(str_replace("_", " ", ucfirst($wKey))); ?>
          &nbsp;<font class="error">&nbsp;<?php echo $errors['widget_'.$widget."_".$wKey.'']; ?></font>
        </td>
      </tr>
      <?php }?>
    </tbody>
  </table>
  <?php }?>
  <p style="padding-left:210px;">
    <input class="button" type="submit" name="submit" value="<?php echo __('Save Changes'); ?>">
    <input class="button" type="reset" name="reset" value="<?php echo __('Reset Changes'); ?>">
  </p>
</form>