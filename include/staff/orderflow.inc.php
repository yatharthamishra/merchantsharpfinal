<?php
global $cfg;
$staticFields = getCachedTicketFields();
//_pr($staticFields); die;
$taskStaticFields = getCachedTaskFields();
$isExport = FALSE;
if($_GET['EXPORT']=='exportTrue')
  $isExport = TRUE;
?>
<center>
    <div style="margin-bottom:5px;">
        <h2 style="cursor:pointer;" id="filter">Filter</h2>
        <form id="formFilter" name="formFilter" action="orderflow.php" method="get">
            <?php echo __('Department'); ?>
            <select name="departmentFilter">
                <option value="-1">No Filter</option>
                <option value="0" <?php if(isset($_GET['departmentFilter']) && $_GET['departmentFilter']==0) echo "selected" ?>>All</option>
                <?php
                foreach ($staticFields["department"] as $key => $value) {
                    $text = '<option value="'.$key.'"';
                    if($_GET['departmentFilter']==$key) 
                        $text.= "selected";
                    $text.= '>'.$value['name'].'</option>';
                    echo $text;
                }
                ?>
            </select>
            <input type="submit" value="Filter">
        </form>
    </div>
</center>
<table class="list" width="100%">
    <form name="orderFlow" action="orderflow.php" method="POST">
        <thead>
            <tr><th colspan="8"><?php echo __('Order Flow machine'); ?></th></tr>
        </thead>
        <tbody>
            <tr><th><?php echo __('Status Type'); ?></th>
                <th><?php echo __('Order Status'); ?></th>
                <th><?php echo __('Refund Status'); ?></th>
                <th><?php echo __('Dept'); ?></th>
                <th><?php echo __('Task'); ?></th>
                <th style='background: #060; color:#fff;'><?php echo __('Response'); ?></th>
                <th style='background: #060; color:#fff;'><?php echo __('Communnication'); ?></th>
                <th ><?php echo __('Remove'); ?></th>
            </tr>
        </tbody>
        <?php
	$emailTemp = EmailTemplateNew::objects()->filter(array("code_name__startswith"=>"order"))->values("id","templateGroup__template_name","subject")->all();
 foreach ($emailTemp as $template) {
     if(empty($template["templateGroup__template_name"])){
          $emailTempArr[$template["id"]] = $template["subject"];
        }else{
          $emailTempArr[$template["id"]] = $template["templateGroup__template_name"];
        }
 }


        if(isset($_GET['departmentFilter'])){
            if($_GET['departmentFilter']!=-1){
                $urlargs="?departmentFilter=".$_GET['departmentFilter'];
                $filter = array('dept' =>$_GET['departmentFilter'],'flowstatus'=>1);
                $filterSet=1;
            }
            else
                $filterSet=0;
        }
        else
            $filterSet=0;
        if($filterSet)
            $orderFlow = OrderFlow::objects()->filter($filter);
        else
            $orderFlow = OrderFlow::objects()->filter(array("flowstatus"=>1));
        $page_limit = PAGE_LIMIT;
        $bulkPageLimit = json_decode($cfg->config['customLimits']["value"],true);
        $bulkPageLimit = isset($bulkPageLimit["bulkTickets"])?$bulkPageLimit["bulkTickets"]:5000;
        $page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
        $count = $orderFlow->count();
        $orderFlowAll=clone $orderFlow;
        if ($_GET['pl']) {
            $page_limit = $_GET['pl'];
        }

        if($_SESSION['bulk'])
            $pageNav = new Pagenate($count, $page, $bulkPageLimit);
        else
            $pageNav = new Pagenate($count, $page, $page_limit);
        $pageNav->setURL('orderflow.php'.$urlargs, $args);
        $orderFlow = $pageNav->paginate($orderFlow);

        if($_SESSION['bulk'])
            $orderFlow->limit($bulkPageLimit);


        foreach ($orderFlow->all() as $flow) {
            $taskIdArr[] = $flow->orderflow_id;
            $flowArr[] = $flow;
        } 
        $start = $count>0?($page-1)*$page_limit+1:0;
        $last = ($page*$page_limit)<$count?($page*$page_limit):$count;
        $showing = "Showing $start to $last of $count";
        
    if($isExport) { 
    ob_get_clean();
    $fp = fopen("php://output", "w");
$header = array('Status Type','Order Status','Refund Status','Dept','Task','Response','Communnication');
    fputcsv($fp, $header);
   
        $orderFlowAll->values()->all();
   
         foreach ($orderFlowAll as $flow) {
            $ordStattype= $flow['status_type'];
            $ordStatus= $staticFields["orderStatus"][$flow['status_type']][$flow['status']]?:"Any";
            $refundStat= $staticFields["orderStatus"]["Refund"][$flow['refund_status']]?:"Any";
            $dept = $staticFields["department"][$flow['dept']]["name"]?:"All Department";
            $taskType=  $taskStaticFields["Task Types"][$flow['task_type']]?:"Any Task";
            $taskresp=$taskStaticFields["Task Response"][$flow['task_response']];
            $communication=$emailTempArr[$flow['communication']]?:"No communication";
            $dataInCsv = array($ordStattype,$ordStatus,$refundStat,$dept,$taskType,$taskresp,$communication);
            fputcsv($fp,$dataInCsv);    
        }
    fclose($fp);
    die;
    echo '<div id="exportTicketData" style="display:none">YES</div>';
}    
        ?>
        <caption><span class="pull-left" style="display:inline-block;vertical-align:middle"><?php
                 echo $showing; ?></span>
        </caption>
        <?php
        foreach ($flowArr as $flow) { ?>
            <tbody>
                <tr>
                    <td><?php echo $flow->status_type; ?></td>
                    <td><?php echo $staticFields["orderStatus"][$flow->status_type][$flow->status]?:"Any"; ?></td>
                    <td><?php echo $staticFields["orderStatus"]["Refund"][$flow->refund_status]?:"Any"; ?></td>
                    <td><?php echo $flow->dept?$staticFields["department"][$flow->dept]["name"]:"All Department"; ?></td>
                    <td><?php echo $flow->task_type?$taskStaticFields["Task Types"][$flow->task_type]:"Any Task"; ?></td>
                    <td><select class='response' flowId="<?php echo $flow->orderflow_id; ?>" style="width:150px;">
                            <?php
                            $completeSelected = "selected";
                            foreach ($taskStaticFields["Task Response"] as $id=>$data) {
                                if ($flow->task_response == $id) {
                                    $selected = "selected";
                                    $completeSelected = "";
                                }
                                ?>
                                <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $data ?></option>
                                    <?php $selected = "";
                                }; ?>
                        </select></td>
                    <td><select class='communication' flowId="<?php echo $flow->orderflow_id; ?>" style="width:150px;">
                            <?php
                            $completeSelected = "selected";
                            foreach ($emailTempArr as $id=>$data) {
                                if ($flow->communication == $id) {
                                    $selected = "selected";
                                    $completeSelected = "";
                                }
                                ?>
                                <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $data ?></option>
        <?php $selected = "";
    }; ?>
                            <option value='0' <?php echo $completeSelected; ?>>No Communication</option>
                        </select>
                    </td>
                    <td><input type="submit" value="X" name="Remove-<?php echo $flow->orderflow_id; ?>"></td>
                </tr>
            </tbody>
                        <?php } ?>
        <tbody>
            <tr><th colspan="7"><br><br></th></tr>
        </tbody>
        <tbody>
            <tr>
                <td><select name='type'  id='statusType' style="width:150px;" readonly>
                        <?php foreach ($staticFields["orderStatus"] as $type => $os) { ?>
                        <option value='<?php echo $type; ?>' <?php if($type=="Order"){?> selected="selected" <?php }else{ ?>  disabled="disabled" <?php }?> ><?php echo $type; ?></option>
                        <?php }; ?>
                    </select></td>
                <td>   
                    <?php foreach ($staticFields["orderStatus"] as $type => $os) { ?>
                        <select class='dependentOptions' name='<?php echo $type; ?>Status' id='<?php echo $type; ?>' style="width:150px; <?php if($type!="Order"){?> display:none; <?php }?>">
                            <option value='-1'>Any</option>
                                <?php foreach ($os as $id => $statusName) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                            <?php }; ?>
                        </select>
                        <?php } ?>

                </td>
                <td>   
                    <select name='combinedRefundStatus' style="width:150px;">
                                <option value='0'>Any</option>
                    <?php foreach ($staticFields["orderStatus"]["Refund"] as $id => $statusName) { ?>
                                <option value='<?php echo $id; ?>'><?php echo $statusName; ?></option>
                        <?php } ?>
                    </select>

                </td>
                <td><select name='taskDept' style="width:150px;">
                        <option value='0'>Any Dept</option>
                        <?php foreach ($staticFields["department"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data["name"] ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='taskType' style="width:150px;">
                        <option value='0'>Any Task</option>
                        <?php foreach ($taskStaticFields["Task Types"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
                        <?php }; ?>
                    </select>
                </td>
                <td><select name='response' style="width:150px;">
                        <?php foreach ($taskStaticFields["Task Response"] as $id => $data) { ?>
                            <option value='<?php echo $id ?>'><?php echo $data ?></option>
<?php }; ?>
                    </select>
                <td><select name='communication' style="width:150px;">
                         <option value='0'>No Communication</option>
                        <?php
                        
                        foreach ($emailTempArr as $id=>$tem) { ?>
                            <option value='<?php echo $id ?>'><?php echo $tem ?></option>
<?php }; ?>
                    </select></td>
                <td></td>
            </tr>
        </tbody>
        <tbody>
            <tr><th colspan="7"><input type='submit' value="Add Flow"></th></tr>
        </tbody>
    </form>
</table>
<?php
$getParam = explode('?', $_SERVER['REQUEST_URI']);
     if(!empty($getParam[1]))
        $appendInUrl = $getParam[1].'&EXPORT=exportTrue';
    else 
        $appendInUrl='EXPORT=exportTrue';
    
echo '<div class="paging">&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;'; 
echo '<div class="export_bttm pull-right"><a class="export-csv no-pjax" href="?'.$appendInUrl.'">Export</a></div>';
?>
<script>
    $('.communication').change(function () {
        var flowId = $(this).attr("flowId");
        var communication = $(this).val();
        var data = {"flowId": flowId, "communication": communication};
        var url = 'ajax.php/tasks/orderflowupdate';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (resp)
            {
                alert(resp);
            }
        });
    });
        $('.response').change(function () {
        var flowId = $(this).attr("flowId");
        var response = $(this).val();
        var data = {"flowId": flowId, "response": response};
        var url = 'ajax.php/tasks/orderflowupdate';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (resp)
            {
                alert(resp);
            }
        });
    });
    $("#statusType").change(function(){
        var statusType = $("#statusType").val();
        $(".dependentOptions").hide();
        $("#"+statusType).show();
        
    });
    $("#filter").on("click",function(){
        $("#formFilter").slideToggle();
    });
</script>
