<?php
/* * *
 * Filename - settings.orderwidgets.php
 * Purpose  - This file contains the configurable settings for order Widgets for agent panel
 * @author  - Akash Kumar
 */
if (!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config)
  die('Access Denied');
global $cfg;
$configWidget = json_decode($cfg->config['customWidgets']["value"],true);
?>
<h2><?php echo __('Widgets Setting'); ?></h2>
<form action="settings.php?t=widget" method="post" id="save">
  <?php csrf_token(); ?>
  <input type="hidden" name="t" value="widgets" >
  <?php foreach($configWidget as $widget=>$widgetSetting){ ?>
  <table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
      <tr>
        <th colspan="2">
    <h4><?php echo __(ucfirst($widget).' widget settings'); ?></h4>
    <em><?php echo __("Disabling any widget disables that particular widget."); ?></em>
    </th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($widgetSetting as $wKey=>$wSetting){ 
            $wKey=ucfirst($wKey); ?>
      <tr>
        <td width="180"><?php echo __($wKey); ?>:</td>
        <td>
          <input type="checkbox" name="widget_<?php echo $widget."_".$wKey; ?>" value="1" <?php echo $wSetting ? 'checked="checked"' : ''; ?> >
          <?php echo __('Show '.$wKey.' Widget'); ?>
          &nbsp;<font class="error">&nbsp;<?php echo $errors['widget_'.$widget."_".$wKey.'']; ?></font>
        </td>
      </tr>
      <?php }?>
    </tbody>
  </table>
  <?php }?>
  <p style="padding-left:210px;">
    <input class="button" type="submit" name="submit" value="<?php echo __('Save Changes'); ?>">
    <input class="button" type="reset" name="reset" value="<?php echo __('Reset Changes'); ?>">
  </p>
</form>