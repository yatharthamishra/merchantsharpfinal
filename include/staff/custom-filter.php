<div class="table_with_links left_tickets_link grid2 float_left">
    <h3>Ticket Views</h3> <span style='float: right;margin-top: -15px;margin-right: 10px;'><a href='tickets.php?clear_filter=1'><i class="icon-refresh"></i></a></span> 
    <div id="accordion" class="left_slide_bar">   
        <?php
        //require_once 'class.dept.php';
        $sid = $thisstaff->getId();   // Staff id
        $triagePermission = ($thisstaff->hasPerm(User::PERM_READ_COMMENT) || $thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT))?1:0;
        $dep = $thisstaff->getDepartments();
        
        $accCount = 0;
        $depCondition = '';
        if (count($dep) > 0) {
            $depCondition = " where (D.id IN ('" . implode("','", $dep) . "'))";
            $innerOuter = 'outer';
        } else {
            $depCondition = " where (D.id IN ('10000'))"; // No department to show
        }

        /*         * *************    Department data      ************ */
        $query = "select D.id,D.name,D.path from mst_department D " . $depCondition; // Department details
        
        $res = db_query($query);
        $staffDept = array();
        $i = 0;
        while ($dept_array = db_fetch_array($res)) {
            $staffDept[$dept_array['id']] = $dept_array['name']; // department name
            $path = explode("/", trim($dept_array['path'], "/"));
            foreach ($path as $p) {
                $staffDept[$p] = 1;
            }
            $i++;
        }

        // Department class obj
        $deptObj = new Dept();
        $count_array = $deptObj->ticketCount(); // Array of departments and corrosponding
        
        // ticket counts
        // Open departments tickets count (No assigned dept)
        $no_assigned_dept = $deptObj->openCount();
	global $showDepartmentView;
        $showDepartmentView=checkIfShowDepartmentView();
        ?>
        <!-----------------Unassigned Filter-------------------------->
        <?php if ($showDepartmentView && $thisstaff->hasPerm(SearchBackend::PERM_FILTER_UNASSIGNED) && !$thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT)) { ?>
            <h3 style="color:black;">Unassigned Tickets<i class="arrow_down"></i></h3>
            <div>
                <?php
                $ticketObj = new Ticket();
                $filterStat = $ticketObj->getUnassignedTicketStats($thisstaff);  // Array of Team id and tickets cnt 
                ?>
                <ul class="customMenu" style='border-bottom: none;'><?php foreach ($filterStat as $k => $v) { ?>
                        <li><a class="linkOnUpdateList" update="un<?php echo $v["unassigned"]; ?>" href="tickets.php?unassigned=1&type=<?php echo $v["unassigned"]; ?>"><?php echo $k; ?><span><?php echo $v["count"]; ?></span></a></li>
                    <?php } ?>
                </ul><br><br>
            </div>
            <?php
            $acc["unassigned"] = $accCount;
            $accCount++;
        }
        ?>


        <!-----------------Complete Filter-------------------------->
        <?php if ($showDepartmentView && $thisstaff->hasPerm(SearchBackend::PERM_FILTER_TICKET)  && !$thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT)) { ?>
            <h3 style="color:black;">Ticket Respond<i class="arrow_down"></i></h3>
            <?php 
                
                $CommObj = DynamicList::lookup(array("name" => "Communication Flags"));
                if ($CommObj && $CommObj->getId()) {
                    $CommFlagsObj = DynamicListItem::objects()->filter(array('list_id' => $CommObj->getId()))->order_by('sort');
                    foreach ($CommFlagsObj as $st) {
                        $communication[$st->extra] = $st->value;
                    }
                }
                //  ECHO "<pre>"; print_r($communication); die;
                $GLOBALS['communicationMapping'] = $communication;
                $ticketObj=new Ticket();
                $staticTriageData = getCachedTriageFields();
                $GLOBALS['priorityMapping'] = $ticketObj->getPriorityFlags($staticTriageData);
                $priority=$GLOBALS['priorityMapping'];
                
            if(!$thisstaff->checkIfLoginUserOfEDDepartment()){
              ?>
            <div>
                <?php
                $ticketObj = new Ticket();
                $filterStat = $ticketObj->getStaffTicketUpdateStats($thisstaff);  // Array of Team id and tickets cnt 
                
                $priorityFilterStat = $filterStat["priority"];

                $mainFilterStat = $filterStat["main"];
  
                //  echo "<pre>"; print_r($priority); echo "</pre>"; //die;
                ?> 
                <ul class="customMenu"><?php
                    foreach ($mainFilterStat as $k => $v) {
                        ?>
                        <li>
                            <a class="linkOnUpdateList nodown" update="<?php echo $v["id"]; ?>" href="tickets.php?a=search&query=&ticket=1&respond=1&update=<?php echo $v["id"]; ?>&label=<?php echo $k; ?>"><?php echo $k . "<span>$v[count]</span>"; ?></a>
                            <ul style="display: none;" class="subMenu update<?php echo $v["id"]; ?>">        
                                <?php
                                foreach ($priority as $pk => $pv) {
                                    $countKey = $k . "-priority[" . $pv . "]";
                                    ?>
                                    <li>
                                        <a class="linkOnUpdateList" update="p<?php echo $v["id"] . $pk; ?>" href="tickets.php?a=search&query=&ticket=1&respond=1&update=<?php echo $v["id"]; ?>&priority=<?php echo $pk; ?>&label=<?php echo $pv; ?>"><?php
                                            echo $pv;
                                            echo "<span>$priorityFilterStat[$countKey]</span>";
                                            ?></a>
                                        <ul style="display: none;" class="subMenu updatep<?php echo $v["id"] . $pk; ?>">        
                                            <li>
                                                <a update="c<?php echo $v["id"] . $pk . '11'; ?>" href="tickets.php?a=search&query=&ticket=1&respond=1&update=<?php echo $v["id"]; ?>&priority=<?php echo $pk; ?>&call=<?php echo 1; ?>&label=Call Customer">Call<?php echo "<span>" . $priorityFilterStat[$countKey . "-communication[call]"] . "</span>"; ?></a>
                                            </li>
            <?php foreach ($communication as $ck => $cv) { ?>
                                                <li>
                                                    <a update="c<?php echo $v["id"] . $pk . $ck; ?>" href="tickets.php?a=search&query=&ticket=1&respond=1&update=<?php echo $v["id"]; ?>&priority=<?php echo $pk; ?>&communication=<?php echo $ck; ?>&label=<?php echo $cv; ?>"><?php
                                                        echo $cv;
                                                        echo "<span>" . $priorityFilterStat[$countKey . "-communication[" . $cv . "]"] . "</span>";
                                                        ?></a>
                                                </li>
            <?php } ?>
                                        </ul>

                                    </li>
                        <?php } ?>
                            </ul>
                        </li>
    <?php }
    ?>
                </ul>
            </div>
                    <?php
                } else {
                    $ticketObj = new Ticket();
                    $ticketRespondFilters = $ticketObj->getEDStaffTicketStats($thisstaff);  // Array of Team id and tickets cnt 
                    ?>
                    <div> 
                        <ul class="customMenu">
                            <?php
                            foreach ($ticketRespondFilters as $key => $ticketRespond) {
                                ?>
                                <li id="ddtr<?php echo $key; ?>">
                                    <i class="arrow"></i>
                                    <a update="ed<?php echo $key+1;?>" href="tickets.php?a=search&edRespond=1&source=<?php
                                        echo $ticketRespond['priority'];?>" data-link='<?php echo $ticketRespond['priority']; ?>'><?php
                                          echo $ticketRespond['channel']; ?><?php
                                        ?><span><?php echo $ticketRespond['ticketsOpenWithinSla'] +
                               $ticketRespond['ticketsOpenWithinOSla'];
                                           ?></span>
                                    </a>
                                    <?php ?>
                                    <ul class="subMenu showAlways updatet1">        
                                        <li>
                                            <a update="ed0<?php echo $key+1;?>" data-link='<?php echo $ticketRespond['priority'].'sla';?>'  href="tickets.php?a=search&edRespond=1&source=<?php
                                            echo $ticketRespond['priority']; ?><?php
                                               ?>&ticketState=sla">Within SLA<?php
                                                ?><span><?php echo $ticketRespond['ticketsOpenWithinSla']; ?></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a update="ed1<?php echo $key+1;?>" data-link='<?php echo $ticketRespond['priority'].'osla';?>' href="tickets.php?a=search&edRespond=1&source=<?php
                                             echo $ticketRespond['priority']; ?><?php
                                               ?>&ticketState=osla">OSLA<?php
                                                ?><span><?php echo $ticketRespond['ticketsOpenWithinOSla']; ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>

                <?php
                $acc["ticket"] = $accCount;
                $accCount++;
            }
        ?>

        <!-----------------Complete Task Filter-------------------------->
            <?php 
renderTasksWidget("Unassigned Tasks",$acc,$accCount);

        //-----------------Ticket Status -------------------------->


        if ($showDepartmentView && $triagePermission) {
            ?>
            <h3 style="color:black;">Triage Status <i class="arrow_down"></i></h3>
            <div>
                    <?php
                    $ticketObj = new Ticket();
                    $ticketTriageStats = $ticketObj->getStaffTriageStats($thisstaff);  // Array of Team id and tickets cnt 
                    ?>
                <ul class="customMenu"><?php
                $triageSLAids = array("OSLA"=>1,"OSLA Alert"=>2,"Within SLA"=>3);
                    foreach ($ticketTriageStats as $k => $v) { ?>
                        <li id="<?php echo 'triage' . '-' . $v["id"]; ?>">
                            <i class="arrow"></i>
                            <a update="t<?php echo $v["id"]; ?>"  href="tickets.php?a=search&query=&triageId=<?php echo $v["id"]; ?>&label=<?php echo $k; ?>"><?php echo $k . "<span>" . $v['total'] . "</span>"; ?></a>
        <?php if ($v["stat"]) { ?>
                                <ul class="subMenu showAlways updatet<?php echo $v["id"]; ?>">        
                                    <?php
                                   foreach ($v["stat"] as $tk => $tv) { ?>
                                        <li>
                                            <a update="t<?php echo $v["id"] . $triageSLAids[$tk]; ?>" href="tickets.php?a=search&query=&triageId=<?php echo $v["id"]; ?>&subTriage=<?php echo $triageSLAids[$tk];?>&label=<?php echo $k; ?>"><?php echo $tk . "<span>" . $tv . "</span>"; ?></a>
                                        </li>
                            <?php } ?>
                                </ul>
        <?php } ?>
                        </li>
            <?php }
            ?>
                </ul>
            </div>
            <?php
            $acc["triage"] = $accCount;
            $accCount++;
        }
        ?>
        <!-----------------Ticket Status -------------------------->

                <?php if ($showDepartmentView && $thisstaff->hasPerm(SearchBackend::PERM_FILTER_DEPT) && !$_SESSION["advsearch"]  && !$thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT)) { ?>
            <h3 style="color:black;">Departments <i class="arrow_down"></i></h3>
            <div>
                <ul class="customMenu">
                    <?php
                    $previousCount = 1;
                    if ($sid == $su) {
                        $depts = $deptObj->getDepartments();
                        foreach ($depts as $key => $value) {
                            $depName = explode("/", $value);
                            $countOfDep = count($depName);
                            if ($depName[1] && count($depName) > 1 && $previousCount < $countOfDep) {
                                $ul = 1;
                                echo "<ul class='subMenu dep" . $prevDep . "'>";
                            } else if ($ul == 1 && $previousCount > $countOfDep) {
                                $ul = 0;
                                for ($loop = 0; $loop < ($previousCount - $countOfDep); $loop++) {
                                    echo "</ul></li>";
                                }
                            } else {
                                echo "</li>";
                            }
                            $previousCount = $countOfDep;
                            $nameOfFilter = $depName[count($depName) - 1];
                            $countOfFilter = $count_array[$key] ? $count_array[$key] : "0";
                            ?>

                            <li>
                                <i class="arrow"></i>
                                <a class="linkOnList" dep="<?php
                                   $prevDep = $key;
                                   echo $key;
                                   ?>" href="tickets.php?a=search&query=&deptId=<?php echo $key; ?>&filter_type=custom"><?php echo $nameOfFilter . '<span>' . $countOfFilter . '</span>'; ?></a>
                                <?php
                            }
                            ?>
                            <!--     <li><a href="tickets.php?a=search&query=&deptId=0&filter_type=custom">No Dept
                                 (<?php echo $no_assigned_dept; ?>)</a></li>-->
                            <?php
                        } else if($showDepartmentView){
                            
                            global $thisstaff;
                            $staffID=$thisstaff->getId();
                            $depts = $deptObj->getDepartments();
                            
                            // login staff department values
                            foreach ($depts as $key => $value) {
                                if (!$staffDept[$key])
                                    continue;
                                $depName = explode("/", $value);
                                
                                $countOfDep = count($depName);
                                
                                if ($depName[1] && count($depName) > 1 && $previousCount < $countOfDep) {
                                    $ul = 1;
                                    if (!$previousCount)
                                        $visible = "style='display:block'";
                                    else
                                        $visible = "";
                                    echo "<ul class='subMenu dep" . $prevDep . "' " . $visible . ">";
                                }else if ($ul == 1 && $previousCount > $countOfDep) {
                                    $ul = 0;
                                    for ($loop = 0; $loop < ($previousCount - $countOfDep); $loop++) {
                                        echo "</ul></li>";
                                    }
                                } else {
                                    echo "</li>";
                                }
                                $previousCount = $countOfDep;


                                $nameOfFilter = $depName[count($depName) - 1];
                                $countOfFilter = $count_array[$key] ? $count_array[$key] : "0";
                                
                                ?>

                            <li>
                                <i class="arrow"></i>
                                <a class="linkOnList" dep="<?php
                                   $prevDep = $key;
                                   echo $key;
                                   ?>" href="tickets.php?a=search&query=&deptId=<?php echo $key; ?>&filter_type=custom"> <?php echo $nameOfFilter . '(' . $countOfFilter . ')'; ?></a>
                    <?php
                }
            }
            ?>
                </ul>
            </div>
            <?php
            $acc["dept"] = $accCount;
            $accCount++;
        }
        ?>
        <!-----------------Status wise filters ------------------------->
                <?php if ($showDepartmentView && $thisstaff->hasPerm(SearchBackend::PERM_FILTER_TICKET_STATUS) && !$_SESSION["advsearch"]  && !$thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT)) { ?>
            <h3 >Ticket Status<i class="arrow_down"></i></h3>
            <div>
                <ul  class="customMenu">  
                    <?php
                    // Show Statuses and Stats
                    $ticketObj = new Ticket();
                    $stats = $ticketObj->getStatsCount();
                    
                    foreach ($stats as $skey => $sval) {
                        if ($skey == 'resolved') {
                            ?>
                            <li id="closed">
                                <i class="arrow"></i>
                                <a href="tickets.php?status=<?php echo 'closed'; ?>">
            <?php echo ucfirst($skey) . "<span>$sval</span>"; ?></a>
                            </li>
                                    <?php
                                } else {
                                    ?>
                            <li id="<?php echo $skey; ?>">
                                <i class="arrow"></i>
                                <a href="tickets.php?status=<?php echo $skey; ?>">
            <?php echo ucfirst($skey) . "<span>$sval</span>"; ?></a>
                            </li>
                    <?php
                }
            }
            ?>
                </ul>
            </div>
    <?php
    $acc["status"] = $accCount;
    $accCount++;
}
?>
            
                    <!-----------------Status wise filters ------------------------->
                <?php if ($showDepartmentView && 1 || $thisstaff->hasPerm(SearchBackend::PERM_FILTER_TICKET_STATUS) && !$_SESSION["advsearch"]  && !$thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT)) { ?>
            <h3 >Departmentwise Tasks<i class="arrow_down"></i></h3>
            <div>
                <ul  class="customMenu">  
                    <?php
                    // Show Statuses and Stats
                    $ticketObj = new Ticket();
                    $stats = $ticketObj->getDepartmentWiseTaskBreakUp();
                    $cachedFields=getCachedTicketFields();
                    $taskDept=$_GET['taskdept'];
                    foreach ($stats as $skey => $sval) {
                        
                                    ?>
                            <li id="<?php echo $skey; ?>">
                                <i class="arrow"></i>
                                <a taskDept="<?php echo $skey; ?>" href="tickets.php?taskdept=<?php echo $skey; ?>" <?php if($taskDept==$skey){echo " class='active' ";}?>>
            <?php echo $cachedFields['dept'][$skey] . "<span>$sval</span>"; ?></a>
                            </li>
                    <?php
                
            }
            ?>
                </ul>
            </div>
    <?php
    $acc["taskDept"] = $accCount;
    $accCount++;
}

?>
        <!-----------------Complete Task Filter-------------------------->
            <?php renderTasksWidget("Tasks Respond",$acc,$accCount,TRUE);
?>
            
            <?php 
            function renderTasksWidget($widgetHeading,&$acc,&$accCount,$showForParticularStaff=0) {
                global $thisstaff;
                $urlToBeAppended="";
		global $showDepartmentView;		
                if($showForParticularStaff){
                    $urlToBeAppended="&selfStaff=1";
                }
    if ($thisstaff->hasPerm(SearchBackend::PERM_FILTER_TASK) && !$thisstaff->hasPerm(UserModel::PERM_ONLY_READ_COMMENT)) {
        ?>
                    <h3  <?php if($widgetHeading=="Tasks Respond" && !$showDepartmentView){echo "data-atr='tskre'";} ?>style="color:black;"><?php echo $widgetHeading;?><i class="arrow_down"></i></h3>
                    <div>
        <?php
        $taskObj = new Task();
        $taskFilterData = $taskObj->getTaskStats($thisstaff, $showForParticularStaff);  // Array of Team id and tickets cnt 
        // echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++</br>";echo "<pre>"; print_r($taskFilterData);// die;

        $filterStat = $taskFilterData['main'];
        $priorityFilterStat = $taskFilterData['priority'];
        ?>
                        <ul class="customMenu"><?php
        foreach ($filterStat as $k => $v) {
            ?>
                                    <li id="<?php echo 'task' . '-' . $v["id"]; ?>">
                                        <a  class="linkOnUpdateList nodown" update="<?php echo 'task_' . $v["id"]; ?>" href="tickets.php?a=search&task=1&query=&respond=1<?php echo $urlToBeAppended;?>&update=<?php echo $v["id"]; ?>&label=<?php echo $k; ?>"><?php echo $k . "<span>$v[count]</span>"; ?></a>
                                        <ul style="display: none;" class="subMenu update<?php echo 'task_' . $v["id"]; ?>">        
            <?php
            foreach ($priority as $pk => $pv) {
                $countKey = $k . "-priority[" . $pv . "]";
                ?>
                                                    <li>
                                                        <a class="linkOnUpdateList" update="task_p<?php echo $v["id"] . $pk; ?>" href="tickets.php?a=search&query=&task=1&respond=1<?php echo $urlToBeAppended;?>&update=<?php echo $v["id"]; ?>&priority=<?php echo $pk; ?>&label=<?php echo $pv; ?>"><?php
                echo $pv;
                echo "<span>$priorityFilterStat[$countKey]</span>";
                ?></a>
                                                        <ul style="display: none;" class="subMenu updatetask_p<?php echo $v["id"] . $pk; ?>">        
                                                            <li>
                                                                <a update="c<?php echo $v["id"] . $pk . '11'; ?>" href="tickets.php?a=search&query=&task=1&respond=1<?php echo $urlToBeAppended;?>&update=<?php echo $v["id"]; ?>&priority=<?php echo $pk; ?>&call=<?php echo 1; ?>&label=Call Customer">Call<?php echo "<span>" . $priorityFilterStat[$countKey . "-communication[call]"] . "</span>"; ?></a>
                                                            </li>
                <?php foreach ($communication as $ck => $cv) { ?>
                                                                    <li>
                                                                        <a update="c<?php echo $v["id"] . $pk . $ck; ?>" href="tickets.php?a=search&query=&task=1&respond=1<?php echo $urlToBeAppended;?>&update=<?php echo $v["id"]; ?>&priority=<?php echo $pk; ?>&communication=<?php echo $ck; ?>&label=<?php echo $cv; ?>"><?php
                    echo $cv;
                    echo "<span>" . $priorityFilterStat[$countKey . "-communication[" . $cv . "]"] . "</span>";
                    ?></a>
                                                                    </li>
                <?php } ?>
                                                        </ul>

                                                    </li>
            <?php } ?>
                                        </ul>
                                    </li>
        <?php }
        ?>
                        </ul>
                    </div>
        <?php
        $acc[$widgetHeading] = $accCount;
        $accCount++;
    }
   
}
            ?>
 
                    <!-----------------RR filter ------------------------->
                <?php 
                global $cfg;
                if ($thisstaff->getDeptId()==$cfg->getEDDepartmentId()) {
                    // Show Statuses and Stats
                    $ticketObj = new Ticket();
                    $rrDeptID=$cfg->getRRDepartmentId();
                    $stats = $ticketObj->getOSLASLACountForDepartment($rrDeptID);
                    $ticketsWithinSLA=$stats[0]['ticketsOpenWithinSla'];
                    $ticketsOSLA=$stats[0]['ticketsOpenWithinOSla'];
                    
                    ?>
            <h3 >RR Tickets<i class="arrow_down"></i></h3>
            <div>
                <ul class="customMenu">  
                                                <li id="rrinsla">
                                <i ></i>
                                <a href="tickets.php?a=search&rr=sla" data-link='rrsla'>
            In SLA<span><?php echo $ticketsWithinSLA;?></span></a>
                            </li>
                                   <li id="rrosla">
                                <i ></i>
                                <a href="tickets.php?a=search&rr=osla" data-link='rrosla'>
            OSLA<span><?php echo $ticketsOSLA;?></span></a>
                            </li>
                                    </ul>
            </div>
    <?php
    $acc["rrsla"] = $accCount;
    $accCount++;
}
?>
    </div>
    <br><br>

<?php
global $entityPageType;
if (!(isset($_REQUEST["id"]))) { ?>
    <?php 
    if($entityPageType=="ticket"){?>
        <center style="background:#EAEAEA;"><div id="bulkCreator" tabindex="-1" style="	visibility: hidden;">
                <h4 class="widgetHead" style="margin-bottom:10px;border-top:15px solid #EAEAEA;">Bulk Request Creation</h4>
                <h2></h2>
                <table>
                    <tr  id="bulkStatus"  style="display:none;"  ><td>Status</td><td>
                            <select style="width:130px;display:none;" name='status'>
                                <option value=''>select</option>
    <?php foreach ($staticFields["status"] as $id => $status) { ?>
                                    <option value='<?php echo $id; ?>'><?php echo $status; ?></option>
                                <?php } ?>
                            </select>
                        </td></tr>
                    <tr  id="bulkDept" style="display:none;" ><td>Department Transfer</td><td>
                            <select style="width:130px; display:none;" name='department'>
                                <option value=''>select</option>
                                <?php foreach ($staticFields["department"] as $id => $status) { ?>
                                    <option value='<?php echo $id; ?>'><?php echo $status["finalPath"]; ?></option>>
                                <?php } ?>
                            </select></td></tr>
                    <tr  id="bulkAssign" ><td>Assignee</td><td>
                            <select style="width:130px;"  name='Assignee'>
                                <option value=''>select</option>
                            <?php foreach ($staticFields["staff"] as $id => $status) { ?>
                                    <option value='<?php echo $id; ?>'><?php echo $status; ?></option>>
                            <?php } ?>
                            </select></td></tr>
                    <tr id="bulkComment"><td>Comment</td><td><br><br>
                            <?php
                            $inlineResponse = '<select  name="cannedResp" id="bulkCannedSelector" style="width:130px;">
                               <option value="0" selected="selected">&mdash; 
                               ' . __('Select a canned response') . ' &mdash;</option>';
                            if (($cannedResponses = Canned::getCannedResponses())) {
                                foreach ($cannedResponses as $id => $title) {
                                    $inlineResponse .='<option value=' . $id . '>' . $title . '</option>';
                                }
                            }
                            $inlineResponse .='</select>';
                            echo $inlineResponse;
                            ?>
                            <br>
                            <textarea style="width:130px;" id="commentBulk"  name='Comment'></textarea></td></tr>
                    <tr  id="bulkSla" ><td>SLA</td><td>
                    <input class="datetimepicker" style='width:100px;' id="nextCustomerUpdate" name='nextCustomerUpdate' type="text" placeholder="Next Comm. Date" >
                        </td></tr>
                    <tr id="bulkNote" style="display:none;"><td>Note</td><td><textarea style="max-width:170px;width:170px;height:140px" id="noteBulk"  name='note'></textarea></td></tr>

                    <tr><td colspan="2"><br><br></td></tr>
                    <tr><td colspan="2"><center><button id='submit_bulk_action' class="button" value='1' name='bulkAction' id="bulk_action"><i class="icon-gear"></i><?php echo __('Submit Bulk Request'); ?></button></center></td></tr>
                </table>
            </div>
        </center>
    <?php }else{?>
     <center style="background:#EAEAEA;"><div id="bulkTaskCreator" tabindex="-1" style="	visibility: hidden;">
                <h4 class="widgetHead" style="margin-bottom:10px;border-top:15px solid #EAEAEA;">Bulk Task Assignment</h4>
                <h2></h2>
                <table>
                    <?php 
                    global $thisstaff;
                    $deptIds=$thisstaff->getDepts();
                    $totalStaffs=[];
                    foreach ($deptIds as $deptId){
                        $staffIdsList=$staticFields['departmentStaffMap'][$deptId];
                        foreach ($staffIdsList as $staffId){
                            $totalStaffs[]=$staffId;
                        }
                    }
                    
                ?>
                    <tr  id="bulkAssign" ><td>Assignee</td><td>
                            <select style="width:130px;"  name='Assignee'>
                                <option value=''>select</option>
                            <?php foreach ($totalStaffs as $staffId) {
                                ?>
                                    <option value='<?php echo $staffId; ?>'><?php echo $staticFields["staff"][$staffId]; ?></option>>
                            <?php } ?>
                            </select></td></tr>
                    <br>
                    <tr  style="display:none" id="bulkResponse" ><td>Task Response</td><td>
                            <select style="width:130px;"  name='taskResponse'>
                                <option value=''>select</option>
                            </select></td></tr>
                    <tr id="bulkNote" style="display:none;"><td>Note</td><td><textarea style="max-width:170px;width:170px;height:140px" id="noteBulk"  name='note'></textarea></td></tr>
                    <tr><td colspan="2"><br><br></td></tr>
                    <tr><td colspan="2"><center><button id='submit_bulk_action' class="button" value='1' name='bulkAction' id="bulk_action"><i class="icon-gear"></i><?php echo __('Submit Bulk Request'); ?></button></center></td></tr>
                </table>
            </div>
        </center>
<?php }} ?>
</div>
<script>
    $(document).ready(function () {
        $('.datetimepicker').datetimepicker({
 formatDate:'d/m/Y H:i'});
        $("#bulkCreator").focusin(function () {
            $("#bulkCreator").addClass('bulk_bigger');
        });
        $("#bulkCreator").focusout(function () {
            $("#bulkCreator").removeClass('bulk_bigger');
        });
        $("input[name='tids[]']").on('click', function () {
            if ($("input.ckb:checked").length > 0) {
                $("#bulkTaskCreator").hide();
                $("#bulkCreator").css({"visibility": "visible", "background": "#fff"});
                $("#bulkCreator").show();
            } else {
              //  $("#bulkTaskCreator").hide();
            
                $("#bulkCreator").css({"visibility": "hidden", "background": "#fff"});
            }
            if ($("input.taskassign:checked").length > 0) {
                var allInput=$("input.taskassign:checked");
                var previousVal=$(allInput[0]).attr('data-ttype');
                var nextVal;
                var allSameTask=1;
                $("input.taskassign:checked").each(function(){
                    nextVal=$(this).attr('data-ttype');
                     if(nextVal!=previousVal){
                        allSameTask=0;
                        return false;
                    }
                    previousVal=nextVal;
                })
                if(allSameTask){
                     $("#bulkResponse").show();
                     var responsesToBeAdded=taskResponse[previousVal];
                     var selectResponse=$('#bulkResponse').find("select[name=taskResponse]");
                     for(var id in responsesToBeAdded){
                         if(selectResponse.find('option[value="'+id+'"]').length==0){
                         selectResponse.append("<option value="+id+" >"+responsesToBeAdded[id]+"</option>");
                        }
                     }
                 }else{
                     $("#bulkResponse").hide();
                     $('#bulkResponse').find("select[name=taskResponse]").html("<option value=0 >Select</option>");
                   }
                $("#bulkCreator").hide();
                $("#bulkTaskCreator").css({"visibility": "visible", "background": "#fff"});
            } else {
                $('#bulkResponse').find("select[name=taskResponse]").html("<option value=0 >Select</option>");
                $("#bulkResponse").hide();
                $("#bulkTaskCreator").css({"visibility": "hidden", "background": "#fff"});
            }
        });
$("#submit_bulk_action").click(function(){
if($("input[type='checkbox']:checked").length<1){
        alert("Please Select Some tickets");return false;
    }
    var selectedValue,selectedField,ticketId;

    var status = $("#bulkStatus select").val();
    var bulkDept = $("#bulkDept select").val();
    var assign = $("#bulkAssign select").val();
    var bulkSla = $("#bulkSla input").val();
    var commentTitle = $("#bulkCannedSelector").val();
    var comment = $("#commentBulk").val();
    var noteBulk="";
    var taskResponse=$("select[name='taskResponse']").val();
    if(status || bulkDept || assign || comment || bulkSla){
        if($("#noteBulk:visible").length>0){
            noteBulk = $("#noteBulk").val();
        }else{
            $("#bulkStatus,#bulkDept,#bulkAssign,#bulkComment,#bulkSla").slideUp();
            $("#bulkNote").slideDown();
            return false;
        }
    }else{
            alert("Please Select Some action to perform");
            return false;
    }
    var entityId;
    $.each($("input[type='checkbox']:checked") , function( index, value ) {
         if(entityId==null)
             entityId = $(value).val();
         else
             entityId = entityId+","+$(value).val();
    });
    var entityType=$("#bulkEntity").val();

    var data = {"entityIds":entityId,entityType:entityType,"taskResponse":taskResponse, "status":status,"Dept":bulkDept,"assign":assign,"commentTitle":commentTitle,"comment":comment,"noteBulk":noteBulk,"bulkSla":bulkSla};
    var url = 'ajax.php/tickets/createJob';
 $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false,
        beforeSend: function() {
            $("#bulkCreator").slideUp('normal',function(){
                $("#bulkCreator table").remove();
                $("#bulkCreator h2").text("Creating Job......");
                $("#bulkCreator").slideDown();
            });
        },
        success: function(resp)
        { console.log(resp);
            if($('#bulkCreator').length>0){
            bulkElement=$('#bulkCreator');
        }else{
            bulkElement=$('#bulkTaskCreator');
        }
                bulkElement.slideUp('normal',function(){
                bulkElement.find('table').remove();
                bulkElement.find('h2').remove();
                bulkElement.slideDown();
                bulkElement.removeClass('bulk_bigger');
                bulkElement.append('<p>Bulk Job Success</p>');
            });
        }
    });
});



    });
$("h3[data-atr='tskre']").trigger('click');
</script>

