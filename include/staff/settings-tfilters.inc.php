<?php
/* * *
 * Filename - settings.tfilters.inc.php
 * Purpose  - This file contains the configurable settings for filtering tickets
 * for agent panel
 * @author  - Vishal Sachdeva
 */

if (!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config)
  die('Access Denied');
?>
<h2><?php echo __('Ticket Filters and Options'); ?></h2>
<form action="settings.php?t=tfilters" method="post" id="save">
  <?php csrf_token(); ?>
  <input type="hidden" name="t" value="tfilters" >
  <table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
      <tr>
        <th colspan="2">
    <h4><?php echo __('Ticket Filters settings for Agents'); ?></h4>
    <em><?php echo __("Disabling any filter disables Agents' filter settings."); ?></em>
    </th>
    </tr>
    </thead>
    <tbody>
      <tr>
        <td width="180"><?php echo __('Departments'); ?>:</td>
        <td>
          <input type="checkbox" name="restrict_departments" value="1" <?php echo $config['restrict_departments'] ? 'checked="checked"' : ''; ?> >
          <?php echo __('Show Department Wise tickets'); ?>
          &nbsp;<font class="error">&nbsp;<?php echo $errors['restrict_departments']; ?></font>
          <i class="help-tip icon-question-sign" href="#restrict_departments"></i>
        </td>
      </tr>
      
        <tr>
        <td width="180"><?php echo __('Teams'); ?>:</td>
        <td>
          <input type="checkbox" name="restrict_teams" value="1" <?php echo $config['restrict_teams'] ? 'checked="checked"' : ''; ?> >
          <?php echo __('Show Team Wise tickets'); ?>
          &nbsp;<font class="error">&nbsp;<?php echo $errors['restrict_teams']; ?></font>
          <i class="help-tip icon-question-sign" href="#restrict_teams"></i>
        </td>
      </tr>
      
      <tr>
        <td width="180"><?php echo __('Statuses'); ?>:</td>
        <td>
          <input type="checkbox" name="restrict_status" value="1" <?php echo $config['restrict_status'] ? 'checked="checked"' : ''; ?> >
        <?php echo __('Show Status Wise tickets'); ?>
        &nbsp;<font class="error">&nbsp;<?php echo $errors['restrict_status']; ?></font>
        <i class="help-tip icon-question-sign" href="#restrict_status"></i>
        </td>
      </tr>
      
    </tbody>
  </table>
  <p style="padding-left:210px;">
    <input class="button" type="submit" name="submit" value="<?php echo __('Save Changes'); ?>">
    <input class="button" type="reset" name="reset" value="<?php echo __('Reset Changes'); ?>">
  </p>
</form>