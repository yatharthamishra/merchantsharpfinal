<?php
global $cfg;
header("Content-Type: text/html; charset=UTF-8");
if (!isset($_SERVER['HTTP_X_PJAX'])) { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html<?php
if(!$startLogTime)
$startLogTime = microtime(get_as_float);
$memoryStartUsage = memory_get_usage();
if (($lang = Internationalization::getCurrentLanguage())
        && ($info = Internationalization::getLanguageInfo($lang))
        && (@$info['direction'] == 'rtl'))
    echo ' dir="rtl" class="rtl"';
if ($lang) {
    echo ' lang="' . Internationalization::rfc1766($lang) . '"';
}
?>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="x-pjax-version" content="<?php echo GIT_VERSION; ?>">
    <title><?php echo ($ost && ($title=$ost->getPageTitle()))?$title:'osTicket :: '.__('Staff Control Panel'); ?></title>
    <!--[if IE]>
    <style type="text/css">
        .tip_shadow { display:block !important; }
    </style>
    <![endif]-->

    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-1.11.2.min.js?33f18c5"></script>
    
    <!--Date time picker -->
    <script src="<?php echo ROOT_PATH; ?>js/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript">
        var timeForTracking;
        <?php if($timeForTracking){ ?>
            timeForTracking = <?php echo $timeForTracking; ?>;
            <?php }
            else{ ?>
                timeForTracking = new Date().getTime() / 1000;
                timeForTracking = Math.round(timeForTracking);
                <?php } ?>
        var redirect_url="<?php 
                if ($_SERVER['HTTP_REFERER']) {
                    $refrer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
                }
                global $cfg;
                $singleSignonAuthUrl = json_decode($cfg->config['singleSignonAuthUrl']['value'], true);

                $authDomain = $refrer ? $refrer : $singleSignonAuthUrl['defaultDomain']; //"http://localhost";
                $authUrl = $authDomain . explode("?",$singleSignonAuthUrl['path'])[0]."?dispatch=auth.logout&"; //"/branches/b5/UniTechCity.php?dispatch=ticket.auth&destination=";
                
                if(!stristr($authUrl,"ttp")){
                    $authUrl = "http://".  $authUrl;
                }
                $authUrl = str_replace("support.shopclues","cs.shopclues",$authUrl);
                $link = $_SERVER['REQUEST_URI'];
                $link_array = explode('/',$link);
                $landing = $_GET["url"] ? $_GET["url"] : (end($link_array));
                $return_url=$authUrl . $landing;
        echo $return_url; ?>";
        var time="<?php echo $cfg->config['loginAjaxTime']['value']?$cfg->config['loginAjaxTime']['value']:5000; ?>";
        var locallogin="<?php echo $cfg->config['localLoginEnabled']['value']; ?>";
    </script>
    <script src="<?php echo ROOT_PATH; ?>scp/js/login_check.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/timodal.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH ?>css/jquery.datetimepicker.css"/>
    <!--Date time picker -->
    <link rel="stylesheet" href="<?php echo ROOT_PATH ?>css/thread.css?33f18c5" media="all"/>
     
       
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/redactor.css?33f18c5" media="screen"/>
    <link rel="stylesheet" href="./css/typeahead.css?33f18c5" media="screen"/>
    <!-- <link type="text/css" href="<?php echo ROOT_PATH; ?>css/ui-lightness/jquery-ui-1.10.3.custom.min.css?33f18c5"
         rel="stylesheet" media="screen" /> -->
     <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/font-awesome.min.css?33f18c5"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/font-awesome-ie7.min.css?33f18c5"/>
    <![endif]-->
    <?php 
    $actual_link = $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
    $linkContent = explode('/scp',$actual_link);
    if(stristr($linkContent[1],'reports.php'))
        {
            ?>
            <link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>scp/css/report.css" >
            <?php
        }
    if(($linkContent[1]=="/" || stristr($linkContent[1],'tickets.php')  || stristr($linkContent[1],'reports.php') || stristr($linkContent[1],'index.php')) && !stristr($actual_link[1],'&a=')){ ?>
     <link rel="stylesheet" href="./css/header_top.css?1" media="all"/>
    <?php }else{ ?>
     <link rel="stylesheet" href="./css/header_menu.css" media="all"/>
     <link rel="stylesheet" href="./css/scp.css?33f18c5" media="all"/>
     <?php }?>
      <link rel="stylesheet" href="./css/common.css" media="all"/>
    <link type="text/css" rel="stylesheet" href="./css/dropdown.css?33f18c5"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/loadingbar.css?33f18c5"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/flags.css?33f18c5"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/select2.min.css?33f18c5"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/rtl.css?33f18c5"/>
    <link type="text/css" rel="stylesheet" href="./css/translatable.css?33f18c5"/>
    <link href="./css/jquery.orgchart.css" rel="stylesheet">

    <?php
    if($ost && ($headers=$ost->getExtraHeaders())) {
        echo "\n\t".implode("\n\t", $headers)."\n";
    }
    ?>
</head>
<body class="no-pjax">
<div id="container" class="main_width">
    <?php
    if($ost->getError())
        echo sprintf('<div id="error_bar">%s</div>', $ost->getError());
    elseif($ost->getWarning())
        echo sprintf('<div id="warning_bar">%s</div>', $ost->getWarning());
    elseif($ost->getNotice())
        echo sprintf('<div id="notice_bar">%s</div>', $ost->getNotice());
    ?>
    <div id="header" class="header grid-row padding-container">
        <div class="grid7 float_left left_menu">
        <a href="index.php?clear_filter=1" class="logo" id="logo">
            <span class="valign-helper"></span>
            <img src="logo.php" alt="osTicket &mdash; <?php echo __('Customer Support System'); ?>"/>
        </a>
        <ul id="nav" class="topnav">
        <?php include STAFFINC_DIR . "templates/navigation.tmpl.php"; ?>
    </ul>
        </div>
        <ul id="info" class="pull-right no-pjax grid5"><li><?php echo sprintf(__('Welcome, %s.'), '<span style="font-weight:bold">'.$thisstaff->getFirstName().'</span>'); ?></li>
           <?php
            if($thisstaff->isAdmin() && !defined('ADMINPAGE')) { ?>
             <li><a href="admin.php" class="no-pjax admin_panel"><?php echo __('Admin Panel'); ?></a></li>
            <?php }else{ ?>
             <li><a href="index.php" class="no-pjax"><?php echo __('Agent Panel'); ?></a></li>
            <?php } ?>
             <li><a href="profile.php" class="no-pjax profile"><?php echo __('Profile'); ?></a></li>
             <li style='display:none;'><a href="logout.php?auth=<?php echo $ost->getLinkToken(); ?>" class="no-pjax logout"><?php echo __('Log Out'); ?></a></li>
        </ul>
    </div>
    <div id="pjax-container" class="main_wrapper<?php if ($_POST) echo 'no-pjax'; ?>">
<?php } else {
    header('X-PJAX-Version: ' . GIT_VERSION);
    if ($pjax = $ost->getExtraPjax()) { ?>
    <script type="text/javascript">
    <?php foreach (array_filter($pjax) as $s) echo $s.";"; ?>
    </script>
    <?php }
    foreach ($ost->getExtraHeaders() as $h) {
        if (strpos($h, '<script ') !== false)
            echo $h;
    } ?>
    <title><?php echo ($ost && ($title=$ost->getPageTitle()))?$title:'osTicket :: '.__('Staff Control Panel'); ?></title><?php
} # endif X_PJAX ?>
    <div class="nav main_content grid-row padding-container">
    <ul id="sub_nav" class="grid7 float_left">
<?php include STAFFINC_DIR . "templates/sub-navigation.tmpl.php"; ?>
    </ul>
    <!-- SEARCH FORM START -->
<div id='basic_search' class="search_section grid5 float_left">
    <form id='searchTicketFrm' action="tickets.php" method="get" >
    <input type="hidden" name="a" value="search">
    <input type="hidden" name="search-type" value=""/>
    
    <select name='SearchBy' id = 'searchParam'>
        <?php  
        $searchBY = json_decode($cfg->config['searchParam']["value"],true);
        $previousSelected = $_REQUEST['SearchBy'];
        foreach($searchBY as $key=>$val){
            $sel = '';
            if($previousSelected == $key)
                $sel = 'selected="selected"';
            echo "<option ".$sel." value='".$key."'>".$val." </option>";
        }
        ?>
    </select>
    <input type="text"  class="basic-search" placeholder="Search"  name="query"
        autofocus size="30" value="<?php echo Format::htmlchars($_REQUEST['query'], true); ?>"
        autocomplete="off" autocorrect="off" autocapitalize="off">
      
      <input type="submit" class="attached button" value=""> <!-- <i class="icon-search"/></i> --> 
    <a href="#" onclick="javascript:
        $.dialog('ajax.php/tickets/search', 201);"
        >[<?php echo __('advanced'); ?>]</a>
        <i class="help-tip icon-question-sign help_icon" href="#advanced"></i>
    </form>
</div>
<!-- SEARCH FORM END -->
</div>
    <div id="content" class="main_content content_bg">
        <?php if($errors['err']) { ?>
            <div id="msg_error"><?php echo $errors['err']; ?></div>
        <?php }elseif($msg) { ?>
            <div id="msg_notice"><?php echo $msg; ?></div>
        <?php }elseif($warn) { ?>
            <div id="msg_warning"><?php echo $warn; ?></div>
        <?php }
        foreach (Messages::getMessages() as $M) { ?>
            <div class="<?php echo strtolower($M->getLevel()); ?>-banner"><?php
                echo (string) $M; ?></div>
<?php   } ?>
