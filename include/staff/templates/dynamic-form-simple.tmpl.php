<div class="form-simple">
    <?php
    echo $form->getMedia();
    foreach ($form->getFields() as $name=>$f) {
?>
        <div class="flush-left custom-field" id="field<?php echo $f->getWidget()->id;
            ?>" <?php if (!$f->isVisible()) echo 'style="display:none;"'; ?>>
        <div>
  <?php if ($f->get('label')) { ?>
        <div class="field-label <?php if ($f->get('required')) echo 'required'; ?>">
        <label for="<?php echo $f->getWidget()->name; ?>">
            <?php echo Format::htmlchars($f->get('label')); ?>:
  <?php if ($f->get('required')) { ?>
            <span class="error">*</span>
  <?php } ?>
        </label>
        </div>
  <?php } ?>
        <?php
        if ($f->get('hint')) { ?>
            <em style="color:gray;display:block"><?php
                echo Format::viewableImages($f->get('hint')); ?></em>
        <?php
        } ?>
        </div><div>
        <?php
            $listId = $list->getId();
            global $cfg;
            $listItemField = $cfg->config['list_item_auto']['value'];
            if ($name == 'extra') {
                $listItemAutoIDs = json_decode($listItemField, 1);
	/*	
                if (in_array($listId,$listItemAutoIDs)) {
                    $listId = (int) $listId;
                    $sql = "select count(1) as numberOfItems from ".LIST_ITEM_TABLE." where list_id=$listId";
                    $rs = db_query($sql);
                    $row = db_fetch_array($rs);
                    $numberOfItems = $row['numberOfItems'];
                    $f->_widget->value = $numberOfItems + 1;
                }
*/
            }
       $f->render($options);
        ?>
        </div>
        <?php
        if ($f->errors()) { ?>
            <div id="field<?php echo $f->getWidget()->id; ?>_error">
            <?php
            foreach ($f->errors() as $e) { ?>
                <div class="error"><?php echo $e; ?></div>
            <?php
            } ?>
            </div>
        <?php
        } ?>
        </div>
    <?php
    }
    $form->emitJavascript($options);
    ?>
</div>
