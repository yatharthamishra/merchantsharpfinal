<?php
global $thisstaff, $ticket;
// Map states to actions
$actions= array(
        'closed' => array(
            'icon'  => 'icon-ok-circle',
            'action' => 'close',
            'href' => 'tickets.php'
            ),
        'open' => array(
            'icon'  => 'icon-undo',
            'action' => 'reopen'
            ),
        );

$states = array('open');
if ($thisstaff->getRole($ticket ? $ticket->getDeptId() : null)->hasPerm(TicketModel::PERM_CLOSE)
        && (!$ticket || !$ticket->getMissingRequiredFields()))
    $states = array_merge($states, array('closed'));


$nextStatuses = array();
/**
 * Commented for state machine statues by - Akash Kumar
 */
if(!$ticket){
foreach (TicketStatusList::getStatuses(
            array('states' => $states)) as $status) {
    if (!isset($actions[$status->getState()])
            || $statusId == $status->getId())
        continue;
   // $nextStatuses[] = $status; // Commented to prevent mass change status - Akash Kumar
}
}else{
/**
 * Status of ticket Info
 * @author Akash Kumar
 */

$idOfListObj = DynamicList::lookup(array("name"=>"State Machine"));
if($idOfListObj && $idOfListObj->getId()){
	$idOfList = $idOfListObj->getId();
$StateMachineObj = DynamicListItem::objects()->filter(array('list_id'=>$idOfList,'extra'=>$ticket->getStatus()->name));
foreach($StateMachineObj as $st){
    $stateData=array_values(json_decode($st->properties,true));
    
    foreach($stateData[0] as $k=>$v){
        $nextStateData[$k]=  $v;
    }
}
}

$nextStatusArr = array_keys($nextStateData);

foreach (TicketStatusList::getStatuses() as $status) {
    if (!in_array($status->getId(),$nextStatusArr))
        continue;
    $nextStatuses[] = $status;
}
}
//***************************
if (!$nextStatuses)
    return;
?>

<span
    class="action-button"
    data-dropdown="#action-dropdown-statuses">
    <i class="icon-caret-down pull-right"></i>
    <a class="tickets-action"
        href="#statuses"><i
        class="icon-flag"></i> <?php
        echo __('Change Status'); ?></a>
</span>
<div id="action-dropdown-statuses"
    class="action-dropdown anchor-right">
    <ul>
<?php foreach ($nextStatuses as $status) { ?>
        <li>
            <a class="no-pjax <?php
                echo $ticket? 'ticket-action' : 'tickets-action'; ?>"
                href="<?php
                    echo sprintf('#%s/status/%s/%d',
                            $ticket ? ('tickets/'.$ticket->getId()) : 'tickets',
                            $actions[$status->getState()]['action'],
                            $status->getId()); ?>"
                <?php
                if (isset($actions[$status->getState()]['href']))
                    echo sprintf('data-href="%s"',
                            $actions[$status->getState()]['href']);

                ?>
                ><i class="<?php
                        echo $actions[$status->getState()]['icon'] ?: 'icon-tag';
                    ?>"></i> <?php
                        echo __($status->getName()); ?></a>
        </li>
    <?php
    } ?>
    </ul>
</div>
