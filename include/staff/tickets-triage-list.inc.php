<?php
$search = SavedSearch::create();
$tickets = TicketModel::objects();
$staticFields = getCachedTicketFields();

//Ticket Custom data to display in table
global $cfg;
$configSetting = json_decode($cfg->config['customSetting']["value"], true);
$bulkPageLimit = json_decode($cfg->config['customLimits']["value"], true);
$bulkPageLimit = isset($bulkPageLimit["bulkTickets"]) ? $bulkPageLimit["bulkTickets"] : 5000;
//array of ticket fields to be displayed - Akash Kumar
$customFieldsToDisplay = 0; //$configSetting["ticketCustomData"];

$clear_button = false;
$view_all_tickets = $date_header = $date_col = false;

// Figure out REFRESH url — which might not be accurate after posting a
// response

$isExport = FALSE;
if($_GET['EXPORT']=='exportTrue')
    $isExport = TRUE;

list($path, ) = explode('?', $_SERVER['REQUEST_URI'], 2);
$args = array();
parse_str($_SERVER['QUERY_STRING'], $args);

// Remove commands from query
unset($args['id']);
if ($args['a'] !== 'search')
    unset($args['a']);

$refresh_url = $path . '?' . http_build_query($args);

$sort_options = array(
    'priority,updated' => __('Priority + Most Recently Updated'),
    'updated' => __('Most Recently Updated'),
    'priority,created' => __('Priority + Most Recently Created'),
    'due' => __('Due Soon'),
    'priority,due' => __('Priority + Due Soon'),
    'number' => __('Ticket Number'),
    'answered' => __('Most Recently Answered'),
    'closed' => __('Most Recently Closed'),
    'hot' => __('Longest Thread'),
    'relevance' => __('Relevance'),
);
$use_subquery = true;

$queue_name = strtolower($_GET['a'] ? : $_GET['status']); //Status is overloaded
// Stash current queue view
$_SESSION['::Q'] = $queue_name;

// Super user
$superUser = db_query("SELECT value from `mst_config` where `key`='mst_super_user'");
$su = db_fetch_array($superUser);
$su = $su['value'];
/**
 * Advance search clear search button Automatically
 * @author Akash Kumar
 */
if (array_keys($args)[0] != 'advanced' && isset($_SESSION['advsearch'])) {
    //unset($_SESSION['advsearch']);
    unset($_SESSION['bulk']);
}
$edDepartmentId=$cfg->getEDDepartmentId();
if(!($_GET['a']=='search' && isset($_GET['SearchBy']))){
    if($thisstaff->checkIfLoginUserOfEDDepartment()){
        $tickets->filter(array('dept_id__in'=>array($edDepartmentId)));
    }else{
        $tickets->filter(array('dept_id__notin'=>array($edDepartmentId)));
    }
}
if (isset($_SESSION['advsearch'])) {
    $form = $search->getFormFromSession('advsearch');
    /**
     * Display advance search parameters
     * @author Akash Kumar
     */
    $searchParam = getSearchParameters($search, $form);
    $tickets = $search->mangleQuerySet($tickets, $form); //print_r($tickets);die;
    $view_all_tickets = $thisstaff->hasPerm(SearchBackend::PERM_EVERYTHING);
    $results_type = __('Advanced Search')
            . '<a class="action-button" href="?clear_filter"><i style="top:0" class="icon-ban-circle"></i> <em>' . __('clear') . '</em></a>';
    $has_relevance = false;
    foreach ($tickets->getSortFields() as $sf) {
        if ($sf instanceof SqlCode && $sf->code == '`relevance`') {
            $has_relevance = true;
            break;
        }
    }
    if ($has_relevance) {
        $use_subquery = false;
        array_unshift($queue_sort_options, 'relevance');
    } elseif ($_SESSION[$queue_sort_key] == 'relevance') {
        unset($_SESSION[$queue_sort_key]);
    }
}
switch ($queue_name) {
    case 'closed':
        $status = 'closed';
        $results_type = __('Closed Tickets');
        $showassigned = true; //closed by.
        //$tickets->values('staff__firstname', 'staff__lastname', 'team__name', 'team_id');
        $queue_sort_options = array('closed', 'priority,due', 'due',
            'priority,updated', 'priority,created', 'answered', 'number', 'hot');
        break;
    case 'overdue':
        $status = 'open';
        $results_type = __('Overdue Tickets');
        $tickets->filter(array('isoverdue' => 1));
        $queue_sort_options = array('priority,due', 'due', 'priority,updated',
            'updated', 'answered', 'priority,created', 'number', 'hot');
        break;
    case 'assigned':
        $status = 'open';
        $staffId = $thisstaff->getId();
        $results_type = __('My Tickets');
        $tickets->filter(array('staff_id' => $thisstaff->getId()));
        $queue_sort_options = array('updated', 'priority,updated',
            'priority,created', 'priority,due', 'due', 'answered', 'number',
            'hot');
        break;
    case 'answered':
        $status = 'open';
        $showanswered = true;
        $results_type = __('Answered Tickets');
        $tickets->filter(array('isanswered' => 1));
        $queue_sort_options = array('answered', 'priority,updated', 'updated',
            'priority,created', 'priority,due', 'due', 'number', 'hot');
        break;
    default:
    case 'search':
        $queue_sort_options = array('priority,updated', 'priority,created',
            'priority,due', 'due', 'updated', 'answered',
            'closed', 'number', 'hot');
        // Consider basic search
        if ($_REQUEST['query']) {
            $results_type = __('Search Results');
            // Use an index if possible
            if ($_REQUEST['search-type'] == 'typeahead' && Validator::is_email($_REQUEST['query'])) {
                $tickets = $tickets->filter(array(
                    'user__emails__address' => $_REQUEST['query'],
                ));
            } else {
                $basic_search = Q::any(array(
                            'number__startswith' => $_REQUEST['query'],
                            'user__name__contains' => $_REQUEST['query'],
                            'user__emails__address__contains' => $_REQUEST['query'],
                            'user__org__name__contains' => $_REQUEST['query'],
                ));
                if (!$_REQUEST['search-type']) {
                    // [Search] click, consider keywords too. This is a
                    // relatively ugly hack. SearchBackend::find() add in a
                    // constraint for the search. We need to pop that off and
                    // include it as an OR with the above constraints
                    $tickets = $ost->searcher->find($_REQUEST['query'], $tickets);
                    $keywords = array_pop($tickets->constraints);
                    $basic_search->add($keywords);
                    // FIXME: The subquery technique below will crash with
                    //        keyword search
                    $use_subquery = false;
                }
                $tickets->filter($basic_search);
            }
            break;
        } // Apply user filter
        elseif (isset($_GET['uid']) && ($user = User::lookup($_GET['uid']))) {
            $tickets->filter(array('user__id' => $_GET['uid']));
            $results_type = sprintf('%s — %s', __('Search Results'), $user->getName());
            // Don't apply normal open ticket
            break;
        } elseif (isset($_GET['orgid']) && ($org = Organization::lookup($_GET['orgid']))) {
            $tickets->filter(array('user__org_id' => $_GET['orgid']));
            $results_type = sprintf('%s — %s', __('Search Results'), $org->getName());
            // Don't apply normal open ticket
            break;
        }
        /*         * **
         * For department & team wise ticket filtering: Added by vishal sachdeva
         */ elseif (isset($_GET['deptId'])) {
            if ($_GET['deptId'] != 0) {
                $dept = Dept::lookup($_GET['deptId']);
                $results_type = sprintf('%s — %s', __('Search Results'), $dept->getName());
            } else {
                $results_type = sprintf('%s — %s', __('Search Results'), 'No Department Assigned');
            }
            $tickets->filter(array('dept__id' => $_GET['deptId']));
            // View Restictions from direct url hit 
            if ($thisstaff->getId() != $su) {
                $viewAccessDept = $dept->canSeeDepartmentTickets($thisstaff->getId(), $_GET['deptId']);
            }
            break;
        } elseif (isset($_GET['teamId']) && $team = Team::lookup($_GET['teamId'])) {

            $tickets->filter(array('team_id__in' => array($_GET['teamId'])));
            $results_type = sprintf('%s — %s', __('Search Results'), $team->getName());
            $showTeamFieldInView = TRUE;

            // View Restictions from direct url hit
            if ($thisstaff->getId() != $su) {
                $viewAccessTeam = $team->canSeeTeamTickets($thisstaff->getId(), $_GET['teamId']);
            }
            break;
        } elseif (isset($_GET['triageId'])) {
            //$createdDate = SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value']));
            if ($_GET['triageId'] == "1") {
                $tickets->filter(Q::any(array('triage__triage' => 1, 'triage__triage__isnull' => true)));
                //$tickets->filter(Q::any(array()));
                //$tickets->filter(array('created__gt' => $createdDate));
            } else {
                $tickets->filter(array('triage__triage__in' => array($_GET['triageId'])));
            }

            $triageSLAids = array_flip(array("OSLA" => 1, "OSLA Alert" => 2, "Within SLA" => 3));
            
            if ($_GET['triageId']==3 && $_GET['subTriage']) {
                if($_GET['subTriage']==1){
                    $tickets->filter(array('triage__triage_again__lt' =>  SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value']))));
                }else if($_GET['subTriage']==2){
                    $tickets->filter(array('triage__triage_again__gt' => SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value'])),'triage__triage_again__lt' =>  SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriageAlert']['value']))));
                }else{
                    $tickets->filter(array('triage__triage_again__gt' => SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value']))));
                }
            }elseif ($_GET['subTriage']) {
                if($_GET['subTriage']==1){
                    $tickets->filter(array('created__lt' =>  SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value']))));
                }else if($_GET['subTriage']==2){
                    $tickets->filter(array('created__gt' => SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value'])),'created__lt' =>  SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriageAlert']['value']))));
                }else{
                    $tickets->filter(array('created__gt' => SqlExpression::minus(SqlFunction::NOW(), SqlInterval::SECOND($cfg->config['TimeToTriage']['value']))));
                }
            }
            $results_type = sprintf('%s — %s', __('Search Results'), $_GET['label']);
            //$showTeamFieldInView = TRUE;

            break;
        } elseif (isset($_GET['respond'])) {
            $mapping = json_decode($cfg->config['customerUpdateFilters']['value'], true);
            if (isset($_GET['priority'])) {
                $tickets->filter(array('triage__priority__in' => array($_GET['priority'])));
            }
            if (isset($_GET['communication'])) {
                $tickets->filter(array('triage__communication__in' => array($_GET['communication'])));
            }
            if (isset($_GET['type'])) {
                
            }
            if (isset($_GET['update'])) {
                $time = explode("_", $mapping[$_GET['update']]["range"]);
                $datetimeLow = date('Y-m-d H:i:s', time() + ($time[0] * 3600));
                $datetimeHigh = date('Y-m-d H:i:s', time() + ($time[1] * 3600));

                $tickets->filter(array('est_duedate__gt' => $datetimeLow, 'est_duedate__lt' => $datetimeHigh));
            }
            $results_type = sprintf('%s — %s', __('Search Results'), $_GET['label']);
            //$showTeamFieldInView = TRUE;

            break;
        }
    // Fall-through and show open tickets
    case 'open':
        $status = 'open';
        $results_type = __('Open Tickets');
        $showassigned = ($cfg && $cfg->showAssignedTickets()) || $thisstaff->showAssignedTickets();
        if (!$cfg->showAnsweredTickets())
            $tickets->filter(array('isanswered' => 0));
        if (!$showassigned)
            $tickets->filter(Q::any(array('staff_id' => 0, 'team_id' => 0)));
        else
            $tickets->values();
        $queue_sort_options = array('priority,updated', 'updated',
            'priority,due', 'due', 'priority,created', 'answered', 'number',
            'hot');
        break;

    case 'deleted':
        $status = 'deleted';
        $results_type = __('Deleted Tickets');
        $queue_sort_options = array('answered', 'priority,updated', 'updated',
            'priority,created', 'priority,due', 'due', 'number', 'hot');
        break;

    case 'archived':
        $status = 'archived';
        $results_type = __('Archived Tickets');
        $queue_sort_options = array('answered', 'priority,updated', 'updated',
            'priority,created', 'priority,due', 'due', 'number', 'hot');
        break;
}
// Apply primary ticket status
if ($status) {
    $tickets->filter(array('status_id__in' => $staticFields["state"][$status]));
}

// Impose visibility constraints
// ------------------------------------------------------------
if (!$view_all_tickets && !$_GET['filter_type'] == 'custom' && !isset($_GET['teamId']) && !$_SESSION["advsearch"]) {
    // -- Open and assigned to me
    /*
      $assigned = Q::any(array(
      'staff_id__in' => $thisstaff->getAgents(),
      ));
     * 
     */
    /*
      // -- Open and assigned to a team of mine
      if ($teams = array_filter($thisstaff->getTeams()))
      $assigned->add(array('team_id__in' => $teams));
     */
    $visibility = Q::any(new Q(array('status_id__in' => $staticFields["state"]["open"])));

    /*
      // -- Routed to a department of mine
      if (!$thisstaff->showAssignedOnly() && ($depts=$thisstaff->getDepts()))
      $visibility->add(array('dept_id__in' => $depts));
     */
    // -- Routed to a department of mine

    $tickets->filter(Q::any($visibility));
}

// TODO :: Apply requested quick filter
// Apply requested pagination
$page_limit = PAGE_LIMIT;
if ($_GET['pl']) {
    $page_limit = $_GET['pl'];
}

$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$count = $tickets->count();
if ($_SESSION['bulk'])
    $pageNav = new Pagenate($count, $page, $bulkPageLimit);
else
    $pageNav = new Pagenate($count, $page, $page_limit);
$pageNav->setURL('tickets.php', $args);
$tickets = $pageNav->paginate($tickets);

// Apply requested sorting
$queue_sort_key = sprintf(':Q%s:%s:sort', ObjectModel::OBJECT_TYPE_TICKET, $queue_name);

if (isset($_GET['sort'])) {
    $_SESSION[$queue_sort_key] = $_GET['sort'];
} elseif (!isset($_SESSION[$queue_sort_key])) {
    $_SESSION[$queue_sort_key] = $queue_sort_options[0];
}

switch ($_SESSION[$queue_sort_key]) {
    case 'number':
        $tickets->extra(array(
            'order_by' => array(SqlExpression::times(new SqlField('number'), 1))
        ));
        break;

    case 'priority,created':
        $tickets->order_by('-odata__priority');
    // Fall through to columns for `created`
    case 'created':
        $date_header = __('Date Created');
        $date_col = 'created';
        $tickets->values('created');
        $tickets->order_by('-created');
        break;

    case 'priority,due':
        $tickets->order_by('-odata__priority');
    // Fall through to add in due date filter
    case 'due':
        $date_header = __('Due Date');
        $date_col = 'est_duedate';
        $tickets->values('est_duedate');
        $tickets->order_by(SqlFunction::COALESCE(new SqlField('est_duedate'), 'zzz'));
        break;

    case 'closed':
        $date_header = __('Date Closed');
        $date_col = 'closed';
        $tickets->values('closed');
        $tickets->order_by('-closed');
        break;

    case 'answered':
        $date_header = __('Last Response');
        $date_col = 'thread__lastresponse';
        $date_fallback = '<em class="faded">' . __('unanswered') . '</em>';
        $tickets->order_by('-thread__lastresponse');
        $tickets->values('thread__lastresponse');
        break;

    case 'hot':
        $tickets->order_by('-thread_count');
        $tickets->annotate(array(
            'thread_count' => SqlAggregate::COUNT('thread__entries'),
        ));
        break;

    case 'relevance':
        $tickets->order_by(new SqlCode('relevance'));
        break;

    default:
    case 'priority,updated':
        $tickets->order_by('-odata__priority');
    // Fall through for columns defined for `updated`
    case 'updated':
        $date_header = __('Last Updated');
        $date_col = 'lastupdate';
        $tickets->order_by('-lastupdate');
        break;
}




if ($isExport) {
    set_time_limit($cfg->config['exporttimeout']["value"]);
    $fp = fopen("php://output", "w");
    $header = array( 'Ticket','Status','Priority','Communication','Call Customer','Triage Status','Issue Type','Triage SLA');
    fputcsv($fp,$header);
    $offset=0;
    $limit = $cfg->config['exportbucketsize']["value"];
    
    while(1){
        $ticketClone = clone $tickets; 
        $ticketClone->values('staff_id','est_duedate','staff__firstname','staff__lastname','isoverdue', 'team_id', 'ticket_id', 'number', 'source', 'status_id','status__name', 'dept_id','dept__name' ,'created','lastupdate', 'isanswered','thread_count','attach_count','odata__subject','odata__order_ids','odata__priority','topic__topic');
        $ticketForImport = $ticketClone->limit($limit)->offset($offset*$limit)->all();
        $offset++;
        $count = 0;
        foreach ($ticketForImport as $T) {
            $count++;
            $tidArr[$T["ticket_id"]] = $T["number"];
        }
        $allCustomDataArr = getCompleteTriageData($tidArr);

        foreach ($ticketForImport as $T) {
            $TriageData = $allCustomDataArr[$T["ticket_id"]];
            $tnum = $T['number'];          
            $Tri_status=$staticFields["status"][$T["status_id"]];               
            $Tri_prior=$TriageData["Priority Flags"] ? $TriageData["Priority Flags"] : '--';
            $Tri_com=$TriageData["Communication Flags"] ? $TriageData["Communication Flags"] : '--';
            $Tri_Call= $TriageData["Call Customer"] ? $TriageData["Call Customer"] : '--';
            $Tri_stat= $TriageData["Triage Status"] ? $TriageData["Triage Status"] : 'Not Triage';                   
            $tri_issue=$T["topic__topic"] ? $T["topic__topic"] : '--'; 
            $Tri_SLA=Format::datetime($T['est_duedate']);                        
            $dataInCsv = array($tnum, $Tri_status,$Tri_prior,$Tri_com,$Tri_Call,$Tri_stat,$tri_issue,$Tri_SLA);
            // print_r($dataInCsv );
            fputcsv($fp,$dataInCsv);
        
        }
        if($count<$limit)
            break;
    }
    fclose($fp);
    die;
    echo '<div id="exportTicketData" style="display:none">YES</div>';
}






if ($_SESSION['bulk'])
    $tickets->limit($bulkPageLimit);
// Rewrite $tickets to use a nested query, which will include the LIMIT part
// in order to speed the result
//
// ATM, advanced search with keywords doesn't support the subquery approach
/**
  if ($use_subquery) {
  $orig_tickets = clone $tickets;
  $tickets2 = TicketModel::objects();
  $tickets2->values = $tickets->values;
  $tickets2->filter(array('ticket_id__in' => $tickets->values_flat('ticket_id')));

  // Transfer the order_by from the original tickets
  $tickets2->order_by($tickets->getSortFields());
  $tickets = $tickets2;
  }
 */
TicketForm::ensureDynamicDataView();
$tickets->filter(array("status__state" => "open"));
// Select pertinent columns
// ------------------------------------------------------------
//$tickets->values('lock__staff_id', 'staff_id', 'isoverdue', 'team_id', 'ticket_id', 'number', 'cdata__subject', 'user__default_email__address', 'source', 'cdata__:priority__priority_color', 'cdata__:priority__priority_desc', 'status_id', 'status__name', 'status__state', 'dept_id', 'dept__name', 'user__name', 'lastupdate', 'isanswered');
$tickets->values('staff_id', 'est_duedate', 'created', 'isoverdue', 'team_id', 'ticket_id', 'number', 'source', 'status_id', 'dept_id', 'lastupdate', 'isanswered', 'thread_count', 'attach_count', 'odata__subject', 'odata__order_ids', 'odata__priority', 'topic__topic','lock__staff_id');

// Add in annotations
/* $tickets->annotate(array(
  'collab_count' => TicketThread::objects()
  ->filter(array('ticket__ticket_id' => new SqlField('ticket_id', 1)))
  ->aggregate(array('count' => SqlAggregate::COUNT('collaborators__id'))),
  'attachment_count' => TicketThread::objects()
  ->filter(array('ticket__ticket_id' => new SqlField('ticket_id', 1)))
  ->filter(array('entries__attachments__inline' => 0))
  ->aggregate(array('count' => SqlAggregate::COUNT('entries__attachments__id'))),
  'thread_count' => TicketThread::objects()
  ->filter(array('ticket__ticket_id' => new SqlField('ticket_id', 1)))
  ->exclude(array('entries__flags__hasbit' => ThreadEntry::FLAG_HIDDEN))
  ->aggregate(array('count' => SqlAggregate::COUNT('entries__id'))),
  ));
 * 
 */

// Save the query to the session for exporting
$_SESSION[':Q:tickets'] = $orig_tickets;
?>

<!-- basic-search-move-here -->

<div class="clear"></div>
<div onclick="javascript:        $.dialog('ajax.php/tickets/search', 201);" style="cursor:pointer;margin-bottom:20px; padding-top:10px;text-align: center; position:relative;">
<input id="bulkEntity" type="hidden" value="ticket">
    <?php
/**
 * Display advance search parameters
 * @author Akash Kumar
 */
$searchParam = getSearchParameters($search, $form);
 global $entityPageType;
      $entityPageType="ticket";
if ($searchParam) {
    print_r($searchParam);
}
?>
</div>
<div class="clear"></div>

<!--*******************************************
    Code for displaying customized ticket filters
    Added by vishal Sachdeva -->
<div class="container_section grid-row">

<?php include('custom-filter.php'); ?>
    <!-- Filter code ends here   -->


    <div id="customDivContainer" class="grid10 table_section float_right">
        <div class="sticky bar opaque">
            <div class="content heading_action_section grid-row padding-container">
                <div class="pull-left flush-left">
                    <h2><a href="<?php echo $refresh_url; ?>"
                           title="<?php echo __('Refresh'); ?>"><i class="icon-refresh"></i> <?php echo
$results_type . $showing;
?></a></h2>
                </div>
                <div class="pull-right flush-right delete_by">
    <?php
    if ($count) {
        if ($thisstaff->canManageTickets()) {
            echo TicketStatus::status_options();
        }
        if ($thisstaff->hasPerm(TicketModel::PERM_DELETE, false)) {
            ?>
                            <a id="tickets-delete" class="red button action-button tickets-action"
                               href="#tickets/status/delete"><i
                                    class="icon-trash"></i> <?php echo __('Delete'); ?></a>
        <?php
    }
}
?>
                </div>
                <div class="pull-right shor_by">
                    <span class="valign-helper"></span>
                    <span class="action-button muted" data-dropdown="#sort-dropdown">

                        <span><i class="icon-sort-by-attributes-alt"></i> <?php echo __('Sort'); ?></span>
                    </span>
                    <div id="sort-dropdown" class="action-dropdown anchor-right"
                         onclick="javascript: $.pjax({
                url: '?' + addSearchParam('sort', $(event.target).data('mode')),
                timeout: 2000,
                container: '#pjax-container'});">
                        <ul class="bleed-left">
                    <?php
                    foreach ($queue_sort_options as $mode) {
                        $desc = $sort_options[$mode];
                        $selected = $mode == $_SESSION[$queue_sort_key];
                        ?>
                                <li <?php if ($selected) echo 'class="active"'; ?>>
                                    <a href="#" data-mode="<?php echo $mode; ?>"><i class="icon-fixed-width <?php
                    if ($selected)
                        echo 'icon-hand-right';
                    ?>"></i> <?php echo Format::htmlchars($desc); ?></a>
                                </li>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <form action="tickets.php" method="POST" name='tickets' id="tickets">
<?php csrf_token(); ?>
            <input type="hidden" name="a" value="mass_process" >
            <input type="hidden" name="do" id="action" value="" >
            <input type="hidden" name="status" value="<?php echo
Format::htmlchars($_REQUEST['status'], true);
?>" >
            <table class="list" border="0" cellspacing="1" cellpadding="2" width="940">
                <thead>
                    <tr>
                            <?php if ($thisstaff->canManageTickets()) { ?>
                            <th width="12px"><a id="selectToggle" style='background:none;' href="#ckb"><?php echo __('&#10004'); ?></a></th>
                                    <?php } ?>
                        <th width="70">
                                                                                <?php echo __('Ticket'); ?></th>
                        <th>
                            <?php echo __('Status'); ?></th>
                        <th <?php echo $pri_sort; ?>>
<?php echo __('Priority'); ?></th>
                        <th <?php echo $comm_sort; ?>>
<?php echo __('Communication'); ?></th>
                        <th <?php echo $call_sort; ?>>
<?php echo __('Call Customer'); ?></th>
                        <th <?php echo $tri_sort; ?>>
            <?php echo __('Triage Status'); ?></th>
                        <th <?php echo $issue_sort; ?>>
<?php echo __('Issue Type'); ?></th>
                        <th <?php echo $due_sort; ?>>
                   <?php echo __('Triage SLA'); ?></th>

<?php { ?>

                            <?php
                        }
                        foreach ($customFieldsToDisplay as $fields => $val) {
                            if ($val == 1) {
                                ?>
                                <th>
                                    <?php echo __(ucwords(str_replace("_", " ", $fields))); ?></th>
                                <?php }
                            } ?>
                    </tr>
                </thead>
                <tbody>
                            <?php
                            if ($viewAccessDept == '0' || $viewAccessTeam == '0') {
//echo '<tr><td colspan="7">'.__('There are no tickets matching your criteria.!').'</td></tr>'; //-- commented by akash Kumar for javascript loading issue
                            }
                            // Setup Subject field for display
                            $class = "row1";
                            $total = 0;
                            $ids = ($errors && $_POST['tids'] && is_array($_POST['tids'])) ? $_POST['tids'] : null;
//        echo '<pre>';print_r($tickets);
                            //custom fields - Akash
                            foreach ($tickets as $T) {
                                $tidArr[$T["ticket_id"]] = $T["number"];
                            }
                            $allCustomDataArr = getCompleteTriageData($tidArr);

                            foreach ($tickets as $T) {
                                $total += 1;
                                $tag = $T['staff_id'] ? 'assigned' : 'openticket';
                                $flag = null;
                                if ($T['lock__staff_id'] && $T['lock__staff_id'] != $thisstaff->getId())
                                    $flag = 'locked';
                                elseif ($T['isoverdue'])
                                    $flag = 'overdue';

                                $TriageData = $allCustomDataArr[$T["ticket_id"]];

                                if ($showassigned) {
                                    if ($T['staff_id'])
                                        $lc = $staticFields["staff"][$T['staff_id']]; //new AgentsName($T['staff__firstname'].' '.$T['staff__lastname']);
                                    elseif ($T['team_id'])
                                        $lc = Team::getLocalById($T['team_id'], 'name', $T['team__name']);
                                }
                                elseif ($showTeamFieldInView) {
                                    // Print Team value if team wise filters are called:added by vishal
                                    $lc = Team::getLocalById($_GET['teamId'], 'name', $team->getName());
                                } else {
                                    //error_reporting(E_ALL);ini_set("display_errors", 1);
                                    $lc = $staticFields["department"][$T['dept_id']]["finalPath"]; //Dept::getLocalById($T['dept_id'], 'name', $T['dept__name']); //Commented by Akash Kumar to show full path
                                }
                                $tid = $T['number'];
                                if (!in_array($T['status_id'], $staticFields["state"]["open"]) && !$T['isanswered'] && !$T['lock__staff_id']) {
                                    $tid = sprintf('<b>%s</b>', $tid);
                                }
                                ?>

                        <tr id="<?php echo $T['ticket_id']; ?>"  class="ticketCheckbox" style="background-color:<?php 
            if ($flag == 'locked') { echo '#FFDFB1';}?>;">
                        <?php
                        if ($thisstaff->canManageTickets()) {

                            $sel = false;
                            if ($ids && in_array($T['ticket_id'], $ids))
                                $sel = true;
                            ?>
                                <td align="center" class="nohover">
                                    <input class="ckb" type="checkbox" name="tids[]"
                                           value="<?php echo $T['ticket_id']; ?>" <?php echo $sel ? 'checked="checked"' : ''; ?>>
                                    <label for="aa_<?php echo $T['ticket_id']; ?>"></label>
                                </td>
                        <?php } ?>
                            <td title="<?php echo $T['user__default_email__address']; ?>" nowrap>
                                <a class="Icon <?php echo strtolower($T['source']); ?>Ticket preview"
                                   title="Preview Ticket"
                                   href="tickets.php?id=<?php echo $T['ticket_id']; ?>"
                                   data-preview="#tickets/<?php echo $T['ticket_id']; ?>/preview"
                                   ><?php echo $tid; ?></a></td>
                            <td>
                        <?php echo $staticFields["status"][$T["status_id"]]; ?></td>
                            <td>
                        <?php echo $TriageData["Priority Flags"] ? $TriageData["Priority Flags"] : '--'; ?></td>
                            <td >
                            <?php echo $TriageData["Communication Flags"] ? $TriageData["Communication Flags"] : '--'; ?></td>
                            <td >
                            <?php echo $TriageData["Call Customer"] ? $TriageData["Call Customer"] : '--'; ?></td>
                            <td >
                            <?php echo $TriageData["Triage Status"] ? $TriageData["Triage Status"] : 'Not Triage'; ?></td>
                            <td >
                            <?php echo $T["topic__topic"] ? $T["topic__topic"] : '--'; ?></td>
                            <td align="center" title="<?php echo Format::relativeTime($T['est_duedate']); ?>" nowrap><?php echo Format::datetime($T['est_duedate']); ?></td>

    <?php
} //end of foreach
if (!$total)
    $ferror = __('There are no tickets matching your criteria.');
?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7">
<?php if ($total && $thisstaff->canManageTickets()) { ?>
                                <?php echo __('Select'); ?>:&nbsp;
                                <a id="selectAll" href="#ckb"><?php echo __('All'); ?></a>&nbsp;&nbsp;
                                <a id="selectNone" href="#ckb"><?php echo __('None'); ?></a>&nbsp;&nbsp;
                                <a id="selectToggle" href="#ckb"><?php echo __('Toggle'); ?></a>&nbsp;&nbsp;
                            <?php
                            } else {
                                echo '<i>';
                                echo $ferror ? Format::htmlchars($ferror) : __('Query returned 0 results.');
                                echo '</i>';
                            }
                            ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
                        <?php
               $_SERVER['REQUEST_URI'] = str_replace('&_pjax=%23pjax-container', '', $_SERVER['REQUEST_URI']);
                $_SERVER['REQUEST_URI'] = str_replace('%20', '', $_SERVER['REQUEST_URI']);
$getParam = explode('?', $_SERVER['REQUEST_URI']);
    if(!empty($getParam[1]))
        $appendInUrl = $getParam[1].'&EXPORT=exportTrue';
    
    if ($total>0) { //if we actually had any tickets returned.
        echo '<div class="paging">&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;';
        echo sprintf('<div class="export_bttm"><a class="export-csv no-pjax" href="?'.$appendInUrl.'">%s</a>',               
                __('Export'));
            
        echo '&nbsp;<i class="help-tip icon-question-sign" href="#export"></i></div></div>';
                        }
                        ?>
        </form>    
    </div>
</div>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm'); ?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="mark_overdue-confirm">
<?php echo __('Are you sure you want to flag the selected tickets as <font color="red"><b>overdue</b></font>?'); ?>
    </p>
    <div><?php echo __('Please confirm to continue.'); ?></div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="button" value="<?php echo __('No, Cancel'); ?>" class="close">
        </span>
        <span class="buttons pull-right">
            <input type="button" value="<?php echo __('Yes, Do it!'); ?>" class="confirm">
        </span>
    </p>
    <div class="clear"></div>
</div>


<script type="text/javascript">
    $(function () {
        $(document).off('.tickets');
        $(document).on('click.tickets', 'a.tickets-action', function (e) {
            e.preventDefault();
            var count = checkbox_checker($('form#tickets'), 1);
            if (count) {
                var url = 'ajax.php/'
                        + $(this).attr('href').substr(1)
                        + '?count=' + count
                        + '&_uid=' + new Date().getTime();
                $.dialog(url, [201], function (xhr) {
                    window.location.href = window.location.href;
                });
            }
            return false;
        });
        $(".ticketCheckbox td.nohover").on('click', function () {
            $(this).children("input.ckb").click();
        });
    });

<?php include('script-custom-filter.inc.php'); ?>

    $("#bulkCannedSelector").change(function () {
        var selectedCanned = $(this).val();
        var url = 'ajax.php/kb/getCannedFields-Update/' + selectedCanned;
        $.ajax({
            type: "GET",
            url: url,
            cache: false,
            success: function (resp)
            {
                $("#commentBulk").val(resp.response);
            }
        });
    });

</script>

<?php

/**
 * function to give search parameters of advanced search
 * @param type $search search Object
 * @param type $customForm form Object
 * @return string of search parameters to be displayed
 * @author Akash Kumar
 */
function getSearchParameters($search, $customForm) {
    if (!stristr($_SERVER['QUERY_STRING'], "advance"))
        return;
    $searchBasedParam = $search->getSearchFields($customForm);
    foreach ($searchBasedParam as $searchCat) {
        if (!$searchCat["active"])
            continue;
        foreach ($searchCat["value"] as $k => $param) {
            if (isset($param["name"])) {
                $searchFields = $searchFields . " " . $param["name"];
            } else {
                $searchFields = $searchFields . " " . $param;
            }
        }
        $searchBasedOn[$searchCat["field"]->getLabel()]["method"] = $searchCat["method"];
        $searchBasedOn[$searchCat["field"]->getLabel()]["value"] = $searchFields;
        $searchParamCustom .= "<span class='asField'>" . $searchCat["field"]->getLabel() . "</span> <span class='asMethod'>" . $searchCat["method"] . "</span> <span class='asValue'>" . $searchFields . "</span> <br>";
        $searchFields = "";
    }
    return $searchParamCustom;
}

/**
 * Function to give data for all custom fields of ticket
 * @param array $TIdArr - array of ticket ids
 * @return array of details for every ticket id
 * @author Akash Kumar
 */
function getTicketCustomFields($TIdArr) {
    $ticketDetailArr = array();
    $form = DynamicForm::lookup(2);
    $customFields = $form->getDynamicFields();
    foreach ($customFields as $f) {
        $fieldVarName = $f->get('name');
        $fieldName = str_replace(" ", "_", strtolower($f->get('label')));
        $fieldCustom[$fieldName] = $fieldVarName;
    }
    $sql = "SELECT * FROM " . TICKET_ODATA_TABLE . " WHERE ticket_id IN ('" . implode("','", array_keys($TIdArr)) . "')";
    if (($res = db_query($sql)) && db_num_rows($res))
        while ($row = db_fetch_array($res)) {
            $newRow = $row;
            foreach (array_flip($fieldCustom) as $k => $v) {
                $newRow[$v] = $row[$k];
            }
            $ticketDetailArr[$row["ticket_id"]] = $newRow;
        }
    return $ticketDetailArr;
}

/**
 * Function to give data for all custom fields of ticket
 * @param array $TIdArr - array of ticket ids
 * @return array of details for every ticket id
 * @author Akash Kumar
 */
function getCompleteTriageData($TIdArr) {
    $ticketDetailArr = array(); 
    $listed = array();
    $staticTriageData = getCachedTriageFields();
    $listed["Triage Status"] = $staticTriageData["Triage Status"];
    $listed["Priority Flags"] = $staticTriageData["Priority Flags"];
    $listed["Communication Flags"] = $staticTriageData["Communication Flags"];
   
    $sql = "SELECT ticket_id,priority As `Priority Flags`,communication as `Communication Flags`,triage as `Triage Status`,case when comment=1 then 'Yes' ELSE 'No' END as comment,case when call_customer=1 THEN 'Yes' ELSE 'No' END as `Call Customer` FROM " . TRIAGE_TABLE . " WHERE ticket_id IN ('" . implode("','", array_keys($TIdArr)) . "')";
    if (($res = db_query($sql)) && db_num_rows($res))
        while ($row = db_fetch_array($res)) {
            $row;
            foreach ($row as $k => $v) {
                if ($listed[$k][$v])
                    $ticketDetailArr[$row["ticket_id"]][$k] = $listed[$k][$v];
                else
                    $ticketDetailArr[$row["ticket_id"]][$k] = $v;
            }
        }
    return $ticketDetailArr;
}
