<?php
/*********************************************************************
    class.canned.php

    Canned Responses AKA Premade replies

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
include_once(INCLUDE_DIR.'class.file.php');

class Canned
extends VerySimpleModel {
    static $meta = array(
        'table' => CANNED_TABLE,
        'pk' => array('canned_id'),
        'joins' => array(
            'attachments' => array(
                'constraint' => array(
                    "'C'" => 'Attachment.type',
                    'canned_id' => 'Attachment.object_id',
                ),
                'list' => true,
                'null' => true,
                'broker' => 'GenericAttachments',
            ),
        ),
    );

    
    const PERM_MANAGE = 'canned.manage';

    static protected $perms = array(
            self::PERM_MANAGE => array(
                'title' =>
                /* @trans */ 'Premade',
                'desc'  =>
                /* @trans */ 'Ability to add/update/disable/delete canned responses')
    );

    static function getPermissions() {
        return self::$perms;
    }

    function getId(){
        return $this->canned_id;
    }

    function isEnabled() {
         return $this->isenabled;
    }

    function isActive(){
        return $this->isEnabled();
    }

    function getFilters() {

        if (!isset($this->_filters)) {
            $this->_filters = array();
            $cid = sprintf('"canned_id":%d', $this->getId());
            $sql='SELECT filter.id, filter.name '
                .' FROM '.FILTER_TABLE.' filter'
                .' INNER JOIN '.FILTER_ACTION_TABLE.' action'
                .'  ON (filter.id=action.filter_id)'
                .' WHERE action.type="canned"'
                ."  AND action.configuration LIKE '%$cid%'";

            if (($res=db_query($sql)) && db_num_rows($res))
                while (list($id, $name) = db_fetch_row($res))
                    $this->_filters[$id] = $name;
        }

        return $this->_filters;
    }

    function getAttachedFiles($inlines=false) {
        return AttachmentFile::objects()
            ->filter(array(
                'attachments__type'=>'C',
                'attachments__object_id'=>$this->getId(),
                'attachments__inline' => $inlines,
            ));
    }

    function getNumFilters() {
        return count($this->getFilters());
    }

    function getTitle() {
        return $this->title;
    }

    function getResponse() {
        return $this->response;
    }
    function getResponseWithImages() {
        return Format::viewableImages($this->getResponse());
    }

    function getReply() {
        return $this->getResponse();
    }

    function getHtml() {
        return $this->getFormattedResponse('html');
    }

    function getPlainText() {
        return $this->getFormattedResponse('text.plain');
    }

    function getFormattedResponse($format='text', $cb=null,$orderid=null,$email=null) {
        $resp = array();
        $html = true;
        switch($format) {
            case 'json.plain':
                $html = false;
                // fall-through
            case 'json':
                $resp['id'] = $this->getId();
                $resp['title'] = $this->getTitle();
                $resp['response'] = $this->getResponseWithImages();
                
                // Callback to strip or replace variables!
                if ($cb && is_callable($cb))
                    $resp = $cb($resp);
                
                $resp['files'] = array();
                foreach ($this->getAttachedFiles(!$html) as $file) {
                    $resp['files'][] = array(
                        'id' => $file->id,
                        'name' => $file->name,
                        'size' => $file->size,
                        'download_url' => $file->getDownloadUrl(),
                    );
                }
                // Strip html
                if (!$html) {
                    $resp['response'] = Format::html2text($resp['response'], 90);
                }
                $cannBody=$resp['response'];
                
                // Order Variables Replacer:added by vishal
                if($orderid !=null && $cannBody==null)
                { 
                  $finalCann = $this->getOrderInfoToDisplay($orderid,$email);
                  $resp['response']=$finalCann;
                }
                /**
                * Condition for order Details
                * @author Akash Kumar
                */
                elseif($orderid !=null)
                {
                  //$finalCann = $this->getOrderInfoForTicket(null,$orderid, $cannBody); 
                  //$resp['response']=$finalCann;
                }   
                ob_clean();
                return Format::json_encode($resp);
                break;
            case 'html':
            case 'text.html':
                $response = $this->getResponseWithImages();
                break;
            case 'text.plain':
                $html = false;
            case 'text':
            default: 
                $response = $this->getResponse();
                if (!$html)
                    $response = Format::html2text($response, 90);
                break;
        }

        // Callback to strip or replace variables!
        if ($response && $cb && is_callable($cb))
            $response = $cb($response);
            
        return $response;
    }

    function getNotes() {
        return $this->ht['notes'];
    }

    function getDeptId(){
        return $this->ht['dept_id'];
    }

    function getExtraDeptId(){
        $dept = explode(',', $this->ht['extra_dept_id']);

        return $dept;
    }
    function getHashtable() {
        $base = $this->ht;
        unset($base['attachments']);
        return $base;
    }

    function getInfo() {
        return $this->getHashtable();
    }

    function getNumAttachments() {
        return $this->attachments->count();
    }

    function delete(){
        if ($this->getNumFilters() > 0)
            return false;

        if (!parent::delete())
            return false;

        $this->attachments->deleteAll();

        return true;
    }

    /*** Static functions ***/

    static function create($vars=false) {
        $faq = parent::create($vars);
        $faq->created = SqlFunction::NOW();
        return $faq;
    }

    static function getIdByTitle($title) {
        $row = static::objects()
            ->filter(array('title' => $title))
            ->values_flat('canned_id')
            ->first();

        return $row ? $row[0] : null;
    }

    static function getCannedResponses($deptId=0, $explicit=false) {
         /**
          * Issue in department Id in canned response
          * @author - Akash Kumar
          * 
          */
         if(!$deptId)
            $deptId=1;
        //if ($deptId && !$explicit)
          //      $deptId=0;
        //$deptId=1;
        $filter = array('isenabled' => true);
        
        $canned = static::objects()
            ->filter($filter)
            ->order_by('title')
            ->values_flat('canned_id', 'title', 'dept_id', 'extra_dept_id');
        
        global $cfg;
        $setting = json_decode($cfg->config['customSetting']['value'],true);
        $setting = $setting["canned"];
        
        if($deptId && $setting["show_parent_departments"]){
            $depratmentObj = new Dept();
            $depArrDep = $depratmentObj->getAllDependentDepartments($deptId);
        }
        if($deptId && $setting["show_staff_departments"]){
            global $thisstaff;
            $staffId = $thisstaff->getId();
            $depArrStaff = $depratmentObj->getAllDependentDepartments($deptId);
        }
        $depArr = explode(",",$depArrDep.",".$depArrStaff);
        
        
        /**
         * Issue in department Id in canned response
         * @author - Akash Kumar
        if ($deptId) {
            $depts = Q::any(array('dept_id' => $deptId));
            if (!$explicit)
                $depts->add(array('dept_id' => 0));
            $canned->filter($depts);
        }
         */
        $responses = array();
        foreach ($canned as $row) {
            list($id, $title) = $row;
            $dept_ids = explode(',', $row[3]);
            if((in_array($row[2],$depArr)||count(array_intersect($dept_ids, $depArr)) != 0) && $deptId)
                $responses[$id] = $title;
            elseif(!$deptId)
                $responses[$id] = $title;
        }

        return $responses;
    }

    static function getVarScope() {
          $statictask= getCachedTaskFields();
        $tasklist=$statictask['Default_Task Types'];
        foreach ($tasklist as $key=>$value) {
            $value1=  str_replace(' ','_',$value);
          $value2='updated_sla_'.$value1;
       // $base= array($value=>$key);
            $base[$value2]=$value;
        }
        $idOfList = DynamicList::lookup(array("name"=>"Order Variable"))->getId();
        $li = DynamicListItem::objects()->filter(array('list_id'=>$idOfList));
         foreach($li as $key=>$listElement){
            $element = json_decode(json_encode($listElement),true)["ht"];
            $value=  str_replace('%{order.','',$listElement->getValue());
            $value=  str_replace('}','',$value);
            $content[$value]=$element["extra"];
        }
       return $base+$content;
    }
    function responsesByDeptId($deptId, $explicit=false) {
        return self::getCannedResponses($deptId, $explicit);
    }
    
    
    /**** Get departments from canned id:Added by Vishal Sachdeva 15 Dec 15****/
    function getDepartmentsFromCanned($cannedId)
    {
      $sql ='SELECT dept_id,canned_id FROM '.CANNED_TABLE
             .' WHERE canned_id='.$cannedId.' AND isenabled=1';
      $response = array();
      if(($res=db_query($sql)) && db_num_rows($res))
      {
        while(list($dept_id,$canned_id)=db_fetch_row($res))
          $response[$canned_id]=  $dept_id;    
      }
       return Format::json_encode($response);
    }
    
    /********* Get all assignees corrosponding to Department:
     * Function used while adding a new canned response
     */
    function getAssigneeForDept($deptid)
    {
      $sql="SELECT staff_id,firstname,lastname FROM ".STAFF_TABLE
           ." WHERE dept_id=".$deptid;
      $response = array();
      if(($res=db_query($sql)) && db_num_rows($res))
      {
        $i=0;
        while($result=db_fetch_row($res))
        {
          $response[$i] = array('assignee_id'=>$result[0],
              'firstname'=>$result[1],'lastname'=>$result[2]); 
          $i++;
        }
      }
        return Format::json_encode($response);
    }
    
    /****** Get fields for canned and update for tickets (used in ticket preview)
     * 
     */
    function getAndUpdateTicketFieldsForCanned($cannedId,$data)
    {
      /*** If doupdate=1 ,the update tickets table also***/
      $fields = "SELECT ccf.status,ccf.staff_id,ocr.dept_id,ocr.response FROM "
     .TABLE_PREFIX.'canned_custom_fields as ccf INNER JOIN  mst_canned_response as ocr'.
     ' ON ccf.canned_id=ocr.canned_id WHERE ccf.canned_id='.$cannedId.
      ' AND ocr.isenabled=1'  ;
      $response = array();
      if(($res=  db_query($fields)) && db_num_rows($res))
      {
        $result = db_fetch_row($res);
        $response['status']=$result[0];
        $response['staff_id']=$result[1];
        $response['dept_id']=$result[2];
        $response['response']=$result[3];
      }
      // If action do update then update the ticket else return response
      if(!isset($data['data']) && !isset($data['ticket']))
      {
        return Format::json_encode($response);
      }
      else
      {
        $sqlUpdate= 'UPDATE '.TICKET_TABLE.' SET status_id='.$response['status'].
              ',staff_id='.$response['staff_id'].',dept_id='.$response['dept_id'].
              ' WHERE number='.$data['ticket'];
        $sqlresult = db_query($sqlUpdate);
        if($sqlresult)
        {
          $resp = array("success"=>1,"response"=>$response);
        }
        else{
          $resp = array("success"=>0);
        }
        return Format::json_encode($resp);
      }
      
    }
    /*************************************************
     * Functionn to update canned text in thread table
     */
    function updateCannedInThread($data)
    {
      $body = $data['cannedtext'];
      $thread= $data['threadid'];
      $source='Web';
      $format='html';
      $pid=0;
      $updated = date('Y-m-d h:i:s');
      $user_id= $data['userid'];
      $staff_id= $data['staffid'];
      $staff_name= $data['staffname'];
      $sqlinsert='INSERT INTO '.THREAD_ENTRY_TABLE.' SET pid='.$pid.',thread_id='.$thread
              .',staff_id='.$staff_id.',source='."'$source'".',title="Ticket Update",body='              ."'$body'".',format='."'$format'".',updated='."'$updated'".',user_id=0,type="M",poster='."'$staff_name'";
      $result = db_query($sqlinsert);
      if($result)
      {
        $resp = array("success"=>1);
      }
      else
      {
        $resp = array("success"=>0);
      }
      return Format::json_encode($resp);
    }
    function save($refetch=false) {
        if ($this->dirty || $refetch)
            $this->updated = SqlFunction::NOW();
        return parent::save($refetch || $this->dirty);
    }

    function update($vars,&$errors) {
      
        global $cfg;

        $vars['title'] = Format::striptags(trim($vars['title']));

        $id = isset($this->canned_id) ? $this->canned_id : null;
        if ($id && $id != $vars['id'])
            $errors['err']=__('Internal error. Try again');

        if (!$vars['title'])
            $errors['title'] = __('Title required');
        elseif (strlen($vars['title']) < 3)
            $errors['title'] = __('Title is too short. 3 chars minimum');
        elseif (($cid=self::getIdByTitle($vars['title'])) && $cid!=$id)
            $errors['title'] = __('Title already exists');

        if (!$vars['response'])
            $errors['response'] = __('Response text is required');

        if ($errors)
            return false;
        if(is_array($vars['dept_id']))
        {
          $f_dept_id = array_shift($vars['dept_id']);
          $extra_dept_id = implode(',', $vars['dept_id']);
        }
        else
        {
          $f_dept_id = $vars['dept_id'];
        }

        $this->dept_id = $f_dept_id ?: 0;
        $this->extra_dept_id = $extra_dept_id ?: NULL;
        $this->isenabled = $vars['isenabled'];
        $this->title = $vars['title'];
        $this->response = Format::sanitize($vars['response']);
        $this->notes = Format::sanitize($vars['notes']);

        $isnew = !isset($id);
        if ($this->save())
        {
           //-----Insertion and updation of custom fields ------//
           $action = $vars['do'];
           if($action=='create')
           {
            $lid=db_insert_id(); // Canned id (mst_canned_response)
            $this->changeCustomFields($lid,$vars,'insert');
           }
           elseif($action=='update')
           {
            $this->changeCustomFields($id,$vars,'update');
           } 
          return true;
        }
         
        if ($isnew)
            $errors['err'] = sprintf(__('Unable to update %s.'), __('this canned response'));
        else
            $errors['err']=sprintf(__('Unable to create %s.'), __('this canned response'))
               .' '.__('Internal error occurred');

        return true;
    }
    
    /****** Function for insertion and updation of custom fields*****/
    function changeCustomFields($canned_id,$vars,$action)
    {
      $sql1='canned_id='.$canned_id.
                ',status='.db_input($vars['status']).
                ',staff_id='.db_input($vars['assignee_staff']);
      
      if($action=='create')
      {
        $sqli='INSERT INTO '.TABLE_PREFIX.'canned_custom_fields SET '.$sql1;
        db_query($sqli);
      }
      else
      {
        //Also check if the records exists for old created canned responses
        // in custom fields table
        $sqlcount = 'SELECT COUNT(id) as count FROM '.TABLE_PREFIX.'canned_custom_fields'.
                          ' WHERE canned_id='.$canned_id;
        $res = db_query($sqlcount);
        $resArr = db_fetch_array($res);
        if($resArr['count'] >0)
        {
          $sqlu='UPDATE '.TABLE_PREFIX.'canned_custom_fields SET '.$sql1.
                    ' WHERE canned_id='.db_input($canned_id);
        }
        else
        {
          $sqlu='INSERT INTO '.TABLE_PREFIX.'canned_custom_fields SET '.$sql1;
        }
        db_query($sqlu); 
      }
      
    }
    
    /****Function for getting custom fields data from canned id ***/
    function getCustomInfo($canned_id)
    {
      
      $sql='SELECT c.status,c.staff_id,s.firstname,s.lastname FROM '.TABLE_PREFIX.'canned_custom_fields as c LEFT JOIN '.STAFF_TABLE.' as s ON c.staff_id=s.staff_id WHERE canned_id='.$canned_id;
      $response = array();
      if(($res=db_query($sql)) && db_num_rows($res))
      {
        while(list($status,$staff_id,$firstname,$lastname)=db_fetch_row($res))
        {
          $response['status']=  $status; 
          $response['staff_id']= $staff_id;
          $response['firstname']= $firstname;
          $response['lastname']= $lastname;
        }  
      }
      return $response;
      
    }
    
    // Function to get Order info data for ticket and returns Canned response
    // with order data replace in it
    function getOrderInfoToDisplay($orderid,$email='')
    {
       $ticketObj = new Ticket();
       $orderResponse  = $ticketObj->getOrderInfoFromCustomApi($orderid);
       $orderResponse  = json_decode($orderResponse,true);
       global $cfg;
       
       $configForWidget = json_decode($cfg->config['customWidgets']["value"],true);
       $imageBasePath = $cfg->config['liveImageBasePath']["value"];
       $configForShipmentUrl = $cfg->config['customShipmentUrl']["value"];
       $configForCbAccntUrl = $cfg->config['customCbAccount']["value"];

       $staticData = getCachedTicketFields();  
       
        $expression=$_SERVER['REQUEST_URI'];
        $expressArr=  explode('tickets', $expression); 
        $expressArr2=  explode('/', $expressArr[1]);
        $ticketId=$expressArr2[1];
        $returnId=TaskData::objects()->filter(array("order_id"=>$orderid,"ticket_id"=>$ticketId))->values('retrn_id')->limit(1)->all()[0]['retrn_id'];
        $returnRes=$ticketObj->getOrderInfoFromReturnApi($returnId,$orderid,$email='');
        $returnRes=json_decode($returnRes,true);
        $ReturnApiData = $returnRes["order_info"];
         if($ReturnApiData){
        $returnData=$ReturnApiData['Return_Data'];
        //$refundData=$ReturnApiData['Refund_Data'];
        }
        $orderData = $orderResponse[0]["order_info"];
        $productWeight=$orderData['product_weight'];
        $NeftDetails=$orderData['neft_details'];
        $cluebucks=$orderData['clue_bucks'];
        $productStatus=$orderData['product_status'];
        $paymentMode = $orderData['payment'];
        $manifestData = $orderData['manifest_data'];
        $sellerInfo = $ticketObj->getSellerInfoFromCustomApi($orderData['Seller_Info']['email']);
        $sellerInfo  = json_decode($sellerInfo,true);
        $customerInfo = $orderData['user_address'];
        $itemsData = $orderData['Items'];
        $shippingData = $orderData['Shipping_Data'];
        $pddEddData = $orderData['pdd_edd'];
        $delivery = $orderData['address'];
        
        if(!$returnData) {
            $refundData=$orderData['Refund_Data'];
        }
       
       //for return data 
        $requiredReturnData=array();
        foreach($returnData as $key=>$returnDetails)  {
        if($returnId == $returnDetails['return_id']) {
            $requiredReturnData[0]=$returnDetails;
			break;
          }
        }
        $returnCount=count($returnData);

        if(!$requiredReturnData)
            $requiredReturnData=$returnData;

        //for product details in case of return ticket   
        $productdata= array();
        foreach($itemsData as $key=>$item)  {
        if($requiredReturnData[0]['product'][0]['product_id'] == $item['product_id']) {
                $productdata[0]=$item;       
            }
        }
        
      if(!$productdata)
          $productdata=$itemsData;
    
       if(!$orderData["order_id"])
           return 0;
       foreach($statusArr as $k=>$v){
           $status[$v["status"]]=$v["description"];
       }
       
       if($configForWidget["order"]["customerinfo"]){
           $configUserUrl = $cfg->config['customUserUrl']["value"];
           $customerInf['Head'] = "Customer Details";
           $customerInf['Customer Name'] = '<div class="padding10"><div class="ajaxPDtitle">Customer Name : </div><div class="ajaxPDvalue"><a href="'.$configUserUrl.$orderData['user_id'].'" target="_blank" > '.ucfirst($customerInfo['s_firstname']).' '.ucfirst($customerInfo['s_lastname']).'</a></div></div>';
           $customerInf['Contact No'] = '<div class="padding10"><div class="ajaxPDtitle">Contact No : </div><div class="ajaxPDvalue"> '.$customerInfo['s_phone'].' </div></div>';
           $customerInf['Customer Address'] = '<div class="padding10"><div class="ajaxPDtitle">Customer Address : </div><div class="ajaxPDvalue"> '.ucfirst($customerInfo['s_address']).' </div></div>';
           $respArr["customerInfo"]=$customerInf;
       }
       
       if($configForWidget["order"]["sellerinfo"]){
           $configSellerUrl = $cfg->config['customSellerUrl']["value"];
           $sellerInf['Head'] = "Seller  Details";
           $sellerInf['SoldBy'] = '<div class="padding10"><div class="ajaxPDtitle">Sold  By : </div><div class="ajaxPDvalue"><a href="'.$configSellerUrl.$orderData['company_id'].'" target="_blank" > '.$sellerInfo['company'].'</a></div></div>';
           $sellerInf['Contact No'] = '<div class="padding10"><div class="ajaxPDtitle">Contact No : </div><div class="ajaxPDvalue"> '.$sellerInfo['phone'].' </div></div>';
           $sellerInf['Fulfillment Type'] = '<div class="padding10"><div class="ajaxPDtitle">Fulfillment Type : </div><div class="ajaxPDvalue"> '.$sellerInfo['fulfillment'].' </div></div>';
           $sellerInf['City/State'] = '<div class="padding10"><div class="ajaxPDtitle">City/State : </div><div class="ajaxPDvalue"> '.$sellerInfo['city'].'/'.$sellerInfo['state'].' </div></div>';
           $sellerInf['Store Status'] = '<div class="padding10"><div class="ajaxPDtitle">Store Status : </div><div class="ajaxPDvalue"> '.$sellerInfo['status_description'].' </div></div>';
           $sellerInf['Last 3 payout status & amount'] = '<div class="padding10"><div class="ajaxPDtitle">Last 3 payout status & amount :  </div>';
           $sellerInf['Latest'] = '<div class="padding10"><div class="ajaxPDtitle">Latest : </div><div class="ajaxPDvalue"> '.$sellerInfo['lastThreeRecordsData'][0]['current_status'].'/'.$sellerInfo['lastThreeRecordsData'][0]['request_amount'].' </div></div>';
           $sellerInf['2nd Latest'] = '<div class="padding10"><div class="ajaxPDtitle">2nd Latest : </div><div class="ajaxPDvalue"> '.$sellerInfo['lastThreeRecordsData'][1]['current_status'].'/'.$sellerInfo['lastThreeRecordsData'][1]['request_amount'].' </div></div>';
           $sellerInf['3rd Latest'] = '<div class="padding10"><div class="ajaxPDtitle">3rd Latest : </div><div class="ajaxPDvalue"> '.$sellerInfo['lastThreeRecordsData'][2]['current_status'].'/'.$sellerInfo['lastThreeRecordsData'][2]['request_amount'].' </div></div>';
           $respArr["sellerInfo"]=$sellerInf;
       }

       foreach ($orderData['Status_Transition'] as $k => $value) {
         $timelines[$value['to_status']] = $value['transition_date'];
         $lastTransDate = ($lastTransDate>$value['transition_date'])?$lastTransDate:$value['transition_date'];
       }
       if($configForWidget["order"]["basic"]){
          
            $configOrderUrl = $cfg->config['customOrderUrl']["value"];
            $orderYear = date("Y",$orderData["timestamp"]);
            $basic["Head"]="Order Details";
            $basic['orderId'] = "<div class='ajaxPDtitle'> Order Id :  </div>  <div class='ajaxPDvalue'><a href='".$configOrderUrl.$orderData['order_id']."'> ".$orderData['order_id']." </a> </div>";
            if(!empty($orderData['status_Description']['desc']))
            {
                
            $count=count($orderData['Status_Transition']);
            
           for($i=0;$i<$count;$i++)
           {
               $date=Format::datetime(date('d-m-Y H:i:s',$orderData['Status_Transition'][$i]['transition_date']));
          
               $from=$orderData['Status_Transition'][$i]['from_status'];
               $to= $orderData['Status_Transition'][$i]['to_status'];
               $FromStatus= $staticData["orderStatus"]["Order"]["$from"];
               $toStatus= $staticData["orderStatus"]["Order"]["$to"];
               
            $orderStatus= $orderStatus.$date." -> ". $FromStatus. " -> ".$toStatus."\n";
           }
                $basic['orderStatus'] = '<div class="padding10"><div class="ajaxPDtitle">Order status : </div><div class="ajaxPDvalue hoverIn" title="'.$orderStatus.'" > '.$orderData['status_Description']['desc'].' </div></div>'; 
           
             //   $basic['orderStatus'] = '<div class="padding10"><div class="ajaxPDtitle">Order status : </div><div class="ajaxPDvalue"> '.$orderData['status_Description']['desc'].' </div></div>';
                
            }
            //$basic["Trasition_Dt"] = "<div class='padding10'><div class='ajaxPDtitle'>Last Transition Dt :</div> <div class='ajaxPDvalue'>".Format::date(date('d-m-Y H:i:s',$lastTransDate))."</div></div>";
            $basic["orderPlace"]="<div class='padding10'><div class='ajaxPDtitle'>Order Place Date : </div><div class='ajaxPDvalue'>".date("d M Y",$orderData["timestamp"])."</div></div>";
            //$basic["Delvery"] = "<div class='padding10'><div class='ajaxPDtitle'>EDD/PDD :</div> <div class='ajaxPDvalue'>".$pddEddData['edd2']."/".$pddEddData['s_pdd']." ".$orderYear."</div></div>".
            //"<div class='padding10'><div class='ajaxPDtitle'>Delivery Date :</div> <div class='ajaxPDvalue'>".($timelines['H']?Format::datetime(date('d-m-Y H:i:s',$timelines['H'])):"NA")."</div></div>";
            $respArr["basic"]=$basic;
       }
       
       
       
       if($configForWidget["order"]["product"]){
            $configForProductUrl = $cfg->config['customProductUrl']["value"];
            $configForShopProductUrl = $cfg->config['customShopProductUrl']["value"];
           
            //PRODUCT INFO
            $product["Head"]="Product Details";
            foreach($productdata as $k=>$v){
                $pos = strpos($v['product_image_path'],'http');
                if($pos === FALSE)
                    $imageUrl = $imageBasePath.$v['product_image_path'];
                else
                    $imageUrl = $v['product_image_path'];
             if(empty($v["warranty_type"]))
                 $v["warranty_type"] = 'Not available';
           
             $Prod_Status=$productStatus[0]['status'];
           $ProductStatus= $staticData["orderStatus"]["Product"]["$Prod_Status"];
             
             $product["Product"].="<table><tr><td><img width='50px' height='50px' src='".$imageUrl."'></td><td><div class='ajaxPDvalue'><a href='".$configForProductUrl.$v["product_id"]."'>".$v["product_name"]."</a></div></td></tr></table>    <div class='ajaxPDtitle'> Product Id :  </div>  <div class='ajaxPDvalue'><a href='".$configForShopProductUrl.$v["product_id"]."' target='_blank' > ".$v["product_id"]." </a></div> </br> "
                     . "   <div class='ajaxPDtitle'> Warranty Type:  </div>  <div class='ajaxPDvalue'> ".$v["warranty_type"]."  </div> </br>   "
                 .   "  <div class='ajaxPDtitle'> Product Weight :  </div>  <div class='ajaxPDvalue'> ".$productWeight[$k][0]["weight"]."  </div></br>       <div class='ajaxPDtitle'> Status of Product :  </div>  <div class='ajaxPDvalue'> ".$ProductStatus."  </div>";
          
           }
            $respArr["product"]=$product;
       }
       
       if($configForWidget["order"]["manifestinfo"]){
           $configManifestUrl = $cfg->config['customManifestUrl']["value"];
           $manifestInf['Head'] = "Manifest Details";
           $manifestInf['Manifest Id'] = '<div class="padding10"><div class="ajaxPDtitle">Manifest Id : </div><div class="ajaxPDvalue"><a href="'.$configManifestUrl.$manifestData['manifest_id'].'" target="_blank" > '.$manifestData['manifest_id'].'</a></div></div>';
           $manifestInf['Manifest Created'] = '<div class="padding10"><div class="ajaxPDtitle">Manifest Created : </div><div class="ajaxPDvalue"> '.date("d M Y",strtotime($manifestData['date_created'])).' </div></div>';
           $manifestInf['Count of Orders'] = '<div class="padding10"><div class="ajaxPDtitle">Count of Orders : </div><div class="ajaxPDvalue"> '.$manifestData['count_orders'].' </div></div>';
           $respArr["manifestinfo"]=$manifestInf;
       }
       
       if($configForWidget["order"]["payment"]){
            
           //$gc_bc = $orderData['gc_used'] + $orderData['cb_used'];
         
//           if(!empty($paymentMode))
//               $paymentMethod = $paymentMode['method'];
//           else
//           $paymentMethod = 'Not Available';
           $configPayoutReportUrl = $cfg->config['customPayoutReportUrl']["value"];
           $configAdjustmentUrl = $cfg->config['customAdjustmentUrl']["value"];
           $payment["Head"]="Payment Details";
           //$payment["paymentMethod"] = '<div class="padding10"><div class="ajaxPDtitle">Payment Mode : </div><div class="ajaxPDvalue"> '.$paymentMethod.' </div></div>';
           //$payment["discount"] = '<div class="padding10"><div class="ajaxPDtitle">Discount Applied: </div><div class="ajaxPDvalue"> '.$orderData['discount'].' </div></div>';
           //$payment["subtotal"] = '<div class="padding10"><div class="ajaxPDtitle">SubTotal : </div><div class="ajaxPDvalue"> '.$orderData['subtotal'].' </div></div>';
           //$payment["GC"] = '<div class="padding10"><div class="ajaxPDtitle">GC Used : </div><div class="ajaxPDvalue"> '.$orderData['gc_used'].' </div></div>';
           //$payment["CB"] = '<div class="padding10"><div class="ajaxPDtitle">CB Used : </div><div class="ajaxPDvalue"> '.$orderData['cb_used'].' </div></div>';
           //$payment["shippingCost"] = '<div class="padding10"><div class="ajaxPDtitle">Shipping Cost : </div><div class="ajaxPDvalue"> '.$orderData['shipping_cost'].' </div></div>';
           //$payment["CodprosessingFee"] = '<div class="padding10"><div class="ajaxPDtitle">Cod Processing Fee : </div><div class="ajaxPDvalue"> '.$orderData['cod_fee'].' </div></div>';
           //$payment["cluebucks"] = '<div class="padding10"><div class="ajaxPDtitle">ClueBucks Earned : </div><div class="ajaxPDvalue"> '.$cluebucks[0]['data'] .' </div></div>';
           
           $payment["billingStatus"] = '<div class="padding10"><div class="ajaxPDtitle">Billing Status : </div><div class="ajaxPDvalue"> '.$orderData['billing_done']  .' </div></div>';
           $payment["totalPaid"] = '<div class="padding10"><div class="ajaxPDtitle">Total paid : </div><div class="ajaxPDvalue"><a href="'.$configPayoutReportUrl.'" target="_blank" > '.$orderData['total']  .'</a></div></div>';
           $payment["adjustmentPanel"] = '<div class="padding10"><center><div class="ajaxPDvalue" align-text="center" ><a href="'.$configAdjustmentUrl.$orderData['order_id'].'" target="_blank" > Adjustment Panel </div></center></div>';
           $respArr["payment"]=$payment;
       }
       
       if($configForWidget["order"]["shipment"]){
            $configForShipmentUrl = $cfg->config['customShipmentUrl']["value"];
            $shipping["address"]="<center>".$orderData["s_address"].",".$orderData["s_city"].", ".$orderData["s_state"].",".$orderData["s_country"]."</center>";
            foreach($shippingData as $k=>$v){
                if(!empty($v["title"])){
                    $showTitle = "<td style='width:20px;border: 1px solid #aaa;'>".$v["title"]."</td>";
                }
                if($v['reverse_shipment'] == 1)
                    $reverse = 'YES';
                else
                    $reverse = 'NO';
                if(empty($v['origin']))
                    $v['origin'] = 'N/A';
                if(empty($v['destination']))
                    $v['destination'] =  'N/A';
                $carrier_id = $v['carrier_id'];
                $trackingNo = $v['tracking_number'];
                //$titleTr = 'This Tracking No. is not trackable from here';
            if(is_int($k))
            {              $shipping["ship".$k]="<table width='100%' style='padding: 4px;border: 1px solid #aaa;'>"
                    //. "<tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Shipment ID : </div><div class='ajaxPDvalue'><a href='".$configForShipmentUrl.$v['shipment_id']."'> ". $v['shipment_id'] ." </a></div></div>  </td></tr>   "
                    . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Tracking No. : </div><div class='ajaxPDvalue hoverIn' title='".$titleTr."' id='tracking_no' data-carrier='".$carrier_id."' data-billing-carrier='".$v['billing_carrier_id']."'>  ".$trackingNo."</div></div>  </td></tr>   "
                    . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Carrier : </div><div class='ajaxPDvalue'> ".$v['carrier']." </div></div>  </td></tr>  "
                    //. "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Origin : </div><div class='ajaxPDvalue'> ".$v['origin']." </div></div>  </td></tr>    "
                    //. "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Destination : </div><div class='ajaxPDvalue'> ".$v['destination']." </div></div>  </td></tr>    "
                    //. "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Is Reverse ship : </div><div class='ajaxPDvalue'> ".$reverse." </div></div>  </td></tr>    "
                    //. " <tr><td>  <div class='padding10'><div class='ajaxPDtitle'>Shipping Date :</div> <div class='ajaxPDvalue'>".($timelines['A']?Format::datetime(date('d-m-Y H:i:s',$timelines['A'])):"NA")."</div></div>  </td></tr>  "
                    . "</table>";
            }
            }
            $shipping["Head"]="Shipment Details";
            $respArr["shipping"]=$shipping;
       }
       
       if($configForWidget["order"]["return"] && !empty($returnData)){
            $return["Head"]="Return Details";
            $singleSignonAuthUrl = json_decode($cfg->config['singleSignonAuthUrl']['value'], true);
            $configOrderUrl = $cfg->config['customOrderUrl']["value"];
            $link=$singleSignonAuthUrl['defaultDomain'];
            $link.='/UniTechCity.php?dispatch=rma.details&return_id=';
            if(!empty($orderData['FIR_DATE']))
            $return["FirDate"]='<div class="padding10"><div class="ajaxPDtitle"> FIR Date : </div><div class="ajaxPDvalue"> '.$orderData['FIR_DATE']  .' </div></div>  </br>';
            //RETURN INFO
            foreach($requiredReturnData as $k=>$data){
                if(is_int($k)){
                    if(!empty($data['reverse_shippment'])){
                        $shipId = $data['reverse_shippment'][0]['shipment_id'];
                        $carrier = $data['reverse_shippment'][0]['carrier'];
                    }
                    else{
                        $shipId = $shippingData[0]['tracking_number'];
                        $carrier = $shippingData[0]['carrier'];
                    }
                   
             //last transition date 
             $index = count($data['rma_History'])-1;        
               if(count($data['rma_History']))
                  $lastUpdateDate = $data['rma_History'][$index]['datetime'];
                else
                   $lastUpdateDate=$data['timestamp']; 
           //return history 
              $count=count($data['rma_History']);
 
             if($data['images']) {
                 foreach($data['images'] as $key=>$image)
                 { if(strpos($image['image_path'],"cdn.shopclues.com"))
                        $imagepath=$image['image_path'];
                   else
                        $imagepath="http://cdn.shopclues.com/".$image['image_path'];  
           
                   $returnImages.= "<tr><td><a href='".$imagepath."' target='_blank'>Attachment".++$key."</a></td></tr>";  
                 }
             }
             
             if(!$returnImages)
                 $returnImages='N/A';
            
                 
         
              for($i=0;$i<$count;$i++)
           {
            $date=Format::datetime(date('d-m-Y H:i:s',$data['rma_History'][$i]['datetime']));
               $from=$data['rma_History'][$i]['statusFromDes']['desc'];
               $to= $data['rma_History'][$i]['toStatusDes']['desc'];
               if($data['rma_History'][$i]['status_to']=='I')
               {
                   $pickupDate=Format::datetime(date('d-m-Y H:i:s',$data['rma_History'][$i]['datetime']));
               }
               if($data['rma_History'][$i]['status_to']=='J' || $data['rma_History'][$i]['status_to']=='114')
               {
                   $returnDate=Format::datetime(date('d-m-Y H:i:s',$data['rma_History'][$i]['datetime']));
               }
             //  $FromStatus= $staticData["orderStatus"]["Order"]["$from"];
               //$toStatus= $staticData["orderStatus"]["Order"]["$to"];
               
            $returnStatus.= $date." -> ". $from. " -> ".$to."\n";
           } 
           if($data['refund_in_ac']=='Y')
                $refundMode='Refund in Account';
           elseif($data['refund_in_cb']=='Y')
                $refundMode='Refund in ClueBucks';
           else
                $refundMode='N/A';
           
           if($NeftDetails[0]['is_active']=='Y')
                $isactive='Yes';
           elseif($NeftDetails[0]['is_active']=='N')
                $isactive='No';
           else
                $isactive='N/A';
   
    $ac_no= str_repeat('X', strlen($NeftDetails[0]['account_no']) - 4) . substr($NeftDetails[0]['account_no'], -4);     
           
                
          if($data['return_address_id']==1)
          { 
              $ShippingAddress=$orderData["s_address"].",".$orderData["s_city"].", ".$orderData["s_state"].",".$orderData["s_country"];
          } else
          {  $ShippingAddress=$orderData["user_address"]["s_address"].",".$orderData["user_address"]["s_city"].", ".$orderData["user_address"]["s_state"].",".$orderData["user_address"]["s_country"];
           }
        $returnAction= getCachedReturnFlowFields()['return_action'][$data['action']];
        $returnReason= getCachedReturnFlowFields()['sub_reason'][$data['product'][0]['reason']];
        $return["return".$k] = "<table width='100%' style='padding: 4px;border: 1px solid #aaa;'>" 
             . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Order ID. : </div><div class='ajaxPDvalue'> <a href='".$configOrderUrl.$orderData['order_id']."' target='_blank'> ".$orderData['order_id']." </div></div>  </td></tr>   "
             . "<tr><td> <div class='padding10'><div class='ajaxPDtitle'>Order Status : </div><div class='ajaxPDvalue'> ".$orderData['status_Description']['desc']." </div></div>  </td></tr>   "
             . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Return Quantity : </div><div class='ajaxPDvalue'> ".$data['total_amount']." </div></div>  </td></tr>  "
             //. "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Last Transition dt. : </div><div class='ajaxPDvalue'> ".date("d M Y, g:ia",$lastUpdateDate)." </div></div>  </td></tr>   "
             . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Tracking ID. : </div><div class='ajaxPDvalue'> <a href='".$configForShipmentUrl.$shipId."' target='_blank'> ".$shipId." </a> </div></div>  </td></tr> "
             . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Carrier Name : </div><div class='ajaxPDvalue'> ".$carrier." </div></div>  </td></tr>  " 
             . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Delivered Date : </div><div class='ajaxPDvalue'> ".$returnDate." </div></div>  </td></tr>  " 
             /*. "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Reason for Return : </div><div class='ajaxPDvalue'> ".$returnReason." </div></div>  </td></tr>"               .  "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Date of Intiation : </div><div class='ajaxPDvalue'> ".date("d M Y, g:ia",$data['timestamp'])." </div></div>  </td></tr>  "
             . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Return Amount : </div><div class='ajaxPDvalue'> ".$data['product'][0]['price']." </div></div>  </td></tr>  "
             . " <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Pickup Address : </div><div class='ajaxPDvalue'>".$ShippingAddress."</div></div></td></tr> "
                . " <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Pickup Date : </div><div class='ajaxPDvalue'> ".$pickupDate." </div></div>  </td></tr> "
             ." <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Return Action : </div><div class='ajaxPDvalue'> ".$returnAction." </div></div>  </td></tr> "
             . " <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Refund Mode : </div><div class='ajaxPDvalue'> ".$refundMode." </div></div>  </td></tr> 
      <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Images : </div><div class='ajaxPDvalue'> ".$returnImages." </div></div>  </td></tr>
            <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> NEFT Details :-</div><div class='ajaxPDvalue'>
            <table> <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Bank Name : </div><div class='ajaxPDvalue'> ".$NeftDetails[0]['bank']." </div></div>  </td></tr> <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Account Number : </div><div class='ajaxPDvalue'> ".$ac_no." </div></div>  </td></tr><tr><td>  <div class='padding10'><div class='ajaxPDtitle'> IFSC code : </div><div class='ajaxPDvalue'> ".$NeftDetails[0]['ifsc_code']." </div></div>  </td></tr> <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Bank Branch : </div><div class='ajaxPDvalue'> ".$NeftDetails[0]['bank_branch']." </div></div>  </td></tr> <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Is Active : </div><div class='ajaxPDvalue'> ".$isactive." </div></div>  </td></tr>*/
            ."</table>";
        if($returnCount){
        $return["return".$k].="<tr><td>  <div class='ajaxPDvalue'> Total Returns for this Order: ".$returnCount." </div>  </td></tr>";}
                                
             $return["return".$k].="</div></div>  </td></tr>  " 
                . " </table>";
                }
   $returnStatus=null; $returnImages=null;
                }
            $respArr["return"]=$return;
       }
       
       
       /*if($configForWidget["order"]["refund"] && !empty($refundData)){
         
           $refundTransitionHistory = $ReturnApiData['Refund_History'];//['return_refund_data']['response']['refund_transition_history'];
           if(!$refundTransitionHistory){
               $refundTransitionHistory=$orderData['Refund_History'];
           }
           $lastUpdateTimeRefund = $refundTransitionHistory[count($refundTransitionHistory)-1]['updated_time'];
              
               foreach($refundData as $k=>$v ){
                   if(empty($v['transaction_id']))
                       $v['transaction_id'] = 'N/A';
                   //for refund Status
                   $Ref_stat=$v['status'];
                   $Status=$staticData["orderStatus"]["Refund"]["$Ref_stat"];
                  
                  
                   //refund history 
              $count=count($refundTransitionHistory);
      for($i=0;$i<$count-1;$i++)
         {
            $date=Format::datetime(date('d-m-Y H:i:s',$refundTransitionHistory[$i]['updated_time']));
            $from=$refundTransitionHistory[$i]['StatusDescription'];
           $to= $refundTransitionHistory[$i+1]['StatusDescription'];
            $refundStatus.= $date." -> ". $from ." -> " .$to. "\n";
         } 
           $refund["$refund".$k] = "<table width='100%' style='padding: 4px;border: 1px solid #aaa;'>"
       . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Mode of Refund : </div><div class='ajaxPDvalue'> ".$v["refund_mode"]." </div></div>  </td></tr>  "
                . "<tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Refund Amount : </div><div class='ajaxPDvalue'> ". $v['total_refund'] ." </div></div>  </td></tr>   ";
             if(!empty($lastUpdateTimeRefund))
                 $refund["$refund".$k].=  "<tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Last Update Date : </div><div class='ajaxPDvalue'> ".date('d M Y, g:ia',$lastUpdateTimeRefund)  ." </div></div>  </td></tr>   " ;
                $refund["$refund".$k].= "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> RRN/UTR No. : </div><div class='ajaxPDvalue'> ".$v['transaction_id']." </div></div>  </td></tr>   "
                . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Refund Status : </div><div class='ajaxPDvalue hoverIn' title='$refundStatus'> ".$Status." </div></div>  </td></tr>  " 
                . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Date of Requested : </div><div class='ajaxPDvalue'> ".Format::datetime(date('d-m-Y H:i:s',$v['timestamp']))." </div></div>  </td></tr>  "
                . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Expected date of Refund : </div><div class='ajaxPDvalue'> ".Format::datetime(date('d-m-Y H:i:s',$v["batch_id"]))." </div></div>  </td></tr> "
                . "      </table>";
         }
                $refund['Head'] = 'Refund Details';
                $respArr["refund"]=$refund;
           }*/
        if($configForWidget["order"]["contact"] && !empty($orderData)){
               
        $contact["contact"] = "<table width='100%' style='padding: 4px;border: 1px solid #aaa;'>"
            . "<tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Phone : </div><div class='ajaxPDvalue'> ". $orderData['phone'] ." </div></div>  </td></tr>   "
            . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Billing Phone. : </div><div class='ajaxPDvalue'> ".$orderData['b_phone']." </div></div>  </td></tr>   "
            . "  <tr><td>  <div class='padding10'><div class='ajaxPDtitle'> Shipping Phone : </div><div class='ajaxPDvalue'> ".$orderData['s_phone']." </div></div>  </td></tr>  " 
            . "      </table>";
        $contact['Head'] = 'Contact Details';
        $respArr["contact"]=$contact;
        }
       return $respArr;
    }
    // Function to get Order info data for ticket and returns Canned response
    // with order data replace in it

    function getReturnInfoForTicket($returnData,$dataKey,$tags,$cannText,$tkey,$orderData='')
    {    $NeftDetails=$orderData['neft_details'];
             $getFieldForOrder = explode('.', $tags); 
            $field = $getFieldForOrder[1];
            $charPos= strpos($field,"}"); // position of closing order vars
            $field = substr($field, 0,$charPos);
        
              switch ($field) { 
                case 'id': 
                  $replacedValue = $returnData[$dataKey]['return_id'];
                  break;
             case 'action':  //return_action
                  $replacedValue =getCachedReturnFlowFields()['return_action'][$returnData[$dataKey]['action']];
                  break;
                case 'status':
                  $replacedValue = $returnData[$dataKey]['Status_Description']['desc'];
                  break;
                 case 'reason':
                  $replacedValue = getCachedReturnFlowFields()['sub_reason'][$returnData[$dataKey]['product'][0]['reason']];
                  break;
               case 'bank_ac':
                $replacedValue =str_repeat('X', strlen($NeftDetails[0]['account_no']) - 4) . substr($NeftDetails[0]['account_no'], -4); 
                  break;
              case 'bank_name':
                $replacedValue = $NeftDetails[0]['bank'];
                  break;
              case 'bank_branch':
                $replacedValue = $NeftDetails[0]['bank_branch'];
                  break;
              case 'bank_ifsc':
                $replacedValue = $NeftDetails[0]['ifsc_code'];
                  break;
              case 'address':
                if($returnData[$dataKey]['return_address_id']==1)
          { 
              $ShippingAddress=$orderData["s_address"].",".$orderData["s_city"].", ".$orderData["s_state"].",".$orderData["s_country"];
          } else
          {  $ShippingAddress=$orderData["user_address"]["s_address"].",".$orderData["user_address"]["s_city"].", ".$orderData["user_address"]["s_state"].",".$orderData["user_address"]["s_country"];
           }     $replacedValue = $ShippingAddress;
                  break;
              case 'quantity':
                  $replacedValue = $returnData[$dataKey]['total_amount'];
                  break;
                case 'product': 
                   foreach($returnData[$dataKey]['product'] as $product)
                {
                   $products=$products.$product['product'].', ' ;
                    }
                  $replacedValue=$products;
                  break;
              
               case 'amount':
                    foreach($returnData[$dataKey]['product'] as $price)
                {
                   $Totalprice+=$price['price'] ;
                    }
                  $replacedValue=$Totalprice;
                  break;
              case 'date':
                  $replacedValue = date("D,M j Y g:i a",$returnData[$dataKey]['timestamp']);
                  break;
              case 'mode':
                  if($returnData[$dataKey]['refund_in_ac']=='Y')
                    $refundMode='Refund in Account';
                  elseif($returnData[$dataKey]['refund_in_cb']=='Y')
                    $refundMode='Refund in ClueBucks';
                 else
                    $refundMode='N/A';
                      $replacedValue = $refundMode;
                  break;
                default:
                  # code...
                  break;
              }
          
            return $replacedValue;
            }
    

     function getOrderInfoForTicket($return_id,$orderid,$cannText)
    { //for ticketId 
        
        $expression=$_SERVER['REQUEST_URI'];
        $expressArr=  explode('tickets', $expression);
        $expressArr2=  explode('/', $expressArr[1]);
        $ticketId=$expressArr2[1]; 
        $i=0;
       //task details
       $ticketObj = new Ticket();
       $taskObj = Task::objects()->filter(array("object_type"=>"T","object_id"=>$ticketId));
          foreach($taskObj as $taskeach) {
                $type=$taskeach->getTaskType();
                $type1=strtolower($type);
               $type2=str_replace(' ','',$type1);
                $taskType[$i]=$type2;
                $updated[$i]=$taskeach->getDueDate();
                $i++;
                
          }
           
          if($ticketId){
            $condition = array("ticket_id"=>$ticketId);
        }else{
             $condition = array("order_id"=>$orderid);
             if($return_id)
                 $condition["retrn_id"] = $return_id;
        }
             
              $ticketData=TicketOData::objects()->filter($condition)->values()->limit(1)->all();
         
      // Order Data
       // custom api called to get pdd/edd of order
          //retrieve return_id for pre existing ticket from odata corresponding to ticket_id
            $returnId=$ticketData[0]['retrn_id'];
            
           // return_id for new ticket
            if($return_id) {
            $returnId=$return_id;
            }         
    $responseOrderData = $ticketObj->getOrderInfoFromCustomApi($orderid,"");
       $responseOrderData = json_decode($responseOrderData,true);
       $orderData = $responseOrderData['order_info'];
    
       if(stristr($cannText,'%{return')){
            $responseReturnData = $ticketObj->getOrderInfoFromReturnApi($returnId,$orderid,$email='');
       }
       $responseReturnData = json_decode($responseReturnData,true);
       $ReturnData = $responseReturnData['order_info'];
       
       $pddEddData = $orderData['pdd_edd'];
       $returnData=$ReturnData['Return_Data'];
     //  $response  = $ticketObj->getOrderInfoFromSoaApis($orderid);
       //$response  = json_decode($response,true);
$dataKey=0;
       foreach($returnData as $key=>$returnDetails)  {
       if($returnId == $returnDetails['return_id']) {
          $dataKey=$key;
          }
        }
       // Cann Data
       //$cannText = $ticketObj->getCannedText($cann);
       $cannText = str_replace('<br />', '% ', $cannText);
       $cannText= explode(' ', $cannText);
       
       // Replace dynamic tag with order data
       $i=0;
       $finalCannResponse = array();
       foreach($cannText as $tkey=> $tags)
       {
          if (strpos($tags, '%{') === 0) {
                if(strpos($tags, 'return.')) {
    $replacedValue = Canned::getReturnInfoForTicket($returnData,$dataKey,$tags,$cannText,$tkey,$orderData);    
         }

             elseif (strpos($tags, 'task.')) {
                    $getFieldForTask = explode('.', $tags);
                    if ($getFieldForTask[1] == 'comment') {
                        $taskcomment = Task::objects()->filter(array("object_id" => $ticketId))->values('data__taskComment')->order_by('-id')->first()['data__taskComment'];
                        $field = $getFieldForTask[2];
                        $charPos = strpos($field, "}"); // position of closing order vars
                        $field = substr($field, 0, $charPos);
                        //[LIST-Call Details||Phone Details||Acc Number]

                        if (stristr($taskcomment, '[LIST-')) {
                            preg_match_all("/\[([^\]]*)\]/", $taskcomment, $tasktemp);
                            $taskcomment = $tasktemp[1][0]; //print_r($tasktemp) ;die;
                            $taskcomment = str_replace('LIST-', '', $taskcomment);
                            //  $taskcomment=  str_replace(']','',$taskcomment);
                            $taskcomment = explode('||', $taskcomment);
                            $commentCount = count($taskcomment);
                            if ($field == 'list') {
                                $replacedValue .='<ol>';
                                for ($i = 1; $i <= $commentCount; $i++) {
                                    $replacedValue.='<li>' . $taskcomment[$i - 1] . '</li>';
                                } $replacedValue.='</ol>';
                            } elseif ($field == 'comma') {
                                $replacedValue.=$taskcomment[0];
                                for ($i = 1; $i < $commentCount; $i++) {
                                    $replacedValue.=',' . $taskcomment[$i];
                                }
                            }
                        }
                    }
                } elseif (stristr($tags,"order.")) {
          $getFieldForOrder = explode('.', $tags); 
            $field = $getFieldForOrder[1];
            $charPos= strpos($field,"}"); // position of closing order vars
            $field = substr($field, 0,$charPos);
      if($ticketData[0][$field]){
              $replacedValue=$ticketData[0][$field];
          if($replacedValue==1000000){
              $replacedValue='Undefined';
            }
         }      
      else {            
             if(stristr($field,"items"))
             {
              $fieldArray = explode('_', $field);
              switch ($fieldArray[1]) {
                case 'product':
                  $replacedValue = $orderData['Items'][$fieldArray[2]-1]['product_name']?$orderData['Items'][$fieldArray[2]-1]['product_name']:'     ';
                  break;
                case 'pddEdd':
                  $replacedValue=$pddEddData['edd2']."/".$pddEddData['s_pdd'];
                  break;
                case 'carrier':
                  $replacedValue = $orderData['Shipping_Data'][$fieldArray[2]-1]['carrier']?$orderData['Shipping_Data'][$fieldArray[2]-1]['carrier']:'     ';
                  break;
                case 'trackingNo':
                  $replacedValue = $orderData['Shipping_Data'][$fieldArray[2]-1]['tracking_number']?$orderData['Shipping_Data'][$fieldArray[2]-1]['tracking_number']:'    ';
                  break;
                default:
                  # code...
                  break;
              }
            }
          elseif(stristr($field,"updated_sla_"))
          { 

 //print_r($field);
 $fieldarr=explode('updated_sla_', $field);
            $checktask = strtolower($fieldarr[1]);
          
               $checkType= str_replace('_', '', $checktask) ;
               
           foreach ($taskType as $key => $value) {
            if($value==$checkType) {
              $replacedValue="Next communication date is ".Format::daydatetime($updated[$key]);                  }  
            }  
         }
          }
     }
            $charPosTemp = strpos($tags,"}");
            $replaceThis = substr($tags,0,$charPosTemp+1);
            $cannText[$tkey]=str_replace($replaceThis, $replacedValue, $tags); 
           //Canned Order Field should be same as order data Field (must)    
          } $replacedValue=null; 
         $i++;
      }
          
       $res = implode(" ",$cannText); 
       $res = str_replace('% ', '<br />', $res);
       return $res;
    }
}
    
RolePermission::register( /* @trans */ 'Knowledgebase', Canned::getPermissions());

?>