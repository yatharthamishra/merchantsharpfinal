<?php
    $filePath=$_SESSION['fileDownloadPath'];
    if(empty($filePath)){
        die('FilePath required for downloading');
    }
    if (file_exists($filePath)) {

        //set appropriate headers
        header('Content-Description: File Transfer');
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();

        //read the file from disk and output the content.
        readfile($file);
        exit;
    }else{
        die('File does not exists');
    }
    unset($_SESSION['fileDownloadPath']);
    unlink($filePath);


?>
