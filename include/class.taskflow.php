<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TaskFlowModel extends VerySimpleModel {

    static $meta = array(
        'table' => TASK_FLOW_TABLE,
        'ordering' => array('task_id'),
        'pk' => array('task_id')
    );

}

class TaskFlow extends TaskFlowModel {

    static function lookup($id) {
        $taskFlow = self::objects()->filter(array("flowstatus"=>1,'task_id'=>$id))->first();
        return $taskFlow; 
    }
    
    function createNext($nextTask, $parentTaskId, $tid, $listFormated,$orderId,$manifestId,$taskVariableData,$taskDataPrevious,$taskData) {
        $ticketObj = Ticket::lookup($tid);
        $data["ticketId"] = $tid;
        $data["parent_task_id"] = $parentTaskId;
        $data["sla"] = $sla;
        $data["type"] = array_keys($listFormated["Task Types"][$nextTask->type])[0];
        $data["type_id"] = $nextTask->type;
$data["description"] = array_values($listFormated["Task Types"][$nextTask->type])[0];
        $data["title"]=array_values($listFormated["Task Types"][$nextTask->type])[0];
                
	$data["source"] = $listFormated["Task Sources"][3];
        $data["status"] = $listFormated["Task Status"][1];
        $data["duedate"] = $ticketObj->est_duedate;
        $data['order_id']=$orderId;
        $data['manifest_id']=$manifestId;
        $data['product_id']=$taskData->product_id;
        $data['batch_id']=$taskData->batch_id;
        $data["deptId"] = (isset($dept) ? $dept : $nextTask->dept_id);
        if(isset($taskVariableData['taskExtraField'])){
         $data["extra"]=$taskVariableData['taskExtraField'];
        }
        if(isset($taskVariableData['attachmentRequired'])){
         $data["is_attachment_required"]=$taskVariableData['attachmentRequired'];
        }
$newtask=$this->createTask($data,$taskDataPrevious);
return $newtask;
    }

    function createTask($data,$taskDataPrevious) {

        # Pull off some meta-data
        $alert = (bool) (isset($data['alert']) ? $data['alert'] : true);
        $autorespond = (bool) (isset($data['autorespond']) ? $data['autorespond'] : true);
       
        # Assign default value to source if not defined, or defined as NULL
        $data['source'] = isset($data['source']) ? $data['source'] : 'API';
        # Create the task with the data (attempt to anyway)
        $dataTask = array(
            "object_type" => "T",
            "object_id" => $data["ticketId"],
            "creater" => "System",
            "duedate" => $data["duedate"],
            "internal_formdata" => array(
                "dept_id" => $data["deptId"],
                "parent_task_id" => $data["parent_task_id"],
            ),
            "order_id"=>$data['order_id'],
            "manifest_id"=>$data['manifest_id'],
            "product_id"=>$data['product_id'],
            "batch_id"=>$data['batch_id'],
            "default_formdata" => array(
                "taskType" => $data["type"],
                "title" => $data["description"],
                "description" => $data["description"],
                "taskSource" => $data["source"],
                "taskStatus" => $data["status"],
                "extra"=>$data["extra"],
                "is_attachment_required"=>$data["is_attachment_required"]
            )
        );
        $dataTask['previousData']=$taskDataPrevious;
        $slaObj = TaskSLA::lookup($data["type_id"]);
        if ($slaObj && $slaObj->sla_id) {
            $slaId = $slaObj->sla_id;
            $dataTask["internal_formdata"]["sla"] = $slaId;
        } else {
            $dataTask["internal_formdata"]["duedate"] = $data["duedate"];
        }
        $task = Task::create($dataTask);

        # Return errors (?)
        if (count($errors)) {
            if (isset($errors['errno']) && $errors['errno'] == 403)
                return $this->exerr(403, __('Task denied'));
            else
                return $this->exerr(
                                400, __("Unable to create new task: validation errors") . ":\n"
                                . Format::array_implode(": ", "\n", $errors)
                );
        } elseif (!$task) {
            return $this->exerr(500, __("Unable to create new ticket: unknown error"));
        }

        return $task;
    }

}
