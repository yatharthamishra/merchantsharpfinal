<?php

Class TicketClosure{
    
    public function __construct() {
        ;
    }
    public function autoCloseTickets(){
        global $cfg;
        $ticketAutoClosureTime=$cfg->getTimeToCloseTicket();
        $cachedFields=getCachedTicketFields();
        $statusIdMap=$cachedFields['statusIdMap'];
        $tickets=$this->getTicketsToAutoClose($ticketAutoClosureTime);
        foreach ($tickets as $ticketDetail){
            $ticketObj=new Ticket();
            foreach ($ticketDetail as $keyName=>$keyValue){
                $ticketObj->ht[$keyName]=$keyValue;
            }
            $ticketObj->ht=$ticketDetail;
            $ticketObj->setStatus($statusIdMap['closed']);
        }
        
        
    }
    
    public function getTicketsToAutoClose($ticketAutoClosureTime){
        $tickets=[];
        $ticketAutoClosureTime=(int)$ticketAutoClosureTime;
        if($ticketAutoClosureTime<=0){
            return array();
        }
        $sql="SELECT 
                ticket_id
              FROM
                mst_ticket where  
                status_id=7 and 
                 (closed ='0000-00-00 00:00:00' or closed is null)  ";
//		AND solvedDateTime <= DATE_SUB(NOW(), INTERVAL $ticketAutoClosureTime HOUR)
  //              ";
        $rs=db_query($sql);
        while($row=  db_fetch_array($rs)){
            $tickets[]=$row;
        }
	return $tickets;
    }
}
?>
