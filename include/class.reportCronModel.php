<?php

Class ReportCronModel {

    public function __construct() {
        _set_db_connection('report');
    }

    public function insertDataIntoPendencyTable($dataToBeInserted) {
        foreach ($dataToBeInserted as $key => $data) {
            $insertValueSql[] = "('" . join("','", array_values($dataToBeInserted[$key])) . "')";
        }
        $columnNames = array_keys($dataToBeInserted[0]);
        foreach ($columnNames as $key => $colName) {
            $columnNames[$key] = 'mst_pendency_report.' . $columnNames[$key];
            if ($colName != 'date' && $colName != 'status_id') {
                $onDuplicateKeySyntax[$key].="mst_pendency_report." . $colName . "=VALUES(mst_pendency_report." . $colName . ")";
            }
        }
        $insertSytax = "Insert into mst_pendency_report  ( " . join(",", $columnNames) . " ) VALUES " . implode(',', $insertValueSql);
        $onDuplicateKey = "ON DUPLICATE KEY UPDATE " . implode(',', $onDuplicateKeySyntax);
        $finalSql = $insertSytax . $onDuplicateKey;
        db_query($finalSql);
        return db_error();
    }
    public function getOldDataForPendency($fromDate,$toDate){
        if($fromDate==''){
            $fromDate='1000-01-01';
        }
        $sql="select * from mst_pendency_report where DATE(date) between '$fromDate' and '$toDate' and status_id=1";
        $rs=db_query($sql);
        $pendencyData=[];
        while($row=  db_fetch_array($rs)){
            $dateOnly=date('Y-m-d',  strtotime($row['date']));
            $pendencyData[$dateOnly]=$row;
        }
        return $pendencyData;
    }

    public function insertDataIntoSlaSummaryTable($dataToBeInserted) {
        foreach ($dataToBeInserted as $key => $data) {
            $insertValueSql[] = "('" . join("','", array_values($dataToBeInserted[$key])) . "')";
        }
        $columnNames = array_keys($dataToBeInserted[0]);
        foreach ($columnNames as $key => $colName) {
            $columnNames[$key] = 'mst_sla_summary_report.' . $columnNames[$key];
            if ($colName != 'date' && $colName != 'status_id') {
                $onDuplicateKeySyntax[$key].="mst_sla_summary_report." . $colName . "=VALUES(mst_sla_summary_report." . $colName . ")";
            }
        }
        $insertSytax = "Insert into mst_sla_summary_report  ( " . join(",", $columnNames) . " ) VALUES " . implode(',', $insertValueSql);
        $onDuplicateKey = "ON DUPLICATE KEY UPDATE " . implode(',', $onDuplicateKeySyntax);
        $finalSql = $insertSytax . $onDuplicateKey;
        db_query($finalSql);
        return db_error();
    }
    public function getOldDataForSlaSummary($fromDate,$toDate){
        if($fromDate==''){
            $fromDate='1000-01-01';
        }
        $sql="select * from mst_sla_summary_report where DATE(date) between '$fromDate' and '$toDate' and status_id=1";
        $rs=db_query($sql);
        $slaSummaryData=[];
        while($row=  db_fetch_array($rs)){
            $dateOnly=date('Y-m-d',  strtotime($row['date']));
            $slaSummaryData[$dateOnly]=$row;
        }
        return $slaSummaryData;
    }

}
?>
