<?php
include_once INCLUDE_DIR . 'class.api.php';
include_once INCLUDE_DIR . 'class.thread.php';
class TicketCommentApiController{
	public function fetch(){
		$ticketNo=$_GET['ticketNo'];
		$result=array();
		if(!empty($ticketNo)){
			$obj = ThreadEntry::objects()->filter(array('thread__ticket__number'=>$ticketNo,'poster__neq'=>"SYSTEM",'type__in'=>array("M","R")))->values("body","type","created","poster");
	    	foreach($obj as $key=>$ticketComments){
	    		$result[$key]=$ticketComments;
	    	}
		}
		echo json_encode($result);
	}
}
?>
