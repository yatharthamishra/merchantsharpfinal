<?php

/**
 * Class used for bulk operations
 * @author Akash Kumar
 */

class bulkjob {
    
    private $pageSize = 20;
    private $status = array("created"=>0,"inProgress"=>1,"reportPending"=>2,"completed"=>4,"pause"=>5);
    private $tasks = array("assign"=>1,"status"=>2,"Dept"=>3,"comment"=>4,"bulkSla"=>5,"taskAssign"=>6,"taskResponse"=>7);
    private $maxEntitysToPerform = 200;

    function __construct(){
	global $disableDefaultShutdownFunction;
        $disableDefaultShutdownFunction=1;
	global $disableDefaultExceptionHandler;
	$disableDefaultExceptionHandler=1;
	register_shutdown_function(array($this,'shutdown1'));
	set_exception_handler(array($this,'exception_handler1'));
    }
    
    /**
     * Function to Send Job Completion report
     * @param array $jobInfo
     * @param string $taskType
     */ 
    public function sendJobReport($job){
        $success=array();
        $error=array();
        $email = db_result(db_squery("SELECT email from ".STAFF_TABLE." WHERE staff_id=".$job['assigned_by'].""));
        $report = db_fetch_array(db_squery("SELECT * from ".BULK_JOB_TABLE." WHERE id=".$job['id'].""));
        
        $reportSuccess = explode(",",$report['entity_ids']);
        $reportFailure = explode(",",$report['error_entity_ids']);
        
        foreach($reportSuccess as $id){
            if(in_array($id, $reportFailure)){
                $error[] = $id;
            }else {
                $success[] = $id;
            }
        }
        
	$type = $job['entity_type'];
	if($type == 'ticket') $entity = 'Ticket';
	else $entity = 'Task';
	 
        $reportFinal = "<table width='100%'><tr style='background:#eee;'><th>".$entity." Id(Assignment) </th><th>Status</th></tr>".
                       "<tr><td style='text-align:center'>".implode(", ",$success)."</td><td style='color:#080;text-align:center'>Success</td></tr>";
        if(!empty($error)) $reportFinal .="<tr><td style='text-align:center'>".implode(", ",$error)."</td><td style='color:#a00;text-align:center'>Failure</td></tr>";
        $reportFinal .="</table>";
         
        $to = $email;
	//$to = 'meghraj.sharma@shopclues.com';
        $subject = 'JOB#'.$job['id'].' Completion Report';

        $headers = "From: " . strip_tags('osTicketBulkJob@shopclues.com') . "\r\n";
        $headers .= "Reply-To: ". strip_tags('support@shopclues.com') . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        
        mail($to, $subject, $reportFinal, $headers);
        $this->setStatus($job['id'],$email);
    }
    
    /**
     * Function to Perform Job Operation based on priority
     * 
     */
    public function performJobOperation($jobId=''){
        
	global $entityValue;
        global $errorEntitys;
        global $entitys;
	global $rowId;
	
	$results = $this->getData($jobId);
        if(count($results)<0) return;
         
        foreach($results as $data){
	    
       	    $entityIds = explode(",",$data["entity_ids"]);
            $entityIdToPerform = array_filter($entityIds);
            $status = $data["status"];
        
            //Task to perform
            $taskToPerform = json_decode($data["task"],true);
             
            //Job Owner
            $taskBy = $data["assigned_by"];
            
            if($taskToPerform["noteBulk"]){
                $note = stripslashes($taskToPerform["noteBulk"]);
                unset($taskToPerform["noteBulk"]);
            }else{
                $note = "";
            }
            $entitysInList = $data["entity_ids"];
	    $rowId = $data['id'];
	    $entitys = $entitysInList;
	    $errorEntitys = $data["error_entity_ids"];
            $staffObj =  Staff::lookup($taskBy);
            //Entitys in loop too perform operation_
            foreach($entityIdToPerform as $entityId){
                $entityValue = $entityId; 
                $notes = $note;
                $entityType = $data["entity_type"];
                if($entityType == "ticket") {
                    $entityObj=Ticket::lookup($entityId);
                    $lockInfo = $this->isLocked($entityObj,$staffObj);
                }
                else if($entityType == "task") {
                    $entityObj=Task::lookup($entityId);
                    $lockInfo = 1;
                }
                try{
                    if($lockInfo==1){
                        $success=0;$error=0;
                        foreach($taskToPerform as $taskType=>$taskTo){
                            if(!$this->tasks[$taskType])
                                $error = "Invalid Task - ".$taskType;
                            else{
                                if(!is_array($taskTo))
                                        $taskTo = stripslashes($taskTo);
                                // Perform Operation 
                                try {
                                        $performed = $this->performTask($entityObj,$staffObj,$entityId,$taskType,$taskTo,$notes,$taskBy);_pr($entityType.$entityId.$taskType.$performed);
                                } catch (Exception $e) {
                                    $this->logException("Exception",$e->getMessage());
                                }
                            }
                        }
                    }else{
                        $msg = "Ticket is locked";
                        throw new Exception($msg);
                    }
                } catch (Exception $e) {
                    $this->logException("Exception",$e->getMessage());
                }
            }
        $this->sendJobReport($data); 
        echo "Job Completed"; echo "<br>"; 
	}
    }
    
    private function setStatus($jobId,$email){
        
        $sql = "UPDATE ".BULK_JOB_TABLE." SET `email`='".$email."',`updated`='NOW()',`status`='".$this->status["completed"]."' WHERE `id`=".$jobId." LIMIT 1";
        db_query($sql);
    }
    
    function shutdown1() {
    	$lastError = error_get_last();
    	if ($lastError['type'] === E_ERROR) {
        	$this->logException("Shutdown",$lastError);
    	}
    }

    function exception_handler1($exception) {
        $this->logException("PHP Exception",$exception->getMessage());	
    }

    function logException($errorType , $errorMessage) {

	global $entityValue;
        global $errorEntitys;
        global $entitys;
	global $rowId;
	echo $errorType." : ".$errorMessage;
        if(!empty($errorEntitys)) $errorIds = $errorEntitys.",".$entityValue;
        else $errorIds = $entityValue;
        $entityIds = explode(",",$entitys);
        $newCount = count($entityIds) - array_search($entityValue, $entityIds) - 1;
	$setStatus = "";
	if(empty($newCount)) $setStatus = ",status = '4'";
        $sql = "UPDATE ".BULK_JOB_TABLE." SET `error_entity_ids`='".$errorIds."', `entity_count`='".$newCount."'".$setStatus." WHERE `id`=".$rowId." LIMIT 1";
        db_query($sql);
	exit();

    }

    private function getData($jobId) {
        
        global $cfg;
        $this->maxEntitysToPerform = $cfg->config['bulk_max_entity']['value'];
        $where .= "status!='".$this->status["completed"]."' ";
        $sql = "select * from ".BULK_JOB_TABLE." WHERE ".$where."ORDER BY entity_count DESC,id ASC LIMIT 10";
        $res = db_query($sql);
        $entityCount = 0;
        $upperBound = 0;
        while ($row = db_fetch_array($res)) {
            if($entityCount + $row['entity_count']> $this->maxEntitysToPerform){
                break;
            }
            $entityCount += $row['entity_count'];
	    $entityIds = explode(",",$row['entity_ids']);
	    $row['entity_ids'] = implode(",",array_slice($entityIds,count($entityIds) - $row['entity_count']));
            $results[$row['id']] = $row;
	    $rowIds[] = $row['id'];
            $upperBound = $row['id'];
        }
        if($entityCount < 200){
            $limit =(int)(($this->maxEntitysToPerform - $entityCount)/$results[$upperBound]['entity_count']);
	   if(count($rowIds)==0){
		return [];
	   }
            $sql = "select * from ".BULK_JOB_TABLE." WHERE ".$where."AND entity_count<=".$results[$upperBound]['entity_count']." AND id NOT IN (".implode(",",$rowIds).") ORDER BY entity_count DESC,id ASC LIMIT ".$limit;
            $res = db_query($sql);
            while ($row = db_fetch_array($res)) {
                $results[$row['id']] = $row;
                $rowId[] = $row['id'];
            }
        }
        $upperBound++;
	if(count($rowIds)==0){
		return ;
	}
        $sqlUpdate = "Update ".BULK_JOB_TABLE." set status=".$this->status["inProgress"]." WHERE id IN (".implode(",",$rowIds).")";
        db_query($sqlUpdate);
        
        return $results;
    }
    
    /**
     * Function to check if current ticket is locked by some other staff
     * @param int $ticket
     * @return boolean
     */
    private function isLocked($entity,$thisstaff){
        $lock = $entity->getLock();
        if ($lock && $lock->getStaffId()!=$thisstaff->getId()) {
            $staffLocked = Staff::lookup($lock->getStaffId());
            return "Error - Locked by -".$staffLocked->getName();
        }
        return 1;
    }
    
    /**
     * Function to perform different operation, acts as factory function
     * @param int $ticket
     * @param string $task
     * @param type $taskToState
     * @param int $taskBy
     * @return string
     */
    private function performTask($entityObj,$staff,$entity,$task,$taskToState,$note='',$taskBy){
            
        if($task=="assign"){
            $msg = $this->bulkAssignee($entityObj,$entity,$taskToState,$note,$taskBy);
        }elseif($task=="status"){
            $msg = $this->bulkStatusChange($entityObj,$entity,$taskToState,$note,$taskBy);
        }elseif($task=="bulkSla"){return ;
            $msg = $this->bulkSlaChange($entityObj,$entity,$taskToState,$note,$taskBy);
        }elseif($task=="Dept"){
            $msg = $this->bulkDepartment($entityObj,$entity,$taskToState,$note,$taskBy);
        }elseif($task=="comment"){
            $msg = $this->bulkCannedReply($entityObj,$entity,$taskToState,$staff);
        }elseif($task=="taskAssign"){
            $msg = $this->taskAssign($entityObj,$entity,$taskToState,$staff);
        }elseif($task=="taskResponse"){
            $msg = $this->bulkResponse($entityObj,$entity,$taskToState,$staff);
        }else{
            $msg["error"] = "Invalid Operation";
            // Log Info for BulkJob Error
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D5"];
            $module = $logConfig["module"][$domain]["D5_M2"];
            $error = $logConfig["error"]["E3"];
            $logObj = new Analog_logger();
            $logObj->report($domain,$module, $logConfig["level"]["ERROR"], $error,"title:Bulk Job Error","log_location:class.bulkjob.php","task:".$task,"",  substr(json_encode($entityObj)."STAFF- ".json_encode($staff)."TICKET- ".json_encode($entity)."TASK- ".json_encode($task)."TO_STATE- ".json_encode($taskToState),0,2000));
        }
	if (strpos($msg, 'Error') !== false) {
            throw new Exception($msg);
        }
        return $msg;
    }
    
    /**
     * Function to perform assigning operation
     * 
     * @param int $ticketId
     * @param int $assigneeTo
     * @param int $assignedBy
     * @return string
     */
    private function bulkAssignee($ticket,$ticketId,$assigneeTo,$note='',$assignedBy){
        
        $output = $ticket->assign("s".$assigneeTo,$note);
        if($output)
            return "Done";
        else
            return "Error-".print_r($output,true);
    }
    
    private function bulkSlaChange($ticket,$ticketId,$setdate,$note='',$assignedBy){
        $date = date('Y-m-d H:i:s',$setdate);
        if($setdate-time()>0)
            $output = $ticket->setDueDate($date);
        if($output){
            $ticket->logEvent('edited',array("duedate"=>$date));
            return "Done";
        }
        else
            return "Error-".print_r($output,true);
    }
    
    /**
     * Function to perform status operation
     * 
     * @param int $ticketId
     * @param int $assigneeTo
     * @param int $assignedBy
     * @return string
     */
    private function bulkStatusChange($ticket,$ticketId,$statusTo,$note='',$assignedBy){
        
        $output = $ticket->setStatus($statusTo,$note);
        if($output)
            return "Done";
        else
            return "Error-".print_r($output,true);
    }
    /**
     * Function to perform status operation
     * 
     * @param int $ticketId
     * @param int $assigneeTo
     * @param int $assignedBy
     * @return string
     */
    private function bulkDepartment($ticket,$ticketId,$deptTo,$note='',$assignedBy){
        
        $output = $ticket->transfer($deptTo,$note);
        if($output)
            return "Done";
        else
            return "Error-".print_r($output,true);
    }
    /**
     * Function to commenting operation
     * 
     * @param int $ticketId
     * @param int $comment
     * @param int $assignedBy
     * @return string
     */
    private function bulkCannedReply($ticket,$ticketId,$comment,$assignedBy){
        
        $commentFinal["emailreply"] = 1;
        $commentFinal["emailcollab"] = 1;
//        $commentFinal["reply_status_id"] = 1;
        $commentFinal["signature"] = "none";
        $commentFinal["cannedResp"] = $comment["cannedId"];
        //$comment["comment"] = $ticket->replaceVars(comment["comment"]);
        $commentFinal["response"] = $this->replaceCannedVariables($ticket,$ticketId,$comment["comment"]);
        $commentFinal["response"]=preg_replace('/%{[\s\S]+?}/', '', $commentFinal["response"]);
        $output = $ticket->postReply($commentFinal,$errors);
        //$output = $ticket->postCannedReply($commentFinal["cannedResp"],$commentFinal["response"]);
        if($output)
            return "Done";
        else
            return "Error-".print_r($output,true);
    }
    
    private function taskAssign($task,$taskId,$assignedTo,$assignedBy){
        
        $noteData = array();
        $noteData['id'] = $taskId;
        $noteData['a'] = "postnote";
        $staffObj = Staff::lookup($assignedTo);
        $assignTo = $staffObj->getName()->name;
        $staffObj = Staff::lookup($assignedBy->ht['staff_id']);
        $assignBy = $staffObj->getName()->name;
        $noteData['note'] = "Task assigned to ".$assignTo." via Bulk job by ".$assignBy;
        //$noteData['task_status'] = "open";
        $output= $task->postNote($noteData, $errors);
        if($output)
            return "Done";
        else
            return "Error-".print_r($output,true);
    }
    
    private function bulkResponse($task,$taskId,$response,$assignedBy){
        
        $data = array();
        $data["taskId"] = $taskId;
        $data["agent"] = "SYSTEM - On Task Reminder";
        $data["email"] = "system.auto@shopclues.com";
        $data['source'] = 'CRON';
        $data["taskResponse"] = $response;
        $output=Task::updateTask($data, $errors, $data['source']);
        if($output)
            return "Done";
        else
            return "Error-".print_r($output,true);
    }
    /**
     * Function to replace canned response variables
     * @param int $ticketId Id of ticket
     * @param string $message
     */
    function replaceCannedVariables($ticket,$id,$message){
       $messageTic= $ticket->replaceVars($message,array('comments' => 'comment')); 
       return $messageTic;
    }
}
