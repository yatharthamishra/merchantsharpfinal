<?php

/*
 * Class handling oder Status table
 * @author - Akash
 */

class OrderSwapMonitor extends VerySimpleModel {

    static $meta = array(
        'table' => ORDER_CRON_TABLE,
        'pk' => array('id')
    );

    public function getTickets($max = 1) {
        $orders = OrderSwapMonitor::objects()->filter(array("processed" => 0, "status_type__in" => array('Clone')))->order_by('id')->limit($max);
        global $cfg, $ost;
        foreach ($orders as $order) {
            $ticketOdata = TicketOData::objects()->filter(array("order_id"=>$order->order_id))->values("ticket_id")->all();
            $orderArr[$order->status_type][$ticketOdata[0]["ticket_id"]]["Details"] = $order->to_status;
            $orderArr[$order->status_type][$ticketOdata[0]["ticket_id"]]["Object"] = $order;
        }
        return $orderArr;
    }

    public function PerformOperation($ordersArr) {
        global $cfg;
        $taskStaticData = getCachedTaskFields();
        foreach ($ordersArr as $statusType => $orderStatus) {
            foreach ($orderStatus as $ticketId => $status) {
                if (!$processedOrder[$ticketId]) {
                    
                    $TasksForCreateClone = json_decode($cfg->config['taskToRespondOnClone']['value'],true);
                    
                    $filterArray = array("object_id"=>$ticketId,"dept_id"=>$TasksForCreateClone["department_id"],"flags"=>1);
                    $Tasks = Task::objects()->filter($filterArray);
                    foreach($Tasks as $taskToRespond){
                        $taskData = TaskData::objects()->filter(array("task_id"=>$taskToRespond->id))->values("taskType")->all();
                        $taskType = explode(",",$taskData[0]["taskType"])[0];
                        if($taskType==$TasksForCreateClone["task_id"]){
                            $data = array("taskId" => $taskToRespond->getId(), "taskResponse" => $TasksForCreateClone['response'], "taskAgent" => "On Clone Creation", "email" => "system.auto@shopclues.com");
                            $taskToRespond->updateTask($data);
                        }
                    }
                    $orderStatus[$ticketId]["Object"]->processed = 1;
                    $orderStatus[$ticketId]["Object"]->save();
                }
                $processedOrder[$ticketId] = 1;
            }
        }
    }

}
