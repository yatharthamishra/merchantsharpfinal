<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportUpdateDailyCron
 *
 * @author shopclues
 */

define("PENDENCY_REPORT_TABLE_NAME",'mst_pendency_report');
define("SLA_SUMMARY_REPORT_TABLE_NAME",'mst_pendency_report');
define("REPORT_LOG_PATH_PATTERN",'/tmp/reportdailycron%date%');
define("REPORT_FULL_LOG_PATH_PATTERN",'/tmp/reportfullcron%date%');
include 'class.reportCronModel.php';
class ReportUpdateDailyCron {
    //put your code here 
    private $reportLogPath;
    private $fromDatePendency;
    private $toDatePendency;
    private $fromDateSummary;
    private $toDateSummary;
    private $logFilePointer;
    private $reportLib;
    private $cronModel;
    private $updateSource;
    private $analogLogger;
    private $logDomain;
    private $logModule;
    public function __construct() {
        if(REPORT_TYPE=='daily'){
            $this->updateSource="dailycron";
            $this->reportLogPath=str_replace('%date%', date('Y-m-d'), REPORT_LOG_PATH_PATTERN);
        }else{
            $this->updateSource="fullscript";
            $this->reportLogPath=str_replace('%date%', date('Y-m-d'), REPORT_FULL_LOG_PATH_PATTERN);
        }
        
        if(NUMBER_OF_DAYS_TO_CALCULATE_FOR_PENDENCY==0){
            $this->fromDatePendency='';
        }else{
            $this->fromDatePendency=date('Y-m-d', strtotime(date('Y-m-d') .' -'.NUMBER_OF_DAYS_TO_CALCULATE_FOR_PENDENCY.' day'));
        }
        $this->toDatePendency=date('Y-m-d', strtotime(date('Y-m-d') .' -'.'1'.' day'));
        if(NUMBER_OF_DAYS_TO_CALCULATE_FOR_SLA_SUMMARY==0){
            $this->fromDateSummary='';
        }else{
        $this->fromDateSummary=date('Y-m-d', strtotime(date('Y-m-d') .' -'.NUMBER_OF_DAYS_TO_CALCULATE_FOR_SLA_SUMMARY.' day'));
        }
        $this->toDateSummary=date('Y-m-d', strtotime(date('Y-m-d') .' -'.'1'.' day'));
        $this->reportLib=new ReportLibrary();
        $this->cronModel=new ReportCronModel();
        $this->analogLogger = logConfig();
        $this->logDomain = $this->analogLogger["domain"]["D5"];
        $this->logModule = $this->analogLogger["module"][$this->logDomain]["D5_M3"];
    }
    
    

    public function main(){
        global $reportRelatedConfig;
        $reportConfigObject=new CustomConfig(array( 'report'));
        $config=$reportConfigObject->getConfigInfo();
        $reportRelatedConfig=$config['report'];
        $this->logFilePointer=fopen($this->reportLogPath, "w");
        $this->updateInsertLastPendencyEntries();
        echo 'Pendency Record Updated \n';
        $this->updateInsertLastSlaSummaryEntries();
        echo 'Sla Summary Report Updated \n';
        echo 'Cron Completed \n';
    }
    
    private function _logDbErrorIntoAnalog($entity,$dbError,$sql){
        $aLogger=new Analog_logger();
        $aLogger->report($this->logDomain, 
                $this->logModule, 
                $this->analogLogger['level']['ALERT'], 
                $this->analogLogger['error']['E18'],"title:Db Error failure",
                "log_location:class.reportUpdateCron.php","dbError:$dbError:$sql","entity:$entity");
    }
    
    private function _logTimeConsumedInfo($entity,$timeConsumed){
        $aLogger=new Analog_logger();
        $aLogger->report($this->logDomain, 
                $this->logModule, 
                $this->analogLogger['level']['INFO'], 
                '',"title:TimeConsumptionLog",
                "log_location:class.reportUpdateCron.php","timeConsumed:$timeConsumed","entity:$entity");
    }

    public function updateInsertLastPendencyEntries() {
        set_time_limit(3600);
        $startTime = microtime(TRUE);
        $this->_logEntityWiseData('--Pendency Report--', 'Start');
        echo 'Fetching Data for Pendency \n ';
        $this->reportLib->triage_pendency($tickets, array('threadCreateFromdate' => $this->fromDatePendency
            , 'threadCreateTodate' => $this->toDatePendency));
        echo 'Got Pendency Output \n ';
        $pendencyOldData=$this->cronModel->getOldDataForPendency($this->fromDatePendency, $this->toDatePendency);
        $index=0;
        $dataToBeInserted=[];
        $recordsUpdated=0;
        $recordsInserted=0;
        foreach ($tickets as $date => $ticketDetail) {
            $dataToBeInserted[$index]['fresh_tickets_received_untriaged'] = $ticketDetail['triage_total'];
            $dataToBeInserted[$index]['fresh_tickets_received_triage_again'] = $ticketDetail['triage_again_total'];
            $dataToBeInserted[$index]['old_tickets_untriaged'] = $ticketDetail['old_ticket_triage'];
            $dataToBeInserted[$index]['date'] = $date;
            $dataToBeInserted[$index]['old_tickets_triage_again'] = $ticketDetail['old_ticket_triage_again'];
            $dataToBeInserted[$index]['tickets_handled_untriaged'] = $ticketDetail['triage_handled'];
            $dataToBeInserted[$index]['tickets_handled_triage_again'] = $ticketDetail['triage_again_handled'];
            $dataToBeInserted[$index]['tickets_pending_untriaged'] = $ticketDetail['triage_pending'];
            $dataToBeInserted[$index]['tickets_pending_triage_again'] = $ticketDetail['triage_again_pending'];
            $dataToBeInserted[$index]['update_source'] = $this->updateSource;
            $dataToBeInserted[$index]['status_id'] = 1;
            if(isset($pendencyOldData[$date])){
                $recordsUpdated++;
                $this->_logEntityWiseData("Old Data $date , oldData - ", $pendencyOldData[$date]);
                $this->_logEntityWiseData("Data Updated For $date , finalData - ", $dataToBeInserted[$index]);
            }else{
                $recordsInserted++;
                $this->_logEntityWiseData("Data Inserted For $date , insertedData - ", $dataToBeInserted[$index]);
            }
            $index++;
        }
        
        if (count($tickets) > 0) {
            $dbError=$this->cronModel->insertDataIntoPendencyTable($dataToBeInserted);
            $this->_logEntityWiseData('Pendency Records Update', $recordsUpdated);
            $this->_logEntityWiseData('Pendency Records Inserted', $recordsInserted);
            if(!empty($dbError)){
                $this->_logDbErrorIntoAnalog('Pendency', $dbError);
                $this->_logEntityWiseData("Error Occured", $dbError);
            }
        } else {
            $data = array($this->fromDatePendency, $this->toDatePendency);
            $this->_logEntityWiseData("No records found for ", $data);
        }
        $endTime = microtime(TRUE);
        $timeTaken = round(($endTime - $startTime), 2);
        $this->_logEntityWiseData('--Pendeny end -- Time Consumed : ', $timeTaken);
        $this->_logTimeConsumedInfo('Pendency', $timeTaken);
    }

    public function updateInsertLastSlaSummaryEntries(){
        $this->_logEntityWiseData('--Summary Report--', 'Start');
        $startTime=  microtime(true);
        $newFromDate = date('Y-m-d',strtotime($this->fromDateSummary.'-1 day'));
        echo "Fetching Data for Sla Summary .. \n ";
        $this->reportLib->triage_sla_summary($tickets, array('threadCreateFromdate' => $this->fromDateSummary
            , 'threadCreateTodate' => $this->toDateSummary));
        if($this->fromDateSummary){
            $summaryOldData=$this->cronModel->getOldDataForSlaSummary($newFromDate, $this->toDateSummary);
            if($summaryOldData[$newFromDate])
            $previousDateData = $summaryOldData[$newFromDate];
        }
        echo "Fetching Data Complete for SLA Summary \n ";
        $index=0;
        $dataToBeInserted=[];
        $recordsUpdated=0;
        $recordsInserted=0;
        
        foreach ($tickets as $date => $ticketDetail) {
            $dataToBeInserted[$index]['tickets_received_untriaged'] = $previousDateData['tickets_received_untriaged']+$ticketDetail['triage_total_overall'];
            $dataToBeInserted[$index]['tickets_received_triage_again'] = $previousDateData['tickets_received_triage_again']+$ticketDetail['triage_again_total_overall'];
            $dataToBeInserted[$index]['tickets_handled_within_sla_untriaged'] = $previousDateData['tickets_handled_within_sla_untriaged']+$ticketDetail['triage_handled_wsla'];
            $dataToBeInserted[$index]['date'] = $date;
            $dataToBeInserted[$index]['tickets_handled_within_sla_triage_again'] = $previousDateData['tickets_handled_within_sla_triage_again']+$ticketDetail['triage_again_handled_wsla'];
            $dataToBeInserted[$index]['tickets_open_within_sla_untriaged'] = $ticketDetail['triage_open_wsla'];
            $dataToBeInserted[$index]['tickets_open_within_sla_triage_again'] = $ticketDetail['triage_again_open_wsla'];
            $dataToBeInserted[$index]['update_source'] = $this->updateSource;
            $dataToBeInserted[$index]['status_id'] = 1;
            if(isset($summaryOldData[$date])){
                $recordsUpdated++;
                $this->_logEntityWiseData("Old Data $date , oldData - ", $summaryOldData[$date]);
                $this->_logEntityWiseData("Data Updated For $date , finalData - ", $dataToBeInserted[$index]);
            }else{
                $recordsInserted++;
                $this->_logEntityWiseData("Data Inserted For $date , insertedData - ", $dataToBeInserted[$index]);
            }
            $index++;
        }
        
        if (count($tickets) > 0) {
            $dbError=$this->cronModel->insertDataIntoSlaSummaryTable($dataToBeInserted);
            $this->_logEntityWiseData('Sla Summary Records Update', $recordsUpdated);
            $this->_logEntityWiseData('Sla Summary Records Inserted', $recordsInserted);
            if(!empty($dbError)){
                $this->_logDbErrorIntoAnalog('SLA Summary', $dbError);
                $this->_logEntityWiseData("Error Occured", $dbError);
            }
        } else {
            $data = array($this->fromDateSummary, $this->toDateSummary);
            $this->_logEntityWiseData("No records found for ", $data);
        }
        $endTime=  microtime(TRUE);
        $timeTaken=round(($endTime-$startTime),2);
        $this->_logEntityWiseData('--SLA Summary End -- Time Consumed : ', $timeTaken);
        $this->_logTimeConsumedInfo('SLA Summary', $timeTaken);
        
    }
    private function _logEntityWiseData($heading, $data) {
        fwrite($this->logFilePointer, $heading . " : \n");
        if(!is_array($data)){
            fwrite($this->logFilePointer, $data . "\n");
        }
        if (is_null($data) || empty($data)) {
            return;
        }
        foreach ($data as $entityName => $entityTextData) {
            $entityText=$entityTextData;
            if (is_array($entityTextData)) {
                if (count($entityTextData) > 0) {
                    $entityText = implode("|", $entityTextData);
                }
            }
            fwrite($this->logFilePointer, $entityName . " : " );
            fwrite($this->logFilePointer, $entityText . "\n");
        }
    }
    
}

?>
