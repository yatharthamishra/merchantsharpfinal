<?php

/**
 * Description of analog_logger
 *
 * @author akash.kumar@shopclues.com
 */            
class Analog_logger {

    private $analog_enable;
    private $web_path_file;
    private $domain_module;
    private $threshhold;

    function __construct() {
        
    }

    public function report($domain, $module, $level, $error_name, $field1 = NULL, $field2 = NULL, $field3 = NULL, $field4 = NULL, $others = NULL, $source = 'osticket') {
        $log_levels = unserialize(analog_log_levels);
        $log_level = $log_levels;
        $config["LOG_LEVELS"] = $log_level;
        $analogLogger = unserialize(analog_logger);
        $analogLogger["analog_logger"] = true;
        $analogLogger["DOMAIN_MODULE_THRESHOLD_LOG_LEVEL"] = unserialize(analog_domain_module);
        //$analogLogger = 1//
        $this->analog_enable = $analogLogger['analog_enable'];
        $this->web_path_file = dirname(__FILE__) . "/" . $analogLogger['web_log_file_path'];

        $this->domain_module = $analogLogger['DOMAIN_MODULE_THRESHOLD_LOG_LEVEL'];
        $this->threshhold = $analogLogger['THRESHOLD_LOG_LEVEL'];

        // Analog Include 
        if ($this->analog_enable && class_exists('Analog') && defined('ANALOG_LOG') && isset($this->web_path_file) && isset($this->domain_module) && isset($this->threshhold)) {

            Analog::handler(Analog\Handler\File::init($this->web_path_file));
            Analog::$default_level = $this->threshhold;
            Analog::$default_levels_domain_module = $this->domain_module;
        }
        //Analog include end        

        try {

            $log_level = $this->log_level;
        
            if (class_exists('Analog') && !empty($level) && defined('ANALOG_LOG') && $this->analog_enable) {

                $message_array = array(
                    'field1' => $field1,
                    'field2' => $field2,
                    'field3' => $field3,
                    'field4' => $field4,
                    'error_name' => $error_name,
                    'others' => $others
                );
                Analog::log($domain, $module, json_encode($message_array), $level);
                if($_GET["testing"]){
                    print_r("Domain-".$domain."<br><br>Module-".$module."<br><br>Msg-".json_encode($message_array)."<br><br>Level-".$level."<br><br>".$source);die;
                }

            } else {

                // 
            }
        } catch (Exception $ex) {
            
        }
    }

}
