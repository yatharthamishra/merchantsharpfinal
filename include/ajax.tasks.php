<?php

/* * *******************************************************************
  ajax.tasks.php

  AJAX interface for tasks
  Peter Rotich <peter@osticket.com>
  Copyright (c)  20014 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */

if (!defined('INCLUDE_DIR'))
    die('403');
include_once(INCLUDE_DIR.'class.tracking.php');

include_once(INCLUDE_DIR . 'class.ticket.php');
require_once(INCLUDE_DIR . 'class.ajax.php');
require_once(INCLUDE_DIR . 'class.task.php');

class TasksAjaxAPI extends AjaxController {

    function lookup() {
        global $thisstaff;

        $limit = isset($_REQUEST['limit']) ? (int) $_REQUEST['limit'] : 25;
        $tasks = array();

        $visibility = Q::any(array(
                    'staff_id' => $thisstaff->getId(),
                    'team_id__in' => $thisstaff->teams->values_flat('team_id'),
        ));

        if (!$thisstaff->showAssignedOnly() && ($depts = $thisstaff->getDepts())) {
            $visibility->add(array('dept_id__in' => $depts));
        }


        $hits = TaskModel::objects()
                ->filter(Q::any(array(
                            'number__startswith' => $_REQUEST['q'],
                )))
                ->filter($visibility)
                ->values('number')
                ->annotate(array('tasks' => SqlAggregate::COUNT('id')))
                ->order_by('-created')
                ->limit($limit);

        foreach ($hits as $T) {
            $tasks[] = array('id' => $T['number'], 'value' => $T['number'],
                'info' => "{$T['number']}",
                'matches' => $_REQUEST['q']);
        }

        return $this->json_encode($tasks);
    }

    function orderflowupdate() {
        global $thisstaff;
        $flowId = $_POST["flowId"];
        $flowObj = OrderFlow::lookup($flowId);
        $ExistingflowObj = $flowObj;
        if ($_POST["communication"]) {
            $communication = $_POST["communication"];
           UtilityFunctions::flowLogs('mst_order_flow',$flowId,$communication,$thisstaff->ht['staff_id'],$flowObj->communication,'orderflow communication',null);
            $flowObj->communication = $communication;
            $msg = "Communication Assigned successfully";
        } else if ($_POST["response"]) {
            $response = $_POST["response"];
         UtilityFunctions::flowLogs('mst_order_flow',$flowId,$response,$thisstaff->ht['staff_id'],$flowObj->task_response,'orderflow response',null);   
            $flowObj->task_response = $response;
            $msg = "Response Assigned successfully";
        }

        $flowObj->save();
        
        // Log Info for BulkJob Error
        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D4"];
        $module = $logConfig["module"][$domain]["D4_M1"];
        $error = $logConfig["error"]["E13"];
        $level = $logConfig["level"]["INFO"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $level, $error, "title:Order Flow Updated", "log_location:ajax.tasks.php", "msg:" . $msg, "flowid-" . $flowId, substr("Data--" . json_encode($ExistingflowObj) . "--Update--" . json_encode($_POST), 0, 2000));
        echo $msg;
    }

    function flowupdate() {
        global $thisstaff;
        $flowIdArr = array_filter(explode(",", $_POST["flowId"]));
        $flowType = $_POST["flowType"];

        foreach ($flowIdArr as $flowId) {
            if ($flowType == "Return")
                $flowObj = ReturnFlow::lookup($flowId);
            elseif ($flowType == "ReturnTask")
                $flowObj = ReturnTaskAssign::lookup($flowId);
            else
                return "Invalid Request";
            
            $ExistingflowObj = $flowObj;
            if ($_POST["field"]) {
           UtilityFunctions::flowLogs('mst_return_flow',$_POST['flowId'],$_POST['value'],$thisstaff->ht['staff_id'],$flowObj->$_POST["field"],$_POST['field'],null);
                $flowObj->$_POST["field"] = $_POST["value"];
                $flowObj->save();
                $msg = $_POST["field"] . " Assigned successfully";
            }

            // Log Info for BulkJob Error
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D4"];
            $module = $logConfig["module"][$domain]["D4_M1"];
            $error = $logConfig["error"]["E13"];
            $level = $logConfig["level"]["INFO"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $level, $error, "title:" . $flowType . " Flow Updated", "log_location:ajax.tasks.php", "msg:" . $msg, "flowid-" . $flowId, substr("Data--" . json_encode($ExistingflowObj) . "--Update--" . json_encode($_POST), 0, 2000));
            
        }
        echo $msg;
    }

    function claimTask() {
        global $thisstaff;
        $task = Task::lookup($_GET["taskId"]); //Ticket::lookup($_POST["tid"]);
        $deptOfStaff=$thisstaff->getDepts();
        if(!in_array($task->dept_id, $deptOfStaff)){
            $msg=__('Couldnt assign other department task');
            return $msg;
        }
        if ($task->staff_id != $thisstaff->getId()) {
            $task->staff_id = $thisstaff->getId();
            $task->save();
        }
        $msg = __('Assigned');
        return $msg;
    }

    function nextTaskFlow() {
        global $thisstaff;
        $flowId = $_POST["flowId"];
        $nextTask = $_POST["nextTask"];
        $task = $_POST["task"];
        $communication =  $_POST["communication"];
        if ($task=="next" && $flowId == $nextTask) {
            echo "Same TAsk Cannot be next task";
        } else {
            $flowObj = TaskFlow::lookup($flowId);
            $ExistingflowObj = $flowObj;
            if ($flowObj && $task=="next") {
            UtilityFunctions::flowLogs('mst_task_flow',$flowId,$nextTask,$thisstaff->ht['staff_id'],$flowObj->next_task_id,'next taskflow',null);
                $flowObj->next_task_id = $nextTask;
                echo "Next Flow Assigned successfully";
            }else if ($flowObj && $task=="communication") {
                UtilityFunctions::flowLogs('mst_task_flow',$flowId,$communication,$thisstaff->ht['staff_id'],$flowObj->communication,'communication',null);
                $flowObj->communication = $communication;
                echo "Communication Assigned successfully";
            } else {
                echo "No such task exists";
            }
            $flowObj->save();            
            // Log Info for BulkJob Error
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D4"];
            $module = $logConfig["module"][$domain]["D4_M1"];
            $error = $logConfig["error"]["E15"];
            $level = $logConfig["level"]["INFO"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $level, $error, "title:Next Task Flow Updated", "log_location:ajax.tasks.php", "flowId-" . $flowId, "", substr("Data--" . json_encode($ExistingflowObj) . "--Update--" . json_encode($_POST), 0, 2000));
        }
    }


       function createTask() {

        global $thisstaff;
        $dept = $_POST["dept"];
        $ticketId = $_POST["ticketId"];
        $mandateIds=[];
        if($_POST['mandateId']){
            $mandateIds = $_POST['mandateId'];
            $mandateIds=  explode(',', $mandateIds);
        }
        $mandateType = $_POST['mandateType'];
    
        $commaSeperatedMandateIds = UtilityFunctions::getInClauseFromArray($mandateIds);
        // $type = explode("-", $_POST["type"]);
        // $status = explode("-", $_POST["status"]);
        $staticFields = getCachedTaskFields();
        $type = array("1" => $_POST["type"], "2" => $staticFields['Default_Task Types'][$_POST["type"]]);
        $status = array("1" => $_POST["status"], "2" => $staticFields['Default_Task Status'][$_POST["status"]]);
        $ticket = Ticket::lookup($ticketId);
        $taskAlreadyExists=[];
        $mandateInfo = TicketUtilityFunctions::getMandatoryInfoInWhichTicketHasBeenMade($ticketId);
        if($mandateInfo && count($mandateIds)>0){
           $invalidMandateInfo=TicketUtilityFunctions::checkIfMandatoryInfoValid($mandateInfo, $mandateIds);
           if(count($invalidMandateInfo)>0){
               $this->response(500, "Mandatory Ids not valid :- " .  implode(',', $invalidMandateInfo));
           }
        }
        $manifest_id=[];
        
        if (empty($mandateInfo) && !empty($mandateType )) {
            if (!$mandateType && count($mandateIds) > 0) {
                $this->response(500, "Please enter mandate Type");
            }
            $mandateInfo = $mandateType;
            if($mandateType=="batch_upload" || $mandateType=="batch_update"){
                $mandateInfo="batch";
            }
	   
            $invalidMandateInfo = TicketUtilityFunctions::checkIfMandatoryInfoValid($mandateInfo, $mandateIds);
            if (count($invalidMandateInfo) > 0) {
                $this->response(500, "Mandatory Ids not valid :- " . implode(',', $invalidMandateInfo));
            }
            TicketUtilityFunctions::setMandatoryInfoForTicket($mandateType, $ticketId, $mandateIds,$ticket);
        }
        if (count($mandateIds) > 0) {
            switch ($mandateInfo) {
                case 'order':
                    $create = $ticket->getOrderStatusFromOrderIds($mandateIds);
                    $taskExtraData=array('order_ids'=>  implode(',', $mandateIds));
                    $orderInfoMapDetails=Task::getOrderInfoMap($taskExtraData);
                    $order_ids=$mandateIds;
                    break;
                case 'manifest':
                    if ($commaSeperatedMandateIds) {
                        $taskExtraData=array('manifest_ids'=>  implode(',', $mandateIds));
                        $orderInfoMapDetails=Task::getOrderInfoMap($taskExtraData);
                        
                        $sql = "select order_id,manifest_id from " . CLUES_ORDER_MANIFEST_DETAILS_TABLE . " where manifest_id in ($commaSeperatedMandateIds) 
                        group by manifest_id ";
                        $res = db_query($sql);
                        $order_ids = [];
                        while ($result = db_fetch_array($res)) {
                            $order_ids[] = $result['order_id'];
                            $manifest_id[]=$result['manifest_id'];
                            $manifestOrderIdMap[$result['manifest_id']] = $result['order_id'];
                        }
                        $create = $ticket->getOrderStatusFromOrderIds($order_ids);
                    }
                    break;
                case 'product':
                    $create=TicketUtilityFunctions::getProductStatusFromProductIds($mandateIds);
                    break;
                case 'batch':
                case 'batch_update':
                case 'batch_upload':
                    $create=TicketUtilityFunctions::getBatchStatusFromBatchIds($mandateIds);
                    break;
            }
        }
        
        $presentMandateIds= explode(',', $ticket->getEntities($mandateInfo));
        $updatedMandateIds=array_unique(array_merge($presentMandateIds,$mandateIds));
        TicketUtilityFunctions::setMandatoryInfoForTicket($mandateInfo, $ticketId, $updatedMandateIds,$ticket);
        $agent = $thisstaff->getId();
        $title = $type[2];
        $sources = DynamicList::objects()->filter(array("name" => "Task Sources"));
        foreach ($sources as $s) {
            foreach ($s->items as $item) {
                if ($item->extra == 1) {
                    $source = array($item->id => $item->value);
                }
            }
        }
        
        $orderInfoMap=$orderInfoMapDetails['orderInfoMap'];
        $manifestInfoMap=$orderInfoMapDetails['manifestInfoMap'];
        if(count($mandateIds)==0){
            $dataTask = array(
                "object_type" => ObjectModel::OBJECT_TYPE_TICKET,
                "object_id" => $ticketId,
                "ticket_id"=>$ticketId,
                "creater" => "System",
                "internal_formdata" => array(
                    "dept_id" => $dept,
                ),
                "default_formdata" => array(
                    "taskType" => $type[1],
                    "title" => $title,
                    "description" => $title,
                    "taskSource" => json_encode($source),
                    "taskStatus" => json_encode(array($status[1] => $status[2])),
                    "createdBy" => $agent
               //     "task_mandate_type"=>$mandateInfo
                )
                
                
            );
            $slaObj = TaskSLA::lookup($staticFields["id_extra_Task Types"][$type[1]]);
            if ($slaObj && $slaObj->sla_id) {
                $slaId = $slaObj->sla_id;
                $dataTask["internal_formdata"]["sla"] = $slaId;
            } else {
                $dataTask["internal_formdata"]["duedate"] = $ticket->est_duedate;
            }
            $task = Task::create($dataTask);
            if (stristr($task, 'exist'))
                $taskAlreadyExists[]="unknown";
        }
        foreach ($mandateIds as $mandateId) {
            $dataTask = array(
                "object_type" => ObjectModel::OBJECT_TYPE_TICKET,
                "object_id" => $ticketId,
                "ticket_id"=>$ticketId,
                "creater" => "System",
                "internal_formdata" => array(
                    "dept_id" => $dept,
                ),
                "default_formdata" => array(
                    "taskType" => $type[1],
                    "title" => $title,
                    "description" => $title,
                    "taskSource" => json_encode($source),
                    "taskStatus" => json_encode(array($status[1] => $status[2])),
                    "createdBy" => $agent
                )
            );
            
            switch ($mandateInfo) {
                case 'order':
                    $dataTask["default_formdata"]['order_id'] = $mandateId;
                    $dataTask['order_id'] = $mandateId;
                    $dataTask['orderInfo']=$orderInfoMap[$mandateId];
                    $dataTask['task_mandate_type']=$mandateInfo;
                    $dataTask["default_formdata"]["createStatus"]=$create[$mandateId];
                    break;
                case 'manifest':
                    $dataTask["default_formdata"]['manifest_id'] = $mandateId;
                    $dataTask['manifest_id'] = $mandateId;
                    $dataTask['task_mandate_type']=$mandateInfo;
                    $dataTask["default_formdata"]['order_id'] = $manifestOrderIdMap[$mandateId];
                    $dataTask['order_id'] = $manifestOrderIdMap[$mandateId];
                    $dataTask["default_formdata"]["createStatus"]=$create[$manifestOrderIdMap[$mandateId]];
                    $dataTask['orderInfo']=$manifestInfoMap[$mandateId][$manifestOrderIdMap[$mandateId]];
                    break;
                case 'product':
                    $dataTask['product_id'] = $mandateId;
                    $dataTask['task_mandate_type']=$mandateInfo;
                    $dataTask["default_formdata"]["createStatus"]=$create[$mandateId];
                    break;
                case 'batch':
                case 'batch_upload':
                case 'batch_update':
                    $dataTask['batch_id'] = $mandateId;
                    $dataTask['task_mandate_type']=$mandateInfo;
                    $dataTask["default_formdata"]["createStatus"]=$create[$mandateId];
                    break;
            }
            
            $slaObj = TaskSLA::lookup($staticFields["id_extra_Task Types"][$type[1]]);
            if ($slaObj && $slaObj->sla_id) {
                $slaId = $slaObj->sla_id;
                $dataTask["internal_formdata"]["sla"] = $slaId;
            } else {
                $dataTask["internal_formdata"]["duedate"] = $ticket->est_duedate;
            }
            $task = Task::create($dataTask);
            if (stristr($task, 'exist'))
                $taskAlreadyExists[]=$mandateId;
            else if ($task) {
                //If Task Created
                global $cfg;
                $closeTaskOnCreation = json_decode($cfg->config['closeTaskOnCreation']['value'], true);
                if ($closeTaskOnCreation) {
                    $taskCloseObj = Task::objects()->filter(array("data__taskType__in" => array_keys($closeTaskOnCreation), "object_id" => $ticketId, "flags" => 1))->values('id', 'data__taskType')->all();
                    foreach ($taskCloseObj as $taskTobeClosed) {
                        $newResponse = $closeTaskOnCreation[$taskTobeClosed['data__taskType']];
                        if ($newResponse) {
                            $data = array("taskId" => $taskTobeClosed["id"], "taskResponse" => $newResponse, "taskAgent" => "On Task Creation", "email" => "system.auto@shopclues.com");
                            $taskToBeClosedObj = Task::lookup($taskTobeClosed["id"]);
                            $taskToBeClosedObj->updateTask($data);
                        }
                    }
                }


                /*
                  Tracking user movements
                 */
                $trackingData = array('staff_id' => $thisstaff->ht['staff_id'],
                    'last_timestamp' => $_POST['timeForTracking'],
                    'timestamp' => date("Y-m-d H:i:s", time()),
                    'event' => 'Task Creation',
                    'ticket_id' => $ticketId,
                    'task_id' => $task->getId()
                );
                $trackingObj = new Tracking;
                $trackingObj->track($trackingData);
                /*
                  user tracking code ends
                 */
                
            }
            else
                $this->response(500, "Some Error Occurred");
        }
        if(count($taskAlreadyExists)>0){
            $mandateIdsForTaskCreated=  implode(',', $taskAlreadyExists);
            $this->response(500, "Task Already Exists for $mandateIdsForTaskCreated");
        }
        echo "Task Created Successfully";
    }

    function slaupdate(){
        global $thisstaff,$cfg;
        $taskId = $_POST["id"];
        $action = $_POST["action"];
        $response = $_POST["response"];
        $taskObjArr = TaskSLA::objects()->filter(array("task_type"=>$taskId));
        $default_task_sla_id = $cfg->config['default_task_sla_id']['value'];
        $default_task_ed_sla_id = $cfg->config['default_task_ed_sla_id']['value'];
        $count = $taskObjArr->count();
        if($count<1){
            if($action=="sla")
                $var = array('sla_id'=>$response, 'ed_sla'=>$default_task_ed_sla_id,'api_sla'=>0);
            if($action=="edsla")
                $var = array('ed_sla'=>$response, 'sla_id'=>$default_task_sla_id,'api_sla'=>0);
            if($action=="api")
                $var = array('api_sla'=>$response,"sla_id"=>$default_task_sla_id, 'ed_sla'=>$default_task_ed_sla_id);
            $var["task_type"] = $taskId;
            $taskObj = TaskSLA::create($var);
            $taskObj->save();
        }else{
            $taskObjArr = TaskSLA::objects()->filter(array("task_type"=>$taskId));
            foreach($taskObjArr as $taskObj){
                if($action=="sla"){
                    $taskObj->sla_id = $response;
                }
                else if($action=="edsla"){
                    $taskObj->ed_sla = $response;
                }
                else if($action=="api"){
                    $taskObj->api_sla = $response;
                }
                $taskObj->save();
            }
        }
        echo "Task SLA Updated Successfully";
    }
       function updateFirstTaskFlow(){
        
        $flowId=(int)$_POST['flowId'];
        $taskExtraId=(int)$_POST['taskExtra'];
        if($flowId>0 && 
                $taskExtraId>0){
            
            $sql="update ".TASK_MAPPING_TABLE." set task_type_id=$taskExtraId where id=$flowId";
            db_query($sql);
            return 1;
                }
                return 0;
    }
    function updateFirstResponseFlow(){

        $flowId=(int)$_POST['flowId'];
        $taskResponseId=(int)$_POST['taskResponse'];
        if($flowId>0 
                ){

            $sql="update ".TASK_MAPPING_TABLE." set autoresponse=$taskResponseId where id=$flowId";
            db_query($sql);
            return 1;
                }
                return 0;
    }

    function updateTaskFlow(){
        $flowId=(int)$_POST['flowId'];
        $taskExtraId=(int)$_POST['taskExtra'];
        if($flowId>0 && 
                $taskExtraId>0){
            $sql="update ".TASK_FLOW_TABLE." set `type`=$taskExtraId where task_id=$flowId";
            db_query($sql);
            return 1;
                }
                return 0;
        
    }
    
    function getResponseStatus(){
        global $cfg;
        $defaultStatus = $cfg->config['default_response_status']['value'];
        $response = $_POST['responseId'];
        $status = Task::getStatusOfResponse($response);
        if(!empty($status)) return $status;
        return $defaultStatus;
    }

    function changeField(){
        
         global $thisstaff;
         $taskId = $_POST["taskId"];
         $task=Task::lookup($taskId);
         $taskdataObj = TaskData::lookup($taskId);

         $ticketId = $task->object_id;
         
        $staticTaskData=getCachedTaskFields();

         $change = $_POST["change"];
         if($change=="Dept"){
             $dept = $_POST["valueOfField"];
             $transferForm = new TransferForm();
             $transferForm->setDept($dept);
             $errors = array();
             $result = $task->transfer($transferForm,$errors);
         }elseif($change=="duedate"){
             $duedate = $task->duedate;
             $task->duedate = date('Y-m-d H:i:s',strtotime($_POST["valueOfField"]));
             
             $task->postNote(array(
                        'note' => 'Duedate changed - '.$duedate.' to '.$task->duedate,
                        'title' => __('Due Date Changed'),
                        ),
                    $_errors,
                    $thisstaff);
             $task->save();
             $result=1;
         }
         else{
            $fieldChanged = $_POST["change"];
            $formElement = $_POST["FieldId"];
            $imageRequiredOnTask=$_POST["imageOption"];
	    $attachmentRequiredOnTask=$_POST["attachmentRequired"];
            if($imageRequiredOnTask){
               $taskExtraField=  json_encode(array("imageTemplateVariable"=>$imageRequiredOnTask));
            }
            if ($change == "comment") {
                $key = "taskComment";
                $value = $_POST["valueOfField"];
		$dynamicValue=$_POST["nameOfField"];
            } else {
                $value = json_encode(array($_POST["nameOfField"] => $_POST["valueOfField"]));
		$value_string = $_POST["nameOfField"];
		$dynamicValue=$_POST["nameOfField"];
            }

            if ($change == "response") {
                $key = "taskResponse";
            } else if ($change == "status") {
                $key = "taskStatus";
            }
            $dataToUpdate[$key] = $dynamicValue;
            if ($dataToUpdate) {
                $task->editDynamicData($dataToUpdate);
                $result = 1;
            }
            if ($change == "comment") {
                $task->postNote(array(
                    'note' => $_POST["valueOfField"],
                    'title' => __('Commented'),
                        ), $_errors, $thisstaff);
                $taskdataObj->taskComment = $_POST["valueOfField"];

            }else{
                 $task->logEvent("edited",array('fields' => $fieldChanged.' to '.$_POST["valueOfField"]));
            }
            if ($change == "status" && $_POST["moreInfo"] == "close") {
                $taskdataObj->taskStatus = $value_string;
                if (isset($_POST['closeOnly']))
                    $task->setStatus('closed', 'closeOnly');
                else
                    $task->setStatus('closed');
            }elseif ($change == "status") {
                $taskdataObj->taskStatus = $value_string;
                $task->setStatus('open');
            }
            if ($change == "response") {
                $taskdataObj->taskResponse = $value_string;
                if($taskExtraField){
                    $taskdataObj->extra=$taskExtraField;
   	 	    $taskdataObj->is_attachment_required=$attachmentRequiredOnTask;	
                    $sellerMailQueueObject=SellerMailQueue::create();
                    $sellerMailQueueObject->ticket_id=$ticketId;
                    $sellerMailQueueObject->task_id=$taskId;
                    $sellerMailQueueObject->order_id=$taskdataObj->order_id;
                    $sellerMailQueueObject->manifest_id=$taskdataObj->manifest_id;
                    $sellerMailQueueObject->templateVariables=$taskExtraField;
                    $sellerMailQueueObject->save();
                }
                $taskdataObj->save();
                $previousData=array('taskExtraField'=>$taskExtraField,
	"attachmentRequired"=>$attachmentRequiredOnTask);
                $task->OnResponse($value,$previousData);
                $task->hook("response");
            }
            if (is_object($taskdataObj))
                $taskdataObj->save();
         }
         $task->loadDynamicData(1);
         /*
            Tracking user movements
         */
         $timestamp = date("Y-m-d H:i:s",time());
         $trackingData = array('staff_id' => $thisstaff->ht['staff_id'],
                                'last_timestamp' => $_POST['timeForTracking'],
                                'timestamp' => $timestamp,
                                'event' => 'Changing Task field '.$_POST['change'].":".$_POST["nameOfField"]."-".$_POST["valueOfField"].'',
                                'ticket_id' => $ticketId,
                                'task_id' => $taskId
                                 );
        $trackingObj = new Tracking;
        $trackingObj->track($trackingData,1);
        if($change == "response" && $task->isClosed())
        {
            $trackingData['event'] = "Task closed on ".$change.":".$_POST["nameOfField"]."-".$_POST["valueOfField"];
            $trackingObj = new Tracking;
            $trackingObj->track($trackingData);
        }
        /*
            user tracking code ends
        */
         if($result){
             $res=$task->getTaskResponse();
             $value=$staticTaskData['Default_Task Response'][$res];
             echo json_encode(array("status"=>(string) $task->getTaskStatusName(),"comment"=>(string) $task->getTaskComments(),"response"=>(string) $value,"duedate"=>(string) $task->duedate,"close"=>$task->isClosed(),"updatedTime"=>strtotime($timestamp)));
         }else
             echo 0;
    }

    function add() {
        global $thisstaff;

        $info = $errors = array();
        if ($_POST) {
            Draft::deleteForNamespace('task.add', $thisstaff->getId());
            // Default form
            $form = TaskForm::getInstance();
            $form->setSource($_POST);
            // Internal form
            $iform = TaskForm::getInternalForm($_POST);
            $isvalid = true;
            if (!$iform->isValid())
                $isvalid = false;
            if (!$form->isValid())
                $isvalid = false;

            if ($isvalid) {
                $vars = $_POST;
                $vars['default_formdata'] = $form->getClean();
                $vars['internal_formdata'] = $iform->getClean();
                $desc = $form->getField('description');
                if ($desc && $desc->isAttachmentsEnabled() && ($attachments = $desc->getWidget()->getAttachments()))
                    $vars['cannedattachments'] = $attachments->getClean();
                $vars['staffId'] = $thisstaff->getId();
                $vars['poster'] = $thisstaff;
                $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];
                if (($task = Task::create($vars, $errors)))
                    Http::response(201, $task->getId());
            }

            $info['error'] = __('Error adding task - try again!');
        }

        include STAFFINC_DIR . 'templates/task.tmpl.php';
    }

    function preview($tid) {
        global $thisstaff;

        // No perm. check -- preview allowed for staff
        // XXX: perhaps force preview via parent object?
        if (!$thisstaff || !($task = Task::lookup($tid)))
            Http::response(404, __('No such task'));

        include STAFFINC_DIR . 'templates/task-preview.tmpl.php';
    }

    function edit($tid) {
        global $thisstaff;

        if (!($task = Task::lookup($tid)))
            Http::response(404, __('No such task'));

        if (!$task->checkStaffPerm($thisstaff, Task::PERM_EDIT))
            Http::response(403, __('Permission Denied'));

        $info = $errors = array();
        $forms = DynamicFormEntry::forObject($task->getId(), ObjectModel::OBJECT_TYPE_TASK);

        if ($_POST && $forms) {
            // TODO: Validate internal form
            // Validate dynamic meta-data
            if ($task->update($forms, $_POST, $errors)) {
                Http::response(201, 'Task updated successfully');
            } elseif (!$errors['err']) {
                $errors['err'] = __('Unable to update the task. Correct the errors below and try again!');
            }
            $info = Format::htmlchars($_POST);
        }

        include STAFFINC_DIR . 'templates/task-edit.tmpl.php';
    }

    function massProcess($action) {
        global $thisstaff;

        $actions = array(
            'transfer' => array(
                'verbed' => __('transferred'),
            ),
            'assign' => array(
                'verbed' => __('assigned'),
            ),
            'delete' => array(
                'verbed' => __('deleted'),
            ),
            'reopen' => array(
                'verbed' => __('reopen'),
            ),
            'close' => array(
                'verbed' => __('closed'),
            ),
        );

        if (!isset($actions[$action]))
            Http::response(404, __('Unknown action'));


        $info = $errors = $e = array();
        $inc = null;
        $i = $count = 0;
        if ($_POST) {
            if (!$_POST['tids'] || !($count = count($_POST['tids'])))
                $errors['err'] = sprintf(
                        __('You must select at least %s.'), __('one task'));
        } else {
            $count = $_REQUEST['count'];
        }

        switch ($action) {
            case 'assign':
                $inc = 'task-assign.tmpl.php';
                $form = AssignmentForm::instantiate($_POST);
                if ($_POST && $form->isValid()) {
                    foreach ($_POST['tids'] as $tid) {
                        if (($t = Task::lookup($tid))
                                // Make sure the agent is allowed to
                                // access and assign the task.
                                && $t->checkStaffPerm($thisstaff, Task::PERM_ASSIGN)
                                // Do the transfer
                                && $t->assign($form, $e)
                        )
                            $i++;
                    }

                    if (!$i) {
                        $info['error'] = sprintf(
                                __('Unable to %1$s %2$s'), __('assign'), _N('selected task', 'selected tasks', $count));
                    }
                }
                break;
            case 'transfer':
                $inc = 'task-transfer.tmpl.php';
                $form = TransferForm::instantiate($_POST);
                if ($_POST && $form->isValid()) {
                    foreach ($_POST['tids'] as $tid) {
                        if (($t = Task::lookup($tid))
                                // Make sure the agent is allowed to
                                // access and transfer the task.
                                && $t->checkStaffPerm($thisstaff, Task::PERM_TRANSFER)
                                // Do the transfer
                                && $t->transfer($form, $e)
                        )
                            $i++;
                    }

                    if (!$i) {
                        $info['error'] = sprintf(
                                __('Unable to %1$s %2$s'), __('transfer'), _N('selected task', 'selected tasks', $count));
                    }
                }
                break;
            case 'reopen':
                $info['status'] = 'open';
            case 'close':
                $inc = 'task-status.tmpl.php';
                $info['status'] = $info['status'] ? : 'closed';
                $perm = '';
                switch ($info['status']) {
                    case 'open':
                        // If an agent can create a task then they're allowed to
                        // reopen closed ones.
                        $perm = Task::PERM_CREATE;
                        break;
                    case 'closed':
                        $perm = Task::PERM_CLOSE;
                        break;
                    default:
                        $errors['err'] = __('Unknown action');
                }
                // Check generic permissions --  department specific permissions
                // will be checked below.
                if ($perm && !$thisstaff->hasPerm($perm, false))
                    $errors['err'] = sprintf(
                            __('You do not have permission to %s %s'), __($action), __('tasks'));

                if ($_POST && !$errors) {
                    if (!$_POST['status'] || !in_array($_POST['status'], array('open', 'closed')))
                        $errors['status'] = __('Status selection required');
                    else {
                        foreach ($_POST['tids'] as $tid) {
                            if (($t = Task::lookup($tid)) && $t->checkStaffPerm($thisstaff, $perm ? : null, 1) // Akash Permit
                                    && $t->setStatus($_POST['status'], $_POST['comments'])
                            )
                                $i++;
                        }

                        if (!$i) {
                            $info['error'] = sprintf(
                                    __('Unable to change status of %1$s'), _N('selected task', 'selected tasks', $count));
                        }
                    }
                }
                break;
            case 'delete':
                $inc = 'task-delete.tmpl.php';
                $info[':placeholder'] = sprintf(__(
                                'Optional reason for deleting %s'), _N('selected task', 'selected tasks', $count));
                $info['warn'] = sprintf(__(
                                'Are you sure you want to DELETE %s?'), _N('selected task', 'selected tasks', $count));
                $info[':extra'] = sprintf('<strong>%s</strong>', __('Deleted tasks CANNOT be recovered, including any associated attachments.')
                );

                if ($_POST && !$errors) {
                    foreach ($_POST['tids'] as $tid) {
                        if (($t = Task::lookup($tid)) && $t->getDeptId() != $_POST['dept_id'] && $t->checkStaffPerm($thisstaff, Task::PERM_DELETE) && $t->delete($_POST, $e)
                        )
                            $i++;
                    }

                    if (!$i) {
                        $info['error'] = sprintf(
                                __('Unable to %1$s %2$s'), __('delete'), _N('selected task', 'selected tasks', $count));
                    }
                }
                break;
            default:
                Http::response(404, __('Unknown action'));
        }


        if ($_POST && $i) {

            // Assume success
            if ($i == $count) {
                $msg = sprintf(__('Successfully %s %s.'), $actions[$action]['verbed'], sprintf(__('%1$d %2$s'), $count, _N('selected task', 'selected tasks', $count))
                );
                $_SESSION['::sysmsgs']['msg'] = $msg;
            } else {
                $warn = sprintf(
                        __('%1$d of %2$d %3$s %4$s'), $i, $count, _N('selected task', 'selected tasks', $count), $actions[$action]['verbed']);
                $_SESSION['::sysmsgs']['warn'] = $warn;
            }
            Http::response(201, 'processed');
        } elseif ($_POST && !isset($info['error'])) {
            $info['error'] = $errors['err'] ? : sprintf(
                            __('Unable to %1$s  %2$s'), __('process'), _N('selected task', 'selected tasks', $count));
        }

        if ($_POST)
            $info = array_merge($info, Format::htmlchars($_POST));


        include STAFFINC_DIR . "templates/$inc";
        //  Copy checked tasks to the form.
        echo "
        <script type=\"text/javascript\">
        $(function() {
            $('form#tasks input[name=\"tids[]\"]:checkbox:checked')
            .each(function() {
                $('<input>')
                .prop('type', 'hidden')
                .attr('name', 'tids[]')
                .val($(this).val())
                .appendTo('form.mass-action');
            });
        });
        </script>";
    }

    function transfer($tid) {
        global $thisstaff;

        if (!($task = Task::lookup($tid)))
            Http::response(404, __('No such task'));

        if (!$task->checkStaffPerm($thisstaff, Task::PERM_TRANSFER))
            Http::response(403, __('Permission Denied'));

        $errors = array();

        $info = array(
            ':title' => sprintf(__('Task #%s: %s'), $task->getNumber(), __('Tranfer')),
            ':action' => sprintf('#tasks/%d/transfer', $task->getId())
        );

        $form = $task->getTransferForm($_POST);
        if ($_POST && $form->isValid()) {
            if ($task->transfer($form, $errors)) {
                $_SESSION['::sysmsgs']['msg'] = sprintf(
                        __('%s successfully'), sprintf(
                                __('%s transferred to %s department'), __('Task'), $task->getDept()
                        )
                );
                Http::response(201, $task->getId());
            }

            $form->addErrors($errors);
            $info['error'] = $errors['err'] ? : __('Unable to transfer task');
        }

        $info['dept_id'] = $info['dept_id'] ? : $task->getDeptId();

        include STAFFINC_DIR . 'templates/task-transfer.tmpl.php';
    }

    function assign($tid) {
        global $thisstaff;

        if (!($task = Task::lookup($tid)))
            Http::response(404, __('No such task'));

        if (!$task->checkStaffPerm($thisstaff, Task::PERM_ASSIGN))
            Http::response(403, __('Permission Denied'));

        $errors = array();
        $info = array(
            ':title' => sprintf(__('Task #%s: %s'), $task->getNumber(), $task->isAssigned() ? __('Reassign') : __('Assign')),
            ':action' => sprintf('#tasks/%d/assign', $task->getId()),
        );
        if ($task->isAssigned()) {
            $info['notice'] = sprintf(__('%s is currently assigned to %s'), __('Task'), $task->getAssigned());
        }

        $form = $task->getAssignmentForm($_POST);
        if ($_POST && $form->isValid()) {
            if ($task->assign($form, $errors)) {
                $_SESSION['::sysmsgs']['msg'] = sprintf(
                        __('%s successfully'), sprintf(
                                __('%s assigned to %s'), __('Task'), $form->getAssignee())
                );
                Http::response(201, $task->getId());
            }

            $form->addErrors($errors);
            $info['error'] = $errors['err'] ? : __('Unable to assign task');
        }

        include STAFFINC_DIR . 'templates/task-assign.tmpl.php';
    }

    function delete($tid) {
        global $thisstaff;

        if (!($task = Task::lookup($tid)))
            Http::response(404, __('No such task'));

        if (!$task->checkStaffPerm($thisstaff, Task::PERM_DELETE))
            Http::response(403, __('Permission Denied'));

        $errors = array();
        $info = array(
            ':title' => sprintf(__('Task #%s: %s'), $task->getNumber(), __('Delete')),
            ':action' => sprintf('#tasks/%d/delete', $task->getId()),
        );

        if ($_POST) {
            if ($task->delete($_POST, $errors)) {
                $_SESSION['::sysmsgs']['msg'] = sprintf(
                        __('%s #%s deleted successfully'), __('Task'), $task->getNumber(), $task->getDept());
                Http::response(201, 0);
            }
            $info = array_merge($info, Format::htmlchars($_POST));
            $info['error'] = $errors['err'] ? : __('Unable to delete task');
        }
        $info[':placeholder'] = sprintf(__(
                        'Optional reason for deleting %s'), __('this task'));
        $info['warn'] = sprintf(__(
                        'Are you sure you want to DELETE %s?'), __('this task'));
        $info[':extra'] = sprintf('<strong>%s</strong>', __('Deleted tasks CANNOT be recovered, including any associated attachments.')
        );

        include STAFFINC_DIR . 'templates/task-delete.tmpl.php';
    }

    function task($tid) {
        global $thisstaff;

        if (!($task = Task::lookup($tid)) || !$task->checkStaffPerm($thisstaff))
            Http::response(404, __('No such task'));

        $info = $errors = array();
        $note_form = new SimpleForm(array(
            'attachments' => new FileUploadField(array('id' => 'attach',
                'name' => 'attach:note',
                'configuration' => array('extensions' => '')))
        ));

        if ($_POST) {

            switch ($_POST['a']) {
                case 'postnote':
                    $vars = $_POST;
                    $attachments = $note_form->getField('attachments')->getClean();
                    $vars['cannedattachments'] = array_merge(
                            $vars['cannedattachments'] ? : array(), $attachments);
                    if (($note = $task->postNote($vars, $errors, $thisstaff))) {
                        $msg = __('Note posted successfully');
                        // Clear attachment list
                        $note_form->setSource(array());
                        $note_form->getField('attachments')->reset();
                        Draft::deleteForNamespace('task.note.' . $task->getId(), $thisstaff->getId());
                    } else {
                        if (!$errors['err'])
                            $errors['err'] = __('Unable to post the note - missing or invalid data.');
                    }
                    break;
                default:
                    $errors['err'] = __('Unknown action');
            }
        }

        include STAFFINC_DIR . 'templates/task-view.tmpl.php';
    }

    function taskType() {
        $html = "<option value=''>Select</option>";
        $tasktypes = getCachedTaskFields();

        $task_ids = explode(',', Dept::lookup($_GET['dept_id'])->ht['task_id']);
        foreach ($task_ids as $id) {
            $task_extra = $id;
            $task_value = $tasktypes['Task Types'][$id];
            $task_id = array_search($task_value, $tasktypes['Default_Task Types']);
            $html = $html . "<option value='" . $task_extra . "-" . $task_id . "-" . $task_value . "'>" . $task_value . "</option>";
        }
        return $html;
    }

  function task_update_attachment(){
        global $thisstaff;
        $staff_id=$thisstaff->getId();
        $user_id=NULL;
        $task_id=$_POST['task_id'];
        if(!$_POST['attach:response']){
        return 0;
        }
        $arr=$_POST['attach:response'];
                $type='A';
                $object_id=$task_id;
    
                $task=Task::lookup($task_id);
               if(!$task){
               return 0;
                }
   /*if($order_id = $task->_answers['order_id']){
                $object_id=$order_id;
                $type='O';
                }else if($manifest_id = $task->_answers['manifest_id']){
                $object_id=$manifest_id;
                $type='M';
                }else if($product_id = $task->_answers['product_id']){
                $object_id=$product_id;
                $type='P';
                }

*/
               $ticket_id = $task->ht['object_id'];
        foreach($arr as $k=>$v){
        $f=explode(",",$v);
        $attachParam = array("object_id" => $object_id, "file_id" => $f[0], "type" => $type, "inline" => 0, "staff_id" => $staff_id, "user_id" => $user_id, "ticket_id" => $ticket_id);

        $attachParamObj = Attachment::create($attachParam);
        $attachParamObj->created = SqlFunction::NOW();
        $attachParamObj->save();
                      }
return 1;
  }
}
?>
