<?php

/*
 * Class handling oder Status table
 * @author - Akash
 */

class OrderStatusCron extends VerySimpleModel {

    public $cloneParentCompleteId = "Z";
    static $meta = array(
        'table' => ORDER_CRON_TABLE,
        'pk' => array('id')
    );

    public function preventMultipleCron($timeGap = 1) {
        $ordersUpdatedTime = OrderStatusCron::objects()->filter(array("processed" => 1))->values('update_timestamp')->order_by('-update_timestamp')->limit(1)->all();
        if ((time() - strtotime($ordersUpdatedTime[0]["update_timestamp"])) < $timeGap) {
            // Log Info for BulkJob Error
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D5"];
            $module = $logConfig["module"][$domain]["D5_M2"];
            $error = $logConfig["error"]["E4"];
            $level = $logConfig["level"]["INFO"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $level, $error, "title:Bulk Job running", "log_location:class.bulkjob.php", "", "", "Time-" . $ordersUpdatedTime[0]["update_timestamp"]);
            //die("Another Cron Running");
        }
    }
    
    public function getOrders($max = 1) {
        
        $sql = 'SELECT * from '.ORDER_CRON_TABLE.' WHERE processed=0 AND status_type="Order" ORDER BY id LIMIT '.$max;
        $query = db_query($sql);
        global $cfg, $ost;
        while ($order = db_fetch_array($query)) {
            $logOrder = 1;
            if ($doNotConsider[$order['order_id']]) {
                $logOrder = 0;
            }
            if ($logOrder) {
                $statuses_query = 'SELECT status_type,to_status,transition_time FROM '.ORDER_CRON_TABLE.' WHERE order_id='.$order['order_id'].' ORDER BY id';
                $result = db_query($statuses_query);
                while ($statusArr = db_fetch_array($result)) {
                    $orderStatusTemp = "Order";
                    if (!$statusChange[$orderStatusTemp] || ($statusArr["transition_time"] <= $order['transition_time'] && $statusArr["transition_time"] > $statusChange[$orderStatusTemp]["time"])) {
                        $statusChange[$orderStatusTemp]["status"] = $statusArr["to_status"];
                        $statusChange[$orderStatusTemp]["time"] = $statusArr["transition_time"];
                    }
                }
                $currentOrderStatus = $order['to_status'];
                $orderStatus = "Order";
                $orderArr[$orderStatus][$order['order_id']]["Details"] = $currentOrderStatus;
                $orderArr[$orderStatus][$order['order_id']]["Object"] = $order;
                $doNotConsider[$order['order_id']] = 1;
            }
        }
        return $orderArr;
    }

    public function performTaskList() {
        $taskFields = getCachedTaskFields();
        $response = $taskFields["Default_Task Response"];

        $taskStatusResponseObject = OrderFlow::objects()->filter(array("flowstatus"=>1));
        foreach ($taskStatusResponseObject as $taskStatusResponseList) {
            $res = array();
            $resValue = "";
            $dept = $taskStatusResponseList->dept;
            $statusType = $taskStatusResponseList->status_type;
            $refundStatusVal = 0;
            $statusVal = $taskStatusResponseList->status;
            $res = $taskStatusResponseList->task_response;
            $taskType = $taskFields["Task Types"][$taskStatusResponseList->task_type];
            $communication = $taskStatusResponseList->communication;
            $performArr[$statusType][$statusVal][$refundStatusVal][$taskType][$dept]["response"] = $res;
            $performArr[$statusType][$statusVal][$refundStatusVal][$taskType][$dept]["communication"] = $communication;
        }
        return $performArr;
    }
    
    public function ticketFlowList() {
        $TicketFlowObject = TicketFlow::objects();
        foreach ($TicketFlowObject as $ticketFlow) {
            $res = array();
            $resValue = "";
            $statusType = $ticketFlow->status_type;
            $statusVal = $ticketFlow->status;
            $help_topic = $ticketFlow->help_topic;
            $changeStatus = $ticketFlow->ChangeStatus;


            $closeTicketArr[$statusType][$statusVal][$help_topic] = $changeStatus;
        }
        return $closeTicketArr;
    }

    public function PerformOperation($ordersArr, $performArr, $ticketFlowArr) {
        $taskStaticData = getCachedTaskFields();
        foreach ($ordersArr as $statusType => $orderStatus) {
            foreach ($orderStatus as $orderId => $status) {
                if (!$processedOrder[$orderId]) {
                    if ($performArr[$statusType][$status["Details"]]) {
                        $findTicketArr = array("odata__order_ids__contains" => $orderId, "status__state" => 'open');
                        $ticketIdsArr = array();
                        $tickets = Ticket::objects()->filter($findTicketArr)->values("topic_id", "ticket_id")->all();
                        foreach ($tickets as $t) {
                            if ($ticketFlowArr[$statusType][$status["Details"]][$t["topic_id"]]) {
                                $ticketToBeClosedTicket = Ticket::lookup($t["ticket_id"]);
                                $ticketToBeClosedTicket->setStatus($ticketFlowArr[$statusType][$status["Details"]][$t["topic_id"]], 'By system - On ticket Logical End');
                            } else {
                                $ticketIdsArr[] = $t["ticket_id"];
                            }
                        }
                        $taskId = array();
                        $ticketRelated = array();
                        if (count($ticketIdsArr) > 0) {
                            $tasksObj = Task::objects()->filter(array("object_type" => "T", "flags"=>1, "object_id__in" => $ticketIdsArr))->values('id', 'dept_id', 'object_id');
                            //$tasksObj = Task::objects()->filter(array("object_type" => "T", "flags"=>1, "object_id__in" => array('322356')))->values('id', 'dept_id', 'object_id');
				foreach ($tasksObj as $task) {
                                $taskId[] = $task["id"];
                                $taskDept[$task["id"]] = $task["dept_id"];
                                $ticketRelated[$task["id"]] = $task["object_id"];
                            }
                            if (count($taskId) > 0) {
                                $taskTypeArr = array();
                                $taskTypeObj = Task::objects()->filter(array('id__in' => $taskId,"flags"=>1))->values('id', 'data__taskType')->all();
                                foreach ($taskTypeObj as $row) {
                                    $taskTypeArr[$row["id"]] = $taskStaticData["Default_Task Types"][$row["data__taskType"]];
                                }
                                foreach ($taskTypeArr as $id => $type) {
                                    if (!$response) {
                                        if(!$response)
                                            $response = $performArr[$statusType][$status["Details"]][0][$type][$taskDept[$id]]["response"];
                                        if(!$response)
                                            $response = $performArr[$statusType][$status["Details"]][0][$type][0]["response"];
                                        if(!$response)
                                            $response = $performArr[$statusType][0][0][$type][$taskDept[$id]]["response"];
                                        if(!$response)
                                            $response = $performArr[$statusType][0][0][$type][0]["response"];
                                    }
                                    if ($response) {
                                        $data = array();
                                        $data["taskId"] = $id;
                                        $data["agent"] = "SYSTEM - On order Status Update";
                                        $data["email"] = "system.auto@shopclues.com";
                                        $data['source'] = isset($data['source']) ? $data['source'] : 'CRON';
                                        $data["taskResponse"] = $response;
                                        global $statusPerform;
                                        $statusPerform[$ticketRelated[$id]] = $status["Details"];
                                        $updateresult = Task::updateTask($data, $errors, $data['source']);
                                        // Log Info for BulkJob Error
                                        $logConfig = logConfig();
                                        $domain = $logConfig["domain"]["D5"];
                                        $module = $logConfig["module"][$domain]["D5_M1"];
                                        $error = $logConfig["error"]["E5"];
                                        $level = $logConfig["level"]["INFO"];
                                        $logObj = new Analog_logger();
                                        $logObj->report($domain, $module, $level, $error, "title:Auto Response", "log_location:class.orderStatus.php", "task:" . $id, "Response:" . $response, substr(json_encode($data) . " Error-" . json_encode($errors), 0, 2000));
                                    }
                                    
                                    $communication = $performArr[$statusType][$status["Details"]][$status["Refund"]][$type][$taskDept[$id]]["communication"];
                                    if (!$communication) {
                                        if(!$communication)
                                            $communication = $performArr[$statusType][$status["Details"]][0][$type][$taskDept[$id]]["communication"];
                                        if(!$communication)
                                            $communication = $performArr[$statusType][$status["Details"]][$status["Refund"]][$type][0]["communication"];
                                        if(!$communication)
                                            $communication = $performArr[$statusType][$status["Details"]][0][$type][0]["communication"];
                                        if(!$communication)
                                            $communication = $performArr[$statusType][0][0][$type][$taskDept[$id]]["communication"];
                                        if(!$communication)
                                            $communication = $performArr[$statusType][0][$status["Refund"]][$type][0]["communication"];
                                        if(!$communication)
                                            $communication = $performArr[$statusType][0][$status["Refund"]][$type][$taskDept[$id]]["communication"];
                                        if(!$communication)
                                            $communication = $performArr[$statusType][0][0][$type][0]["communication"];
                                    }
                                    if ($communication) {
                                        global $cfg;
                                        
                                        $ticketObj = Ticket::lookup($ticketRelated[$id]);
                                        //Alert
                                        if ($cfg->alertONOrderStatusChange()) {
                                            if (!$cfg || !($dept = $ticketObj->getDept()) || !($tpl = $dept->getTemplate()) || !($email = $dept->getAutoRespEmail())
                                                ) {
                                                 return false;  //bail out...missing stuff.
                                                }
                                                $template = EmailTemplate::lookup($communication);
                                                if(is_object($template) && $template->getCodeName()){
                                                    $templateName = $template->getCodeName();
                                                }
$autorespond=1;
//Send auto response - if enabled.
                                                if ($autorespond && $templateName && ($msg = $tpl->getMsgTemplate($templateName))
                                                ) {
                                                    $msg = $ticketObj->replaceVars(
                                                            $msg->asArray(), array('message' => $message,
                                                        'recipient' => $ticketObj->getOwner(),
                                                        'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : ''
                                                        )
                                                    );
                                                    $options = array();
                                                    $email->sendAutoReply($ticketObj->getOwner(), $msg['subj'], $msg['body'], null, $options);
                                                    $autoSentMessage["note"] = "Auto Mail - <br>" . $msg['subj'] . "<br>" . $msg['body'];
                                                    $ticketObj->postNote($autoSentMessage);
                                                }
                                        }

                                        $tasksDueDatesArr = array();
                                        // Reset Due date
                                        $tasksDueDates = Task::objects()->filter(array("object_type" => "T", "object_id" => $ticketRelated[$id], "flags" => 1))->values("duedate")->all();
                                        foreach ($tasksDueDates as $arrDate) {
                                            $tasksDueDatesArr[] = $arrDate["duedate"];
                                        }

                                        if ($tasksDueDatesArr) {
                                            $maxDuedate = max($tasksDueDatesArr);
                                            $diffSuggested = ($maxDuedate - time()) / 3600 * 24;
                                            if ($diffSuggested > 7) {
                                                $diffSuggestedDays = 3;
                                            } else {
                                                $diffSuggestedDays = 2;
                                            }
                                            $maxDuedateTOBeGiven = time() + $diffSuggestedDays * 24 * 3600;
                                            $ticketObj = Ticket::lookup($ticketRelated[$id]);
                                            $ticketObj->duedate = date('Y-m-d H:i:s', $maxDuedateTOBeGiven);
                                            $ticketObj->est_duedate = date('Y-m-d H:i:s', $maxDuedateTOBeGiven);
                                            $ticketObj->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $sql = 'UPDATE '.ORDER_CRON_TABLE.' SET processed=1 WHERE order_id='.$orderId;
                    db_query($sql);
                }
                $processedOrder[$orderId] = 1;
            }
        }
    }

}
