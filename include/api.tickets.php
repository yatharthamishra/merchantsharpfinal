<?php
require_once (INCLUDE_DIR.'class.api.php');
require_once (INCLUDE_DIR.'class.ticket.php');
class TicketApiController extends ApiController {
    # Supported arguments -- anything else is an error. These items will be
    # inspected _after_ the fixup() method of the ApiXxxDataParser classes
    # so that all supported input formats should be supported

    function getRequestStructure($format, $data = null) {
        $supported = array(
            "alert", "autorespond", "source", "topicId",
            "attachments" => array("*" =>
                array("name", "type", "data", "encoding", "size")
            ),
            "message", "ip", "priorityId"
        );
        # Fetch dynamic form field names for the given help topic and add
        # the names to the supported request structure
        if (isset($data['topicId']) && ($topic = Topic::lookup($data['topicId'])) && ($forms = $topic->getForms())) {
            foreach ($forms as $form)
                foreach ($form->getDynamicFields() as $field)
                    $supported[] = $field->get('name');
        }

        # Ticket form fields
        # TODO: Support userId for existing user
        if (($form = TicketForm::getInstance()))
            foreach ($form->getFields() as $field)
                $supported[] = $field->get('name');

        # User form fields
        if (($form = UserForm::getInstance()))
            foreach ($form->getFields() as $field)
                $supported[] = $field->get('name');

        if (!strcasecmp($format, 'email')) {
            $supported = array_merge($supported, array('header', 'mid',
                'emailId', 'to-email-id', 'ticketId', 'reply-to', 'reply-to-name',
                'in-reply-to', 'references', 'thread-type',
                'flags' => array('bounce', 'auto-reply', 'spam', 'viral'),
                'recipients' => array('*' => array('name', 'email', 'source'))
            ));

            $supported['attachments']['*'][] = 'cid';
        }

        return $supported;
    }

    /*
      Validate data - overwrites parent's validator for additional validations.
     */

    function validate(&$data, $format, $strict = true) {
        global $ost;

        //Call parent to Validate the structure
        if (!parent::validate($data, $format, $strict) && $strict)
            $this->exerr(400, __('Unexpected or invalid data received'));

        // Use the settings on the thread entry on the ticket details
        // form to validate the attachments in the email
        $tform = TicketForm::objects()->one()->getForm();
        $messageField = $tform->getField('message');
        $fileField = $messageField->getWidget()->getAttachments();

        // Nuke attachments IF API files are not allowed.
        if (!$messageField->isAttachmentsEnabled())
            $data['attachments'] = array();

        //Validate attachments: Do error checking... soft fail - set the error and pass on the request.
        if ($data['attachments'] && is_array($data['attachments'])) {
            foreach ($data['attachments'] as &$file) {
                if ($file['encoding'] && !strcasecmp($file['encoding'], 'base64')) {
                    if (!($file['data'] = base64_decode($file['data'], true)))
                        $file['error'] = sprintf(__('%s: Poorly encoded base64 data'), Format::htmlchars($file['name']));
                    $file['encoding'] = "";
                }
                // Validate and save immediately
                try {
                    $F = $fileField->uploadAttachment($file);
                    $file['id'] = $F->getId();
                } catch (FileUploadError $ex) {
                    $file['error'] = $file['name'] . ': ' . $ex->getMessage();
                }
            }
            unset($file);
        }

        return true;
    }

    function create($format) {
        global $cfg;
        global $__send_to_master;
        global $APIKey;
        $memcacheTiketFirstTime=$cfg->config['ticketTimeMemcacheFirstExpire']['value'];
        $memcache=new MemcacheStorage($memcacheTiketFirstTime);
        
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $APIKey = $this->requireApiKey();
        $ticket = null;
        if (!strcasecmp($format, 'email')) {
            # Handle remote piped emails - could be a reply...etc.
            $ticket = $this->processEmail();
        } else {
            $data = $this->getRequest($format);
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D1"];
            $module = $logConfig["module"][$domain]["D1_M1"];
            $error = $logConfig["error"]["E16"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $logConfig["level"]["INFO"], $error, "title:Ticket Creation API Data", "log_location:api.ticket.php", "", "", substr(json_encode($data), 0, 2000));

            #Parse request body
            $data['email'] = str_replace(' ', '', $data['email']);
            $data['name'] = !isset($data['name']) ? preg_replace('/[^A-Za-z0-9\-]/', '', explode("@", $data['email'])[0]) : $data["name"];

            if (!empty($data['agent'])) {
                $agentObj = Staff::lookup($data['agent']['email']);
                if (!$agentObj && $data['agent']['email'] && $data['agent']['firstname']) {
                    $adminAgentConfig = json_decode($cfg->config['adminAgentConfigSingleSignon']['value'], true);
                    $defaultPassDec = $adminAgentConfig['password']['decoded']; //'welcome';
                    $defaultPassEnc = $adminAgentConfig['password']['encoded']; //; // Setting Password as welcome
                    $newStaff = Staff::create();
                    $newStaff->username = $data['agent']['email'];
                    $newStaff->firstname = $data['agent']['firstname'];
                    $newStaff->lastname = $data['agent']['lastname'];
                    $newStaff->isactive = $adminAgentConfig['default_active'];
                    $newStaff->created = SqlFunction::NOW();
                    $newStaff->permissions = json_encode($priv);
                    $newStaff->isadmin = $adminAgentConfig['default_admin'];
                    $newStaff->dept_id = $adminAgentConfig['default_dept_id'];
                    $newStaff->role_id = $adminAgentConfig['default_role_id'];
                    $newStaff->passwd = $defaultPassEnc;
                    $newStaff->max_page_size = 50;
                    $newStaff->email = $data['agent']['email'];
                    $newStaff->save();
                    $agentObj = Staff::lookup($data['agent']['email']);
                }
                if (!$agentObj) {
                    return array("error" => 1, "msg" => "Invalid agent Info");
                }
            }
            if (empty($data["topicId"])) {
                return $this->exerr(500, __("Unable to create new Ticket: Issue Type Error"));
            }

	   global $cfg;
           if ($cfg->config['allow_duplicate_ticket']['value'] && Ticket::checkDupicateTicket($data)) {
               
               return $this->exerr(500, __("Unable to create new Ticket: Duplicate ticket"));
            }          
            $error=array();
            if($cfg->config['validate_ticket_info']['value'] && Ticket::validateTicket($data,$error)) {
               return $this->exerr(500, __("Unable to create new Ticket: ".$error['err']));
            }
            

            global $mandateInfoCreation;
            global $mandateDataCreation;
            global $mapIds;
            $mapIds=array();
            if(empty($mandateDataCreation)){
                $mandateDataCreation=[];
            }
            if (count($mandateDataCreation) > 0) {
                $inValidsMandateIds = TicketUtilityFunctions::checkIfMandatoryInfoValid($mandateInfoCreation, explode(',', $mandateDataCreation));
                if (count($inValidsMandateIds) > 0) {
                    return $this->exerr(500, __("Unable to create new Ticket: " . 'Mandate Ids not valid' . implode(',', $inValidsMandateIds)));
                }
            }
            $data["mandateType"]=$mandateInfoCreation;
            $data["mandateIds"]=$mandateDataCreation;
            $ticket = $this->createTicket($data);
           

            if (!$ticket)
                return $this->exerr(500, __("Unable to create new ticket: unknown error")); 
            if($cfg->config['merchant_ticket_enable_queue']['value'] && $cfg->config['merchantTicketCreation']['value']){
                global $merchantMessage;
                $dataForQueue = array("ticket"=>  serialize($ticket),"data"=>$data,"callingobject"=>  serialize($this),"messageVersion"=>MERCHANT_CONSUMER_VERSION,
                    "ticketId"=>$ticket->getId(),'merchantMessage'=>serialize($merchantMessage));
                $ticketCreationEmailQueue = json_decode($cfg->config['merchantTicketCreation']['value'],true);
                
                $exchange = $ticketCreationEmailQueue["exchange"];
                $key =  $ticketCreationEmailQueue["key"];
                
                AddToQueue(($dataForQueue), $exchange, $key);
                $logObj = new Analog_logger();
                $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TASK ADDED TO QUEUE", "ticker_id:".$ticket->getId(),"", "", "VARS:".substr(json_encode($vars),0,2000));
                $this->response(201, $ticket->getNumber());
            }
            
            unset($data["task"]);
            
            if ($data["isReturn"] && $data["order_id"] && $data["return_id"]) {
                
            } else {
                $orderRelatedData=[];
                if($mandateInfoCreation=="order" || $mandateInfoCreation=="manifest"){
                    $orderRelatedData=Task::getOrderInfoMap($data);
                }
                $tasksToBeMade = Task::firstTasksToBeMade($data,$orderRelatedData,$ticket);
                global $merchantMessage;
                if(empty($tasksToBeMade)){
		    $ticket->sendMail($ticket,$data,$merchantMessage);
                    $logObj = new Analog_logger();
                    $logObj->report($domain, $module, $logConfig["level"]["INFO"], $logConfig["error"]["E2"], "title:TASK MAPPING NOT FOUND", "ticker_id:".$ticket->getId(),"Calling Side:Client", "", "VARS:".substr(json_encode($vars),0,2000));
                    $map_error= " No task mapping found";                
                }else {
                    $dataTask["task"] = $tasksToBeMade;
                    $data['task_mandate_type']=$tasksToBeMade[0]['task_mandate_type'];
                    $dataTask["ticketId"] = $ticket->getId();
                    $dataTask["duedate"] = $ticket->est_duedate;
                    $dataTask["source"] = 2; // On ticket Create 
                    $dataTask["orderReportingMasterData"] = $orderInfoMap;
                    $dataTask["mandateType"]=$data["mandateType"];
                    $task = $this->createTask($dataTask);
		    $sql = 'select max(ceil((TIMESTAMPDIFF(SECOND,created,duedate))/(24*60*60))) as max_sla from mst_task where object_id='.$dataTask["ticketId"];
                    $sla = db_result(db_query($sql));
                    $ticket->sendMail($ticket,$data,$merchantMessage,$sla);
                }
            }
           
            if (!empty($data['attachment'])) {

            $userId=$ticket->getUserId();

            foreach ($task as $t) {
                $t->taskAttachments($data,NULL,$userId);
            }
                $ticket->attachments($data['attachment']);
        }
            if ($map_error)
                $this->response(201, $ticket->getNumber().$map_error); 
            if (empty($task))
                return $this->exerr(500, __($ticket->getNumber()." Unable to create new Task: unknown error"));
        }
       
        $this->response(201, $ticket->getNumber());
    }
    
 
    
    function findStatusCorrespondingToMandateInfo($mandateInfo,$mandateIds){
        $mandateIdToMandateStatus=[];
        switch ($mandateInfo){
            case 'product':
                $productIdsFromDB=ProductDetails::objects()->filter(array('product_id__in'=>$mandateIds))->values('product_id','status')->all();
                
                foreach ($productIdsFromDB as $productIdFromDB) {
                    $mandateIdToMandateStatus[$productIdFromDB['product_id']] = $productIdFromDB['status'];
                }
                break;
            case 'batch':
                
                break;
        }
        return $mandateIdToMandateStatus;
    }

    function createTaskForTicket($format) {
        global $cfg;
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $data = $this->getRequest($format);

        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D1"];
        $module = $logConfig["module"][$domain]["D1_M1"];
        $error = $logConfig["error"]["E16"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $logConfig["level"]["INFO"], $error, "title:Return Task Creation API Data", "log_location:api.ticket.php", "", "", substr(json_encode($data), 0, 2000));

        $ticket = Ticket::lookupByNumber($data["ticket_id"]);
        $returnArr = TicketOdata::objects()->filter(array("ticket_id" => $ticket->getId()))->values("retrn_id", "order_id")->all();

        $data["return_id"] = $returnArr[0]["retrn_id"];
        $data["order_id"] = $returnArr[0]["order_id"];
        $data["issue_id"] = 691;
        $data["sub_issue_id"][] = 0;
        $data["sub_sub_issue_id"][] = 0;

        if ($ticket && $data["return_id"]) {
            $rmaProducts = CscartRMAReturnsProducts::objects()->filter(array("return_id" => $data["return_id"]))->values("return_id", "product__meta_category_id", "product__sub_category_id", "reason", "price", "issue__issue_id", "product_id")->distinct("return_id", "product_id")->all();
            $data["category"][] = 0;
            $data["value"][] = 0;
            $data["sub_reason"][] = 0;
            foreach ($rmaProducts as $rma) {
                if ($rma["product__meta_category_id"])
                    $data["category"][] = $rma["product__meta_category_id"];
                if ($rma["product__sub_category_id"])
                    $data["category"][] = $rma["product__sub_category_id"];
                $data["sub_reason"][] = $rma["reason"];
                $data["sub_issue_id"][] = $rma["issue__issue_id"];
            }

            $rmaHistory = CscartRmaReturns::objects()->filter(array("return_id" => $data["return_id"]))->limit(1)->values("status", "customer_action")->all();
            $data["return_status"][] = '0';
            $data["return_action"][] = 0;
            foreach ($rmaHistory as $rma) {
                $data["return_status"][] = $rma["status"];
                $data["return_action"][] = $rma["customer_action"];
            }
            $data["category"] = array_unique($data["category"]);
            $data["sub_reason"] = array_unique($data["sub_reason"]);


            $data["fulfillment_id"][] = 0;
            $orderDetails = CscartOrders::objects()->filter(array("order_id" => $data["order_id"]))->values("total", "cb_used", "cb_used", "company__fulfillment_id", "status")->all();
            $data["fulfillment_id"][] = $orderDetails[0]["company__fulfillment_id"]? : 0;
            $data["value"] = $orderDetails[0]["total"] + $orderDetails[0]["cb_used"] + $orderDetails[0]["gc_used"];
            $data["ord_status"] = $orderDetails[0]["status"];

            $filter = array("issue_id__in" => array(0, $data["issue_id"]),
                'merchant_type__in' => $data["fulfillment_id"],
                'category__in' => $data["category"],
                'return_action__in' => $data["return_action"],
                'sub_issue_id__in' => $data["sub_issue_id"],
                'sub_sub_issue_id__in' => $data["sub_sub_issue_id"],
                'sub_reason__in' => $data["sub_reason"],
                'return_status__in' => $data["return_status"]
            );

            $taskToCreateArr = ReturnTaskAssign::objects()
                            ->filter($filter)
                            ->values("issue_id", "sub_issue_id", "sub_sub_issue_id", 'merchant_type', 'category', 'return_status', 'value_min', 'value_max', 'sub_reason', 'return_action', 'dept', 'task_type')->all();
            foreach ($taskToCreateArr as $taskInfo) {
                if (($taskInfo["value_min"] <= $data["value"] && $taskInfo["value_max"] >= $data["value"]) || ($taskInfo["value_max"] == 0 && $taskInfo["value_min"] == 0)) {
                    if (!$taskCreated[$taskInfo["dept"] . "-" . $taskInfo["task_type"]]) {
                        $taskCreated[$taskInfo["dept"] . "-" . $taskInfo["task_type"]] = 1;
                        $taskToCreate["department_id"] = $taskInfo["dept"];
                        $taskToCreate["task_id"] = $taskInfo["task_type"];
                        $responsePendingStatus = explode(",", $adminConfig["config"]['osTicket']['taskStatusMapWithOrderStatus']['Response Pending']);
                        $taskToCreate["status"] = in_array($data["ord_status"], $responsePendingStatus) ? 2 : 1;
                        $data["task"][] = $taskToCreate;
                    }
                }
            }
        }
        if ($data["task"]) {
            $dataTask["task"] = $data["task"];
            $dataTask["ticketId"] = $ticket->getId();
            $dataTask["duedate"] = $ticket->est_duedate;
            $dataTask["source"] = 2; // On ticket Create
            $task = $this->createTask($dataTask);
            if (!$task)
                return $this->exerr(500, __("Unable to create new Task: unknown error"));
            if ($cfg->config['autoTaskautoTriage']['value']) {
                $autoTriageFields = json_decode($cfg->config['autoTaskautoTriage']['value'], true);
                $triageData["priority"] = $autoTriageFields["priority"];
                $triageData["communication"] = $autoTriageFields["communication"];
                $triageData["triage"] = $autoTriageFields["triage"];
                $triageData["call_customer"] = $autoTriageFields["call_customer"];
                $triageData["ticket_id"] = $ticket->getId();

                $note = "Auto Triaged";
                $ticket->setStatus(6, $note, $errors, true, 'Triaged');
                $ticket->triageTicket($triageData);
            }
        }
        if (!empty($data["attach"])) {
            $attachData = array("ticket_id" => $ticket->getId(), "agent" => "System Auto", "email" => "system.auto@shopclues.com", "attach" => $data["attach"]);
            $this->updateTicket($attachData);
        }
        //Respond Bucketing task
        if (count($dataTask) > 0) {
            $respondBucketTask = json_decode($cfg->config['resspondBucketTask']['value'], true);
            $bucketTaskDept = $respondBucketTask["Dept"];
            $bucketTask = $respondBucketTask["Task"];
            $respondBucketTask = $respondBucketTask["Response"];
            if ($respondBucketTask) {
                $tasksObj = Task::objects()->filter(array("object_type" => "T", "object_id" => $ticket->getId(), "flags" => 1, "dept_id" => $bucketTaskDept));
                foreach ($tasksObj as $task) {
                    $data = array();
                    $data["taskId"] = $task->id;
                    $data["agent"] = "SYSTEM - On Exceptional Return Status Update";
                    $data["email"] = "system.auto@shopclues.com";
                    $data["taskResponse"] = $respondBucketTask;
                    $updateresult = Task::updateTask($data, $errors, $data['source']);
                }
            }
        }
        return $this->exerr(200, json_encode(array('status' => "success", 'tasks' => count($data["task"]))));
    }

    function createForwardTask($ticketIdArr) {
        $memcacheObj = new MemcacheStorage();
        $done = json_decode($memcacheObj->getValue("re"), true);

        $ticketObjArr = Ticket::objects()->filter(array("created__gt" => '2017-01-19 10:00:00', "created__lt" => '2017-01-20 05:00:00', 'ticket_id__notin' => array_keys($done)));
        foreach ($ticketObjArr as $ticket) {
            $taskCount = Task::objects()->filter(array("object_id" => $ticket->ticket_id))->count();
            if ($taskCount || $done[$ticket->ticket_id] || $ticket->topic_id == 70) {
                $done[$ticket->ticket_id] = 1;
                $memcacheObj->setValue("re", json_encode($done));
                continue;
            }
            $ticketDataArr = TicketOData::objects()->filter(array("ticket_id" => $ticket->ticket_id))->values()->all();
            $data["order_id"] = $ticketDataArr[0]["order_id"];
            $data["ord_status"] = $ticketDataArr[0]["ord_status"];
            $data["refund_status"] = $ticketDataArr[0]["refund_status"];
            $data["fulfillment_id"] = $ticketDataArr[0]["merchant_type"];
            $ticketTopicNumber = Ticket::objects()->filter(array("topic_id" => $ticket->topic_id, "ticket_id__lt" => '509359', "ticket_id__gt" => '500598'))->limit(1)->values("number")->all()[0]["number"];
            $already = 1;
            $cluesCustomerQueries = CluesCustomerQueries::objects()->filter(array("ost_ticket_id" => $ticket->number, "issue_id__neq" => 10000))->values()
                            ->limit(1)->all();
            if (!$cluesCustomerQueries[0]["ost_ticket_id"]) {
                $cluesCustomerQueries = CluesCustomerQueries::objects()->filter(array("ost_ticket_id" => $ticketTopicNumber, "issue_id__neq" => 10000))->values()
                                ->limit(1)->all();
                $already = 0;
            }
            $data["issue_id"] = $cluesCustomerQueries[0]["issue_id"];
            $data["sub_issue_id"] = $cluesCustomerQueries[0]["sub_issue_id"];
            $data["sub_sub_issue_id"] = $cluesCustomerQueries[0]["sub_sub_issue_id"];
            if (!$already) {
                $ccqArr = array("ost_ticket_id" => $ticket->number, "ticket_number" => $ticket->number, "order_id" => $data["order_id"], "customer_email" => $ticket->user->emails->address, "callno" => "NA", "issue_id" => $data["issue_id"], "sub_issue_id" => $data["sub_issue_id"], "sub_sub_issue_id" => $data["sub_sub_issue_id"], "product_id" => 0, "agent_user_id" => 1);
                if (CluesCustomerQueries::objects()->filter(array("ost_ticket_id" => $ticket->number))->values()->limit(1)->count() < 1) {
                    $ccqObj = CluesCustomerQueries::create($ccqArr);
                    $ccqObj->save();
                } else {
                    $ccqObjArr = CluesCustomerQueries::objects()->filter(array("ost_ticket_id" => $ticket->number));
                    foreach ($ccqObjArr as $ccqObj) {
                        $ccqObj->issue_id = $data["issue_id"];
                        $ccqObj->sub_issue_id = $data["sub_issue_id"];
                        $ccqObj->sub_sub_issue_id = $data["sub_sub_issue_id"];
                        $ccqObj->save();
                    }
                } echo "CCq-" . $ticket->number . "------";
            }
            //Creating Task
            if (!$data["ord_status"] || !$data["refund_status"] && !$data["fulfillment_id"]) {
                $ordStatus = OrderReportingMaster::objects()->filter(array("order_id" => $data["order_id"]))->values()->limit(1)->all();
                foreach ($ordStatus as $status) {
                    $data["ord_status"] = $status["order_status"];
                    $data["fulfillment_id"] = $status["fulfillment_id"];
                    $data["refund_status"] = $status["refund_status"]? : 0;
                }
            }

            if ($data["fulfillment_id"] && $data["issue_id"] && $data["ord_status"]) {
                $adminConfig = getCachedAdminConfigFields();
                unset($data["task"]);
                $data["sub_sub_issue_id"] = ($data["sub_sub_issue_id"] > 0) ? $data["sub_sub_issue_id"] : 0;
                $data["refund_status"] = isset($data["refund_status"]) ? $data["refund_status"] : 0;
                $filterNewTask = array(
                    "issue_id" => $data["issue_id"],
                    "sub_issue_id" => $data["sub_issue_id"],
                    "sub_sub_issue_id" => $data["sub_sub_issue_id"],
                    "merchant_type_id" => $data["fulfillment_id"]
                );
                if ($data["refund_status"]) {
                    $filterNewTask["order_status_id__in"] = array($data["ord_status"]);
                    $filterNewTask["refund_status_id__in"] = array($data["refund_status"]);
                } else {
                    $filterNewTask["order_status_id__in"] = array($data["ord_status"]);
                    $filterNewTask["refund_status_id__in"] = array("0");
                }
                $ticketWithTaskResult = TaskWithTicket::objects()
                                ->filter($filterNewTask)
                                ->values('department_id', 'task_id')->all();

                foreach ($ticketWithTaskResult as $tasksWith) {
                    if ($newTasksListed[$tasksWith["department_id"]] != $tasksWith["task_id"]) {
                        $newTasks[] = array("department_id" => $tasksWith["department_id"], "task_id" => $tasksWith["task_id"]);
                        $newTasksListed[$tasksWith["department_id"]] = $tasksWith["task_id"];
                        global $statusPerform;
                        $statusPerform[] = $data["ord_status"];
                    }
                }
            }

            // Log Info for BulkJob Error
            foreach ($newTasks as $taskParam) {
                $taskToCreate["department_id"] = $taskParam["department_id"];
                $taskToCreate["task_id"] = $taskParam["task_id"];
                //Response pending vs new task status
                $responsePendingStatus = explode(",", $adminConfig["config"]['osTicket']['taskStatusMapWithOrderStatus']['Response Pending']);
                $taskToCreate["status"] = in_array($data["ord_status"], $responsePendingStatus) ? 2 : 1;
                $data["task"][] = $taskToCreate;
            }
            if ($data["task"]) {
                $dataTask["task"] = $data["task"];
                $dataTask["ticketId"] = $ticket->getId();
                $dataTask["duedate"] = $ticket->est_duedate;
                $dataTask["source"] = 2; // On ticket Create
                $taskC = new TicketApiController();
                $task = $taskC->createTask($dataTask);
                echo "Task-" . $ticket->getId() . "<br>";
            }
            $done[$ticket->ticket_id] = 1;
            $memcacheObj->setValue("re", json_encode($done));
            echo "Ticket-" . $ticket->getId() . "<br>";
        }
    }

    function getTaskInfoToCreate($data) {

        return $data["task"];
    }

    /* private helper functions */

    function createTask($taskData) {
        
        if(empty($taskData)){
            $this->response(200, 'No Mapping Found');
        }
        $mandateType=$taskData['mandateType'];
        
        $taskMandateType=$taskData['task'][0]['task_mandate_type'];
        $mandateColumnName=$taskMandateType."_id";
        # Pull off some meta-data
        $alert = (bool) (isset($taskData['alert']) ? $taskData['alert'] : true);
        $autorespond = (bool) (isset($taskData['autorespond']) ? $taskData['autorespond'] : true);
        # Assign default value to source if not defined, or defined as NULL
        $taskData['source'] = isset($taskData['source']) ? $taskData['source'] : 'API';
        
        $dataTask = array();
        $staticTaskFields = getCachedTaskFields();
        $listFormated["Task Types"] = $staticTaskFields["create"]["Task Types"];
        $listFormated["Task Status"] = $staticTaskFields["create"]["Task Status"];
        $listFormated["Task Response"] = $staticTaskFields["create"]["Task Response"];
        $listFormated["Task Sources"] = $staticTaskFields["create"]["Task Sources"];
        $taskArray = array();
        foreach ($taskData["task"] as $data) {


            # Create the task with the data (attempt to anyway)
            $dataTask = array(
                "object_type" => "T",
                "object_id" => $taskData["ticketId"],
                "creater" => "System",
                "internal_formdata" => array(
                    "dept_id" => $data["department_id"],
                ),
                "ticket_id" => $taskData['ticketId'],
                "default_formdata" => array(
                    "taskType" => $listFormated["Task Types"][$data["task_id"]],
                    "taskStatus" => $listFormated["Task Status"][$data["status"]],
                    "title" => array_values($listFormated["Task Types"][$data["task_id"]])[0],
                    "description" => array_values($listFormated["Task Types"][$data["task_id"]])[0],
                    "createStatus"=>$data["createStatus"]
                ),
                'orderInfo' => $data['orderInfo']
            );
            $dataTask[$mandateColumnName]=$data[$mandateColumnName];
            $dataTask['task_mandate_type']=$taskMandateType;
            $slaObj = TaskSLA::lookup($data["task_id"]);
            if ($slaObj && $slaObj->sla_id) {
                $slaId = $slaObj->sla_id;
                $dataTask["internal_formdata"]["sla"] = $slaId;
            } else {
                $dataTask["internal_formdata"]["duedate"] = $taskData["duedate"];
            } if ($taskData["source"])
                $dataTask["default_formdata"]["taskSource"] = $listFormated["Task Sources"][$taskData["source"]];

            $task = Task::create($dataTask);
	    if(!empty($data['autoResponse'])){
		$responseData = array();
            	$responseData["taskId"] = $task->id;
            	$responseData["agent"] = "SYSTEM - On Task creation";
            	$responseData["email"] = "system.auto@shopclues.com";
            	$responseData['source'] = 'CRON';
            	$responseData["taskResponse"] = $data['autoResponse'];
	    	$output=Task::updateTask($responseData, $errors, $responseData['source']);
	    }
            array_push($taskArray, $task);
        }
        # Return errors (?)
        if (count($errors)) {
            if (isset($errors['errno']) && $errors['errno'] == 403)
                return $this->exerr(403, __('Task denied'));
            else
                return $this->exerr(
                                400, __("Unable to create new task: validation errors") . ":\n"
                                . Format::array_implode(": ", "\n", $errors)
                );
        } elseif (!$task) {
            return $this->exerr(500, __("Unable to create new task: unknown error"));
        }

        return $taskArray;
    }

    function addThread($inputdata) {
        $data = $this->getRequest($inputdata);
        if (!empty($data['customer_e_mail'])) {
            $newUserObj = User::lookupByEmail($data['customer_e_mail']);
            if ($newUserObj)
                $userId = $newUserObj->getId();
            if (!$userId && stristr($data['customer_e_mail'], "@shopclues.com")) {
                $data['agent']['email'] = $data['customer_e_mail'];
                $nameArr = explode(".", explode("@", $data['customer_e_mail'])[0]);
                $data['agent']['firstname'] = ucwords($nameArr[0]);
                $data['agent']['lastname'] = ucwords($nameArr[1]);
            }
        }
        if (!$userId && !empty($data['agent'])) {
            $agentObj = Staff::lookup($data['agent']['email']);
            if (!$agentObj && $data['agent']['email'] && $data['agent']['firstname']) {
                $adminAgentConfig = json_decode($cfg->config['adminAgentConfigSingleSignon']['value'], true);
                $defaultPassDec = $adminAgentConfig['password']['decoded']; //'welcome';
                $defaultPassEnc = $adminAgentConfig['password']['encoded']; //; // Setting Password as welcome
                $newStaff = Staff::create();
                $newStaff->username = $data['agent']['email'];
                $newStaff->firstname = $data['agent']['firstname'];
                $newStaff->lastname = $data['agent']['lastname'];
                $newStaff->isactive = $adminAgentConfig['default_active'];
                $newStaff->created = SqlFunction::NOW();
                $newStaff->permissions = json_encode($priv);
                $newStaff->isadmin = $adminAgentConfig['default_admin'];
                $newStaff->dept_id = $adminAgentConfig['default_dept_id'];
                $newStaff->role_id = $adminAgentConfig['default_role_id'];
                $newStaff->passwd = $defaultPassEnc;
                $newStaff->max_page_size = 50;
                $newStaff->email = $data['agent']['email'];
                $newStaff->save();
                $agentObj = Staff::lookup($data['agent']['email']);
            }
            if (!$agentObj) {
                return array("error" => 1, "msg" => "Invalid agent Info");
            }
            $agentId = $agentObj->getId();
        }

        $tktObj = Ticket::lookupByNumber($data['ticketNum']);
        if (empty($userId))
            $userId = $tktObj->user_id;
        $data['userId'] = $userId;
        $data['poster'] = $data['customer_e_mail'];
        unset($data['customer_e_mail']);
        $MSG = $tktObj->postMessage($data);
        if (!empty($data["attach"])) {
            $attachData = array("ticket_id" => $tktObj->getId(), "agent" => $data['agent']['firstname'] . " " . $data['agent']['lastname'], "email" => $data['agent']['email'], "attach" => $data["attach"]);
            $tktObj->updateTicket($attachData, $errors);
        }
        if ($agentObj)
            $tktObj->logEvent('behalf', null, $agentObj);
        if (!empty($MSG))
            return TRUE;
        else
            return FALSE;
    }

    function communication($inputdata) {
        $data = $this->getRequest($inputdata);
        $ticketObj = Ticket::lookupByNumber($data['ticket_no']);
        $staticData = getCachedTicketFields();
        $output["ticket"]["status"] = stristr($staticData["status"][$ticketObj->status_id], "lose") ? "closed" : "open";
        $output["ticket"]["created"] = $ticketObj->created;
        $output["ticket"]["closed"] = $ticketObj->closed;
        $output["ticket"]["sla"] = $ticketObj->getEstDueDate();
        $threads = Ticket::objects()->filter(array("ticket_id" => $ticketObj->ticket_id))->values("thread__entries__id", "thread__entries__staff_id", "thread__entries__user_id", "thread__entries__title", "thread__entries__body", "thread__entries__type", "thread__entries__created")->all();

        foreach ($threads as $thread) {
            $communication = array("Title" => $thread["thread__entries__title"],
                "body" => $thread["thread__entries__body"],
                "public" => in_array($thread["thread__entries__type"], array("M", "R")) ? 1 : 0,
                "Type" => $thread["thread__entries__type"],
                "created_at" => $thread["thread__entries__created"],
                "via" => array("channel" => "web"),
                "user_id" => $thread["thread__entries__user_id"]? : 0,
                "User" => $staticData["staff"][$thread["thread__entries__staff_id"]]
            );
            $output["comments"][] = $communication;
        }

        return json_encode($output);
    }

    function isTktStatusClosed($inputdata) {
        $data = $this->getRequest($inputdata);
        include_once(INCLUDE_DIR . 'class.ticket.php');
        $tktObj = Ticket::lookupByNumber($data['ticketNum']);
        $statusId = $tktObj->getStatusId(); //echo 'bjnj';
        if ($statusId == 2 || $statusId == 3)
            $output = json_encode(array('status' => 'CLOSED'));
        else
            $output = json_encode(array('status' => 'NOT CLOSED'));
        return $output;
    }

    /* private helper functions */

    function createTicket(&$data) {

        # Pull off some meta-data
        $alert = (bool) (isset($data['alert']) ? $data['alert'] : true);
        $autorespond = (bool) (isset($data['autorespond']) ? $data['autorespond'] : true);

        # Assign default value to source if not defined, or defined as NULL
        $data['source'] = isset($data['source']) ? $data['source'] : empty($source_notes) ? 'API' : $source_notes;
        $data["source_extra"] = $this->apikey->ht['notes'];

        # Create the ticket with the data (attempt to anyway)
        $errors = array();

        $ticket = Ticket::create($data, $errors, $data['source'], $autorespond, $alert);

        # Return errors (?)
        if (count($errors)) {
            if (isset($errors['errno']) && $errors['errno'] == 403)
                return $this->exerr(403, __('Ticket denied'));
            else
                return $this->exerr(
                                400, __("Unable to create new ticket: validation errors") . ":\n"
                                . Format::array_implode(": ", "\n", $errors)
                );
        } elseif (!$ticket) {
            return $this->exerr(500, __("Unable to create new ticket: unknown error"));
        }

        return $ticket;
    }

    function update($format) {
        if (!($key = $this->getApiKey())) {
            $this->response(201, json_encode(array("status" => "error", "msg" => "Valid API key required")));
        } elseif (!$key->isActive() || $key->getIPAddr() != $_SERVER['REMOTE_ADDR'])
            $this->response(201, json_encode(array("status" => "error", "msg" => "API key not found/active or source IP not authorized")));

        if (!strcasecmp($format, 'email')) {
            # Handle remote piped emails - could be a reply...etc.
            $ticket = $this->processEmail();
        } else {
            # Parse request body
            $ticket = $this->updateTicket($this->getRequest($format));
            $ticket = json_encode($ticket);
            $this->response(201, $ticket);
        }
    }

    function updateTicket($data) {
        # Pull off some meta-data
        $alert = (bool) (isset($data['alert']) ? $data['alert'] : true);
        $autorespond = (bool) (isset($data['autorespond']) ? $data['autorespond'] : true);
        # Assign default value to source if not defined, or defined as NULL
        $data['source'] = isset($data['source']) ? $data['source'] : 'API';
        # Create the ticket with the data (attempt to anyway)
        $errors = array();
        $updateresult = Ticket::updateTicket($data, $errors, $data['source'], $autorespond, $alert);
        if (count($errors)) {
            if (isset($errors['errno']) && $errors['errno'] == 403) {
                return array("status" => "error", "msg" => "Ticket denied");
                exit;
            } else {
                return $updateresult;
                exit;
            }
        }
        return $updateresult;
    }

    function processEmail($data = false) {

        if (!$data)
            $data = $this->getEmailRequest();

        $seen = false;
        if (($entry = ThreadEntry::lookupByEmailHeaders($data, $seen)) && ($message = $entry->postEmail($data))
        ) {
            if ($message instanceof ThreadEntry) {
                return $message->getThread()->getObject();
            } else if ($seen) {
                // Email has been processed previously
                return $entry->getThread()->getObject();
            }
        }

        // Allow continuation of thread without initial message or note
        elseif (($thread = Thread::lookupByEmailHeaders($data)) && ($message = $thread->postEmail($data))
        ) {
            return $thread->getObject();
        }

        // All emails which do not appear to be part of an existing thread
        // will always create new "Tickets". All other objects will need to
        // be created via the web interface or the API
        return $this->createTicket($data);
    }

    function postMessage($inputdata=FALSE){
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
             return $this->exerr(401, __('API key not authorized'));
       
        $data = $this->getRequest($inputdata);
        $ticketObj = Ticket::lookupByNumber($data['ticket_no']);
if(!is_object($ticketObj)){
 return $this->exerr(400, __("Unable to post message: Ticket number is not correct"));

}       
        $mid=$data['mid'];
        $output= Ticket::objects()->filter(array("odata__ticket_id" => $ticketObj->getId(),"odata__merchant_id"=>$mid))->values("user_id")->all();       
if(empty($output[0]["user_id"])){
              return $this->exerr(400, __("Unable to post message: Ticket doesn't belong to merchant"));
         }

         $user=UserModel::objects()->filter(array("id"=>$output[0]["user_id"]))->values("name")->all();
         
         $data['userId']=$output[0]["user_id"];
         $data['poster']=$user[0]["name"];
        $output=$ticketObj->postMessage($data, $origin = '', $alerts = true);
        if($output->ht['type']=='M' && $data["api_attachments"]){
        $attach = $data["api_attachments"];
        $object_id=$output->ht['email_info']->ht['thread_entry_id'];
        $ticket_id=$ticketObj->getId();
        $user_id=$data['user_id'];
        $type = "H";
        foreach($attach as $files){        
            $key = 'MessaageApi' . time() . rand(1, 99999);
            $signature = md5('signature');
            //$files["name"] = pathinfo($files["name"], PATHINFO_FILENAME);
            $fileParam = array("ft" => "T", "bk" => "F", "type" => $files["type"], "size" => $files["size"], "key" => $key, "signature" => $signature, "name" => $files["name"], "path" => $files["path"],"cdn"=>1);
            $fileParamObj = AttachmentModel::create($fileParam);
            $fileParamObj->created = SqlFunction::NOW();
            $fileParamObj->save();
            $attachParam = array("object_id" => $object_id, "file_id" => $fileParamObj->id, "type" => $type, "inline" => 0, "staff_id" => 0, "user_id" => $user_id,"ticket_id"=>$ticket_id);

            $attachParamObj = Attachment::create($attachParam);
            $attachParamObj->created = SqlFunction::NOW();
            $attachParamObj->save();
        }
        }
        if(!$output){
            return $this->exerr(500, __("Unable to post message: unknown error"));
        }
        return $this->exerr(201, json_encode(array('status' => "success")));
        
    }

   function tickets(){
         if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
             return $this->exerr(401, __('API key not authorized'));

        $parms = $_GET;
        $result =TicketApiController::getTicketList($parms);
        $count = $result['count'];
        $total_count = $result['total_count'];
        $result = TicketApiController::getTicketdetail($result['ticket_list']);
        foreach ($result as $v) {
            
            $v['issue_type'] = $v['dept_name'];
            $v['count'] = $count;
            $v['total_count']=$total_count;
            
            $topic = Topic::lookup($v['topic_id']);
            $helptopic = array();
            if ($topic) {
                $helptopic = explode('/', $topic->getFullName());
            }
            $v['helptopic']=$helptopic;
            $v['issue'] = $helptopic[0];
            $v['sub_issue'] = end($helptopic);
            $output[] = $v;
        }

       $this->response(200, json_encode($output));
   }


  function getTicket($number){

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
             return $this->exerr(401, __('API key not authorized'));
        $ticket_id = Ticket::getIdByNumber($number);
        $ticket = Ticket::lookup($ticket_id);
        $ticket->ht['order_id'] = $ticket->getEntities('order');
        $ticket->ht['product_id'] = $ticket->getEntities('product');
        $ticket->ht['manifest_id'] = $ticket->getEntities('manifest');
        $ticket->ht['batch_id'] = $ticket->getEntities('batch');
        $ticket->ht['subject'] = $ticket->getSubject();
        $thread_id=$ticket->getThread()->ht['id'];
        $sql = "Select body from mst_thread_entry where thread_id=$thread_id and type='M' order by id asc limit 1 ";
        $res=db_query($sql);
        $row=db_fetch_row($res);
        $attachments=TicketApiController::getAttachments($ticket_id); 
        $ticket->ht['attachment_link'] = $attachments;
        $ticket->ht['ticket_detail'] = $row[0];
        $ticket->ht['staff'] = $ticket->getAssignee()->name;
        $ticket->ht['status'] = $ticket->getStatus()->ht['name'];
        $ticket->ht['issue_type'] = $ticket->getDeptName();
        $ticket->ht['sla'] = $ticket->getSLA()->ht['grace_period'];
        $helptopic = explode('/', $ticket->getHelpTopic());
        $ticket->ht['helptopic']=$helptopic;
        $ticket->ht['issue'] = $helptopic[0];
        $ticket->ht['sub_issue'] = end($helptopic);
        $ticket->ht['count'] = 1;
        $ticket->ht['subject']=$ticket->getHelpTopic();
        $output[] = $ticket->ht;
        $this->response(200, json_encode($output));
  }

  function getAttachments($ticket_id){
  global $cfg;
  $data=Attachment::objects()->filter(array("ticket_id" => $ticket_id,"type__notin" => array('H')))->values('file__path','file__name')->all();
  $links=array();
  foreach($data as $val){
  array_push($links,'<a href="'.$cfg->config["merchant_cdn_path"]["value"] . $val["file__path"].'" style="word-wrap: break-word;width: 200px;"><b>'.$val["file__name"].'</b> </a>');
  }
 return $links; 
  }
  
   function getTicketList($parms){
       if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
             return $this->exerr(401, __('API key not authorized'));

        $mid = $parms['mid'];        
        $email = $parms['email'];
        $date = $parms['date'];
        $start_date = $parms['start_date'];
        $end_date = $parms['end_date'];
        $order_id = $parms['order_id'];
        $product_id = $parms['product_id'];
        $manifest_id = $parms['manifest_id'];
        $batch_id = $parms['batch_id'];
        $parent_id = $parms['parent_id'];
        $status = $parms['status'];
        $page = $parms['page'];
        $limit = $parms['limit'];	
        $number=$parms['number'];
        if($mid){
        $qwhere .=" AND TD.merchant_id=$mid ";
        }
       
       if($number){
        $ticket_id = Ticket::getIdByNumber($number);
        $qwhere .= " AND T.ticket_id=$ticket_id ";
       } 
       if ($email) {

            $sql = "select user_id from mst_user_email where address='$email' limit 1";
            $res = db_query($sql);
            list($user_id) = db_fetch_row($res);
            $user_id = ($user_id) ? $user_id : 0;
            $qwhere .= " AND T.user_id=$user_id";
        }

        if ($date) {
            $qwhere .= " AND DATE(T.created)='$date'";
        }
        if ($start_date && $end_date) {
            $qwhere .= " AND DATE(T.created) between '$start_date' and '$end_date'";
        }

       if ($status) {
            $status_arr=  explode(",", $status);
            $map = array('Open'=>6, 'New'=>1, 'Solved'=>7,'Closed'=>2,'Reopen'=>9); 

      $status_id=array();
            foreach($status_arr as $val){
                $status_id[]=$map[$val];
               
            }
            $status_id=  implode(",", $status_id);
            $qwhere .= " AND T.status_id in ($status_id) ";
        }
 
         if ($order_id && $mid) {

            $qwhere .= " AND (TD.order_ids like '$order_id,%' or TD.order_ids like '%,$order_id' or TD.order_ids like '%,$order_id,%' or  TD.order_ids='$order_id') ";
        }

         if ($manifest_id && $mid) {

            $qwhere .= " AND (TD.manifest_ids like '$manifest_id,%' or TD.manifest_ids like '%,$manifest_id' or TD.manifest_ids like '%,$manifest_id,%' or  TD.manifest_ids='$manifest_id') ";
        }
         if ($product_id && $mid) {

            $qwhere .= " AND (TD.product_ids like '$product_id,%' or TD.product_ids like '%,$product_id' or TD.product_ids like '%,$product_id,%' or  TD.product_ids='$product_id') ";
        }
         if ($batch_id && $mid) {

            $qwhere .= " AND (TD.batch_ids like '$batch_id,%' or TD.batch_ids like '%,$batch_id' or TD.batch_ids like '%,$batch_id,%' or  TD.batch_ids='$batch_id') ";
        }



        $result = array();
        if ($parent_id) {
            $res = TicketApiController::TopicListHelper($parent_id, $result);

            $ids = implode(",", $result[0]);
            $qwhere .= " AND T.topic_id in ($ids) ";
        }
//end filter

         if ($limit && $page) {
                $l = $limit * ($page - 1);
                $limit_string = " limit $l,$limit";
         }

       $sql="SELECT T.ticket_id from mst_ticket T inner join mst_ticket__data TD on T.ticket_id=TD.ticket_id where 1=1 ". $qwhere ." order by T.created desc ". $limit_string;;
       $res = db_query($sql);
            $list = array();
            while ($row = db_fetch_row($res)) {
                $list[] = $row[0];
            }

       $sql = "SELECT count(T.ticket_id) from mst_ticket T inner join mst_ticket__data TD on T.ticket_id=TD.ticket_id  WHERE 1=1 " . $qwhere;
            $res = db_query($sql);
            list($count) = db_fetch_row($res);

       if ($mid) {
            $sql = "SELECT count(T.ticket_id),T.status_id from mst_ticket T inner join mst_ticket__data TD on T.ticket_id=TD.ticket_id WHERE 1=1 and TD.merchant_id=$mid group by T.status_id";
            $res = db_query($sql);
            $total_count = array();
            while (list($c, $s) = db_fetch_row($res)) {
                $map = array(6 => 'Open', 1 => 'New', 7 => 'Solved', 2 => 'Closed',9 => 'Reopen');
                $total_count[$map[$s]] = $c;
            }
        }

         return array('ticket_list' => $list,'count'=>$count,'total_count'=>$total_count);
       }

      static function TopicListHelper($ids, &$result) {
        $sql = "SELECT topic_id,topic_pid from " . TOPIC_TABLE . " WHERE topic_pid in ($ids)  AND isactive=1 order by topic asc";

        $res = db_query($sql);
        $topics = array();

        while (list($id, $pid) = db_fetch_row($res)) {

            if ($id) {
                array_push($topics, $id);
            } else {
                array_push($result, $pid);
            }
        }
        $string = implode(",", $topics);

        if ($string == "") {
            $top = explode(",", $ids);
            array_push($result, $top);
            return true;
        }
        TicketApiController::TopicListHelper($string, $result);
    }

      function getTicketdetail($ticket_list) {
        $string = implode(",", $ticket_list);

        $sql = "select T.*,D.name,S.name as status,concat(St.firstname,' ', St.lastname)as staff,TD.order_ids,TD.product_ids,TD.manifest_ids,
TD.batch_ids ,TD.subject from " . TICKET_TABLE . " T left join 
         " . STAFF_TABLE . " St on T.staff_id=St.staff_id 
          left join 
          " . DEPT_TABLE . " D on D.id=T.dept_id 
         left join " . TICKET_STATUS_TABLE . " S on S.id=T.status_id  left join mst_ticket__data TD on T.ticket_id=TD.ticket_id WHERE T.ticket_id in (" . $string . ") order by T.created desc ";
     
   if (!($res = db_query($sql)) || !db_num_rows($res))
            return false;
        $output = array();
        while ($row = db_fetch_array($res)) {
	    
            $output[] = $row;
        }
        return $output;
    }

function getThreads() {
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $parms=$_GET;
        $ticket_id = Ticket::getIdByNumber($parms['ticket_number']);
        $ticket = Ticket::lookup($ticket_id);
       if($key->getNotes()=='MCR'){
       $type=array('M', 'R', 'N');
       }else{
       $type=array('M', 'R');
       }
       $thread_obj=$ticket->getThread();
       $entries = $thread_obj->getEntries();
       if ($type && is_array($type))
            $entries->filter(array('type__in' => $type));
       
        $output = array();
        foreach ($entries as $entry) {
         TicketApiController::threadAttachment($entry->id); 
         $th['created']=$entry->created;
          $th['poster']=$entry->poster;
          $th['title']=$entry->title;
          $th['body']['body']=$entry->body;
          $th['user']=$entry->user->id;
          $th['staff']=$entry->staff->staff_id;
          $th['format']=$entry->format;
	  $th['attachment']=TicketApiController::threadAttachment($entry->id);
         $output[]= $th;
        }
        $this->response(200, json_encode($output));
    }

function threadAttachment($object_id){
$attach=Attachment::objects()->filter(array("object_id" =>$object_id, "type" => 'H'))->values('file__name','file__path')->all();
$output=array();
if(!empty($attach)){
foreach($attach as $val){
array_push($output,array('name'=>$val['file__name'],'path'=>$val['file__path']));
}
}
return $output;
}
 function postNote($format){
   //Array ( [__CSRFToken__] => 7d722672040d68fac0a97938a42c3da06f8ab19a [id] => 123 [locktime] => 180 [a] => postnote [lockCode] => ogRB5uISQc [title] => zxjcnxz [note] => test [note_status_id] => 6 [draft_id] => [cannedattachments] => Array ( ) )
       
        global $thisstaff;

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));
        $errors = array();
        $parms = $this->getRequest($format);

        $ticket_id = Ticket::getIdByNumber($parms['ticket_number']);
        $vars['id'] = $ticket_id;
        $vars['title'] = $parms['title'];
        $vars['note'] = $parms['note'];
        $vars['email'] = $parms['staff_email'];
       
        $staffData = Staff::objects()->filter(array("email" => $vars['email']))->values("staff_id")->all(); 
        $staffObj=Staff::lookup($staffData[0]['staff_id']);
        if (trim($vars['title']) == '') {

            return $this->exerr(500, json_encode(__("Unable to post internal note:Please Enter Title")));
        }

        $ticket = Ticket::lookup($ticket_id);
        if ($ticket && is_object($ticket)) {
            //everything right
        } else {
            return $this->exerr(500, json_encode(__("Unable to post internal note: unknown error")));
        }

        if ($ticket->postNote($vars, $errors,$staffObj)) {
            $this->response(201, $ticket->getId());
        } else {
            return $this->exerr(500, __("Unable to post internal note: unknown error"));
        }
        }

       function getTopics(){
         global $APIKey;  

         if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
             return $this->exerr(401, __('API key not authorized'));
 
        $APIKey=$this->requireApiKey();
        $parms=$_GET;
        $topics = Topic::getChildIssues($parms['parent_id']);
        if($topics==null){
        $this->response(404, '{}');
        }
        $this->response(200, json_encode($topics));
       }

      function getMandatoryInfo($format){
        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
             return $this->exerr(401, __('API key not authorized'));
    
        $parms = $_GET;
        $topic = Topic::lookup($parms['topicId']);
        if($topic==null){
        $this->response(404, '{}');
        }
        $info = $topic->getCapabilities();
        $info=json_decode($info);
        $this->response(200, json_encode($info));
    }
}
//Local email piping controller - no API key required!
class PipeApiController extends TicketApiController {

    //Overwrite grandparent's (ApiController) response method.
    function response($code, $resp) {

        //Use postfix exit codes - instead of HTTP
        switch ($code) {
            case 201: //Success
                $exitcode = 0;
                break;
            case 400:
                $exitcode = 66;
                break;
            case 401: /* permission denied */
            case 403:
                $exitcode = 77;
                break;
            case 415:
            case 416:
            case 417:
            case 501:
                $exitcode = 65;
                break;
            case 503:
                $exitcode = 69;
                break;
            case 500: //Server error.
            default: //Temp (unknown) failure - retry
                $exitcode = 75;
        }

        //echo "$code ($exitcode):$resp";
        //We're simply exiting - MTA will take care of the rest based on exit code!
        exit($exitcode);
    }

    function process() {
        $pipe = new PipeApiController();
        if (($ticket = $pipe->processEmail()))
            return $pipe->response(201, $ticket->getNumber());

        return $pipe->exerr(416, __('Request failed - retry again!'));
    }

}

?>
