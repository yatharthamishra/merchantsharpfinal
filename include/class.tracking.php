<?php

/**
* This class is used to track movement of users
*/
class TrackingModel extends VerySimpleModel {
	static $meta = array(
        'table' => TRACKING_TABLE,
        'pk' => array('id')
    );
}

class Tracking extends TrackingModel{
	function getId() {
        return $this->id;
    }

    function getStaffId() {
        return $this->staff_id;
    }
    function getLastTimstamp() {
        return $this->last_timestamp;
    }
    function getNewTimestamp() {
        return $this->new_timestamp;
    }
    function getTicketId() {
        return $this->ticket_id;
    }
    function getTaskId() {
        return $this->task_id;
    }


	function track($data,$flag=0)
	{
        $last_timestamp = date("Y-m-d H:i:s",$data['last_timestamp']);
        $var = array('staff_id' => $data['staff_id'],
                        'last_timestamp' => $last_timestamp,
                        'new_timestamp' => $data['timestamp'],
                        'event_name' => $data['event'],
                        'ticket_id' => $data['ticket_id'],
                        'task_id' => $data['task_id'] );
        $trackingModelObj = TrackingModel::create($var);
        $trackingModelObj->save();
	}
}
