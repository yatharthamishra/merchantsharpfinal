<?php
/**
*  Model class for data reprocessing script
*  
*/
include_once(INCLUDE_DIR.'class.orderStatusCron.php');
define("DATA_REPROCESSING_LOG_PATH",LOGS_DIR.'dataReprocessing%date.txt');
define("DATA_REPROCESSING_REVERT_PATH", LOGS_DIR.'revert%date.txt');
define('TICKETS_PATH', ROOT_PATH.'scp/tickets.php?id=');

class DataReprocessingModel
{
	private $logFilePointer;
	private $logPath;
	function __construct(){
		$this->logPath = DATA_REPROCESSING_LOG_PATH;
	}

	public function main($data){

		if(empty($data))
			return 0;

		echo "INPUT:<pre>";
		print_r($data);

		$this->logFilePointer=fopen($this->logPath, "w");
		$this->_logEntityWiseData('--INPUT--', $data);
		echo "START<br>";
		$inputData = array('ticketIds' => $data['ticketIds'],
							'fromDate' => $data['fromDate'],
							'toDate' => $data['toDate']
							);
		$outputData = array();
		if(in_array('all', $data['operation'])){
			$operations[] = 'openTickets';
			$operations[] = 'setNegativeTicketIdForTasks';
			$operations[] = 'openLastTasks';
			$operations[] = 'reprocessTickets';
		}
		else
			$operations = $data['operation'];
		foreach ($operations as $operation) {
			$this->$operation($inputData,$outputData);
		}
		// $this->openTickets($data['ticketIds']);
		// $taskIds = $this->setNegativeTicketIdForTasks($data);
		// $this->openLastTasks($data['ticketIds']);
		// $this->reprocessTickets($data);

		$msg = "FINISHED";
		$this->_logEntityWiseData('--'.$msg.'--', '.............');
		echo $msg;
		fclose($this->logFilePointer);

		$this->logPath = DATA_REPROCESSING_REVERT_PATH;
		$this->logFilePointer=fopen($this->logPath, "w");
		$this->queriesToRevertBack($data['ticketIds'],$outputData['taskIds']);
		return 1;
	}
	/*
	Verify user Password
	*/
	public function verifyPassword($password){
		if(!is_numeric($password))
			$password = strtotime($password);
		$Ldate = strtotime('-5 minute');
		$Udate = strtotime('+5 minute');
		if($password>=$Ldate && $password<=$Udate){
			$code=1;
			$msg = "Success";
		}
		else{
			$code = 0;
			$msg = "Enter a valid password";
		}
		return array('code' => $code,
					'message'=> $msg
					);
	}

	/*
	Open tickets i.e set status_id=6
	*/
	private function openTickets($inputData,&$outputData){
		$ticketIds = $inputData['ticketIds'];
		$msg = "Changing status of tickets to open.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$ticketObj = Ticket::objects()->filter(array('ticket_id__in' => $ticketIds ));
		foreach ($ticketObj as $ticket) {
			if($ticket->status_id!=6){
				$ticket->status_id=6;
				$ticket->save();
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - opened<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Opened');
			}
			else{
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - Already open<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Already Open');
			}
		}
		$msg = "Status of tickets changed to open.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
	}

	/*
	close tickets i.e set status_id=2
	*/
	private function closeTickets($inputData,&$outputData){
		$ticketIds = $inputData['ticketIds'];
		$msg = "Changing status of tickets to close.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$ticketObj = Ticket::objects()->filter(array('ticket_id__in' => $ticketIds ));
		foreach ($ticketObj as $ticket) {
			if($ticket->status_id!=6){
				$ticket->status_id=6;
				$ticket->save();
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - closed<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Closed');
			}
			else{
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - Already close<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Already close');
			}
		}
		$msg = "Status of tickets changed to close.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
	}

	/*
	set object_id of the tasks created between time range to be -1* object_id
	*/
	private function setNegativeTicketIdForTasks($data,&$outputData){
		$msg = "setting object_id of the tasks created between time range to be -1* object_id.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$fromDate = date('Y-m-d H:i:s',strtotime($data['fromDate']));
		$toDate = date('Y-m-d H:i:s',strtotime($data['toDate']));
		$taskObj = Task::objects()->filter(array('created__gte' => $fromDate, 'created__lte' => $toDate, 'object_id__in' => $data['ticketIds']));
		foreach ($taskObj as $task) {
			if($task->object_id>0){
				$task->object_id *= -1;
				$task->save();
				$taskIds[] = $task->id;
				echo "task id=".$task->id.", object_id=".$task->object_id."<br>";
				$this->_logEntityWiseData('-task id='.$task->id, 'object id='.$task->object_id);
			}
			else{
				echo "task id=".$task->id.", object_id=".$task->object_id."Object id was already negative<br>";
				$this->_logEntityWiseData('-task id='.$task->id, 'object id='.$task->object_id.'---object id was already negative');
			}
		}
		$msg = "Setting of negative object_ids done.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
		$outputData['taskIds'] = $taskIds;
		
	}
	/*
	open last tasks of these tickets closed before the start time of time range
	*/
	private function openLastTasks($inputData,&$outputData){
		$ticketIds = $inputData['ticketIds'];
		$msg = "Opening last task of these tickets.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$taskObj = Task::objects()->filter(array('object_id__in' => $ticketIds, 'created__lte'=> $inputData['fromDate'], 'data__taskSource__neq'=>'56,Manual' ));
		foreach ($taskObj as $task) {
			if(($T[$task->object_id] && ($task->id)>$T[$task->object_id]['max_task_id']) || !$T[$task->object_id])
				$T[$task->object_id]['max_task_id'] = $task->id;
		}
		foreach ($T as $ticket_id => $value) {
			$taskIds[] = $value['max_task_id'];
		}
		$taskObj = Task::objects()->filter(array('id__in' => $taskIds));
		foreach ($taskObj as $task) {
			foreach ($task->data as $DataObj) {
				$currentStatus = $DataObj->taskStatus;
				$currentResponse = $DataObj->taskResponse;
				$DataObj->taskStatus = '45,New';
				$DataObj->taskResponse = NULL;
				$DataObj->save();
			}
			$task->flags = 1;
			$task->save();
			echo "task id=".$task->id.", object_id=".$task->object_id."---Task opened Task Status changed from ".$currentStatus." to 45,New and task response changed from ".$currentResponse." to NULL<br>";
			$this->_logEntityWiseData('-task id='.$task->id, 'Task Opened. Task Status changed from '.$currentStatus.' to 45,New and task response changed from '.$currentResponse.' to NULL');
		}
		$msg = "Last tasks opened of all the tickets.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
	}

	/*
	get order_id and return_id of tickets
	*/
	function getOrdRtnId($ticketIds){
		$ticketOdataObj = TicketOData::objects()->filter(array('ticket_id__in' => $ticketIds));
		foreach ($ticketOdataObj as $ticket) {
			$order_id[] = $ticket->order_id;
			if($ticket->retrn_id > 0)
				$order_id[] = $ticket->retrn_id;
		}
		return $order_id;
	}
	/*
	find order_id and return_id of these tickets and reprocess them.
	*/
	private function reprocessTickets($data,&$outputData){
		$msg = "Reprocessing Tickets.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$order_id = $this->getOrdRtnId($data['ticketIds']);
		$cronObj = OrderStatusCron::objects()->filter(array('order_id__in' => $order_id, 'update_timestamp__gte' => $data['fromDate'], 'update_timestamp__lte' => $data['toDate'] ));
		foreach ($cronObj as $order) {
			if($order->processed==1){
				$order->processed = 0;
				$order->save();
				echo "order id=".$order->order_id.", processed=0 done<br>";
				$this->_logEntityWiseData("id=".$order->id.",order id=".$order->order_id,"processed=0 done");
			}
			else{
				echo "order id=".$order->order_id.", processed=0 It was already unprocessed<br>";
				$this->_logEntityWiseData("id=".$order->id.",order id=".$order->order_id,"processed=0 It was already unprocessed");
			}
		}
		$msg = "Tickets added in queue for reprocessing.<br>";
		$this->_logEntityWiseData('Reprocessing Started', $msg);
		echo $msg;
	}

	/*
	function to revert back to previous state
	*/
	private function queriesToRevertBack($ticketIds,$taskIds){
		if($ticketIds){
			$sql[] = "update mst_ticket set status_id=2 where ticket_id IN ".implode(',', $ticketIds);
		}
		if($taskIds)
			$sql[] = "update mst_task set object_id=-1*object_id where id IN ".implode(',', $taskIds);
		$this->_logEntityWiseData('--Queries To revert the chages made by data reprocessing script--', $sql);
	}

	/*
	function to log in file
	*/
	private function _logEntityWiseData($heading, $data) {
        fwrite($this->logFilePointer, $heading . " : \n");
        if(!is_array($data)){
            fwrite($this->logFilePointer, $data . "\n");
        }
        if (is_null($data) || empty($data)) {
            return;
        }
        foreach ($data as $entityName => $entityTextData) {
            $entityText=$entityTextData;
            if (is_array($entityTextData)) {
                if (count($entityTextData) > 0) {
                    $entityText = implode("|", $entityTextData);
                }
            }
            fwrite($this->logFilePointer, $entityName . " : " );
            fwrite($this->logFilePointer, $entityText . "\n");
        }
    }
}


/**
* Class for ticket related functions
*/
class ModelTicket extends LogModelClass
{
	
	//private $Obj;
	function __construct()
	{
		parent::__construct();
	}

	/*
	get ticket Ids
	*/
	function getTicketIds($var,$param){
		if($param=='order_id'){
			$ticketObj = TicketOData::objects()->filter(array('order_id__in' => $var ))->values('ticket_id')->all();
			$ticketCheck = 0;
		}
		else if ($param=='ticket_num') {
			$ticketObj = Ticket::objects()->filter(array('number__in' => $var ))->values('ticket_id')->all();
			$ticketCheck = 0;
		}
		else if ($param=='ticket_id') {
			return $var;
		}
		foreach ($ticketObj as $ticket) {
			$ticketIds[] = $ticket['ticket_id'];
		}
		return $ticketIds;

	}


	/*
	get order_id and return_id of tickets
	*/
	function getOrdRtnId($ticketIds){
		$ticketOdataObj = TicketOData::objects()->filter(array('ticket_id__in' => $ticketIds));
		foreach ($ticketOdataObj as $ticket) {
			$order_id[] = $ticket->order_id;
			if($ticket->retrn_id > 0)
				$order_id[] = $ticket->retrn_id;
		}
		return $order_id;
	}

	/*
	Open tickets i.e set status_id=6
	*/
	public function openTickets($inputData,&$outputData){
		global $cfg;
		$chunkSize = $cfg->config['stopScriptChunkSize']["value"];
		if($inputData['allTickets']){
			$allTickets = 1;
			$filterForQuery = array('created__lte' => $inputData['toDate'], 
									'created__gte' => $inputData['fromDate'] 
									);
		}
		else{
			$ticketIds = $inputData['ticketIds'];
			$filterForQuery = array('ticket_id__in' => $ticketIds, 
									'created__lte' => $inputData['toDate'], 
									'created__gte' => $inputData['fromDate'] );
		}
		$ticketIds = $inputData['ticketIds'];
		$msg = "Changing status of tickets to open.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$ticketObj = Ticket::objects()->filter($filterForQuery);
		$count=0;
		foreach ($ticketObj as $ticket) {
			if($count%$chunkSize==0 && ModelAuth::stopScript()){
				echo "\n Script interrupted";
				break;
			}
			if($allTickets){
				$ticketIds .= ",".$ticket->ticket_id; 
			}
			if($ticket->status_id!=6){
				$ticket->status_id=6;
				$ticket->save();
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - opened<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Opened');
			}
			else{
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - Already open<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Already Open');
			}
			$count++;
		}
		$msg = "Status of tickets changed to open.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
	}

	/*
	close tickets i.e set status_id=6
	*/
	public function closeTickets($inputData,&$outputData){
		global $cfg;
		$chunkSize = $cfg->config['stopScriptChunkSize']["value"];
		if($inputData['allTickets']){
			$allTickets = 1;
			$filterForQuery = array('created__lte' => $inputData['toDate'], 
									'created__gte' => $inputData['fromDate'] 
									);
		}
		else{
			$ticketIds = $inputData['ticketIds'];
			$filterForQuery = array('ticket_id__in' => $ticketIds, 
									'created__lte' => $inputData['toDate'], 
									'created__gte' => $inputData['fromDate'] );
		}
		$ticketIds = $inputData['ticketIds'];
		$msg = "Changing status of tickets to close.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$ticketObj = Ticket::objects()->filter($filterForQuery);
		$count=0;
		foreach ($ticketObj as $ticket) {
			if($count%$chunkSize==0 && ModelAuth::stopScript()){
				echo "\n Script interrupted";
				break;
			}
			if($ticket->status_id!=2){
				$ticket->setStatus(2,'Ticket closed by Script');
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - closed<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Closed');
			}
			else{
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - Already close<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'status- Already close');
			}
			$count++;
		}

		$msg = "Status of tickets changed to close.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
	}

	function changeToTriageAgain($inputData){
		global $cfg;
		$chunkSize = $cfg->config['stopScriptChunkSize']["value"];
		if($inputData['allTickets']){
			$allTickets = 1;
			$filterForQuery = array('created__lte' => $inputData['toDate'], 
									'created__gte' => $inputData['fromDate'] 
									);
		}
		else{
			$ticketIds = $inputData['ticketIds'];
			$filterForQuery = array('ticket_id__in' => $ticketIds, 
									'created__lte' => $inputData['toDate'], 
									'created__gte' => $inputData['fromDate'] );
		}
		$ticketObj = Ticket::objects()->filter($filterForQuery);
		$count=0;
		foreach ($ticketObj as $ticket) {
			if($count%$chunkSize==0 && ModelAuth::stopScript()){
				echo "\n Script interrupted";
				break;
			}
			if($ticket->triage->triage!=3){
				$ticket->triage->triage = 3;
				$ticket->triage->triage_again = date('Y-m-d H:i:s');
				$ticket->triage->save();
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> - Triage Status- Triage Again<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'Triage Status- Triage again');
			}
			else{
				echo "Ticket id=<a href='".TICKETS_PATH.$ticket->ticket_id."'>".$ticket->ticket_id."</a> Triage Status- Already Triage again<br>";
				$this->_logEntityWiseData('-Ticket id='.$ticket->ticket_id.'-', 'Triage Status- Already Triage again');
			}
			$count++;
		}
	}

}

/**
* class for task related functions
*/
class ModelTask extends LogModelClass
{
	private $Obj;
	function __construct()
	{
		parent::__construct();
	}

	public function setNegativeTicketIdForTasks($data,&$outputData){
		global $cfg;
		$chunkSize = $cfg->config['stopScriptChunkSize']["value"];
		$msg = "setting object_id of the tasks created between time range to be -1* object_id.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$fromDate = date('Y-m-d H:i:s',strtotime($data['fromDate']));
		$toDate = date('Y-m-d H:i:s',strtotime($data['toDate']));
		$taskObj = Task::objects()->filter(array('created__gte' => $fromDate, 'created__lte' => $toDate, 'object_id__in' => $data['ticketIds']));
		$count=0;
		foreach ($taskObj as $task) {
			if($count%$chunkSize==0 && ModelAuth::stopScript()){
				echo "\n Script interrupted";
				break;
			}
			if($task->object_id>0){
				$task->object_id *= -1;
				$task->save();
				$taskIds[] = $task->id;
				echo "task id=".$task->id.", object_id=".$task->object_id."<br>";
				$this->_logEntityWiseData('-task id='.$task->id, 'object id='.$task->object_id);
			}
			else{
				echo "task id=".$task->id.", object_id=".$task->object_id."Object id was already negative<br>";
				$this->_logEntityWiseData('-task id='.$task->id, 'object id='.$task->object_id.'---object id was already negative');
			}
			$count++;
		}
		$msg = "Setting of negative object_ids done.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
		$outputData['taskIds'] = $taskIds;
		
	}

	/*
	open last tasks of these tickets closed before the start time of time range
	*/
	public function openLastTasks($inputData,&$outputData,$type){
		global $cfg;
		$chunkSize = $cfg->config['stopScriptChunkSize']["value"];
		$ticketIds = $inputData['ticketIds'];
		if($type=="overall"){
			$filterForQuery = array('object_id__in' => $ticketIds, 
									'data__taskSource__neq'=>'56,Manual' );
		}
		else{
			$filterForQuery = array('object_id__in' => $ticketIds, 
									'created__lte'=> $inputData['fromDate'], 
									'data__taskSource__neq'=>'56,Manual' );
		}
		$msg = "Opening last task of these tickets.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$taskObj = Task::objects()->filter($filterForQuery);
		foreach ($taskObj as $task) {
			if(($T[$task->object_id] && ($task->id)>$T[$task->object_id]['max_task_id']) || !$T[$task->object_id])
				$T[$task->object_id]['max_task_id'] = $task->id;
		}
		foreach ($T as $ticket_id => $value) {
			$taskIds[] = $value['max_task_id'];
		}
		$taskObj = Task::objects()->filter(array('id__in' => $taskIds));
		$count=0;
		foreach ($taskObj as $task) {
			if($count%$chunkSize==0 && ModelAuth::stopScript()){
				echo "\n Script interrupted";
				break;
			}
			foreach ($task->data as $DataObj) {
				$currentStatus = $DataObj->taskStatus;
				$currentResponse = $DataObj->taskResponse;
				$DataObj->taskStatus = '45,New';
				$DataObj->taskResponse = NULL;
				$DataObj->save();
			}
			$task->flags = 1;
			$task->save();
			echo "task id=".$task->id.", object_id=".$task->object_id."---Task opened Task Status changed from ".$currentStatus." to 45,New and task response changed from ".$currentResponse." to NULL<br>";
			$this->_logEntityWiseData('-task id='.$task->id, 'Task Opened. Task Status changed from '.$currentStatus.' to 45,New and task response changed from '.$currentResponse.' to NULL');
			$count++;
		}
		$msg = "Last tasks opened of all the tickets.<br>";
		$this->_logEntityWiseData($msg, 'END');
		echo $msg;
	}
}

/**
* 
*/
class ModelOrder extends LogModelClass
{
	
	function __construct()
	{
		parent::__construct();
	}

	/*
	find order_id and return_id of these tickets and reprocess them.
	*/
	public function reprocessTickets($data,&$outputData){
		global $cfg;
		$chunkSize = $cfg->config['stopScriptChunkSize']["value"];
		$msg = "Reprocessing Tickets.<br>";
		$this->_logEntityWiseData('--'.$msg.'--', 'START');
		echo $msg;
		$order_id = ModelTicket::getOrdRtnId($data['ticketIds']);
		$cronObj = OrderStatusCron::objects()->filter(array('order_id__in' => $order_id, 'update_timestamp__gte' => $data['fromDate'], 'update_timestamp__lte' => $data['toDate'] ));
		$count=0;
		foreach ($cronObj as $order) {
			if($count%$chunkSize==0 && ModelAuth::stopScript()){
				echo "\n Script interrupted";
				break;
			}
			if($order->processed==1){
				$order->processed = 0;
				$order->save();
				echo "order id=".$order->order_id.", processed=0 done<br>";
				$this->_logEntityWiseData("id=".$order->id.",order id=".$order->order_id,"processed=0 done");
			}
			else{
				echo "order id=".$order->order_id.", processed=0 It was already unprocessed<br>";
				$this->_logEntityWiseData("id=".$order->id.",order id=".$order->order_id,"processed=0 It was already unprocessed");
			}
			$count++;
		}
		$msg = "Tickets added in queue for reprocessing.<br>";
		$this->_logEntityWiseData('Reprocessing Started', $msg);
		echo $msg;
	}
}

class ModelAuth
{
	/*
	Verify user Password
	*/
	public function verifyPassword($password){
		if(!is_numeric($password))
			$password = strtotime($password);
		$Ldate = strtotime('-5 minute');
		$Udate = strtotime('+5 minute');
		if($password>=$Ldate && $password<=$Udate){
			$code=1;
			$msg = "Success";
		}
		else{
			$code = 0;
			$msg = "Enter a valid password";
		}
		return array('code' => $code,
					'message'=> $msg
					);
	}
	public function stopScript(){
		$sql = "SELECT `value` from mst_config where `key`='stopDataReprocessing'";
		$result = db_query($sql);
		if($result){
			while ($row = db_fetch_array($result)) {
				$value = $row['value'];
				return $value;
			}
		}
		return 0;
	}
}


class LogModelClass
{
	static private $filePointer;
	private $logFilePointer;
	private $logPath;
	function __construct(){
		if(!self::$filePointer){
			$this->logPath = str_replace('%date', date('Y-m-d'), DATA_REPROCESSING_LOG_PATH);;
			self::$filePointer=fopen($this->logPath, "w");
		}
		$this->logFilePointer = self::$filePointer;
	}

	private function changeLogFile($filename){
		$this->logPath = $filename;
		$this->logFilePointer=fopen($this->logPath, "w");
	}
	/*
	function to log in file
	*/
	public function _logEntityWiseData($heading, $data) {
        fwrite($this->logFilePointer, $heading . " : \n");
        if(!is_array($data)){
            fwrite($this->logFilePointer, $data . "\n");
        }
        if (is_null($data) || empty($data)) {
            return;
        }
        foreach ($data as $entityName => $entityTextData) {
            $entityText=$entityTextData;
            if (is_array($entityTextData)) {
                if (count($entityTextData) > 0) {
                    $entityText = implode("|", $entityTextData);
                }
            }
            fwrite($this->logFilePointer, $entityName . " : " );
            fwrite($this->logFilePointer, $entityText . "\n");
        }
    }

    /*
	function to revert back to previous state
	*/
	public function queriesToRevertBack($ticketIds,$taskIds){
		$this->changeLogFile(str_replace('%date', date('Y-m-d'), DATA_REPROCESSING_REVERT_PATH));
		if($ticketIds){
			$sql[] = "update mst_ticket set status_id=2 where ticket_id IN ".implode(',', $ticketIds);
		}
		if($taskIds)
			$sql[] = "update mst_task set object_id=-1*object_id where id IN ".implode(',', $taskIds);
		$this->_logEntityWiseData('--Queries To revert the chages made by data reprocessing script--', $sql);
	}

}

