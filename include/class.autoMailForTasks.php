<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author shopclues
 */
class TaskImageMailer {

//put your code here

    private $config;

    function __construct() {
        global $cfg;
        $this->config = $cfg;
    }

    public function sendAutoMailer() {
        global $ost;
        //$templateName=$this->config->config['image_auto_task_mail_template']['value'];
	$templateDetails=UtilityFunctions::getAutoMailerTemplateDetails('More info required mail','instant');
	$templateName=$templateDetails['code_name'];
	$imagetemplate = new EmailTemplate(0, null, $templateName);
        $autoMailerAndData = $this->_getDataForAutoMailer();
        if(empty($autoMailerAndData[0])){
            return;
        }
        $autoMailerData = $autoMailerAndData[0];
        $ticketIDs = $autoMailerAndData[1];
        $startProcessingTime = date('Y-m-d H:i:s');
        $tickets = $this->_getTickets($ticketIDs);

        $ticketObjectArray = [];
        foreach ($tickets as $ticket) {
            $ticketObj = new Ticket();
            $ticketObj = UtilityFunctions::initiatizeObjectFromArray($ticketObj, $ticket);
            $ticketObjectArray[$ticket['ticket_id']] = $ticketObj;
        }
        foreach ($autoMailerData as $ticketId => $mailData) {
            $ticketObj=$ticketObjectArray[$ticketId];
            $entityType = $ticketObj->getEntityType();
            $entityIds=[];
            $mailIds=[];
            foreach ($mailData as $mailForOneTicketData) {
                $entityIds[] = $mailForOneTicketData[$entityType . '_id'];
                $mailIds[] = $mailForOneTicketData['id'];
            }
            $this->startDataProcessing($mailIds);
            $ticketObj = $ticketObjectArray[$ticketId];
            $entity_ids = implode(',', $entityIds);
            $ticketObj->setEntities($entity_ids, $entityType);
            $msg = $ticketObj->replaceVars(
                    $imagetemplate->asArray(), array('message' => "sdf",
                'recipient' => $ticketObj->getOwner()
                    )
            );
            $email = $ost->getConfig()->getDefaultEmail();
            $email->send($ticketObj->getOwner(), Format::striptags($msg['subj']), $msg['body']);
            $autoPostNote['note']="Subject - ".$msg['subj'].$msg['body'];
            $ticketObj->postNote($autoPostNote);
            $this->finishDataProcessing($mailIds);
        }

    }

    private function startDataProcessing($ids) {
        $commaSeperatedIds = UtilityFunctions::getInClauseFromArray($ids);
        $sql = "update mst_seller_mail_queue set startProcessDateTime=NOW() where id in ($commaSeperatedIds) ";
        db_query($sql);
        return db_affected_rows();
        
    }

    private function finishDataProcessing($ids) {
        $commaSeperatedIds = UtilityFunctions::getInClauseFromArray($ids);
        $sql = "update mst_seller_mail_queue set endProcessDateTime=NOW(), isProcessed=1 where id in ($commaSeperatedIds) ";
        db_query($sql);
        return db_affected_rows();
    }

    private function _getDataForAutoMailer() {

        $bucketSize = $this->config->config['autoMailerBucketSize']['value'] ? $this->config->config['autoMailerBucketSize']['value'] : 100;
        $sql="SELECT 
                `ticket_id`,
                MAX(`addDateTime`) AS maxAddDateTime 
              FROM
                `mst_seller_mail_queue` 
              WHERE isProcessed = 0 
                AND isActive = 1 
              GROUP BY `ticket_id` 
              HAVING (
                  MAX(`addDateTime`) < DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                ) limit $bucketSize;";
        $rs=  db_query($sql);
        $ticket_ids=[];
        while ($row=  db_fetch_array($rs)){
            $ticket_ids[]=$row['ticket_id'];
        }
	if(count($ticket_ids)==0){
		return ;
	}
        $commaSeperatedTicketIds=UtilityFunctions::getInClauseFromArray($ticket_ids);
        $sql = "SELECT 
                * 
              FROM
                mst_seller_mail_queue ms 
              WHERE ticket_id in ($commaSeperatedTicketIds) and isProcessed = 0 
                AND isActive = 1 ";
        $rs = db_query($sql);
        $autoMailerData = [];
        $ticketIDs = [];
        while ($row = db_fetch_array($rs)) {
            $autoMailerData[$row['ticket_id']][] = $row;
            $ticketIDs[] = $row['ticket_id'];
        }
        return array($autoMailerData, $ticketIDs);
    }

    private function _getTickets($ticketIds) {
        $commaSeperatedTicketIds = UtilityFunctions::getInClauseFromArray($ticketIds);
        $sql = "select * from mst_ticket mt join mst_ticket__data mtd on mt.ticket_id=mtd.ticket_id where mt.ticket_id in ($commaSeperatedTicketIds)";
        $rs = db_query($sql);
        $ticketData = [];
        while ($row = db_fetch_array($rs)) {
            $ticketData[] = $row;
        }
        return $ticketData;
    }

}

?>
