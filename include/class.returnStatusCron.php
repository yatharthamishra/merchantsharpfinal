<?php

/*
 * Class handling oder Status table
 * @author - Akash
 */

class ReturnStatusCron extends VerySimpleModel {

    static $meta = array(
        'table' => ORDER_CRON_TABLE,
        'pk' => array('id')
    );

    public function preventMultipleCron($timeGap = 1) {
        $ordersUpdatedTime = ReturnStatusCron::objects()->filter(array("processed" => 1, "status_type" => "Return"))->values('update_timestamp')->order_by('-update_timestamp')->limit(1)->all();
        if ((time() - strtotime($ordersUpdatedTime[0]["update_timestamp"])) < $timeGap) {
            // Log Info for BulkJob Error
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D5"];
            $module = $logConfig["module"][$domain]["D5_M2"];
            $error = $logConfig["error"]["E4"];
            $level = $logConfig["level"]["INFO"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $level, $error, "title:Return Cron running", "log_location:class.bulkjob.php", "", "", "Time-" . $ordersUpdatedTime[0]["update_timestamp"]);
            // die("Another Return Cron Running");
        }
    }

    public function getOrders($max = 1) {
        $orders = ReturnStatusCron::objects()->filter(array("processed" => 0, "status_type__in" => array("Return", "RET_ACT", "RET_VALUE", "TAG")))->order_by('id')->limit($max);
        global $cfg, $ost;
        foreach ($orders as $order) {
            $returnIdArr = array();
            $to_status = "";
            if ($order->status_type == "TAG") {
                $rmaHistory = CscartRmaReturns::objects()->filter(array("order_id" => $order->order_id))->values("return_id")->all();
                foreach ($rmaHistory as $returnArr) {
                    $returnIdArr[] = $returnArr["return_id"];
                }
                if (count($returnIdArr) < 1) {
                    $order->processed = 1;
                    $order->save();
                }
            } else {
                $returnIdArr[] = $order->order_id;
            }
            if ($order->status_type == "RET_ACT" || $order->status_type == "RET_VALUE" || $order->status_type == "TAG") {
                $last_to_status = ReturnStatusCron::objects()->filter(array("status_type" => "Return", "order_id" => $returnIdArr[0], "id__lt" => $order->id))->order_by('-id')->values("to_status")->limit(1)->all();
                if ($last_to_status[0]["to_status"])
                    $to_status = $last_to_status[0]["to_status"];
                else {
                    $rmaHistory = CscartRmaReturns::objects()->filter(array("return_id" => $returnIdArr[0]))->values("status")->limit(1)->all();
                    $to_status = $rmaHistory[0]["status"];
                }
            } else {
                $to_status = $order->to_status;
            }
            $status_type = "Return";
            foreach ($returnIdArr as $returnId) {
                if(!$orderArr[$status_type][$returnId]["Details"] || ($orderArr[$status_type][$returnId]["Details"] && $order<$orderArr[$status_type][$returnId]["Object"]->transition_time)){
                        $orderArr[$status_type][$returnId]["Details"] = $to_status;
                        $orderArr[$status_type][$returnId]["Object"] = $order;
                }
            }
        }
        return $orderArr;
    }

    public function PerformOperation($ordersArr) {
        global $cfg;
        $taskStaticData = getCachedTaskFields();
        foreach ($ordersArr as $statusType => $orderStatus) {
            foreach ($orderStatus as $returnId => $status) {
                if (!$processedOrder[$returnId]) {
                    //Tickets
                    $findTicketArr = array("odata__retrn_id" => $returnId, "status__state" => 'open');
                    $ticketIdsArr = array();
                    $tickets = Ticket::objects()->filter($findTicketArr)->values("topic_id", "ticket_id", 'odata__order_id', 'odata__ord_clone_id')->all();
                    foreach ($tickets as $t) {
                        $ticketIdsArr[] = $t["ticket_id"];
                        if ($orderStatus[$returnId]["Object"]->order_id == $t["odata__ord_clone_id"])
                            $orderId = $t["odata__ord_clone_id"];
                        else
                            $orderId = $t["odata__order_id"];
                    }
                    $taskId = array();
                    $ticketRelated = array();

                    if (count($ticketIdsArr) > 0) {
                        //Task
                        $tasksObj = Task::objects()->filter(array("object_type" => "T", "object_id__in" => $ticketIdsArr, "flags" => 1))->values('id', 'dept_id', 'object_id');
                        foreach ($tasksObj as $task) {
                            $taskId[] = $task["id"];
                            $taskDept[$task["id"]] = $task["dept_id"];
                            $ticketRelated[$task["id"]] = $task["object_id"];
                        }


                        $exceptionTaskForReturnStatus = json_decode($cfg->config['exceptionTaskForReturnStatus']['value'],true);
                        if (empty($exceptionTaskForReturnStatus[$status["Details"]])) {

                            if (count($taskId) > 0) {
                                $taskTypeArr = '';
                                $taskTypeIdArr = '';
                                $taskTypeObj = Task::objects()->filter(array('id__in' => $taskId))->values('id', 'data__taskType')->all();
                                foreach ($taskTypeObj as $row) {
                                    $taskTypeIdArr[$row["id"]] = $taskStaticData["id_extra_Task Types"][explode(",", $row["data__taskType"])[0]];
                                    $taskTypeArr[$row["id"]] = $taskStaticData["Default_Task Types"][explode(",", $row["data__taskType"])[0]];
                                }
                                //Tag Name
                                $causeList = CluesExceptionOrderCause::objects()->filter(array('order_id' => $orderId, "type" => "Tag"))->values("cause_id")->all();
                                $tag_name = array('0');
                                foreach ($causeList as $cause) {
                                    $tag_name[] = $cause["cause_id"];
                                }
                                $rmProducts = CscartRMAReturnsProducts::objects()->filter(array("return_id" => $returnId))->values("reason", "price", "product_id")->distinct("return_id", "product_id")->all();
                                $data["category"] = '0';
                                $subReason = array('0');
                                $returnValue = 0;
                                foreach ($rmProducts as $rma) {
                                    if ($rma["reason"])
                                        $subReason[] = $rma["reason"];
                                }
                                $orderDetails = CscartOrders::objects()->filter(array("order_id" => $orderId))->values("total", "cb_used", "cb_used")->all();
                                $returnValue = $orderDetails[0]["total"] + $orderDetails[0]["cb_used"] + $orderDetails[0]["gc_used"];

                                $rmaHistory = CscartRmaReturns::objects()->filter(array("return_id" => $returnId))->values("status", "action")->limit(1)->all();
                                $returnStatus = array('0');
                                $returnStatus[] = $status['Details'];
                                $returnAction = array('0');
                                foreach ($rmaHistory as $rma) {
                                    $returnAction[] = $rma["action"];
                                }
                                foreach ($taskTypeArr as $id => $type) {
                                    $returnFilter = array("status_type" => "Return", "status__in" => $returnStatus,
                                        "return_action__in" => $returnAction,
                                        "tag_name__in" => $tag_name,
                                        "sub_reason__in" => $subReason,
                                        "dept" => $taskDept[$id],
                                        "flowstatus"=>1,
                                        "task_type" => $taskTypeIdArr[$id],
                                    );

                                    $performParamArr = ReturnFlow::objects()->filter($returnFilter)->values("status", "return_action", "tag_name", "value_min", "value_max", "sub_reason", "dept", "task_type", "task_response", "communication")->all();
                                    foreach ($performParamArr as $performItVal) {
                                        if (($performItVal["value_min"] <= $returnValue && $performItVal["value_max"] >= $returnValue) || ($performItVal["value_min"] == 0 && $performItVal["value_max"] == 0)) {
                                            $response = $performItVal["task_response"];
                                            $communication = $performItVal["communication"];
                                        }
                                    }

                                    if (!$response) {
                                        continue;
                                    }
                                    if ($response) {
                                        $data = array();
                                        $data["taskId"] = $id;
                                        $data["agent"] = "SYSTEM - On order Status Update";
                                        $data["email"] = "system.auto@shopclues.com";
                                        $data['source'] = isset($data['source']) ? $data['source'] : 'CRON';
                                        $data["taskResponse"] = $response;
                                        $response = '';
                                        $updateresult = Task::updateTask($data, $errors, $data['source']);

                                        // Log Info for BulkJob Error
                                        $logConfig = logConfig();
                                        $domain = $logConfig["domain"]["D5"];
                                        $module = $logConfig["module"][$domain]["D5_M1"];
                                        $error = $logConfig["error"]["E5"];
                                        $level = $logConfig["level"]["INFO"];
                                        $logObj = new Analog_logger();
                                        $logObj->report($domain, $module, $level, $error, "title:Auto Response", "log_location:class.orderStatus.php", "task:" . $id, "Response:" . $response, substr(json_encode($data) . " Error-" . json_encode($errors), 0, 2000));
                                    }

                                    if ($communication) {
                                        global $cfg;
                                        //Alert
                                        if ($cfg->alertONOrderStatusChange()) {
                                            
                                        }

                                        $tasksDueDatesArr = array();
                                        // Reset Due date
                                        $tasksDueDates = Task::objects()->filter(array("object_type" => "T", "object_id" => $ticketRelated[$id], "flags" => 1))->values("duedate")->all();
                                        foreach ($tasksDueDates as $arrDate) {
                                            $tasksDueDatesArr[] = $arrDate["duedate"];
                                        }

                                        if ($tasksDueDatesArr) {
                                            $maxDuedate = max($tasksDueDatesArr);
                                            $diffSuggested = ($maxDuedate - time()) / 3600 * 24;
                                            if ($diffSuggested > 7) {
                                                $diffSuggestedDays = 3;
                                            } else {
                                                $diffSuggestedDays = 2;
                                            }
                                            $maxDuedateTOBeGiven = time() + $diffSuggestedDays * 24 * 3600;
                                            $ticketObj = Ticket::lookup($ticketRelated[$id]);
                                            $ticketObj->duedate = date('Y-m-d H:i:s', $maxDuedateTOBeGiven);
                                            $ticketObj->est_duedate = date('Y-m-d H:i:s', $maxDuedateTOBeGiven);
                                            $ticketObj->save();
                                        }
                                        $communication = '';
                                    }
                                }
                            }
                        } else {
                            $exceptionResponse = $exceptionTaskForReturnStatus[$status["Details"]]["Response"];
                            $exceptionTask = $exceptionTaskForReturnStatus[$status["Details"]]["Task"];
                            if ($exceptionTask) {
                                foreach ($ticketIdsArr as $ticketId) {
                                 foreach ($exceptionTask as $subtask) {
                                    $dataTask = array(
                                        "object_type" => "T",
                                        "object_id" => $ticketId,
                                        "creater" => "System",
                                        "internal_formdata" => array(
                                            "dept_id" => $subtask["department_id"]
                                        ),
                                        "default_formdata" => array(
                                            "taskType" => json_encode($taskStaticData["create"]["Task Types"][$subtask["taskType"]]),
                                            "taskStatus" => json_encode($taskStaticData["create"]["Task Status"][$subtask["status"]]),
                                            "title" => $taskStaticData["Task Types"][$subtask["taskType"]],
                                            "description" => $taskStaticData["Task Types"][$subtask["taskType"]],
                                        )
                                    );
                                    $slaObj = TaskSLA::lookup($data["task_id"]);
                                    if ($slaObj && $slaObj->sla_id) {
                                        $slaId = $slaObj->sla_id;
                                        $dataTask["internal_formdata"]["sla"] = $slaId;
                                    }

                                    if ($taskData["source"])
                                        $dataTask["default_formdata"]["taskSource"] = json_encode($taskStaticData["create"]["Task Sources"][4]);

                                    $newSubTask = Task::create($dataTask);
                                    $newDept[] = $subtask["department_id"];
                                }
                              }
                            }
                            if ($exceptionResponse) {
                                $tasksObj = Task::objects()->filter(array("object_type" => "T", "object_id__in" => $ticketIdsArr, "flags" => 1));
                                foreach ($tasksObj as $task) {
                                    if(!in_array($task->dept_id,$newDept)){
                                        $data = array();
                                        $data["taskId"] = $task->id;
                                        $data["agent"] = "SYSTEM - On Exceptional Return Status Update";
                                        $data["email"] = "system.auto@shopclues.com";
                                        $data['source'] = isset($data['source']) ? $data['source'] : 'CRON';
                                        $data["taskResponse"] = $exceptionResponse;
                                        $response = '';
                                        $updateresult = Task::updateTask($data, $errors, $data['source']);
                                    }
                                }
                            }
                        }
                    }
                    $orderStatus[$returnId]["Object"]->processed = 1;
                    $orderStatus[$returnId]["Object"]->save();
                }
                $processedOrder[$returnId] = 1;
            }
        }
    }

}
