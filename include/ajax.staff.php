<?php

require_once(INCLUDE_DIR . 'class.staff.php');

class StaffAjaxAPI extends AjaxController {
  /**
   * Ajax: GET /staff/<id>/set-password
   *
   * Uses a dialog to add a new department
   *
   * Returns:
   * 200 - HTML form for addition
   * 201 - {id: <id>, name: <name>}
   *
   * Throws:
   * 403 - Not logged in
   * 403 - Not an administrator
   * 404 - No such agent exists
   */
  function setPassword($id) {
      global $ost, $thisstaff;

      if (!$thisstaff)
          Http::response(403, 'Agent login required');
      if (!$thisstaff->isAdmin())
          Http::response(403, 'Access denied');
      if ($id && !($staff = Staff::lookup($id)))
          Http::response(404, 'No such agent');

      $form = new PasswordResetForm($_POST);
      $errors = array();
      if (!$_POST && isset($_SESSION['new-agent-passwd']))
          $form->data($_SESSION['new-agent-passwd']);

      if ($_POST && $form->isValid()) {
          $clean = $form->getClean();
          if ($id == 0) {
              // Stash in the session later when creating the user
              $_SESSION['new-agent-passwd'] = $clean;
              Http::response(201, 'Carry on');
          }
          try {
              if ($clean['welcome_email']) {
                  $staff->sendResetEmail();
              }
              else {
                  $staff->setPassword($clean['passwd1'], null);
                  if ($clean['change_passwd'])
                      $staff->change_passwd = 1;
              }
              if ($staff->save())
                  Http::response(201, 'Successfully updated');
          }
          catch (BadPassword $ex) {
              $passwd1 = $form->getField('passwd1');
              $passwd1->addError($ex->getMessage());
          }
          catch (PasswordUpdateFailed $ex) {
              $errors['err'] = __('Password update failed:').' '.$ex->getMessage();
          }
      }

      $title = __("Set Agent Password");
      $verb = $id == 0 ? __('Set') : __('Update');
      $path = ltrim($ost->get_path_info(), '/');

      include STAFFINC_DIR . 'templates/quick-add.tmpl.php';
  }

    function changePassword($id) {
        global $cfg, $ost, $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$id || $thisstaff->getId() != $id)
            Http::response(404, 'No such agent');

        $form = new PasswordChangeForm($_POST);
        $errors = array();

        if ($_POST && $form->isValid()) {
            $clean = $form->getClean();
            if (($rtoken = $_SESSION['_staff']['reset-token'])) {
                $_config = new Config('pwreset');
                if ($_config->get($rtoken) != $thisstaff->getId())
                    $errors['err'] =
                        __('Invalid reset token. Logout and try again');
                elseif (!($ts = $_config->lastModified($rtoken))
                        && ($cfg->getPwResetWindow() < (time() - strtotime($ts))))
                    $errors['err'] =
                        __('Invalid reset token. Logout and try again');
            }
            if (!$errors) {
                try {
                    $thisstaff->setPassword($clean['passwd1'], @$clean['current']);
                    if ($thisstaff->save()) {
                        if ($rtoken) {
                            $thisstaff->cancelResetTokens();
                            Http::response(200, $this->encode(array(
                                'redirect' => 'index.php'
                            )));
                        }
                        Http::response(201, 'Successfully updated');
                    }
                }
                catch (BadPassword $ex) {
                    $passwd1 = $form->getField('passwd1');
                    $passwd1->addError($ex->getMessage());
                }
                catch (PasswordUpdateFailed $ex) {
                    $errors['err'] = __('Password update failed:').' '.$ex->getMessage();
                }
            }
        }

        $title = __("Change Password");
        $verb = __('Update');
        $path = ltrim($ost->get_path_info(), '/');

        include STAFFINC_DIR . 'templates/quick-add.tmpl.php';
    }

    function getAgentPerms($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->isAdmin())
            Http::response(403, 'Access denied');
        if (!($staff = Staff::lookup($id)))
            Http::response(404, 'No such agent');

        return $this->encode($staff->getPermissionInfo());
    }

    function resetPermissions() {
        global $ost, $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->isAdmin())
            Http::response(403, 'Access denied');

        $form = new ResetAgentPermissionsForm($_POST);

        if (@is_array($_GET['ids'])) {
            $perms = new RolePermission();
            $selected = Staff::objects()->filter(array('staff_id__in' => $_GET['ids']));
            foreach ($selected as $staff)
                // XXX: This maybe should be intersection rather than union
                $perms->merge($staff->getPermission());
            $form->getField('perms')->setValue($perms->getInfo());
        }

        if ($_POST && $form->isValid()) {
            $clean = $form->getClean();
            Http::response(201, $this->encode(array('perms' => $clean['perms'])));
        }

        $title = __("Reset Agent Permissions");
        $verb = __("Continue");
        $path = ltrim($ost->get_path_info(), '/');

        include STAFFINC_DIR . 'templates/reset-agent-permissions.tmpl.php';
    }
    function addSubAgent(){
        global $ost, $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->isAdmin())
            Http::response(403, 'Access denied');
        $agent_id = $_POST["agent_id"];
        $manager_id = $_POST["manager_id"];
        $dept_id = $_POST["dept_id"];
        
        $sql1 = "insert ignore into ".STAFF_REPORTING."(manager_id,dept_id, agent_id) values ('".$manager_id."',".$dept_id.",'".$agent_id."')";
        $insert1 = db_query($sql1);

        if($insert1){
            $sql1a = "insert ignore into ".STAFF_MAP."(manager_id, agent_id,depth) VALUES ('".$agent_id."','".$agent_id."',0)";
            $insert1a = db_query($sql1a);
                
                
            $sql2 = "insert into ".STAFF_MAP."(manager_id, agent_id, depth)
                    select p.manager_id, c.agent_id, p.depth+c.depth+1  from ".STAFF_MAP." p, ".STAFF_MAP." c
                    where p.agent_id='".$manager_id."' and c.manager_id='".$agent_id."'";
            $insert2 = db_query($sql2);
        }
        if($insert2)
            return 1;
    }
    function resetAgentTree(){
        global $ost, $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->isAdmin())
            Http::response(403, 'Access denied');
        $roleArr = array(2,3,4,5,6,7,8,9);
        $staffObj = StaffMap::objects()->filter(array('dept_id' => 1,'agent_id__lt'=>98))->delete();
        $staffObj = StaffMap::objects()->filter(array('dept_id' => 1,'agent_id__gt'=>98))->delete();
        foreach($roleArr as $roleId){
        $staffObj = Staff::objects()->filter(array('dept_id' => 1,'role__grade'=>$roleId));
        foreach ($staffObj as $staff) {
            $agent_id  = $staff->staff_id;
            if($agent_id){
                $sql1a = "insert ignore into ".STAFF_MAP."(manager_id, agent_id,depth) VALUES ('".$agent_id."','".$agent_id."',0)";
                $insert1a = db_query($sql1a);

                $managerObj = StaffManager::objects()->filter(array('dept_id' => 1,'agent_id'=>$agent_id))->values('manager_id')->all();
                foreach($managerObj as $managerId){
                    $manager_id = $managerId["manager_id"];
                    $sql2 = "insert into ".STAFF_MAP."(manager_id, agent_id, depth)
                            select p.manager_id, c.agent_id, p.depth+c.depth+1 from ".STAFF_MAP." p, ".STAFF_MAP." c
                            where p.agent_id='".$manager_id."' and c.manager_id='".$agent_id."'";
                    $insert2 = db_query($sql2);
                }
            }
        }
        }
        if($insert2)
            return 1;
    }
    function deleteLastAgent(){
        global $ost, $thisstaff;

        $agent_id = $_POST["agent_id"];
        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->isAdmin())
            Http::response(403, 'Access denied');
        
        $sql1 = "delete from ".STAFF_REPORTING." where `agent_id` = '".$agent_id."'";
        $insert1 = db_query($sql1);
        if($insert1){
            $sql2 = "delete from ".STAFF_MAP." where `agent_id` = '".$agent_id."'";
            $insert2 = db_query($sql2);
        }
        if($insert2)
            return 1;
    }

    function changeDepartment() {
        global $ost, $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->isAdmin())
            Http::response(403, 'Access denied');

        $form = new ChangeDepartmentForm($_POST);

        // Preselect reasonable dept and role based on the current  settings
        // of the received staff ids
        if (@is_array($_GET['ids'])) {
            $dept_id = null;
            $role_id = null;
            $selected = Staff::objects()->filter(array('staff_id__in' => $_GET['ids']));
            foreach ($selected as $staff) {
                if (!isset($dept_id)) {
                    $dept_id = $staff->dept_id;
                    $role_id = $staff->role_id;
                }
                elseif ($dept_id != $staff->dept_id)
                    $dept_id = 0;
                elseif ($role_id != $staff->role_id)
                    $role_id = 0;
            }
            $form->getField('dept_id')->setValue($dept_id);
            $form->getField('role_id')->setValue($role_id);
        }

        if ($_POST && $form->isValid()) {
            $clean = $form->getClean();
            Http::response(201, $this->encode($clean));
        }

        $title = __("Change Primary Department");
        $verb = __("Continue");
        $path = ltrim($ost->get_path_info(), '/');

        include STAFFINC_DIR . 'templates/quick-add.tmpl.php';
    }
}
