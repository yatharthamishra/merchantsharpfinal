<?php

/* * *******************************************************************
  class.task.php

  Task
  Peter Rotich <peter@osticket.com>
  Copyright (c)  2014 osTicket
  http://www.osticket.com

  Released under the GNU General Public License WITHOUT ANY WARRANTY.
  See LICENSE.TXT for details.

  vim: expandtab sw=4 ts=4 sts=4:
 * ******************************************************************** */

include_once INCLUDE_DIR . 'class.role.php';
include_once INCLUDE_DIR . 'class.taskflow.php';

class TaskModel extends VerySimpleModel {

    static $meta = array(
        'table' => TASK_TABLE,
        'pk' => array('id'),
        'joins' => array(
            'dept' => array(
                'constraint' => array('dept_id' => 'Dept.id'),
            ),
            'lock' => array(
                'constraint' => array('lock_id' => 'Lock.lock_id'),
                'null' => true,
            ),
            'staff' => array(
                'constraint' => array('staff_id' => 'Staff.staff_id'),
                'null' => true,
            ),
            'team' => array(
                'constraint' => array('team_id' => 'Team.team_id'),
                'null' => true,
            ),
            'thread' => array(
                'constraint' => array(
                    'id' => 'TaskThread.object_id',
                    "'A'" => 'TaskThread.object_type',
                ),
                'list' => false,
                'null' => false,
            ),
            'cdata' => array(
                'constraint' => array('id' => 'TaskCData.task_id'),
                'list' => true,
            ),
            'data' => array(
                'constraint' => array('id' => 'TaskData.task_id'),
                'list' => true,
            ),
            'odata' => array(
                'constraint' => array('object_id' => 'TicketOData.ticket_id'),
                'list' => true,
            ),
            'ticket' => array(
                'constraint' => array(
                    'object_type' => "'T'",
                    'object_id' => 'Ticket.ticket_id',
                ),
                'null' => true,
            ),
        ),
    );

    const PERM_CREATE = 'task.create';
    const PERM_EDIT = 'task.edit';
    const PERM_ASSIGN = 'task.assign';
    const PERM_TRANSFER = 'task.transfer';
    const PERM_REPLY = 'task.reply';
    const PERM_CLOSE = 'task.close';
    const PERM_DELETE = 'task.delete';

    static protected $perms = array(
        self::PERM_CREATE => array(
            'title' =>
            /* @trans */ 'Create',
            'desc' =>
            /* @trans */ 'Ability to create tasks'),
        self::PERM_EDIT => array(
            'title' =>
            /* @trans */ 'Edit',
            'desc' =>
            /* @trans */ 'Ability to edit tasks'),
        self::PERM_ASSIGN => array(
            'title' =>
            /* @trans */ 'Assign',
            'desc' =>
            /* @trans */ 'Ability to assign tasks to agents or teams'),
        self::PERM_TRANSFER => array(
            'title' =>
            /* @trans */ 'Transfer',
            'desc' =>
            /* @trans */ 'Ability to transfer tasks between departments'),
        self::PERM_REPLY => array(
            'title' =>
            /* @trans */ 'Post Reply',
            'desc' =>
            /* @trans */ 'Ability to post task update'),
        self::PERM_CLOSE => array(
            'title' =>
            /* @trans */ 'Close',
            'desc' =>
            /* @trans */ 'Ability to close tasks'),
        self::PERM_DELETE => array(
            'title' =>
            /* @trans */ 'Delete',
            'desc' =>
            /* @trans */ 'Ability to delete tasks'),
    );

    const ISOPEN = 0x0001;
    const ISOVERDUE = 0x0002;

    protected function hasFlag($flag) {
        return ($this->get('flags') & $flag) !== 0;
    }

    protected function clearFlag($flag) {
        return $this->set('flags', $this->get('flags') & ~$flag);
    }

    protected function setFlag($flag) {
        return $this->set('flags', $this->get('flags') | $flag);
    }

    function getId() {
        return $this->id;
    }

    function getNumber() {
        return $this->number;
    }

    function getStaffId() {
        return $this->staff_id;
    }

    function getStaff() {
        return $this->staff;
    }

    function getTeamId() {
        return $this->team_id;
    }

    function getTeam() {
        return $this->team;
    }

    function getDeptId() {
        return $this->dept_id;
    }

    function getDept() {
        return $this->dept;
    }

    function getCreateDate() {
        return $this->created;
    }

    function getDueDate() {
        return $this->duedate;
    }

    function getPromisedDueDate() {
        return $this->promised_duedate;
    }

    function getCloseDate() {
        // TODO: have true close date
        return $this->isClosed() ? $this->updated : '';
    }

    /**
     * Status for task
     * @author Akash Kumar
     */
    function getStatus() {
        return $this->status;
    }

    function isOpen() {
        return $this->hasFlag(self::ISOPEN);
    }

    function isClosed() {
        return !$this->isOpen();
    }

    protected function close() {
        return $this->clearFlag(self::ISOPEN);
    }

    protected function reopen() {
        return $this->setFlag(self::ISOPEN);
    }

    function isAssigned() {
        return (($this->getStaffId() || $this->getTeamId()));
    }

    function isOverdue() {
        return $this->hasFlag(self::ISOVERDUE);
    }

    static function getPermissions() {
        return self::$perms;
    }

}

RolePermission::register(/* @trans */ 'Tasks', TaskModel::getPermissions());

class SellerMailQueue extends VerySimpleModel {

    static $meta = array(
        'pk' => array('id'),
        'table' => SELLER_MAIL_QUEUE
    );

}

class Task extends TaskModel implements RestrictedAccess, Threadable {

    var $form;
    var $entry;
    public $closeOnly = 0;
    var $_thread;
    var $_entries;
    var $_answers;
    var $_answersId;
    var $lastrespondent;

    function __onload() {
        $this->loadDynamicData();
    }

// old loadDynamicData
    function loadOldDynamicData() {
        if (!isset($this->_answers)) {
            $this->_answers = array();
            foreach (DynamicFormEntryAnswer::objects()
                    ->filter(array(
                        'entry__object_id' => $this->getId(),
                        'entry__object_type' => ObjectModel::OBJECT_TYPE_TASK
                    )) as $answer
            ) {
                $tag = mb_strtolower($answer->field->name)
                        ? : 'field.' . $answer->field->id;
                $this->_answers[$tag] = $answer;
            }
        }

        return $this->_answers;
    }

    //modified loadDynamicData
    function loadDynamicData($force = 0) {
        if (!isset($this->_answers) || $force == 1) {
            $this->_answers = array();

            $data = TaskData::lookup($this->getId());
            foreach ($data->ht as $key => $value) {
                $keyS = mb_strtolower($key);
                if ($keyS == "taskcomment") {
                    $this->_answers[$keyS] = $value;
                } else {
                    $this->_answers[$keyS] = stristr($value, ",") ? explode(",", $value)[1] : $value;
                }
                $this->_answersId[$keyS] = stristr($value, ",") ? explode(",", $value)[0] : $value;
            }
        }
        return $this->_answers;
    }

    function hook($action) {
        $action = 'On' . ucfirst($action);
        $this->$action();
    }

    function OnResponse($responseJson='',$taskData = []) {
	global $thisstaff;
        $taskId = $this->getId();
	if(is_array(json_decode($responseJson,1))){
        	$response = key(json_decode($responseJson, true));
	}else{
		$response=$responseJson;
	}
        if ($this->isOpen()) {
            $taskResponse = DynamicListItem::lookup($response);
            $StatusId = key(array_values(json_decode($taskResponse->properties, true))[0]);

            $taskStatusObj = DynamicListItem::lookup($StatusId);
            $taskStatus = $taskStatusObj->extra;
	if ($taskStatus) {
                $data = array("taskId" => $taskId, "taskStatus" => $taskStatus, "taskAgent" => "On Response Change", "email" => "system.auto@shopclues.com");
                $statusChange = $this->updateTask($data, $error);
	
		$noteData = array();
                $noteData['id'] = $taskId;
                $staticTaskData = getCachedTaskFields();
                $taskType = $staticTaskData['Default_Task Response'][$response];
                $noteData['note'] = "Task Response : ".$taskType;
		$note = $this->postNote($noteData, $errors, $thisstaff);
            }
            if ($this->isOpen()) {
                $this->nextTaskCheck($taskData);
            }
            $logConfig = logConfig();
            $domain = $logConfig["domain"]["D1"];
            $module = $logConfig["module"][$domain]["D1_M1"];
            $error = $logConfig["error"]["E16"];
            $logObj = new Analog_logger();
            $logObj->report($domain, $module, $logConfig["level"]["INFO"], $error, "title:TASK ON RESPONSE", "log_location:class.task.php", "ticket_id:".$this->ht['object_id'], "task_id:".$taskId, substr($responseJson, 0, 2000));
        }
    }

    function OnClose() {
        $taskId = $this->getId();
        $taskData = TaskData::lookup($taskId);
        if($taskData->ht["extra"]){
  //          $jsonExtra=json_encode(json_decode($taskData->ht["extra"],true));
            $previousData=array('taskExtraField'=>$taskData->ht["extra"]);
        }else{
            $previousData=[];
        }
        global $thisstaff;
        $this->nextTaskCheck($previousData);
        $ticket = Ticket::lookup($this->object_id);
        $taskData->closureStatus = $ticket->getOrderStatusFromOrderIds(array($taskData->ht['order_id']))[$taskData->ht['order_id']];
        $taskData->closedBy = $thisstaff->ht['staff_id'];
        $taskData->save();
    }

    function getIdFromstring($string) {
        return explode(",", $string)[0]? : $string;
    }

    function nextTaskCheck($taskVariableData) {
        global $cfg;
        $taskId = $this->getId();
        $this->loadDynamicData(1);
        $taskData = TaskData::lookup($this->getId());
        $taskResponse = $this->getIdFromstring($taskData->taskResponse);
        $taskStatus = $this->getIdFromstring($taskData->taskStatus);
        $taskType = $this->getIdFromstring($taskData->taskType);
        $ticketId = $this->object_id;
        $ticket = Ticket::lookup($ticketId);
        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D2"];
        $module = $logConfig["module"][$domain]["D2_M1"];
        $error = $logConfig["error"]["E2"];
        $level = $logConfig["level"]["INFO"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $level, "NEXT TASK", "title:Next_TASK", "R" . $taskResponse, "task:" . $this->getId(), "S" . $taskStatus, "T" . $taskType);
        if ($ticket->getState() == "open" && !$this->closeOnly) {
            if ($taskType && $taskStatus && $taskResponse) {
                $typeList = DynamicList::objects()->filter(array("name__in" => array("Task Types", "Task Status", "Task Response", "Task Sources")));
                foreach ($typeList as $list) {
                    foreach ($list->items as $items) {
                        $listed[$list->name][$items->id]["id"] = $items->extra;
                        $listed[$list->name][$items->id]["name"] = $items->value;
                        $listFormated[$list->name][$items->extra] = array($items->id => $items->value);
                    }
                }

                $taskFilter = array("dept_id" => $this->getDeptId(), "type__in" => array($listed["Task Types"][$taskType]["id"], 0), "task_status" => $listed["Task Status"][$taskStatus]["id"], "flowstatus" => 1, "task_response" => $listed["Task Response"][$taskResponse]["id"]);
                $taskFlow = TaskFlow::objects()->filter($taskFilter);
                if(count($taskFlow)>0){
                    $taskDataPreviousVal=TaskData::objects()->filter(array('task_id'=>  $this->getId()))->values()->all();
$taskDataPreviousVal['createStatus']=$taskDataPreviousVal['ord_status'];
                }
                
                foreach ($taskFlow as $flow) {
                    $nextTask = $flow->next_task_id;
                    if ($nextTask) {
                        $nextTaskFlow = TaskFlow::lookup($nextTask);
                        if ($nextTaskFlow) {
                            $taskFlowObj = new TaskFlow();
                            $new_task = $taskFlowObj->createNext($nextTaskFlow, $taskId, $this->object_id, $listFormated,$taskData->order_id,$taskData->manifest_id,$taskVariableData,$taskDataPreviousVal);
                            $this->moveTaskAttachment($new_task);
                        }
                    }
                    if ($flow->communication) {
                        $communication = $flow->communication;
                        if ($cfg->alertONTaskFlowChange()) {
                            if (!$cfg || !($dept = $ticket->getDept()) || !($tpl = $dept->getTemplate()) || !($email = $dept->getAutoRespEmail())
                            ) {
                                //bail out...missing stuff.
                            } else {
                                $template = EmailTemplate::lookup($communication);
                                if (is_object($template) && $template->getCodeName()) {
                                    $templateName = $template->getCodeName();
                                }
                                $autorespond = 1;
                                //Send auto response - if enabled.
                                if ($autorespond && $templateName && ($msg = $tpl->getMsgTemplate($templateName))
                                ) {

                                    if ($cfg->config['use_mail_task_closure_reply']['value']) {
                                        $objects = serialize(array('msg' => $msg, 'ticket' => $ticket, 'task' => $this, 'taskData' => $taskData, 'dept' => $dept, 'email' => $email));
                                        $dataForQueue = array('objects' => $objects, 'message' => $message, 'autoSentMessage' => $autoSentMessage);
                                        addMailToMailingQueue('taskClosureAutoReply', $dataForQueue);
                                    } else {
                                        $msg = $ticket->replaceVars(
                                                $msg->asArray(), array('message' => $message,
                                            'recipient' => $ticket->getOwner(),
                                            'signature' => ($dept && $dept->isPublic()) ? $dept->getSignature() : ''
                                                )
                                        );
                                        $options = array();
                                        $email->sendAutoReply($ticket->getOwner(), $msg['subj'], $msg['body'], null, $options);
                                        $autoSentMessage["note"] = "<strong>Auto Mail -</strong> <br>" . $msg['subj'] . "<br>" . $msg['body'];

                                        $ticket->postNote($autoSentMessage);
                                        $taskData->autocommunication = 1;
                                        $taskData->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //Check is no task is open - Close the ticket
        $openTaskFilter = array("object_type" => "T", "object_id" => $this->object_id, "flags" => 1);

        $openTask = Task::objects()->filter($openTaskFilter);
	if ($openTask->count() == 0) {
	   if(is_null($ticket)){
		$ticket = Ticket::lookup($ticketId);
	   }
            $ticket->setStatus(7, 'Solved by SYSTEM on tasks end.');
        }
    }
    
    function moveTaskAttachment($new_task){
    global $thisstaff;
    $old_attachment=Attachment::objects()->filter(array("object_id" => $this->getId(),"type"=>'A'))->values()->all();
    
           foreach($old_attachment as $k=>$v){
           $attachParam = array("object_id" => $new_task->getId(), "file_id" => $v['file_id'], "type" => $v['type'], "inline" => 0, "staff_id" => $thisstaff->getId(), "ticket_id" => $v['ticket_id']);

            $attachParamObj = Attachment::create($attachParam);
            $attachParamObj->created = SqlFunction::NOW();
            $attachParamObj->save();
            }
       } 

    function getStatus() {
        return $this->isOpen() ? __('Open') : __('Completed');
    }

    function getTaskSource() {
        return $this->_answers["tasksource"];
    }

    function getTaskStatus() {
        return $this->_answers["taskstatus"];
    }
    function getTaskStatusName(){
        $taskStatusId=  $this->_answers["taskstatus"];
        $cachedTaskFields=getCachedAPIFields();
        return $cachedTaskFields['Default_Task Status'][$taskStatusId];
    
    }
    function getTaskResponse() {
        return $this->_answersId["taskresponse"];
    }

    function getTaskComments() {
        return $this->_answers['taskcomment'];
    }

    function getTaskType() {
        return $this->_answers["tasktype"];
    }

    function getTaskTypeId() {
        return $this->_answersId["tasktype"];
    }

    function getCreateAgent() {
        return $this->_answers["createdby"];
    }

    function getCloseAgent() {
        return $this->_answers["closedby"];
    }

    function getcreateStatus() {
        return $this->_answers["createstatus"];
    }

    function getcreateReturnStatus() {
        return $this->_answers["createreturnstatus"];
    }

    function getcreateRefundStatus() {
        return $this->_answers["createrefundstatus"];
    }

    function getclosureStatus() {
        return $this->_answers["closurestatus"];
    }

    function getclosureReturnStatus() {
        return $this->_answers["closurereturnstatus"];
    }

    function getclosureRefundStatus() {
        return $this->_answers["closurerefundstatus"];
    }

    function getTitle() {
        return $this->__cdata('title', ObjectModel::OBJECT_TYPE_TASK);
    }

    function checkStaffPerm($staff, $perm = null, $force = 0) {

        if ($force)
            return true;
        // Must be a valid staff
        if (!$staff instanceof Staff && !($staff = Staff::lookup($staff)))
            return false;

        // Check access based on department or assignment
        if (!$staff->canAccessDept($this->getDeptId()) && $this->isOpen() && $staff->getId() != $this->getStaffId() && !$staff->isTeamMember($this->getTeamId()))
            return false;

        // At this point staff has access unless a specific permission is
        // requested
        if ($perm === null)
            return true;

        // Permission check requested -- get role.
        if (!($role = $staff->getRole($this->getDeptId())))
            return false;

        // Check permission based on the effective role
        return $role->hasPerm($perm);
    }

    function getAssignee() {

        if (!$this->isOpen() || !$this->isAssigned())
            return false;

        if ($this->staff)
            return $this->staff;

        if ($this->team)
            return $this->team;

        return null;
    }

    function getAssigneeId() {

        if (!($assignee = $this->getAssignee()))
            return null;

        $id = '';
        if ($assignee instanceof Staff)
            $id = 's' . $assignee->getId();
        elseif ($assignee instanceof Team)
            $id = 't' . $assignee->getId();

        return $id;
    }

    function getAssignees() {

        $assignees = array();
        if ($this->staff)
            $assignees[] = $this->staff->getName();

        //Add team assignment
        if ($this->team)
            $assignees[] = $this->team->getName();

        return $assignees;
    }

    function getAssigned($glue = '/') {
        $assignees = $this->getAssignees();

        return $assignees ? implode($glue, $assignees) : '';
    }

    function getLastRespondent() {

        if (!isset($this->lastrespondent)) {
            $this->lastrespondent = Staff::objects()
                            ->filter(array(
                                'staff_id' => static::objects()
                                ->filter(array(
                                    'thread__entries__type' => 'R',
                                    'thread__entries__staff_id__gt' => 0
                                ))
                                ->values_flat('thread__entries__staff_id')
                                ->order_by('-thread__entries__id')
                                ->limit(1)
                            ))
                            ->first()
                    ? : false;
        }

        return $this->lastrespondent;
    }

    function getParticipants() {
        $participants = array();
        foreach ($this->getThread()->collaborators as $c)
            $participants[] = $c->getName();

        return $participants ? implode(', ', $participants) : ' ';
    }

    function getThreadId() {
        return $this->thread->getId();
    }

    function getThread() {
        return $this->thread;
    }

    function getThreadEntry($id) {
        return $this->getThread()->getEntry($id);
    }

    function getThreadEntries($type = false) {
        $thread = $this->getThread()->getEntries();
        if ($type && is_array($type))
            $thread->filter(array('type__in' => $type));
        return $thread;
    }

    function postThreadEntry($type, $vars, $options = array()) {
        $errors = array();
        $poster = isset($options['poster']) ? $options['poster'] : null;
        $alert = isset($options['alert']) ? $options['alert'] : true;
        switch ($type) {
            case 'N':
            default:
                return $this->postNote($vars, $errors, $poster, $alert);
        }
    }

    function getForm() {
        if (!isset($this->form)) {
            // Look for the entry first
            if ($this->form = DynamicFormEntry::lookup(
                            array('object_type' => ObjectModel::OBJECT_TYPE_TASK))) {
                return $this->form;
            }
            // Make sure the form is in the database
            elseif (!($this->form = DynamicForm::lookup(
                            array('type' => ObjectModel::OBJECT_TYPE_TASK)))) {
                $this->__loadDefaultForm();
                return $this->getForm();
            }
            // Create an entry to be saved later
            $this->form = $this->form->instanciate();
            $this->form->object_type = ObjectModel::OBJECT_TYPE_TASK;
        }

        return $this->form;
    }

    function getAssignmentForm($source = null) {

        if (!$source)
            $source = array('assignee' => array($this->getAssigneeId()));

        return AssignmentForm::instantiate($source, array('dept' => $this->getDept()));
    }

    function getTransferForm($source = null) {

        if (!$source)
            $source = array('dept' => array($this->getDeptId()));

        return TransferForm::instantiate($source);
    }

    function editDynamicData($data) {
        $formId = DynamicFormEntry::forObject($this->id, ObjectModel::OBJECT_TYPE_TASK)->values('id')->first();
        $formId = $formId["id"];
        $tf = TaskForm::getInstance($this->id, true);
        foreach ($tf->getFields() as $f) {
            if (isset($data[$f->get('name')])) {
                $taskformentry = DynamicFormEntryAnswer::lookup(array("entry_id" => $formId, "field_id" => $f->get('id')));
                $taskformentry->value = $data[$f->get('name')];
                $taskformentry->save();
                $this->_answers[strtolower($f->get('name'))] = $data[$f->get('name')];
            }
        }
        return $tf;
    }

    function addDynamicData($data) {
        $tf = TaskForm::getInstance($this->id, true);
        foreach ($tf->getFields() as $f)
            if (isset($data[$f->get('name')]))
                $tf->setAnswer($f->get('name'), $data[$f->get('name')]);
        $tf->save();
        return $tf;
    }

    function getDynamicData($create = true) {
        if (!isset($this->_entries)) {
            $this->_entries = DynamicFormEntry::forObject($this->id, ObjectModel::OBJECT_TYPE_TASK)->all();
            if (!$this->_entries && $create) {
                $f = TaskForm::getInstance($this->id, true);
                $f->save();
                $this->_entries[] = $f;
            }
        }

        return $this->_entries ? : array();
    }

    function getTaskGrouping($data , $taskNumPreserve) {

        foreach ($data as $task) {
            $task->ht['display_number'] = $taskNumPreserve[$task->ht['id']];
            $groupingKey = 2000000;
            if(!empty($task->_answers['order_id'])) $groupingKey = $task->_answers['order_id'];
            $taskGrouping[$groupingKey]['tasks'][] = $task;
            if($task->ht['flags']) $taskGrouping[$groupingKey]['priority']++;

        }

        $sort_priority = array();
        foreach ($taskGrouping as $key => $value) {
            $sort_priority[$key] = $value['priority'];
        }
        array_multisort($sort_priority, SORT_DESC, $taskGrouping);

        foreach ($taskGrouping as $key => $value) {

            $sort_created = $sort_flags = array();
            foreach ($value['tasks'] as $k => $val) {
                $sort_created[$k] = $val->ht['created'];
            }
            array_multisort($sort_created, SORT_ASC, $value['tasks']);
            foreach ($value['tasks'] as $k => $val) {
                $sort_flags[$k] = $val->ht['flags'];
            }
            array_multisort($sort_flags, SORT_DESC, $value['tasks']);
        }
        return $taskGrouping;
    }

    function setStatus($status, $comments = '') {
        global $thisstaff;

        $ecb = null;
        switch ($status) {
            case 'open':
                if ($this->isOpen())
                    return false;

                $this->reopen();
                $this->closed = null;

                $ecb = function ($t) {
                    $t->logEvent('reopened', false, null, 'closed');
                };
                break;
            case 'closed':
                if ($this->isClosed())
                    return false;

                $this->close();
                $this->closed = SqlFunction::NOW();
                $ecb = function($t) {
                    $t->logEvent('closed');
                };
                $this->save();
                if ($comments == 'closeOnly')
                    $this->closeOnly = 1;
                $this->hook('close');
                break;
            default:
                return false;
        }

        if (!$this->save(true))
            return false;

        // Log events via callback
        if ($ecb)
            $ecb($this);

        if ($comments) {
            $errors = array();
            $this->postNote(array(
                'note' => $comments,
                'title' => sprintf(
                        __('Status changed to %s'), $this->getStatus())
                    ), $errors, $thisstaff);
        }

        return true;
    }

    function to_json() {

        $info = array(
            'id' => $this->getId(),
            'title' => $this->getTitle()
        );

        return JsonDataEncoder::encode($info);
    }

    function __cdata($field, $ftype = null) {

        foreach ($this->getDynamicData() as $e) {
            // Make sure the form type matches
            if (!$e->form || ($ftype && $ftype != $e->form->get('type')))
                continue;

            // Get the named field and return the answer
            if ($a = $e->getAnswer($field))
                return $a;
        }

        return null;
    }

    function __toString() {
        return (string) $this->getTitle();
    }

    /* util routines */

    function logEvent($state, $data = null, $user = null, $annul = null) {
        //$this->getThread()->getEvents()->log($this, $state, $data, $user, $annul);
    }

    function assign(AssignmentForm $form, &$errors, $alert = true) {
        global $thisstaff;

        $evd = array();
        $assignee = $form->getAssignee();
        if ($assignee instanceof Staff) {
            if ($this->getStaffId() == $assignee->getId()) {
                $errors['assignee'] = sprintf(__('%s already assigned to %s'), __('Task'), __('the agent')
                );
            } elseif (!$assignee->isAvailable()) {
                $errors['assignee'] = __('Agent is unavailable for assignment');
            } else {
                $this->staff_id = $assignee->getId();
                if ($thisstaff && $thisstaff->getId() == $assignee->getId())
                    $evd['claim'] = true;
                else
                    $evd['staff'] = array($assignee->getId(), $assignee->getName());
            }
        } elseif ($assignee instanceof Team) {
            if ($this->getTeamId() == $assignee->getId()) {
                $errors['assignee'] = sprintf(__('%s already assigned to %s'), __('Task'), __('the team')
                );
            } else {
                $this->team_id = $assignee->getId();
                $evd = array('team' => $assignee->getId());
            }
        } else {
            $errors['assignee'] = __('Unknown assignee');
        }

        if ($errors || !$this->save(true))
            return false;

        $this->logEvent('assigned', $evd);

        $this->onAssignment($assignee, $form->getField('comments')->getClean(), $alert);

        return true;
    }

    function onAssignment($assignee, $comments = '', $alert = true) {
        global $thisstaff, $cfg;

        if (!is_object($assignee))
            return false;

        $assigner = $thisstaff ? : __('SYSTEM (Auto Assignment)');

        //Assignment completed... post internal note.
        $note = null;
        if ($comments) {

            $title = sprintf(__('Task assigned to %s'), (string) $assignee);

            $errors = array();
            $note = $this->postNote(
                    array('note' => $comments, 'title' => $title), $errors, $assigner, false);
        }

        // Send alerts out if enabled.
        if (!$alert || !$cfg->alertONTaskAssignment())
            return false;

        if (!($dept = $this->getDept()) || !($tpl = $dept->getTemplate()) || !($email = $dept->getAlertEmail())
        ) {
            return true;
        }

        // Recipients
        $recipients = array();
        if ($assignee instanceof Staff) {
            if ($cfg->alertStaffONTaskAssignment())
                $recipients[] = $assignee;
        } elseif (($assignee instanceof Team) && $assignee->alertsEnabled()) {
            if ($cfg->alertTeamMembersONTaskAssignment() && ($members = $assignee->getMembers()))
                $recipients = array_merge($recipients, $members);
            elseif ($cfg->alertTeamLeadONTaskAssignment() && ($lead = $assignee->getTeamLead()))
                $recipients[] = $lead;
        }

        if ($recipients && ($msg = $tpl->getTaskAssignmentAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(), array('comments' => $comments,
                'assignee' => $assignee,
                'assigner' => $assigner
                    )
            );
            // Send the alerts.
            $sentlist = array();
            $options = $note instanceof ThreadEntry ? array(
                'inreplyto' => $note->getEmailMessageId(),
                'references' => $note->getEmailReferences(),
                'thread' => $note) : array();

            foreach ($recipients as $k => $staff) {
                if (!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)) {
                    continue;
                }

                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }

        return true;
    }

    function transfer(TransferForm $form, &$errors, $alert = true) {
        global $thisstaff, $cfg;

        $cdept = $this->getDept();
        $dept = $form->getDept();
        if (!$dept || !($dept instanceof Dept))
            $errors['dept'] = __('Department selection required');
        elseif ($dept->getid() == $this->getDeptId())
            $errors['dept'] = __('Task already in the department');
        else
            $this->dept_id = $dept->getId();

        if ($errors || !$this->save())
            return false;

        // Log transfer event
        $this->logEvent('transferred');

        // Post internal note if any
        $note = $form->getField('comments')->getClean();
        if ($note) {
            $title = sprintf(__('%1$s transferred from %2$s to %3$s'), __('Task'), $cdept->getName(), $dept->getName());

            $_errors = array();
            $note = $this->postNote(
                    array('note' => $note, 'title' => $title), $_errors, $thisstaff, false);
        }

        // Send alerts if requested && enabled.
        if (!$alert || !$cfg->alertONTaskTransfer())
            return true;

        if (($email = $dept->getAlertEmail()) && ($tpl = $dept->getTemplate()) && ($msg = $tpl->getTaskTransferAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(), array('comments' => $note, 'staff' => $thisstaff));
            // Recipients
            $recipients = array();
            // Assigned staff or team... if any
            if ($this->isAssigned() && $cfg->alertAssignedONTaskTransfer()) {
                if ($this->getStaffId())
                    $recipients[] = $this->getStaff();
                elseif ($this->getTeamId() && ($team = $this->getTeam()) && ($members = $team->getMembers())
                ) {
                    $recipients = array_merge($recipients, $members);
                }
            } elseif ($cfg->alertDeptMembersONTaskTransfer() && !$this->isAssigned()) {
                // Only alerts dept members if the task is NOT assigned.
                if ($members = $dept->getMembersForAlerts())
                    $recipients = array_merge($recipients, $members);
            }

            // Always alert dept manager??
            if ($cfg->alertDeptManagerONTaskTransfer() && ($manager = $dept->getManager())) {
                $recipients[] = $manager;
            }

            $sentlist = $options = array();
            if ($note instanceof ThreadEntry) {
                $options += array(
                    'inreplyto' => $note->getEmailMessageId(),
                    'references' => $note->getEmailReferences(),
                    'thread' => $note);
            }

            foreach ($recipients as $k => $staff) {
                if (!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)
                ) {
                    continue;
                }
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }

        return true;
    }

    function postNote($vars, &$errors, $poster = '', $alert = true) {
        global $cfg, $thisstaff;

        $vars['staffId'] = 0;
        $vars['poster'] = 'SYSTEM';
        if ($poster && is_object($poster)) {
            $vars['staffId'] = $poster->getId();
            $vars['poster'] = $poster->getName();
        } elseif ($poster) { //string
            $vars['poster'] = $poster;
        }

        if (!($note = $this->getThread()->addNote($vars, $errors)))
            return null;

        $assignee = $this->getStaff();

        if (isset($vars['task_status']))
            $this->setStatus($vars['task_status']);

        $this->onActivity(array(
            'activity' => $note->getActivity(),
            'threadentry' => $note,
            'assignee' => $assignee
                ), $alert);

        return $note;
    }

    /* public */

    function postReply($vars, &$errors, $alert = true) {
        global $thisstaff, $cfg;


        if (!$vars['poster'] && $thisstaff)
            $vars['poster'] = $thisstaff;

        if (!$vars['staffId'] && $thisstaff)
            $vars['staffId'] = $thisstaff->getId();

        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        if (!($response = $this->getThread()->addResponse($vars, $errors)))
            return null;

        $assignee = $this->getStaff();
        // Set status - if checked.
        if ($vars['reply_status_id'] && $vars['reply_status_id'] != $this->getStatusId()
        ) {
            $this->setStatus($vars['reply_status_id']);
        }

        /*
          // TODO: add auto claim setting for tasks.
          // Claim on response bypasses the department assignment restrictions
          if ($thisstaff
          && $this->isOpen()
          && !$this->getStaffId()
          && $cfg->autoClaimTasks)
          ) {
          $this->staff_id = $thisstaff->getId();
          }
         */

        $this->lastrespondent = $response->staff;
        $this->save();

        // Send activity alert to agents
        $activity = $vars['activity'] ? : $response->getActivity();
        $this->onActivity(array(
            'activity' => $activity,
            'threadentry' => $response,
            'assignee' => $assignee,
        ));
        // Send alert to collaborators
        if ($alert && $vars['emailcollab']) {
            $signature = '';
            $this->notifyCollaborators($response, array('signature' => $signature)
            );
        }

        return $response;
    }

    function pdfExport($options = array()) {
        global $thisstaff;

        require_once(INCLUDE_DIR . 'class.pdf.php');
        if (!isset($options['psize'])) {
            if ($_SESSION['PAPER_SIZE'])
                $psize = $_SESSION['PAPER_SIZE'];
            elseif (!$thisstaff || !($psize = $thisstaff->getDefaultPaperSize()))
                $psize = 'Letter';

            $options['psize'] = $psize;
        }

        $pdf = new Task2PDF($this, $options);
        $name = 'Task-' . $this->getNumber() . '.pdf';
        Http::download($name, 'application/pdf', $pdf->Output($name, 'S'));
        //Remember what the user selected - for autoselect on the next print.
        $_SESSION['PAPER_SIZE'] = $options['psize'];
        exit;
    }

    /* util routines */

    function replaceVars($input, $vars = array()) {
        global $ost;

        return $ost->replaceTemplateVariables($input, array_merge($vars, array('task' => $this)));
    }

    function asVar() {
        return $this->getNumber();
    }

    function getVar($tag) {
        global $cfg;

        if ($tag && is_callable(array($this, 'get' . ucfirst($tag))))
            return call_user_func(array($this, 'get' . ucfirst($tag)));

        switch (mb_strtolower($tag)) {
            case 'phone':
            case 'phone_number':
                return $this->getPhoneNumber();
                break;
            case 'staff_link':
                return sprintf('%s/scp/tasks.php?id=%d', $cfg->getBaseUrl(), $this->getId());
                break;
            case 'create_date':
                return new FormattedDate($this->getCreateDate());
                break;
            case 'due_date':
                if ($due = $this->getEstDueDate())
                    return new FormattedDate($due);
                break;
            case 'close_date':
                if ($this->isClosed())
                    return new FormattedDate($this->getCloseDate());
                break;
            case 'last_update':
                return new FormattedDate($this->last_update);
            default:
                if (isset($this->_answers[$tag]))
                // The answer object is retrieved here which will
                // automatically invoke the toString() method when the
                // answer is coerced into text
                    return $this->_answers[$tag];
        }
        return false;
    }

    static function getVarScope() {
        $base = array(
            'assigned' => __('Assigned agent and/or team'),
            'close_date' => array(
                'class' => 'FormattedDate', 'desc' => __('Date Closed'),
            ),
            'create_date' => array(
                'class' => 'FormattedDate', 'desc' => __('Date created'),
            ),
            'dept' => array(
                'class' => 'Dept', 'desc' => __('Department'),
            ),
            'due_date' => array(
                'class' => 'FormattedDate', 'desc' => __('Due Date'),
            ),
            'number' => __('Task number'),
            'recipients' => array(
                'class' => 'UserList', 'desc' => __('List of all recipient names'),
            ),
            'status' => __('Status'),
            'staff' => array(
                'class' => 'Staff', 'desc' => __('Assigned/closing agent'),
            ),
            'subject' => 'Subject',
            'team' => array(
                'class' => 'Team', 'desc' => __('Assigned/closing team'),
            ),
            'thread' => array(
                'class' => 'TaskThread', 'desc' => __('Task Thread'),
            ),
            'last_update' => array(
                'class' => 'FormattedDate', 'desc' => __('Time of last update'),
            ),
        );

        $extra = VariableReplacer::compileFormScope(TaskForm::getInstance());
        return $base + $extra;
    }

    function onActivity($vars, $alert = true) {
        global $cfg, $thisstaff;

        if (!$alert // Check if alert is enabled
                || !$cfg->alertONTaskActivity() || !($dept = $this->getDept()) || !($email = $cfg->getAlertEmail()) || !($tpl = $dept->getTemplate()) || !($msg = $tpl->getTaskActivityAlertMsgTemplate())
        ) {
            return;
        }

        // Alert recipients
        $recipients = array();
        //Last respondent.
        if ($cfg->alertLastRespondentONTaskActivity())
            $recipients[] = $this->getLastRespondent();

        // Assigned staff / team
        if ($cfg->alertAssignedONTaskActivity()) {
            if (isset($vars['assignee']) && $vars['assignee'] instanceof Staff)
                $recipients[] = $vars['assignee'];
            elseif ($this->isOpen() && ($assignee = $this->getStaff()))
                $recipients[] = $assignee;

            if ($team = $this->getTeam())
                $recipients = array_merge($recipients, $team->getMembers());
        }

        // Dept manager
        if ($cfg->alertDeptManagerONTaskActivity() && $dept && $dept->getManagerId())
            $recipients[] = $dept->getManager();

        $options = array();
        $staffId = $thisstaff ? $thisstaff->getId() : 0;
        if ($vars['threadentry'] && $vars['threadentry'] instanceof ThreadEntry) {
            $options = array(
                'inreplyto' => $vars['threadentry']->getEmailMessageId(),
                'references' => $vars['threadentry']->getEmailReferences(),
                'thread' => $vars['threadentry']);

            // Activity details
            if (!$vars['message'])
                $vars['message'] = $vars['threadentry'];

            // Staff doing the activity
            $staffId = $vars['threadentry']->getStaffId() ? : $staffId;
        }

        $msg = $this->replaceVars($msg->asArray(), array(
            'note' => $vars['threadentry'], // For compatibility
            'activity' => $vars['activity'],
            'message' => $vars['message']));

        $isClosed = $this->isClosed();
        $sentlist = array();
        foreach ($recipients as $k => $staff) {
            if (!is_object($staff)
                    // Don't bother vacationing staff.
                    || !$staff->isAvailable()
                    // No need to alert the poster!
                    || $staffId == $staff->getId()
                    // No duplicates.
                    || isset($sentlist[$staff->getEmail()])
                    // Make sure staff has access to task
                    || ($isClosed && !$this->checkStaffPerm($staff))
            ) {
                continue;
            }
            $alert = $this->replaceVars($msg, array('recipient' => $staff));
            $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
            $sentlist[$staff->getEmail()] = 1;
        }
    }

    /*
     * Notify collaborators on response or new message
     *
     */

    function notifyCollaborators($entry, $vars = array()) {
        global $cfg;

        if (!$entry instanceof ThreadEntry || !($recipients = $this->getThread()->getParticipants()) || !($dept = $this->getDept()) || !($tpl = $dept->getTemplate()) || !($msg = $tpl->getTaskActivityNoticeMsgTemplate()) || !($email = $dept->getEmail())
        ) {
            return;
        }

        // Who posted the entry?
        $skip = array();
        if ($entry instanceof Message) {
            $poster = $entry->getUser();
            // Skip the person who sent in the message
            $skip[$entry->getUserId()] = 1;
            // Skip all the other recipients of the message
            foreach ($entry->getAllEmailRecipients() as $R) {
                foreach ($recipients as $R2) {
                    if (0 === strcasecmp($R2->getEmail(), $R->mailbox . '@' . $R->host)) {
                        $skip[$R2->getUserId()] = true;
                        break;
                    }
                }
            }
        } else {
            $poster = $entry->getStaff();
        }

        $vars = array_merge($vars, array(
            'message' => (string) $entry,
            'poster' => $poster ? : _S('A collaborator'),
                )
        );

        $msg = $this->replaceVars($msg->asArray(), $vars);

        $attachments = $cfg->emailAttachments() ? $entry->getAttachments() : array();
        $options = array('inreplyto' => $entry->getEmailMessageId(),
            'thread' => $entry);

        foreach ($recipients as $recipient) {
            // Skip folks who have already been included on this part of
            // the conversation
            if (isset($skip[$recipient->getUserId()]))
                continue;
            $notice = $this->replaceVars($msg, array('recipient' => $recipient));
            $email->send($recipient, $notice['subj'], $notice['body'], $attachments, $options);
        }
    }

    /**
     * Function to update Task to be used by api
     * @global type $thisstaff
     * @param type $vars
     * @param type $errors
     * @param type $origin
     * @param type $autorespond
     * @param type $alertstaff
     * @return boolean|string|int
     */
    static function updateTask($vars, &$errors='', $origin='', $autorespond = true, $alertstaff = true) {
        global $thisstaff, $cfg;
        if ($vars["taskAgent"] && !$vars["agent"]) {
            $vars["agent"] = $vars["taskAgent"];
        }
        /*         * ***********  Form fields validations ************ */
        $fields = array();
        $result = array();

        $fields['taskId'] = array('type' => 'int', 'required' => 1, 'error' => __('task Id is required'));
        $fields['taskDepartment'] = array('type' => 'int', 'required' => 0, 'error' => __('Task Department is required'));
        $fields['taskType'] = array('type' => 'array', 'required' => 0, 'error' => __('Select a valid Type'));
        $fields['taskDue'] = array('type' => 'date', 'required' => 0, 'error' => __('Invalid date format - must be MM/DD/YY'));
        $fields['taskComment'] = array('type' => 'string', 'required' => 0, 'error' => __('Invalid task comment'));
        $fields['taskStatus'] = array('type' => 'int', 'required' => 0, 'error' => __('Task task status'));
        $fields['taskResponse'] = array('type' => 'int', 'required' => 0, 'error' => __('Invalid task response'));
        $fields['assignTo'] = array('type' => 'int', 'required' => 0, 'error' => __('Invalid assignee'));
        $fields['attach'] = array('type' => 'array', 'required' => 0, 'error' => __('Invalid attachment'));
        $fields['subtask'] = array('type' => 'array', 'required' => 0, 'error' => __('Invalid subTask Request'));
        $fields['agent'] = array('type' => 'string', 'required' => 1, 'error' => __('Invalid Agent Display Name'));
        $fields['email'] = array('type' => 'string', 'required' => 1, 'error' => __('Invalid Email'));

        $validation = Validator::process($fields, $vars, $errors);

        // $validation =1 means there is no error
        // $errors is array containing fields error

        if ($validation != 1 || sizeof($errors) > 0) {
            $errors[]['err'] = __('Missing or invalid data - check the errors and try again');
            $result['error'] = $errors['err'];
            $output = array("status" => "error", "msg" => $result['error']);
            return $output;
        }
        $posterStaffObj = Staff::lookup($vars['email']);
        if (!$posterStaffObj) {
            global $cfg;
            $emailName = explode("@", $vars["email"])[0];
            $adminAgentConfig = json_decode($cfg->config['adminAgentConfigSingleSignon']['value'], true);
            $defaultPassDec = $adminAgentConfig['password']['decoded']; //'welcome';
            $defaultPassEnc = $adminAgentConfig['password']['encoded']; //; // Setting Password as welcome
            $newStaff = Staff::create();
            $newStaff->username = $vars["email"];
            $newStaff->firstname = ucfirst(explode(".", $emailName)[0]);
            $newStaff->lastname = ucfirst(explode(".", $emailName)[1]);
            $newStaff->isactive = $adminAgentConfig['default_active'];
            $newStaff->created = SqlFunction::NOW();
            $newStaff->permissions = json_encode($priv);
            $newStaff->isadmin = $adminAgentConfig['default_admin'];
            $newStaff->dept_id = $adminAgentConfig['default_dept_id'];
            $newStaff->role_id = $adminAgentConfig['default_role_id'];
            $newStaff->passwd = $defaultPassEnc;
            $newStaff->max_page_size = 50;
            $newStaff->email = $vars["email"];
            $newStaff->save();
        }
        $posterStaffObj = Staff::lookup($vars['email']);
        if (!$posterStaffObj) {
            $errors[] = array("err" => "Invalid Agent");
            return $errors;
        }
        /*         * ******* Ticket updation ********* */
        try {
            $task = Task::lookup($vars["taskId"]);
            $taskdataObj = TaskData::lookup($vars["taskId"]);

            $actionOnClosedTask = $cfg->config['actionOnClosedTask']['value'];
            if (!$task || !$taskdataObj) {
                $errors[] = array("err" => "Task does not Exists");
                return $errors;
            } else if ($task->isClosed()) {
                if (!$actionOnClosedTask) {
                    $errors[] = array("err" => "Task Already Closed");
                    return $errors;
                }
            }
            if (!empty($vars["assignTo"])) {

                $newStaffId = $vars["assignTo"];
                $staffObj = Staff::lookup($newStaffId);
                if ($staffObj && is_numeric($newStaffId)) {
                    if ($task->staff_id != $newStaffId) {
                        $task->staff_id = $newStaffId;
                        $task->save();
                        $task->postNote(array(
                            'note' => "Task Assigned to " . $vars["agent"] . "(" . $vars["email"] . ") by " . $vars["agent"],
                            'title' => __('Task Assigned'),
                                ), $_errors, $posterStaffObj);
                    }
                } else {
                    $errors[] = array("err" => "Such Staff not Exists");
                    return $errors;
                }
            }
            if (!empty($vars["taskDepartment"])) {
                $task->dept_id = $vars["taskDue"];
            }
            if (!empty($vars["taskStatus"])) {
                $taskStatusFormObject = DynamicFormField::lookup(array("name" => "taskStatus"));
                $taskStatusObject = DynamicList::lookup(array("name" => "Task Status", "items__extra" => $vars["taskStatus"]));
                foreach ($taskStatusObject->items as $taskStatusList) {
                    if ($taskStatusList->extra == $vars["taskStatus"]) {
                        $value_json = $taskStatusList->id;
                        $value_string = $taskStatusList->id;
                        $status = $taskStatusList->value;
                        $state = array_values(json_decode($taskStatusList->properties, true))[0] ? "open" : "close";
                        break;
                    }
                }
                if ($value_json) {
                    $dynamicData["taskStatus"] = $value_json;
                    $taskdataObj->taskStatus = $value_string;
                    $task->editDynamicData($dynamicData);
                    $taskdataObj->save();
                    $task->save();
                }
	//	$previousTaskStatus=$task->getTaskStatus();
                if ($state == "close") {
                    $task->setStatus('closed');
                } elseif ($state == "open") {
                    $task->setStatus('open');
                }
            //    $cachedTaskFields=getCachedTaskFields();
 		$task->postNote(array(
                    'note' => "Status Updated to " . $status . "(" . $state . ")",
                    'title' => __('Status Updated'),
                        ), $_errors, $posterStaffObj);
            }
            if (!empty($vars["taskResponse"])) {
                $taskResponseFormObject = DynamicFormField::lookup(array("name" => "taskResponse"));
                $formElement = $taskResponseFormObject->id;

                $taskResponseObject = DynamicList::lookup(array("name" => "Task Response", "items__extra" => $vars["taskResponse"]));
		foreach ($taskResponseObject->items as $taskResponseList) {
                    if ($taskResponseList->extra == $vars["taskResponse"]) {
                        $value_json = $taskResponseList->id;
                        $value_string = $taskResponseList->id;
                        $response = $taskResponseList->value;
                        break;
                    }
                }
                $staticTaskData = getCachedTaskFields();
                $resp_ID = $task->getTaskResponse();
                $response = $staticTaskData['Default_Task Response'][$resp_ID];
                $old = $response ? $response : "null";
                if ($value_json) {
                    $dynamicData["taskResponse"] = $value_json;
                    $taskdataObj->taskResponse = $value_string;
                    $task->editDynamicData($dynamicData);
                    $taskdataObj->save();
                    $task->save();
                }
                $task->postNote(array(
                    'note' => "Task Response from " . $old . " to " . $response,
                    'title' => __('Task Response Updated'),
                        ), $_errors, $posterStaffObj);
                $task->OnResponse($value_json);
            }
            if (!empty($vars["taskComment"])) {
                $taskResponseFormObject = DynamicFormField::lookup(array("name" => "taskComment"));
                $formElement = $taskResponseFormObject->id;

                $dynamicData["taskComment"] = $vars["taskComment"];
                $taskdataObj->taskComment = $vars["taskComment"];

                $task->postNote(array(
                    'note' => $vars["taskComment"],
                    'title' => __('Commented'),
                        ), $_errors, $posterStaffObj);
            }

            if (!empty($vars["attach"])) {
                $numFiles = 0;

                //check for mandatory id
                $type='A';
                $object_id=$vars['taskId'];
 /*
                if($order_id = $task->_answers['order_id']){
                $object_id=$order_id;
                $type='O';
                }else if($manifest_id = $task->_answers['manifest_id']){
                $object_id=$manifest_id;
                $type='M';
                }else if($product_id = $task->_answers['product_id']){
                $object_id=$product_id;
                $type='P';
                }
*/
                foreach ($vars["attach"] as $files) {
                    $key = 'Admin' . time() . rand(1, 99999);
                    $signature = md5('signature');
                    $files["name"] = pathinfo($files["path"], PATHINFO_FILENAME);
                    $fileParam = array("ft" => "T", "bk" => "F", "type" => $files["type"], "size" => $files["size"], "key" => $key, "signature" => $signature, "name" => $files["name"], "path" => $files["path"], "cdn" => 1);
                    $fileParamObj = AttachmentModel::create($fileParam);
                    $fileParamObj->created = SqlFunction::NOW();
                    $fileParamObj->save();
                    if (is_object($posterStaffObj)) {
                        $staffId = $posterStaffObj->getId();
                    } else if (is_object($newStaff)) {
                        $staffId = $newStaff->getId();
                    } else {
                        $staffId = 0;
                    }
                    $ticket_id = $task->ht['object_id'];
                    $attachParam = array("object_id" => $object_id, "file_id" => $fileParamObj->id, "type" => $type, "inline" => 0, "staff_id" => $staffId, "ticket_id" => $ticket_id);
                    $attachParamObj = Attachment::create($attachParam);
                    $attachParamObj->created = SqlFunction::NOW();
                    $attachParamObj->save();
                    //Attachment
                    $numFiles++;
                }
                if ($numFiles > 0) {
                    $task->postNote(array(
                        'note' => $numFiles . " Files Attached",
                        'title' => __('Attached'),
                            ), $_errors, $posterStaffObj);
                }
            }
            if (!empty($vars["subtask"])) {
                $numTasks = 0;
                $taskStaticData = getCachedTaskFields();
                foreach ($vars["subtask"] as $subtask) {
                    $dataTask = array(
                        "object_type" => "T",
                        "object_id" => $task->object_id,
                        "creater" => "System",
                        "internal_formdata" => array(
                            "dept_id" => $subtask["department_id"],
                            "parent_task_id" => $task->id,
                        ),
                        "default_formdata" => array(
                            "taskType" => json_encode($taskStaticData["create"]["Task Types"][$subtask["taskType"]]),
                            "taskStatus" => json_encode($taskStaticData["create"]["Task Status"][$subtask["status"]]),
                            "title" => $taskStaticData["Task Types"][$subtask["taskType"]],
                            "description" => $taskStaticData["Task Types"][$subtask["taskType"]],
                        )
                    );
                    $slaObj = TaskSLA::lookup($data["task_id"]);
                    if ($slaObj && $slaObj->sla_id) {
                        $slaId = $slaObj->sla_id;
                        $dataTask["internal_formdata"]["sla"] = $slaId;
                    }

                    if ($taskData["source"])
                        $dataTask["default_formdata"]["taskSource"] = json_encode($taskStaticData["create"]["Task Sources"][4]);

                    $newSubTask = Task::create($dataTask);
                    //Attachment
                    if ($newSubTask)
                        $numTasks++;
                }
                if ($numTasks > 0) {
                    $task->postNote(array(
                        'note' => $numTasks . " Sub Tasks Created",
                        'title' => __('Sub Task Created'),
                            ), $_errors, $posterStaffObj);
                    $data = array("taskId" => $task->id, "agent" => "On SubTask Creation", "email" => "system.auto@shopclues.com");
                    $newSLA = Task::objects()->filter(array("parent_task_id" => $task->id, "flags" => 1))->order_by("-duedate")->limit(1)->values("duedate")->all();
                    if ($newSLA) {
                        $data["taskDue"] = $newSLA;
                        $task->postNote(array(
                            'note' => "SLA Updated as per subtask - " . $newSLA,
                            'title' => __('SLA Updated'),
                                ), $_errors, $posterStaffObj);
                    }
                    global $cfg;

                    $awaitingResponse = $subtask["response"]? : $cfg->config['awaitingResponseOnSubTask']['value'];
                    if ($awaitingResponse) {
                        $data["taskResponse"] = $awaitingResponse;
                    }
                    $statusChange = $task->updateTask($data, $error);
                }
            }
            if (!empty($vars["taskDue"])) {

                //$task->logEvent('edited', array("duedate"=>$vars["taskDue"]),$posterStaffObj);
                $task->postNote(array(
                    'note' => "SLA Updated from " . $task->duedate . " to " . $vars["taskDue"],
                    'title' => __('SLA Updated'),
                        ), $_errors, $posterStaffObj);
                $task->duedate = $vars["taskDue"];
            }
            if ($dynamicData)
                $task->editDynamicData($dynamicData);
            $taskdataObj->save();
            $task->save();


            $task->loadDynamicData(1);
            $output = array("status" => "success", "msg" => 1);
            return $output;
        } catch (Exception $e) {
            $output = array("status" => "error", "msg" => "exception occured");
            return $output;
        }
    }

    //End UpdateTask
    function update($forms, $vars, &$errors) {
        global $thisstaff;


        if (!$forms || !$this->checkStaffPerm($thisstaff, Task::PERM_EDIT))
            return false;


        foreach ($forms as $form) {
            $form->setSource($vars);
            if (!$form->isValid(function($f) {
                        return $f->isVisibleToStaff() && $f->isEditableToStaff();
                    }, array('mode' => 'edit'))) {
                $errors = array_merge($errors, $form->errors());
            }
        }

        if ($errors)
            return false;

        // Update dynamic meta-data
        $changes = array();
        foreach ($forms as $f) {
            $changes += $f->getChanges();
            $f->save();
        }


        if ($vars['note']) {
            $_errors = array();
            $this->postNote(array(
                'note' => $vars['note'],
                'title' => __('Task Update'),
                    ), $_errors, $thisstaff);
        }

        if ($changes)
            $this->logEvent('edited', array('fields' => $changes));

        Signal::send('model.updated', $this);
        return $this->save();
    }

    /* static routines */

    static function lookupIdByNumber($number) {

        if (($task = self::lookup(array('number' => $number))))
            return $task->getId();
    }

    static function isNumberUnique($number) {
        return !self::lookupIdByNumber($number);
    }

    static function firstTasksToBeMade($data , $orderRelatedData,$ticket) {
        $mandateInfoForTicket=$data["mandateType"];
        switch ($mandateInfoForTicket){
            case 'order':
            case 'manifest':
                $tasksToBeMade=self::getFirstTaskToBeMadeForOrderRelatedInfo($data, $orderRelatedData, $ticket);
                return $tasksToBeMade;
            case 'product':
            case 'batch_update':
            case 'batch_upload':
                $tasksToBeMade=self::getFirstTaskToBeMadeForProductRelatedInfo($data, $orderRelatedData, $ticket);
                return $tasksToBeMade;
        }
    }
    
    /**
     * 
     * @param type $data on the basis of billing status ,return status , order status
     * @param type $orderRelatedData
     * @param type $ticket
     * @return type
     */
    static function getFirstTaskToBeMadeForOrderRelatedInfo($data, $orderRelatedData, $ticket) {
        if(!$data['topicId']){
            return [];
        }
        $topic_id=(int)$data['topicId'];
        $orderInfoMap = $orderRelatedData['orderInfoMap'];
        $orderStatusToIdMap = $orderRelatedData["orderStatusIdMap"];
        $orderStatus = $orderRelatedData['orderStatus'];
        $billingStatus=$orderRelatedData['orderInfoMap']['billing_done'];
        $returnStatus=$orderRelatedData['orderInfoMap']['return_status'];
        $orderIdArray = $orderRelatedData['orderIdsList'];
        $create = $ticket->getOrderStatusFromOrderIds($orderIdArray);
        $i=0;
        $tasksWhichCanBeMade=[];
        $tasksToBeMade=[];
        foreach ($orderInfoMap as $order_id => $orderDetail) {
            $order_status = db_input($orderDetail['order_status'])?:-1;
            $billingStatus = db_input($orderDetail['billing_done'])?:-1;
            $returnStatus = db_input($orderDetail['return_status'])?:-1;
            $sql = "select dept_id,task_type_id,order_status_id,return_status,billing_status from ".TASK_MAPPING_TABLE." where 
                (order_status_id=$order_status or order_status_id=-1) and (return_status=-1 or return_status=$returnStatus) and 
                    (billing_status=-1 or billing_status=$billingStatus) and entity_type='order' and task_status_id=1 and topic_id=".$topic_id;
            $rs=db_query($sql);
            while($row=  db_fetch_array($rs)){
                $tasksWhichCanBeMade[]=$row;
            }
            if(count($tasksWhichCanBeMade)==0){
                        continue;
            }
            $closestTask=self::getClosestFirstTaskToBeMade($tasksWhichCanBeMade,array('order_status_id','return_status','billing_status'));
            $tasksToBeMade[$i]['order_id'] = $order_id;
            $tasksToBeMade[$i]['task_id'] = $closestTask['task_type_id'];
            $tasksToBeMade[$i]['department_id'] = $closestTask['dept_id'];
            $tasksToBeMade[$i]["status"] = 1;
            $tasksToBeMade[$i]['task_mandate_type'] = 'order';
            $tasksToBeMade[$i]['orderInfo'] = $orderInfoMap[$order_id];
            $tasksToBeMade[$i]['createStatus'] = $create[$order_id];
            $i++;
        }
        return $tasksToBeMade;
    }
    
    static function getClosestFirstTaskToBeMade($tasksWhichCanBeMade,$keysToCheck=array()){
        $oveMinimumNumberOfAnyCond=PHP_INT_MAX;
        $taskIndexToBeUsed=0;
        $i=0;
        foreach ($tasksWhichCanBeMade as $tasks){
            $numberOfAny=0;
            foreach ($tasks as $key=>$value){
                if(in_array($key, $keysToCheck)){
                    if($value==-1){
                        $numberOfAny++;
                    }
                }
            }
            if($oveMinimumNumberOfAnyCond>$numberOfAny){
                $oveMinimumNumberOfAnyCond=$numberOfAny;
                $taskIndexToBeUsed=$i;
            }
            $i++;
        }
        return $tasksWhichCanBeMade[$taskIndexToBeUsed];
    }
    
    static function getFirstTaskToBeMadeForProductRelatedInfo($data, $orderRelatedData, $ticket) {
        if (empty($data['topicId'])) {
            return [];
        }
        $topic_id = (int)$data['topicId'];
        switch ($data["mandateType"]) {
            case 'product':
                $product_ids = explode(',', $data['product_ids']);
                $productDetails =
                        CSCartProducts::objects()->filter(array('product_id__in' => $product_ids))->values(array('status', 'product_id'))->all();
                
                $tasksToBeMade = [];
                $i = 0;
                foreach ($productDetails as $productDetail) {
                    $productStatus = ($productDetail['status']);
                    $productId = $productDetail['product_id'];
                    $sql = "select dept_id,task_type_id,product_status,batch_upload_status,batch_update_status from " . TASK_MAPPING_TABLE . " where 
                (product_status='$productStatus' or product_status=-1) and (batch_upload_status=-1) and 
                    ( batch_update_status=-1 ) and entity_type='product' and task_status_id=1 and topic_id=" . $topic_id;
                    $rs = db_query($sql);
                    $taskOptions = [];
                    while ($row = db_fetch_array($rs)) {
                        $taskOptions[] = $row;
                    }
                    if(count($taskOptions)==0){
                        continue;
                    }
                    $closestTask = self::getClosestFirstTaskToBeMade($taskOptions, array('product_status', 'batch_upload_status', 'batch_update_status'));
                    $tasksToBeMade[$i]['product_id'] = $productId;
                    $tasksToBeMade[$i]['task_id'] = $closestTask['task_type_id'];
                    $tasksToBeMade[$i]['department_id'] = $closestTask['dept_id'];
                    $tasksToBeMade[$i]["status"] = 1;
                    $tasksToBeMade[$i]['orderInfo'] = [];
                    $tasksToBeMade[$i]['task_mandate_type'] = 'product';
                    $tasksToBeMade[$i]['createStatus'] = $productStatus;
		    $i++;
                }
                return $tasksToBeMade;
                break;
            case 'batch_upload':
            case 'batch_update':
                $batch_ids = explode(',', $data['batch_ids']);
                
                $bulkUploads=
                        BulkBatchUpload::objects()->filter(array('batch_id__in'=>$batch_ids))->values(array('batch_id','status'))->all();
                $i=0;
                foreach ($bulkUploads as $bulkUpload){
                    $batch_id=$bulkUpload['batch_id'];
                    $taskOptions = [];
                    $batch_status=$bulkUpload['status'];
                    $sql = "select dept_id,task_type_id,product_status,batch_upload_status,batch_update_status from " . TASK_MAPPING_TABLE . " where 
                (product_status=-1) and (batch_upload_status=-1 or batch_upload_status='$batch_status') and 
                    ( batch_update_status=-1 ) and task_status_id=1 and entity_type='product' and topic_id=" . $topic_id;
		    $rs=db_query($sql);
                    while($row=  db_fetch_array($rs)){
                        $taskOptions[]=$row;
                    }
                    if(count($taskOptions)==0){
                        continue;
                    }
                    $closestTask = self::getClosestFirstTaskToBeMade($taskOptions, array('product_status', 'batch_upload_status', 'batch_update_status'));
                    $tasksToBeMade[$i]['batch_id'] = $batch_id;
                    $tasksToBeMade[$i]['task_id'] = $closestTask['task_type_id'];
                    $tasksToBeMade[$i]['department_id'] = $closestTask['dept_id'];
                    $tasksToBeMade[$i]["status"] = 1;
                    $tasksToBeMade[$i]['orderInfo'] = [];
                    $tasksToBeMade[$i]['task_mandate_type'] = 'batch';
                    $tasksToBeMade[$i]['createStatus'] = $batch_status;
		    $i++;
                }
//		var_dump($tasksToBeMade);die;
                return $tasksToBeMade;
                break;
        }
    }

    static function getOrderInfoMap($data) {
        if ($data["isReturn"] && $data["order_id"] && $data["return_id"]) {
            
        } else {

            $manifestDetailsOrderIdMap = [];
            $manifestOrderIdMap = [];
            $carrierOrderIdDetailMap = [];
            $carrierOrderIdMap = [];
            $orderIdToManifestIdMap = [];
            $orderIdArray = [];
            if (empty($data['order_ids']) && empty($data['manifest_ids'])) {
                return [];
            }
            if (!empty($data['manifest_ids'])) {
                $manifestIdArray = explode(',', $data['manifest_ids']);
                $numberOfManifestId = count($manifestIdArray);
                if ($numberOfManifestId == 0) {
                    return [];
                }

                $commaSeperatedManifestId = UtilityFunctions::getInClauseFromArray($manifestIdArray);
                $sql = "select order_id,manifest_id,awbno as tracking_no from " . CLUES_ORDER_MANIFEST_DETAILS_TABLE . " where manifest_id in ($commaSeperatedManifestId) 
                        group by manifest_id limit $numberOfManifestId";

                $res = db_query($sql);

                while ($result = db_fetch_array($res)) {
                    $manifestDetailsOrderIdMap[$result['order_id']] = $result;
                    $orderIdArray[] = $result['order_id'];
                    $orderIdToManifestIdMap[$result['manifest_id']] = $result['order_id'];
                    $manifestIdToOrderIdMap[$result['order_id']] = $result['manifest_id'];
                }

                $sql = "SELECT carrier_id,carrier_name,manifest_type_id,date_created,done,dispatch_date,expected_pickup_date,manifest_id 
                        FROM  " . CLUES_ORDER_MANIFEST_TABLE . " where manifest_id in ($commaSeperatedManifestId)";
                $res1 = db_query($sql);
                while ($result = db_fetch_array($res1)) {
                    $orderId = $orderIdToManifestIdMap[$result['manifest_id']];
                    $manifestOrderIdMap[$orderId] = $result;
                    $carrierOrderIdMap[$result['carrier_id']] = $result['order_id'];
                    $careerId[] = $result['carrier_id'];
                }

                $data['order_ids'] = implode(',', $orderIdArray);
                $carrierLookupDetails = CareerLookup::objects()->filter(array('carrier_id__in' => $careerId))->values('status', 'carrier_id')->all();
                foreach ($carrierLookupDetails as $carrierDetail) {
                    $orderId = $carrierOrderIdMap[$carrierDetail['carrier_id']];
                    $carrierOrderIdDetailMap[$orderId] = $carrierDetail;
                }
            }
            $orderIds = $data['order_ids'];
            $orderIdToStatusMap = [];
            $orderStatusToIdMap = [];
            $orderStatus = [];
            $orderIdsArray = explode(',', $orderIds);
            $orderInfoMap = [];

            //Creating Tasks
            if (!$data["task"] && !$data["ord_status"]) {
                $ordStatusArray = OrderReportingMaster::objects()->filter(array("order_id__in" => $orderIdsArray))->values()->all();
                $uniqueOrdStatus=[];
                foreach ($ordStatusArray as $orderValues){
                    $uniqueOrdStatus[$orderValues['order_id']]=$orderValues;
                }
                $companyIds = [];
                $orderCompanyIdMap = [];
                foreach ($uniqueOrdStatus as $orderValues) {
                    $companyIds[$orderStatusValues['order_id']] = $orderValues['company_id'];
                    $orderCompanyIdMap[$orderValues['company_id']] = $orderValues['company_id'];
                }
                $commaSeperatedcompanyIds = UtilityFunctions::getInClauseFromArray($companyIds);
                if ($commaSeperatedcompanyIds) {
                    $warehouseContactDetails = [];
                    $sql = "SELECT warehouse_state as o_mer_state,warehouse_pcontact_phone as o_merchant_phone,company_id FROM clues_warehouse_contact WHERE company_id in ($commaSeperatedcompanyIds);";
                    $res1 = db_query($sql);
                    while ($result = db_fetch_array($res1)) {
                        $company_id = $result['company_id'];
                        $warehouseContactDetails[$orderCompanyIdMap[$company_id]] = $result;
                    }
                    $csCartCompanies = [];
                    $sql = "SELECT phone as o_merchant_bphone,company_id FROM cscart_companies WHERE company_id in ($commaSeperatedcompanyIds)";
                    $res1 = db_query($sql);
                    while ($result = db_fetch_array($res1)) {
                        $companyId = $result['company_id'];
                        $csCartCompanies[$orderCompanyIdMap[$companyId]] = $result;
                    }
                }
                foreach ($uniqueOrdStatus as $orderStatusValues) {
                    if (is_array($carrierOrderIdDetailMap[$orderStatusValues['order_id']]) && count($carrierOrderIdDetailMap[$orderStatusValues['order_id']]) > 0) {
                        $orderStatusValues+=$carrierOrderIdDetailMap[$orderStatusValues['order_id']];
                    }
                    if (is_array($manifestDetailsOrderIdMap[$orderStatusValues['order_id']]) &&
                            count($manifestDetailsOrderIdMap[$orderStatusValues['order_id']]) > 0) {
                        $orderStatusValues+=$manifestDetailsOrderIdMap[$orderStatusValues['order_id']];
                    }
                    if (is_array($manifestOrderIdMap[$orderStatusValues['order_id']]) &&
                            count($manifestOrderIdMap[$orderStatusValues['order_id']]) > 0) {
                        $orderStatusValues+=$manifestOrderIdMap[$orderStatusValues['order_id']];
                    }
                    if (is_array($warehouseContactDetails[$orderStatusValues['order_id']]) &&
                            count($warehouseContactDetails[$orderStatusValues['order_id']]) > 0) {
                        $orderStatusValues+=$warehouseContactDetails[$orderStatusValues['order_id']];
                    }
                    if (is_array($csCartCompanies[$orderStatusValues['order_id']]) &&
                            count($csCartCompanies[$orderStatusValues['order_id']]) > 0) {
                        $orderStatusValues+=$csCartCompanies[$orderStatusValues['order_id']];
                    }
                    $orderIdToStatusMap[$orderStatusValues['order_id']] = $orderStatusValues['order_status'];
                    $orderStatusToIdMap[$orderStatusValues['order_status']][] = $orderStatusValues['order_id'];
                    $orderStatus[] = $orderStatusValues['order_status'];
                    $orderInfoMap[$orderStatusValues['order_id']] = $orderStatusValues;
                    $manifestInfoMap[$manifestIdToOrderIdMap[$orderStatusValues['order_id']]] = $orderInfoMap;
                }
            }
        }
      
        return array('orderInfoMap'=>$orderInfoMap,'orderStatusIdMap'=>$orderStatusToIdMap,'manifestInfoMap'=>$manifestInfoMap,'orderStatus'=>$orderStatus,'orderIdsList'=>$orderIdsArray);
    }

    static function create($vars = false) {
        global $thisstaff, $cfg, $__send_to_master;

        if (!is_array($vars) || !$thisstaff || !$thisstaff->hasPerm(Task::PERM_CREATE, false)) {
            if (!($vars["creater"] && $vars["creater"] == "System")) {
                return null;
            }
        }
        if($vars['task_mandate_type']=="batch_upload" || $vars['task_mandate_type']=="batch_update"){
            $vars['task_mandate_type']="batch";
        }
        $taskMandateType=$vars['task_mandate_type'];
        
        $mandateColumnName=$taskMandateType."_id";
        
        $task = parent::create(array(
                    'flags' => self::ISOPEN,
                    'object_id' => $vars['object_id'],
                    'object_type' => $vars['object_type'],
                    'number' => $cfg->getNewTaskNumber(),
                    'created' => new SqlFunction('NOW'),
                    'updated' => new SqlFunction('NOW'),
        ));

        /**
         * To prevent creation of duplicate task
         */
        $__send_to_master = TRUE;

        $ticketOpenCount = Ticket::objects()->filter(array("ticket_id" => $vars['object_id'], "status__state" => "open"))->count();
        if ($ticketOpenCount < 1) {
            return "Ticket is Closed";
        }

        $taskOfSameTypeFilter = array(
            'object_type' => $vars['object_type'],
            'object_id' => $vars['object_id'],
            'flags' => 1,
            'dept_id' => $vars['internal_formdata']['dept_id'],
            'mandate_info'=>$taskMandateType
        );
        if($taskMandateType=="batch_upload" || $taskMandateType=="batch_update"){
            $mandateColumnName="batch_id";
        }
	if(!empty($taskMandateType)){
           $taskOfSameTypeFilter['data__'.$mandateColumnName]=$vars[$mandateColumnName];
        }
        if (!empty($vars["manifest_id"])) {
            $taskOfSameTypeFilter["data__manifest_id"] = $vars["manifest_id"];
        }
        $taskOfSameType = Task::objects()->filter($taskOfSameTypeFilter)->values('id', 'data__taskType')->all();
        foreach ($taskOfSameType as $taskData) {
            $type = (int) $taskData["data__taskType"];
            if ($type == $vars['default_formdata']["taskType"]) {
                return "Task Already Exists";
            }
        }

        $__send_to_master = FALSE;
        //ED TIckets Task
        $TicketDeptArr = Ticket::objects()->filter(array("ticket_id" => $vars['object_id']))->values("dept_id")->all()[0];
        $TicketTaskManipulation = json_decode($cfg->config['TicketTaskDeptManipulation']['value'], true);
        $taskManipulation = $TicketTaskManipulation[$TicketDeptArr["dept_id"]];

        if ($taskManipulation) {
            if ($taskManipulation[$vars['internal_formdata']['dept_id']]) {
                $vars['internal_formdata']['dept_id'] = $taskManipulation[$vars['internal_formdata']['dept_id']];
            }
        }
        //**Task Manipulation End**

        if ($vars['internal_formdata']['dept_id'])
            $task->dept_id = $vars['internal_formdata']['dept_id'];

        // if ($vars['internal_formdata']['duedate']){
        //     $task->duedate = date('Y-m-d G:i:s',strtotime($vars['internal_formdata']['duedate'])); // Edited - Bug fix - Akash Kumar
        // }  
        $taskType = is_array($vars['default_formdata']['taskType']) ? $vars['default_formdata']['taskType'] : json_decode($vars['default_formdata']['taskType'], true);
        $value = key($taskType);
        $alltasktype = getCachedTaskFields()["id_extra_Task Types"];
        $taskTypeId = $alltasktype[$value];
        $taskSlaObj = TaskSLA::lookup($taskTypeId);
        $api_sla_flag = $taskSlaObj->api_sla;
        $ed_sla = $taskSlaObj->ed_sla;
        if (!$task->duedate) {
            $ticketDeptId = Ticket::objects()->filter(array('ticket_id' => $vars['object_id']))->values('dept_id')->all()[0]['dept_id'];
            $staticFields = getCachedTicketFields();
            if ($vars['internal_formdata']['sla']) {
                $sla_id = $vars['internal_formdata']['sla'];
                $sla = SLA::lookup(array("id" => $sla_id));
                $grace = $sla->getGracePeriod();
                $task->duedate = date('Y-m-d G:i:s', Misc::dbtime(time()) + $grace * 60 * 60);
            } else if ($vars['internal_formdata']['dept_id']) {
                $dep = Dept::lookup(array("id" => $vars['internal_formdata']['dept_id']));
                $slaId = $dep->getSLAId();
                if (!$slaId) {
                    $slaId = $cfg->config['default_task_sla_id']['value'];
                }
                if ($slaId) {
                    $sla = SLA::lookup(array("id" => $slaId));
                    $grace = $sla->getGracePeriod();
                    $task->duedate = date('Y-m-d G:i:s', Misc::dbtime(time()) + $grace * 60 * 60);
                }
            } else {
                $sla_id = $cfg->config['default_task_sla_id']['value'];
                $sla = SLA::lookup(array("id" => $sla_id));
                $grace = $sla->getGracePeriod();
                $task->duedate = date('Y-m-d G:i:s', Misc::dbtime(time()) + $grace * 60 * 60);
            }
        }

        if ($vars['internal_formdata']['assignee']->staff_id)
            $task->staff_id = $vars['internal_formdata']['assignee']->staff_id;
        if ($task->duedate) {
            $task->promised_duedate = $task->duedate; // Edited - Bug fix - Akash Kumar
        }
        $task->parent_task_id = $vars['internal_formdata']["parent_task_id"] ? $vars['internal_formdata']["parent_task_id"] : 0;
        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D2"];
        $module = $logConfig["module"][$domain]["D2_M1"];
        $error = $logConfig["error"]["E2"];
        $level = $logConfig["level"]["INFO"];
        $logObj = new Analog_logger();


        $detailsToLog = array('vars' => $vars, "task-objectId" => $task->object_id, "taskinfo" => $task);
        $ticket = Ticket::lookup($task->object_id);
        $task->mandate_info=$ticket->ht['mandate_info'];
        if (!$task->save(true)) {
            return false;
        }
        //for task creation status entry
        if ($thisstaff)
            $vars['default_formdata']['createdBy'] = $thisstaff->getId();
//             
        // Add dynamic data

        $task->addDynamicData($vars['default_formdata']);

        //task data entering
        // Create a thread + message.
        $thread = TaskThread::create($task);
        $thread->addDescription($vars);
        // For ticket Odata updation
        $dynamicFormEntries = $vars['default_formdata'];
        //navneet
        $taskdataObj = TaskData::create();
        $taskdataObj->$mandateColumnName=$vars[$mandateColumnName];
        
        //$taskdataObj->order_id = $vars['order_id'];
        if($vars['extra']){
            $taskdataObj->extra=$vars['extra'];
        }
        $taskdataObj->task_id = $task->getId();
        $taskBasicDetails=  json_decode($cfg->config['task_data_basic_details']['value'],1);
        if(!empty($vars['previousData']) && is_array($vars['previousData']) && count($vars['previousData'])>0){
            foreach ($vars['previousData']  as $key=>$taskPreviousData){
                foreach ($taskPreviousData as $columnName=>$data){
                    if(in_array($columnName, $taskBasicDetails)){
                        continue;
                    }
                    $taskdataObj->$columnName=$data;
                }
            }
        }else{
        $taskdataObj->ticket_id = $vars['ticket_id']?:$vars['object_id'];
        $taskdataObj->call_no = $vars['orderInfo']['phone'];
        $taskdataObj->payment_id = $vars['orderInfo']['payment_id'];
        $taskdataObj->refund_id = $vars['orderInfo']['refund_id'];
        $taskdataObj->refund_date = $vars['orderInfo']['refund_date'];
        $taskdataObj->refund_status = $vars['orderInfo']['refund_status'];
        $taskdataObj->ord_hist_from = $vars['orderInfo']['from_status'];
        $taskdataObj->ord_hist_to = $vars['orderInfo']['to_status'];
        $taskdataObj->ord_hist_date = $vars['orderInfo']['transition_date'];
        $taskdataObj->price_total = $vars['orderInfo']['order_total'];
        $taskdataObj->price_sub_total = $vars['orderInfo']['subtotal'];
        $taskdataObj->price_discount = $vars['orderInfo']['discount'];
        $taskdataObj->price_sub_dis = $vars['orderInfo']['subtotal_discount'];
        $taskdataObj->price_ship_cost = $vars['orderInfo']['shipping_cost'];
        $taskdataObj->ord_date = $vars['orderInfo']['order_date'];
        $taskdataObj->ord_status = $vars['orderInfo']['order_status'];
        $taskdataObj->ord_fname = $vars['orderInfo']['firstname'];
        $taskdataObj->ord_lname = $vars['orderInfo']['lastname'];
        $taskdataObj->ord_company = $vars['orderInfo']['company_name'];
        $taskdataObj->ord_phone = $vars['orderInfo']['phone'];
        $taskdataObj->ord_bphone = $vars['orderInfo']['b_phone'];
        $taskdataObj->ord_sphone = $vars['orderInfo']['s_phone'];
        $taskdataObj->ord_email = $vars['orderInfo']['email'];
        $taskdataObj->price_gift_charges = $vars['orderInfo']['gifting_charge'];
        $taskdataObj->price_cb_used = $vars['orderInfo']['cb_used'];
        $taskdataObj->price_gc_used = $vars['orderInfo']['gc_used'];
        $taskdataObj->price_cod_fee = $vars['orderInfo']['cod_fee'];
        $taskdataObj->rma_hist_from = $vars['orderInfo']['rma_from_status'];
        $taskdataObj->rma_hist_to = $vars['orderInfo']['rma_to_status'];
        $taskdataObj->rma_hist_date = $vars['orderInfo']['rma_transition_date'];
        $taskdataObj->ord_prod_id = $vars['orderInfo']['product_id'];
        $taskdataObj->ord_prod_name = $vars['orderInfo']['product_name'];
        $taskdataObj->retrn_status = $vars['orderInfo']['return_status'];
        $taskdataObj->ship_id = $vars['orderInfo']['shipment_id'];
        $taskdataObj->ship_carrier = $vars['orderInfo']['shipment_carrier_name'];
        $taskdataObj->ship_date = $vars['orderInfo']['shipment_timestamp'];
        $taskdataObj->ship_dlvry_date = $vars['orderInfo']['delivery_date'];
        $taskdataObj->ship_status = $vars['orderInfo']['shipment_status'];
        $taskdataObj->ship_type = $vars['orderInfo']['shipment_type'];
        $taskdataObj->ship_track_id = $vars['orderInfo']['tracking_number'];
        $taskdataObj->merchant = $vars['orderInfo']['company_id'];
        $taskdataObj->billed = $vars['orderInfo']['billing_done'];
        $taskdataObj->merchant_type = $vars['orderInfo']['fulfillment_id'];
        $taskdataObj->merchant_state_id = $vars['orderInfo']['o_mer_state'];
        $taskdataObj->merchant_phone = $vars['orderInfo']['o_merchant_phone'];
        $taskdataObj->merchant_sphone = $vars['orderInfo']['o_merchant_bphone'];
        $taskdataObj->category = $vars['orderInfo']['meta_category_id'];
        $taskdataObj->retrn_id = $vars['orderInfo']['return_id'];
        $taskdataObj->retrn_date = $vars['orderInfo']['return_timestamp'];
        $taskdataObj->retrn_status = $vars['orderInfo']['return_status'];

        if (isset($vars['orderInfo']['manifest_id']) && $vars['orderInfo']['manifest_id'] > 0) {
            $taskdataObj->manifest_id = $vars['orderInfo']['manifest_id'];
            $taskdataObj->tracking_no = $vars['orderInfo']['tracking_no'];
            $taskdataObj->carrier_id = $vars['orderInfo']['carrier_id'];
            $taskdataObj->carrier_name = $vars['orderInfo']['carrier_name'];
            $taskdataObj->manifest_type_id = $vars['orderInfo']['manifest_type_id'];
            $taskdataObj->manifest_created = $vars['orderInfo']['date_created'];
            $taskdataObj->pickup_done = $vars['orderInfo']['done'];
            $taskdataObj->dispatch_date = $vars['orderInfo']['dispatch_date'];
            $taskdataObj->expected_pickup_date = $vars['orderInfo']['expected_pickup_date'];
            $taskdataObj->carrier_status = $vars['orderInfo']['status'];
        }
    }
    if (!empty($vars['default_formdata']['createStatus'])) {
            switch ($taskMandateType) {
                case 'product':
                    $taskdataObj->product_status=$vars['default_formdata']['createStatus'];
                    break;
                case 'batch':
                    $taskdataObj->batch_status=$vars['default_formdata']['createStatus'];
                    break;
            }
    }
    
    foreach ($dynamicFormEntries as $k => $entries) {
            if ($k!="extra" && json_decode($entries)) {
                $entries = json_decode($entries, true);
            }
            $entries = is_array($entries) ? key($entries) : $entries;
            $taskdataObj->$k = $entries;
        }
        $taskdataObj->save();

        if (!($vars["creater"] && $vars["creater"] == "System")) {
            $task->logEvent('created', null, $thisstaff);
            // Get role for the dept
            $role = $thisstaff->getRole($task->dept_id);
        } else {
            $role = 1;
        }
        // Assignment
        if ($vars['internal_formdata']['assignee']
                // skip assignment if the user doesn't have perm.
                && ($role == 1 || ($role && $role->hasPerm(Task::PERM_ASSIGN)))) {
            $_errors = array();
            $form = AssignmentForm::instantiate(array(
                        'assignee' => $vars['internal_formdata']['assignee']));
            $task->assign($form, $_errors);
        }
        // Log Info for BulkJob Error
        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D2"];
        $module = $logConfig["module"][$domain]["D2_M1"];
        $error = $logConfig["error"]["E2"];
        $level = $logConfig["level"]["INFO"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $level, $error, "title:Task Created", "ticker_id:".$ticket->getId(),"log_location:class.task.php", "task:" . $task->getId(), "");

        Signal::send('task.created', $task);

//Author:navneet --attaching taskdata info to task object
        // if (!isset($task->_answers)) {
            $task->_answers = array();

            $data = TaskData::lookup($task->getId());
            foreach ($data->ht as $key => $value) {
                $keyS = mb_strtolower($key);
                if ($keyS == "taskcomment") {
                    $task->_answers[$keyS] = $value;
                } else {
                    $task->_answers[$keyS] = stristr($value, ",") ? explode(",", $value)[1] : $value;
                }
                $task->_answersId[$keyS] = stristr($value, ",") ? explode(",", $value)[0] : $value;
            }
       // }
         //End here
        return $task;
    }

    function updateTaskWithOrderParameters() {
        
    }

    function delete($comments = '') {
        global $ost, $thisstaff;

        $thread = $this->getThread();

        if (!parent::delete())
            return false;

        $thread->delete();

        Draft::deleteForNamespace('task.%.' . $this->getId());

        foreach (DynamicFormEntry::forObject($this->getId(), ObjectModel::OBJECT_TYPE_TASK) as $form)
            $form->delete();

        // Log delete
        $log = sprintf(__('Task #%1$s deleted by %2$s'), $this->getNumber(), $thisstaff ? $thisstaff->getName() : __('SYSTEM'));

        if ($comments)
            $log .= sprintf('<hr>%s', $comments);

        $ost->logDebug(
                sprintf(__('Task #%s deleted'), $this->getNumber()), $log);

        return true;
    }

    static function __loadDefaultForm() {

        require_once INCLUDE_DIR . 'class.i18n.php';

        $i18n = new Internationalization();
        $tpl = $i18n->getTemplate('form.yaml');
        foreach ($tpl->getData() as $f) {
            if ($f['type'] == ObjectModel::OBJECT_TYPE_TASK) {
                $form = DynamicForm::create($f);
                $form->save();
                break;
            }
        }
    }

    /* Quick staff's task stats */

    function getTaskStats($staff,$getResultForStaff=false) {
        global $cfg;
        $priorityMapping = $GLOBALS['priorityMapping'];

        $communicationMapping = $GLOBALS['communicationMapping'];
        $edDepartmentId = $cfg->getEDDepartmentId();
//print_r($staff->getAgents());die;
        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;

        if (!$output) {

            //$where = array('(task.staff_id='.db_input($staff->getId()) .')');
        //    $where = array('(ticket.staff_id IN ("' . implode('","', $staff->getAgents()) . '"))');
            $where2 = '';

            $depts = $staff->getDepts();
            /* foreach ($depts as $dep_id) {
              $role = $staff->getRole($dep_id);
              if ($role->hasPerm(TicketModel::FULL_DEPT))
              $where[] = 'ticket.dept_id = "' . $dep_id . '" ';
              } */

            if (($depts = $staff->getDepts())) //Staff with limited access just see Assigned tickets.
                $condition[] = 'task.dept_id IN(' . implode(',', db_input($depts)) . ') ';
            /*  if ($staff->showAssignedOnly()){
              $condition[] = 'ticket.staff_id = "'.$staff->getId().'" ';
              }else{
              $condition[] = '(ticket.dept_id IN ('.implode(',', db_input($depts)).'))';
              } */

            if ($condition)
                $where[] = implode(' AND ', $condition);
            
            if ($cfg && ($cfg->config['filterOnAdvance']['value']) && $_SESSION['advsearch']) {
                $where3 = ' AND ' . getAdvanceSearchQuery(false) . ($condition ? (" AND " . implode(' AND ', $condition)) : "");
                $where3 = str_replace('staff_id IN', 'ticket.staff_id IN', $where3);
            }

            if (!$cfg || !($cfg->showAssignedTickets() || $staff->showAssignedTickets()))
                $where2 = ' AND (task.staff_id=0 OR task.team_id=0)';
            $where = implode(' AND ', $where);
            if ($where)
                $where = 'AND ( ' . $where . ' ) ';
            if ($staff->checkIfLoginUserOfPrimaryCCGDepartment()) {
                $departmentIdsNotBeShownOnCCG = $cfg->getDepartmentsNotToShowOnCCg();
                $mapping = json_decode($cfg->config['customerUpdateFilters']['value'], true);
           //     $where.=" And ticket.dept_id not in ($departmentIdsNotBeShownOnCCG) ";
            } else if ($staff->checkIfLoginUserOfEDDepartment()) {
                $mapping = json_decode($cfg->config['taskRespondEDFilter']['value'], true);
                $staffPrimaryDepartmentId = $staff->getDeptId();
             //   $where.=" And ticket.dept_id in ($staffPrimaryDepartmentId) ";
            } else {
                $mapping = json_decode($cfg->config['customerUpdateFilters']['value'], true);
                $staffPrimaryDepartmentId = $staff->getDeptId();
             //   $where.=" And ticket.dept_id in ($staffPrimaryDepartmentId) ";
            }

            /*     foreach($mapping as $map){
              $time = explode("_",$map["range"]);
              $gt = 'now() + interval [X] hour';
              if($time[0]<=0)
              $gt = str_replace("+","-",$gt);
              $gt = str_replace("[X]",abs($time[0]),$gt);

              $lt = 'now() + interval [X] hour';
              if($time[1]<=0)
              $lt = str_replace("+","-",$lt);

              $lt = str_replace("[X]",abs($time[1]),$lt);


              $sqlCondition[] = 'sum(task.duedate > ('.$gt.') and  task.duedate < ('.$lt.')) `'.$map["label"].'`';
              } */
            $sql = 'SELECT task.duedate,task.object_id,task.id ';
            $sql .= implode(", ", $sqlCondition);
            $sql .= 'FROM ' . TASK_TABLE . ' task JOIN '.TASK_DATA_TABLE.' taskdata on taskdata.task_id=task.id ' 
                    . 'LEFT JOIN  ' . TICKET_TABLE . ' ticket '
                    . ' ON ticket.ticket_id=task.object_id ';
            if ($where3) {
                $sql .= ' LEFT JOIN ' . TICKET_ODATA_TABLE . ' odata ON ticket.ticket_id=odata.ticket_id ';
                $sql .= ' LEFT JOIN ' . TRIAGE_TABLE . ' triage ON ticket.ticket_id=triage.ticket_id ';
            }

            $sql .= ' WHERE task.flags=1 ';
            if($getResultForStaff){
                global $thisstaff;
                $staff_id=$thisstaff->getId();
                $sql .= ' And task.staff_id= '.$staff_id;    
            }else{
                $sql .= ' And task.staff_id=0 ';    
            }
            
            
            if ($where3) {
                $sql .= $where3;
            } else {
                $staticFields = getCachedTicketFields();
                if (count($staticFields["state"]["open"]) > 0)
                    $sql .= " AND ticket.status_id IN ('" . implode("','", $staticFields["state"]["open"]) . "')";

                $sql .= $where . $where2;
            }
            $res = db_query($sql);
            $presentTime = time();
            $indexForTrack = 1;

            /*             * ****************************adding count inside mapping data like count of tickets responded delayed******************* */




            while ($row = db_fetch_array($res)) {
                $dueDateTime = strtotime($row['duedate']);
                $timeRange = explode("_", $mapping[$indexForTrack]["range"]);
                $diffInHour = floor(($dueDateTime - $presentTime) / 3600);
                $countData[$diffInHour]["count"] ++;
                if ($row['id'])
                    $countData[$diffInHour]['taskIdInCsv'].= $row['id'] . ',';
            }

            foreach ($mapping as $k => $val) {
                $timeRange = explode("_", $val["range"]);
                for ($i = $timeRange[0]; $i < $timeRange[1]; $i++) {
                    $mapping[$k]['count'] += $countData[$i]["count"];
                    if ($countData[$i]['taskIdInCsv'])
                        $mapping[$k]['taskIdInCsv'] .= trim($countData[$i]['taskIdInCsv'], ",") . ',';
                }
            }
            /*             * *********************************************END adding count**************************************************************** */




            $stats = array();
            $formattedTriageData = array();
            /*             * ********************************START Format mapping data PLUS getting triage data***************************************** */
            foreach ($mapping as $k => $v) {
                $mapping[$k]['taskIdInCsv'] = trim($v['taskIdInCsv'], ',');
                if (!empty($mapping[$k]['taskIdInCsv'])) {
                    $triageDataTask = $this->getTriageDataForTaskFilters($mapping[$k]['taskIdInCsv']);
                    $mapping[$k]['triageData'] = $triageDataTask;
                }


                if (empty($v['count']))
                    $count = 0;
                else
                    $count = $v['count'];
                $stats[$v['label']] = array('id' => $k, 'count' => $count);
            }
            /*             * ********************************END Format mapping data PLUS getting triage data******************************************** */

            //  echo "<pre>"; print_r($mapping); die;

            /*             * *********************************START Format triage data******************************************************************* */
            $priorityName = 'priority';
            $communName = 'communication';
            foreach ($mapping as $key => $data) {
                $index = $data['label'];
                if (!empty($data['count']))
                    $formattedTriageData[$index] = $data['count'];
                else
                    $formattedTriageData[$index] = 0;
                foreach ($priorityMapping as $pr) {
                    $indexPr = $data['label'] . '-' . $priorityName . '[' . $pr . ']';
                    if (!empty($data['triageData'][$pr]))
                        $formattedTriageData[$indexPr] = $data['triageData'][$pr]['count'];
                    else
                        $formattedTriageData[$indexPr] = 0;

                    foreach ($communicationMapping as $cm) {
                        $indexCm = $data['label'] . '-' . $priorityName . '[' . $pr . ']-' . $communName . '[' . $cm . ']';
                        if (!empty($data['triageData'][$pr][$cm]))
                            $formattedTriageData[$indexCm] = $data['triageData'][$pr][$cm]['count'];
                        else
                            $formattedTriageData[$indexCm] = 0;
                    }
                    $indexCall = $indexCm = $data['label'] . '-' . $priorityName . '[' . $pr . ']-' . $communName . '[call]';
                    if (!empty($data['triageData'][$pr]['callCustomer']))
                        $formattedTriageData[$indexCall] = $data['triageData'][$pr]['callCustomer'];
                    else
                        $formattedTriageData[$indexCall] = 0;
                }
            }

            /*             * *********************************END Format triage data******************************************************************* */

            $output = array('main' => $stats, 'priority' => $formattedTriageData);
        }
        return $output;
    }

    /* Input => a csv string containing task id's
     * Output => array containing triage data corrospond to task id's comes in csv */

    private function getTriageDataForTaskFilters($idCsv) {
        $priorityMapping = $GLOBALS['priorityMapping'];
        $communicationMapping = $GLOBALS['communicationMapping'];
        $querygetTriageDataForTask = 'select a.priority , a.communication , a.call_customer , a.ticket_id ,b.object_id,b.id,count(*) as cnt from mst_triage_data a , mst_task b where a.ticket_id = b.object_id and b.id in (' . $idCsv . ')  group by a.priority,a.communication,a.call_customer';
        $resTriageDataForTask = db_query($querygetTriageDataForTask);
        $returnArray = array();
        $presentPriority = $presentCommunication = 0;
        while ($row = db_fetch_array($resTriageDataForTask)) {
            if (empty($returnArray[$priorityMapping[$row['priority']]]['count']))
                $returnArray[$priorityMapping[$row['priority']]]['count'] = $row['cnt'];
            else
                $returnArray[$priorityMapping[$row['priority']]]['count'] = $returnArray[$priorityMapping[$row['priority']]]['count'] + $row['cnt'];
            if (empty($returnArray[$priorityMapping[$row['priority']]][$communicationMapping[$row['communication']]]['count']))
                $returnArray[$priorityMapping[$row['priority']]][$communicationMapping[$row['communication']]]['count'] = $row['cnt'];
            else
                $returnArray[$priorityMapping[$row['priority']]][$communicationMapping[$row['communication']]]['count'] = $returnArray[$priorityMapping[$row['priority']]][$communicationMapping[$row['communication']]]['count'] + $row['cnt'];
            if ($row['call_customer'] == 2)
                $returnArray[$priorityMapping[$row['priority']]]['callCustomer'] = $row['cnt'];
        }
        return $returnArray;
    }

    /* Quick staff's stats */

    static function getStaffStats($staff) {
        global $cfg;

        /* Unknown or invalid staff */
        if (!$staff || (!is_object($staff) && !($staff = Staff::lookup($staff))) || !$staff->isStaff())
            return null;

        $where = array('(task.staff_id=' . db_input($staff->getId())
            . sprintf(' AND task.flags & %d != 0 ', TaskModel::ISOPEN)
            . ') ');
        $where2 = '';

        if (($teams = $staff->getTeams()))
            $where[] = ' ( task.team_id IN(' . implode(',', db_input(array_filter($teams)))
                    . ') AND '
                    . sprintf('task.flags & %d != 0 ', TaskModel::ISOPEN)
                    . ')';

        if (!$staff->showAssignedOnly() && ($depts = $staff->getDepts())) //Staff with limited access just see Assigned tasks.
            $where[] = 'task.dept_id IN(' . implode(',', db_input($depts)) . ') ';

        $where = implode(' OR ', $where);
        if ($where)
            $where = 'AND ( ' . $where . ' ) ';

        $sql = 'SELECT \'open\', count(task.id ) AS tasks '
                . 'FROM ' . TASK_TABLE . ' task '
                . sprintf(' WHERE task.flags & %d != 0 ', TaskModel::ISOPEN)
                . $where . $where2
                . 'UNION SELECT \'overdue\', count( task.id ) AS tasks '
                . 'FROM ' . TASK_TABLE . ' task '
                . sprintf(' WHERE task.flags & %d != 0 ', TaskModel::ISOPEN)
                . sprintf(' AND task.flags & %d != 0 ', TaskModel::ISOVERDUE)
                . $where
                . 'UNION SELECT \'assigned\', count( task.id ) AS tasks '
                . 'FROM ' . TASK_TABLE . ' task '
                . sprintf(' WHERE task.flags & %d != 0 ', TaskModel::ISOPEN)
                . 'AND task.staff_id = ' . db_input($staff->getId()) . ' '
                . $where
                . 'UNION SELECT \'closed\', count( task.id ) AS tasks '
                . 'FROM ' . TASK_TABLE . ' task '
                . sprintf(' WHERE task.flags & %d = 0 ', TaskModel::ISOPEN)
                . $where;

        $res = db_query($sql);
        $stats = array();
        while ($row = db_fetch_row($res))
            $stats[$row[0]] = $row[1];

        return $stats;
    }

    static function getAgentActions($agent, $options = array()) {
        if (!$agent)
            return;

        require STAFFINC_DIR . 'templates/tasks-actions.tmpl.php';
    }

    /*
      function to get promised sla on task creation

     */

    public function getPromisedSla($dataforsla = array()) {
        $orderID = TicketOData::lookup($dataforsla['ticket_id'])->order_id;
        $data = array(
            "order_id" => $orderID,
            "platform" => "M"
        );
        $curl_post_data = json_encode($data);
        $key = "d32121c70dda5edfgd1df6633fdb36c0";
        $url = "http://api.shopclues.com/api/v11/atom/order/osticketinfo?key=" . $key;
        $result = curl_request($url, $curl_post_data, "POST");

        // Log Info for BulkJob Error
        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D2"];
        $module = $logConfig["module"][$domain]["D2_M1"];
        $error = $logConfig["error"]["E17"];
        $level = $logConfig["level"]["INFO"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $level, $error, "title:SLA Service Response", "log_location:class.task.php", "task:" . $this->getId() . "--Ticket:" . $ticket_id, "SLA:" . $result['response']['task_end_sla'], substr(("URL-" . $url . "---Data-" . $curl_post_data . "---Result:" . json_encode($result)), 0, 2500));

        $response = $result['response'];
        $current_status_timestamp = $response['current_status_timestamp'];
        $task_end_sla = $response['task_end_sla'];
        $next_group_sla = $response['next_group_sla'];
        $current_timestamp = time();
        $task_sla = $current_status_timestamp + $task_end_sla;

        //temporary code to handle particular case : if merchant is premium,task type is track and deliver and order status is HTC then the sla will also contain next_group_sla

        if ($dataforsla['merchant_id'] == 1 && $dataforsla['task_type_id'] == 64 && $dataforsla['task_status'] == '104') {
            if (($task_end_sla + $next_group_sla) == 0)
                return 0;
            $task_sla = $task_sla + $next_group_sla;
        }
        else {
            if ($task_end_sla == 0) {
                return 0;
            }
        }
        if ($task_sla < $current_timestamp) {
            return 0;
        }
        $slaFormat = date("Y-m-d G:i:s", $task_sla);
        return $slaFormat;
    }

    public function taskAttachments($vars, $staff_id, $user_id) {
        //$vars["attachments"] = json_decode($vars["attachments"], true);
$order_id = $this->_answers['order_id'];
        $manifest_id = $this->_answers['manifest_id'];
        $product_id = $this->_answers['product_id'];
        $ticket_id = $this->_answers['ticket_id'];
        global $mapIds;
        $type = "A";
        $object_id = $this->getId();
        if ($manifest_id) {
            $type = "M";
            $object_id = $manifest_id;
            $attachData = $vars["attachment"]['manifest'][$manifest_id];
        } elseif ($order_id) {
            $type = "O";
            $object_id = $order_id;
            $attachData = $vars["attachment"]['order'][$order_id];
        } elseif ($product_id) {
            $type = "P";
            $object_id = $product_id;
            $attachData = $vars["attachment"]['product'][$product_id];
        }elseif ($batch_id){
            $type="B";
            $object_id=$batch_id;
           $attachData=$vars["attachment"]['batch'][$batch_id];
        }  
        if (!empty($attachData)) {
             array_push($mapIds, $object_id);
            foreach($attachData as $attach){
            $numFiles = 0;
            $files = $attach;
            $key = 'Task' . time() . rand(1, 99999);
            $signature = md5('signature');
            if(empty($files["name"])){
            $files["name"] = pathinfo($files["path"], PATHINFO_FILENAME);
            }
            $fileParam = array("ft" => "T", "bk" => "F", "type" => $files["type"], "size" => $files["size"], "key" => $key, "signature" => $signature, "name" => $files["name"], "path" => $files["path"], "cdn" => 1);
            $fileParamObj = AttachmentModel::create($fileParam);
            $fileParamObj->created = SqlFunction::NOW();
            $fileParamObj->save();
            $attachParam = array("object_id" => $object_id, "file_id" => $fileParamObj->id, "type" => $type, "inline" => 0, "staff_id" => $staff_id, "user_id" => $user_id, "ticket_id" => $ticket_id);

            $attachParamObj = Attachment::create($attachParam);
            $attachParamObj->created = SqlFunction::NOW();
            $attachParamObj->save();
            //Attachment
            $numFiles++;
            }
        } else {
            return false;
        }
        return true;
    }

}

class TaskCData extends VerySimpleModel {

    static $meta = array(
        'pk' => array('task_id'),
        'table' => TASK_CDATA_TABLE,
        'joins' => array(
            'task' => array(
                'constraint' => array('task_id' => 'TaskModel.task_id'),
            ),
        ),
    );

}

Class OrderDetailsModel extends VerySimpleModel {

    static $meta = array(
        'pk' => array('task_id'),
        'table' => TASK_DATA_TABLE,
        'joins' => array(
            'task' => array(
                'constraint' => array('task_id' => 'TaskModel.task_id'),
            ),
        ),
    );

}

class OrderDetails extends OrderDetailsModel {

    function getOrderDate() {
        return $this->ht['ord_date'];
    }
    
    function getOrderStatus(){
        $apiFields=getCachedTicketFields();
        return $apiFields['orderStatus']['Order'][$this->ht['ord_status']];
    }

    function getCallNumber() {
        return $this->ht[''];
    }

    function getTotalPrice() {
        return $this->ht['price_total'];
    }

    function getOrdProdId() {
        return $this->ht['ord_prod_id'];
    }

    function getOrdProdName() {
        return $this->ht['ord_prod_name'];
    }

    function getShipId() {
        return $this->ht['ship_id'];
    }

    function getShipCarrier() {
        return $this->ht['ship_carrier'];
    }

    function getMerchant() {
        return $this->ht['merchant'];
    }

    function getMerchantType() {
        $merchantType = "";
        switch ($this->ht['merchant_type']) {
            case 1:
                $merchantType = "Premium";
                break;
            case 2:
                $merchantType = "Basic";
                break;
            case 3:
                $merchantType = "mdf";
                break;
        }
        return $merchantType;
    }

    function getMerchantStateId() {
        return $this->ht['merchant_state_id'];
    }

    function getMerchantPhone() {
        return $this->ht['merchant_phone'];
    }

    function getVar($tag) {

        switch (mb_strtolower($tag)) {
            case 'ord_date':
                return $this->getOrderDate();
                break;
            case 'ord_status':
                return $this->getOrderStatus();
                break;
            case 'call_no':
                return $this->getCallNumber();
                break;
            case 'price_total':
                return $this->getTotalPrice();
                break;
            case 'ord_prod_id':
                return $this->getOrdProdId();
                break;
            case 'ord_prod_name':
                return $this->getOrdProdName();
                break;
            case 'ship_id':
                return $this->getShipId();
                break;
            case 'ship_carrier':
                return $this->getShipCarrier();
                break;
            case 'merchant':
                return $this->getMerchant();
                break;
            case 'merchant_type':
                return $this->getMerchantType();
                break;
            case 'merchant_state_id':
                return $this->getMerchantStateId();
                break;
            case 'merchant_phone':
                return $this->getMerchantPhone();
                break;
        }
    }

}

class TaskData extends VerySimpleModel {

    static $meta = array(
        'pk' => array('task_id'),
        'table' => TASK_DATA_TABLE,
        'joins' => array(
            'task' => array(
                'constraint' => array('task_id' => 'TaskModel.task_id'),
            ),
        ),
    );

}

class TaskSLA extends VerySimpleModel {

    static $meta = array(
        'pk' => array('task_type'),
        'table' => TASK_SLA_TABLE
    );

}

class TaskForm extends DynamicForm {

    static $instance;
    static $defaultForm;
    static $internalForm;
    static $forms;
    static $cdata = array(
        'table' => TASK_CDATA_TABLE,
        'object_id' => 'task_id',
        'object_type' => 'A',
    );

    static function objects() {
        $os = parent::objects();
        return $os->filter(array('type' => ObjectModel::OBJECT_TYPE_TASK));
    }

    static function getDefaultForm() {
        if (!isset(static::$defaultForm)) {
            if (($o = static::objects()) && $o[0])
                static::$defaultForm = $o[0];
        }

        return static::$defaultForm;
    }

    static function getInstance($object_id = 0, $new = false) {
        if ($new || !isset(static::$instance))
            static::$instance = static::getDefaultForm()->instanciate();

        static::$instance->object_type = ObjectModel::OBJECT_TYPE_TASK;

        if ($object_id)
            static::$instance->object_id = $object_id;

        return static::$instance;
    }

    static function getInternalForm($source = null, $options = array()) {
        if (!isset(static::$internalForm))
            static::$internalForm = new TaskInternalForm($source, $options);

        return static::$internalForm;
    }

}

class TaskInternalForm extends AbstractForm {

    static $layout = 'GridFormLayout';

    function buildFields() {

        $fields = array(
            'dept_id' => new DepartmentField(array(
                'id' => 1,
                'label' => __('Department'),
                'required' => true,
                'layout' => new GridFluidCell(6),
                    )),
            'assignee' => new AssigneeField(array(
                'id' => 2,
                'label' => __('Assignee'),
                'required' => false,
                'layout' => new GridFluidCell(6),
                    )),
            'sla' => new SlaField(array(
                'id' => 3,
                'label' => __('SLA'),
                'required' => false,
                'layout' => new GridFluidCell(6),
                    )),
            'duedate' => new DatetimeField(array(
                'id' => 4,
                'label' => __('Due Date'),
                'required' => false,
                'configuration' => array(
                    'min' => Misc::gmtime(),
                    'time' => true,
                    'gmt' => true,
                    'future' => true,
                ),
                    )),
        );

        $mode = @$this->options['mode'];
        if ($mode && $mode == 'edit') {
            unset($fields['dept_id']);
            unset($fields['staff_id']);
        }

        return $fields;
    }

}

// Task thread class
class TaskThread extends TaskObjectThread {

    function addDescription($vars, &$errors = array()) {

        $vars['threadId'] = $this->getId();
        $vars['message'] = $vars['description'];
        unset($vars['description']);

        return TaskMessageThreadEntry::create($vars, $errors);
    }

    static function create($task) {
        $id = is_object($task) ? $task->getId() : $task;
        $thread = parent::create(array(
                    'object_id' => $id,
                    'object_type' => ObjectModel::OBJECT_TYPE_TASK
        ));
        if ($thread->save())
            return $thread;
    }

}

?>
