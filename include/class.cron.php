<?php
/*********************************************************************
    class.cron.php

    Nothing special...just a central location for all cron calls.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    TODO: The plan is to make cron jobs db based.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
//TODO: Make it DB based!
require_once INCLUDE_DIR.'class.signal.php';

class Cron {

    function MailFetcher() {
        require_once(INCLUDE_DIR.'class.mailfetch.php');
        MailFetcher::run(); //Fetch mail..frequency is limited by email account setting.
    }
    /**
     * Function to mark ticket as closed
     * @author Akash Kumar
     */
    function ticketStatusClose() {
        require_once(INCLUDE_DIR.'class.closeTicket.php');
        closeTicket::run(); //Fetch mail..frequency is limited by email account setting.
    }

    function TicketMonitor() {
        require_once(INCLUDE_DIR.'class.ticket.php');
        Ticket::checkOverdue(); //Make stale tickets overdue
        // Cleanup any expired locks
        require_once(INCLUDE_DIR.'class.lock.php');
        Lock::cleanup();
    }
    function OrderStatusMonitor(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.task.php');
        require_once(INCLUDE_DIR.'class.orderStatusCron.php');
        
        $max = $cfg->config['order_cron_batch_size']['value'];
        $timeGap = $cfg->config['order_cron_timegap']['value'];
        
        $orderStatusObj = new OrderStatusCron;
        //$orderStatusObj->preventMultipleCron($timeGap);
        $orders = $orderStatusObj->getOrders($max);
        $performListArr = $orderStatusObj->performTaskList();
        $ticketFlowArr = $orderStatusObj->ticketFlowList();
        $orderStatusObj->PerformOperation($orders,$performListArr,$ticketFlowArr);
        
    }
    function AutoRefundForCODMonitor(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.task.php');
        require_once(INCLUDE_DIR.'class.autoRefundCODCron.php');
        $AutoRefundCODCronobj = new AutoRefundCODCron;
        $AutoRefundCODCronobj->AutoRespond();
    }
    function autoCloseTicketsFromSolved() {
        global $cfg;
        require_once(INCLUDE_DIR . 'class.autoCloseTicket.php');
        $autoCloseTicket = new TicketClosure();
        $autoCloseTicket->autoCloseTickets();
    }

    function sendAutoMailForImageTask() {
        global $cfg;
        require_once(INCLUDE_DIR . 'class.autoMailForTasks.php');
        $autoMailerForTask = new TaskImageMailer();
        $autoMailerForTask->sendAutoMailer();
    }

    function sendAutoMailForManifestPickUp() {
        global $cfg;
        require_once(INCLUDE_DIR . 'class.autoMailForManifestPickUp.php');
        $autoMailForManifest = new ManifestPickUp();
        $autoMailForManifest->sendAutoMailer();
    }

    function sendReminderForImage() {
        global $cfg;
        require_once(INCLUDE_DIR . 'class.autoReminderForImage.php');
        $autoMailerForTask = new AutoReminderForImage();
        $autoMailerForTask->sendReminderForImage();
    }

    function FirstTimeUpdater(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.ticket.php');
        $firstTimeOdataUpdaterObj = new FirstTimeOdataUpdater;
        $orderArr = $firstTimeOdataUpdaterObj->getOrders();
        $firstTimeOdataUpdaterObj->update($orderArr);
    }
    function OrderCloneSwapMonitor(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.ticket.php');
        require_once(INCLUDE_DIR.'api.tickets.php');
        require_once(INCLUDE_DIR.'class.orderCLoneSwapCron.php');
        $OrderSwapMonitorobj = new OrderSwapMonitor;
        $ticketArr = $OrderSwapMonitorobj->getTickets();
        $OrderSwapMonitorobj->PerformOperation($ticketArr);        
    }
    function ReturnStatusMonitor(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.task.php');
        require_once(INCLUDE_DIR.'class.returnStatusCron.php');
        $max = $cfg->config['order_cron_batch_size']['value'];
        $timeGap = $cfg->config['order_cron_timegap']['value'];
        
        $orderStatusObj = new ReturnStatusCron;
        $orderStatusObj->preventMultipleCron($timeGap);
        $orders = $orderStatusObj->getOrders($max);
        $orderStatusObj->PerformOperation($orders);
    }
    function ConsumeNewTicketEmailQueue(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.task.php');
        require_once(INCLUDE_DIR.'class.returnStatusCron.php');
        $ticketCreationEmailQueue = json_decode($cfg->config['mailingQueue']['value'],true);
            
        $exchange = $ticketCreationEmailQueue["exchange"];
        $key =  $ticketCreationEmailQueue["key"];
        $queue =  $ticketCreationEmailQueue["queue"];
        
        require_once ROOT_DIR . 'libraries/amqplib/clues/merchant_email_send_process.php';
    }
    
    function ConsumeNewTicketCreation(){
        global $cfg;
        require_once(INCLUDE_DIR.'class.task.php');
        require_once(INCLUDE_DIR.'class.returnStatusCron.php');
        $ticketCreationEmailQueue = json_decode($cfg->config['merchantTicketCreation']['value'],true);
        $exchange = $ticketCreationEmailQueue["exchange"];
        $key =  $ticketCreationEmailQueue["key"];
        $queue =  $ticketCreationEmailQueue["queue"];
        
        require_once ROOT_DIR . 'libraries/amqplib/clues/merchant_ticket_creation_process.php';
    }
    
    function UpdateReportData(){
        require_once(INCLUDE_DIR.'class.reportUpdateCron.php');
        include_once REPORT_LIBRARY_PATH . "ReportLibrary.php";
        define("REPORT_TYPE",'daily');
        define("NUMBER_OF_DAYS_TO_CALCULATE_FOR_PENDENCY", 1);
        define("NUMBER_OF_DAYS_TO_CALCULATE_FOR_SLA_SUMMARY", 1);
        $reportObj=new ReportUpdateDailyCron();
        $reportObj->main();
    }
    
    function UpdateFullReportData(){
        require_once(INCLUDE_DIR.'class.reportUpdateCron.php');
        include_once REPORT_LIBRARY_PATH . "ReportLibrary.php";
        define("REPORT_TYPE",'full');
        define("NUMBER_OF_DAYS_TO_CALCULATE_FOR_PENDENCY", 0);
        define("NUMBER_OF_DAYS_TO_CALCULATE_FOR_SLA_SUMMARY", 0);
        $reportObj=new ReportUpdateDailyCron();
        $reportObj->main();
        
    }
    function PurgeLogs() {
        global $ost;
        // Once a day on a 5-minute cron
        if (rand(1,300) == 42)
            if($ost) $ost->purgeLogs();
    }

    function PurgeDrafts() {
        require_once(INCLUDE_DIR.'class.draft.php');
        Draft::cleanup();
    }

    function CleanOrphanedFiles() {
        require_once(INCLUDE_DIR.'class.file.php');
        AttachmentFile::deleteOrphans();
    }

    function MaybeOptimizeTables() {
        // Once a week on a 5-minute cron
        $chance = rand(1,2000);
        switch ($chance) {
        case 42:
            @db_query('OPTIMIZE TABLE '.LOCK_TABLE);
            break;
        case 242:
            @db_query('OPTIMIZE TABLE '.SYSLOG_TABLE);
            break;
        case 442:
            @db_query('OPTIMIZE TABLE '.DRAFT_TABLE);
            break;

        // Start optimizing core ticket tables when we have an archiving
        // system available
        case 142:
            #@db_query('OPTIMIZE TABLE '.TICKET_TABLE);
            break;
        case 542:
            #@db_query('OPTIMIZE TABLE '.FORM_ENTRY_TABLE);
            break;
        case 642:
            #@db_query('OPTIMIZE TABLE '.FORM_ANSWER_TABLE);
            break;
        case 342:
            #@db_query('OPTIMIZE TABLE '.FILE_TABLE);
            # XXX: Please do not add an OPTIMIZE for the file_chunk table!
            break;

        // Start optimizing user tables when we have a user directory
        // sporting deletes
        case 742:
            #@db_query('OPTIMIZE TABLE '.USER_TABLE);
            break;
        case 842:
            #@db_query('OPTIMIZE TABLE '.USER_EMAIL_TABLE);
            break;
        }
    }

    function run(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        self::MailFetcher();
        self::TicketMonitor();
        self::OrderStatusMonitor();
        //Cron for monitoring order status and perform actions accordingly
        self::OrderStatusMonitor();
        self::PurgeLogs();
        // Run file purging about every 10 cron runs
        if (mt_rand(1, 9) == 4)
            self::CleanOrphanedFiles();
        self::PurgeDrafts();
        self::MaybeOptimizeTables();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
    }
}
?>
