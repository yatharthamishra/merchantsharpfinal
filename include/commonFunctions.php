<?php

/**
 * Function to give data for all ticket Fields
 * @param array $TIdArr - array of ticket ids
 * @return array of details for every ticket id
 * @author Akash Kumar
 */
function getCachedTicketFields() {
    
    global $cachedTicketFeilds;
    
    if(!is_null($cachedTicketFeilds)){
      return $cachedTicketFeilds;
    }
    
    $memcacheObj = new MemcacheStorage();
    $staticValue = $memcacheObj->getValue("staticData");
    
    if ($staticValue && !$_GET["flush"] ){
        $staticValue= unserialize(gzuncompress($staticValue));
        $cachedTicketFeilds=$staticValue;
        return $staticValue;
    }
        

    $sqlStatus = "SELECT * FROM " . TICKET_STATUS_TABLE;
    if (($res = db_query($sqlStatus)) && db_num_rows($res))
        while ($row = db_fetch_array($res)) {
            $status[$row["id"]] = $row["name"];
            $statusIdStateMap[lcfirst($row["name"])] = $row["id"];
            $state[$row["state"]][] = $row["id"];
        }

    $sqlPriority = "SELECT * FROM " . TICKET_PRIORITY_TABLE;
    if (($res = db_query($sqlPriority)) && db_num_rows($res))
        while ($row = db_fetch_array($res)) {
            $priority[$row["priority_id"]]["name"] = $row["priority_desc"];
            $priority[$row["priority_id"]]["color"] = $row["priority_color"];
        }


    $sqlStaff = "SELECT staff_id,firstname,lastname,dept_id FROM " . STAFF_TABLE." where dept_id>0";
    if (($res = db_query($sqlStaff)) && db_num_rows($res))
        while ($row = db_fetch_array($res)) {
            $staff[$row["staff_id"]] = $row["firstname"] . " " . $row["lastname"];
            $staffDepartmentMap[$row['staff_id']]=$row['dept_id'];
            $departmentStaffMap[$row['dept_id']][]=$row['staff_id'];
        }


    // To get full department with path
    $deptObject = Dept::objects();
    foreach ($deptObject as $d) { //print_r($d);die;
        $departments[$d->getId()]["name"] = $d->getName();
        $departments[$d->getId()]["path"] = $d->path;
        $dep[$d->getId()] = $d->getName();
    }
    
    foreach ($departments as $k => $d) {
        $depArr = explode("/", trim($d["path"], "/"));
        foreach ($depArr as $depEle) {
            $department[$k]["finalPath"] .= $departments[$depEle]["name"] . " / ";
        }
        $departments[$k]["finalPath"] = trim($department[$k]["finalPath"], " / ");
    }

    $statusTypes = array("O" => "Order", "R" => "Return","X"=>"Refund", "P"=>"Product");
    $statusTypesUpper = array("O" => "OrderUpper", "R" => "ReturnUpper","X"=>"RefundUpper", "P"=>"ProductUpper");
    $orderStatus = OrderStatusDescription::objects()->values("status", "type", "description")->all();
    foreach ($orderStatus as $Ostatus) {
	$Ostatus["status"]=strtoupper($Ostatus["status"]);
	if ($statusTypes[$Ostatus["type"]])
            $orderStatusFull[$statusTypesUpper[$Ostatus["type"]]][$Ostatus["status"]] = $Ostatus["description"];
        if ($statusTypes[$Ostatus["type"]])
            $orderStatusFull[$statusTypes[$Ostatus["type"]]][$Ostatus["status"]] = $Ostatus["description"];
	$Ostatus["status"]=strtolower($Ostatus["status"]);
	if ($statusTypes[$Ostatus["type"]])
            $orderStatusFull[$statusTypes[$Ostatus["type"]]][$Ostatus["status"]] = $Ostatus["description"];
    }
    
    global $cfg;
    $batchUPloadStatus = json_decode($cfg->config['batch_upload_status']['value'],True);
    
    foreach ($batchUPloadStatus as $key => $value) {
	$key=strtoupper($key);
	$orderStatusFull["batchUploadStatusUpper"][$key] = $value;
        $orderStatusFull["batchUploadStatus"][$key] = $value; 
	$key=strtolower($key);
	$orderStatusFull["batchUploadStatus"][$key] = $value;
    }

    $helpTopicArr = Topic::objects()->filter(array('isactive' => 1))->values('topic_id', 'topic','topic_pid')->all();
    foreach ($helpTopicArr as $helpT) {
        $helpTopic[$helpT["topic_id"]] = $helpT["topic"];
        $helpTopic['parent_id'][$helpT["topic_id"]] = $helpT["topic_pid"];

    }
    $billing_status=["Y"=>"Done","N"=>"Not Done"];

    $typeList = DynamicList::objects()->filter(array("name__in" => array("Triage Status")));
    foreach ($typeList as $list) {
        foreach ($list->items as $items) {
            $listFormated[$list->name][$items->extra] = $items->value;
            $listFormated["id_extra_".$list->name][$items->id] = $items->extra;
            $listFormated["Default_" . $list->name][$items->id] = $items->value;
        }
    }

    $ChannelList = DynamicList::objects()->filter(array("name" => "Priority Flags"));
    foreach ($ChannelList as $list) {
        foreach ($list->items as $items) {
            $listFormatedChannel[$list->name][$items->extra] = $items->value;
            $listFormatedChannel["id_extra_".$list->name][$items->id] = $items->extra;
            $listFormatedChannel["Default_" . $list->name][$items->id] = $items->value;
        }
    }

    $staticData = array("state" => $state, "helpTopic" => $helpTopic, "status" => $status, "priority" => $priority, "staff" => $staff, "department" => $departments, "dept" => $dep, 'orderStatus' => $orderStatusFull,"Triage" => $listFormated,
        'staffDepartmentMap'=>$staffDepartmentMap,'departmentStaffMap'=>$departmentStaffMap,'channel' =>$listFormatedChannel,'statusIdMap'=>$statusIdStateMap,"billingStatus"=>$billing_status);
    $memcacheObj->setValue("staticData", gzcompress(serialize($staticData),5));

    return $staticData;
}
function getTopicParentChildMapping() {
    global $topicParentDict;
    global $topicDict;
    if (isset($topicParentDict) && isset($topicDict)) {
        return array("topicParentDict" => $topicParentDict, "topicDict" => $topicDict);
    }
    $sql = "select topic_id,topic_pid,topic from mst_help_topic where isactive=1";
    $rs = db_query($sql);
    while ($row = db_fetch_array($rs)) {
        $topicParentDict[$row['topic_id']] = $row['topic_pid'];
        $topicDict[$row['topic_id']] = $row["topic"];
    }
    return array("topicParentDict" => $topicParentDict, "topicDict" => $topicDict);
}

function getTopicDetailDictionary() {
    $topicOveDict = getTopicParentChildMapping();
    $topicParentMap = $topicOveDict['topicParentDict'];
    $topicOveDictionary = $topicOveDict['topicDict'];
    $topicFullName=[];
    foreach ($topicParentMap as $topic_id => $topic_pid) {
        $overallText = getParentToChildText($topic_id, $topicParentMap, $topicOveDictionary);
        $topicFullName[$topic_id]=$overallText;
    }
    return $topicFullName;
}

function getParentToChildMapping(){
    $topicOveDict=getTopicParentChildMapping();
    $topicParentMap=$topicOveDict['topicParentDict'];
    $topicOveDictionary=$topicOveDict['topicDict'];
   foreach($topicParentMap as $topic_id=>$topic_pid){
	 $overallText=getParentToChildText($topic_id, $topicParentMap, $topicOveDictionary);
	 $sql="update mst_help_topic set topic_full_text='".$overallText."' where topic_id=".$topic_id;
	 db_query($sql);
     } 
    return 'done';
}

function checkIfLoginUserOfMCG(){
    global  $thisstaff;
    global $cfg;
    $mcg_dept_id=$cfg->config['mcg_dept_id']['value'];
    $staffDepts=$thisstaff->getDepts();
    if(in_array($mcg_dept_id, $staffDepts)){
        return 1;
    }
    return 0;
}

function checkIfShowDepartmentView(){
    global $thisstaff;
    global $cfg;
    $dep_view_ids=$cfg->config['dept_view_ids']['value'];
    $dept_view_list=  explode(',', $dep_view_ids);
    $staffDepts=$thisstaff->getDepts();
    $commonValues=array_intersect($dept_view_list, $staffDepts);
    if(count($commonValues)>0){
        return 1;
    }
    return 0;
}

function getParentToChildText($topicId,$topicParentMap,$topicOveDictionary){
    $topic_name=$topicOveDictionary[$topicId];
    if(!$topicParentMap[$topicId]){
        return $topic_name;
    }
    $overalTopicName.=getParentToChildText($topicParentMap[$topicId],$topicParentMap,$topicOveDictionary)."/".$topic_name;
    return $overalTopicName;
}

/**
 * Function to give data for all task Fields
 * @return array of details for every ticket id
 * @author Akash Kumar
 */
function getCachedAPIFields() {
    global $cachedApiFeilds;
    if(0 && !is_null($cachedApiFeilds)){
        return $cachedApiFeilds;
    }
    $memcacheObj = new MemcacheStorage();
    $staticValue = $memcacheObj->getValue("taskStaticApiData");
    if (0 && $staticValue && !$_GET["flush"]){
        $cachedApiFeilds=$staticValue;
        return $staticValue;
    
    }

    $typeList = DynamicList::objects()->filter(array("name__in" => array("Task Types", "Task Status", "Task Response", "Task Sources","Task Image Type")));
    foreach ($typeList as $list) {
        foreach ($list->items as $items) {
            $listFormated[$list->name][$items->extra] = $items->value;
 	    $listFormated["Default_" . $list->name][$items->id] = $items->value;
	    $listFormated["Default_Prop_" . $list->name][$items->id] = $items->ht['properties'];
        }
    }
    $memcacheObj->setValue("taskStaticApiData", $listFormated);

    return $listFormated;
}
/**
 * Function to give data for all task Fields
 * @return array of details for every ticket id
 * @author Akash Kumar
 */
function getCachedTaskFields() {
    global $cachedTaskFields;
    if(!is_null($cachedTaskFields)){
        return $cachedTaskFields;
    }
    $memcacheObj = new MemcacheStorage();
    $staticValue = $memcacheObj->getValue("taskStaticData");
    
    $useCacheForTaskField=$cfg->config['useCacheForTaskField']['value'];
    if ( $staticValue && !$_GET["flush"] && $useCacheForTaskField){
        $cachedTaskFields=$staticValue;
        return $staticValue;
    
    }
    
    $typeList = DynamicList::objects()->filter(array("name__in" => array("Task Types", "Task Status", "Task Response", "Task Sources")));
    foreach ($typeList as $list) {
        foreach ($list->items as $items) {
            $listFormated["create"][$list->name][$items->extra][$items->id] = $items->value;
            $listFormated[$list->name][$items->extra] = $items->value;
            $listFormated["id_extra_".$list->name][$items->id] = $items->extra;
            $listFormated["Default_" . $list->name][$items->id] = $items->value;
            $listFormated["Prop_" . $list->name][$items->value] = $items->properties;
            $listFormated["List_" . $list->name] = $items->list_id;
        }
    }
    $memcacheObj->setValue("taskStaticData", $listFormated);

    return $listFormated;
}
function getCachedTriageFields() {
    global $cachedTriageFields;
    if(!is_null($cachedTriageFields)){
        return $cachedTriageFields;
    }
    $memcacheObj = new MemcacheStorage();
    $staticValue = $memcacheObj->getValue("triageStaticData");
    
    if ($staticValue && !$_GET["flush"]){
        $cachedTriageFields=$staticValue;
        return $staticValue;
    
    }
    $typeList = DynamicList::objects()->filter(array("name__in" => array("Triage Status", "Priority Flags", "Communication Flags", "State Machine")));
    foreach ($typeList as $list) {
        $listFormId=$list->getForm()->id;
        $listFormFields=DynamicFormField::objects()->filter(array("form_id"=>$listFormId));
        foreach($listFormFields as $field){
            $idToNameFormMap[$field->id]=$field->name;
        };
        foreach ($list->items as $items) {
            $listFormated["create"][$list->name][$items->extra][$items->id] = $items->value;
            $listFormated[$list->name][$items->extra] = $items->value;
            $listFormated["id_extra_".$list->name][$items->id] = $items->extra;
            $listFormated["Default_" . $list->name][$items->id] = $items->value;
            $listFormated["Prop_" . $list->name][$items->value] = $items->properties;
            $listFormated["List_" . $list->name] = $items->list_id;
            $listFormated["PropId_" . $list->name][$items->extra] = $items->properties;
            if(!empty($items->properties)){
                $itemPropertiesParsed=  json_decode($items->properties);
                foreach ($itemPropertiesParsed as $formID=>$formValue){
                    $formFieldName=$idToNameFormMap[$formID];
                    $listFormated["PropName_$list->name"][$items->extra][$formFieldName]=$formValue;
                }
                
            }
        }
    }
    $memcacheObj->setValue("triageStaticData", $listFormated);
    return $listFormated;
}
function getFlowMapFields() {
    global $flowMapCachedFields;
    if(!is_null($flowMapCachedFields)){
        return $flowMapCachedFields;
    }
    $memcacheObj = new MemcacheStorage();
    $staticValue = $memcacheObj->getValue("FlowMapFields");
   
    if (0 && $staticValue && !$_GET["flush"]){
        $flowMapCachedFields=$staticValue;
        return $staticValue;
    
    }
    global $cfg;
    $issuesArr = CluesIssues::objects()->filter(array('type__in'=>array('C')))->values()->all();
    foreach($issuesArr as $issue){
        $issue_idArr[$issue["parent_issue_id"]][$issue["issue_id"]] = $issue["name"];
    }
    $issue_id = $issue_idArr[0];$sub_issue_id = $issue_idArr;$sub_sub_issue_id = $issue_idArr;
    $merchant_type = array("0"=>"Any","1"=>"Premium","2"=>"Basic","3"=>"MDF");
    $issuesArr = CluesIssues::objects()->filter(array('type__in'=>array('C')))->values()->all();
    foreach($issuesArr as $issue){
        $issue_idArr[$issue["parent_issue_id"]][$issue["issue_id"]] = $issue["name"];
    }
    
    $catArr = CscartCategoryDescription::objects()->filter(array("cat__parent_id"=>0))->values("category_id","category")->all();
    $cat_idArr[0] = "Any";
    foreach($catArr as $cat){
        $cat_idArr[$cat["category_id"]] = $cat["category"];
    }
    $additionalCat = json_decode($cfg->config['additional_category']['value'], true);
    foreach($additionalCat as $cat){
        $cat_idArr[$cat["category_id"]] = $cat["category"];
    }
    $cat = $cat_idArr;
    
    $valArr = json_decode($cfg->config['value_range']['value'], true);
    $listFormated = array("issue_id"=>$issue_id,"sub_issue_id"=>$sub_issue_id,"sub_sub_issue_id"=>$sub_sub_issue_id,"merchant_type"=>$merchant_type,"category"=>$cat,"valueRange"=>$valArr);
    $memcacheObj->setValue("FlowMapFields", $listFormated);

    return $listFormated;
}
function getCachedReturnFlowFields() {
    global $cachedReturnFlowFields;
    if(!is_null($cachedReturnFlowFields)){
        return $cachedReturnFlowFields;
    }
    $memcacheObj = new MemcacheStorage();
    $staticValue = $memcacheObj->getValue("ReturnFlowData");
   
    if (0 && $staticValue && !$_GET["flush"]){
     $cachedReturnFlowFields=$staticValue;
        return $staticValue;
    
    }

    global $cfg;
    $returnAction = json_decode($cfg->config['returnFlow_return_action']['value'], true);
    
    $subReasonArr = RMAPropertyDescription::objects()->values("property_id","property")->all();
    $sub_reason[0] = "Any";
    foreach($subReasonArr as $sub){
        $sub_reason[$sub["property_id"]] = $sub["property"];
    }
    $tag = json_decode($cfg->config['tag_name']['value'], true);
    
    $listFormated = array("return_action"=>$returnAction,"tagName"=>$tag,"sub_reason"=>$sub_reason);
    $memcacheObj->setValue("ReturnFlowData", $listFormated);

    return $listFormated;
}

function getCachedFunctionalRoles() {

    $memcacheObj = new MemcacheStorage(3600);
    $staticValue = $memcacheObj->getValue("functionalRole");
    if ($staticValue && !$_GET["flush"]){
    return $staticValue;
    
    }

    $roleList = Role::objects()->values("id", "name", "grade")->all();
    foreach ($roleList as $list) {
        $roles[$list["grade"]] = $list;
    }

    $memcacheObj->setValue("functionalRole", $roles);

    return $roles;
}
function getReturnTicketMapping() {

    $memcacheObj = new MemcacheStorage(3600);
    $staticValue = $memcacheObj->getValue("ReturnTicketMapping");
    if ($staticValue && !$_GET["flush"])
        return $staticValue;
    $configArrResult = ReturnFlow::objects()->filter(array("flowstatus"=>1))->values("issue_id", "sub_issue_id", "sub_sub_issue_id",'merchant_type','category','return_status','value_min','value_max','sub_reason','return_action','dept','task_type')->all();
    foreach ($configArrResult as $config) {
        $configArr[$config["issue_id"]?:0][($config["sub_issue_id"]?:0)][($config["sub_sub_issue_id"]?:0)][$config["merchant_type"]][$config["category"]][$config["return_status"]][$config["return_action"]][($config["sub_reason"]?:0)]["task"] = array("dept"=>$config["dept"],"task"=>$config["task_type"],"value_min"=>$config["value_min"],"value_max"=>$config["value_max"]);
    }
    $memcacheObj->setValue("ReturnTicketMapping", $returnMapping);
    return $returnMapping;
}

function getCachedAdminConfigFields() {

    $memcacheObj = new MemcacheStorage(3600);
    $staticValue = $memcacheObj->getValue("AdminConfig");
    if ($staticValue && !$_GET["flush"])
        return $staticValue;

    $configArrResult = AdminOSTConfig::objects()->values("module", "submodule", "key",'value')->all();
    foreach ($configArrResult as $config) {
        $configArr[$config["module"]][$config["submodule"]][$config["key"]] = $config["value"];
    }
    
    $adminVars = array("config"=>$configArr);
    $memcacheObj->setValue("AdminConfig", $adminVars);

    return $adminVars;
}

function clues_encrypt_data($data) {
    global $cfg;
    $singleSignonDecrypt = json_decode($cfg->config['signOndecryption']['value'], true);
    $encryption_method = $singleSignonDecrypt["encryption_method"]; //"AES-256-CBC"; // Shall come from config..
    $secret_hash = $singleSignonDecrypt["secret_hash"]; //"1cc8c7ff35q5979x151f2136cd13b099"; // Shall come from config...
    $iv_size = $singleSignonDecrypt["iv_size"]; //16
    $validity = $singleSignonDecrypt["validity"]; //300
    $data_separator = $singleSignonDecrypt["data_separator"]; //'______$$$$$$______';

    $iv = bin2hex(openssl_random_pseudo_bytes($iv_bytes_length));
    $iv_size = strlen($iv);

     if (is_array($data) && count($data) > 0) {
       $data = serialize($data);
     }

    $data = $data . $data_separator . (time() + $validity);

    $encrypted_message = bin2hex(rawurlencode(openssl_encrypt($data, $encryption_method, $secret_hash, 0, $iv)));
    //$encrypted_message = openssl_encrypt($data, $encryption_method, $secret_hash, 0, $iv);
    $encrypted_message = $iv . $encrypted_message;
 
    return $encrypted_message;
}
function clues_decrypt_data($data) {
    global $cfg;
    $singleSignonDecrypt = json_decode($cfg->config['signOndecryption']['value'], true);
    $encryption_method = $singleSignonDecrypt["encryption_method"]; //"AES-256-CBC"; // Shall come from config..
    $secret_hash = $singleSignonDecrypt["secret_hash"]; //"1cc8c7ff35q5979x151f2136cd13b099"; // Shall come from config...
    $iv_size = $singleSignonDecrypt["iv_size"]; //16
    $validity = $singleSignonDecrypt["validity"]; //300
    $data_separator = $singleSignonDecrypt["data_separator"]; //'______$$$$$$______';

    $iv = substr($data, 0, $iv_size);

    $decrypted_message = openssl_decrypt(rawurldecode(pack("H*", substr($data, $iv_size))), $encryption_method, $secret_hash, 0, $iv);
    $data = explode($data_separator, $decrypted_message);

    if ($data[1] < time()) {
        //return FALSE;
    }
    if (is_array(unserialize($data[0]))) {
        $data[0] = unserialize($data[0]);
    }
    return $data[0];
}

function getAdvanceSearchQuery($ignoreTaskFilter=true) {
    $fieldArr = array("assignee" => "staff_id", "Order Status" => "ord_status", "Return Status" => "retrn_status", "Task Type" => "taskType", "Merchant Type" => "merchant_type", "Courier Name" => "ship_carrier", "Order Ammount" => "price_sub_total", "Task Status" => "taskStatus", "Task Name" => "taskType", "Triage Status" => "triage.triage", "Reply Count" => "thread_count");
    $searched = $_SESSION['searched'];
    foreach ($searched as $search) {
        if ($ignoreTaskFilter && ($search["name"] == "Task Type" || $search["name"] == "Task Status"))
            continue;
        $query = " " . (array_key_exists($search["name"], $fieldArr) ? $fieldArr[$search["name"]] : $search["name"]);
        if ($search["method"] == "includes") {
            $query .= " IN (";
            foreach ($search["value"] as $id => $values) {
                if (preg_replace('/\D/', '', $id))
                    $query .= "'" . preg_replace('/\D/', '', $id) . "',";
                else
                    $query .= "'" . $id . "',";
            }
            $query = trim($query, ",");
            $query .= ") ";
            if ($search["name"] == "Triage Status") {
                if (in_array("Not Triage",$search["value"]) || in_array("Non Triage",$search["value"])) {
                    $query .= " OR triage.triage IS NULL ";
                }
            }
        } else if ($search["method"] == "!includes") {
            $query .= " NOT IN (";
            foreach ($search["value"] as $id => $values) {
                $query .= "'" . $id . "',";
            }
            $query = trim($query, ",");
            $query .= ") ";
        } else if ($search["method"] == "assigned") {
            $query .= " > 0 ";
        } else if ($search["method"] == "!assigned") {
            $query .= " < 1 ";
        } else if ($search["method"] == "set") {
            $query .= " > 0 ";
        } else if ($search["method"] == "nset") {
            $query .= " < 1 ";
        } else if ($search["method"] == "equal") {
            if (count(explode("-", $search["value"])) > 1){
                    $var=explode("-", $search["value"]);
            $var=$var[0];
            $var=$var+1;
            $var2=explode("-", $search["value"]);
            $var2=$var2[1];
                $query .= " BETWEEN " . ($var) . " AND " .$var2;
            }
            else
                $query .= " = " . preg_replace('/\D/', '', $search["value"]);
        }else if ($search["method"] == "customer_reply") {
            $query = " user_thread_count >= " . preg_replace('/\D/', '', $search["value"]);
        } else if ($search["method"] == "agent_reply") {
            $query = " agent_thread_count >= " . preg_replace('/\D/', '', $search["value"]);
        } else if ($search["method"] == "agent_note") {
            $query = " agent_note_count >= " . preg_replace('/\D/', '', $search["value"]);
        }
        $query = "(".$query.")";
        $finalQuery[] = $query;
    }
    //die;
    if ($finalQuery)
        $finalQuery = " ( " . implode(" AND ", $finalQuery) . " )";
//print_r($finalQuery);die;
    return $finalQuery;
}


function logConfig(){
    $loglevel = unserialize(analog_log_levels);
    $logConfig = unserialize(analog_domain_module);
    $logDomains = unserialize(analog_domains);
    $logModules = unserialize(analog_modules);
    $logErrors = unserialize(analog_errors);
    $log = array("domain_module_level"=>$logConfig,"domain"=>$logDomains,"module"=>$logModules,"error"=>$logErrors,"level"=>$loglevel);
    return $log;
}


function array2csv(array &$array)
{
    $head = array_keys($array);
    $fp = fopen('file.csv', 'w');

    foreach ($head as $fields) {
        fputcsv($fp, $fields);
    }
    foreach ($array as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
}

function curl_request($url,$data,$method)
{
    $method = strtoupper($method);
    $curl = curl_init();
    $default = array(
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_URL => $url,
                    CURLOPT_CUSTOMREQUEST => $method, 
                    CURLOPT_POSTFIELDS => $data 
                    );
    curl_setopt_array($curl, $default);
    //curl_setopt($curl, CURLOPT_URL, $url);
    //curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); 
    //curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($curl,CURLOPT_POSTFIELDS, $curl_post_data);
    $result = curl_exec($curl);
    $result=  json_decode($result,true);
    curl_close($curl);
    return $result;
}
function getIssueSubIssueInHeirachalForm($staticFields) {
    $issues = array();
    $subIssue = array();
    $subsubIssue = array();
    $issueId = array();
    $subIssueId = array();
    $subsubIssueId = array();
    $subsubIssueToTopicIdMapping=array();
    $subIssueToTopicIdMapping=array();
    $issueToTopicIdMapping=array();
    foreach ($staticFields['helpTopic'] as $topicId => $topic) {
        if ($topicId == "parent_id") {
            continue;
        }
        $issueSubIssueInfo = getIssuesRelatedInfoFromTopicId($staticFields, $topicId);
        if(!empty($issueSubIssueInfo['subsubIssueId'])){
            $subsubIssueToTopicIdMapping[$issueSubIssueInfo['subsubIssueId']]=$topicId;
        }elseif (!empty ($issueSubIssueInfo['subIssueId'])) {
            $subIssueToTopicIdMapping[$issueSubIssueInfo['subIssueId']]=$topicId;
        }elseif(!empty($issueSubIssueInfo['issueId'])){
            $issueToTopicIdMapping[$issueSubIssueInfo['issueId']]=$topicId;
        }
        if (!empty($issueSubIssueInfo['issueId'])) {
            if (!isset($issueId[$issueSubIssueInfo['issueId']])) {
                $issues[$issueSubIssueInfo['issueId']] = array('name' => $issueSubIssueInfo['issueType'], 'id' => $issueSubIssueInfo['issueId'],'topicId'=>$topicId);
                $issueId[$issueSubIssueInfo['issueId']] = 1;
                
            }
        }
        if (!empty($issueSubIssueInfo['subIssueId'])) {
            if (!isset($subIssueId[$issueSubIssueInfo['subIssueId']])) {
                $subIssue[$issueSubIssueInfo['issueId']][] = array('name' => $issueSubIssueInfo['issueSubType'], 'id' => $issueSubIssueInfo['subIssueId'],'topicId'=>$topicId);
                $subIssueId[$issueSubIssueInfo['subIssueId']] = 1;
                
            }
        }
        if (!empty($issueSubIssueInfo['subsubIssueId'])) {
            if (!isset($subsubIssueId[$issueSubIssueInfo['subsubIssueId']])) {
                $subsubIssue[$issueSubIssueInfo['subIssueId']][] = array('name' => $issueSubIssueInfo['issueSubSubType'], 'id' => $issueSubIssueInfo['subsubIssueId'],'topicId'=>$topicId);
                $subsubIssueId[$issueSubIssueInfo['subsubIssueId']] = 1;
                
            }
        }
        
    }
    $outputArray = array('issues' => $issues,
        'subIssues' => $subIssue,
        'subsubIssues' => $subsubIssue,
        'issueToTopicMapping'=>$issueToTopicIdMapping,
        'subIssueToTopicMapping'=>$subIssueToTopicIdMapping,
        'subsubIssueToTopicMapping'=>$subsubIssueToTopicIdMapping);
    return $outputArray;
}

function getIssuesRelatedInfoFromTopicId($staticFields, $topic_id) {
    $issueSubIssueMap=array(
        'issueType'=>'',
        'issueSubType'=>'',
        'issueSubSubType'=>'',
        'issueSubSubSubType'=>'',
        'issueId'=>'',
        'subIssueId'=>'',
        'subsubIssueId'=>'',
        'subsubsubIssueId'=>''
    );
    $parent_topic_id = $staticFields['helpTopic']['parent_id'][$topic_id];
    $parent_parent_topic_id = $staticFields['helpTopic']['parent_id'][$parent_topic_id];
    $parent_parent_parent_topic_id = $staticFields['helpTopic']['parent_id'][$parent_parent_topic_id];
    if($parent_parent_topic_id>0 && $parent_topic_id>0 && $parent_parent_parent_topic_id>0){
        $issueSubIssueMap['issueType']=$staticFields['helpTopic'][$parent_parent_parent_topic_id];
        $issueSubIssueMap['issueSubType']=$staticFields['helpTopic'][$parent_parent_topic_id];
        $issueSubIssueMap['issueSubSubType']=$staticFields['helpTopic'][$parent_topic_id];
        $issueSubIssueMap['issueSubSubSubType']=$staticFields['helpTopic'][$topic_id];
        $issueSubIssueMap['issueId']=$parent_parent_parent_topic_id;
        $issueSubIssueMap['subIssueId']=$parent_parent_topic_id;
        $issueSubIssueMap['subsubIssueId']=$parent_topic_id;
        $issueSubIssueMap['subsubsubIssueId']=$topic_id;
        
    }else if($parent_parent_topic_id>0 && $parent_topic_id>0){
        $issueSubIssueMap['issueType']=$staticFields['helpTopic'][$parent_parent_topic_id];
        $issueSubIssueMap['issueSubType']=$staticFields['helpTopic'][$parent_topic_id];
        $issueSubIssueMap['issueSubSubType']=$staticFields['helpTopic'][$topic_id];
        $issueSubIssueMap['issueId']=$parent_parent_topic_id;
        $issueSubIssueMap['subIssueId']=$parent_topic_id;
        $issueSubIssueMap['subsubIssueId']=$topic_id;
        
    }else if($parent_topic_id>0){
        $issueSubIssueMap['issueType']=$staticFields['helpTopic'][$parent_topic_id];
        $issueSubIssueMap['issueSubType']=$staticFields['helpTopic'][$topic_id];
        $issueSubIssueMap['issueId']=$parent_topic_id;
        $issueSubIssueMap['subIssueId']=$topic_id;
    }else{
        $issueSubIssueMap['issueType']=$staticFields['helpTopic'][$topic_id];
        $issueSubIssueMap['issueId']=$topic_id;
    }
    return $issueSubIssueMap;
}

function getHoursSpendBetweenTwoDates($createDate, $closedDate) {
    if (is_null($createDate) && is_null($closedDate)) {
        return 'unknown-Status';
    }
    if (is_null($closedDate)) {
        return '';
    }
    $createTime = strtotime($createDate); 
    $closedTime=  strtotime($closedDate);
    return $closedTime-$createTime;
    
}
function formatDateTimeToDateFormat($dateTimeToBeFormated){
    if(is_null($dateTimeToBeFormated)){
        return '';
    }
    $dt = new DateTime($dateTimeToBeFormated);
    if($dt){
        return $dt->format('d/m/Y H:i:s'); 
    }
    return '';
}
function getDepartmentNameFromId($staticFields,$deptId){
    return $staticFields['dept'][$deptId];
}
function getAgentAndManagerDetailsFromAgentId($staffManagerMapping,$agentId){
    $agentName=$staffManagerMapping[$agentId]['name'];
    $agentEmail=$staffManagerMapping[$agentId]['email'];
    $managerId=$staffManagerMapping[$agentId]['manager_id'];
    $managerName=$staffManagerMapping[$managerId]['name'];
    return array(
        'agentName'=>$agentName,
        'agentEmail'=>$agentEmail,
        'managerName'=>$managerName
    );
}
function getTaskResponseForDisplay($taskResponse){
    $taskResponseArray=explode(',', $taskResponse);
    if(count($taskResponseArray)>1){
        return $taskResponseArray[1];
    }
    return '';
}
function getTaskTypeToDisplay($taskType) {
    global $taskTypeDictionary;
    if (is_null($taskTypeDictionary)) {
        
        $cachedTaskFields = getCachedTaskFields();
        
        $taskTypeDictionary = $cachedTaskFields['Default_Task Types'];
    }
    $taskTypeArray = explode(',', $taskType);
    $taskTypeId = $taskTypeArray[0];
    return $taskTypeDictionary[$taskTypeId];
}
function getTaskStatusToDisplay($taskStatus){
    global $taskStatusDictionary;
    if (is_null($taskStatusDictionary)) {
        $cachedTaskFields = getCachedTaskFields();
        $taskStatusDictionary = $cachedTaskFields['Default_Task Status'];
    }
    $taskStatusArray = explode(',', $taskStatus);
    $taskStatusID = $taskStatusArray[0];
    return $taskStatusDictionary[$taskStatusID];
    
}
function getSLAPercentage($totalSolvedInSLA,$totalLoggedOpenInSLA,$totalAssigned){
    $denom=$totalAssigned-$totalLoggedOpenInSLA;
    if($denom==0){
        return 0;
    }
    $filter=$totalSolvedInSLA/$denom*100;
    $slaPercentage=round($filter);
    return $slaPercentage;
}
function getTopicIdFromIssueIdAndSubSubIssueId($staticFields, $issueIds, $subIssueIds = null, $subsubIssueIds = null) {
    $issueSubIssueData = getIssueSubIssueInHeirachalForm($staticFields);
    
    $issueToTopicMapping = $issueSubIssueData['issueToTopicMapping'];
    $subIssueToTopicMapping = $issueSubIssueData['subIssueToTopicMapping'];
    $subsubIssueToTopicMapping = $issueSubIssueData['subsubIssueToTopicMapping'];
    $topicIds = array();
    if (!is_null($subsubIssueIds)) {
        foreach ($subsubIssueIds as $subsubIssueId) {
            $topicIds[] = $subsubIssueToTopicMapping[$subsubIssueId];
        }
        return $topicIds;
    }
    if (!is_null($subIssueIds)) {
        foreach ($subIssueIds as $subIssueId) {
            $topicIds[] = $subIssueToTopicMapping[$subIssueId];
        }
        return $topicIds;
    }
    if (!is_null($issueIds)) {
        foreach ($issueIds as $issueId) {
            $topicIds[] = $issueToTopicMapping[$issueId];
        }
        return $topicIds;
    }
    return array();
}
function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
}
function _pr($data){
            echo '<pre>';
        print_r($data);
        echo '</pre>';
}
function sort2dArrayAlphabeticallyAccordingToKey(&$arrayToBeSorted,$keyName){
    usort($arrayToBeSorted, function($a,$b) use ($keyName){
        return strcasecmp($a[$keyName],$b[$keyName]);
    });
}
function getPriorityIdNameMap() {
    $typeList = DynamicList::objects()->filter(array("name" => "Priority Flags"));
    foreach ($typeList as $list) {
        foreach ($list->items as $items) {
            $priorityData[$items->extra] = $items->value;
        }
    }
    return $priorityData;
}
function getTimeToTriage(){
    global $cfg;
    $TimeToTriage = $cfg->config['TimeToTriage']["value"];
    return $TimeToTriage;
}
function getNumberOfHoursBetweenTwoDates($startDate, $endDate) {
  
    if (!is_null($startDate) && !is_null($endDate)) {
        $date1 = new DateTime($startDate);
        $date2 = new DateTime($endDate);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);
        return $hours;
    }
    return '';
}
function printLoggedDataIfAllowed() {
    if ($_GET['displayLog'] == 1) {
        global $queryLog;
        if (count($queryLog) > 0) {
            echo '<div>';
            foreach ($queryLog as $logType => $logDetails) {
                echo '<div style="    box-sizing: border-box;
    border-style: double;margin-bottom: 7px;">';
                $logTypeToDisplay=  ucwords($logType);
                echo "<center> <h1>$logTypeToDisplay </h1></center>";
                echo '<p><b>Number Of Queries:</b>' . count($logDetails) . '</p>';
                echo '<p><b>Total Query Execution Time</b> : ' . $logDetails['totalTimeTaken'] . '</p>';
                if (count($logDetails) > 0) {
                    foreach ($logDetails as $queries) {
                        echo '<div     style="    margin: 11px;
    padding: 4px;
    border-style: dotted;">';
                        foreach ($queries as $queryHeading => $queryVal) {
                            echo '<p>';
                            echo '<b>';
                            echo $queryHeading;
                            echo '</b>';
                            echo ': ' . $queryVal;
                            echo '</p>';
                        }
                        echo '</div>';
                    }
                }
                echo '</div>';
            }
            echo '</div>';
        }
    }}
