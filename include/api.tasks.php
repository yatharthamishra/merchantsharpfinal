<?php

include_once INCLUDE_DIR . 'class.api.php';
include_once INCLUDE_DIR . 'class.task.php';

class TaskApiController extends ApiController {
    # Supported arguments -- anything else is an error. These items will be
    # inspected _after_ the fixup() method of the ApiXxxDataParser classes
    # so that all supported input formats should be supported

    function getRequestStructure($format, $data = null) {
        $supported = array(
            "alert", "autorespond", "source", "topicId",
            "attachments" => array("*" =>
                array("name", "type", "data", "encoding", "size")
            ),
            "message", "ip", "priorityId"
        );
        # Fetch dynamic form field names for the given help topic and add
        # the names to the supported request structure
        if (isset($data['topicId']) && ($topic = Topic::lookup($data['topicId'])) && ($forms = $topic->getForms())) {
            foreach ($forms as $form)
                foreach ($form->getDynamicFields() as $field)
                    $supported[] = $field->get('name');
        }

        # Ticket form fields
        # TODO: Support userId for existing user
        if (($form = TicketForm::getInstance()))
            foreach ($form->getFields() as $field)
                $supported[] = $field->get('name');

        # User form fields
        if (($form = UserForm::getInstance()))
            foreach ($form->getFields() as $field)
                $supported[] = $field->get('name');

        if (!strcasecmp($format, 'email')) {
            $supported = array_merge($supported, array('header', 'mid',
                'emailId', 'to-email-id', 'ticketId', 'reply-to', 'reply-to-name',
                'in-reply-to', 'references', 'thread-type',
                'flags' => array('bounce', 'auto-reply', 'spam', 'viral'),
                'recipients' => array('*' => array('name', 'email', 'source'))
            ));

            $supported['attachments']['*'][] = 'cid';
        }

        return $supported;
    }

    /*
      Validate data - overwrites parent's validator for additional validations.
     */

    function validate(&$data, $format, $strict = true) {
        global $ost;

        //Call parent to Validate the structure
        if (!parent::validate($data, $format, $strict) && $strict)
            $this->exerr(400, __('Unexpected or invalid data received'));

        // Use the settings on the thread entry on the ticket details
        // form to validate the attachments in the email
        $tform = TicketForm::objects()->one()->getForm();
        $messageField = $tform->getField('message');
        $fileField = $messageField->getWidget()->getAttachments();

        // Nuke attachments IF API files are not allowed.
        if (!$messageField->isAttachmentsEnabled())
            $data['attachments'] = array();

        //Validate attachments: Do error checking... soft fail - set the error and pass on the request.
        if ($data['attachments'] && is_array($data['attachments'])) {
            foreach ($data['attachments'] as &$file) {
                if ($file['encoding'] && !strcasecmp($file['encoding'], 'base64')) {
                    if (!($file['data'] = base64_decode($file['data'], true)))
                        $file['error'] = sprintf(__('%s: Poorly encoded base64 data'), Format::htmlchars($file['name']));
                    $file['encoding'] = "";
                }
                // Validate and save immediately
                try {
                    $F = $fileField->uploadAttachment($file);
                    $file['id'] = $F->getId();
                } catch (FileUploadError $ex) {
                    $file['error'] = $file['name'] . ': ' . $ex->getMessage();
                }
            }
            unset($file);
        }

        return true;
    }

    function create($format) {

        if (!($key = $this->requireApiKey()) || !$key->canCreateTickets())
            return $this->exerr(401, __('API key not authorized'));

        $ticket = null;
        if (!strcasecmp($format, 'email')) {
            # Handle remote piped emails - could be a reply...etc.
            $ticket = $this->processEmail();
        } else {
            # Parse request body
            $ticket = $this->createTicket($this->getRequest($format));

            //Creating Task
            $dataTask = $this->getRequest($format);
            $dataTask = $dataTask["task"];
            $dataTask["ticketId"] = $ticket->getId();
            $dataTask["sla"] = $ticket->getSLA();
            $task = $this->createTask($dataTask);
        }

        if (!$ticket)
            return $this->exerr(500, __("Unable to create new task: unknown error"));
        if (!$task)
            return $this->exerr(500, __("Unable to create new Task: unknown error"));

        $this->response(201, $ticket->getNumber());
    }

    /* private helper functions */

    function createTask($data) {
         if(empty($data)){
            $this->response(401, 'No Mapping Found');
        }
        # Pull off some meta-data
        $alert = (bool) (isset($data['alert']) ? $data['alert'] : true);
        $autorespond = (bool) (isset($data['autorespond']) ? $data['autorespond'] : true);

        # Assign default value to source if not defined, or defined as NULL
        $data['source'] = isset($data['source']) ? $data['source'] : 'API';
        $dataTask = array();
        # Create the task with the data (attempt to anyway)
        $taskType = DynamicList::lookup(array("name" => "Task Types"))->item;
        $dataTask = array(
            "object_type" => "T",
            "object_id" => $data["ticketId"],
            "creater" => "System",
            "internal_formdata" => array(
                "dept_id" => $data["deptId"],
            ),
            "default_formdata" => array(
                "taskType" => $data["type"],
                "title" => array_values($data["title"])[0],
                "description" => array_values($data["title"])[0],
                "taskSource" => $data["source"]
            )
        );
        if (is_numeric($data["type"])) {
            $taskTypeId = $data["type"];
            $slaObj = TaskSLA::lookup($taskTypeId);
        }
        if ($slaObj && $slaObj->sla_id) {
            $slaId = $slaObj->sla_id;
            $dataTask["internal_formdata"]["sla"] = $slaId;
        } else {
            $dataTask["internal_formdata"]["duedate"] = $data["duedate"];
        }


        $task = Task::create($dataTask);

        # Return errors (?)
        if (count($errors)) {
            if (isset($errors['errno']) && $errors['errno'] == 403)
                return $this->exerr(403, __('Task denied'));
            else
                return $this->exerr(
                                400, __("Unable to create new task: validation errors") . ":\n"
                                . Format::array_implode(": ", "\n", $errors)
                );
        } elseif (!$task) {
            return $this->exerr(500, __("Unable to create new ticket: unknown error"));
        }

        return $task;
    }

    /* private helper functions */


    /*     * ********************************
     * function update()
     * -For updating os task from API.
     * @params - $format
     * @author - Akash Kumar
     * ** */

    function update($format) {
        if (!($key = $this->getApiKey()))
            $this->response(401, json_encode(array("status" => "error", "msg" => "Valid API key required")));
        elseif (!$key->isActive())
            $this->response(401, json_encode(array("status" => "error", "msg" => "API key not found/active or source IP not authorized")));


        # Parse request body
        $ticket = $this->updateTask($this->getRequest($format));
        $ticket = json_encode($ticket);
        $this->response(201, $ticket);
    }

    /*     * ********************************* 
     * Function -updateTicket: 
     * @params  -$data
     * @author  - Akash Sachdeva
     *  **** */

    function updateTask($data) {
        if (!($key = $this->getApiKey()))
            $this->response(401, json_encode(array("status" => "error", "msg" => "Valid API key required")));
        elseif (!$key->isActive())
            $this->response(401, json_encode(array("status" => "error", "msg" => "API key not found/active or source IP not authorized")));

        $logConfig = logConfig();
        $domain = $logConfig["domain"]["D2"];
        $module = $logConfig["module"][$domain]["D2_M3"];
        $error = $logConfig["error"]["E12"];
        $logObj = new Analog_logger();
        $logObj->report($domain, $module, $logConfig["level"]["INFO"], $error, "title:task Update API", "log_location:api.task.php", "", "", substr(json_encode($data), 0, 2000));
        # Pull off some meta-data
        $alert = (bool) (isset($data['alert']) ? $data['alert'] : true);
        $autorespond = (bool) (isset($data['autorespond']) ? $data['autorespond'] : true);
        # Assign default value to source if not defined, or defined as NULL
        $data['source'] = isset($data['source']) ? $data['source'] : 'API';
        # Create the ticket with the data (attempt to anyway)
        $errors = array();
        $taskIdArr = explode(",", $data["taskId"]);
        foreach ($taskIdArr as $taskId) {
            $data["taskId"] = $taskId;
            $updateresult = Task::updateTask($data, $errors, $data['source'], $autorespond, $alert);
        }
        if (count($errors)) {
            $this->response(400, json_encode(array("status" => "error", "error_msg" => $errors, "msg" => $updateresult)));
        }
        $this->response(200, json_encode($updateresult));
    }

    function getMoreInfo() {
        global $cfg;
        if (!($key = $this->getApiKey()))
            $this->response(401, json_encode(array("status" => "error", "msg" => "Valid API key required")));
        elseif (!$key->isActive())
            $this->response(401, json_encode(array("status" => "error", "msg" => "API key not found/active or source IP not authorized")));


        $ticket_no = $_GET['ticket_no'];
        $type = $cfg->config['more_info_seller_task']['value'];
        $task = json_decode($type,true);
        $taskType=$task['taskType'];
        $query = "SELECT TA.flags as status,TA.id as task_id,O.order_id,O.manifest_id,O.product_id,O.batch_id,O.extra,O.is_attachment_required,TA.promised_duedate FROM " . TICKET_TABLE . " T INNER JOIN " . TASK_DATA_TABLE . " O ON T.ticket_id=O.ticket_id INNER JOIN " .
                TASK_TABLE . " TA ON TA.id=O.task_id WHERE O.taskType=$taskType " .
                " AND T.number=$ticket_no AND O.extra is not null AND TA.flags=1";
        $res = db_query($query);
       if(!db_num_rows($res)){
           $this->response(404, json_encode(array("status" => "error", "msg" => "No Task found")));
        }
     
       while ($result = db_fetch_array($res)) {
            $extra = json_decode($result['extra'], true);         
       $list = getCachedAPIFields();
            $image_array = array();
            foreach ($extra['imageTemplateVariable'] as $val) {
                $template_id = $val;
                $image = $list['Default_Task Image Type'][$template_id];
                array_push($image_array, $image);
            }
            if (!empty($result['manifest_id'])) {
                $mandatory = "manifest";
                $mandatory_id=$result['manifest_id'];
            } elseif (!empty($result['order_id'])) {
                $mandatory = "order";
                $mandatory_id=$result['order_id'];
            } elseif (!empty($result['product_id'])) {
                $mandatory = "product";
                $mandatory_id=$result['product_id'];
            } elseif (!empty($result['batch_id'])){
               $mandatory = "batch";
               $mandatory_id=$result['batch_id'];
            }


         $array=array("image"=>$image_array, "due_date"=>$result['promised_duedate'],"status"=>$result['status'],"task_id"=>$result['task_id'],"attachment_flag"=>$result['is_attachment_required']);
          if($mandatory){
          $array[$mandatory]=$mandatory_id;
          }


 $arr[] = $array;
}

        $this->response(200, json_encode(array('status' => "success",'info'=>$arr)));
    }

    function postMoreInfo($format) {
       global $cfg;
        if (!($key = $this->getApiKey()))
            $this->response(401, json_encode(array("status" => "error", "msg" => "Valid API key required")));
        elseif (!$key->isActive())
            $this->response(401, json_encode(array("status" => "error", "msg" => "API key not found/active or source IP not authorized")));

        $data = $this->getRequest($format);
        $merchant_comments = $data['comment'];

        $type = $cfg->config['more_info_seller_task']['value'];
        $task = json_decode($type,true);
        $taskType=$task['taskType'];

        $task_id=$data['task_id']; 
        $sql="SELECT O.task_id,T.user_id FROM " . TICKET_TABLE . " T INNER JOIN " . TASK_DATA_TABLE . " O ON T.ticket_id=O.ticket_id " .
                          " WHERE O.taskType=$taskType " .
        " AND O.task_id=$task_id";
        $res=  db_query($sql);
        $result = db_fetch_array($res);
        $t = Task::lookup($result['task_id']);   
        $ticket = Ticket::lookupByNumber($data["number"]);
	$triageData=[];
	$triageData["ticket_id"]=$ticket->getId();
	$ticket->changeTriage('merchant','more_info');
//	$ticket->triageTicket($triageData,'more_info');
        $outcome=$t->taskAttachments($data,NULL,$result['user_id']);
        $ticket->attachments($data['attachment']);
     
        $query="UPDATE ".TASK_DATA_TABLE." SET customer_comments='".$merchant_comments."' WHERE task_id=".$result['task_id'];
        if(!db_query($query)){
            return $this->exerr(500, __("Unable to save merchant comments: Database error"));
        }
      //response

                $responseData = array();
                $responseData["taskId"] = $task_id;
                $responseData["agent"] = "STOREMANGER- On more info";
                $responseData["email"] = "system.auto@shopclues.com";
                $responseData['source'] = 'API';
                $responseData["taskResponse"] = $data['autoResponse'];

                $output=Task::updateTask($responseData, $errors, $responseData['source']);
        
         return $this->exerr(201, json_encode(array('status' => "success")));
    }

}

?>
