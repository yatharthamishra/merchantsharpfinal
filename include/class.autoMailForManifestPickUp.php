<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author shopclues
 */
class ManifestPickUp {

    private $config;

    function __construct() {
        global $cfg;
        $this->config = $cfg;//json_decode($cfg->config['manifestPickUp']['value'],TRUE);
    }

    public function sendAutoMailer() {
        global $ost;
        $autoMailerAndData = $this->_getDataForAutoMailer();
        if(empty($autoMailerAndData[0])){
            return;
        }
        $autoMailerData = $autoMailerAndData[0];
        $manifestIds = $autoMailerAndData[1];
	$config = json_decode($this->config->config['manifestPickUp']['value'],TRUE);
	$templateIds = $config['manifestTemplateId'];
	$responseIds = $config['manifestResponseId'];
        foreach ($autoMailerData as $task) {
	    if(array_key_exists($task['data__manifest_id'],$manifestIds)) {
		if(!empty($manifestIds[$task['data__manifest_id']]['expected_pickup_date'])){
		    $dateDiff = abs(strtotime($manifestIds[$task['data__manifest_id']]['expected_pickup_date']) - strtotime($task['created']));
		    $dateDiff = floor($dateDiff / (60 * 60 * 24));
		}else{
		    continue;
		}
		if($dateDiff == 0) {
		    $tempId=EmailAutoMailer::lookup($templateIds['today']);
		    $template = new EmailTemplate($tempId->ht['template_id']);
		    $response = $responseIds['today'];
		}else if($dateDiff == 1 ) {
		    $tempId=EmailAutoMailer::lookup($templateIds['tomorrow']);
                    $template = new EmailTemplate($tempId->ht['template_id']);
		    $response = $responseIds['tomorrow'];
		}else if($dateDiff == 2) {
		    if(date('N', strtotime('+1 day',strtotime($task['created']))) > 6){
			$tempId=EmailAutoMailer::lookup($templateIds['dayAfterTomorrow']);
                        $template = new EmailTemplate($tempId->ht['template_id']);
		   	$response = $responseIds['dayAfterTomorrow'];
		    }
		}else {
		    continue;
		}
		$ticketObj = Ticket::lookup($task['odata__ticket_id']);
		$msg = $ticketObj->replaceVars(
                    	$template->asArray(), array('message' => "sdf",
                	'recipient' => $ticketObj->getOwner()
                   	 )
            	);
		//make a response on task ..
                $data = array();
                $data["taskId"] = $task['id'];
                $data["agent"] = "SYSTEM - On Task Reminder";
                $data["email"] = "system.auto@shopclues.com";
                $data['source'] = 'CRON';
                $data["taskResponse"] = $response;
                $output=Task::updateTask($data, $errors, $data['source']);
		
		//send the Email
           	$email = $ost->getConfig()->getDefaultEmail();
            	$email->send($ticketObj->getOwner(), Format::striptags($msg['subj']), $msg['body']);
            	$autoPostNote['note']="Subject - ".$msg['subj'].$msg['body'];
            	$ticketObj->postNote($autoPostNote);
		$sql = "UPDATE mst_config set `value`=".$task['id']." where id=399";
		db_query($sql); 
		echo 'Response and Email has been sent to Task: '.$task['id'];
		echo '<br>';	
	    }		
        }
    }

    private function _getDataForAutoMailer() {
        
	$config = json_decode($this->config->config['manifestPickUp']['value'],TRUE);
	$bucketSize = $config['autoMailerBucketSize'] ? $config['autoMailerBucketSize'] : 100;
	$deptId = $config['deptId'];
	$taskType = $config['taskType'];
	$taskOffset = $this->config->config['manifestPickUpOffset']['value'];
        $res = Task::objects()->filter(array('id__gt'=>$taskOffset,'odata__manifest_ids__isnull'=>0,'data__manifest_id__isnull'=>0,'dept_id'=>$deptId,'data__taskType'=>$taskType))->values('id','odata__ticket_id','data__manifest_id','created')->group_by('id')->order_by('id')->limit($bucketSize)->all();
        $manifestIds = [];
	$ticketIds = [];
        foreach($res as $data){
            $manifestIds[] = $data['data__manifest_id'];
        }
	$manifestIds = array_unique($manifestIds);
	$manifestData = OrderManifest::objects()->filter(array('manifest_id__in'=>$manifestIds))->values('manifest_id','date_created','expected_pickup_date')->all();
	$manifestMap = [];
	foreach($manifestData as $val){
		$manifestMap[$val['manifest_id']] = $val;
	} 
        return array($res, $manifestMap);
    }

}

?>
