<?php
/*********************************************************************
    mysqli.php

    Collection of MySQL helper interface functions.
    Mostly wrappers with error/resource checking.

    Peter Rotich <peter@osticket.com>
    Jared Hancock <jared@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
$__db = null; // OST Master DB Connection Object
$__slave_db = null; // OST Slave DB Connection Object
$__sclues_ost_db = null; // SCLUES OST Master DB Connection Object
$__last_db_used = null; // ost_master, ost_slave, sclues_ost... These are the possible options...
$__send_to_master = FALSE; // SET THIS VARIABLE TO TRUE JUST ABOVE YOUR QUERY, USE global $__send_to_master;
$__write_tables = array(); // All write tables will go here, if select query comes for one of these table, connection will be master in that case.

$__order_master_db = null;
$__order_slave_db = null;

$__catalog_master_db = null;
$__catalog_slave_db = null;

$__ff_master_db = null;
$__ff_slave_db = null;

$__ba_master_db = null;
$__ba_slave_db = null;

$__sclues_misc_db = null;
global $queryLog;
if(is_null($queryLog)){
    $queryLog=[];
}
global $last_query;
if(is_null($last_query)){
    $last_query="";
}

/**
 * Connect to DB mentioned in parameters..
 */
function __connect_to_sclues_db($connection_server) {
  global $db_osticket_config, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db;

  switch ($connection_server) {
    case 'order_master':
      if (empty($__order_master_db)) {
        // OST Shopclues Intermediate DB - some tables are in read mode and some are in write mode.
        $db_config = $db_osticket_config['newarch']['order']['servers']['master'];
        $__order_master_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__order_master_db->connect_error) {
          // Analog this error instead of die....
          logError('Order Master Connect Error: ' . $__order_master_db->connect_error);
        }

        $__order_master_db->query('SET NAMES "utf8"');
        $__order_master_db->query('SET CHARACTER SET "utf8"');
        $__order_master_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__order_master_db->set_charset('utf8');
        $__order_master_db->autocommit(true);

        return $__order_master_db;
      }
    break;
    case 'order_slave':
      if (empty($__order_slave_db)) {
        $db_config = $db_osticket_config['newarch']['order']['servers']['slave'];
        $__order_slave_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__order_slave_db->connect_error) {
          // Analog this error instead of die....
          logError('Order Slave Connect Error: ' . $__order_slave_db->connect_error);
        }

        $__order_slave_db->query('SET NAMES "utf8"');
        $__order_slave_db->query('SET CHARACTER SET "utf8"');
        $__order_slave_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__order_slave_db->set_charset('utf8');

        return $__order_slave_db;
      }
    break;
    case 'catalog_master':
      if (empty($__catalog_master_db)) {
        $db_config = $db_osticket_config['newarch']['catalog']['servers']['master'];
        $__catalog_master_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__catalog_master_db->connect_error) {
          // Analog this error instead of die....
          logError('Catalog Master Connect Error: ' . $__catalog_master_db->connect_error);
        }

        $__catalog_master_db->query('SET NAMES "utf8"');
        $__catalog_master_db->query('SET CHARACTER SET "utf8"');
        $__catalog_master_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__catalog_master_db->set_charset('utf8');
        $__catalog_master_db->autocommit(true);

        return $__catalog_master_db;
      }
    break;
    case 'catalog_slave':
      if (empty($__catalog_slave_db)) {
        $db_config = $db_osticket_config['newarch']['catalog']['servers']['slave'];
        $__catalog_slave_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__catalog_slave_db->connect_error) {
          // Analog this error instead of die....
          logError('Catalog Slave Connect Error: ' . $__catalog_slave_db->connect_error);
        }

        $__catalog_slave_db->query('SET NAMES "utf8"');
        $__catalog_slave_db->query('SET CHARACTER SET "utf8"');
        $__catalog_slave_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__catalog_slave_db->set_charset('utf8');

        return $__catalog_slave_db;
      }
    break;
    case 'ff_master':
      if (empty($__ff_master_db)) {
        $db_config = $db_osticket_config['newarch']['ff']['servers']['master'];
        $__ff_master_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__ff_master_db->connect_error) {
          // Analog this error instead of die....
          logError('FF Master Connect Error: ' . $__ff_master_db->connect_error);
        }

        $__ff_master_db->query('SET NAMES "utf8"');
        $__ff_master_db->query('SET CHARACTER SET "utf8"');
        $__ff_master_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__ff_master_db->set_charset('utf8');
        $__ff_master_db->autocommit(true);

        return $__ff_master_db;
      }
    break;
    case 'ff_slave':
      if (empty($__ff_slave_db)) {
        $db_config = $db_osticket_config['newarch']['ff']['servers']['slave'];
        $__ff_slave_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__ff_slave_db->connect_error) {
          // Analog this error instead of die....
          logError('FF Slave Connect Error: ' . $__ff_slave_db->connect_error);
        }

        $__ff_slave_db->query('SET NAMES "utf8"');
        $__ff_slave_db->query('SET CHARACTER SET "utf8"');
        $__ff_slave_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__ff_slave_db->set_charset('utf8');

        return $__ff_slave_db;
      }
    break;
    case 'ba_master':
      if (empty($__ba_master_db)) {
        $db_config = $db_osticket_config['newarch']['ba']['servers']['master'];
        $__ba_master_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__ba_master_db->connect_error) {
          // Analog this error instead of die....
          logError('Billing Master Connect Error: ' . $__ba_master_db->connect_error);
        }

        $__ba_master_db->query('SET NAMES "utf8"');
        $__ba_master_db->query('SET CHARACTER SET "utf8"');
        $__ba_master_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__ba_master_db->set_charset('utf8');
        $__ba_master_db->autocommit(true);

        return $__ba_master_db;
      }
    break;
    case 'ba_slave':
      if (empty($__ba_slave_db)) {
        $db_config = $db_osticket_config['newarch']['ba']['servers']['slave'];
        $__ba_slave_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__ba_slave_db->connect_error) {
          // Analog this error instead of die....
          logError('Billing Slave Connect Error: ' . $__ba_slave_db->connect_error);
        }

        $__ba_slave_db->query('SET NAMES "utf8"');
        $__ba_slave_db->query('SET CHARACTER SET "utf8"');
        $__ba_slave_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__ba_slave_db->set_charset('utf8');

        return $__ba_slave_db;
      }
    break;
    case 'sclues_misc':
      if (empty($__sclues_misc_db)) {
        $db_config = $db_osticket_config['newarch']['misc']['servers']['slave'];
        $__sclues_misc_db = new mysqli($db_config['db_host'], $db_config['db_user'], $db_config['db_password'], $db_config['db_name'], DBPORT);
        if ($__sclues_misc_db->connect_error) {
          // Analog this error instead of die....
          logError('Sclues Misc Connect Error: ' . $__sclues_misc_db->connect_error);
        }

        $__sclues_misc_db->query('SET NAMES "utf8"');
        $__sclues_misc_db->query('SET CHARACTER SET "utf8"');
        $__sclues_misc_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
        $__sclues_misc_db->set_charset('utf8');

        return $__sclues_misc_db;
      }
    break;
  }
}

function db_connect($host, $user, $passwd, $options = array()) {
  global $__db, $__slave_db, $__sclues_ost_db, $db_osticket_config;

  // print_r($db_osticket_config);exit;
  // MASTER DB settings....
  //
  if(empty($__db)){
  $__db = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME, DBPORT);
  if ($__db->connect_error) {
    // Analog this error instead of die....
    logError('OST Master Connect Error: ' . $__db->connect_error);
  }

  $__db->query('SET NAMES "utf8"');
  $__db->query('SET CHARACTER SET "utf8"');
  $__db->query('SET COLLATION_CONNECTION=utf8_general_ci');
  $__db->set_charset('utf8');

  // db_set_variable('sql_mode', '');

  $__db->autocommit(true);
  }
  // // Use connection timing to seed the random number generator
  Misc::__rand_seed((microtime(true) - $start) * 1000000);



  // SLAVE DB settings.....
  if(empty($__slave_db)){
  $__slave_db = new mysqli(SLAVE_DBHOST, SLAVE_DBUSER, SLAVE_DBPASS, SLAVE_DBNAME, SLAVE_DBPORT);
  if ($__slave_db->connect_error) {
    // Analog this error instead of die....
    logError('OST Slave Connect Error: ' . $__slave_db->connect_error);
  }

  $__slave_db->query('SET NAMES "utf8"');
  $__slave_db->query('SET CHARACTER SET "utf8"');
  $__slave_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
  $__slave_db->set_charset('utf8');
  }

  // OST Shopclues Intermediate DB - some tables are in read mode and some are in write mode.
  if(empty($__sclues_ost_db)){
  $__sclues_ost_db = new mysqli(SCLUES_OST_DBHOST, SCLUES_OST_DBUSER, SCLUES_OST_DBPASS, SCLUES_OST_DBNAME, SCLUES_OST_DBPORT);
  if ($__sclues_ost_db->connect_error) {
    // Analog this error instead of die....
    logError('OST Misc Connect Error: ' . $__sclues_ost_db->connect_error);
  }

  $__sclues_ost_db->query('SET NAMES "utf8"');
  $__sclues_ost_db->query('SET CHARACTER SET "utf8"');
  $__sclues_ost_db->query('SET COLLATION_CONNECTION=utf8_general_ci');
  $__sclues_ost_db->set_charset('utf8');

  $__sclues_ost_db->autocommit(true);
  }

  return $__db;
}

function db_autocommit($enable=true) {
  global $__db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__catalog_master_db, $__ff_master_db, $__ba_master_db,$__ost_slave_report; // ost_master, ost_slave, sclues_ost;

  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->autocommit($enable);
    break;
    case 'ost_master':
      return $__db->autocommit($enable);
    break;
    case 'order_master':
      return $__order_master_db->autocommit($enable);
    break;
    case 'catalog_master':
      return $__catalog_master_db->autocommit($enable);
    break;
    case 'ff_master':
      return $__ff_master_db->autocommit($enable);
    break;
    case 'ba_master':
      return $__ba_master_db->autocommit($enable);
    break;
    case 'ost_slave_report':
        return $__ost_slave_report->autocommit($enable);
  }

}

function db_rollback() {
  global $__db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__catalog_master_db, $__ff_master_db, $__ba_master_db,$__ost_slave_report;


  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->rollback($enable);
    break;
    case 'ost_master':
      return $__db->rollback($enable);
    break;
    case 'order_master':
      return $__order_master_db->rollback($enable);
    break;
    case 'catalog_master':
      return $__catalog_master_db->rollback($enable);
    break;
    case 'ff_master':
      return $__ff_master_db->rollback($enable);
    break;
    case 'ba_master':
      return $__ba_master_db->rollback($enable);
    break;
    case 'ost_slave_report':
        return $__ost_slave_report->rollback($enable);
  }

}

function db_close() {
  global $__db, $__slave_db, $__sclues_ost_db, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db;

  @$__slave_db->close();
  @$__sclues_ost_db->close();
  @$__order_master_db->close();
  @$__order_slave_db->close();
  @$__catalog_master_db->close();
  @$__catalog_slave_db->close();
  @$__ff_master_db->close();
  @$__ff_slave_db->close();
  @$__ba_master_db->close();
  @$__ba_slave_db->close();
  @$__sclues_misc_db->close();

  return @$__db->close();
}

function db_version() {
  $version=0;
  $matches = array();
  if(preg_match('/(\d{1,2}\.\d{1,2}\.\d{1,2})/', db_result(db_query('SELECT VERSION()')), $matches))
      $version=$matches[1];

  return $version;
}

function db_timezone() {
  return db_get_variable('system_time_zone', 'global');
}

function db_get_variable($variable, $type='session') {
  $sql =sprintf('SELECT @@%s.%s', $type, $variable);
  return db_result(db_query($sql));
}

function db_set_variable($variable, $value, $type='session') {
  $sql =sprintf('SET %s %s=%s',strtoupper($type), $variable, db_input($value));
  return db_query($sql);
}


function db_select_database($database) {
  global $__db;
  return true;//($database && @$__db->select_db($database));
}

function db_create_database($database, $charset='utf8', $collate='utf8_general_ci') {
  global $__db;
  return @$__db->query(sprintf('CREATE DATABASE %s DEFAULT CHARACTER SET %s COLLATE %s',
          $database, $charset, $collate));
}




/**
 * Check query is read or write as per first word of query...
 * @param  string $query
 * @return string read / write
 */
function _db_get_query_type ($query) {
  global $ost;
  $query_type = strtolower(current(explode(' ', $query)));
  $write_types_array = unserialize(OST_QUERY_WRITE_TYPES);
  $read_types_array = unserialize(OST_QUERY_READ_TYPES);

  if (in_array($query_type, $write_types_array)) {
    return 'write';
  }
  elseif (in_array($query_type, $read_types_array)) {
    require_once dirname(__FILE__) . '/../libraries/PHP-SQL-Parser/vendor/autoload.php';

    try {
      $parsed = new SqlParser\Parser($query);
    }
    catch (Exception $e) {
      $ost->logWarning('Query Parsing Error', json_encode(array($query, $e->getMessage())));
    }

    $query_read_write = '';
    if (!empty($parsed->statements)) {
      foreach ($parsed->statements as $statement_key => $statement_obj) {
        $statement_class = get_class($statement_obj);
        $statement_type = end(explode("\\", $statement_class));

        if ($statement_type == 'SelectStatement' &&
            ($query_read_write == '' || $query_read_write == 'read')) {
          $query_read_write = 'read';
        }

        if ($statement_type == 'UpdateStatement') {
          $query_read_write = 'write';
          break 1;
        }
      }

      return $query_read_write;
    }
    else {
      return 'read';
    }
  }
  else {
    return 'write';
  }
}


/**
 * Parse the quuery using phpmyadmin Query parser and find tables involved in query
 * @param  string $query
 * @return array
 */
function _db_get_query_tables($query) {
  // https://github.com/phpmyadmin/sql-parser
  require_once dirname(__FILE__) . '/../libraries/PHP-SQL-Parser/vendor/autoload.php';

  try {
    $parsed = new SqlParser\Parser($query);
  }
  catch (Exception $e) {
    $ost->logWarning('Query Parsing Error', json_encode(array($query, $e->getMessage())));
  }


  $table_names = array();

  if (!empty($parsed->statements)) {
    foreach ($parsed->statements as $statement_key => $statement_obj) {
      $statement_class = get_class($statement_obj);
      $statement_type = end(explode("\\", $statement_class));

      switch ($statement_type) {
        case 'SelectStatement':
          // If from is set...
          if (isset($statement_obj->from)) {
            foreach ($statement_obj->from as $from_key => $from_obj) {
              if (isset($from_obj->table) && $from_obj->table != ''
                  && !in_array($from_obj->table, $table_names)) {
                $table_names[] = $from_obj->table;
              }
            }
          }


          if (isset($statement_obj->join)) {
            foreach ($statement_obj->join as $join_key => $join_obj) {
              if (isset($join_obj->expr->table) && $join_obj->expr->table != ''
                  && !in_array($join_obj->expr->table, $table_names)) {
                $table_names[] = $join_obj->expr->table;
              }
            }
          }

          if (isset($statement_obj->where)) {
            foreach ($statement_obj->where as $where_key => $where_obj) {
              if (isset($where_obj->expr)) {
                // lower string and remove multiple spaces...
                $where_expr = preg_replace('!\s+!', ' ', $where_obj->expr);
                $where_expr_tmp = strtolower($where_expr);

                if (strstr($where_expr_tmp, 'select ')) { // If query is select query..
                  $where_expr_array = explode('(', $where_expr);
                  unset($where_expr_array[0]);

                  $where_expr = implode('(', $where_expr_array);

                  $parsed_subquery = new SqlParser\Parser($where_expr);
                  if (!empty($parsed_subquery->statements)) {
                    foreach ($parsed_subquery->statements as $sub_statement_key => $sub_statement_obj) {

                      if (isset($sub_statement_obj->from)) {
                        foreach ($sub_statement_obj->from as $sub_from_key => $sub_from_obj) {
                          if (isset($sub_from_obj->table) && $sub_from_obj->table != ''
                              && !in_array($sub_from_obj->table, $table_names)) {
                            $table_names[] = $sub_from_obj->table;
                          }
                        }
                      }


                      if (isset($sub_statement_obj->join)) {
                        foreach ($sub_statement_obj->join as $sub_join_key => $sub_join_obj) {
                          if (isset($sub_join_obj->expr->table) && $sub_join_obj->expr->table != '' && !in_array($sub_join_obj->expr->table, $table_names)) {
                            $table_names[] = $sub_join_obj->expr->table;
                          }
                        }
                      }
                    }
                  }


                }
              }
            }
          }
          break;
        case 'DeleteStatement':
          if (isset($statement_obj->from)) {
            foreach ($statement_obj->from as $from_key => $from_obj) {
              if (isset($from_obj->table) && $from_obj->table != ''
                  && !in_array($from_obj->table, $table_names)) {
                $table_names[] = $from_obj->table;
              }
            }
          }

          break;
        case 'InsertStatement':
          if (isset($statement_obj->into->dest->table)
              && !in_array($statement_obj->into->dest->table, $table_names)) {
              $table_names[] = $statement_obj->into->dest->table;
          }
          break;
        case 'ReplaceStatement':
          if (isset($statement_obj->into->dest->table)
              && !in_array($statement_obj->into->dest->table, $table_names)) {
              $table_names[] = $statement_obj->into->dest->table;
          }

          break;
        case 'UpdateStatement':
          if (isset($statement_obj->tables)) {
            foreach ($statement_obj->tables as $table_key => $table_obj) {
              if (isset($table_obj->table) && $table_obj->table != '' && !in_array($table_obj->table, $table_names)) {
                $table_names[] = $table_obj->table;
              }
            }
          }
          break;
        default:

          break;
      }
    }
  }

  return $table_names;
}

function _set_db_connection($connectionName){
    global $__dbConnectionToPick;
    switch ($connectionName){
        case 'report':
            $__dbConnectionToPick=[];
            $__dbConnectionToPick['report']=1;
            break;
        default : 
            break;
    }
}

function _connectToReportSlaveDb() {
    global $__ost_slave_report;
    if(!is_null($__ost_slave_report)){
        return $__ost_slave_report->connect_error;
    }
    // ost report SLAVE DB settings.....
    $__ost_slave_report = new mysqli(REPORT_SLAVE_DBHOST, REPORT_SLAVE_DBUSER, REPORT_SLAVE_DBPASS, REPORT_SLAVE_DBNAME, REPORT_SLAVE_DBPORT);
    
    if ($__ost_slave_report->connect_error) {
        
        // Analog this error instead of die....
        logError('OST Slave Report Connect Error: ' . $__ost_slave_report->connect_error);
        $__ost_slave_report->connect_error;
    }

    $__ost_slave_report->query('SET NAMES "utf8"');
    $__ost_slave_report->query('SET CHARACTER SET "utf8"');
    $__ost_slave_report->query('SET COLLATION_CONNECTION=utf8_general_ci');
    $__ost_slave_report->set_charset('utf8');
    return $__ost_slave_report->connect_error;
}
function _connectToReportSlaveWriteDb() {
    global $__ost_slave_report_write;
    if(!is_null($__ost_slave_report_write)){
        return $__ost_slave_report_write->connect_error;
    }
    // ost report SLAVE DB settings.....
    $__ost_slave_report_write = new mysqli(REPORT_SLAVE_WRITE_DBHOST, REPORT_SLAVE_WRITE_DBUSER, REPORT_SLAVE_WRITE_DBPASS, REPORT_SLAVE_WRITE_DBNAME, REPORT_SLAVE_WRITE_DBPORT);
    
    if ($__ost_slave_report_write->connect_error) {
        
        // Analog this error instead of die....
        logError('OST Slave Report Connect Error: ' . $__ost_slave_report_write->connect_error);
        $__ost_slave_report_write->connect_error;
    }

    $__ost_slave_report_write->query('SET NAMES "utf8"');
    $__ost_slave_report_write->query('SET CHARACTER SET "utf8"');
    $__ost_slave_report_write->query('SET COLLATION_CONNECTION=utf8_general_ci');
    $__ost_slave_report_write->set_charset('utf8');
    return $__ost_slave_report_write->connect_error;
}

function getQueryTypeAndTablestemp($query){
    global $__sqlmemcacheObject;
    if(!isset($__sqlmemcacheObject)){
        $__sqlmemcacheObject=new MemcacheStorage();
    }
    $connectionKeyToBeUsed=md5("connection:$query");
    if(!$_GET['flush']){
    $queryTypeTables=$__sqlmemcacheObject->getValue($connectionKeyToBeUsed);
    }
    if(($queryTypeTables)){
        return $queryTypeTables;
    }
   $query_type = _db_get_query_type($query);
   $tables_tmp = _db_get_query_tables($query);
   $memcacheVal=array($query_type,$tables_tmp);
   $__sqlmemcacheObject->setValue($connectionKeyToBeUsed,$memcacheVal);
   return $memcacheVal;
    
}
/**
 * Check connection server from the query type and past insert queries..
 * @param  string $query SQL Query
 * @return string
 */
function _db_get_connection_server($query) {
  global $__send_to_master, $__write_tables, $db_osticket_config, $db_osticket_sclues_tables,$__dbConnectionToPick;
  $queryTypeAndTablesInfo=  getQueryTypeAndTablestemp($query);
  $query_type = $queryTypeAndTablesInfo[0];
  $tables_tmp = $queryTypeAndTablesInfo[1];
  if($query_type=='read' && $__dbConnectionToPick['report']==1){
      $connectionError=_connectToReportSlaveDb();
      if($connectionError){
          return 'ost_slave';
      }
      return 'ost_slave_report';
  }
  if($query_type=='write' && $__dbConnectionToPick['report']==1){
      _connectToReportSlaveWriteDb();
      return 'ost_slave_report_write';
  }
  
  // Remove backtick from table names if exists....
  $tables = array();
  foreach ($tables_tmp as $table_key => $table_value) {
    $tables[] = trim($table_value, '`');
  }

  $connection = ''; // Initialize connection name, kept empty intentionally...

  if ($query_type == 'write') {
    foreach ($tables as $table_index => $table_value) {
      if (!in_array($table_value, $__write_tables)) {
        $__write_tables[] = $table_value;
      }
    }
  }

  // Query write, query tables exists in ost_misc write array
  $ost_misc_write_tables = $db_osticket_config['newarch']['ost_misc']['write_tables'];
  if ($query_type == 'write' && count($tables) > 0 && count(array_intersect($tables, $ost_misc_write_tables)) == count($tables)) {
    $connection = 'sclues_ost';
  }

  // Query read, query tables exists in ost_misc, if master set or tables in write pool
  if ($query_type == 'read' && count($tables) > 0 && count(array_intersect($tables, $ost_misc_write_tables)) == count($tables)) {
    if ($__send_to_master || count(array_intersect($tables, $__write_tables)) == count($tables)) {
      $connection = 'sclues_ost';
    }
    else {
      $connection = 'ost_slave';
    }
  }


  $ost_write_tables = $db_osticket_config['newarch']['ost']['write_tables'];
  if ($query_type == 'write' && count($tables) > 0 && count(array_intersect($tables, $ost_write_tables)) == count($tables)) {
    $connection = 'ost_master';
  }

  if ($query_type == 'read' && count($tables) > 0 && count(array_intersect($tables, $ost_write_tables)) == count($tables)) {
    if ($__send_to_master || count(array_intersect($tables, $__write_tables)) == count($tables)) {
      $connection = 'ost_master';
    }
    else {
      $connection = 'ost_slave';
    }
  }


  $order_write_tables = $db_osticket_config['newarch']['order']['tables'];
  if ($query_type == 'write' && count($tables) > 0 && count(array_intersect($tables, $order_write_tables)) == count($tables)) {
    $connection = 'order_master';
  }

  if ($query_type == 'read' && count($tables) > 0 && count(array_intersect($tables, $order_write_tables)) == count($tables)) {
    if ($__send_to_master || count(array_intersect($tables, $__write_tables)) == count($tables)) {
      $connection = 'order_master';
    }
    else {
      $connection = 'order_slave';
    }
  }


  $catalog_write_tables = $db_osticket_config['newarch']['catalog']['tables'];
  if ($query_type == 'write' && count($tables) > 0 && count(array_intersect($tables, $catalog_write_tables)) == count($tables)) {
    $connection = 'catalog_master';
  }

  if ($query_type == 'read' && count($tables) > 0 && count(array_intersect($tables, $catalog_write_tables)) == count($tables)) {
    if ($__send_to_master || count(array_intersect($tables, $__write_tables)) == count($tables)) {
      $connection = 'catalog_master';
    }
    else {
      $connection = 'catalog_slave';
    }
  }


  $ff_write_tables = $db_osticket_config['newarch']['ff']['tables'];
  if ($query_type == 'write' && count($tables) > 0 && count(array_intersect($tables, $ff_write_tables)) == count($tables)) {
    $connection = 'ff_master';
  }

  if ($query_type == 'read' && count($tables) > 0 && count(array_intersect($tables, $ff_write_tables)) == count($tables)) {
    if ($__send_to_master || count(array_intersect($tables, $__write_tables)) == count($tables)) {
      $connection = 'ff_master';
    }
    else {
      $connection = 'ff_slave';
    }
  }


  $ba_write_tables = $db_osticket_config['newarch']['ba']['tables'];
  if ($query_type == 'write' && count($tables) > 0 && count(array_intersect($tables, $ba_write_tables)) == count($tables)) {
    $connection = 'ba_master';
  }

  if ($query_type == 'read' && count($tables) > 0 && count(array_intersect($tables, $ba_write_tables)) == count($tables)) {
    if ($__send_to_master || count(array_intersect($tables, $__write_tables)) == count($tables)) {
      $connection = 'ba_master';
    }
    else {
      $connection = 'ba_slave';
    }
  }

  if (empty($connection) && $query_type == 'read' && count($tables) > 0  && count(array_intersect($tables, $db_osticket_sclues_tables)) == count($tables)) {
    // print $query;exit;
      $connection = 'sclues_misc';
  }
  
  if(empty($connection) && $query_type == 'read' && count($tables) > 0 && (stristr((implode(",",$tables)),"lues_") || stristr((implode(",",$tables)),"scart_") )){
      $connection = 'sclues_misc';
  }

  // Defaults to OST Slave if no match found...
  if (empty($connection)) {
    $connection = 'ost_slave';
  }
//echo $query."---------".$connection.'----';var_dump((stristr((implode(",",$tables)),"lues_") || stristr((implode(",",$tables)),"scart_") ));echo "<br><br><br><br>";
  return $connection;
}



/**
 * Function: db_query
 * Execute sql query
 *
 * Parameters:
 * $query - (string) SQL query (with parameters)
 * $logError - (mixed):
 *      - (bool) true or false if error should be logged and alert email sent
 *      - (callable) to receive error number and return true or false if
 *      error should be logged and alert email sent. The callable is only
 *      invoked if the query fails.
 *
 * Returns:
 * (mixed) MysqliResource if SELECT query succeeds, true if an INSERT,
 * UPDATE, or DELETE succeeds, false or null if the query fails.
 */
function db_query($query, $logError=true, $buffered=true) {
  global $ost, $__db, $__slave_db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db,$__ost_slave_report,$__ost_slave_report_write;
  $query=trim($query);
  $queryType=explode(" ", $query);
  if(strtolower($queryType[0])=="drop"){
      return;
  }
  if(strtolower($queryType[0])=="delete"){
      $msg='['.$query.']'."\n\n".db_error();
      $ost->logDeleteQueries('Delete Query #'.db_errno(), $msg);
  }
  $connection_server = _db_get_connection_server($query);
  $__last_db_used = $connection_server;

  // file_put_contents(dirname(__FILE__) .'/../test.log', $query."\n\n", FILE_APPEND);
  $startTime=microtime(true);
  switch ($connection_server) {

    case 'sclues_ost':
      ## sclues ost misc server queries.....

      if ($__sclues_ost_db->unbuffered_result) {
        $__sclues_ost_db->unbuffered_result->free();
        $__sclues_ost_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__sclues_ost_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__sclues_ost_db->errno==2006){
            $__sclues_ost_db=NULL;
            db_connect();
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__sclues_ost_db->errno == 1213 || $__sclues_ost_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__sclues_ost_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__sclues_ost_db->unbuffered_result = $res;
      }

    break;

    case 'ost_master':
      ## OST Master queries.....

      if ($__db->unbuffered_result) {
        $__db->unbuffered_result->free();
        $__db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__db->errno==2006){
            $__db=NULL;
            db_connect();
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__db->errno == 1213 || $__db->errno==2006 ));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__db->unbuffered_result = $res;
      }

    break;

    case 'order_master':

    __connect_to_sclues_db($connection_server);
      ## sclues ost misc server queries.....

      if ($__order_master_db->unbuffered_result) {
        $__order_master_db->unbuffered_result->free();
        $__order_master_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__order_master_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__order_master_db->errno==2006){
            $__order_master_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__order_master_db->errno == 1213 || $__order_master_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__order_master_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__order_master_db->unbuffered_result = $res;
      }


    # code...
    break;
    case 'order_slave':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__order_slave_db->unbuffered_result) {
        $__order_slave_db->unbuffered_result->free();
        $__order_slave_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__order_slave_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__order_slave_db->errno==2006){
            $__order_slave_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__order_slave_db->errno == 1213 || $__order_slave_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__order_slave_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__order_slave_db->unbuffered_result = $res;
      }

    break;
    case 'catalog_master':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__catalog_master_db->unbuffered_result) {
        $__catalog_master_db->unbuffered_result->free();
        $__catalog_master_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__catalog_master_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
         if($__catalog_master_db->errno==2006){
            $__catalog_master_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__catalog_master_db->errno == 1213 || $__catalog_master_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__catalog_master_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__catalog_master_db->unbuffered_result = $res;
      }

    break;
    case 'catalog_slave':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__catalog_slave_db->unbuffered_result) {
        $__catalog_slave_db->unbuffered_result->free();
        $__catalog_slave_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__catalog_slave_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__catalog_slave_db->errno==2006){
            $__catalog_slave_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__catalog_slave_db->errno == 1213 || $__catalog_slave_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__catalog_slave_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__catalog_slave_db->unbuffered_result = $res;
      }

    break;
    case 'ff_master':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__ff_master_db->unbuffered_result) {
        $__ff_master_db->unbuffered_result->free();
        $__ff_master_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__ff_master_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__ff_master_db->errno==2006){
            $__ff_master_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__ff_master_db->errno == 1213 || $__ff_master_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__ff_master_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__ff_master_db->unbuffered_result = $res;
      }

    break;
    case 'ff_slave':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__ff_slave_db->unbuffered_result) {
        $__ff_slave_db->unbuffered_result->free();
        $__ff_slave_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__ff_slave_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__ff_slave_db->errno==2006){
            $__ff_slave_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__ff_slave_db->errno == 1213 || $__ff_slave_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__ff_slave_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__ff_slave_db->unbuffered_result = $res;
      }

    break;
    case 'ba_master':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__ba_master_db->unbuffered_result) {
        $__ba_master_db->unbuffered_result->free();
        $__ba_master_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__ba_master_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__ba_master_db->errno==2006){
            $__ba_master_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__ba_master_db->errno == 1213 || $__ba_master_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__ba_master_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__ba_master_db->unbuffered_result = $res;
      }

    break;
    case 'ba_slave':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__ba_slave_db->unbuffered_result) {
        $__ba_slave_db->unbuffered_result->free();
        $__ba_slave_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__ba_slave_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__ba_slave_db->errno==2006){
            $__ba_slave_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__ba_slave_db->errno == 1213 || $__ba_slave_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__ba_slave_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__ba_slave_db->unbuffered_result = $res;
      }

    break;
    case 'sclues_misc':
      __connect_to_sclues_db($connection_server);

      ## sclues ost misc server queries.....

      if ($__sclues_misc_db->unbuffered_result) {
        $__sclues_misc_db->unbuffered_result->free();
        $__sclues_misc_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__sclues_misc_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__sclues_misc_db->errno==2006){
            $__sclues_misc_db=NULL;
            __connect_to_sclues_db($connection_server);
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__sclues_misc_db->errno == 1213 || $__sclues_misc_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__sclues_misc_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__sclues_misc_db->unbuffered_result = $res;
      }

    break;
    case 'ost_slave_report':
            global $__ost_slave_report;
            $res=queryInDbMultipleTimesAndReportIfError($__ost_slave_report,$query,$buffered, 3,$logError);
            break;
    case 'ost_slave_report_write':
        global $__ost_slave_report_write;
         $res=queryInDbMultipleTimesAndReportIfError($__ost_slave_report_write,$query,$buffered, 3,$logError);
         break;
    default:
    ## OST Slave Queries....
      if ($__slave_db->unbuffered_result) {
          $__slave_db->unbuffered_result->free();
          $__slave_db->unbuffered_result = false;
      }

      $tries = 3;
      do {
        $res = $__slave_db->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        if($__slave_db->errno==2006){
            $__slave_db=NULL;
            db_connect();
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && ($__slave_db->errno == 1213 || $__slave_db->errno==2006));

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($__slave_db->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $__slave_db->unbuffered_result = $res;
      }

    break;
  }
  $endTime=  microtime(true);
  logAllQueries($query, $startTime, $endTime,  debug_backtrace());
  return $res;
}

function queryInDbMultipleTimesAndReportIfError($dbConnectionObject,$query,$buffered,$numberOfTries=3,$logError=true){
    global $ost;
     if ($dbConnectionObject->unbuffered_result) {
        $dbConnectionObject->unbuffered_result->free();
        $dbConnectionObject->unbuffered_result = false;
      }

      $tries = $numberOfTries;
      do {
        $res = $dbConnectionObject->query($query, $buffered ? MYSQLI_STORE_RESULT : MYSQLI_USE_RESULT);
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && $dbConnectionObject->errno == 1213);

      if(!$res && $logError && $ost) { //error reporting
        // Allow $logError() callback to determine if logging is necessary
        if (is_callable($logError) && !($logError($dbConnectionObject->errno)))
            return $res;

        $msg='['.$query.']'."\n\n".db_error();
        $ost->logDBError('DB Error #'.db_errno(), $msg);
        //echo $msg; #uncomment during debuging or dev.
      }

      if (is_object($res) && !$buffered) {
        $dbConnectionObject->unbuffered_result = $res;
      }
      return $res;

}

function prepareQueryInDbMultpleTimes(&$dbConnectionObject,$query,$connection_server,$connectToShopclueDb=0,$numberOfTries=3){
      $tries = $numberOfTries;
      do {
        $res = $dbConnectionObject->prepare($query);
        $query.=" ";
        if((!$dbConnectionObject) || $dbConnectionObject->errno==2006){
            $dbConnectionObject=null;
            if($connectToShopclueDb){
                __connect_to_sclues_db($connection_server);
            }else{
                  db_connect();
            }
        }
        // Retry the query due to deadlock error (#1213)
        // TODO: Consider retry on #1205 (lock wait timeout exceeded)
        // TODO: Log warning
      } while (!$res && --$tries && $dbConnectionObject->errno!=1064); // syntax error case 
      return $res;
}

function db_query_unbuffered($sql, $logError=false) {
  return db_query($sql, $logError, true);
}

function db_squery($query) { //smart db query...utilizing args and sprintf
  $args  = func_get_args();
  $query = array_shift($args);
  $query = str_replace("?", "%s", $query);
  $args  = array_map('db_real_escape', $args);
  array_unshift($args, $query);
  $query = call_user_func_array('sprintf', $args);
  return db_query($query);
}

function db_count($query) {
    return db_result(db_query($query));
}

function db_result($res, $row=false) {
  if (!$res)
    return NULL;

  if ($row !== false)
    $res->data_seek($row);

  list($value) = db_output($res->fetch_row());
  return $value;
}

function db_fetch_array($res, $mode=MYSQLI_ASSOC) {
  return ($res) ? db_output($res->fetch_array($mode)) : NULL;
}

function db_fetch_row($res) {
  return ($res) ? db_output($res->fetch_row()) : NULL;
}

function db_fetch_field($res) {
  return ($res) ? $res->fetch_field() : NULL;
}

function db_assoc_array($res, $mode=false) {
  $result = array();
  if($res && db_num_rows($res)) {
    while ($row=db_fetch_array($res, $mode))
        $result[]=$row;
  }
  return $result;
}

function db_num_rows($res) {
  return ($res) ? $res->num_rows : 0;
}

function db_affected_rows() {
  global $__db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__catalog_master_db, $__ff_master_db, $__ba_master_db,$__ost_slave_report,$__ost_slave_report_write;

  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->affected_rows;
    break;
    case 'ost_master':
      return $__db->affected_rows;
    break;
    case 'order_master':
      return $__order_master_db->affected_rows;
    break;
    case 'catalog_master':
      return $__catalog_master_db->affected_rows;
    break;
    case 'ff_master':
      return $__ff_master_db->affected_rows;
    break;
    case 'ba_master':
      return $__ba_master_db->affected_rows;
    break;
    case 'ost_slave_report':
        return $__ost_slave_report->affected_rows;
    case 'ost_slave_report_write':
        return $__ost_slave_report_write->affected_rows;
  }

  return 0; // Default
}

function db_data_seek($res, $row_number) {
  return ($res && $res->data_seek($row_number));
}

function db_data_reset($res) {
  return db_data_seek($res, 0);
}

function db_insert_id() {
  global $__db, $__sclues_ost_db, $__last_db_used,$__ost_slave_report,$__ost_slave_report_write,$__slave_db;

  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->insert_id;
    break;
    case 'ost_master':
      return $__db->insert_id;
    break;
    case 'order_master':
      return $__order_master_db->insert_id;
    break;
    case 'catalog_master':
      return $__catalog_master_db->insert_id;
    break;
    case 'ff_master':
      return $__ff_master_db->insert_id;
    break;
    case 'ba_master':
      return $__ba_master_db->insert_id;
    case 'ost_slave_report':
        return $__ost_slave_report->insert_id;
    case 'ost_slave_report_write':
        return $__ost_slave_report_write->insert_id;
    case 'ost_slave':
        return $__slave_db->insert_id;
  }

  return 0; // Default
}

function db_free_result($res) {
  return ($res && $res->free());
}

function db_output($var) {
  static $no_magic_quotes = null;

  if (!isset($no_magic_quotes))
      $no_magic_quotes = !function_exists('get_magic_quotes_runtime') || !get_magic_quotes_runtime();

  if ($no_magic_quotes) //Sucker is NOT on - thanks.
      return $var;

  if (is_array($var))
      return array_map('db_output', $var);

  return (!is_numeric($var))?stripslashes($var):$var;
}

//Do not call this function directly...use db_input
function db_real_escape($val, $quote=false) {
  global $__slave_db;

  //Magic quotes crap is taken care of in main.inc.php
  $val=$__slave_db->real_escape_string($val);

  return ($quote)?"'$val'":$val;
}

function db_input($var, $quote=true) {
  if(is_array($var))
    return array_map('db_input', $var, array_fill(0, count($var), $quote));
  elseif($var && preg_match("/^(?:\d+\.\d+|[1-9]\d*)$/S", $var))
    return $var;

  return db_real_escape($var, $quote);
}

function db_field_type($res, $col=0) {
  global $__db;
  return $res->fetch_field_direct($col);
}



function db_prepare($stmt) {
    // file_put_contents(dirname(__FILE__) .'/../test.log', $stmt."\n\n", FILE_APPEND);
  global $ost, $__db, $__slave_db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db,$__ost_slave_report,$__ost_slave_report_write;

  $connection_server = _db_get_connection_server($stmt);
  $__last_db_used = $connection_server;

  switch ($connection_server) {
    case 'sclues_ost':
      $res=prepareQueryInDbMultpleTimes($__sclues_ost_db, $stmt,$connection_server,0);
      break;
    case 'ost_master':
      $res=prepareQueryInDbMultpleTimes($__db, $stmt,$connection_server,0);
      break;
    case 'order_master':
      __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__order_master_db, $stmt,$connection_server,1);
      break;
    case 'order_slave':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__order_slave_db, $stmt,$connection_server,1);
      break;
    case 'catalog_master':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__catalog_master_db, $stmt,$connection_server,1);
      break;
    case 'catalog_slave':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__catalog_slave_db, $stmt,$connection_server,1);
      break;
    case 'ff_master':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__ff_master_db, $stmt,$connection_server,1);
      break;
    case 'ff_slave':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__ff_slave_db, $stmt,$connection_server,1);
      break;
    case 'ba_master':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__ba_master_db, $stmt,$connection_server,1);
      break;
    case 'ba_slave':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__ba_slave_db, $stmt,$connection_server,1);
      break;
    case 'sclues_misc':
    __connect_to_sclues_db($connection_server);
      $res=prepareQueryInDbMultpleTimes($__sclues_misc_db, $stmt,$connection_server,1);
      break;
    case 'ost_slave_report':
        $res=$__ost_slave_report->prepare($stmt);
        break;
    case 'ost_slave_report_write':
        $res=$__ost_slave_report_write->prepare($stmt);
        break;
    default:
      $res=prepareQueryInDbMultpleTimes($__slave_db, $stmt,$connection_server,0);
  }


  if (!$res && $ost) {
    // Include a backtrace in the error email
    $msg='['.$stmt."]\n\n".db_error();
    $ost->logDBError('DB Error #'.db_errno(), $msg);
  }

  return $res;
}

function db_connect_error() {
  global $__db, $__slave_db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db,$__ost_slave_report,$__ost_slave_report_write;

  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->connect_error;
    break;
    case 'ost_master':
      return $__db->connect_error;
    break;
    case 'order_master':
      return $__order_master_db->connect_error;
    break;
    case 'order_slave':
      return $__order_slave_db->connect_error;
    break;
    case 'catalog_master':
      return $__catalog_master_db->connect_error;
    break;
    case 'catalog_slave':
      return $__catalog_slave_db->connect_error;
    break;
    case 'ff_master':
      return $__ff_master_db->connect_error;
    break;
    case 'ff_slave':
      return $__ff_slave_db->connect_error;
    break;
    case 'ba_master':
      return $__ba_master_db->connect_error;
    break;
    case 'ba_slave':
      return $__ba_slave_db->connect_error;
    break;
    case 'sclues_misc':
      return $__sclues_misc_db->connect_error;
    break;
    case 'ost_slave_report':
        return $__ost_slave_report->error;
    case 'ost_slave_report_write':
        return $__ost_slave_report_write->error;
    default:
      return $__slave_db->connect_error;
    break;
  }

}

function db_error() {
  global $__db, $__slave_db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db,$__ost_slave_report,$__ost_slave_report_write;

  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->error;
    break;
    case 'ost_master':
      return $__db->error;
    break;
    case 'order_master':
      return $__order_master_db->error;
    break;
    case 'order_slave':
      return $__order_slave_db->error;
    break;
    case 'catalog_master':
      return $__catalog_master_db->error;
    break;
    case 'catalog_slave':
      return $__catalog_slave_db->error;
    break;
    case 'ff_master':
      return $__ff_master_db->error;
    break;
    case 'ff_slave':
      return $__ff_slave_db->error;
    break;
    case 'ba_master':
      return $__ba_master_db->error;
    break;
    case 'ba_slave':
      return $__ba_slave_db->error;
    break;
    case 'sclues_misc':
      return $__sclues_misc_db->error;
    break;
    case 'ost_slave_report':
        return $__ost_slave_report->error;
    case 'ost_slave_report_write':
        return $__ost_slave_report_write->error;
    default:
      return $__slave_db->error;
    break;
  }

}

function db_errno() {
  global $__db, $__slave_db, $__sclues_ost_db, $__last_db_used, $__order_master_db, $__order_slave_db, $__catalog_master_db, $__catalog_slave_db, $__ff_master_db, $__ff_slave_db, $__ba_master_db, $__ba_slave_db, $__sclues_misc_db,$__ost_slave_report,$__ost_slave_report_write;

  switch ($__last_db_used) {
    case 'sclues_ost':
      return $__sclues_ost_db->errno;
    break;
    case 'ost_master':
      return $__db->errno;
    break;
    case 'order_master':
      return $__order_master_db->errno;
    break;
    case 'order_slave':
      return $__order_slave_db->errno;
    break;
    case 'catalog_master':
      return $__catalog_master_db->errno;
    break;
    case 'catalog_slave':
      return $__catalog_slave_db->errno;
    break;
    case 'ff_master':
      return $__ff_master_db->errno;
    break;
    case 'ff_slave':
      return $__ff_slave_db->errno;
    break;
    case 'ba_master':
      return $__ba_master_db->errno;
    break;
    case 'ba_slave':
      return $__ba_slave_db->errno;
    break;
    case 'sclues_misc':
      return $__sclues_misc_db->errno;
    break;
    case 'ost_slave_report':
        return $__ost_slave_report->errno;
    case 'ost_slave_report_write':
        return $__ost_slave_report_write->errno;
    default:
      return $__slave_db->errno;
    break;
  }

}
function logError($paramError){
    // Log Info for creation
    $logConfig = logConfig();
    $domain = $logConfig["domain"]["D4"];
    $module = $logConfig["module"][$domain]["D4_M1"];
    $error = $logConfig["error"]["E17"]?:"DB Connection Error";
    $logObj = new Analog_logger();
    $logObj->report($domain,$module, $logConfig["level"]["ERROR"], $error,"title:db error","log_location:mysqli.php","","",  substr(json_encode($paramError),0,2000));    
}

function logAllQueries($query,$startTime,$endTime,$backTrace){
    global $queryLog;
    global $last_query;
    
    $last_query=$query;
    $indexToBeUsed=  count($queryLog['mysql']);
    $queryLog['mysql'][$indexToBeUsed]['query']=$query;
    $timeTaken=$endTime-$startTime;
    $queryLog['mysql']['totalTimeTaken']+=$timeTaken;
    $queryLog['mysql'][$indexToBeUsed]['timeTaken']=round($timeTaken,5);
    $backTraceCount=  count($backTrace);
    $indexForBackTrace=-1;
    for($iterator=0 ; $iterator<$backTraceCount;$iterator++){
       if(strpos($backTrace[$iterator]['file'], 'class.orm.php')===false){
           $indexForBackTrace=$iterator;
           break;
       } 
    }
    if($indexForBackTrace==-1){
        $indexForBackTrace=0;
    }
    $queryLog['mysql'][$indexToBeUsed]['filePath']=$backTrace[$indexForBackTrace]['file'];
    $queryLog['mysql'][$indexToBeUsed]['lineNumber']=$backTrace[$indexForBackTrace]['line'];
}
function last_query(){
    global $last_query;
    return $last_query;
}
function mysqli_prepared_query($link,$sql,$typeDef = FALSE,$params = FALSE){ 
  if($stmt = mysqli_prepare($link,$sql)){ 
    if(count($params) == count($params,1)){ 
      $params = array($params); 
      $multiQuery = FALSE; 
    } else { 
      $multiQuery = TRUE; 
    }  
    
    if($typeDef){ 
      $bindParams = array();    
      $bindParamsReferences = array(); 
      $bindParams = array_pad($bindParams,(count($params,1)-count($params))/count($params),"");         
      foreach($bindParams as $key => $value){ 
        $bindParamsReferences[$key] = &$bindParams[$key];  
      } 
      array_unshift($bindParamsReferences,$typeDef); 
      $bindParamsMethod = new ReflectionMethod('mysqli_stmt', 'bind_param'); 
      $bindParamsMethod->invokeArgs($stmt,$bindParamsReferences); 
    }

    $result = array();
    foreach($params as $queryKey => $query){
      foreach($bindParams as $paramKey => $value){
        $bindParams[$paramKey] = $query[$paramKey];
}
      $queryResult = array();
      if(mysqli_stmt_execute($stmt)){
        $resultMetaData = mysqli_stmt_result_metadata($stmt);
        if($resultMetaData){
          $stmtRow = array();
          $rowReferences = array();
          while ($field = mysqli_fetch_field($resultMetaData)) {
            $rowReferences[] = &$stmtRow[$field->name];
          }
          mysqli_free_result($resultMetaData);
          $bindResultMethod = new ReflectionMethod('mysqli_stmt', 'bind_result');
          $bindResultMethod->invokeArgs($stmt, $rowReferences);
          while(mysqli_stmt_fetch($stmt)){
            $row = array();
            foreach($stmtRow as $key => $value){
              $row[$key] = $value;
            }
            $queryResult[] = $row;
          }
          mysqli_stmt_free_result($stmt);
        } else {
          $queryResult[] = mysqli_stmt_affected_rows($stmt);
        }
      } else {
        $queryResult[] = FALSE;
      }
      $result[$queryKey] = $queryResult;
    }
    mysqli_stmt_close($stmt);
  } else {
    $result = FALSE;
  }

  if($multiQuery){
    return $result;

 } else {
    return $result[0];
  }
}


?>
