
<style type="text/css">

    a:hover {
        color:#CC0033;
    }

    h1 {
        font: bold 14px Helvetica, Arial, sans-serif;
        color: #CC0033;
    }
    h2 {
        font: bold 14px Helvetica, Arial, sans-serif;
        color: #898989;
    }
    #containerPlugin {
        background: #eee;
        margin: 50px auto;
        width: 945px;
    }
    .importTable td, th {
        border: 1px solid black;
    }
    table.importTable {
        border-collapse: collapse;
    }
    .homeTable td, th {
        text-align: center;
        padding: 20px;
    }
    .button {
        display: block;
        width: 115px;
        height: 25px;
        background: #4E9CAF;
        padding: 10px;
        text-align: center;
        border-radius: 5px;
        color: white;
        font-weight: bold;
    }
    .buttonsmall {
        display: block;
        width: 200px;
        background: #bbb none repeat scroll 0% 0%;
        padding: 10px;
        text-align: center;
        border-radius: 5px;
        color: #FFF;
        font-weight: bold;
        margin-top: 10px;
    }
    #form 			{width:150px;margin: 0px auto;}
    #form input     {margin-bottom: 20px;}
</style>
<h1>Import Manager</h1>

<?php
/**
 * To Handle different pages of importing
 * 
 */
if (isset($_GET["page"])) {
    $page = $_GET["page"] . "-import.inc.php";
} else {
    $page = "import-home.inc.php";
}
include($page);
