<?php
require_once(INCLUDE_DIR . 'class.plugin.php');
require_once(INCLUDE_DIR . 'class.signal.php');
require_once(INCLUDE_DIR . 'class.app.php');

require_once('config.php');

//Table used for storing import logs
define('IMPORT_TABLE', TABLE_PREFIX . 'plugin_import');

//Plugin root directory
define('PLUGINS_ROOT', INCLUDE_DIR . 'plugins/');

define('IMPORT_PLUGIN_ROOT', PLUGINS_ROOT . 'import/');
define('IMPORT_INCLUDE_DIR', IMPORT_PLUGIN_ROOT . 'include/');
define('IMPORT_STAFFINC_DIR', IMPORT_INCLUDE_DIR . 'staff/');
define('IMPORT_CLIENTINC_DIR', IMPORT_INCLUDE_DIR . 'client/');

//Import plugin - installation & uninstallation
require_once(IMPORT_INCLUDE_DIR . 'class.import_install.php');

/**
 * This Plugin is used to import data from excel sheet
 * 
 * 
 * @package    OsTicket
 * @subpackage plugin - Import
 * @author     Akash Kumar <akashkuardce@gmail.com>
 */
class ImportPlugin extends Plugin {

    var $config_class = 'ImportConfig';

    function bootstrap() {
        if ($this->firstRun()) {
            $this->configureFirstRun();
        }

        $config = $this->getConfig();
    }

    /**
     * Creates menu links in the staff backend.
     */
    function createStaffMenu() {
        Application::registerStaffApp('Equipment', 'equipment.php', array(iconclass => 'equipment'));
        Application::registerStaffApp('Equipment Categories', 'equipment_categories.php', array(iconclass => 'faq-categories'));
        Application::registerStaffApp('Equipment Status', 'equipment_status.php', array(iconclass => 'equipment_status'));
    }

    /**
     * Creates menu link in the client frontend.
     * Useless as of OSTicket version 1.9.2.
     */
    function createFrontMenu() {
        Application::registerClientApp('Equipment Status', 'equipment_front/index.php', array(iconclass => 'equipment'));
    }

    /**
     * Checks if this is the first run of our plugin.
     * @return boolean
     */
    function firstRun() {
        $sql = 'SHOW TABLES LIKE \'' . IMPORT_TABLE . '\'';
        $res = db_query($sql);
        return (db_num_rows($res) == 0);
    }

    /**
     * Necessary functionality to configure first run of the application
     */
    function configureFirstRun() {
        if (!$this->createDBTables()) {
            echo "First run configuration error.  "
            . "Unable to create database tables!";
        }
    }

    /**
     * setting up database installation scripts
     * @return boolean
     */
    function createDBTables() {
        $installer = new ImportInstaller();
        return $installer->install();
    }

    /**
     * Uninstall Import databases and everything.
     * @param type $errors
     * @return boolean
     */
    function pre_uninstall(&$errors) {
        $installer = new ImportInstaller();
        return $installer->remove();
    }

}
