<div id="containerPlugin">
    <h2><u>Staff Import Log</u></h2><br /><br />
</div>


<?php
// Getting data from log table
$data = getLogData();

/**
 * Displaying Log data
 */
?>
<table class='importTable' width="100%" style="border:solid 1px #000;"><tr>
        <?php
        foreach (array_keys($data[0]) as $column) {
            echo "<th>" . $column . "</th>";
        }
        ?>
    </tr>
    <?php
    foreach ($data as $v) {
        echo "<tr>";
        foreach ($v as $k => $dataV) {
            if ($k == "data")
                $dataV = print_r(json_decode($dataV), true);
            echo "<td>" . $dataV . "</td>";
        }
        echo "</tr>";
    }
    ?>
</table>
<?php

/**
 * Function to get recrds from imported data
 * @param string $type staff import for time being
 * @return $data data from import
 */
function getLogData($type = 'staff') {
    $sql = "SELECT * FROM `" . IMPORT_TABLE . "` WHERE `import`='$type' ORDER BY time DESC";
    $res = db_query($sql);
    $noOfLogs = db_num_rows($res);
    while ($resArray = db_fetch_array($res)) {
        $data[] = $resArray;
    }
    return $data;
}
