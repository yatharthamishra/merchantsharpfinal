<?php

/**
 * Configration for import plugin
 * 
 * class : ImportConfig
 * @package    OsTicket
 * @subpackage plugin - Import
 * @author     Akash Kumar <akashkuardce@gmail.com>
 */
require_once(INCLUDE_DIR . '/class.plugin.php');
require_once(INCLUDE_DIR . '/class.forms.php');

class ImportConfig extends PluginConfig {

    /**
       * getter Function for configration options
       *
       * @return array of config options
       */
    function getOptions() {
        return array(
            'equipment_backend_enable' => new BooleanField(array(
                'id' => 'equipment_backend_enable',
                'label' => 'Enable Backend',
                'configuration' => array(
                    'desc' => 'Staff backend interface')
                    )),
            'equipment_backend_reenable' => new BooleanField(array(
                'id' => 'equipment_backend_reenable',
                'label' => 'Enable Backend re',
                'configuration' => array(
                    'desc' => 'Staff backend interface re')
                    )),
            'equipment_frontend_enable' => new BooleanField(array(
                'id' => 'equipment_frontend_enable',
                'label' => 'Enable Frontend',
                'configuration' => array(
                    'desc' => 'Client facing interface')
                    )),
        );
    }

    /**
       * This function will be called when configuration is submitted by user.
       *
       * @return true when processed
       */
    function pre_save(&$config, &$errors) {
        global $msg;

        if (!$errors)
            $msg = 'Configuration updated successfully';

        return true;
    }

}

?>
