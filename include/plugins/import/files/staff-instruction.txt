*********************************************
*         INSTRUCTION FOR STAFF IMPORT      *
*********************************************


*********************
*  Required Fields  *
*********************
username - Unique username for every staff
firstname - First name of staff 
lastname - Last Name of staff
email - Email Id of Staff for welcome email and further communication
dept_id - Department Ids i.e. 0 or 1 etc 0 is Unassigned
role_id - Role Id 0 is for all access


*********************
*  Optional Fields  *
*********************
phone - Phone Number of staff - In Numeric
mobile -  Numeric Mobile Number of staff
perms - Permissions of Staff, comma seperated like user.create,user.edit, faq.manage etc  
isadmin - 1 for true
islocked - 1 for true
isvisible - 1 for true
onvacation - 1 for true
assigned_only - 1 for true
backend
phone_ext - Phone extension of staff
signature
notes - Notes can be string
