<?php

/**
 * This Plugin is used to import data from excel sheet
 * 
 * 
 * @package    OsTicket
 * @subpackage plugin - Import
 * @author     Akash Kumar <akashkuardce@gmail.com>
 */
set_include_path(get_include_path().PATH_SEPARATOR.dirname(__file__).'/include');
/**
 * Returns an array of plugin details
 * @return array of plugin details
 */
return array(
 'id' => 'akashkumardce:import',
 'version' => '0.1',
 'name' => 'Import Manager',
 'functionality' => 'custom',
 'author' => 'Akash Kumar - shopclues',
 'description' => 'Import management capability.',
 'url' => 'https://github.com/akashkumardce/OsTicket-plugin-Import',
 'plugin' => 'import.php:ImportPlugin'
);
