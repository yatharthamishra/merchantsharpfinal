
<div id="containerPlugin">
    <h2><u>Staff Importer</u></h2><br /><br />


    <?php
    /**
     * Upload and import action
     * 
     */
    if (isset($_POST['submit'])) {
        ?>
        <div id="data">
            <?php
            if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
                echo "<h1>" . "File " . $_FILES['filename']['name'] . " Imported successfully." . "</h1>";
                echo "<h2>Displaying contents:</h2>";
                //readfile($_FILES['filename']['tmp_name']);
            }

            $data = get2DArrayFromCsv($_FILES['filename']['tmp_name'], ',');
            ?>
            <table class='importTable' width="100%" style="border:solid 1px #000;"><tr>
                    <?php
                    foreach (array_keys($data[0]) as $column) {
                        echo "<th>" . $column . "</th>";
                    }
                    ?>
                </tr>
                <?php
                foreach ($data as $v) {
                    $status = importData($v);
                    logImport($v["email"], $v, $status);
                    echo "<tr>";
                    foreach ($v as $dataV) {
                        echo "<td>" . $dataV . "</td>";
                    }
                    echo "<td>" . $status[0] . "</td></tr>";
                }
                ?>
            </table><br><br>
            <table class='homeTable' width="100%">
                <tr>
                    <td width="50%"><center><a class='button' href='<?php
                    $idFromUrl = explode("id=", $_SERVER['QUERY_STRING']);
                    $idFromUrl = explode("&", $idFromUrl[1]);
                    $idFromUrl = $idFromUrl[0];
                    echo $_SERVER['SCRIPT_NAME'] . "?id=" . $idFromUrl;
                    ?>'>Import More Data</a></center></td>
                <td><center><a class='button' style='width:200px;' href='<?php echo $_SERVER['SCRIPT_NAME'] . "?id=" . $idFromUrl . "&page=staff-log"; ?>'>View Import Log</a></center></td>
                </tr>    
            </table>
            <?php
            print "Import done";
        }
        /**
         * Form for uploading CSV file
         * 
         */ else {
            ?>
            <div id="form">
                <form enctype='multipart/form-data' action='<?php echo $_SERVER['REQUEST_URI']; ?>' method='post'>
                    Upload CSV File to import:<br />
                    <input size='50' type='file' name='filename'><br />
                    <input type='submit' name='submit' value='Upload'></form>
                <?php
            }

            /**
             * Function to Read CSV and make array of data
             * @param $file filename
             * @param $delimiter delimeter for csv, normally comma(,)
             * @return $data2DArray array scanned array of data
             */
            function get2DArrayFromCsv($file, $delimiter) {
                if (($handle = fopen($file, "r")) !== FALSE) {
                    $i = 0;
                    while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) {
                        if ($i == 0) {
                            $keyOfArray = $lineArray;
                        } else {
                            $data2DArray[$i - 1] = array_combine($keyOfArray, $lineArray);
                        }
                        $i++;
                    }
                    fclose($handle);
                }
                return $data2DArray;
            }

            /**
             * Function to handling inputing and creating staff
             * @param $data array of data of staff
             * @return array 0 insed for status and 1 index gives registered ID of staff
             */
            function importData($data) {
                $staffData['do'] = 'create';
                $staffData['a'] = 'add';
                $staffData['submit'] = 'create';
                $staffData['username'] = $data['username'];
                $staffData['firstname'] = $data['firstname'];
                $staffData['lastname'] = $data['lastname'];
                $staffData['email'] = $data['email'];
                if ($data['phone'])
                    $staffData['phone'] = $data['phone']; //NR
                if ($data['mobile'])
                    $staffData['mobile'] = $data['mobile']; //NR
                $staffData['dept_id'] = isset($data['department']) ? $data['department'] : 1; //R
                $staffData['role_id'] = isset($data['role_id']) ? $data['role_id'] : 1; //R
                $staffData['perms'] = isset($data['perms']) ? $data['perms'] : Array("user.create", "user.edit", "user.delete", "user.manage", "user.dir", "org.create", "org.edit", "org.delete", "faq.manage"); //R

                if ($data['isadmin'])
                    $staffData['isadmin'] = isset($data['isadmin']) ? $data['isadmin'] : 1;
                if ($data['islocked'])
                    $staffData['islocked'] = isset($data['islocked']) ? $data['islocked'] : 0;
                if ($data['isvisible'])
                    $staffData['isvisible'] = isset($data['isvisible']) ? $data['isvisible'] : 1;
                if ($data['onvacation'])
                    $staffData['onvacation'] = isset($data['onvacation']) ? $data['onvacation'] : 0;
                if ($data['assigned_only'])
                    $staffData['assigned_only'] = isset($data['assigned_only']) ? $data['assigned_only'] : 0;
                if ($data['backend'])
                    $staffData['backend'] = $data['backend'];
                if ($data['phone_ext'])
                    $staffData['phone_ext'] = isset($data['phone_ext']) ? $data['phone_ext'] : "";
                if ($data['signature'])
                    $staffData['signature'] = $data['signature'];
                $staffData['notes'] = isset($data['notes']) ? $data['notes'] : "Imported New Agent";


                $staff = Staff::create();
                if ($staff->update($staffData, $errors)) {
                    unset($_SESSION['new-agent-passwd']);
                    $msg[0] = "Added";
                    $msg[1] = $staff->getId();
                    $_REQUEST['a'] = null;
                } elseif (!$errors['err']) {
                    $msg[0] = "Error - " . implode(", ", $errors);
                } else {
                    $msg[0] = "Error - " . implode(", ", $errors);
                }


                return $msg;
            }

            /**
             * Function to handling logging of imported data
             * @param $email sting as email Id of staff
             * @param $data array of imported data
             * @param $status sting as import status
             * @return int 1 for success
             */
            function logImport($email, $data, $status) {
                $staffId = isset($status[1]) ? $status[1] : 0;
                $sql = "INSERT INTO `" . IMPORT_TABLE . "` (`staff_id`,`import`,`email_id`,`data`,`status`,`time`) VALUES (" . $staffId . ",'staff','" . $email . "','" . json_encode($data) . "','" . $status[0] . "',NOW())";
                $res = db_query($sql);
                return $res;
            }
            ?>

        </div>
    </div>
    <a download href='https://docs.google.com/spreadsheets/d/1wM_QBW_fu-rgyxY1_we_NzEWOtfwtZfCSpKPp5Fi7bU/edit?usp=sharing'><u>Download Sample CSV Sheet</u></a><br>
    <a download href='https://drive.google.com/file/d/0Bxi_G-w-7htrZVUxU3lROFB4U1k/view?usp=sharing'>Instruction for CSV data</a>