<?php

/**
 * Configration for import plugin
 * 
 * class : ImportConfig
 * @package    OsTicket
 * @subpackage plugin - Import
 * @author     Akash Kumar <akashkuardce@gmail.com>
 */
require_once(INCLUDE_DIR . '/class.plugin.php');
require_once(INCLUDE_DIR . '/class.forms.php');

class WidgetConfig extends PluginConfig {

    /**
       * getter Function for configration options
       *
       * @return array of config options
       */
    function getOptions() {
        return array(
            'widget_user_history_enable' => new BooleanField(array(
                'id' => 'widget_user_history_enable',
                'label' => 'User History Widget',
                'configuration' => array(
                    'desc' => 'User History Widget')
                    )),
        );
    }

    /**
       * This function will be called when configuration is submitted by user.
       *
       * @return true when processed
       */
    function pre_save(&$config, &$errors) {
        global $msg;

        if (!$errors)
            $msg = 'Configuration updated successfully';

        return true;
    }

}

?>
