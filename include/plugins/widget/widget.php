<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
require_once(INCLUDE_DIR . 'class.plugin.php');
require_once(INCLUDE_DIR . 'class.signal.php');
require_once(INCLUDE_DIR . 'class.app.php');

require_once('config.php');

//Table used for storing import logs
define('WIDGET_TABLE', TABLE_PREFIX . 'plugin_import');

//Plugin root 
//directory
define('PLUGINS_ROOT', INCLUDE_DIR . 'plugins/');

define('WIDGET_PLUGIN_ROOT', PLUGINS_ROOT . 'widget/');
define('WIDGET_INCLUDE_DIR', WIDGET_PLUGIN_ROOT . 'include/');
define('WIDGET_STAFFINC_DIR', WIDGET_INCLUDE_DIR . 'staff/');
define('WIDGET_CLIENTINC_DIR', WIDGET_INCLUDE_DIR . 'client/');

//Import plugin - installation & uninstallation
require_once(WIDGET_INCLUDE_DIR . 'class.widget_install.php');

/**
 * This Plugin is used for widgets
 * 
 * 
 * @package    OsTicket
 * @subpackage plugin - Widget
 * @author     Akash Kumar <akashkumardce@gmail.com>
 */
class WidgetPlugin extends Plugin {

    var $config_class = 'WidgetConfig';

    function bootstrap() {
        if ($this->firstRun()) {
            $this->configureFirstRun();
        }

        $config = $this->getConfig();
    }

    /**
     * Creates menu links in the staff backend.
     */
    function createStaffMenu() {
        Application::registerStaffApp('Equipment', 'equipment.php', array(iconclass => 'equipment'));
        Application::registerStaffApp('Equipment Categories', 'equipment_categories.php', array(iconclass => 'faq-categories'));
        Application::registerStaffApp('Equipment Status', 'equipment_status.php', array(iconclass => 'equipment_status'));
    }

    /**
     * Creates menu link in the client frontend.
     * Useless as of OSTicket version 1.9.2.
     */
    function createFrontMenu() {
        Application::registerClientApp('Equipment Status', 'equipment_front/index.php', array(iconclass => 'equipment'));
    }

    /**
     * Checks if this is the first run of our plugin.
     * @return boolean
     */
    function firstRun() {
        return 1;
    }

    /**
     * Necessary functionality to configure first run of the application
     */
    function configureFirstRun() {
        if (!$this->createDBTables()) {
            echo "First run configuration error.  "
            . "Unable to create database tables!";
        }
    }

    /**
     * setting up database installation scripts
     * @return boolean
     */
    function createDBTables() {
        $installer = new WidgetInstaller();
        return $installer->install();
    }

    /**
     * Uninstall Import databases and everything.
     * @param type $errors
     * @return boolean
     */
    function pre_uninstall(&$errors) {
        $installer = new WidgetInstaller();
        return $installer->remove();
    }
    
    /**
     * User History log widget.
     * @param int $userid
     * @return null but prints the widget
     */
    function getUserHistory($userId){
        	if($userId && $user=User::lookup($userId)){
                    $tickets = TicketModel::objects();
                    $tickets->filter(array('user_id' => $user->getId()));
                    $tickets->values('staff_id', 'staff__firstname', 'staff__lastname', 'team__name', 'team_id', 'lock__lock_id', 'lock__staff_id', 'isoverdue', 'status_id', 'status__name', 'status__state', 'number', 'cdata__subject', 'ticket_id', 'source', 'dept_id', 'dept__name', 'user_id', 'user__default_email__address', 'user__name', 'lastupdate');
                    global $cfg;
                    $widget = json_decode($cfg->config['pluginWidget']['value']);
                        
                    if($tickets->count()>0){
                        $subject_field = TicketForm::objects()->one()->getField('subject');
                        $tableRow = '<tr style="background:[background];margin-bottom:4px;"><td style=" padding:8px;width:150px;font-size:12px;"><a href="tickets.php?id=[id]" style="word-wrap: break-word;width: 120px;"><b>#[ticketId]</b>-[subject]</a></td><td style="font-size: 12px; color: rgb(119, 119, 119);">[status]</td><tr>';
                        $displayWidget = "<div class='widget' style='width:190px;position: fixed;left: 0px;top: 0px;margin-top: 0px;font-size:14px;background:#fff;'><h2><center>User previous logs</center></h2><div class='widgetLimitHeight'><table style='border-spacing: 0;border-collapse: collapse;'>";
                        $i=0;
                        $maxCountForTicket = isset($widget["userHistory"]["max"])?$widget["userHistory"]["max"]:5;
                        
                        foreach($tickets as $T){
                            $i++;
                            $tnum = $T['number'];
                            $tid = $T['ticket_id'];
                            $status = TicketStatus::getLocalById($T['status_id'], 'value', $T['status__name']);
                            $subject = $subject_field->display($subject_field->to_php($T['cdata__subject']));
                            $background = ($i%2==0)?"#fff":"#F4FAFF;";

                            $displayWidget .= str_replace("[background]",$background,str_replace("[id]", $tid,str_replace("[status]", $status,str_replace("[subject]", $subject,str_replace("[ticketId]", $tnum, $tableRow)))));
                            if($i>=$maxCountForTicket)
                                break;

                        }
                        $displayWidget .='</table></div>';
                        $displayWidget .= '</div>';
                        
                        print_r($displayWidget);
                    }
                    else{
                        return;
                    }
                }
	}
    
        
        /**
        * User History log widget.
        * @param int $userid
        * @return null but prints the widget
        */
        function ticketJourneyWidget($ticket){
            //$thread = $ticket->getThreadEntries();
            //foreach($thread as $th)
           // print_r($th);die;
           // echo "<pre>";print_r($ticket->get);die;
        }

}
?>