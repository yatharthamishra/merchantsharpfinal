<?php

/**
 * This class is used to install necessargy tables and handle unistallation for import plugin
 * 
 * 
 * @package    OsTicket
 * @subpackage plugin - Import
 * @author     Akash Kumar <akashkuardce@gmail.com>
 */
class WidgetInstaller {
    /**
     * Creation of table for storing logs
     * 
     * @return type
     */
    public function install() {
       /* $sql = 'CREATE TABLE `' . IMPORT_TABLE . '` ('
                . '`id` int(10) unsigned NOT NULL AUTO_INCREMENT,'
                . '`staff_id` int(10) unsigned NOT NULL,'
                . '`import` varchar(20) NOT NULL,'
                . '`email_id` varchar(40) NOT NULL,'
                . '`data` varchar(5000) NOT NULL,'
                . '`status` varchar(200) NOT NULL,'
                . '`time` datetime NOT NULL,'
                . 'PRIMARY KEY (`id`)'
                . ') ENGINE=InnoDB'; */
        $res=1;//$res = db_query($sql);
        return $res;
    }
    /**
     * Deletion of table for storing logs - during Uninstallation
     * 
     * @return type
     */
    public function remove() {
        return 1; // Data Not to be deleted on uninstallation
        $sql = 'DROP table ' . IMPORT_TABLE;
        $res = db_query($sql);
        return $res;
    }

}

?>
