
<style type="text/css">

    .widgetUserHistory a:hover {
        color:#CC0033;
    }

  
    .widgetUserHistory{
		width:200px;
	}
    .widgetUserHistory td, th {
        font-size: 12px;
        color: #777;
    }
    
	.widgetUserHistory tr:nth-child(odd) {
	  color: green;
	}
	.widgetUserHistory tr:nth-child(even) {
	  color: red;
	}
    table.widgetUserHistory {
        border-collapse: collapse;
    }
    .homeTable td, th {
        text-align: center;
        padding: 20px;
    }
    .widgetplugin .button {
        display: block;
        width: 115px;
        height: 25px;
        background: #4E9CAF;
        padding: 10px;
        text-align: center;
        border-radius: 5px;
        color: white;
        font-weight: bold;
    }
    .widgetplugin .buttonsmall {
        display: block;
        width: 200px;
        background: #bbb none repeat scroll 0% 0%;
        padding: 10px;
        text-align: center;
        border-radius: 5px;
        color: #FFF;
        font-weight: bold;
        margin-top: 10px;
    }
    .widgetplugin #form 			{width:150px;margin: 0px auto;}
    .widgetplugin #form input     {margin-bottom: 20px;}
</style>
<h1>Widget Manager</h1>

<?php
/**
 * To Handle different pages of importing
 * 
 */
if (isset($_GET["page"])) {
    $page = $_GET["page"] . "-widget.inc.php";
} else {
    $page = "widget-home.inc.php";
}
include($page);
