<?php

/**
 * This Plugin is used to show Widget to users
 * 
 * 
 * @package    OsTicket
 * @subpackage plugin - Widget
 * @author     Akash Kumar <akashkuardce@gmail.com>
 */
set_include_path(get_include_path().PATH_SEPARATOR.dirname(__file__).'/include');
/**
 * Returns an array of plugin details
 * @return array of plugin details
 */
return array(
 'id' => 'akashkumardce:Widget',
 'version' => '0.1',
 'name' => 'Widget',
 'functionality' => 'custom',
 'author' => 'Akash Kumar - shopclues',
 'description' => 'Widget management capability.',
 'url' => 'https://github.com/akashkumardce/OsTicket-plugin-Widget',
 'plugin' => 'widget.php:WidgetPlugin'
);
