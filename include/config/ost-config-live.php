<?php
/*********************************************************************
    ost-config.php

    Static osTicket configuration file. Mainly useful for mysql login info.
    Created during installation process and shouldn't change even on upgrades.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2010 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
    $Id: $
**********************************************************************/

#Disable direct access.
if(!strcasecmp(basename($_SERVER['SCRIPT_NAME']),basename(__FILE__)) || !defined('INCLUDE_DIR'))
    die('kwaheri rafiki!');

#Install flag
define('OSTINSTALLED',TRUE);
if(OSTINSTALLED!=TRUE){
    if(!file_exists(ROOT_DIR.'setup/install.php')) die('Error: Contact system admin.'); //Something is really wrong!
    //Invoke the installer.
    header('Location: '.ROOT_PATH.'setup/install.php');
    exit;
}

# Encrypt/Decrypt secret key - randomly generated during installation.
define('SECRET_SALT','iJl6kz2Cn1roP0GXoBFxcp37ZMg1P0PI');

#Default admin email. Used only on db connection issues and related alerts.
define('ADMIN_EMAIL','shorturlshopclues1@gmail.com');

# Database Options
# ---------------------------------------------------
# Mysql Login info

define('DBTYPE','mysql');
define('DBHOST','10.20.72.15');
define('DBNAME','shopclue_cart');
define('DBUSER','ostdev');
define('DBPASS','0$#tD3v@^&!');
define('DBPORT','3306');

#define('SLAVE_DBTYPE','mysql');
#define('SLAVE_DBHOST','10.20.72.15');
#define('SLAVE_DBNAME','shopclue_cart');
#define('SLAVE_DBUSER','ostdev');
#define('SLAVE_DBPASS','0$#tD3v@^&!');
#define('SLAVE_DBPORT','3306');


define('SLAVE_DBTYPE','mysql');
define('SLAVE_DBHOST','10.20.72.9');
define('SLAVE_DBNAME','shopclue_cart');
define('SLAVE_DBUSER','ostdev');
define('SLAVE_DBPASS','0$#tD3v@^&!');
define('SLAVE_DBPORT','3306');


define('SCLUES_OST_DBTYPE','mysql');
define('SCLUES_OST_DBHOST','10.20.72.8');
define('SCLUES_OST_DBNAME','shopclue_cart');
//define('SCLUES_OST_DBUSER','ostdev');
define('SCLUES_OST_DBUSER','ostuser');
//define('SCLUES_OST_DBPASS','0$#tD3v@^&!');
define('SCLUES_OST_DBPASS','0sTU$3r!Nt');
define('SCLUES_OST_DBPORT','3306');
define('REPORT_SLAVE_DBTYPE','mysql');
define('REPORT_SLAVE_DBHOST','10.20.72.83');
define('REPORT_SLAVE_DBNAME','shopclue_cart');
define('REPORT_SLAVE_DBUSER','ostuser');
define('REPORT_SLAVE_DBPASS','0STU$ER!');
define('REPORT_SLAVE_DBPORT','3306');


define('REPORT_SLAVE_WRITE_DBTYPE','mysql');
define('REPORT_SLAVE_WRITE_DBHOST','10.20.72.83');
define('REPORT_SLAVE_WRITE_DBNAME','shopclue_cart');
define('REPORT_SLAVE_WRITE_DBUSER','ostuser1');
define('REPORT_SLAVE_WRITE_DBPASS','0STU$ER!');
define('REPORT_SLAVE_WRITE_DBPORT','3306');
define('ANALOG_LOG',dirname(__FILE__).'/../../vendor/Analog/vendor/analog/analog/lib/Analog.php');

# List of tables for which write query can go to sclues_osticket DB



# Table prefix
define('TABLE_PREFIX','mst_');
// List of tables for which WRITE query can go to sclues_osticket DB
define('OST_SCLUES_WRITE_TABLES', 
    serialize(
        array(
            TABLE_PREFIX.'ord_status_cron',
            TABLE_PREFIX.'ticket__odata'
        )
    )
);

// List of tables for which READ query can go to sclues_osticket DB
define('OST_SCLUES_READ_TABLES', 
    serialize(
        array(
            'cscart_status_descriptions',
	    'clues_order_reporting_master',
	    TABLE_PREFIX.'ord_status_cron',
            'clues_osticket_map_manager',
           'clues_ticket_dept_map',
           'clues_order_reporting_master'
	    
        )
    )
);

// Write Query types...
define('OST_QUERY_WRITE_TYPES', serialize(array('insert', 'update', 'delete', 'replace', 'set')));
define('OST_QUERY_READ_TYPES', serialize(array('select', 'show', 'explain')));


define("ERROR_TO_LOG",serialize(array(E_ERROR,E_PARSE)));
define('analog_log_levels', serialize(array("URGENT" => 0, "ALERT" => 1, "CRITICAL" => 2, "ERROR" => 3, "WARNING" => 4, "NOTICE" => 5, "INFO" => 6, "DEBUG" => 7)));
define('analog_domain_module', serialize(array("TICKET" => array("Creation" => 7, "communication" => 7), "TASK" => array("Creation" => 7, "Flow" => 7, "Updation" => 7), "ADMIN" => array("login" => 7), "OST" => array("general" => 7, "Fatal" => 7, "Exception" => 7), "CRON" => array("autoResponse" => 7, "bulkJob" => 7), "API" => array("ticket" => 7, "task" => 7))));
define('analog_domains', serialize(array("D1" => "TICKET", "D2" => "TASK", "D3" => "ADMIN", "D4" => "OST", "D5" => "CRON", "D6" => "API")));
define('analog_logger', serialize(array("web_log_file_path"=>"../logs/app_logs.txt","analog_enable"=>true,"THRESHOLD_LOG_LEVEL"=>6)));
define('analog_modules', serialize(array("TICKET" => array("D1_M1" => "Creation", "D1_M2" => "communication"), "TASK" => array("D2_M1" => "Creation", "D2_M2" => "Flow", "D2_M3" => "Updation"), "ADMIN" => array("D3_M1" => "login"), "OST" => array("D4_M1" => "general", "D4_M2" => "Fatal", "D4_M3" => "Exception"), "CRON" => array("D5_M1" => "autoResponse", "D5_M2" => "bulkJob","D5_M3"=>"reportDailyCron"), "API" => array("D6_M1" => "ticket", "D6_M2" => "task"))
));
define('analog_errors', serialize(array("E1"=>"Ticket Creation Error","E2"=>"successful","E3"=>"Invalid Bulk Operation","E4"=>"Another Cron Running","E5"=>"Auto Response Successful","E6"=>"Fatal Error","E7"=>"Exception Occured","E8"=>"High page Load time","E9"=>"High Query execution time","E10"=>"High db connection time","E11"=>"High memory usage","E12"=>"Task update API","E13"=>"Order Flow Log","E14"=>"Ticket Flow Log","E15"=>"Task Flow Log","E16"=>"Ticket Creation API","E17"=>"SLA Service Response","E18"=>"Db Error Occured")));
define("DBCONNECTION_LOG_TIME", 0.1);
define("DBCONNECTION_LOG", serialize(array("domain" => 'OST', "module" => 'general', "errorCon" => 'High db connection time')));


# SSL Options
# ---------------------------------------------------
# SSL options for MySQL can be enabled by adding a certificate allowed by
# the database server here. To use SSL, you must have a client certificate
# signed by a CA (certificate authority). You can easily create this
# yourself with the EasyRSA suite. Give the public CA certificate, and both
# the public and private parts of your client certificate below.
#
# Once configured, you can ask MySQL to require the certificate for
# connections:
#
# > create user osticket;
# > grant all on osticket.* to osticket require subject '<subject>';
#
# More information (to-be) available in doc/security/hardening.md

# define('DBSSLCA','/path/to/ca.crt');
# define('DBSSLCERT','/path/to/client.crt');
# define('DBSSLKEY','/path/to/client.key');

#
# Mail Options
# ---------------------------------------------------
# Option: MAIL_EOL (default: \n)
#
# Some mail setups do not handle emails with \r\n (CRLF) line endings for
# headers and base64 and quoted-response encoded bodies. This is an error
# and a violation of the internet mail RFCs. However, because this is also
# outside the control of both osTicket development and many server
# administrators, this option can be adjusted for your setup. Many folks who
# experience blank or garbled email from osTicket can adjust this setting to
# use "\n" (LF) instead of the CRLF default.
#
# References:
# http://www.faqs.org/rfcs/rfc2822.html
# https://github.com/osTicket/osTicket-1.8/issues/202
# https://github.com/osTicket/osTicket-1.8/issues/700
# https://github.com/osTicket/osTicket-1.8/issues/759
# https://github.com/osTicket/osTicket-1.8/issues/1217

# define(MAIL_EOL, "\r\n");

#
# HTTP Server Options
# ---------------------------------------------------
# Option: ROOT_PATH (default: <auto detect>, fallback: /)
#
# If you have a strange HTTP server configuration and osTicket cannot
# discover the URL path of where your osTicket is installed, define
# ROOT_PATH here.
#
# The ROOT_PATH is the part of the URL used to access your osTicket
# helpdesk before the '/scp' part and after the hostname. For instance, for
# http://mycompany.com/support', the ROOT_PATH should be '/support/'
#
# ROOT_PATH *must* end with a forward-slash!

# define('ROOT_PATH', '/support/');

#
# Session Storage Options
# ---------------------------------------------------
# Option: SESSION_BACKEND (default: db)
#
# osTicket supports Memcache as a session storage backend if the `memcache`
# pecl extesion is installed. This also requires MEMCACHE_SERVERS to be
# configured as well.
#
# MEMCACHE_SERVERS can be defined as a comma-separated list of host:port
# specifications. If more than one server is listed, the session is written
# to all of the servers for redundancy.
#
# Values: 'db' (default)
#         'memcache' (Use Memcache servers)
#         'system' (use PHP settings as configured (not recommended!))
#
 define('SESSION_BACKEND', 'memcache');
define('MEMCACHE_SERVERS', '10.20.74.135:11211');

define("OVERALL_REPORT_DEFAULT_TIME_PERIOD", '1');
define("REPORT_CSV_SAVE_DIR",'/tmp/');
global $reportFilterConfig; 
$reportFilterConfig=array(
'filterDictionary'=>array(
'priority'=>array(
'filter'=>'priority',
'filterviewname'=>'Priority',
'filterRequestParam'=>'p',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'issueType'=>array(
'filter'=>'issueType',
'filterviewname'=>'Issue Type',
'filterRequestParam'=>'it',
'filterType'=>'Select',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'custom',
'defaultDisplay'=>'show'
),
'issueSubType'=>array(
'filter'=>'issueSubType',
'filterviewname'=>'Issue Sub Type',
'filterRequestParam'=>'ist',
'filterType'=>'Select',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'custom'
),
'issueSubSubType'=>array(
'filter'=>'issueSubSubType',
'filterviewname'=>'Issue Sub Sub Type',
'filterRequestParam'=>'isst',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'custom'
),
'agent'=>array(
'filter'=>'agent',
'filterviewname'=>'Agent',
'filterRequestParam'=>'ag',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'teamLeader'=>array(
'filter'=>'teamLeader',
'filterviewname'=>'Team Leader',
'filterRequestParam'=>'tl',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'threadAgent'=>array(
'filter'=>'threadAgent',
'filterviewname'=>'Agent',
'filterRequestParam'=>'tag',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'threadTeamLeader'=>array(
'filter'=>'threadTeamLeader',
'filterviewname'=>'Team Leader',
'filterRequestParam'=>'ttl',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'department'=>array(
'filter'=>'department',
'filterviewname'=>'Department',
'filterRequestParam'=>'dt',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'taskDepartment'=>array(
'filter'=>'taskDepartment',
'filterviewname'=>'Department',
'filterRequestParam'=>'dtt',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'taskType'=>array(
'filter'=>'taskType',
'filterviewname'=>'Task Name',
'filterRequestParam'=>'tt',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'taskStatus'=>array(
'filter'=>'taskStatus',
'filterviewname'=>'Task Status',
'filterRequestParam'=>'tst',
'filterType'=>'Select',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'taskslaStatus'=>array(
'filter'=>'taskslaStatus',
'filterviewname'=>'SLA Status',
'filterRequestParam'=>'sst',
'filterType'=>'Select',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'ticketSlaStatus'=>array(
'filter'=>'ticketSlaStatus',
'filterviewname'=>'SLA Status',
'filterRequestParam'=>'tsst',
'filterType'=>'Select',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'ticketCreateFromdate'=>array(
'filter'=>'ticketCreateFromdate',
'filterviewname'=>'From Date',
'filterRequestParam'=>'tdtf',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'fromdate'
),
'ticketCreateTodate'=>array(
'filter'=>'ticketCreateTodate',
'filterviewname'=>'To Date',
'filterRequestParam'=>'tdtd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'todate'
),
'taskCreateFromDate'=>array(
'filter'=>'taskCreateFromDate',
'filterviewname'=>'From Date',
'filterRequestParam'=>'tcfd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'fromdate'
),
'taskCreateToDate'=>array(
'filter'=>'taskCreateToDate',
'filterviewname'=>'To Date',
'filterRequestParam'=>'tctd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'todate'
),
'taskName'=>array(
'filter'=>'taskName',
'filterviewname'=>'Task Name',
'filterRequestParam'=>'tna',
'filterType'=>'multiSelect',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'triageType'=>array(
'filter'=>'triageType',
'filterviewname'=>'Triage Type',
'filterRequestParam'=>'trty',
'filterType'=>'Select',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show'
),
'threadCreateFromdate'=>array(
'filter'=>'threadCreateFromdate',
'filterviewname'=>'From Date',
'filterRequestParam'=>'thfd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'fromdate'
),
'threadCreateTodate'=>array(
'filter'=>'threadCreateTodate',
'filterviewname'=>'To Date',
'filterRequestParam'=>'thtd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'todate'
),
'threadOnlyCreateFromdate'=>array(
'filter'=>'threadOnlyCreateFromdate',
'filterviewname'=>'From Date',
'filterRequestParam'=>'thofd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'fromdate'
),
'threadOnlyCreateTodate'=>array(
'filter'=>'threadOnlyCreateTodate',
'filterviewname'=>'To Date',
'filterRequestParam'=>'thotd',
'filterType'=>'Date',
'data'=>array(),
'selectedValue'=>NULL,
'handling'=>'normal',
'defaultDisplay'=>'show',
'class'=>'todate'
)
)
);
foreach ($reportFilterConfig['filterDictionary'] as $filterDict){
if(!isset($reportFilterConfig['filterRequestMap']))
{ $reportFilterConfig['filterRequestMap']=array(); }
$reportFilterConfig['filterRequestMap'][$filterDict['filterRequestParam']]=$filterDict['filter'];
}
global $reportFilterDictionary;
$reportFilterDictionary=$reportFilterConfig['filterDictionary'];
global $reportFields;
$reportFields = array(
'data' => array
(
'triaging' => array
(
'all' => array
(
0 => array
(
'fields' => array
(
"Ticket No." => "number",
"Ticket Source" => "source_extra",
//"Ticket Status" => "status__name",
//"Ticket Channel" => "odata__ticket_channel",
"Priority" => "odata__priority",
"Creation Date" => "created",
"Order Number" => "odata__order_id",
"Type" => "",
"Subtype" => "",
"Subsubtype" => "",
"Return Reason" => "",
"Agent Name" => "staff__firstname",
"Agent ID" => "staff__email",
"Team Leader" => "",
"Triaging Date" => "triage__triage_time",
"Triaging Type" => "triage__triage",
"Latest Update Date & Time (by Triagged Group)" => "",
"AHT" => "",
"SLA status" => "isoverdue"
),
'filters' => array
(
$reportFilterDictionary['ticketCreateFromdate'],
$reportFilterDictionary['ticketCreateTodate'],
$reportFilterDictionary['priority'],
$reportFilterDictionary['issueType'],
$reportFilterDictionary['issueSubType'],
$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['triageType'],
$reportFilterDictionary['threadAgent'],
$reportFilterDictionary['threadTeamLeader'],
$reportFilterDictionary['triageSla']
)
)
),
'sla adherence' => array
(
'agent level' => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "staff__firstname",
"Agent ID" => "staff__email",
"Team Leader" => "",
"Tickets Handled - Untriaged" => "",
"Tickets Handled - Triage Again" => "",
"Total Tickets Triaged" => "ticket_count",
/* "SLA - Untriaged" => "",
"SLA - Triage Again" => "",
"SLA - Total Triaged" => "" */
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],
$reportFilterDictionary['threadCreateTodate'],
$reportFilterDictionary['threadAgent'],
$reportFilterDictionary['threadTeamLeader']
)
),
'issue type' => array
(
'fields' => array
(
"Date" => "",
"Type" => "",
"Subtype" => "",
"Subsubtype" => "",
"Tickets Handled Untriaged" => "",
"Tickets Handled Triage Again" => "",
"Total Tickets Triaged" => "",
"SLA - Untriaged" => "",
"SLA - Triage Again" => "",
"SLA - Total Triaged" => ""
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],
$reportFilterDictionary['threadCreateTodate'],
$reportFilterDictionary['issueType'],
$reportFilterDictionary['issueSubType'],
$reportFilterDictionary['issueSubSubType']
)
)
),
'aht' => array
(
'agent level' => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"Tickets Handled - Untriaged" => "",
"Tickets Handled - Triage Again" => "",
"Total Tickets Triaged" => "",
"AHT - Untriaged(in sec)" => "",
"AHT - Triage Again(in sec)" => "",
"AHT - Total Triaged(in sec)" => ""
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],
$reportFilterDictionary['threadCreateTodate'],
$reportFilterDictionary['threadAgent'],
$reportFilterDictionary['threadTeamLeader']
)
),
'issue type' => array
(
'fields' => array
(
"Date" => "",
"Type" => "",
"Subtype" => "",
"Subsubtype" => "",
"Tickets Handled - Untriaged" => "",
"Tickets Handled - Triage Again" => "",
"Total Tickets Triaged" => "",
"AHT - Untriaged(in sec)" => "",
"AHT - Triage Again(in sec)" => "",
"AHT - Total Triaged(in sec)" => ""
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],
$reportFilterDictionary['threadCreateTodate'],
$reportFilterDictionary['issueType'],
$reportFilterDictionary['issueSubType'],
$reportFilterDictionary['issueSubSubType']
)
)
),
// 'productivity' => array
// (	
// 0 => array
// (
// 'fields' => array
// (
// "Date",
// "Agent Name",
// "Agent ID",
// "Team Leader",
// "Tickets Handled - Untriaged",
// "Tickets Handled - Triage Again",
// "Total Tickets Triaged"
// ),
// 'filters' => array
// (
// "Date Range",
// "Priority",
// "Agent ID",
// "Team Leader",
// "Triaging Type"
// )
// )
// ),
'pendency' => array
(
0 => array
(
'fields' => array
(
"Date" => "",
"Fresh Tickets Received Untriaged" => "",
"Fresh Tickets Received Triage Again" => "",
"Old Tickets Untriaged" => "",
"Old Tickets Triage Again" => "",
"Tickets Handled - Untriaged" => "",
"Tickets Handled - Triage Again" => "",
"Tickets Pending - Untriaged" => "",
"Tickets Pending - Triage Again" => ""
// "Date" => "date",	
// "Tickets Received Untriaged" => "ticket_count",
// "Tickets Received Triage Again" => "date_ta",
// "Tickets Handled - Untriaged" => "",
// "Tickets Handled - Triage Again" => "",
// "Tickets Pending - Untriaged" => "",
// "Tickets Pending - Triage Again" => ""
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],
$reportFilterDictionary['threadCreateTodate']
)
)
),
'sla summary' => array
(
0 => array
(
'fields' => array
(
"Date" => "",
"Tickets Received - Untriaged" => "",
"Tickets Received - Triage Again" => "",
"Handled within SLA - Untriaged" => "",
"Handled within SLA - Triage Again" => "",
"Open within SLA - Untriaged" => "",
"Open within SLA - Triage Again" => "",
"SLA% Untriaged" => "",
"SLA% Triage Again" => "",
"SLA% Overall" => ""
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],
$reportFilterDictionary['threadCreateTodate']
)
)
)
),
'ccg' => array
(
'all' => array
(
0 => array
(
'fields' => array
(
"Ticket-Number",
"TicketSource",
"TicketCreateDateTime",
"Order-Number",
"Task-Name",
"Task-Id",
"Task-Source",
"Task-Status",
"Issue-Type",
"Issue-Sub-Type",
"Issue-Sub-Sub-Type",
"Agent-Name",
"Agent-Email",
"Team-Leader",
"Task-Create-Date-Time",
"Task-Close-Date-Time",
"SLA-Status",
"Handling-Time"
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'sla adherence' => array
(
'agent level' => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"No. of Tasks Assigned" => "",
"Tasks Solved Within SLA" => "",
"Tasks Solved OSLA" => "",
"Tasks Open Within SLA" => "",
"Task Open in OSLA" => "",
"SLA Adherence" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],
$reportFilterDictionary['agent'],$reportFilterDictionary['teamLeader']
)
)
),
'productivity' => array
(
0 => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"# Email tasks Handled" => "",
"# Calls tasks Handled" => "",
"Total Productivity" => ""
),
'filters' => array
(
$reportFilterDictionary['threadCreateFromdate'],$reportFilterDictionary['threadCreateTodate'],
$reportFilterDictionary['threadAgent'],$reportFilterDictionary['threadTeamLeader']
)
)
),
'pendency' => array
(
0 => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"# Email tasks Created" => "",
"# Calls tasks Created" => "",
"Total Tasks Created" => "",
"# Email tasks Handled" => "",
"# Calls tasks Handled" => "",
"Total Tasks Handled" => "",
"# Email tasks Pending" => "",
"# Calls tasks Pending" => "",
"Total Tasks Pending" => ""
),
'filters' => array
(
$reportFilterDictionary['threadOnlyCreateFromdate'],$reportFilterDictionary['threadOnlyCreateTodate'],
$reportFilterDictionary['agent'],$reportFilterDictionary['teamLeader']
)
)
)
),
'crg' => array
(
'all' => array
(
0 => array
(
'fields' => array
(
"Ticket-Number",
"TicketCreateDateTime",
"TicketSource",
"Issue-Type",
"Issue-Sub-Type",
"Issue-Sub-Sub-Type",
"Order-Number",
"Task-Name",
"Task-Id",
"Task-Source",
"Task-Create-Date-Time",
"Agent-Name",
"Agent-Email",
"Team-Leader",
"Task-Close-Date-Time",
"Task-Status",
"Handling-Time(In seconds)",
"SLA-Status"
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'sla adherence' => array
(
'agent level' => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"No. of Tasks Assigned" => "",
"Tasks Solved Within SLA" => "",
"Tasks Solved OSLA" => "",
"Tasks Open Within SLA" => "",
"Task Open in OSLA" => "",
"SLA Adherence" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],
$reportFilterDictionary['agent'],$reportFilterDictionary['teamLeader']
)
)
),
'aht' => array
(
'agent level' => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"No. of Tasks Handled" => "",
"AHT" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],
$reportFilterDictionary['agent'],$reportFilterDictionary['teamLeader']
)
)
),
'productivity' => array
(
0 => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"Tasks Assigned" => "",
"Tasks Handled" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],
$reportFilterDictionary['agent'],$reportFilterDictionary['teamLeader']
)
)
)
),
'ops' => array
(
'all' => array
(
0 => array
(
'fields' => array
(
"S.No." => "",
"Ticket No." => "",
"Ticket Creation Date & Time" => "",
"Ticket Source" => "",
"Ticket Channel" => "",
"Issue Type" => "",
"Subtype" => "",
"Subsubtype" => "",
"Return Reason" => "",
"Return ID" => "",
"Order Number" => "",
"Task Name" => "",
"Task ID" => "",
"Task Source" => "",
"Task Creation Date & Time" => "",
"Task Type (Tranfered/Fresh)" => "",
"No. of Times task Updated" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"Department Name" => "",
"Task Assigned date & Time" => "",
"Task Closure date & Time" => "",
"Task Status" => "",
"Latest Task Response" => "",
"Handling Time" => "",
"SLA status (In SLA/OSLA)" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'departmentwise sla' => array
(
0 => array
(
'fields' => array
(
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'taskwise sla' => array
(
0 => array
(
'fields' => array
(
"Task Name" => "taskName",
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
)
),
'bus' => array
(
'issue' => array
(
0 => array
(
'fields' => array
(
"S.No." => "SNumber",
"Type" => "issueType",
"Subtype" => "issueSubType",
"Subsubtype" => "issueSubSubType",
"Tickets Created" => "totalNumberOfTickets",
"Tickets Closed" => "ticketsClosed",
"Tickets Open" => "ticketsOpen",
"Tickets Solved within SLA" => "ticketsClosedWithinSla",
"Tickets Open within SLA" => "ticketsOpenWithinSla",
"Tickets Solved OSLA" => "ticketsClosedWithinOSla",
"Tickets Open OSLA" => "ticketsOpenWithinOSla",
"SLA Adherence" => "slaPercentage",
'Tickets Unknown Status'=>'ticketsUnknownStatus',
"AHT (in days)"=>"aht",
'Tickets Closed But Status Unknown'=>'ticketsClosedButUnknownStatus'
),
'filters' => array
(
$reportFilterDictionary['ticketCreateFromdate'],$reportFilterDictionary['ticketCreateTodate'],
$reportFilterDictionary['priority'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['department'], $reportFilterDictionary['teamLeader'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['ticketSlaStatus'],$reportFilterDictionary['agent'],$reportFilterDictionary['issueSubSubType']
)
)
),
'departmentwise sla' => array
(
0 => array
(
'fields' => array
(
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'taskwise sla' => array
(
0 => array
(
'fields' => array
(
"Task Name" => "taskName",
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'ticket' => array
(
0 => array
(
'fields' => array
(
"Source" => "source_extra",
"Tickets Created" => "totalNumberOfTickets",
"Tickets Closed" => "ticketsClosed",
"Tickets Open" => "ticketsOpen",
"Tickets Solved OSLA" => "ticketsClosedWithinOSla",
"Tickets Solved within SLA" => "ticketsClosedWithinSla",
"Tickets Open OSLA" => "ticketsOpenWithinOSla",
"Tickets Open within SLA" => "ticketsOpenWithinSla",
'Tickets Unknown Status'=>'ticketsUnknownStatus',
'Tickets Closed But Status Unknown'=>'ticketsClosedButUnknownStatus',
"SLA Adherence" => "slaPercentage"
),
'filters' => array
(
$reportFilterDictionary['ticketCreateFromdate'],$reportFilterDictionary['ticketCreateTodate'],
$reportFilterDictionary['priority'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['department'], $reportFilterDictionary['teamLeader'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['ticketSlaStatus'],$reportFilterDictionary['agent'],$reportFilterDictionary['issueSubSubType']
)
)
),
'prod' => array
(
0 => array
(
'fields' => array
(
"Source" => "",
"Agent Name" => "",
"Agent Email Id" => "",
"Team Lead" => "",
"Task Assigned" => "",
"Task Handled" => "",
"Task Open in SLA" => "",
"Task Open in OSLA" => "",
"Task Closed in SLA" => "",
"Task Closed in OSLA" => "",
"SLA Adherence" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'sla adherence' => array
(
'departmentwise' => array
(
'fields' => array
(
"S.No." => "",
"Department" => "",
"No. of Tasks Created" => "",
"# Tasks Closed" => "",
"# Tasks Open" => "",
"Tasks Solved OSLA" => "",
"Tasks Solved within SLA" => "",
"Tasks Open within SLA" => "",
"Tasks Open in OSLA" => "",
"SLA Adherence" => ""
),
'filters' => array
(
)
)
),
'productivity' => array
(
0 => array
(
'fields' => array
(
"Date" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"Tasks Assigned" => "",
"Tasks Handled" => "",
"Tasks Pending in SLA" => "",
"Tasks Pending in OSLA" => ""
),
'filters' => array
(
)
)
)
),
'ove'=>array(
'slarev' => array
(
0 => array
(
'fields' => array
(
"S.No." => "SNumber",
"Type" => "issueType",
"Subtype" => "issueSubType",
"Subsubtype" => "issueSubSubType",
"Tickets Created" => "totalNumberOfTickets",
"Tickets Closed" => "ticketsClosed",
"Tickets Open" => "ticketsOpen",
"Tickets Solved within SLA" => "ticketsClosedWithinSla",
"Tickets Open within SLA" => "ticketsOpenWithinSla",
"Tickets Solved OSLA" => "ticketsClosedWithinOSla",
"Tickets Open OSLA" => "ticketsOpenWithinOSla",
"SLA Adherence" => "slaPercentage",
'Tickets Unknown Status'=>'ticketsUnknownStatus',
'Tickets Closed But Status Unknown'=>'ticketsClosedButUnknownStatus'
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],
$reportFilterDictionary['taskDepartment'],
$reportFilterDictionary['taskType']
)
)
), 
'task' => array
(
0 => array
(
'fields' => array
(
"Task Name" => "taskName",
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),
'issue' => array
(
0 => array
(
'fields' => array
(
"S.No." => "SNumber",
"Type" => "issueType",
"Subtype" => "issueSubType",
"Subsubtype" => "issueSubSubType",
"Tickets Created" => "totalNumberOfTickets",
"Tickets Closed" => "ticketsClosed",
"Tickets Open" => "ticketsOpen",
"Tickets Solved within SLA" => "ticketsClosedWithinSla",
"Tickets Open within SLA" => "ticketsOpenWithinSla",
"Tickets Solved OSLA" => "ticketsClosedWithinOSla",
"Tickets Open OSLA" => "ticketsOpenWithinOSla",
"SLA Adherence" => "slaPercentage",
'Tickets Unknown Status'=>'ticketsUnknownStatus',
'AHT (in days )'=>"aht",
'Tickets Closed But Status Unknown'=>'ticketsClosedButUnknownStatus'
),
'filters' => array
(
$reportFilterDictionary['ticketCreateFromdate'],$reportFilterDictionary['ticketCreateTodate'],
$reportFilterDictionary['priority'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['department'], $reportFilterDictionary['teamLeader'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['ticketSlaStatus'],$reportFilterDictionary['agent'],$reportFilterDictionary['issueSubSubType']
)
)
),'ticketdump' => array
(
0 => array
(
'fields' => array
(
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['ticketCreateFromdate'],$reportFilterDictionary['ticketCreateTodate'],$reportFilterDictionary['priority'],
$reportFilterDictionary['issueType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),'dep' => array
(
0 => array
(
'fields' => array
(
"Department" => "department",
"No. of Tasks Created" => "totalNumberOfTasks",
"Tasks Solved within SLA" => "taskClosedWithinSla",
"Tasks Solved OSLA" => "taskClosedWithinOSla",
"Tasks Open Within SLA" => "taskOpenWithinSla",
"Tasks Open OSLA" => "taskOpenWithinOSla",
// "Task Unknown Status"=>"taskUnknownStatus",
// "Task Closed Unknown Status"=>"taskClosedButUnknownStatus",
"SLA%" => "slaPercentage"
),
'filters' => array(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
),'taskdump' => array
(
0 => array
(
'fields' => array
(
"S.No." => "",
"Ticket No." => "",
"Ticket Creation Date & Time" => "",
"Ticket Source" => "",
"Ticket Channel" => "",
"Issue Type" => "",
"Subtype" => "",
"Subsubtype" => "",
"Return Reason" => "",
"Return ID" => "",
"Order Number" => "",
"Task Name" => "",
"Task ID" => "",
"Task Source" => "",
"Task Creation Date & Time" => "",
"Task Type (Tranfered/Fresh)" => "",
"No. of Times task Updated" => "",
"Agent Name" => "",
"Agent ID" => "",
"Team Leader" => "",
"Department Name" => "",
"Task Assigned date & Time" => "",
"Task Closure date & Time" => "",
"Task Status" => "",
"Latest Task Response" => "",
"Handling Time" => "",
"SLA status (In SLA/OSLA)" => ""
),
'filters' => array
(
$reportFilterDictionary['taskCreateFromDate'],$reportFilterDictionary['taskCreateToDate'],$reportFilterDictionary['priority'],$reportFilterDictionary['taskStatus'],
$reportFilterDictionary['issueType'],$reportFilterDictionary['taskDepartment'],$reportFilterDictionary['taskType'],
$reportFilterDictionary['issueSubType'],$reportFilterDictionary['teamLeader'],
$reportFilterDictionary['taskslaStatus'],$reportFilterDictionary['issueSubSubType'],
$reportFilterDictionary['agent']
)
)
)
)
),
'navigation' => array
(
'Triaging' => array
(
'link' => '&type=triaging&subtype=all',
'subnav' => array
(
'SLA Adherence' => '&type=triaging&subtype=sla adherence&subsubtype=agent level',
'Average Handling Time' => '&type=triaging&subtype=aht&subsubtype=agent level',
'Pendency' => '&type=triaging&subtype=pendency',
'SLA Summary' => '&type=triaging&subtype=sla summary'
)
),
'CCG' => array
(
'link' => '&type=ccg&subtype=all',
'subnav' => array
(
'SLA Adherence' => '&type=ccg&subtype=sla adherence&subsubtype=agent level',
'Productivity' => '&type=ccg&subtype=productivity',
'Pendency' => '&type=ccg&subtype=pendency'
)
),
'CRG' => array
(
'link' => '&type=crg&subtype=all',
'subnav' => array
(
'SLA Adherence' => '&type=crg&subtype=sla adherence&subsubtype=agent level',
'AHT' => '&type=crg&subtype=aht&subsubtype=agent level',
'Productivity' => '&type=crg&subtype=productivity'
)
),
'OPS' => array
(
'link' => '&type=ops&subtype=all',
'subnav' => array
(
'Department wise SLA' => '&type=ops&subtype=departmentwise sla',
'Taskwise SLA' => '&type=ops&subtype=taskwise sla'
)
),
'BUSINESS' => array
(
'link' => '',
'subnav' => array
(
'Issue Type Summary' => '&type=bus&subtype=issue',
'Ticket Source Summary' => '&type=bus&subtype=ticket',
'Department wise SLA' => '&type=bus&subtype=departmentwise sla'
)
),
'Overall' => array
(
'link' => '',
'subnav' => array
(
'Task SLA revised entries' => '&type=ove&subtype=slarev',
'Department wise SLA' => '&type=ove&subtype=dep',
'Task wise SLA' => '&type=ove&subtype=task',
'Issue wise SLA' => '&type=ove&subtype=issue',
'Overall Task Dump'=>'&type=ove&subtype=taskdump',
'Overall Ticket Dump'=>'&type=ove&subtype=ticketdump'
)
)
)
);
global $reportFieldNotToBeDisplayed;
$reportFieldNotToBeDisplayed=array('Tickets Unknown Status','Tickets Closed But Status Unknown');

?>