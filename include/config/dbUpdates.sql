#initial Structure 09-03-2017 | Meghraj Sharma | Initial File Made For First Task Mapping

CREATE TABLE `mst_ticket_first_task_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `task_type_id` int(11) NOT NULL,
  `order_status_id` varchar(55) NOT NULL,
  `merchant_type_id` int(11) NOT NULL,
  `add_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `task_status_id` tinyint(4) DEFAULT '1',
  `task_response` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `mst_flow_logs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `table_name` VARCHAR(64) DEFAULT NULL,
  `field_name` VARCHAR(128) DEFAULT NULL,
  `primary_id` INT(11) DEFAULT NULL,
  `previous_value` VARCHAR(512) DEFAULT NULL,
  `next_value` VARCHAR(512) DEFAULT NULL,
  `staff_id` INT(11) DEFAULT NULL,
  `updated_time` TIMESTAMP NULL DEFAULT NULL,
  `extra` VARCHAR(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
DROP Table if exists `mst_task__data`;
CREATE TABLE `mst_task__data` (
  `task_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `taskType` int(11) DEFAULT NULL,
  `taskSource` varchar(45) DEFAULT NULL,
  `taskStatus` varchar(45) DEFAULT NULL,
  `taskResponse` int(11) DEFAULT NULL,
  `taskComment` varchar(255) DEFAULT NULL,
  `ticket_id` varchar(45) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `call_no` varchar(45) DEFAULT NULL,
  `payment_id` varchar(45) DEFAULT NULL,
  `service_type` varchar(45) NOT NULL,
  `follow_up` varchar(45) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `customer_comments` varchar(200) NOT NULL,
  `ticket_channel` varchar(45) NOT NULL,
  `refund_id` int(11) NOT NULL,
  `refund_date` int(11) NOT NULL,
  `refund_status` varchar(5) NOT NULL,
  `ord_hist_from` varchar(5) NOT NULL,
  `ord_hist_to` varchar(5) NOT NULL,
  `ord_hist_date` int(11) NOT NULL,
  `price_total` int(11) NOT NULL,
  `price_sub_total` int(11) NOT NULL,
  `price_discount` int(11) NOT NULL,
  `price_sub_dis` int(11) NOT NULL,
  `price_pay_sur` int(11) NOT NULL,
  `price_ship_cost` int(11) NOT NULL,
  `ord_date` int(11) NOT NULL,
  `ord_status` varchar(5) NOT NULL,
  `ord_fname` varchar(45) NOT NULL,
  `ord_lname` varchar(45) NOT NULL,
  `ord_company` varchar(45) NOT NULL,
  `ord_phone` int(11) NOT NULL,
  `ord_bphone` int(11) NOT NULL,
  `ord_sphone` int(11) NOT NULL,
  `ord_email` varchar(45) NOT NULL,
  `price_gift_charges` int(11) NOT NULL,
  `price_cb_used` int(11) NOT NULL,
  `price_gc_used` int(11) NOT NULL,
  `price_cod_fee` int(11) NOT NULL,
  `rma_hist_from` varchar(5) NOT NULL,
  `rma_hist_to` varchar(5) NOT NULL,
  `rma_hist_date` int(11) NOT NULL,
  `rma_rtrn_status` varchar(45) NOT NULL,
  `ord_prod_id` int(11) NOT NULL,
  `ord_prod_name` varchar(45) NOT NULL,
  `is_deal_order` int(1) DEFAULT NULL,
  `retrn_id` int(11) NOT NULL,
  `retrn_date` int(11) NOT NULL,
  `retrn_status` varchar(45) NOT NULL,
  `ship_id` int(11) NOT NULL,
  `ship_carrier` varchar(45) NOT NULL,
  `ship_date` int(11) NOT NULL,
  `ship_dlvry_date` int(11) NOT NULL,
  `ship_status` varchar(45) NOT NULL,
  `ship_type` varchar(45) NOT NULL,
  `ship_track_id` varchar(45) NOT NULL,
  `ord_clone_id` int(11) NOT NULL,
  `ord_clone_date` int(11) NOT NULL,
  `subject` mediumtext,
  `priority` mediumtext,
  `merchant` mediumtext,
  `merchant_type` int(1) DEFAULT NULL,
  `merchant_state_id` varchar(25) DEFAULT NULL,
  `billed` mediumtext,
  `pdd` int(10) DEFAULT '0',
  `manifest_id` int(11) DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `carrier_status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `carrier_name` varchar(45) DEFAULT NULL,
  `manifest_type_id` mediumint(8) NOT NULL,
  `manifest_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pickup_done` varchar(1) DEFAULT 'N',
  `tracking_no` int(11) DEFAULT NULL,
  `billing cycle` varchar(45) DEFAULT NULL,
  `LBH` varchar(45) DEFAULT NULL,
  `dispatch_date` datetime DEFAULT NULL,
  `expected_pickup_date` datetime DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category` mediumint(9) DEFAULT NULL,
  `closedBy` int(11) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `order_id` (`order_id`),
  KEY `manifest_id` (`manifest_id`),
  KEY `product_id` (`product_id`),
  KEY `carrier_id` (`carrier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `mst_return_flow` ADD `flowstatus` INT(1) NOT NULL DEFAULT '1';

ALTER TABLE `mst_task_flow` ADD `flowstatus` INT(1) NOT NULL DEFAULT '1';

ALTER TABLE `mst_order_flow` ADD `flowstatus` INT(1) NOT NULL DEFAULT '1';
UPDATE mst_config SET `value`=1 WHERE `key`='default_task_ed_sla_id';

insert into `mst_ticket_status` (`id`, `name`, `state`, `mode`, `flags`, `sort`, `properties`, `created`, `updated`) values('1','New','open','3','0','3','{\"description\":\"Open tickets.\"}','2015-12-16 13:45:29','0000-00-00 00:00:00');
insert into `mst_ticket_status` (`id`, `name`, `state`, `mode`, `flags`, `sort`, `properties`, `created`, `updated`) values('2','Closed','closed','1','0','5','{\"allowreopen\":false,\"description\":\"Resolved tickets\"}','2015-12-16 13:45:29','0000-00-00 00:00:00');
insert into `mst_ticket_status` (`id`, `name`, `state`, `mode`, `flags`, `sort`, `properties`, `created`, `updated`) values('3','Closed with Exception','closed','3','0','6','{\"allowreopen\":true,\"reopenstatus\":0,\"description\":\"Closed tickets. Tickets will still be accessible on client and staff panels.\"}','2015-12-16 13:45:30','0000-00-00 00:00:00');
insert into `mst_ticket_status` (`id`, `name`, `state`, `mode`, `flags`, `sort`, `properties`, `created`, `updated`) values('4','Archived','archived','3','0','7','{\"description\":\"Tickets only adminstratively available but no longer accessible on ticket queues and client panel.\"}','2015-12-16 13:45:30','0000-00-00 00:00:00');
insert into `mst_ticket_status` (`id`, `name`, `state`, `mode`, `flags`, `sort`, `properties`, `created`, `updated`) values('5','Deleted','deleted','3','0','8','{\"description\":\"Tickets queued for deletion. Not accessible on ticket queues.\"}','2015-12-16 13:45:30','0000-00-00 00:00:00');
insert into `mst_ticket_status` (`id`, `name`, `state`, `mode`, `flags`, `sort`, `properties`, `created`, `updated`) values('6','Open','open','1','0','4','{\"description\":\"acaesca\"}','2016-02-04 17:42:47','0000-00-00 00:00:00');


insert into `mst_ticket_priority` (`priority_id`, `priority`, `priority_desc`, `priority_color`, `priority_urgency`, `ispublic`) values('1','low','Low','#DDFFDD','4','1');
insert into `mst_ticket_priority` (`priority_id`, `priority`, `priority_desc`, `priority_color`, `priority_urgency`, `ispublic`) values('2','normal','Normal','#FFFFF0','3','1');
insert into `mst_ticket_priority` (`priority_id`, `priority`, `priority_desc`, `priority_color`, `priority_urgency`, `ispublic`) values('3','high','High','#FEE7E7','2','1');
insert into `mst_ticket_priority` (`priority_id`, `priority`, `priority_desc`, `priority_color`, `priority_urgency`, `ispublic`) values('4','emergency','Emergency','#FEE7E7','1','1');


DROP TABLE IF EXISTS `ost_form`;

CREATE TABLE `ost_form` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned DEFAULT NULL,
  `type` varchar(8) NOT NULL DEFAULT 'G',
  `flags` int(10) unsigned NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `instructions` varchar(512) DEFAULT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `notes` text,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;


insert  into `ost_form`(`id`,`pid`,`type`,`flags`,`title`,`instructions`,`name`,`notes`,`created`,`updated`) values (1,NULL,'U',1,'Contact Information',NULL,'',NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(2,NULL,'T',1,'Customer Support Ticket','Please Describe Your Issue','','This form will be attached to every ticket, regardless of its source. You can add any fields to this form and they will be available to all tickets, and will be searchable with advanced search and filterable.','2015-12-17 16:03:57','2016-01-12 00:46:03'),(3,NULL,'C',1,'Company Information','Details available in email templates','',NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(4,NULL,'O',1,'Organization Information','Details on user organization','',NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(5,NULL,'A',1,'Task Details','Please Describe The Issue','','This form is used to create a task.','2015-12-17 16:03:57','2015-12-17 16:03:57'),(6,NULL,'L1',1,'Ticket Status Properties','Properties that can be set on a ticket status.','',NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(7,NULL,'L2',1,'I will get back to you Properties',NULL,'',NULL,'2016-01-08 19:42:36','2016-01-08 19:42:36'),(8,NULL,'G',1,'Merchant Support Form','Form used for merchant support','','form for merchant support','2016-01-11 17:13:57','2016-01-11 19:05:03'),(9,NULL,'L3',1,'Order Variable Properties',NULL,'',NULL,'2016-01-20 15:05:46','2016-01-20 15:05:46'),(10,NULL,'L4',1,'Task Statuses Properties',NULL,'',NULL,'2016-01-20 15:09:20','2016-01-20 15:09:20'),(11,NULL,'L5',3,'State Machina Properties',NULL,'',NULL,'2016-02-05 15:58:05','2016-02-09 22:44:27'),(12,NULL,'L6',1,'State Machine Properties',NULL,'',NULL,'2016-02-10 15:04:19','2016-02-10 15:04:19'),(13,NULL,'L7',1,'End Status Properties',NULL,'',NULL,'2016-02-18 14:42:44','2016-02-18 14:42:44'),(14,NULL,'L8',1,'Task Response Properties',NULL,'',NULL,'2016-04-11 12:27:12','2016-04-11 12:27:12'),(15,NULL,'L9',1,'Task Sources Properties',NULL,'',NULL,'2016-04-11 12:27:43','2016-04-11 12:27:43'),(16,NULL,'L10',1,'Task Types Properties',NULL,'',NULL,'2016-04-11 12:28:43','2016-04-11 12:28:43'),(17,NULL,'L11',1,'Triage Status Properties',NULL,'',NULL,'2016-04-11 12:29:23','2016-04-11 12:29:23'),(18,NULL,'L12',1,'Priority Flags Properties',NULL,'',NULL,'2016-04-11 12:30:14','2016-04-11 12:30:14'),(19,NULL,'L13',3,'OrderStatus Response Map Properties',NULL,'',NULL,'2016-04-11 12:30:36','2016-05-26 12:27:30'),(20,NULL,'L14',1,'Communication Flags Properties',NULL,'',NULL,'2016-04-11 12:31:29','2016-04-11 12:31:29');

DROP TABLE IF EXISTS `ost_form_field`;

CREATE TABLE `ost_form_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL,
  `flags` int(10) unsigned DEFAULT '1',
  `type` varchar(255) NOT NULL DEFAULT 'text',
  `label` varchar(255) NOT NULL,
  `name` varchar(64) NOT NULL,
  `configuration` text,
  `sort` int(11) unsigned NOT NULL,
  `hint` varchar(512) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;


insert  into `ost_form_field`(`id`,`form_id`,`flags`,`type`,`label`,`name`,`configuration`,`sort`,`hint`,`created`,`updated`) values (1,1,489379,'text','Email Address','email','{\"size\":40,\"length\":64,\"validator\":\"email\"}',1,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(2,1,489379,'text','Full Name','name','{\"size\":40,\"length\":64}',2,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(3,1,13057,'phone','Phone Number','phone',NULL,3,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(4,1,12289,'memo','Internal Notes','notes','{\"rows\":4,\"cols\":40}',4,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(20,2,489249,'text','Issue Summary','subject','{\"size\":40,\"length\":50}',1,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(21,2,480547,'thread','Issue Details','message',NULL,2,'Details on the reason(s) for opening the ticket.','2015-12-17 16:03:57','2015-12-17 16:03:57'),(22,2,274609,'priority','Priority Level','priority',NULL,3,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(23,3,291233,'text','Company Name','name','{\"size\":40,\"length\":64}',1,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(24,3,12545,'text','Website','website','{\"size\":40,\"length\":64}',2,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(25,3,12545,'phone','Phone Number','phone','{\"ext\":false}',3,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(26,3,12545,'memo','Address','address','{\"rows\":2,\"cols\":40,\"html\":false,\"length\":100}',4,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(27,4,489379,'text','Name','name','{\"size\":40,\"length\":64}',1,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(28,4,13057,'memo','Address','address','{\"rows\":2,\"cols\":40,\"length\":100,\"html\":false}',2,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(29,4,13057,'phone','Phone','phone',NULL,3,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(30,4,13057,'text','Website','website','{\"size\":40,\"length\":0}',4,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(31,4,12289,'memo','Internal Notes','notes','{\"rows\":4,\"cols\":40}',5,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(32,5,290977,'text','Title','title','{\"size\":40,\"length\":50}',1,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(33,5,282867,'thread','Description','description',NULL,2,'Details on the reason(s) for creating the task.','2015-12-17 16:03:57','2015-12-17 16:03:57'),(34,6,487665,'state','State','state','{\"prompt\":\"State of a ticket\"}',1,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(35,6,471073,'memo','Description','description','{\"rows\":2,\"cols\":40,\"html\":false,\"length\":100}',3,NULL,'2015-12-17 16:03:57','2015-12-17 16:03:57'),(36,2,13057,'text','Order no','order_id',NULL,4,NULL,'2015-12-30 14:20:56','2016-01-28 13:44:22'),(37,0,13057,'choices','Group','group','{\"choices\":\"0:Select\\r\\n20506115:Admin\\r\\n20434751:In-Bound\\r\\n20185011:Not Shipped\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',5,NULL,'2016-01-08 19:33:22','2016-01-12 00:45:02'),(38,0,13057,'choices','Assignee','assignee','{\"choices\":\"288181898:CL\\r\\n207271474:Cl-Response\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',6,NULL,'2016-01-08 19:33:22','2016-01-12 00:45:02'),(39,0,13057,'text','Merchant Name','mer_name',NULL,7,NULL,'2016-01-08 19:34:29','2016-04-11 13:23:33'),(40,0,13057,'text','Product Category','prod_cat',NULL,8,NULL,'2016-01-08 19:34:29','2016-04-11 13:23:33'),(41,0,13057,'text','Product Name','prod_name',NULL,9,NULL,'2016-01-08 19:34:48','2016-04-11 13:23:33'),(42,0,13057,'choices','Value','prod_val','{\"choices\":\"0:Select\\r\\n1:Less than 1500\\r\\n2:Greater than 1500\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',10,NULL,'2016-01-08 19:34:48','2016-04-11 13:23:33'),(43,0,13057,'text','Order Status','status',NULL,11,NULL,'2016-01-08 19:35:31','2016-04-11 13:23:33'),(44,0,13057,'choices','Follow-Up Actions','followup_action','{\"choices\":\"BD-Arrange Invoice\\r\\nBD-Order in Process\\r\\nBD-Pending RMA Approval\\r\\nCL-Alternate Detail Required\\r\\nCL-AWB Not Traceable\\r\\nCL-Courier Issue regarding Parcel\\r\\nCL-Delivered-Change Status\\r\\nCL-Detained\\r\\nCL-In RTO\\r\\nCL-In Transit\\r\\nCL-Order Status to be Changed\\r\\nCL-Waiting for POD\\r\\nCL-Waiting for POD-2\\r\\nCL-Wrong Destination\\r\\nCL-Arrange RTO\\r\\nCL-Empty \\/ Tampered Parcel Received\\r\\nCL-HTC Beyond SLA\\r\\nCL-HTCL Beyond SLA\\r\\nCL-Octroi Charges paid by Customer\\r\\nCL-Other Charges paid by Customer\\r\\nCL-POD denied by customer\\r\\nCL-Wrong Delivery\\r\\nFO-Cancellation requested by merchant\\r\\nFO-Need to cancel\\r\\nFO-Procurement Delay\\r\\nPayment-Checkout Incomplete\\r\\nPayment-Courier\\/ Octroi\\/ Entry Tax Refund\\r\\nPayment-Courier\\/ Octroi\\/ Entry Tax Refund (NEFT)\\r\\nPayment-Double Charges\\r\\nPayment-Failed\\r\\nPayment-Gc Not Working\\r\\nPayment-NEFT\\r\\nPayment-Partial Refund (CBs\\/ACCT)\\r\\nPayment-Partial Refund (NEFT)\\r\\nPayment-Refund In CBs\\r\\nPayment-Refund Not Received\\r\\nPayment-Refund not received(NEFT)\\r\\nPayment-RMA Manual Creation\\r\\nPayment-RTO Manual Creation\\r\\nPayment-Split Issue\\r\\nPayment-Tracker Pendency\\r\\nPMT-Failed Incomplete\\r\\nRL-Clone required\\r\\nRL-Pickup Related\\r\\nRL-Reinitiate Return\\r\\nRL-Courier charges\\r\\nRL-Dispatched By Buyer\\r\\nRL-Merchant confirmation Required\\r\\nRL-Picked up\\r\\nRL-Picked up-Not Updated\\r\\nRL-POD Required by MO\\r\\nRL-POD Shared with MO\\r\\nRL-Rearrange RTM\\r\\nRL-Refund approval Required\\r\\nSales-Catalog Electronics\\r\\nSales-Catalog General Merchandise\\r\\nSales-Catalog Home\\r\\nSales-Catalog LifeStyle\\r\\nSales-Marketing\\r\\nSales-Technology\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',12,NULL,'2016-01-08 19:35:31','2016-04-11 13:23:33'),(45,0,13057,'choices','Billing Response','billing_response','{\"choices\":\"Payment Not Received\\r\\nStatus Changed\\r\\nStatement Required\\r\\nRefund Screen Shot\\/RRN Shared\\r\\nGC Activated\\r\\nUTR No Shared\\r\\nRefunded In PGW\\r\\nManual Request Created\\r\\nRefunded in CB\\r\\nRefunded in NEFT\\r\\nPending For Approval\\r\\nRefunded\\r\\nNEFT Required\\r\\nNot Eligible for Refund\\r\\nNEFT Bounced\\/Returned\\r\\nApproved in Tracker\\r\\nRefund not Approved\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',13,NULL,'2016-01-08 19:36:27','2016-04-11 13:23:33'),(46,0,13057,'choices','BD Response To RMA Request','bd_response','{\"choices\":\"Arrange Reverse Pkp and Replace\\r\\nAsk for Image\\r\\nCheck With Warehouse\\r\\nDelivered As Per Order, No Action\\r\\nElaborate The Issue\\r\\nMerchant Reply Awaited\\r\\nMerchant Sending Invoice\\r\\nMerchant Will Directly With Customer\\r\\nNot Related To Returns\\r\\nOffer Some CBs\\r\\nPartial Refund\\r\\nPickUp and Refund\\r\\nRefund and debit to courier\\r\\nRefund and No Reverse Pkp\\r\\nReplace and Debit To Courier\\r\\nQuality Issue Request Rejected\\r\\nReship and No Reverse Pkp\\r\\nReturn Case Already Initiated\\r\\nUnder Manufacture Wrnty Visit Service Center\\r\\nAsk customer to contact Merchant\\r\\nRejected due to Out Of SLA\\r\\nArrange Reverse Pkp but Replacement after Merchant confirmation\\r\\nArrange Reverse Pkp but Refund after Merchant confirmation\\r\\nFC reply awaited\\r\\nAsk for Video\\r\\nOthers\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',14,NULL,'2016-01-08 19:36:27','2016-01-28 21:33:34'),(47,0,13057,'choices','Refund Type','refund_type','{\"choices\":\"Full Refund in Account\\r\\nFull Refund in CB\\r\\nPartial Refund in Account\\r\\nPartial Refund in CBs\\r\\nRefund through RTGS\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',15,NULL,'2016-01-08 19:37:24','2016-04-11 13:23:33'),(48,0,13057,'choices','RL Response','rl_response','{\"choices\":\"Courier Charges Approved\\r\\nCourier Charges Not Approved\\r\\nArranging Pickup on Priority Basis\\r\\nCoordinating with Warehouse\\r\\nCoordinating with Merchant\\r\\nAsk for POD\\r\\nClone Created\\r\\nProduct Not Available : Refund\\r\\nRefund Mail Sent to Billing\\r\\nShipped to Merchant but Not Delivered\\r\\nToken number generated\\r\\nWait for Token Number\\r\\nPickup cancelled\\r\\nFinal NSS\\r\\nPicked Up-Inform Cust\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',16,NULL,'2016-01-08 19:37:24','2016-01-28 21:33:34'),(49,0,13057,'list-2','Followup','agent_followup',NULL,17,NULL,'2016-01-08 19:39:05','2016-04-11 13:23:33'),(50,0,13057,'text','Contact Number','contact',NULL,18,NULL,'2016-01-08 19:39:05','2016-04-11 13:23:33'),(51,0,13057,'choices','Refund Details','refund_details',NULL,19,NULL,'2016-01-08 19:39:29','2016-01-28 21:33:34'),(52,0,13057,'choices','Returns Group Issue','return_group',NULL,20,NULL,'2016-01-08 19:39:29','2016-01-28 21:33:34'),(53,0,13057,'choices','Max CBs Offered By BD','max_cb_offered',NULL,21,NULL,'2016-01-08 19:40:11','2016-01-12 00:45:02'),(54,0,13057,'choices','RMA','rma',NULL,22,NULL,'2016-01-08 19:40:11','2016-01-28 21:33:34'),(55,0,13057,'text','Ageing','ageing',NULL,23,NULL,'2016-01-08 19:40:28','2016-01-28 21:33:34'),(56,7,12289,'bool','On','',NULL,1,NULL,'2016-01-08 19:42:36','2016-01-08 19:42:36'),(57,7,12289,'bool','Off','',NULL,2,NULL,'2016-01-08 19:42:36','2016-01-08 19:42:36'),(58,5,13057,'list-10','Type','taskType','{\"choices\":\"CL: AWB Not Traceable\\r\\nCL: Courier Issue regarding Parcel\\r\\nCL: Delivered-Change Status\\r\\nCL: Detained\\r\\nCL: In RTO\\r\\nCL: In Transit\\r\\nCL: Order Status to be Changed\\r\\nCL: Waiting for POD\\r\\nCL: Waiting for POD-2\\r\\nCL: Wrong Destination\\r\\nCL:Arrange RTO\\r\\nCL:Empty \\/ Tampered Parcel Received\\r\\nCL:HTC Beyond SLA\\r\\nCL:HTCL Beyond SLA\\r\\nCL:Octroi Charges paid by Customer\\r\\nCL:Other Charges paid by Customer\\r\\nCL:POD denied by customer\\r\\nCL:Wrong Delivery\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',3,NULL,'2016-01-11 16:43:09','2016-04-12 12:36:54'),(59,8,30465,'text','Merchant Name','merchantname',NULL,1,NULL,'2016-01-11 17:13:57','2016-01-11 17:13:57'),(60,0,30465,'choices','MerchantIssueType','merchantissuetype','{\"choices\":\"Issue with billing\\r\\nIssue with RMA\\r\\nIssue with RTP\\r\\nIssue with catalog\",\"default\":\"\",\"prompt\":\"\",\"multiselect\":false}',2,NULL,'2016-01-11 17:13:57','2016-01-11 19:05:03'),(61,8,13057,'choices','Merchant Type','merchanttype',NULL,2,NULL,'2016-01-11 19:05:03','2016-01-11 19:05:03'),(62,8,13057,'text','Merchant Location','merchantlocation',NULL,3,NULL,'2016-01-11 19:05:03','2016-01-11 19:05:03'),(63,0,13057,'assignee','CS Owner','cs_owner','false',24,NULL,'2016-01-12 18:56:21','2016-04-11 13:23:33'),(64,5,13057,'list-9','Source','taskSource',NULL,4,NULL,'2016-01-29 11:28:35','2016-04-12 12:36:54'),(68,12,28673,'list-1','Next','next','{\"multiselect\":true,\"widget\":\"dropdown\",\"validator-error\":\"\",\"prompt\":\"\",\"default\":null}',1,NULL,'2016-02-10 15:05:15','2016-02-10 15:05:58'),(69,0,28673,'department','Department','dep','{\"multiselect\":true,\"widget\":\"dropdown\",\"validator-error\":\"\",\"prompt\":\"\",\"default\":null}',2,NULL,'2016-02-10 15:05:15','2016-04-11 13:07:38'),(70,0,13057,'text','EndStatus','EndStatus',NULL,25,NULL,'2016-02-18 14:40:45','2016-04-11 13:23:33'),(71,13,28673,'list-1','Initial','initial','{\"multiselect\":false,\"widget\":\"dropdown\",\"validator-error\":\"\",\"prompt\":\"\",\"default\":null}',1,NULL,'2016-02-18 14:42:44','2016-02-18 14:42:59'),(72,13,28673,'list-1','End','end','{\"multiselect\":false,\"widget\":\"dropdown\",\"validator-error\":\"\",\"prompt\":\"\",\"default\":null}',2,NULL,'2016-02-18 14:42:44','2016-02-18 14:43:04'),(73,14,12289,'list-4','Status','moveTaskStatus',NULL,1,NULL,'2016-04-11 12:36:06','2016-04-11 12:36:06'),(74,10,12289,'text','Next','Next',NULL,1,NULL,'2016-04-11 12:41:29','2016-04-11 12:41:29'),(75,16,12289,'list-8','Task Response','taskResponseForTypes','{\"multiselect\":true,\"widget\":\"dropdown\",\"validator-error\":\"\",\"prompt\":\"\",\"default\":null}',1,NULL,'2016-04-11 12:49:36','2016-04-11 12:52:26'),(76,5,13057,'list-4','Status','taskStatus',NULL,5,NULL,'2016-04-12 12:36:54','2016-04-12 12:36:54'),(77,5,13057,'list-8','Response','taskResponse',NULL,6,NULL,'2016-04-12 12:36:54','2016-04-12 12:36:54'),(78,5,13057,'text','Comments','taskComment',NULL,7,NULL,'2016-04-12 12:37:26','2016-04-12 12:37:26'),(79,5,13057,'text','Closed Status','closureStatus',NULL,8,NULL,'2016-08-29 17:27:52','2016-08-29 17:27:52'),(80,5,13057,'text','Created By','createdBy',NULL,9,NULL,'2016-08-29 17:27:52','2016-08-29 17:27:52'),(81,5,13057,'text','Closed By','closedBy',NULL,10,NULL,'2016-08-29 17:28:38','2016-08-29 17:28:38'),(82,5,13057,'text','Create Status','createStatus',NULL,11,NULL,'2016-08-29 17:28:38','2016-08-29 17:28:38');



DROP TABLE IF EXISTS `ost_translation`;

CREATE TABLE `ost_translation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_hash` char(16) CHARACTER SET ascii DEFAULT NULL,
  `type` enum('phrase','article','override') DEFAULT NULL,
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  `revision` int(11) unsigned DEFAULT NULL,
  `agent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(16) NOT NULL DEFAULT '',
  `text` mediumtext NOT NULL,
  `source_text` text,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`lang`),
  KEY `object_hash` (`object_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mst_form`;
DROP TABLE IF EXISTS `mst_translation`;
DROP TABLE IF EXISTS `mst_form_field`;
ALTER TABLE ost_form_field RENAME mst_form_field;
ALTER TABLE ost_translation RENAME mst_translation;
ALTER TABLE ost_form RENAME mst_form;
ALTER TABLE mst_task__data ADD COLUMN category MEDIUMINT DEFAULT NULL ;

INSERT INTO mst_config(`key`,`value`,`namespace`) VALUES ('merchantTicketCreation','{"exchange":"merchant_ticket_exchange","key":"ticket_create","queue":"merchant_ticket_create_queue"}','core');

#initial Structure 14-03-2017 | Meghraj Sharma | Task Flow on order status change

CREATE TABLE `mst_merchant_updation_cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` bigint(20) NOT NULL,
  `entity_type` varchar(10) NOT NULL DEFAULT 'Order',
  `from_status` varchar(10) NOT NULL,
  `to_status` varchar(10) NOT NULL,
  `transition_time` datetime NOT NULL,
  `processed` tinyint(4) NOT NULL DEFAULT '0',
  `update_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`entity_id`,`entity_type`),
  KEY `id` (`id`),
  KEY `processed` (`processed`),
  KEY `update_timestamp` (`update_timestamp`)
) ENGINE=InnoDB ;
INSERT INTO mst_config(`key`,`value`,`namespace`) VALUES ('merchantTicketCreation','{"exchange":"merchantTicket","key":"local","queue":"merchantQueue"}','core');
CREATE TABLE `mst_consumer_error_log` (
  `id` INT(20) NOT NULL AUTO_INCREMENT,
  `entityId` INT(20) DEFAULT NULL,
  `entityType` VARCHAR(256) DEFAULT NULL,
  `queueIdentifier` VARCHAR(256) DEFAULT NULL,
  `data` TEXT,
  `serverDetails` VARCHAR(256) DEFAULT NULL,
  `exceptionOccured` VARCHAR(256) DEFAULT NULL,
  `isProcessed` TINYINT(1) DEFAULT '0',
  `dateTimeAdded` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=INNODB ;
INSERT INTO mst_config (`key`, `value`, `namespace`) 
VALUES
  (
    'useMerchantRabbitmq',
    '1',
    'core'
  ) ;
ALTER TABLE mst_attachment ADD COLUMN user_id INT(16) DEFAULT NULL ;

ALTER TABLE mst_attachment ADD COLUMN staff_id INT(16) DEFAULT NULL ;
INSERT INTO mst_ticket_status (`name`,`state`,`mode`,`flags`,`sort`,`created`) VALUES('Solved','solved',1,0,2,NOW());
INSERT INTO mst_ticket_status (`name`,`state`,`mode`,`flags`,`sort`,`created`) VALUES('Reopened','reopened',1,0,2,NOW());
ALTER TABLE mst_ticket ADD COLUMN `solvedDateTime` DATETIME DEFAULT NULL;
ALTER TABLE mst_thread_event MODIFY COLUMN `state` ENUM('created','closed','reopened','assigned','transferred','overdue','edited','viewed','error','collab','resent','behalf','issueedit','orderlink','returnlink','priority','solved') DEFAULT NULL ;
ALTER TABLE mst_ticket ADD COLUMN `reopenedDateTime` DATETIME DEFAULT NULL;

CREATE TABLE mst_ticket_reopen_logs (
id INT UNSIGNED  NOT NULL AUTO_INCREMENT ,
ticket_id INT NOT NULL , 
solvedDateTime DATETIME DEFAULT NULL , 
reopenDateTime DATETIME DEFAULT NULL , 
reopen_log_status_id TINYINT DEFAULT 1 , 
addDateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)
);
INSERT INTO mst_config(`namespace`,`key`,`value`) VALUES ('core','autoClosureTime','72');
INSERT INTO mst_config(`namespace`,`key`,`value`) VALUES ('core','ticketClosureBatchSize','1000');
ALTER TABLE mst_task ADD COLUMN merchantReplyDateTime DATETIME DEFAULT NULL ;
ALTER TABLE mst_task__data ADD COLUMN extra VARCHAR(256) DEFAULT NULL ;

CREATE TABLE mst_seller_mail_queue (
id INT UNSIGNED NOT NULL AUTO_INCREMENT , 
task_id INT DEFAULT NULL ,
ticket_id int default null , 
order_id INT DEFAULT NULL , 
templateVariables VARCHAR(256) DEFAULT NULL , 
addDateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
startProcessDateTime DATETIME DEFAULT NULL , 
endProcessDateTime DATETIME DEFAULT NULL , 
isActive TINYINT DEFAULT 1 , 
isProcessed TINYINT DEFAULT 0,
PRIMARY KEY (id) 
);
INSERT INTO mst_config (`key`, `namespace`, `value`) 
VALUES
  (
    'image_task_mapping',
    'core',
    '[{"taskTypeId":"5","responseId":"109"}]'
  ) ;

INSERT INTO mst_config (`namespace`,`key`,`value`) VALUES ('core','mailingQueue',
'{"exchange":"merchantTicket","key":"local","queue":"mechantQueue"}');
INSERT INTO mst_config (`namespace`,`key`,`value`) VALUES ('core','use_mail_queue_ticket_create',
'1');
INSERT INTO mst_config (`namespace`,`key`,`value`) VALUES ('core','use_mail_queue_post_reply',
'1');
INSERT INTO mst_config (`namespace`,`key`,`value`) VALUES ('core','use_mail_task_closure_reply',
'1');
INSERT INTO mst_config (`namespace`,`key`,`value`) VALUES ('core','tpl_task_info_template_id',
'2');
UPDATE `mst_config` SET `value`='\n\n{\"ticket\":\"Ticket Num\",\"order\":\"Order ID\",\"mail\":\"Email\",\"phone\":\"Phone\",\"return\":\"Return ID\",\"pid\":\"PID\",\"manifest\":\"Manifest ID\",\"mid\":\"MID\"}\n\n' WHERE `id`='137';

INSERT INTO `mst_config`
(`namespace`,
`key`,
`value`,
`updated`)
VALUES
(
'core',
'merchant_cdn_path',
'http://cdn.shopclues.com',
NOW());

INSERT INTO `mst_config`
(`namespace`,
`key`,
`value`,
`updated`)
VALUES
(
'core',
'Open_ID',
'1',
NOW());

ALTER TABLE mst_ticket__data ADD COLUMN attachments TEXT DEFAULT NULL;

alter table mst_task__data add column autocommunication tinyint default '0';

INSERT INTO `mst_config`
(`namespace`,
`key`,
`value`,
`updated`)
VALUES
(
'core',
'showTaskButton',
'0',
NOW());
INSERT INTO `mst_email` (`email_id`, `noautoresp`, `priority_id`, `dept_id`, `topic_id`, `email`, `name`, `userid`, `userpass`, `mail_active`, `mail_host`, `mail_protocol`, `mail_encryption`, `mail_port`, `mail_fetchfreq`, `mail_fetchmax`, `mail_archivefolder`, `mail_delete`, `mail_errors`, `mail_lasterror`, `mail_lastfetch`, `smtp_active`, `smtp_host`, `smtp_port`, `smtp_secure`, `smtp_auth`, `smtp_spoofing`, `notes`, `created`, `updated`) VALUES('1','1','2','4','0','zeeshankhursheed@gmail.com','Zeeshan','zeeshan','$2$JDEk+fbYB+KmjBGhWCOFvg88eh7cdRFRKSKladwBlIOuH7w=','0','','POP','NONE',NULL,'5','10',NULL,'0','0',NULL,NULL,'0','',NULL,'1','1','0',NULL,'2016-05-11 18:10:42','2016-05-11 18:21:04');

ALTER TABLE mst_ticket__data ADD COLUMN attachment `text`  DEFAULT NULL ;
ALTER TABLE mst_task__data ADD COLUMN merchant_phone VARCHAR(100) DEFAULT NULL ;
ALTER TABLE mst_task__data ADD COLUMN merchant_sphone VARCHAR(100) DEFAULT NULL ;
alter table mst_task__data add column createStatus varchar(256) default null ;
alter table mst_task__data add column closureStatus varchar(256) default null ;
ALTER TABLE mst_help_topic ADD COLUMN capabilities varchar(512)  NOT NULL DEFAULT '{}' ;
alter table mst_attachment add column ticket_id int(11) not null;
alter table mst_file add column cdn tinyint(1) default 0;
alter table mst_attachment add column `created` datetime default null;

UPDATE `mst_config` SET `value`='{\"cdn\":1,\"parameter\":\"-azR\",\"local\":\"uploads/\",\"remote\":\"shopclue@i1:/mnt/vol6/images/OSTicket/\"}' WHERE `id`='142';
UPDATE 
  mst_config 
SET
  `value` = '{"exchange":"merchant_ticket_exchange","key":"ticket_mail","queue":"merchant_mailing_queue"}' 
WHERE `key` = 'mailingQueue' ;
update mst_config set `value`=1 where id=36;
INSERT INTO mst_config (`key`,`value`,`namespace`) VALUES('max_number_of_email_consumers',5,'core');
INSERT INTO mst_config (`key`,`value`,`namespace`) VALUES('max_number_of_ticket_create_consumers',5,'core');
insert into mst_config (`key`,`value`,`namespace`) values ('merchant_ticket_enable_queue',0,'core');
ALTER TABLE mst_seller_mail_queue ADD COLUMN merchant_id INT DEFAULT NULL ;
ALTER TABLE mst_seller_mail_queue ADD COLUMN manifest_id INT DEFAULT NULL ;
insert into mst_config (`key`,`value`,`namespace`) values('closeId','2','core');
CREATE TABLE `clues_merchant_tickets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_ids` varchar(512) DEFAULT NULL,
  `manifest_ids` varchar(512) DEFAULT NULL,
  `product_ids` varchar(512) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT '1',
  `merchant_name` varchar(256) DEFAULT NULL,
  `merchant_contact` varchar(256) DEFAULT NULL,
  `merchant_email` varchar(256) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `follow_up` varchar(100) DEFAULT 'Others',
  `remarks` varchar(500) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '0',
  `visibility` int(1) NOT NULL DEFAULT '0',
  `customer_comments` varchar(500) DEFAULT NULL,
  `ticket_number` varchar(50) DEFAULT NULL,
  `issue` varchar(50) DEFAULT NULL,
  `ticket_channel` varchar(100) DEFAULT NULL,
  `assignee` varchar(100) DEFAULT NULL,
  `remark` varchar(500) NOT NULL,
  `product_url` varchar(500) NOT NULL,
  `agent_user_id` bigint(20) NOT NULL,
  `updated_assignee` varchar(50) DEFAULT NULL,
  `updated_group` varchar(50) DEFAULT NULL,
  `ticket_status` varchar(20) NOT NULL,
  `sub_issue` varchar(50) DEFAULT NULL,
  `sub_sub_issue` varchar(50) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `dept_name` varchar(50) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order_ids`),
  KEY `product_idx` (`product_ids`),
  KEY `manifest_idx` (`manifest_ids`),
  KEY `idx_user_id` (`user_id`),
  KEY `agent_user_idx` (`agent_user_id`),
  KEY `idx_date` (`date`),
  KEY `customer_contact` (`merchant_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

UPDATE `mst_config` SET `value`='{\"cdn\":1,\"parameter\":\"-azR\",\"local\":\"../uploads/\",\"remote\":\"shopclue@i1:/mnt/vol6/images/OSTicket/\"}' WHERE `id`='142';

ALTER TABLE mst_task__data ADD COLUMN merchant_phone VARCHAR(100) DEFAULT NULL ;
ALTER TABLE mst_task__data ADD COLUMN merchant_sphone VARCHAR(100) DEFAULT NULL ;
alter table mst_task__data add column createStatus varchar(256) default null ;


alter table mst_ticket__data add column priority tinyint(1) defualt 2 NOT NULL; 

UPDATE `osticket3`.`mst_config` SET `value`='{\"maxSize\":\"250000\",\"maxNumber\":\"5\",\"type\":[\"image/jpg\",\"image/jpeg\",\"image/png\"],\"mimeType\":[\"text/php\",\"text/x-php\",\"application/php\",\"application/x-php\",\"application/x-httpd-php\",\"application/x-httpd-php-source\",\"image/svg+xml\"]}' WHERE `id`='139';



