<?php

/*
 * Class handling oder Status table
 * @author - Akash
 */

class AutoRefundCODCron extends VerySimpleModel {

    public function AutoRespond($max = 1) {
        global $cfg;
        $staticTaskFields = getCachedTaskFields();
        //For Pure COD Orders
        $refundTasks = json_decode($cfg->config['pureCODautoResponse']['value'],true);
        foreach($refundTasks as $taskType=>$response){
            $taskTypeArr[] = $taskType.",".$staticTaskFields["Default_Task Types"][$taskType];
        }
        
        $tasksObj = Task::objects()->filter(array("flags" => 1,"data__taskType__in"=>$taskTypeArr));
        $taskMapArr = $tasksObj->values('id','data__taskType')->all();
        foreach($taskMapArr as $taskMap){
            $taskTypeMap[$taskMap['id']]=$refundTasks[(explode(",",$taskMap['data__taskType'])[0])];
        }
        $tasksObj = Task::objects()->filter(array("flags" => 1,"data__taskType__in"=>$taskTypeArr));
        $memcacheObj = new MemcacheStorage();
        $TaskNotToRespond = $memcacheObj->getValue("PrepaidOrders");
        foreach($tasksObj as $tasks){
            if($TaskNotToRespond[$tasks->id]){
                $TaskNotToRespond[$tasks->id] = 1;
                continue;
            }
            $pureCODTicket = Ticket::objects()->filter(array("ticket_id"=>$tasks->object_id,'odata__payment_id'=>6,'odata__price_gc_used'=>0,'odata__price_cb_used'=>0,'odata__ord_status'=>'J'))->count();
            if($pureCODTicket>0){
                $refundAuto["taskId"] = $tasks->id;
                $refundAuto["taskResponse"] = $taskTypeMap[$tasks->id];
                $refundAuto["agent"] = "System";
                $refundAuto["email"] = "system.auto@shopclues.com";
                $tasks->updateTask($refundAuto,$error);
            }else{
                $TaskNotToRespond[$tasks->id] = 1;
            }
        }
        $memcacheObj->setValue("PrepaidOrders", $TaskNotToRespond);
        
    }

}
