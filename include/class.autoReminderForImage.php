<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author shopclues
 */
class AutoReminderForImage {
    private $config;

    public function __construct() {
        global $cfg;
        $this->config = $cfg;
    }

    public function sendReminderForImage() {
        global $ost;
        $ticketAndTaskData = $this->getTicketsForWhichMailIsToBeSent();
       // $templateName = $this->config->config['image_auto_task_mail_template']['value'];
        $templateDetails=UtilityFunctions::getAutoMailerTemplateDetails('More info requiered reminder mail','reminder');
	$templateName=$templateDetails['code_name'];
        $reminderTemplate = new EmailTemplate(0, null, $templateName);
        $responseId=  $this->config->config['image_task_response_id']['value'];
        $ticketIds = $ticketAndTaskData['ticketId'];
        $taskData = $ticketAndTaskData['taskRelatedInfo'];
	$maxReminderCount=$this->config->config['max_image_remider_count']['value'];
	$numberOfHoursAfterWhichMailtoBeSend=$this->config->config['more_info_reminder_time_gap']['value'];
        foreach ($taskData as $taskRelatedData) {
            $reminder_mail_count = $taskRelatedData['reminder_mail_count'];
            $last_reminder=$taskRelatedData['last_reminder_send_date'];
            // Create two new DateTime-objects...
            if(!$last_reminder){
                $last_reminder=$taskRelatedData['created'];
            }
            $lastReminderDateTime = new DateTime($last_reminder);
            $currentDateTime = new DateTime(date('Y-m-d H:i:s'));
            $diff = $currentDateTime->diff($lastReminderDateTime);
            $hoursDifference = (int)$diff->h;
            if( $hoursDifference>=$numberOfHoursAfterWhichMailtoBeSend
                     && 
                    $reminder_mail_count < $maxReminderCount){
                //increament reminder_mail_count by 1 . mail the merchant .
                $ticketObj = Ticket::objects()->filter(array('ticket_id' => $taskRelatedData['object_id']))->first();
                $msg = $ticketObj->replaceVars(
                        $reminderTemplate->asArray(), array(
                    'recipient' => $ticketObj->getOwner()
                        )
                );
                $email = $ost->getConfig()->getDefaultEmail();
                $email->send($ticketObj->getOwner(), Format::striptags($msg['subj']), $msg['body']);
                $autoPostNote['note'] = "Subject - " . $msg['subj'] . $msg['body'];
                $ticketObj->postNote($autoPostNote);
                $sql = "update mst_task__data set reminder_mail_count=reminder_mail_count+1,last_reminder_send_date=now() where task_id=" . $taskRelatedData['id'];
                db_query($sql);
            } else if($reminder_mail_count>=$maxReminderCount){
                
                //make a response on task ..
                $data = array();
                $data["taskId"] = $taskRelatedData['id'];
                $data["agent"] = "SYSTEM - On Task Reminder";
                $data["email"] = "system.auto@shopclues.com";   
                $data['source'] = 'CRON';
                $data["taskResponse"] = $responseId;
                $error=Task::updateTask($data, $errors, $data['source']);
            }
        }
    }

    public function getTicketsForWhichMailIsToBeSent() {
        $imageTaskType = $this->config->config['more_info_seller_task']['value'];
	$numberOfHoursAfterWhichMailtoBeSend=$this->config->config['more_info_reminder_time_gap']['value'];
	$deptAndTaskType=json_decode($imageTaskType,1);
	$deptId=$deptAndTaskType['dept'];
	$taskTypeId=$deptAndTaskType['taskType'];
        $sql = "SELECT 
                    mt.object_id, mt.created, mtd.reminder_mail_count,mt.id,mtd.last_reminder_send_date 
                FROM
                    mst_task mt
                        JOIN
                    mst_task__data mtd ON mt.id = mtd.task_id
                WHERE
                    mtd.taskType = $taskTypeId and mt.dept_id=$deptId AND mt.flags = 1 
		and ( mtd.last_reminder_send_date is null or time_to_sec(timediff(now(), mtd.last_reminder_send_date )) / 3600 >$numberOfHoursAfterWhichMailtoBeSend ) 
                     #  AND mt.created BETWEEN SUBDATE(CURRENT_DATE, 1) AND CURDATE() 
		     group by mt.object_id;";
	$rs = db_query($sql);
        $taskRelatedData = [];
        $ticketIds = [];
        while ($row = db_fetch_array($rs)) {
            $taskRelatedData[] = $row;
            $ticketIds[] = $row['object_id'];
        }
        $ticketIds = array_unique($ticketIds);
        return array('taskRelatedInfo' => $taskRelatedData, 'ticketId' => $ticketIds);
    }
}
?>

