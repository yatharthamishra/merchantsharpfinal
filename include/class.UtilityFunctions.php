<?php

/**
 * Class used for bulk operations
 * @author Akash Kumar
 */

class OstFlowLogs extends VerySimpleModel {

    static $meta = array(
        'table' => OST_FLOW_LOGS,
        'pk' => array('id')
    );

}


class UtilityFunctions {
   
    public static function flowLogs($table,$primaryId,$next,$staffId,$previous=null,$field=null,$extra=null){
      //  $previous = (!empty($previous)) ? "'$previous'" : "NULL";//print_r($previous);die;
        $previous = $previous ? $previous :"NULL";
        $extra = $extra ? $extra : "NULL";
        if(is_array($next)){
            $next=  json_encode($next);
        }
       if(is_array($previous)){
            $previous=  json_encode($previous);
        }
        
       
                
            $ostflowlogs=  OstFlowLogs::create(); 
            $ostflowlogs->table_name=$table;
            $ostflowlogs->field_name=$field;
            $ostflowlogs->primary_id = (string)$primaryId;
            $ostflowlogs->previous_value = (string)$previous;
            $ostflowlogs->next_value = (string)$next;
            $ostflowlogs->staff_id = (string)$staffId;
            $ostflowlogs->updated_time=date('Y-m-d H:i:s',time());
            $ostflowlogs->extra=$extra;
            
            $ostflowlogs->save(); 
           
        
  //  $sql="INSERT INTO `ost_flow_logs`(`table_name`, `field_name`, `primary_id`, `previous_value`, `next_value`, `staff_id`, `updated_time`,`extra`) VALUES ('".$table."','".$field."','".$primaryId."','".$previous."','".$next."','".$staffId."','".date('Y-m-d H:i:s',time())."','".$extra."')";
               /// db_query($sql);
    
       
                
    }
    
    public static function getInClauseFromArray($inputArray) {
        foreach ($inputArray as $key => $v) {
            if (is_int($v)) {
                $inputArray[$key] = "'" . $v . "'";
            }else{
                $inputArray[$key] = db_input($v);    
            }
        }
        $inputArray = array_unique($inputArray);
        $commaSeperatedInClause = implode(",", $inputArray);
        return $commaSeperatedInClause;
    }
    
    public static function getDataForMailingQueue(){
        
        
        return $mailingData;
    }
    public static function initiatizeObjectFromArray($objectToInitialize, $dataArray) {
	$objectToInitialize->ht=[];
        foreach ($dataArray as $keyName => $keyValue) {
            $objectToInitialize->ht[$keyName] = $keyValue;
        }
        return $objectToInitialize;
    }
    
    public static function getAutoMailerTemplateDetails($auto_mailer_name,$auto_mailer_type){
        $emailAutoMailerDetails=EmailAutoMailer::objects()->filter(array('auto_mailer_name'=>$auto_mailer_name,'auto_mailer_types'=>$auto_mailer_type))->values()->all();
        return $emailAutoMailerDetails[0];
    }
}
