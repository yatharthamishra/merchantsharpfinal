<?php

/**
* This class is used to check login using AJAX call
*/
require_once(INCLUDE_DIR.'class.ajax.php');
class LoginAjaxAPI extends AjaxController
{
	
	function __construct()
	{
		# code...
	}


	function validate()
	{
		global $cfg;
        $token = $_COOKIE['adminToken'];
        if($token){
           $tokenDecrypt = clues_decrypt_data ($token,1);
           if($tokenDecrypt){
                define("ADMIN_MEMCACHE_SERVERS",$tokenDecrypt["sessionServer"]);
                
                
                $totalServers = count(explode(",",$tokenDecrypt["sessionServer"]));
                $server = 0;
                while(!$adminData && $server<$totalServers){
                    $adminMemecacheObj = new MemcacheStorage(30000, 1,$server);
                    $adminData = $adminMemecacheObj->getValue("userdata" . $tokenDecrypt["userId"]);
                    $server++;
                }
           }
        }
		if(!$adminData )
		{
			$code=0;
			$msg="logged out";
			$data = array('code' =>$code ,
							'message' => $msg
						 );
			return json_encode($data);
		}
		else
		{
			$code=1;
			$msg="Agent logged in";
			$data = array('code' =>$code ,
							'message' => $msg
						 );
			return json_encode($data);
			//Http::response(200, 'Agent logged in');
		}
	}
}
