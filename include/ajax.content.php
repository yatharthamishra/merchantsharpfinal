<?php
/*********************************************************************
    ajax.content.php

    AJAX interface for content fetching...allowed methods.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
if(!defined('INCLUDE_DIR')) die('!');

require_once INCLUDE_DIR.'class.ajax.php';

class ContentAjaxAPI extends AjaxController {

    function log($id) {

        if($id && ($log=Log::lookup($id))) {
            $content=sprintf('<div
                    style="width:500px;">&nbsp;<strong>%s</strong><br><p
                    style="white-space:pre-line;">%s</p>
                    <hr><strong>%s:</strong> <em>%s</em> <strong>%s:</strong> <em>%s</em></div>',
                    $log->getTitle(),
                    Format::display(str_replace(',',', ',$log->getText())),
                    __('Log Date'),
                    Format::daydatetime($log->getCreateDate()),
                    __('IP Address'),
                    $log->getIP());
        }else {
            $content='<div style="width:295px;">&nbsp;<strong>'.__('Error').':</strong>'.
                sprintf(__('%s: Unknown or invalid ID.'), __('log entry')).'</div>';
        }

        return $content;
    }

    function ticket_variables() {
        $buttonStyle = "style='cursor:pointer; border: 1;margin-bottom: 1px;
                            background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);'";
        $content='
<div style="width:680px;">
    <h2>'.__('Ticket Variables').'</h2>
    '.__('Please note that non-base variables depend on the context of use. Visit osTicket Wiki for up to date documentation.').'
    <br/>
    <table width="100%" border="0" cellspacing=1 cellpadding=2>
        <tr><td width="55%" valign="top"><b>'.__('Base Variables').'</b></td><td><b>'.__('Other Variables').'</b></td></tr>
        <tr>
            <td width="55%" valign="top">
                <table class="variables" width="100%" border="0" cellspacing=1 cellpadding=1>
                    <tr><td width="130"><button '.$buttonStyle.'>%{ticket.id}</button></button></td><td>'.__('Ticket ID').' ('.__('internal ID').')</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.entity_id}</button></td><td>'.__('Entity Ids').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.entity_type}</button></td><td>'.__('Entity Type').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.number}</button></td><td>'.__('Ticket number').' ('.__('external ID').')</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.email}</button></td><td>'.__('Email address').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.name}</button></td><td>'.__('Full name').' &mdash;
                        <em>'.__('see name expansion').'</em></button></td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.subject}</button></td><td>'.__('Subject').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.phone}</button></td><td>'.__('Phone number | ext').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.status}</button></td><td>'.__('Status').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.priority}</button></td><td>'.__('Priority').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.assigned}</button></td><td>'.__('Assigned agent and/or team').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.create_date}</button></td><td>'.__('Date created').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.due_date}</button></td><td>'.__('Due date').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.close_date}</button></td><td>'.__('Date closed').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.recipients}</button></td><td>'.__('List of all recipient names').'</td></tr>
                    <tr><td nowrap><button '.$buttonStyle.'>%{recipient.ticket_link}</button></td><td>'.__('Auth. token used for auto-login').'<br/>
                    '.__('Agent\'s ticket view link').'</td></tr>
                    <tr><td colspan="2" style="padding:5px 0 5px 0;"><em><b>'.__('Expandable Variables').'</b></em></td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.topic}</button></td><td>'.__('Help topic').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.dept}</button></td><td>'.__('Department').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.staff}</button></td><td>'.__('Assigned/closing agent').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.team}</button></td><td>'.__('Assigned/closing team').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{ticket.thread}</button></td><td>'.__('Ticket Thread').'</td></tr>
             
                </table>
            </td>
            <td valign="top">
                <table class="variables" width="100%" border="0" cellspacing=1 cellpadding=1>
                    <tr><td width="100"><button '.$buttonStyle.'>%{message}</button></td><td>'.__('Incoming message').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{response}</button></td><td>'.__('Outgoing response').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{comments}</button></td><td>'.__('Assign/transfer comments').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{note}</button></td><td>'.__('Internal note <em>(expandable)</em>').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{assignee}</button></td><td>'.__('Assigned agent/team').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{assigner}</button></td><td>'.__('Agent assigning the ticket').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{url}</button></td><td>'.__('osTicket\'s base url (FQDN)').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{reset_link}</button></td>
                        <td>'.__('Reset link used by the password reset feature').'</td></tr>
                </table>
                <table class="variables" width="100%" border="0" cellspacing=1 cellpadding=1>
                    <tr><td colspan="2"><b>'.__('Name Expansion').'</b></td></tr>
                    <tr><td><button '.$buttonStyle.'>.first</button></td><td>'.__('First Name').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.last</button></td><td>'.__('Last Name').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.full</button></td><td>'.__('First Last').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.short</button></td><td>'.__('First L.').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.shortformal</button></td><td>'.__('F. Last').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.lastfirst</button></td><td>'.__('Last, First').'</td></tr>
                    <tr><td colspan="2" style="padding:5px 0 5px 0;"><em><b>'.__('Ticket Thread expansions').'</b></em></td></tr>
                    <tr><td><button '.$buttonStyle.'>.original</button></td><td>'.__('Original Message').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.lastmessage</button></td><td>'.__('Last Message').'</td></tr>
                    <tr><td colspan="2" style="padding:5px 0 5px 0;"><em><b>'.__('Thread Entry expansions').'</b></em></td></tr>
                    <tr><td><button '.$buttonStyle.'>.poster</button></td><td>'.__('Poster').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>.create_date</button></td><td>'.__('Date created').'</td></tr>
                </table>
            </td>
        </tr>
    </table>
</div>';

        return $content;
    }
    
    function add_template() {
        $templateName = $_POST['tmpName'];
        $templateGroup = $_POST['tmpGrp'];
        $templateDesc = $_POST['tmpDesc'];
        $templatePrefix=$_POST['tmpPrefix'];
        if ($templateName && $templateGroup && $templateDesc && $templatePrefix) {
            $templateGroupData = TemplateGroup::create();
            $code_name_with_dots = preg_replace('!\s+!', ' ', $templateName);
            $code_name = strtolower(str_replace(' ', '.', $code_name_with_dots));
            $codeNameDetails=EmailTemplateMeta::objects()->filter(array('code_name'=>$code_name))->values()->all();
            if(count($codeNameDetails)>0){
                return "Try Another template name , name already exists";
            }
	    $tempalateGroupArray=explode('.',$templateGroup);
	    $removed=array_shift($tempalateGroupArray);
	    $templateGroupToBeUsed=implode('.',$tempalateGroupArray);
            $code_name=$templateGroupToBeUsed.".".$code_name;
            $templateGroupData->code_name = $code_name;
            $templateGroupData->template_name = $templateName;
            $templateGroupData->description = $templateDesc;
            $templateGroupData->group_name = $templateGroup;
            $templateGroupData->save();
            $email_template = EmailTemplateMeta::create();
            $email_template->tpl_id = 1;
            $email_template->code_name = $code_name;
            $email_template->body=$templateDesc;
            $email_template->subject=$templateName;
            $email_template->created=SqlFunction::NOW();
            $email_template->updated=SqlFunction::NOW();
            $email_template->save();
            return "Success";
        }
        return "Some Error Occured";
    }
    
    function update_automail(){
            $templateCodeName=trim($_POST['code_name']);
            $templateId=trim($_POST['template_id']);
            $autoMailerId=trim($_POST['auto_mailer_id']);
            if($templateCodeName && $templateId && $templateCodeName){
                $emailAutoMailerObject=EmailAutoMailer::lookup($autoMailerId);
                $emailAutoMailerObject->code_name=$templateCodeName;
                $emailAutoMailerObject->template_id=$templateId;
                if($emailAutoMailerObject->save()){;
                    return "success";
                }else{
                    return "some error occured";
                }
            }
            return "some missing data";
    }

    /**
    * Order Based supported Variables
    * @author Akash Kumar
    */
    function order_variables() {
        $list = TicketOData::objects()->values()->limit(1)->all();
        foreach($list[0] as $key=>$value){ 
            $li[$key]=$key;
        }
        $content=
           '<div style="width:480px;">
            <h2>'.__('Order Variables').'</h2>
            '.__('Please note that non-base variables depend on the context of use.').'<br/>
                <table class="variables" width="100%" border="0" cellspacing=1 cellpadding=2>
                    <tr>
                        <tr>
                        <td colspan="2" style="padding:5px 0 5px 0;"><em><b><u>'
                            .__('Order Info Variables').'</u></b></em>'
                     . '</td>'
                    . '</tr>';
                  $content.='<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px;
                            background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">'.'%{order.items_product_1}'.'</button></td></tr>'
                          . '<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px;background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">'.'%{order.items_product_2}'.'</button></td></tr> '
                          . '<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px; background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">'.'%{order.items_pddEdd_1}'.'</button></td></tr> '
                          . '<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px; background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">'.'%{order.items_carrier_1}'.'</button></td></tr>'
                          . '<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px; background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">'.'%{order.items_trackingNo_1}'.'</button></td></tr>';  
                    foreach($li as $listElement){
                        $content.='<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px;
                            background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">'.'%{order.'.$listElement.'}'.'</button></td></tr>';
                    }
                    
                    
                    
              $statictask= getCachedTaskFields();
        $tasklist=$statictask['Default_Task Types'];
        foreach ($tasklist as $key=>$value) {
            $value1=  str_replace(' ','_',$value);
          $value2='updated_sla_'.$value1;
        $content.='<tr><td><button  style="cursor:pointer; border: 1;margin-bottom: 1px;
                            background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);">    '.'%{order.'.$value2.'}'.'</button></td></tr>';
        }
              $content.='</tr></table></div>';

        return $content;
    }
    
    
     function template_variables() {
        $return= ContentAjaxAPI::return_variables();
        $ticket= ContentAjaxAPI::ticket_variables();
        $order = ContentAjaxAPI::order_variables();
        return $return.$ticket.$order;
     }
    
     function return_variables() {
        $buttonStyle = "style='cursor:pointer; border: 1;margin-bottom: 1px;
                            background-color: rgba(92, 51, 255, 0.1);border-color: rgba(0, 0, 0, 0.05);'";
        $content='
 <div style="width:680px;">
    <h2>'.__('Return Variables').'</h2>
    '.__('Please note that non-base variables depend on the context of use. Visit osTicket Wiki for up to date documentation.').'
   <br/>
    <table width="100%" border="0" cellspacing=1 cellpadding=2>
        <tr><td width="55%" valign="top"><b>'.__('Base Variables').'</b></td><td><b>'.__('Other Variables').'</b></td></tr>
        <tr>
            <td width="55%" valign="top">
                <table class="variables" width="100%" border="0" cellspacing=1 cellpadding=1>
                                 <tr><td><button '.$buttonStyle.'>%{return.id}</button></td><td>'.__('Return ID').'</td></tr>
                   <tr><td><button '.$buttonStyle.'>%{return.address}</button></td><td>'.__('Return Address').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{return.quantity}</button></td><td>'.__('Return Quantity').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{return.status}</button></td><td>'.__('Return Status').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{return.action}</button></td><td>'.__('Return Action').'</td></tr>
                    <tr><td><button '.$buttonStyle.'>%{return.date}</button></td><td>'.__('Return Date').'</td></tr>
                          <tr><td><button '.$buttonStyle.'>%{return.amount}</button></td><td>'.__('Return Amount').'</td></tr>
                        </table>
                        </td>
               <td width="55%" valign="top">
                <table class="variables" width="100%" border="0" cellspacing=1 cellpadding=1>                   <tr><td><button '.$buttonStyle.'>%{return.mode}</button></td><td>'.__('Return Mode').'</td></tr>
           <tr><td><button '.$buttonStyle.'>%{return.product}</button></td><td>'.__('Return Product').'</td></tr>
             <tr><td><button '.$buttonStyle.'>%{return.reason}</button></td><td>'.__('Return Reason').'</td></tr>
             <tr><td><button '.$buttonStyle.'>%{return.bank_name}</button></td><td>'.__('Return Bank Name').'</td></tr>
              <tr><td><button '.$buttonStyle.'>%{return.bank_ifsc}</button></td><td>'.__('Return Bank Ifsc Code').'</td></tr>
            <tr><td><button '.$buttonStyle.'>%{return.bank_ac}</button></td><td>'.__('Return Account Number').'</td></tr>
           <tr><td><button '.$buttonStyle.'>%{return.bank_branch}</button></td><td>'.__('Return Bank Branch').'</td></tr>
            
                </table>
            </td>
        </tr>
    </table>
</div>';

        return $content;
    }
    
    
    
    function getSignature($type, $id=null) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');

        switch ($type) {
        case 'none':
            break;
        case 'agent':
            if (!($staff = Staff::lookup($id)))
                Http::response(404, 'No such staff member');
            echo Format::viewableImages($staff->getSignature());
            break;
        case 'mine':
            echo Format::viewableImages($thisstaff->getSignature());
            break;
        case 'dept':
            if (!($dept = Dept::lookup($id)))
                Http::response(404, 'No such department');
            echo Format::viewableImages($dept->getSignature());
            break;
        default:
            Http::response(400, 'Unknown signature type');
            break;
        }
    }

    function manageContent($id, $lang=false) {
        global $thisstaff, $cfg;

        if (!$thisstaff)
            Http::response(403, 'Login Required');

        $content = Page::lookup($id, $lang);

        $langs = Internationalization::getConfiguredSystemLanguages();
        $translations = $content->getAllTranslations();
        $info = array(
            'title' => $content->getName(),
            'body' => $content->getBody(),
        );
        foreach ($translations as $t) {
            if (!($data = $t->getComplex()))
                continue;
            $info['trans'][$t->lang] = array(
                'title' => $data['name'],
                'body' => $data['body'],
            );
        }

        include STAFFINC_DIR . 'templates/content-manage.tmpl.php';
    }

    function manageNamedContent($type, $lang=false) {
        global $thisstaff, $cfg;

        if (!$thisstaff)
            Http::response(403, 'Login Required');

        $langs = $cfg->getSecondaryLanguages();

        $content = Page::lookupByType($type, $lang);
        $info = $content->getHashtable();

        include STAFFINC_DIR . 'templates/content-manage.tmpl.php';
    }

    function updateContent($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        elseif (!($content = Page::lookup($id)))
            Http::response(404, 'No such content');

        if (!isset($_POST['body']))
            $_POST['body'] = '';

        $vars = array_merge($content->getHashtable(), $_POST);
        $errors = array();

        // Allow empty content for the staff banner
        if ($content->update($vars, $errors,
            $content->getType() == 'banner-staff')
        ) {
            Http::response(201, 'Have a great day!');
        }
        if (!$errors['err'])
            $errors['err'] = __('Correct the error(s) below and try again!');
        $info = $_POST;
        $errors = Format::htmlchars($errors);
        include STAFFINC_DIR . 'templates/content-manage.tmpl.php';
    }

    function context() {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        if (!$_GET['root'])
            Http::response(400, '`root` is required parameter');

        $items = VariableReplacer::getContextForRoot($_GET['root']);

        if (!$items)
            Http::response(422, 'No such context');

        header('Content-Type: application/json');
        return $this->encode($items);
    }
}
?>
