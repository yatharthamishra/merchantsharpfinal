<?php
/*********************************************************************
    ajax.kbase.php

    AJAX interface for knowledge base related...allowed methods.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
if(!defined('INCLUDE_DIR')) die('!');
//ini_set('display_errors',1);
//error_reporting(-1);

class KbaseAjaxAPI extends AjaxController {

    function cannedResp($id, $format='text') {
        global $thisstaff, $cfg;

        include_once(INCLUDE_DIR.'class.canned.php');

        if(!$id || !($canned=Canned::lookup($id)) || !$canned->isEnabled())
            Http::response(404, 'No such premade reply');

        if (!$cfg->isRichTextEnabled())
            $format .= '.plain';

        return $canned->getFormattedResponse($format);
    }

    function faq($id, $format='html') {
        //XXX: user ajax->getThisStaff() (nolint)
        global $thisstaff;
        include_once(INCLUDE_DIR.'class.faq.php');

        if(!($faq=FAQ::lookup($id)))
            return null;

        //TODO: $fag->getJSON() for json format. (nolint)
        $resp = sprintf(
                '<div style="width:650px;">
                 <strong>%s</strong><div class="thread-body">%s</div>
                 <div class="clear"></div>
                 <div class="faded">'.__('Last updated %s').'</div>
                 <hr>
                 <a href="faq.php?id=%d">'.__('View').'</a> | <a href="faq.php?id=%d">'.__('Attachments (%d)').'</a>',
                $faq->getQuestion(),
                $faq->getAnswerWithImages(),
                Format::daydatetime($faq->getUpdateDate()),
                $faq->getId(),
                $faq->getId(),
                $faq->getNumAttachments());
        if($thisstaff
                && $thisstaff->hasPerm(FAQ::PERM_MANAGE)) {
            $resp.=sprintf(' | <a href="faq.php?id=%d&a=edit">'.__('Edit').'</a>',$faq->getId());

        }
        $resp.='</div>';

        return $resp;
    }

    function manageFaqAccess($id) {
        global $ost, $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Agent login required');
        if (!$thisstaff->hasPerm(FAQ::PERM_MANAGE))
            Http::response(403, 'Access denied');
        if (!($faq = FAQ::lookup($id)))
            Http::response(404, 'No such faq article');

        $form = new FaqAccessMgmtForm($_POST ?: $faq->getHashtable());

        if ($_POST && $form->isValid()) {
            $clean = $form->getClean();
            $faq->ispublished = $clean['ispublished'];
            $faq->save();
            Http::response(201, 'Have a nice day');
        }

        $title = __("Manage FAQ Access");
        $verb = __('Update');
        $path = ltrim($ost->get_path_info(), '/');

        include STAFFINC_DIR . 'templates/quick-add.tmpl.php';
    }
    
    /****************************************
    * This function is used to auto select department while selecting canned
    * while creating a new ticket
    */ 
   function getFieldsForCanned($cannedid)
   {
     
     include_once(INCLUDE_DIR.'class.canned.php');
     if(!$cannedid || !($canned=Canned::lookup($cannedid)) || !$canned->isEnabled())
            Http::response(404, 'No such premade reply');
     
     $result = $canned->getDepartmentsFromCanned($cannedid);
     // Result contains array of canned_id (key) => department_id (value)
     return $result;
     
   }
   /******************************************
    * This function is used to get assignee while selecting department
    * while adding a new canned response
    */
   
   function getAssigneeForCanned($departmentid)
   {
    include_once(INCLUDE_DIR.'class.canned.php');
    $canned = new Canned();
    if(!$departmentid)
    Http::response(404, 'No such premade reply');
    
    $result = $canned->getAssigneeForDept($departmentid);
    $result = json_decode($result);
    //$_GET['other_dept_id']=array("1","2");
    if($_GET['other_dept_id'])
    {
        foreach ($_GET['other_dept_id'] as $key) {
            $deptIds  = json_decode($canned->getAssigneeForDept($key));
            foreach($deptIds as $item)
            {
                if($item!=NULL)
                array_push($result, $item);
            }
        }
    }

    // Result contains array of assignee id, first name, last name
    return json_encode($result);
     
   }
   
   /******** Function to get fields corrosponding to canned and update
    * fields for ticket*********/
   function getUpdateCannedFields($canned_id)
   {
     include_once(INCLUDE_DIR.'class.canned.php');
     if(!$canned_id || !($canned=Canned::lookup($canned_id)) || !$canned->isEnabled())
             Http::response(404, 'No such premade reply'); 
     $result = $canned->getAndUpdateTicketFieldsForCanned($canned_id,$_GET);
     return $result;
   }
   
   /******* Function to update canned text in thread **********/
   function updateCannedTextinThread()
   {
     include_once(INCLUDE_DIR.'class.canned.php');
     $canned = new Canned();
     $result = $canned->updateCannedInThread($_POST);
     return $result;
   }
   /******* Function to get list of canned response with respect to particular department **********/
   function responselistforDept()
   {
        $text="<option value='0' selected='selected'>". __('Select a canned response') . "</option><option value='original'>" . __('Original Message') . "</option><option value='lastmessage'>" . __('Last Message') . "</option>";
        $dept_id = $_POST['dept_id'];
        include_once(INCLUDE_DIR.'class.canned.php');
        $canned = new Canned();
        if(($cannedResponses=$canned->responsesByDeptId($dept_id))) {
            $text= $text . '<option value="0" disabled="disabled">
                ------------- '.__('Premade Replies').' ------------- </option>';
            foreach($cannedResponses as $id =>$title)
                $text = $text . sprintf('<option value="%d">%s</option>',$id,$title);
        }
        //$text = $text . $dept_id . $_POST['dept_name'];
        return $text;
   }
   
}
?>
