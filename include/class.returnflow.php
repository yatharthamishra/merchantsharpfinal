<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ReturnFlowModel extends VerySimpleModel{
    static $meta = array(
        'table' => RETURN_FLOW_TABLE,
        'ordering' => array('flow_id'),
        'pk' => array('flow_id')
    );
}

class ReturnFlow extends ReturnFlowModel{
        static function lookup($id) {
        $returnflow = self::objects()->filter(array("flowstatus"=>1,'flow_id'=>$id))->first();
        return $returnflow; 
    }
}

