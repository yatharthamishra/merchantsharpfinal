<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class OrderFlowModel extends VerySimpleModel{

    static $meta = array(
        'table' => ORDER_FLOW_TABLE,
        'ordering' => array('orderflow_id'),
        'pk' => array('orderflow_id')
    );
}

class OrderFlow extends OrderFlowModel{
        static function lookup($id) {
        $orderflow = self::objects()->filter(array("flowstatus"=>1,'orderflow_id'=>$id))->first();
        return $orderflow; 
    }
}

